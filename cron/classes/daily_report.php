<?php
define('BASEPATH', dirname(dirname(dirname(__FILE__))) . '\system');
define('FCPATH', dirname(dirname(dirname(__FILE__))));
require_once (dirname(dirname(dirname(__FILE__))) . '/application/config/config.php');
require_once (dirname(dirname(dirname(__FILE__))) . '/application/config/constants.php');
require_once (dirname(dirname(dirname(__FILE__))) . '/application/helpers/mpdf/mpdf.php');
require_once (dirname(__FILE__) . '/db.php');

class daily_report {
	public function __construct() {
		$this->db = new db();
	}
	
	public function get_last_daily_report() {
		$query = 'SELECT DATE_FORMAT(report_date, "%Y-%m-%d") as report_date FROM ' . TBL_DAILY_REPORT . ' ORDER BY id DESC limit 1';
		$resource = $this->db->query($query);
		if ($this->db->num_rows($resource)) {
			$report = $this->db->fetch_object_row($resource);
			if (!empty($report->report_date)) {
				$report_date = date('Y-m-d', strtotime($report->report_date . '+1 day'));
			}
			else {
				$report_date = date('Y-m-d', strtotime(date('Y-m-d')));
			}
		}
		else {
			$report_date = date('Y-m-d', strtotime(date('Y-m-d')));
		}
		
		return $report_date;
	}
	
	public function send_daily_report($report_date) {
		$current_date = strtotime(date('Y-m-d'));
		
		if (strtotime($report_date) <= $current_date) {
			global $config;
			$base_url = $config['base_url'];
			//Current Company Details
			$query = 'select * from ' . TBL_COMP . ' where status = 1';
			$resource = $this->db->query($query);
			$company = $this->db->fetch_object_row($resource);
			
			if ((!empty($company->comp_logo)) && (file_exists(FCPATH . '/uploads/' . $company->comp_logo))) {
				$image_url = $base_url . 'uploads/' . $company->comp_logo;
				$alt = $company->comp_name;
			}
			else {
				$image_url = $base_url . 'assets/images/logo.png';
				$alt = 'Logo not found!';
			}
			
			$bg_clr = !empty($bg_clr) ? $bg_clr : '#666';
			$ft_clr = !empty($ft_clr) ? $ft_clr : '#fff';
			$full_width = !empty($full_width) ? $full_width : '999px';
			
			$header = '<table border="0" style="border-collapse:collapse; font-family: calibri; font-size: 14px; width: ' . $full_width . '" cellpadding="5">';
			$header .= '<tr>';
			$header .= '<td width="74%">';
			$header .= '<img src="' . $image_url . '" alt="' . $alt . '" />';
			$header .= '</td>';
			$header .= '<td width="25%" style="text-align: center;">';
			$header .= '<div style="font-size: 20px; padding: 5px 10px; background: ' . $bg_clr . '; color: ' . $ft_clr . '">DAILY REPORT</div>';
			$header .= '<b>Date: </b>' . date('d-m-Y', strtotime($report_date));
			$header .= '</td>';
			$header .= '</tr>';
			
			$header .= '<tr>';
			$header .= '<td colspan="2"><hr /></td>';
			$header .= '</tr>';
			$header .= '</table>';
			
			$query = 'select * from ' . TBL_BRN . ' where status = 1';
			$resource = $this->db->query($query);
			$branches = $this->db->fetch_object_array($resource);
			$i = 0;
			//$body = '';
			$mail_html = $header;
			
			$files = array();
			foreach ($branches as $key => $branch) {
				$body = '<table border="0" style="border-collapse: collapse; font-family: calibri; font-size: 14px; width: ' . $full_width . '" cellpadding="5">';
				$body .='<tr><td height="10"></td></tr>';
				$body .='<tr><td style="font-size: 18px; text-align: left;"><b style="padding-right: 10px;">Branch : </b>' . $branch->branch_name . '</td></tr>';
				$body .= '</table>';
				
				$body .= '<table border="0" style="border-collapse: collapse; font-family: calibri; font-size: 14px; width: ' . $full_width . '" cellpadding="5">';
				$body .= '<thead>';
				$body .= '<tr>';
				$body .= '<th width="5%" style="border-left: 1px solid #000; border-top: 1px solid #000;">S.No</th>';
				$body .= '<th width="13%" style="border-left: 1px solid #000; border-top: 1px solid #000;">Invoice No.</th>';
				$body .= '<th width="13%" style="border-left: 1px solid #000; border-top: 1px solid #000;">Invoice Date</th>';
				$body .= '<th width="19%" style="border-left: 1px solid #000; border-top: 1px solid #000;">Customer</th>';
				$body .= '<th width="10%" style="border-left: 1px solid #000; border-top: 1px solid #000;">Sub Total</th>';
				$body .= '<th width="10%" style="border-left: 1px solid #000; border-top: 1px solid #000;">Discount</th>';
				
				$colspan = 8;
				if (!empty($company->comp_gst_no)) {
					$colspan ++;
					$body .= '<th width="10%" style="border-left: 1px solid #000; border-top: 1px solid #000;">GST Amt</th>';
				}
				
				$body .= '<th width="10%" style="border-left: 1px solid #000; border-top: 1px solid #000;">Total Amt</th>';
				$body .= '<th width="9%" style="border: 1px solid #000; border-bottom: none;">Profit</th>';
				$body .= '</tr>';
				$body .= '</thead>';
				
				$body .= '<tbody>';
				
				//Invoice List
				$query = 'SELECT so.id, so.so_no, DATE_FORMAT(so.so_date, "%d-%m-%Y") as so_date, so.sub_total, so.gst_amount, so.discount_amount, so.total_amt, CASE so.so_type WHEN 1 THEN cus.name ELSE so.customer_name END as name, brn.branch_name, CASE so.gst WHEN 1 THEN SUM(sitem.item_profit) - so.discount_amount ELSE SUM(sitem.item_profit) - so.discount_amount END AS tot_profit, CASE so.status WHEN 3 THEN "Delivered" WHEN 2 THEN "Partial Delivered" ELSE "Not Delivered" END as del_sts, CASE so.payment_status WHEN 1 THEN "Paid" WHEN 3 THEN "Cancelled" ELSE "UnPaid" END as pay_sts, so.so_date FROM (' . TBL_SO . ' AS so) LEFT JOIN ' . TBL_CUS . ' as cus ON cus.id = so.customer_id LEFT JOIN ' . TBL_BRN . ' as brn ON brn.id = so.branch_id LEFT JOIN ' . TBL_SO_ITM . ' as sitem ON sitem.so_id = so.id';
				$query .= ' WHERE ((DATE(so.created_on) >= "' . $report_date . '" AND DATE(so.created_on) <= "' . $report_date . '")';
				#$query .= ' OR (DATE(so.updated_on) >= "' . $from_date  . '" AND DATE(so.updated_on) <= "' . $to_date . '")';
				$query .= ') AND so.status != 10 AND so.branch_id = ' . $branch->id . ' GROUP BY so.id ORDER BY so.id desc';
				$resource = $this->db->query($query);
				if ($this->db->num_rows($resource)) {
					$invoices = $this->db->fetch_object_array($resource);
					$key = 0;
					$to_sub = $to_dis = $to_gst = $to_amt = $profit = 0;
					foreach($invoices as $key => $row) {
						$to_sub += $row->sub_total;
						$to_dis += $row->discount_amount;
						$to_gst += $row->gst_amount;
						$to_amt += $row->total_amt;
						$profit += $row->tot_profit;
						$body .= '<tr>';
						$body .= '<td style="border-left: 1px solid #000; border-top: 1px solid #000;">' . ++ $key . '</td>';
						$body .= '<td style="border-left: 1px solid #000; border-top: 1px solid #000;">' . $row->so_no . '</td>';
						$body .= '<td style="border-left: 1px solid #000; border-top: 1px solid #000;">' . date('d-m-Y', strtotime($row->so_date)) . '</td>';
						$body .= '<td style="border-left: 1px solid #000; border-top: 1px solid #000;">' . strtoupper($row->name) . '</td>';
						$body .= '<td style="border-left: 1px solid #000; border-top: 1px solid #000;" align="right">' . number_format($row->sub_total, 2) . '</td>';
						$body .= '<td style="border-left: 1px solid #000; border-top: 1px solid #000;" align="right">' . number_format($row->discount_amount, 2) . '</td>';
						
						if (!empty($company->comp_gst_no)) {
							$body .= '<td style="border-left: 1px solid #000; border-top: 1px solid #000;" align="right">' . number_format($row->gst_amount, 2) . '</td>';
						}
						
						$body .= '<td style="border-left: 1px solid #000; border-top: 1px solid #000;" align="right">' . number_format($row->total_amt, 2) . '</td>';
						$body .= '<td style="border: 1px solid #000; border-bottom: none;" align="right">' . number_format($row->tot_profit, 2) . '</td>';
						$body .= '</tr>';
					}
					
					#$body .= '<tfoot>';
					$body .= '<tr>';
					$body .= '<th colspan="4" style="border: 1px solid #000; border-right: none; text-align: right; padding-right: 10px;">Total</th>';
					$body .= '<td align="right" style="border: 1px solid #000; border-right: none;">' . number_format($to_sub, 2) . '</td>';
					$body .= '<td align="right" style="border: 1px solid #000; border-right: none;">' . number_format($to_dis, 2) . '</td>';
					
					if (!empty($company->comp_gst_no)) {
						$body .= '<td align="right" style="border: 1px solid #000; border-right: none;">' . number_format($to_gst, 2) . '</td>';
					}
					
					$body .= '<td align="right" style="border: 1px solid #000; border-right: none;">' . number_format($to_amt, 2) . '</td>';
					$body .= '<td align="right" style="border: 1px solid #000;">' . number_format($profit, 2) . '</td>';
					$body .= '</tr>';
					#$body .= '</tfoot>';
				}
				else {
					$body .= '<tr><td colspan="' . $colspan . '" align="center" style="border: 1px solid #000;">No Record Found</td></tr>';
				}
				$body .= '</table>';
				$body .= '<br />';
				$body .= '<table border="0" style="border-collapse: collapse; font-family: calibri; font-size: 14px; width: ' . $full_width . '" cellpadding="5">';
				#$body .= '<tr><td colspan="9" height="50">&nbsp;</td></tr>';
				$total_amt = 0;
				
				$query = 'SELECT id, type_name FROM ' . TBL_PAYMENT_TYPE . ' where status =1';
				$resource = $this->db->query($query);
				if ($this->db->num_rows($resource)) {
					$payment_types = $this->db->fetch_object_array($resource);
					foreach($payment_types as $key => $row) {
						$query = 'SELECT SUM(sp.pay_amt) as pay_amt FROM (' . TBL_SO . ' AS so) LEFT JOIN ' . TBL_SPM . ' as sp ON sp.so_id = so.id AND sp.status != 10 AND sp.pay_pay_type = ' . $row->id . ' LEFT JOIN ' . TBL_BRN . ' as brn ON brn.id = so.branch_id';
						$query .= ' WHERE ((DATE(so.created_on) >= "' . $report_date . '" AND DATE(so.created_on) <= "' . $report_date . '")';
						#$query .= ' OR (DATE(so.updated_on) >= "' . date('Y-m-d') . '" AND DATE(so.updated_on) <= "' . date('Y-m-d H:i:s') . '")';
						$query .= ') AND so.status != 10 AND so.branch_id = ' . $branch->id;
						$resource = $this->db->query($query);
						$invoice_payments = $this->db->fetch_object_array($resource);
						foreach($invoice_payments as $key => $payment) {
							$amount = (!empty($payment->pay_amt)) ? $payment->pay_amt : '0.00';
							$body .= '<tr>';
							$body .= '<th style="width: 618px;">&nbsp;</th>';
							$body .= '<td style="width: 200px; border-left: 1px solid #000; border-top: 1px solid #000;"><b>' . $row->type_name . '</b></td>';
							$body .= '<td align="right" style="width: 180px; border: 1px solid #000; border-bottom: none;">S$ ' . $amount . '</td>';
							$body .= '</tr>';
							$total_amt += $amount;
						}
					}
				}
				
				$body .= '<tr>';
				$body .= '<th style="width: 618px;">&nbsp;</th>';
				$body .= '<td style="width: 200px;border-left: 1px solid #000; border-top: 1px solid #000; background-color: #ccc;"><b>Total</b></td>';
				$body .= '<td align="right" style="width: 180px; border: 1px solid #000; border-bottom: none; background-color: #ccc;">S$ ' . number_format($total_amt, 2) . '</td>';
				$body .= '</tr>';
				
				$body .= '<tr>';
				$body .= '<th>&nbsp;</th>';
				$body .= '<th colspan="2" style="border-top: 1px solid #000;"></th>';
				$body .= '</tr>';
				
				$body .= '<tr>';
				$body .= '<td colspan="3"><hr /></td>';
				$body .= '</tr>';
				$body .= '</tbody>';
				$body .= '</table>';
				
				//Took back file update on cron/daily_report folder
				$mpdf = new mPDF();
				$mpdf->WriteHTML($header . $body);
				$filename = date("d-M-Y_h-i-A") . '.pdf';
				$branch_name = trim($branch->branch_name);
				$branch_dir = dirname(__DIR__) . '/daily_report/' . $branch_name;
				
				if (!is_dir($branch_dir)) {
					if (!mkdir($branch_dir, 0777, true)) 
						error_log('Failed to create folder for ' . $branch_name);
				}
				$file_path = $branch_dir . '/' . $filename;
				$mpdf->Output($file_path, 'F');
				$file = array(
					'file_path' => $file_path,
					'file_name' => $filename
				);
				array_push($files, $file);
				$mail_html .= $body;
			}
			
			$to = $company->daily_to_email;
			$from = $company->daily_from_email;
			$subject = "DAILY REPORT";
			
			$headers  = "MIME-Version: 1.0" . "\r\n";
			$headers .= "From: " . $from . "\r\n";
			//$headers .= "Reply-To: " . $from . "\r\n";
			$headers .= "X-Mailer: PHP/" . phpversion();

			// boundary
			$semi_rand = md5(time());
			$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";

			// headers for attachment 
			$headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"";
			
			// multipart boundary 
			$mail_html = "This is a multi-part message in MIME format.\n\n" . "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"iso-8859-1\"\n" . "Content-Transfer-Encoding: 7bit\n\n" . $mail_html . "\n\n"; 
			// preparing attachments
			foreach ($files as $file) {
				$file_path = $file['file_path'];
				$file_name = $file['file_name'];
				$file_ptr = fopen($file_path, "rb");
				$data = fread($file_ptr, filesize($file_path));
				fclose($file_ptr);
				$data = chunk_split(base64_encode($data));
				
				$mail_html .= "--{$mime_boundary}\n";
				$mail_html .= "Content-Type: {\"application/octet-stream\"};\n" . " name=\"$file_name\"\n";
				$mail_html .= "Content-Disposition: attachment;\n" . " filename=\"$file_name\"\n";
				$mail_html .= "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
				$mail_html .= "--{$mime_boundary}\n";
			}

			if (count($files)) {
				$mail_html .= "--";
			}

			$ok = 0;
			if (!empty($to)) {
				$ok = @mail($to, $subject, $mail_html, $headers); 
			}
			if ($ok) {
				$status = 'Daily Report Mail Sent Successfully!!';
				$mail_sent = 1;
			}
			else {
				$status = 'Mail not sent. Please try again later!!';
				$mail_sent = 0;
			}
			
			$query = 'INSERT INTO ' . TBL_DAILY_REPORT . '(title, mail_sent, report_date, created_on) VALUES("' . $status . '", ' . $mail_sent . ', "' . $report_date . '", NOW())';
			$this->db->query($query);
			
			$report_date = date('Y-m-d', strtotime($report_date . '+1 day'));
			$this->send_daily_report($report_date);
		}
		else {
			//echo 'Daily report sent upto date.';
		}
	}
}
?>