<?php
define('BASEPATH', dirname(dirname(dirname(__FILE__))) . '\system');
define('FCPATH', dirname(dirname(dirname(__FILE__))));
require_once (dirname(dirname(dirname(__FILE__))) . '/application/config/config.php');
require_once (dirname(dirname(dirname(__FILE__))) . '/application/config/constants.php');
require_once (dirname(dirname(dirname(__FILE__))) . '/application/helpers/mpdf/mpdf.php');
require_once (dirname(__FILE__) . '/db.php');

class petty_cash {
	public function __construct() {
		$this->db = new db();
	}
	
	public function get_opening_balance($branch_id, $date) {
		$query = 'SELECT closing_balance FROM (' . TBL_PETTY_CASH . ') WHERE status = 1 AND branch_id = ' . 
		$branch_id . ' AND DATE(date) = DATE_SUB("' . $date . '", INTERVAL 1 DAY)';
		$resource = $this->db->query($query);
		$opening_balance = $this->db->fetch_object_row($resource)->closing_balance;
		return (!empty($opening_balance)) ? $opening_balance : 0;
	}
	
	public function get_unpaid_invoice_amount($branch_id, $date) {
		$query = 'SELECT SUM(total_amt) as total_amt FROM (`' . TBL_SO . '`) WHERE `status` = 1 AND `payment_status` = 2 AND `branch_id` = ' . $branch_id . ' AND DATE(created_on) = "' . $date . '"';
		$resource = $this->db->query($query);
		$unpaid_invoice_amount = $this->db->fetch_object_row($resource)->total_amt;
		return (!empty($unpaid_invoice_amount)) ? $unpaid_invoice_amount : 0;
	}
	
	public function get_prev_unpaid_invoice_amount($branch_id, $date) {
		$query = 'SELECT SUM(total_amt) as total_amt FROM (`' . TBL_SO . '`) WHERE `status` = 1 AND `payment_status` = 2 AND `branch_id` = ' . $branch_id . ' AND DATE(created_on) < "' . $date . '"';
		$resource = $this->db->query($query);
		$prev_unpaid_invoice_amount = $this->db->fetch_object_row($resource)->total_amt;
		return (!empty($prev_unpaid_invoice_amount)) ? $prev_unpaid_invoice_amount : 0;
	}
	
	public function get_prev_sales_amount($branch_id, $date) {
		$query = 'SELECT SUM(sp.pay_amt) as total_amt FROM (`' . TBL_SPM . '` as sp) JOIN `' . TBL_SO . '` as so ON `so`.`id` = `sp`.`so_id` AND DATE(so.created_on) < DATE(sp.created_on) WHERE `sp`.`status` = 1 AND `sp`.`payment_status` NOT IN (2, 3) AND `sp`.`brn_id` = ' . $branch_id . ' AND DATE(sp.created_on) = "' . $date . '"';
		$resource = $this->db->query($query);
		$prev_sales_amount = $this->db->fetch_object_row($resource)->total_amt;
		return (!empty($prev_sales_amount)) ? $prev_sales_amount : 0;
	}
	
	public function get_total_sales_amount($branch_id, $date) {
		$query = 'SELECT SUM(so.total_amt) as total_amt FROM (`' . TBL_SO . '` as so) WHERE `so`.`status` != 10 AND `so`.`branch_id` = ' . $branch_id . ' AND DATE(so.created_on) = "' . $date . '"';
		$resource = $this->db->query($query);
		$total_sales_amount = $this->db->fetch_object_row($resource)->total_amt;
		return (!empty($total_sales_amount)) ? $total_sales_amount : 0;
	}
	
	public function get_sales_payment($branch_id, $date) {
		// Get only cash sales payment
		$query = 'SELECT pt.id, pt.type_name, pt.group_name, (SELECT IFNULL(SUM(sp.pay_amt), 0) FROM ' . TBL_SPM . ' as sp JOIN ' . TBL_SO . ' as so ON so.id = sp.so_id AND DATE(so.created_on) = DATE(sp.created_on) WHERE sp.pay_pay_type = pt.id AND sp.status = 1 AND sp.payment_status NOT IN(2, 3) AND DATE(sp.created_on) = "' . $date . '" AND sp.brn_id = ' . $branch_id . ') as total_amt FROM (`' . TBL_PAYMENT_TYPE . '` as pt) WHERE `pt`.`group_name` != 3 AND `pt`.`status` = 1 GROUP BY `pt`.`id`';
		$resource = $this->db->query($query);
		return $this->db->fetch_object_array($resource);
	}
	
	public function get_prev_day_sales_payment($branch_id, $date) {
		$query = 'SELECT pt.id, pt.type_name, pt.group_name, (SELECT IFNULL(SUM(sp.pay_amt), 0) FROM ' . TBL_SPM . ' as sp JOIN ' . TBL_SO . ' as so ON so.id = sp.so_id AND DATE(so.created_on) < DATE(sp.created_on) WHERE sp.pay_pay_type = pt.id AND sp.status = 1 AND sp.payment_status NOT IN(2, 3) AND DATE(sp.created_on) = "' . $date . '" AND sp.brn_id = ' . $branch_id . ') as total_amt FROM (`' . TBL_PAYMENT_TYPE . '` as pt) WHERE `pt`.`group_name` != 3 AND `pt`.`status` = 1 GROUP BY `pt`.`id`';
		$resource = $this->db->query($query);
		return $this->db->fetch_object_array($resource);
	}
	
	public function get_expense_amount($branch_id, $date) {
		$query = 'SELECT IF (SUM(e.amount) IS NOT NULL, SUM(e.amount), 0) as total_amt FROM (`' . TBL_EXP .'` as e) WHERE `e`.`branch_id` = ' . $branch_id . ' AND `e`.`status` = 1 AND DATE(e.created_on) = "' . $date . '"';
		$resource = $this->db->query($query);
		$expense_amount = $this->db->fetch_object_row($resource)->total_amt;
		return (!empty($expense_amount)) ? $expense_amount : 0;
	}
	
	public function get_payment_amount($branch_id, $date) {
		$query = 'SELECT IF (SUM(p.pay_amount) IS NOT NULL, SUM(p.pay_amount), 0) as total_amt FROM (`' . TBL_PAYM . '` as p) JOIN `' . TBL_PAYMENT_MODE . '` as pm ON `pm`.`id` = `p`.`payment_type` AND pm.is_cash = 1 WHERE `p`.`status` = 1 AND `p`.`branch_id` = ' . $branch_id . ' AND DATE(p.created_on) = "' . $date . '"';
		$resource = $this->db->query($query);
		$payment_amount = $this->db->fetch_object_row($resource)->total_amt;
		return (!empty($payment_amount)) ? $payment_amount : 0;
	}
	
	public function get_deduction_amount($branch_id, $date) {
		$query = 'SELECT IF (SUM(d.ded_amount) IS NOT NULL, SUM(d.ded_amount), 0) as total_amt FROM (`' . TBL_DEDC . '` as d) JOIN `' . TBL_PAYMENT_MODE . '` as pm ON `pm`.`id` = `d`.`payment_type` AND pm.is_cash = 1 WHERE `d`.`status` = 1 AND `d`.`branch_id` = ' . $branch_id . ' AND DATE(d.created_on) = "' . $date . '"';
		$resource = $this->db->query($query);
		$deduction_amount = $this->db->fetch_object_row($resource)->total_amt;
		return (!empty($deduction_amount)) ? $deduction_amount : 0;
	}
	
	public function get_deposit_amount($branch_id, $date) {
		$query = 'SELECT IF (SUM(d.dep_amount) IS NOT NULL, SUM(d.dep_amount), 0) as total_amt FROM (`' . TBL_DEPS . '` as d) JOIN `' . TBL_PAYMENT_MODE . '` as pm ON `pm`.`id` = `d`.`payment_type` AND pm.is_cash = 1 WHERE `d`.`status` = 1 AND `d`.`branch_id` = ' . $branch_id . ' AND DATE(d.created_on) = "' . $date . '"';
		$resource = $this->db->query($query);
		$deposit_amount = $this->db->fetch_object_row($resource)->total_amt;
		return (!empty($deposit_amount)) ? $deposit_amount : 0;
	}
	
	public function get_purchase_amount($branch_id, $date) {
		$query = 'SELECT IF (SUM(p.pay_amt) IS NOT NULL, SUM(p.pay_amt), 0) as total_amt FROM (`' . TBL_PPM . '` as p) JOIN `' . TBL_PAYMENT_TYPE . '` as pt ON `pt`.`id` = `p`.`pay_pay_type` AND pt.group_name = 1 WHERE `p`.`status` = 1 AND `p`.`brn_id` = ' . $branch_id . ' AND DATE(p.created_on) = "' . $date . '"';
		$resource = $this->db->query($query);
		$purchase_amount = $this->db->fetch_object_row($resource)->total_amt;
		return (!empty($purchase_amount)) ? $purchase_amount : 0;
	}
	
	public function get_eo_payments($branch_id, $date) {
		$query = 'SELECT IF (SUM(p.pay_amt) IS NOT NULL, SUM(p.pay_amt), 0) as total_amt FROM (`' . TBL_EXRPM . '` as p) JOIN `' . TBL_PAYMENT_TYPE . '` as pt ON `pt`.`id` = `p`.`pay_pay_type` AND pt.group_name = 1 WHERE `p`.`status` = 1 AND `p`.`brn_id` = ' . $branch_id . ' AND DATE(p.created_on) = "' . $date . '"';
		$resource = $this->db->query($query);
		$eo_payments = $this->db->fetch_object_row($resource)->total_amt;
		return (!empty($eo_payments)) ? $eo_payments : 0;
	}
	
	public function get_external_service_payments($branch_id, $date) {
		$query = 'SELECT IF (SUM(p.pay_amt) IS NOT NULL, SUM(p.pay_amt), 0) as total_amt FROM (`' . TBL_EXTSERPAY . '` as p) JOIN `' . TBL_PAYMENT_TYPE . '` as pt ON `pt`.`id` = `p`.`pay_pay_type` AND pt.group_name = 1 WHERE `p`.`status` = 1 AND `p`.`brn_id` = ' . $branch_id . ' AND DATE(p.created_on) = "' . $date . '"';
		$resource = $this->db->query($query);
		$service_payments = $this->db->fetch_object_row($resource)->total_amt;
		return (!empty($service_payments)) ? $service_payments : 0;
	}
	
	public function get_refund_amount($branch_id, $date) {
		$query = 'SELECT IF (SUM(sr.refund_amount) IS NOT NULL, SUM(sr.refund_amount), 0) as total_amt FROM (`' . TBL_SO_RET . '` as sr) WHERE `sr`.`status` = 1 AND `sr`.`brn_id` = ' . $branch_id . ' AND DATE(sr.created_on) = "' . $date . '"';
		$resource = $this->db->query($query);
		$refund_amount = $this->db->fetch_object_row($resource)->total_amt;
		return (!empty($refund_amount)) ? $refund_amount : 0;
	}
	
	public function close_petty_cash() {
		$date = date('Y-m-d');
		
		$query = 'SELECT * FROM ' . TBL_USER . ' WHERE status = 1 AND is_admin = 1';
		$resource = $this->db->query($query);
		if ($this->db->num_rows($resource)) {
			$user = $this->db->fetch_object_row($resource);
			$user_id = $user->id;
		}
		else {
			$user_id = NULL;
		}
		
		$query = 'SELECT * FROM ' . TBL_BRN . ' WHERE status = 1';
		$resource = $this->db->query($query);
		$branches = $this->db->fetch_object_array($resource);
		
		foreach ($branches as $key => $branch) {
			$query = 'SELECT COUNT(*) as count FROM ' . TBL_PETTY_CASH . ' WHERE status = 1 AND branch_id = ' . $branch->id . ' AND date = "' . $date . '"';
			
			$resource = $this->db->query($query);
			if ($this->db->num_rows($resource)) {
				if ($this->db->fetch_object_row($resource)->count > 0) {
					continue;
				}
			}
			
			$opening_balance = $this->get_opening_balance($branch->id, $date);
			$unpaid_invoice_amount = $this->get_unpaid_invoice_amount($branch->id, $date);
			$prev_unpaid_invoice_amount = $this->get_prev_unpaid_invoice_amount($branch->id, $date);
			$prev_sales_amount = $this->get_prev_sales_amount($branch->id, $date);
			$total_sales_amount = $this->get_total_sales_amount($branch->id, $date);
			$sales_payment = $this->get_sales_payment($branch->id, $date);
			$prev_day_sales_payment = $this->get_prev_day_sales_payment($branch->id, $date);
			$expense_amount = $this->get_expense_amount($branch->id, $date);
			$payment_amount = $this->get_payment_amount($branch->id, $date);
			$deduction_amount = $this->get_deduction_amount($branch->id, $date);
			$deposit_amount = $this->get_deposit_amount($branch->id, $date);
			$purchase_amount = $this->get_purchase_amount($branch->id, $date);
			$eo_payments = $this->get_eo_payments($branch->id, $date);
			$external_service_payment = $this->get_external_service_payments($branch->id, $date);
			$refund_amount = $this->get_refund_amount($branch->id, $date);
			
			$bill_amount = $payment_amount + $deduction_amount;
			$total_purchase_amount = $purchase_amount + $eo_payments + $external_service_payment;
			
			$cash_sale_paymnet = 0;
			foreach($sales_payment as $key => $row) {
				if ($row->group_name == 1) 
					$cash_sale_paymnet += $row->total_amt;
			}
			
			$previous_day_cash_sale_paymnet = 0;
			foreach($prev_day_sales_payment as $key => $row) {
				if ($row->group_name == 1) 
					$previous_day_cash_sale_paymnet += $row->total_amt;
			}
			
			$total_in_amount = $opening_balance + $cash_sale_paymnet + $previous_day_cash_sale_paymnet;
			$total_out_amount = $total_purchase_amount + $bill_amount + $expense_amount + $refund_amount;
			$ledger_amt = $total_in_amount - $total_out_amount;
			$banking = 0;
			$closing_balance = $ledger_amt - $banking;
			
			$insert = array(
				'date' => $date,
				'bank_acc_id' => $branch->bank_acc_id,
				'opening_balance' => $opening_balance,
				'total_sales' => $total_sales_amount,
				'unpaid_sale_amt' => $unpaid_invoice_amount,
				'expense_amt' => $expense_amount,
				'bill_payment' => $bill_amount,
				'prev_day_collection' => $prev_sales_amount,
				'refund_amt' => $refund_amount,
				'purchase_amt' => $total_purchase_amount,
				'ledger_amt' => $ledger_amt,
				'banking' => $banking,
				'closing_balance' => $closing_balance,
				'remarks' => 'Petty Cash closed on end of day',
				'is_auto_close' => 1,
				'branch_id' => $branch->id,
				'created_by' => $user_id,
				'created_on' => date('Y-m-d H:i:s')
			);
			
			$inserted_id = $this->db->insert(TBL_PETTY_CASH, $insert);
			if (!empty($inserted_id)) {
				foreach($sales_payment as $key => $value) {
					$insert = array(
						'petty_cash_id' => $inserted_id,
						'type' => 1,
						'payment_type' => $value->type_name,
						'group_name' => $value->group_name,
						'amount' => $value->total_amt,
						'branch_id' => $branch->id,
						'created_by' => $user_id,
						'created_on' => date('Y-m-d H:i:s')
					);
					$this->db->insert(TBL_PETTY_CASH_ITEMS, $insert);
				}
				
				foreach($prev_day_sales_payment as $key => $value) {
					$insert = array(
						'petty_cash_id' => $inserted_id,
						'type' => 2,
						'payment_type' => $value->type_name,
						'group_name' => $value->group_name,
						'amount' => $value->total_amt,
						'branch_id' => $branch->id,
						'created_by' => $user_id,
						'created_on' => date('Y-m-d H:i:s')
					);
					
					$this->db->insert(TBL_PETTY_CASH_ITEMS, $insert);
				}
			}
		}
	}
}
?>