<?php
require_once (dirname(dirname(dirname(__FILE__))) . '/application/config/database.php');
class db {
	/**
	 * Method to connect mysqli database and select database
	 * @return void
	 */
	public function __construct() {
        global $db;
		$this->db_conn = mysqli_connect($db['default']['hostname'], $db['default']['username'], $db['default']['password']);
        if ($this->db_conn) {
            if (!mysqli_select_db($this->db_conn, $db['default']['database'])) {
                throw new Exception('Database (' . $db['default']['database'] . ') does not exists!');
            }
        }
		else {
			
			throw new Exception('Could not connect to database with the provided credentials! . ');
		}
		
		$this->query('SET SESSION sql_mode = ""');
	}

	/**
	 * Method to close mysqli connection
	 * @return void
	 */
	public function __destruct() {
		mysqli_close($this->db_conn);
	}
        
	public function get_columns($data) {
        return implode(',', array_keys($data));
    }
    
    public function get_column_values($data) {
        $values_str = '';
        $values = array_values($data);
        foreach($values as $key => $value) {
            $values_str .= ($key != 0) ? ', ' : '';
            if (is_string($value)) {
                $values_str .= '\'' . addcslashes(trim($value), "'") . '\'';
            }
            else if (is_numeric($value)) {
                $values_str .= trim($value);
            }
            else {
                $values_str .= '\'\'';
            }
        }
        
        return $values_str;
    }
    
    public function get_update_str($data) {
        $update_str = '';
        $i = 0;
        foreach($data as $key => $value) {
            $update_str .= ($i != 0) ? ', ' : '';
            $update_str .= trim($key) . ' = ';
            if (is_string($value)) {
                $update_str .= '\'' . addcslashes(trim($value), "'") . '\'';
            }
            else if (is_numeric($value)) {
                $update_str .= trim($value);
            }
            else {
                $update_str .= '\'\'';
            }
            
            $i ++;
        }
        
        return $update_str;
    }
    
    public function get_where_str($where) {
        if (!is_array($where)) 
            return $where;
        
        $where = array_filter($where);
        $where_str = '';
        $i = 0;
        foreach($where as $key => $value) {
            $where_str .= ($i != 0) ? ' AND ' : '';
            $where_str .= trim($key) . ' = ';
            if (is_string($value)) {
                $where_str .= '\'' . addcslashes(trim($value), "'") . '\'';
            }
            else if (is_numeric($value)) {
                $where_str .= $value;
            }
            
            $i ++;
        }
        
        return $where_str;
    }
    
	/**
	 * Method to insert row into table
	 * $param1 $query string mysqli query
	 * @return int Affected rows
	 */
	public function insert($tbl_name, $data) {
        if (empty($data))
            throw new Exception('Insert data should not be empty!');
        else if (empty($tbl_name))
            throw new Exception('Table name should not be empty!');
        
        $columns = $this->get_columns($data);
        $values = $this->get_column_values($data);
        $query = 'INSERT INTO ' . $tbl_name . ' (' . $columns . ') VALUES (' . $values . ')';
        $this->query($query);
        return $this->last_inserted_id();
    }
    
	/**
	 * Method to insert row into table
	 * $param1 $query string mysqli query
	 * @return int Affected rows
	 */
	public function update($tbl_name, $data, $where) {
		if (empty($data))
			throw new Exception('Update data should not be empty!');
		else if (empty($tbl_name))
			throw new Exception('Table name should not be empty!');
		
		$update_str = $this->get_update_str($data);
		$where_str = $this->get_where_str($where);
		$query = 'UPDATE ' . $tbl_name . ' SET ' . $update_str . ' WHERE ' . $where_str;
		$resource = $this->query($query);
		return $this->affected_rows($resource);
	}
	
	/**
	 * Method to delete row from table
	 * $param1 $query string mysqli query
	 * @return int Affected rows
	 */
	public function delete($query) {
		$resource = $this->query($query);
		return $this->affected_rows($resource);
	}
	
	/**
	 * Method to get last inserted id
	 * @return int last inserted primary key
	 */
	public function last_inserted_id() {
		return mysqli_insert_id($this->db_conn);
	}
	
	/**
	 * Method to run mysqli database query
	 * @return resource Query resource
	 */
	public function query($query) {
		$result = mysqli_query($this->db_conn, $query);
		
		if (!empty(mysqli_error($this->db_conn))) {
			throw new Exception('Invalid query: ' . mysqli_error($this->db_conn) . ' in "' . $query . '"');
		}
		
		return $result;
	}
	
	/**
	 * Method to count rows in a resource
	 * $param1 $resource resource mysqli resource
	 * @return int Resource rows count
	 */
	public function num_rows($resource) {
		return mysqli_num_rows($resource);
	}

	/**
	 * Method to get affected rows
	 * @return int Affected rows count
	 */
	public function affected_rows() {
		return mysqli_affected_rows($this->db_conn);
	}

	/**
	 * Method to get all rows from resource
	 * $param1 $resource resource mysqli resource
	 * @return array object Associative array object of resource
	 */
	public function fetch_object_array($resource) {
		$result = array();
		if ($this->num_rows($resource) > 0) {
			while($row_obj = mysqli_fetch_object($resource)) {
				array_push($result, $row_obj);
			}
		}
		
		return $result;
	}

	/**
	 * Method to get single row from resource
	 * $param1 $resource resource mysqli resource
	 * @return array Associative array of resource
	 */
	public function fetch_object_row($resource) {
		$result = new stdClass();
		if ($this->num_rows($resource) > 0) {
			$result = mysqli_fetch_object($resource);
		}
		
		return $result;
	}

	/**
	 * Method to get all rows from resource
	 * $param1 $resource resource mysqli resource
	 * @return array Associative array of resource
	 */
	public function fetch_assoc_array($resource) {
		$result = array();
		if ($this->num_rows($resource) > 0) {
			while($row = mysqli_fetch_assoc($resource)) {
				array_push($result, $row);
			}
		}

		return $result;
	}

	/**
	 * Method to get single row from resource
	 * $param1 $resource resource mysqli resource
	 * @return array
	 */
	public function fetch_assoc_row($resource) {
		$result = array();
		if ($this->num_rows($resource) > 0) {
			$result = mysqli_fetch_assoc($resource);
		}
		return $result;
	}
}
?>