<?php
error_reporting(0);
try {
	require_once (dirname(__FILE__) . '/classes/daily_report.php');
	$daily_report = new daily_report();
	$last_daily_report = $daily_report->get_last_daily_report();
	$daily_report->send_daily_report($last_daily_report);
	
	require_once (dirname(__FILE__) . '/classes/petty_cash.php');
	$petty_cash = new petty_cash();
	$petty_cash->close_petty_cash();
}
catch(Exception $e) {
	error_log($e->getMessage());
}
?>