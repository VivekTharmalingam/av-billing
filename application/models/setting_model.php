<?php
class Setting_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get_mysql_directory() {
        $query = 'SHOW VARIABLES LIKE "basedir"';
        return $this->db->query($query)->row();
    }
}
?>