<?php
/* 
 * Created By : Vivek T
 * Purpose : Promotion Module - Buy 1 Get 1 Model
 * Created On : 2k17-02-18 05:35:00
 */
class Buy_one_get_one_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->product = TBL_PDT;
        $this->category = TBL_CAT;
        $this->bogo = TBL_BOGO;
        $this->bogo_items = TBL_BOGO_ITEMS;
    }
    
    public function list_all() {
        $this->db->select('b1g1.*,p.pdt_name, CASE b1g1.status WHEN 1 THEN "Active" WHEN 2 THEN "In Active" ELSE "" END as status_str', FALSE);
        $this->db->join($this->product . ' as p', 'p.id = b1g1.item_id');
        $this->db->where('b1g1.status !=', 10);
        $this->db->order_by('b1g1.id', 'desc');
        return $this->db->get($this->bogo . ' as b1g1')->result();
    }
    
    public function insert($data) {
        $this->db->insert($this->bogo, $data);
        return $this->db->insert_id();
    }
    
    public function insert_item($data) {
        $this->db->insert($this->bogo_items, $data);
        return $this->db->insert_id();
    }
    
    public function get_by_id($id) {
        $this->db->select('b1g1.*,p.pdt_name, CASE b1g1.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->join($this->product . ' as p', 'p.id = b1g1.item_id');
        $this->db->where('b1g1.id', $id);
        return $this->db->get($this->bogo . ' as b1g1')->row();
    }
    
    public function get_by_items($id) {
        $this->db->select('b1g1_items.*,p.pdt_name, CASE b1g1_items.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->join($this->product . ' as p', 'p.id = b1g1_items.offer_item_id');
        $this->db->where('b1g1_items.bogo_id', $id);
        $this->db->where('b1g1_items.status != 10');
        return $this->db->get($this->bogo_items . ' as b1g1_items')->result();
    }
    
    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update($this->bogo, $data);
        return $this->db->affected_rows();
    }
    
    public function update_item($id, $data){
        $this->db->where('id', $id);
        $this->db->update($this->bogo_items, $data);
        return $this->db->affected_rows();
    }
    
    public function delete_item($id, $data){
        $this->db->where('bogo_id', $id);
        $this->db->update($this->bogo_items, $data);
        return $this->db->affected_rows();
    }
    
    
    
}