<?php

class Quotation_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->quotation = TBL_QUO;
        $this->quotation_email_sent = TBL_QUO_EMAIL;
        $this->quotation_item = TBL_QUO_ITM;
        $this->items = TBL_PDT;
        $this->category = TBL_CAT;
        $this->ship_address = TBL_SHIP_ADR;
        $this->company = TBL_COMP;
        $this->customer = TBL_CUS;
        //$this->inventory_product = TBL_INV_PDT;
        $this->branch_products = TBL_BRN_PDT;
        $this->branches = TBL_BRN;
        $this->sales_payment = TBL_SPM;
        $this->payment_type = TBL_PAYMENT_TYPE;
        $this->user = TBL_USER;
        $this->user_data = get_user_data();
    }

    public function list_all() {
        $user_data = get_user_data();
        $this->db->select('quo.id, quo.quo_no, quo.so_date, quo.total_amt, quo.status, quo.your_reference, CASE quo.status WHEN 1 THEN "New" WHEN 2 THEN "Partially Delivered" WHEN 3 THEN "Delivered" ELSE "" END as status_str, CASE quo.quo_type WHEN 1 THEN c.name ELSE quo.customer_name END as name', FALSE);
        $this->db->join($this->customer . ' as c', 'c.id = quo.customer_id', 'left');
        $this->db->where('quo.status !=', 10);
//        if($user_data['is_admin']!='1'){
//            $this->db->where_in('so.branch_id', $user_data['branch_ids']);
//        }
        $this->db->where_in('quo.branch_id', $this->user_data['branch_id']);
        $this->db->order_by('quo.id', 'desc');
        return $this->db->get($this->quotation . ' as quo')->result();
    }

    public function get_by_id($id) {
        $this->db->select('quo.id, quo.quo_no, quo.quo_type, CASE quo.quo_type WHEN 1 THEN c.name ELSE quo.customer_name END as name, DATE_FORMAT(quo.so_date, "%d-%m-%Y") as so_date, DATE_FORMAT(quo.do_date, "%d-%m-%Y") as do_date, quo.leave_date, quo.sub_total, quo.gst, quo.gst_amount, quo.gst_percentage, quo.total_amt, quo.status, quo.payment_status, CASE quo.payment_status WHEN 1 THEN "Paid" WHEN 3 THEN "Cancelled" ELSE "UnPaid" END as payment_status_str, CASE quo.status WHEN 1 THEN "New" WHEN 2 THEN "Partially Delivered" WHEN 3 THEN "Delivered" ELSE "" END as status_str, quo.payment_terms, quo.shipping_address, quo.discount, quo.discount_type, quo.discount_amount, quo.your_reference, quo.remarks, quo.branch_id, quo.customer_id, c.contact_name, c.phone_no, c.email, CASE quo.quo_type WHEN 1 THEN c.address ELSE "CASH CUSTOMER" END as address, u.name AS uname, pt.type_name as payment_type, quo.inv_created', FALSE);
        $this->db->join($this->customer . ' as c', 'c.id = quo.customer_id', 'left');
        $this->db->join($this->user . ' as u', 'u.id = quo.created_by', 'left');
        $this->db->join($this->sales_payment . ' as sp', 'sp.so_id = quo.id', 'left');
        $this->db->join($this->payment_type . ' as pt', 'pt.id = sp.pay_pay_type', 'left');
        $this->db->where('quo.id', $id);
        return $this->db->get($this->quotation . ' as quo')->row();
    }

    public function get_current_address($id) {
        $this->db->where('id', $id);
        return $this->db->get($this->branches)->row();
    }

    public function get_products_by_so($so_id) {
        $this->db->select('quoi.id, quoi.price,quoi.product_description,quoi.unit_cost, quoi.quantity, quoi.discount, quoi.discount_type, quoi.discount_amount, quoi.total, quoi.item_id, p.pdt_code, p.pdt_name, p.pdt_description, quoi.category_id, c.cat_code, c.cat_name');
        $this->db->join($this->items . ' as p', 'p.id = quoi.item_id', 'left');
        $this->db->join($this->category . ' as c', 'c.id = quoi.category_id', 'left');
        $this->db->where('quoi.quo_id', $so_id);
        $this->db->where('quoi.status !=', 10);
        return $this->db->get($this->quotation_item . ' as quoi')->result();
    }

    public function shipping_address_by_id($address_id) {
        $this->db->where('id', $address_id);
        return $this->db->get($this->ship_address)->row();
    }

    public function insert($data) {
        $this->db->insert($this->quotation, $data);
        return $this->db->insert_id();
    }

    public function insert_quotation_mail($data) {
        $this->db->insert($this->quotation_email_sent, $data);
        return $this->db->insert_id();
    }

    public function insert_item($data) {
        $this->db->insert($this->quotation_item, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->quotation, $data);
        return $this->db->affected_rows();
    }

    public function get_so_item($so_item_id) {
        $this->db->where('id', $so_item_id);
        $this->db->where('status !=', 10);
        return $this->db->get($this->quotation_item)->row();
    }

    public function update_item($data, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->quotation_item, $data);
        return $this->db->affected_rows();
    }

    public function delete_so_item($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->quotation_item);
        return $this->db->affected_rows();
    }

    public function get_removed_so_items($so_id, $product_ids = array()) {
        $this->db->where('quo_id', $so_id);
        if (!empty($product_ids))
            $this->db->where_not_in('item_id', $product_ids);

        return $this->db->get($this->quotation_item)->result();
    }

}

?>