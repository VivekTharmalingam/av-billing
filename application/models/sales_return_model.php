<?php
class Sales_Return_Model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->suppliers = TBL_SUP;
        $this->sales_order = TBL_SO;
        $this->branches = TBL_BRN;
        $this->sales_order_item = TBL_SO_ITM;
        $this->sales_return_item = TBL_SO_RET_ITM;
        $this->sales_return = TBL_SO_RET;
        $this->items = TBL_PDT;
        $this->category = TBL_CAT;
        $this->ship_address = TBL_SHIP_ADR;
        $this->company = TBL_COMP;
        $this->customer = TBL_CUS;
        //$this->inventory_product = TBL_INV_PDT;
        $this->branch_products = TBL_BRN_PDT;
        $this->warranty_status_log = TBL_WRSTLG;
        
        $this->user_data = get_user_data();
    }

    public function list_all() {
        $user_data = get_user_data();
        $this->db->select('sor.id, sor.return_date, sor.so_id, so.so_no, so.so_date, brn.branch_name, SUM(sori.returned_qty) as return_qty');
        $this->db->join($this->sales_return_item . ' as sori', 'sori.so_ret_id = sor.id AND sori.status != 10', 'left');
        $this->db->join($this->sales_order . ' as so', 'so.id = sor.so_id', 'left');
        $this->db->join($this->branches . ' as brn', 'brn.id = sor.branch_id', 'left');
        $this->db->where('sor.status !=', 10);
        $this->db->order_by('sor.id', 'desc');
        $this->db->group_by('sor.id');
//        if ($user_data['is_admin'] != '1') {
//            $this->db->where_in('sor.branch_id', $user_data['branch_ids']);
//        }
        $this->db->where_in('sor.branch_id', $this->user_data['branch_id']);
        return $this->db->get($this->sales_return . ' as sor')->result();
    }

    public function get_so_list($id = '') {
        $this->db->_protect_identifiers = FALSE;
        $returned = ', (SELECT CASE WHEN ISNULL(sri.returned_qty) THEN "" ELSE SUM(sri.returned_qty) END as return_quantity FROM ' . $this->sales_return_item . ' as sri JOIN ' . $this->sales_return . ' as sr ON sr.id =  sri.so_ret_id AND sri.status != 10 AND sri.status != 10 WHERE sr.so_id=so.id';
        
        if (!empty($id)) {
            $returned .= ' AND sr.so_id != ' . $id . ') as returned_qty';
        }else{
            $returned .= ') as returned_qty';
        }
        //$returned = '';
        $this->db->select('so.id, so.so_no,cus.name,cus.address,cus.phone_no,cus.email,so.status,sum(soi.quantity) AS sold_qty' . $returned, FALSE);
        $this->db->join($this->customer . ' as cus', 'cus.id = so.customer_id AND cus.status != 10', 'left');
        $this->db->join($this->sales_order_item . ' as soi', 'soi.so_id = so.id AND soi.status != 10', 'left');
        $this->db->where('so.status !=', 10);
        $this->db->order_by('so.id', 'desc');
        $this->db->group_by('so.id', 'desc');
        $this->db->where_in('so.branch_id', $this->user_data['branch_id']);
        $this->db->having('sold_qty > returned_qty OR returned_qty IS NULL', FALSE);
        return $this->db->get($this->sales_order . ' as so')->result();
    }

    /*public function get_so_by_id($id) {
        $this->db->where('so.id', $id);
        return $this->db->get($this->sales_order . ' as so')->row();
    }*/

    public function get_category_by_so($so) {
        $this->db->select('soi.*,cat.id As cat_id,cat.cat_code,cat.cat_name');
        $this->db->join($this->category . ' as  cat', 'cat.id = soi.category_id', 'left');
        $this->db->where('soi.so_id', $so);
        $this->db->where('soi.status !=', 10);
        $this->db->group_by('soi.so_id');
        return $this->db->get($this->sales_order_item . ' as soi')->result();
    }
    
    public function get_sales_order_item_by_id($so_itm_id) {
        $this->db->select('soi.*');
        $this->db->where('soi.id', $so_itm_id);
        return $this->db->get($this->sales_order_item . ' as soi')->result_array();
    }

    public function get_product_by_category($sid, $po_id) {
        $already_returned = ', (SELECT CASE WHEN ISNULL(sri.returned_qty) THEN "" ELSE SUM(sri.returned_qty) END as return_quantity FROM ' . $this->sales_return_item . ' as sri JOIN ' . $this->sales_return . ' as sr ON sr.id =  sri.so_ret_id AND sri.status != 10 AND sri.status != 10 WHERE  sr.so_id=so.id AND sri.item_id = mat.id) as already_returned_qty';
        $this->db->select('soi.item_id, soi.category_id, mat.pdt_code, mat.pdt_name, soi.product_description, soi.quantity as so_quantity, soi.price' . $already_returned, FALSE);
        $this->db->join($this->sales_order_item . ' as soi', 'soi.so_id = so.id AND soi.status != 10');
        $this->db->join($this->items . ' as  mat', 'mat.id = soi.item_id');
        $this->db->where('soi.so_id', $sid);
        if (!empty($po_id)) {
            $this->db->where('soi.category_id', $po_id);
        }

        $this->db->where('so.status !=', 10);
        //$this->db->group_by('soi.item_id');
        return $this->db->get($this->sales_order . ' as so')->result();
    }
    
    public function get_product_by_category_all($sid, $po_id) {
        $already_returned = ', (SELECT CASE WHEN ISNULL(sri.returned_qty) THEN "" ELSE SUM(sri.returned_qty) END as return_quantity FROM ' . $this->sales_return_item . ' as sri JOIN ' . $this->sales_return . ' as sr ON sr.id =  sri.so_ret_id AND sri.status != 10 AND sri.status != 10 WHERE  sr.so_id=so.id AND sri.item_id = soi.item_id) as already_returned_qty';
        $this->db->select('soi.id as so_item_autoid, soi.item_id, soi.serial_no, soi.category_id, soi.so_item_codetxt as pdt_code, soi.product_description as pdt_name, soi.product_description, soi.quantity as so_quantity, soi.price' . $already_returned, FALSE);
        $this->db->join($this->sales_order_item . ' as soi', 'soi.so_id = so.id AND soi.status != 10 and soi.service_form_type != 3');
        $this->db->where('soi.so_id', $sid);
        if (!empty($po_id)) {
            $this->db->where('soi.category_id', $po_id);
        }

        $this->db->where('so.status !=', 10);
        //$this->db->group_by('soi.item_id');
        return $this->db->get($this->sales_order . ' as so')->result();
    }

    public function get_product_soid($sid, $po_id) {
        $this->db->select('soi.item_id,soi.category_id, mat.pdt_code, mat.pdt_name, SUM(soi.quantity) as so_quantity' . $already_returned, FALSE);
        $this->db->join($this->sales_order_item . ' as soi', 'soi.so_id = so.id AND soi.status != 10', 'left');
        $this->db->join($this->items . ' as  mat', 'mat.id = soi.item_id', 'left');
        $this->db->where('soi.so_id', $sid);
        if (!empty($po_id)) {
            $this->db->where('soi.category_id', $po_id);
        }

        $this->db->where('so.status !=', 10);
        //$this->db->group_by('soi.item_id');
        return $this->db->get($this->sales_order . ' as so')->result();
    }
    
    public function get_product_soiditem_sum($sid) {
        $this->db->select('SUM(soi.total) as so_total', FALSE);
        $this->db->where('soi.so_id', $sid);
        $this->db->where('soi.show_in_print', 1);
        //$this->db->where('soi.service_form_type !=', 3);
        $this->db->where('soi.status !=', 10);
        return $this->db->get($this->sales_order_item . ' as soi')->result_array();
    }

    public function insert($data) {
        $this->db->insert($this->sales_return, $data);
        return $this->db->insert_id();
    }

    public function insert_item($data) {
        $this->db->insert($this->sales_return_item, $data);
        return $this->db->insert_id();
    }

    public function get_warehouse_by_po($id) {
        $this->db->select('po.*, ware.*,ware.id as wareid');
        $this->db->join($this->warehouses . ' as  ware', 'ware.id = po.ware_id');
        $this->db->where('po.id', $id);
        $this->db->where('ware.status !=', 10);
        return $this->db->get($this->purchases . ' as po')->row();
    }

    public function get_by_id($id) {
        $this->db->select('sr.*,so.customer_id, so.customer_name as cash_cust,brn.branch_name, cus.name,cus.contact_name,cus.address,cus.phone_no,cus.email, CASE sr.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" END as status_str, so.so_no', FALSE);
        $this->db->join($this->sales_order . ' as so', 'so.id = sr.so_id', 'left');
        $this->db->join($this->sales_return_item . ' as sri', 'sri.so_ret_id = sr.id AND sri.status != 10', 'left');
        $this->db->join($this->branches . ' as brn', 'brn.id = sr.branch_id', 'left');
        $this->db->join($this->customer . ' as cus', 'cus.id = so.customer_id', 'left');
        $this->db->where('sr.id', $id);
        $this->db->order_by('sr.id', 'desc');
        return $this->db->get($this->sales_return . ' as sr')->row();
    }

    public function get_items_by_por($id) {
		$this->db->select('sri.*, mat.pdt_code, mat.pdt_name, SUM(so_quantity)', FALSE);
        $already_returned = '(SELECT CASE WHEN ISNULL(sri.returned_qty) THEN "" ELSE SUM(sri.returned_qty) END as return_quantity FROM ' . $this->sales_return_item . ' as sri JOIN ' . $this->sales_return . ' as sr ON sr.id =  sri.so_ret_id  WHERE sr.id != sri.id AND  sri.so_ret_id = ' . $id . ' AND mat.id = sri.item_id AND sri.status != 10) as already_returned_qty';
        $this->db->select($already_returned, FALSE);
        $this->db->select('CASE sri.return_item_type WHEN 1 THEN "Return" WHEN 2 THEN "Re-Stock" WHEN 3 THEN "Warranty" END as return_item_type_str', FALSE);
		
        //$this->db->select('SUM(sri.returned_qty) AS returned_qty where ', FALSE);        
        $this->db->join($this->items . ' as  mat', 'mat.id = sri.item_id', 'left');
        $this->db->join($this->sales_return . ' as  sr', 'sr.id = sri.so_ret_id', 'left');
        $this->db->where('sri.so_ret_id', $id);
        $this->db->where('sri.status !=', 10);
        $this->db->where('sr.status !=', 10);
        $this->db->group_by('sri.item_id');
        return $this->db->get($this->sales_return_item . ' as sri')->result();
    }
    
    public function get_items_by_sor_all($id) {
		$this->db->select('sri.*, soi.so_item_codetxt as pdt_code, mat.pdt_name, SUM(so_quantity)', FALSE);
        $already_returned = '(SELECT CASE WHEN ISNULL(sri.returned_qty) THEN "" ELSE SUM(sri.returned_qty) END as return_quantity FROM ' . $this->sales_return_item . ' as sri JOIN ' . $this->sales_return . ' as sr ON sr.id =  sri.so_ret_id  WHERE sr.id != sri.id AND  sri.so_ret_id = ' . $id . ' AND mat.id = sri.item_id AND sri.status != 10) as already_returned_qty';
        $this->db->select($already_returned, FALSE);
        $this->db->select('CASE sri.return_item_type WHEN 1 THEN "Return" WHEN 2 THEN "Re-Stock" WHEN 3 THEN "Warranty" END as return_item_type_str', FALSE);
		
        //$this->db->select('SUM(sri.returned_qty) AS returned_qty where ', FALSE);        
        $this->db->join($this->items . ' as  mat', 'mat.id = sri.item_id', 'left');
        $this->db->join($this->sales_order_item . ' as  soi', 'soi.id = sri.so_item_autoid', 'left');
        $this->db->join($this->sales_return . ' as  sr', 'sr.id = sri.so_ret_id', 'left');
        $this->db->where('sri.so_ret_id', $id);
        $this->db->where('sri.status !=', 10);
        $this->db->where('sr.status !=', 10);
        $this->db->group_by('sri.item_id');
        return $this->db->get($this->sales_return_item . ' as sri')->result();
    }
    
    public function get_srnitems_allby_soid($id) {
		$this->db->select('sri.*, soi.so_item_codetxt as pdt_code, mat.pdt_name, SUM(so_quantity)', FALSE);
        $already_returned = '(SELECT CASE WHEN ISNULL(sri.returned_qty) THEN "" ELSE SUM(sri.returned_qty) END as return_quantity FROM ' . $this->sales_return_item . ' as sri JOIN ' . $this->sales_return . ' as sr ON sr.id =  sri.so_ret_id  WHERE sr.id != sri.id AND  sri.so_ret_id = ' . $id . ' AND mat.id = sri.item_id AND sri.status != 10) as already_returned_qty';
        $this->db->select($already_returned, FALSE);
        $this->db->select('CASE sri.return_item_type WHEN 1 THEN "Return" WHEN 2 THEN "Re-Stock" WHEN 3 THEN "Warranty" END as return_item_type_str', FALSE);
		
        $this->db->select("DATE_FORMAT(sr.return_date, '%d/%m/%Y') as return_date_str", FALSE);        
        $this->db->join($this->items . ' as  mat', 'mat.id = sri.item_id', 'left');
        $this->db->join($this->sales_order_item . ' as  soi', 'soi.id = sri.so_item_autoid', 'left');
        $this->db->join($this->sales_return . ' as  sr', 'sr.id = sri.so_ret_id');
        $this->db->where('sr.so_id', $id);
        $this->db->where('sri.status !=', 10);
        $this->db->where('sr.status !=', 10);
        $this->db->group_by('sri.item_id');
        return $this->db->get($this->sales_return_item . ' as sri')->result();
    }
    
    public function get_warranty_return_items() {
        $this->db->select('sri.*, soi.so_item_codetxt as pdt_code, mat.pdt_name, SUM(so_quantity)', FALSE);
        $this->db->select('CASE sri.return_item_type WHEN 1 THEN "Return" WHEN 2 THEN "Re-Stock" WHEN 3 THEN "Warranty" END as return_item_type_str', FALSE);    
        $this->db->join($this->items . ' as  mat', 'mat.id = sri.item_id', 'left');
        $this->db->join($this->sales_order_item . ' as  soi', 'soi.id = sri.so_item_autoid', 'left');
        $this->db->join($this->sales_return . ' as  sr', 'sr.id = sri.so_ret_id', 'left');
        $this->db->where('sri.return_item_type', 3);
        $this->db->where('sri.status !=', 10);
        $this->db->where('sr.status !=', 10);
        $this->db->group_by('sri.item_id');
        return $this->db->get($this->sales_return_item . ' as sri')->result();
    }
    
    public function get_warranty_return_item_byid($id) {
        $this->db->select('sri.*, sup.name as sup_name_txt, so.so_no AS inv, sr.return_date, DATE_FORMAT(sri.warranty_supplier_date, "%d/%m/%Y") as warranty_supplier_date_str, DATE_FORMAT(sr.return_date, "%d/%m/%Y") as ret_date, DATE_FORMAT(so.so_date, "%d/%m/%Y") as so_date_txt, soi.so_item_codetxt as pdt_code, soi.serial_no, sri.pdt_desc as pdt_desc, mat.pdt_name, sri.returned_qty as return_qty, CASE sri.warranty_cleared WHEN 2 THEN "Cleared" WHEN 1 THEN "Processing" WHEN 0 THEN "Not Cleared" ELSE "" END as warranty_cleared_str', FALSE);
        $this->db->select('CASE sri.return_item_type WHEN 1 THEN "Return" WHEN 2 THEN "Re-Stock" WHEN 3 THEN "Warranty" END as return_item_type_str', FALSE);    
        $this->db->join($this->items . ' as  mat', 'mat.id = sri.item_id', 'left');
        $this->db->join($this->sales_order_item . ' as  soi', 'soi.id = sri.so_item_autoid', 'left');
        $this->db->join($this->sales_return . ' as  sr', 'sr.id = sri.so_ret_id', 'left');
        $this->db->join($this->sales_order . ' as  so', 'so.id = sr.so_id', 'left');
        $this->db->join($this->suppliers . ' as sup', 'sup.id = sri.warranty_supplier_id', 'left');
        $this->db->where('sri.id', $id);
        $this->db->where('sri.return_item_type', 3);
        $this->db->where('sri.status !=', 10);
        $this->db->where('sr.status !=', 10);
        return $this->db->get($this->sales_return_item . ' as sri')->result_array();
    }

    public function get_old_mat_quantity($cid, $mat_id, $id) {
        $this->db->select('SUM(sri.returned_qty) as quantity');
        $this->db->where('sri.category_id', $mat_id);
        $this->db->where('sri.item_id', $mat_id);
        $this->db->where('sri.id', $id);
        $this->db->where('sri.status !=', 10);
        return $this->db->get($this->sales_return_item . ' as sri')->row()->quantity;
    }

    public function branch_materials($brn, $prt) {
        $this->db->select('bp.available_qty', FALSE);
        $this->db->where('bp.brn_id', $brn);
        $this->db->where('bp.product_id', $prt);
        $this->db->where('bp.status', 1);
        return $this->db->get($this->branch_products . ' as bp');
    }

    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->sales_return, $data);
        return $this->db->affected_rows();
    }

    public function update_item($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->sales_return_item, $data);
        return $this->db->affected_rows();
    }

    public function update_item_by_so($id, $data) {
        $this->db->where('so_ret_id', $id);
        $this->db->update($this->sales_return_item, $data);
        return $this->db->affected_rows();
    }

    public function row_delete_item($id, $item_id) {
        $data = array('status' => 10);
        $this->db->where('id', $item_id);
        $this->db->update($this->sales_return_item, $data);
    }

    public function get_item_qty_by_mat_id($mat_id, $ware_id) {
        $this->db->select('available_qty');
        $this->db->where('brn_id', $ware_id);
        $this->db->where('product_id', $mat_id);
        $this->db->where('status !=', 10);
        return $this->db->get($this->branch_products)->row();
    }

    public function check_exists_po_return($po_id) {
        $this->db->where('po_id', $po_id);
        $this->db->where('status !=', 10);
        $getData = $this->db->get($this->purchase_invoice);
        if ($getData->num_rows() > 0)
            return 1;
        else
            return 0;
    }
    
    public function insert_status_log($data) {
        $this->db->insert($this->warranty_status_log, $data);
        return $this->db->insert_id();
    }
    
    public function delete_status_log($item_id) {
        $data = array('status' => 10);
        $this->db->where('warranty_id', $item_id);
        $this->db->update($this->warranty_status_log, $data);
    }
    
    public function warranty_status_log_details($war_id) {
        $this->db->select('wlog.id,wlog.log_description,wlog.warranty_status_txt,wlog.created_on as created_date', FALSE);
        $this->db->select('DATE_FORMAT(wlog.created_on, "%d-%m-%Y %r") as created_date_str', FALSE);
        $this->db->where('wlog.warranty_id', $war_id);
        $this->db->where('wlog.status', 1);
        return $this->db->get($this->warranty_status_log . ' as wlog')->result();
    }

    /*public function update_warehouse_material_qty($ware_id, $material_id, $quantity, $op = '+') {
        $update = array(
            'comp_id' => $this->user_data['company_id'],
            'updated_by' => $this->user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s'),
            'ip_address' => $this->ip_address
        );
        $this->db->set('available_qty', 'available_qty ' . $op . $quantity, FALSE);
        $this->db->where('ware_id', $ware_id);
        $this->db->where('mat_id', $material_id);
        if ($op == '-') {
            $this->db->where('available_qty >= ', $quantity);
        }
        $this->db->update($this->warehouse_material, $update);
        return $this->db->affected_rows();
    }

    public function warehouse_materials($ware_id, $mat) {
        $this->db->select('wm.available_qty', FALSE);
        $this->db->where('wm.ware_id', $ware_id);
        $this->db->where('wm.mat_id', $mat);
        $this->db->where('wm.status', 1);
        return $this->db->get($this->warehouse_material . ' as wm');
    }*/

    public function shipping_address_by_id($address_id) {
        $this->db->where('id', $address_id);
        return $this->db->get($this->ship_address)->row();
    }
    
    
    public function warranty_report($from_date, $to_date, $warranty_cleared) {
        $this->db->select('sri.*, so.so_no AS inv, DATE_FORMAT(sr.return_date, "%d/%m/%Y") as ret_date, soi.so_item_codetxt as pdt_code, sri.pdt_desc as pdt_desc, mat.pdt_name, sri.returned_qty as return_qty, CASE sri.warranty_cleared WHEN 2 THEN "Cleared" WHEN 1 THEN "Processing" WHEN 0 THEN "Not Cleared" ELSE "" END as warranty_cleared_str', FALSE);
        $this->db->select('CASE sri.return_item_type WHEN 1 THEN "Return" WHEN 2 THEN "Re-Stock" WHEN 3 THEN "Warranty" END as return_item_type_str', FALSE);    
        $this->db->join($this->items . ' as  mat', 'mat.id = sri.item_id', 'left');
        $this->db->join($this->sales_order_item . ' as  soi', 'soi.id = sri.so_item_autoid', 'left');
        $this->db->join($this->sales_return . ' as  sr', 'sr.id = sri.so_ret_id', 'left');
        $this->db->join($this->sales_order . ' as  so', 'so.id = sr.so_id', 'left');
        if($warranty_cleared!="") {
            $this->db->where('sri.warranty_cleared', $warranty_cleared);
        }
        if (!empty($from_date)) {
            $this->db->where('DATE(sr.return_date) >=', date('Y-m-d', strtotime($from_date)));
        }
        if (!empty($to_date)) {
            $this->db->where('DATE(sr.return_date) <=', date('Y-m-d', strtotime($to_date)));
        }
        $this->db->where('sri.return_item_type', 3);
        $this->db->where('sri.status !=', 10);
        $this->db->where('sr.status !=', 10);
        return $this->db->get($this->sales_return_item . ' as sri')->result();
    }
    
}
?>