<?php
class Service_Status_Model extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->service_status = TBL_SERSTS;
    }    

    public function get_by_id($id) {
        $this->db->select('*', FALSE);
        $this->db->where('id', $id);
        return $this->db->get($this->service_status)->row();
    }
    public function insert($data) {
        $this->db->insert($this->service_status, $data);
        return $this->db->insert_id();
    }
    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update($this->service_status, $data);
        return $this->db->affected_rows();
    }
    public function check_service_status_exists($service_status,$id){
        $this->db->where('status_name',$service_status);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        if(!empty($id))
           $this->db->where('id !=',$id);
        $getData = $this->db->get($this->service_status);
		
        if($getData->num_rows() > 0)
            return 1;
        else
            return 0;
    }
}
?>