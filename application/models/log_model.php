<?php
class Log_Model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->act_log = TBL_ACT_LOG;
        $this->users       = TBL_USER;
    }
	
    public function list_search_all($from_date = '', $to_date = '',$branch_id = '', $user_id = '') {
        $this->db->select('log.*, DATE_FORMAT(log.created_on, "%d-%m-%Y %h:%i %p") as created_on_str, u.name', FALSE);
        if ((!empty($from_date))) {
            $this->db->where('DATE(log.created_on) >=', date('Y-m-d', strtotime($from_date)));
        }
        if ((!empty($to_date))) {
            $this->db->where('DATE(log.created_on) <=', date('Y-m-d', strtotime($to_date)));
        }
		
		if ((!empty($branch_id))) {
            $this->db->where('log.branch_id', $branch_id);
        }
		
		if ((!empty($user_id))) {
            $this->db->where('log.created_by', $user_id);
        }
		
        $this->db->join($this->users . ' as u', 'u.id = log.created_by', 'left');
		$this->db->where_in('log.branch_id', $this->user_data['branch_ids']);
		$this->db->order_by('log.created_on', 'desc');
        return $this->db->get($this->act_log.' AS log')->result();
    }
}
?>