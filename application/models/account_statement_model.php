<?php
class Account_Statement_Model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->sales_order = TBL_SO;
        $this->sales_order_item = TBL_SO_ITM;
        $this->customer = TBL_CUS;
        $this->branches = TBL_BRN;
        $this->sales_payment = TBL_SPM;
        $this->user = TBL_USER;
        $this->user_data = get_user_data();
    }
	
    public function statement_summary($from_date = '', $to_date = '', $cus = '', $pay = '') {
        $this->db->select('so.id, so.so_no, so.your_reference, so.sub_total, so.gst_amount, so.discount_amount,so.total_amt,so.payment_status, CASE so.so_type WHEN 1 THEN cus.name ELSE so.customer_name END as name,brn.branch_name, CASE so.status WHEN 3 THEN "Delivered" WHEN 2 THEN "Partial Delivered"  ELSE "Not Delivered" END as del_sts,CASE so.payment_status WHEN 1 THEN "Paid" WHEN 3 THEN "Cancelled" ELSE "UnPaid" END as pay_sts, u.name as issued_by, SUM(p.pay_amt) as pay_amt', FALSE);
        $this->db->select("DATE_FORMAT(so.so_date, '%d/%m/%Y') as so_date_str", FALSE);
		$pay_condition = 'p.so_id = so.id AND p.payment_status = 1';
        if (!empty($from_date)) {
            $this->db->where('DATE(so.so_date) >=', date('Y-m-d', strtotime($from_date)));
            $pay_condition .= ' AND DATE(p.pay_date) >= "' . date('Y-m-d', strtotime($from_date)) . '"';
        }
		
        if (!empty($to_date)) {
			$this->db->where('DATE(so.so_date) <=', date('Y-m-d', strtotime($to_date)));
			$pay_condition .= ' AND DATE(p.pay_date) <= "' . date('Y-m-d', strtotime($to_date)) . '"';
        }
		
        if (!empty($cus)) {
            $this->db->where('so.customer_id', $cus);
        }
		
        if (!empty($pay)) {
            $this->db->where('so.payment_status', $pay);
        }
		
        $this->db->where_in('so.branch_id', $this->user_data['branch_ids']);
        $this->db->where('so.status !=', 10);
        $this->db->order_by('so.id', 'desc');
        $this->db->group_by('so.id');
        $this->db->join($this->customer . ' as cus', 'cus.id = so.customer_id', 'left');
        $this->db->join($this->branches . ' as brn', 'brn.id = so.branch_id', 'left');
        //$this->db->join($this->sales_order_item . ' as sitem', 'sitem.so_id = so.id', 'left');
        $this->db->join($this->sales_payment . ' as p', $pay_condition, 'left');
        $this->db->join($this->user . ' as u', 'u.id = so.created_by', 'left');
        return $this->db->get($this->sales_order . ' AS so')->result();
    }

    public function get_static_pay_type_by_customer($from_date = '', $to_date = '', $cus = '', $brn = '') {
        $payment_types = payment_terms();
        $i = 0;
        $aa = array();
        foreach ($payment_types as $key => $value) {
            $aa[$key]['title'] = $value;
            $this->db->select('SUM(so.total_amt) as pay_amt', FALSE);
            $this->db->join($this->branches . ' as brn', 'brn.id = so.branch_id', 'left');
            if ((!empty($from_date))) {
                $this->db->where('so.so_date >=', date('Y-m-d H:i:s', strtotime($from_date)));
            }
            if ((!empty($to_date))) {
                $this->db->where('so.so_date <=', date('Y-m-d 23:59:59', strtotime($to_date)));
            }
            if ((!empty($cus))) {
                $this->db->where('so.customer_id', $cus);
            }
            if ((!empty($brn))) {
                $this->db->where('so.branch_id', $brn);
            } else {
                $this->db->where_in('so.branch_id', $this->user_data['branch_ids']);
            }
            $this->db->where('so.payment_status !=1 AND so.payment_status !=3');
            $this->db->where('so.status !=', 10);
            $this->db->where('so.payment_terms', $value);
            $sale_amt = $this->db->get($this->sales_order . ' AS so')->row()->pay_amt;
            $aa[$key]['amt'] = $sale_amt;
        }
        return $aa;
    }

    public function pending_account_summary($from_date = '', $to_date = '', $cus = '') {
        $this->db->select('SUM(so.total_amt) as pay_amt', FALSE);
        $this->db->join($this->branches . ' as brn', 'brn.id = so.branch_id', 'left');
        if ((!empty($from_date))) {
            $this->db->where('DATE(so.so_date) >', date('Y-m-d', strtotime($from_date)));
        }
        if ((!empty($to_date))) {
            $this->db->where('DATE(so.so_date) <=', date('Y-m-d', strtotime($to_date)));
        }
        if ((!empty($cus))) {
            $this->db->where('so.customer_id', $cus);
        }
        if ((!empty($brn))) {
            $this->db->where('so.branch_id', $brn);
        } else {
            $this->db->where_in('so.branch_id', $this->user_data['branch_ids']);
        }
        $this->db->where('so.payment_status !=1 AND so.payment_status !=3');
        $this->db->where('so.status !=', 10);
       // $this->db->where('so.payment_terms', $value);
        return $this->db->get($this->sales_order . ' AS so')->row()->pay_amt;
    }
}
?>