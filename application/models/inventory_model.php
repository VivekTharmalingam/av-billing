<?php

class Inventory_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->branch_product = TBL_BRN_PDT;
        $this->product = TBL_PDT;
        $this->category = TBL_CAT;
        $this->branches = TBL_BRN;
        $this->brand = TBL_CAT;
        $this->inventory_adjustment = TBL_INVENTORY_ADJUST;
        $this->purchase_receive = TBL_PR;
        $this->purchase_receive_items = TBL_PRIT;
        $this->supplier = TBL_SUP;
        $this->user_data = get_user_data();
    }

    public function list_all() {
        $this->db->select('bp.*, CASE bp.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str, p.pdt_code, p.pdt_name, c.cat_name', FALSE);
        $this->db->join($this->product . ' as p', 'p.id = bp.product_id AND p.status != 10');
        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');
		$this->db->where('bp.status !=', 10);
        $this->db->where_in('bp.brn_id', $this->user_data['branch_id']);
        $this->db->order_by('p.pdt_code', 'asc');
        return $this->db->get($this->branch_product . ' as bp')->result();
    }
    
    public function get_trans_data($id) {
        $this->db->select('b.branch_name AS branch, p.pdt_code AS code,p.bar_code as bar_code,p.s_no as s_no, p.pdt_name product, ia.id AS iId, ia.quantity AS qty, CASE ia.adjust_type WHEN 1 THEN "Increament" ELSE "Decrement" END as adjust_type_str, ia.remarks AS rem, c.cat_name AS cat, DATE_FORMAT(ia.created_on, "%d-%m-%Y") as created_on', FALSE);
        $this->db->join(TBL_PDT . ' as p', 'p.id = ia.product_id', 'left');
        $this->db->join(TBL_CAT . ' as c', 'c.id = p.cat_id', 'left');
        $this->db->join(TBL_BRN . ' as b', 'b.id = ia.brn_id', 'left');
        $this->db->where('ia.id', $id);
        $this->db->where('ia.status !=', 10);
        return $this->db->get(TBL_INVENTORY_ADJUST . ' AS ia')->result_array();
    }

    public function list_branch() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->where_in('id', $this->user_data['branch_ids']);
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->branches)->result();
    }

    public function list_product() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('pdt_code', 'asc');
        return $this->db->get($this->product)->result();
    }

    public function list_brand() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->brand)->result();
    }

    public function get_product_by_category($id) {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('cat_id', $id);
        return $this->db->get($this->product)->result();
    }

    public function get_by_id($id) {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('id', $id);
        return $this->db->get($this->companies)->row();
    }

    public function get_branch_by_id($id) {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('branch_id', $id);
        $this->db->order_by('id', 'asc');
        return $this->db->get($this->branches)->result();
    }

    public function get_last_row() {
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'desc');
        $this->db->limit('1');
        return $this->db->get($this->companies)->row();
    }

    public function insert($data) {
        $this->db->insert($this->companies, $data);
        return $this->db->insert_id();
    }

    public function insert_branches($data) {
        $this->db->insert($this->branches, $data);
        return $this->db->insert_id();
    }

    public function list_all_adjustment() {
        $this->db->select('b.branch_name, p.pdt_code, p.pdt_name, ia.id, ia.quantity, ia.adjust_type, CASE ia.adjust_type WHEN 1 THEN "Increament" ELSE "Decrement" END as adjust_type_str, ia.remarks, c.cat_name', FALSE);
        $this->db->join($this->branches . ' as b', 'b.id = ia.brn_id', 'left');
        $this->db->join($this->product . ' as p', 'p.id = ia.product_id', 'left');
		$this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');
        $this->db->where('ia.status !=', 10);
        $this->db->order_by('ia.id', 'desc');
        $this->db->where_in('brn_id', $this->user_data['branch_id']);
        return $this->db->get($this->inventory_adjustment . ' as ia')->result();
    }

    public function insert_adjustment($data) {
        $this->db->insert($this->inventory_adjustment, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->companies, $data);
        return $this->db->affected_rows();
    }

    public function update_branches($update_id, $val) {
        $this->db->where('id', $update_id);
        $this->db->update($this->branches, $val);
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->companies);
        return $this->db->affected_rows();
    }

    public function row_delete($brnId) {
        $this->db->where('id', $brnId);
        $this->db->delete($this->branches);
    }

    public function check_companies_exists($comp, $id) {
        $this->db->where('comp_name', $comp);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        if (!empty($id))
            $this->db->where('id !=', $id);
        $getData = $this->db->get($this->companies);
        if ($getData->num_rows() > 0)
            return 1;
        else
            return 0;
    }

    public function list_search_all($branch_id = '', $brand_id = '', $category_id = '', $sub_category_id = '') {
        $this->db->select('bp.id,bp.available_qty, bp.brn_id, bp.product_id , p.pdt_name,p.pdt_code,p.selling_price, p.buying_price, p.cat_id, b.branch_name, c.cat_name', FALSE);
        $this->db->join($this->product . ' as p', 'p.id = bp.product_id AND p.status != 10');
        $this->db->join($this->branches . ' as b', 'b.id = bp.brn_id', 'left');
        $this->db->join($this->brand . ' as c', 'c.id = p.cat_id', 'left');
        $this->db->where('bp.status !=', 10);
		$this->db->group_by('bp.id');
        if ((!empty($branch_id))) {
            $this->db->where('bp.brn_id', $branch_id);
        }else{
            $this->db->where_in('bp.brn_id', $this->user_data['branch_ids']);
        }
        if ((!empty($brand_id))) {
            $this->db->where('p.cat_id', $brand_id);
        }
		
		if ((!empty($category_id))) {
            $this->db->where('p.item_category', $category_id);
        }
		if ((!empty($sub_category_id))) {
            $this->db->where('p.sub_category', $sub_category_id);
        }
		
        return $this->db->get($this->branch_product . ' as bp')->result();
    }

    public function get_product_by_id($id) {
        $this->db->select('brn.available_qty, brn.brn_id, brn.product_id , p.pdt_name,p.selling_price, p.cat_id, b.branch_name,c.cat_name', FALSE);
        $this->db->join($this->product . ' as p', 'p.id = brn.product_id AND p.status != 10', 'left');
        $this->db->join($this->branches . ' as b', 'b.id = brn.brn_id', 'left');
        $this->db->join($this->brand . ' as c', 'c.id = p.cat_id', 'left');
        $this->db->where('brn.status !=', 10);
        $this->db->where('brn.id', $id);

        return $this->db->get($this->branch_product . ' as brn')->row();
    }
    
    
    public function get_unique_supplier_lists($item_id = '') {        
        $this->db->select('p.pdt_name,pr.sup_id as sid,s.name,s.contact_name,s.phone_no,s.email_id,s.address', FALSE);    
        $this->db->join($this->purchase_receive_items . ' as prit', 'pr.id = prit.pr_id', 'left');   
        $this->db->join($this->supplier . ' as s', 's.id = pr.sup_id', 'left');
        $this->db->join($this->product . ' as p', 'p.id = prit.product_id', 'left');             
        $this->db->where('prit.status !=', 10);
        $this->db->where('pr.pr_status !=', 10);
        if ((!empty($item_id))) {
            $this->db->where('prit.product_id', $item_id);
        }
        $this->db->group_by('pr.sup_id');
        return $this->db->get($this->purchase_receive . ' as pr')->result();
    }
	
	public function products_stock_by_branch_id($branch_id = '') {
        $this->db->select('bp.brn_id, CASE WHEN (bp.available_qty IS NOT NULL AND bp.available_qty > 0) THEN bp.available_qty ELSE "0" END as available_qty, b.branch_name, bp.branch_location, p.id as product_id, p.pdt_code, p.pdt_name, p.bar_code, p.s_no, CONCAT(p.pdt_code, " / ", p.pdt_name) as label, p.pdt_code as value', FALSE);
		$this->db->join($this->branch_product . ' as bp', 'bp.product_id = p.id AND bp.status != 10 AND bp.brn_id = ' . $branch_id, 'left');
		$this->db->join($this->branches . ' as b', 'b.id = bp.brn_id AND b.status != 10', 'left');
		
        $this->db->where('p.status !=', 10);
        return $this->db->get($this->product . ' as p')->result();
    }
}

?>