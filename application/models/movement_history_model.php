<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Movement_History_Model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->branches = TBL_BRN;
        $this->product = TBL_PDT;
        $this->category = TBL_CAT;
        $this->purchase_receive = TBL_PR;
        $this->purchase_receive_items = TBL_PRIT;
        $this->inventory_product = TBL_INV_PDT;
        $this->branch_products = TBL_BRN_PDT;
        $this->purchase_return = TBL_PORET;
        $this->purchase_return_items = TBL_PORETI;
        $this->sales_order = TBL_SO;
        $this->sales_order_item = TBL_SO_ITM;
        $this->services_inventory_items = TBL_SEXINITM;
        $this->sales_return_item = TBL_SO_RET_ITM;
        $this->sales_return = TBL_SO_RET;
        $this->scan_products = TBL_SCN_PDT;
        $this->inv_adjustment = TBL_INVENTORY_ADJUST;
        $this->transfer_stock = TBL_TRANS_STOCK;
        $this->user_data = get_user_data();        
    }
	
    public function po_receive_items() {
        $this->db->select('1 as transaction_type, "Purchase Receive" as transaction_type_str, "+" as op, pr.pr_code as order_no, pri.pr_id as id, pri.product_id as item_id, pri.pr_quantity as quantity,pri.branch_id, pri.created_on, pri.updated_on, p.pdt_code, p.bar_code as bar_code,p.s_no as s_no, p.pdt_name, brn.branch_name, c.cat_name', FALSE);
        $this->db->join($this->purchase_receive . ' as pr', 'pr.id = pri.pr_id', 'left');
        $this->db->join($this->branch_products . ' as bnp', 'bnp.product_id = pri.product_id', 'left');
        $this->db->join($this->branches . ' as brn', 'brn.id = bnp.brn_id', 'left');
        $this->db->join($this->product . ' as p', 'p.id = pri.product_id AND p.status != 10');
        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');
//        if($this->user_data['is_admin'] != '1'){
//            $this->db->where_in('pri.branch_id', $this->user_data['branch_ids']);
//        }
        $this->db->where_in('pri.branch_id', $this->user_data['branch_id']);
        $this->db->where('pri.status !=', 10);
        $this->db->group_by('pri.id');        
        return $this->db->get($this->purchase_receive_items . ' as pri')->result();        
    }
    
    public function po_return_items() {
        $this->db->select('2 as transaction_type, "Purchase Return" as transaction_type_str, "-" as op, pr.por_no as order_no, pri.po_ret_id as id, pri.product_id as item_id, pri.returned_quantity as quantity,pri.brn_id, pri.created_on, pri.updated_on, p.pdt_code, p.bar_code as bar_code,p.s_no as s_no, p.pdt_name, brn.branch_name, c.cat_name', FALSE);
        $this->db->join($this->purchase_return . ' as pr', 'pr.id = pri.po_ret_id', 'left');
        $this->db->join($this->branch_products . ' as bnp', 'bnp.product_id = pri.product_id', 'left');
        $this->db->join($this->branches . ' as brn', 'brn.id = bnp.brn_id', 'left');
        $this->db->join($this->product . ' as p', 'p.id = pri.product_id AND p.status != 10');
        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');
//        if($this->user_data['is_admin'] != '1'){
//            $this->db->where_in('pri.branch_id', $this->user_data['branch_ids']);
//        }
        $this->db->where('pri.brn_id', $this->user_data['branch_id']);
        $this->db->where('pri.status !=', 10);
        $this->db->group_by('pri.id');          
        return $this->db->get($this->purchase_return_items . ' as pri')->result();        
    }
    
    public function so_items() {
        $this->db->select('3 as transaction_type, "Invoice" as transaction_type_str, "-" as op, soi.so_id as id, so.so_no as order_no, soi.item_id, soi.quantity, soi.created_on, soi.updated_on, p.pdt_code, p.bar_code as bar_code,p.s_no as s_no, p.pdt_name, brn.branch_name, c.cat_name', FALSE);
        $this->db->join($this->sales_order . ' as so', 'so.id = soi.so_id', 'left');
        $this->db->join($this->branch_products . ' as bnp', 'bnp.product_id = soi.item_id', 'left');
        $this->db->join($this->branches . ' as brn', 'brn.id = bnp.brn_id', 'left');
        $this->db->join($this->product . ' as p', 'p.id = soi.item_id AND p.status != 10');
        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');
//        if($this->user_data['is_admin'] != '1'){
//            $this->db->where_in('soi.branch_id', $this->user_data['branch_ids']);
//        }
        $this->db->where_in('soi.branch_id', $this->user_data['branch_id']);
        $this->db->where('soi.status !=', 10);
        $this->db->where('soi.exc_form_type !=', 1);
        $this->db->where('soi.service_form_type', 0);
        $this->db->group_by('soi.id');  
        return $this->db->get($this->sales_order_item . ' as soi')->result();        
    }
    
    public function so_service_items() {
        $this->db->select('3 as transaction_type, "Invoice" as transaction_type_str, "-" as op, soi.so_id as id, so.so_no as order_no, serinv.item_id, soi.quantity, soi.created_on, soi.updated_on, p.pdt_code, p.bar_code as bar_code,p.s_no as s_no, p.pdt_name, brn.branch_name, c.cat_name', FALSE);
        $this->db->join($this->sales_order . ' as so', 'so.id = soi.so_id', 'left');
        $this->db->join($this->services_inventory_items . ' as serinv', 'serinv.id = soi.item_id and serinv.form_type = 2 and serinv.status = 1');
        $this->db->join($this->branch_products . ' as bnp', 'bnp.product_id = serinv.item_id', 'left');
        $this->db->join($this->branches . ' as brn', 'brn.id = bnp.brn_id', 'left');
        $this->db->join($this->product . ' as p', 'p.id = serinv.item_id AND p.status != 10');
        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');
//        if($this->user_data['is_admin'] != '1'){
//            $this->db->where_in('soi.branch_id', $this->user_data['branch_ids']);
//        }
        $this->db->where_in('soi.branch_id', $this->user_data['branch_id']);
        $this->db->where('soi.status !=', 10);
        $this->db->where('soi.service_form_type', 2);
        $this->db->group_by('soi.id');  
        return $this->db->get($this->sales_order_item . ' as soi')->result();        
    }
    
    public function so_return_items() {
        $this->db->select('4 as transaction_type, "Invoice Return" as transaction_type_str, "+" as op, soi.so_ret_id as id, sr.sor_no as order_no, soi.item_id, soi.restock_quantity as quantity, soi.created_on, soi.updated_on, p.pdt_code, p.bar_code as bar_code,p.s_no as s_no, p.pdt_name, brn.branch_name, c.cat_name', FALSE);
        $this->db->join($this->sales_return . ' as sr', 'sr.id = soi.so_ret_id', 'left');
        $this->db->join($this->sales_order_item . ' as so_item', 'so_item.id = soi.so_item_autoid and so_item.exc_form_type != 1 and so_item.service_form_type=0 and so_item.status != 10');
        $this->db->join($this->branch_products . ' as bnp', 'bnp.product_id = soi.item_id', 'left');
        $this->db->join($this->branches . ' as brn', 'brn.id = bnp.brn_id', 'left');
        $this->db->join($this->product . ' as p', 'p.id = soi.item_id AND p.status != 10');
        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');
//        if($this->user_data['is_admin'] != '1'){
//            $this->db->where_in('soi.branch_id', $this->user_data['branch_ids']);
//        }
        $this->db->where_in('soi.brn_id', $this->user_data['branch_id']);
        $this->db->where('soi.status !=', 10);
        $this->db->group_by('soi.id');  
        return $this->db->get($this->sales_return_item . ' as soi')->result();        
    }
    
    public function so_return_service_items() {
        $this->db->select('4 as transaction_type, "Invoice Return" as transaction_type_str, "+" as op, soi.so_ret_id as id, sr.sor_no as order_no, serinv.item_id, soi.restock_quantity as quantity, soi.created_on, soi.updated_on, p.pdt_code, p.bar_code as bar_code,p.s_no as s_no, p.pdt_name, brn.branch_name, c.cat_name', FALSE);
        $this->db->join($this->sales_return . ' as sr', 'sr.id = soi.so_ret_id', 'left');
        $this->db->join($this->sales_order_item . ' as so_item', 'so_item.id = soi.so_item_autoid and so_item.service_form_type=2 and so_item.status != 10');
        $this->db->join($this->services_inventory_items . ' as serinv', 'serinv.id = soi.item_id and serinv.form_type = 2 and serinv.status = 1');
        $this->db->join($this->branch_products . ' as bnp', 'bnp.product_id = serinv.item_id', 'left');
        $this->db->join($this->branches . ' as brn', 'brn.id = bnp.brn_id', 'left');
        $this->db->join($this->product . ' as p', 'p.id = serinv.item_id AND p.status != 10');
        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');
//        if($this->user_data['is_admin'] != '1'){
//            $this->db->where_in('soi.branch_id', $this->user_data['branch_ids']);
//        }
        $this->db->where_in('soi.brn_id', $this->user_data['branch_id']);
        $this->db->where('soi.status !=', 10);
        $this->db->group_by('soi.id');  
        return $this->db->get($this->sales_return_item . ' as soi')->result();        
    }
    
    public function scan_items() {
        $this->db->select('5 as transaction_type, "Via Scan" as transaction_type_str, "+" as op, sp.id as id, "SCAN" as order_no, sp.product_id AS item_id, sp.quantity, sp.created_on, sp.created_on AS updated_on , p.pdt_code, p.bar_code as bar_code,p.s_no as s_no, p.pdt_name, brn.branch_name, c.cat_name', FALSE);
        $this->db->join($this->branch_products . ' as bnp', 'bnp.product_id = sp.product_id', 'left');
        $this->db->join($this->branches . ' as brn', 'brn.id = bnp.brn_id', 'left');
        $this->db->join($this->product . ' as p', 'p.id = sp.product_id AND p.status != 10');
        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');        
//        if($this->user_data['is_admin'] != '1'){
//            $this->db->where_in('sp.branch_id', $this->user_data['branch_ids']);
//        } 
        $this->db->where_in('sp.branch_id', $this->user_data['branch_id']);
        $this->db->where('sp.status !=', 10);
        $this->db->group_by('sp.id'); 
        return $this->db->get($this->scan_products . ' as sp')->result();        
    }
    
    public function inventory_adjustment() {
        $this->db->select('6 as transaction_type, "Inventory Adjustment" as transaction_type_str,inad.id as id, "INV_ADJUSTMENT" as order_no, inad.product_id AS item_id, inad.quantity, inad.created_on, inad.updated_on, p.pdt_code, p.bar_code as bar_code,p.s_no as s_no, p.pdt_name, brn.branch_name, c.cat_name,CASE inad.adjust_type WHEN 1 THEN "+" WHEN 2 THEN "-" ELSE "" END as op', FALSE);
        $this->db->join($this->branch_products . ' as bnp', 'bnp.product_id = inad.product_id AND bnp.brn_id = inad.branch_id', 'left');
        $this->db->join($this->branches . ' as brn', 'brn.id = bnp.brn_id', 'left');
        $this->db->join($this->product . ' as p', 'p.id = inad.product_id AND p.status != 10');
        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');        
//        if($this->user_data['is_admin'] != '1'){
//            $this->db->where_in('inad.branch_id', $this->user_data['branch_ids']);
//        }   
        $this->db->where_in('inad.branch_id', $this->user_data['branch_id']);
        $this->db->where('inad.status !=', 10);
        $this->db->group_by('inad.id'); 
        return $this->db->get($this->inv_adjustment . ' as inad')->result();        
    }
    
    public function from_transfer_stock() {
        $this->db->select('7 as transaction_type, "Transter Stock" as transaction_type_str, "-" as op, trs.id as id, "TRNS_STOCK_DEC" as order_no, trs.product_id AS item_id, trs.received_qty AS quantity, trs.created_on, trs.created_on AS updated_on , p.pdt_code, p.bar_code as bar_code,p.s_no as s_no, p.pdt_name, CONCAT("From - ", brn.branch_name) AS branch_name, c.cat_name', FALSE);
        $this->db->join($this->branch_products . ' as bnp', 'bnp.product_id = trs.product_id AND bnp.brn_id = trs.from_brn_id', 'left');
        $this->db->join($this->branches . ' as brn', 'brn.id = bnp.brn_id', 'left');
        $this->db->join($this->product . ' as p', 'p.id = trs.product_id AND p.status != 10');
        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');        
//        if($this->user_data['is_admin'] != '1'){
//            $this->db->where_in('trs.branch_id', $this->user_data['branch_ids']);
//        } 
        $this->db->where_in('trs.from_brn_id', $this->user_data['branch_id']);
        $this->db->where('trs.approval_status', 2);
        $this->db->where('trs.status !=', 10);
        $this->db->group_by('trs.id'); 
        return $this->db->get($this->transfer_stock . ' as trs')->result();        
    }
    
    public function to_transfer_stock() {
        $this->db->select('7 as transaction_type, "Transter Stock" as transaction_type_str, "+" as op, trs.id as id, "TRNS_STOCK_INC" as order_no, trs.product_id AS item_id, trs.received_qty AS quantity, trs.created_on, trs.created_on AS updated_on , p.pdt_code, p.bar_code as bar_code,p.s_no as s_no, p.pdt_name, CONCAT("To - ", brn.branch_name) AS branch_name, c.cat_name', FALSE);
        $this->db->join($this->branch_products . ' as bnp', 'bnp.product_id = trs.product_id AND bnp.brn_id = trs.to_brn_id', 'left');
        $this->db->join($this->branches . ' as brn', 'brn.id = bnp.brn_id', 'left');
        $this->db->join($this->product . ' as p', 'p.id = trs.product_id AND p.status != 10');
        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');        
//        if($this->user_data['is_admin'] != '1'){
//            $this->db->where_in('trs.branch_id', $this->user_data['branch_ids']);
//        }        
        $this->db->where_in('trs.to_brn_id', $this->user_data['branch_id']);
        $this->db->where('trs.approval_status', 2);
        $this->db->where('trs.status !=', 10);
        $this->db->group_by('trs.id'); 
        return $this->db->get($this->transfer_stock . ' as trs')->result();        
    }
}
?>