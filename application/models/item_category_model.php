<?php

class Item_category_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->item_cat = TBL_ICT;
    }

    public function list_all() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->item_cat)->result();
    }

    public function get_by_id($id) {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('id', $id);
        return $this->db->get($this->item_cat)->row();
    }

    public function insert($data) {
        $this->db->insert($this->item_cat, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->item_cat, $data);
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->item_cat);
        return $this->db->affected_rows();
    }

    public function check_exp_exists($exp, $id) {
        $this->db->where('item_category_name', $exp);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        if (!empty($id))
            $this->db->where('id !=', $id);
        $getData = $this->db->get($this->item_cat);
        if ($getData->num_rows() > 0)
            return 1;
        else
            return 0;
    }

}

?>