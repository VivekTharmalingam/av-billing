<?php



class Product_Model extends CI_Model {



    public function __construct() {

        parent::__construct();

        $this->product = TBL_PDT;

        $this->category = TBL_CAT;

        $this->branch_products = TBL_BRN_PDT;

        $this->branch = TBL_BRN;

        $this->unit = TBL_UNIT;

        $this->branch_product = TBL_BRN_PDT;

        $this->scan_products = TBL_SCN_PDT;

        $this->products_log = TBL_PCOL;

        $this->purchase_with_purchase = TBL_PWP;

        $this->purchase_with_purchase_items = TBL_PWP_ITEMS;

        $this->buy_one_get_one = TBL_BOGO;

        $this->buy_one_get_one_items = TBL_BOGO_ITEMS;

        $this->group_buy = TBL_GROUP_BUY;

        $this->group_buy_items = TBL_GROUP_BUY_ITEMS;

        $this->discount_sale = TBL_DISC_SALE;

        $this->item_cat = TBL_ICT;

        $this->sub_cat = TBL_SUB_CAT;

    }



    public function active_category() {

        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);

        $this->db->where('status', 1);

        $this->db->order_by('cat_name', 'asc');

        return $this->db->get($this->category)->result();

    }



    public function category() {

        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);

        $this->db->where('status != 10');

        $this->db->where('status != 2');

        $this->db->order_by('cat_name', 'asc');

        return $this->db->get($this->category)->result();

    }



    public function item_category() {

        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);

        $this->db->where('status != 10');

        $this->db->where('status != 2');

        $this->db->order_by('item_category_name', 'asc');

        return $this->db->get($this->item_cat)->result();

    }



    /* public function branch_name($branch_id) {

      $this->db->select('id, branch_name', FALSE);

      $this->db->where_in('id', $branch_id);

      return $this->db->get($this->branch)->result();

      } */



    public function active_branch_products($item_id = '', $branch_id = '') {

        $this->db->select('bp.id, bp.brn_id, bp.product_id, bp.available_qty, b.branch_name, bp.branch_location, p.pdt_code, p.pdt_name, p.pdt_spec, p.bar_code, p.s_no, CONCAT(p.pdt_code, " / ", p.pdt_name) as label, p.pdt_code as value', FALSE);

        $this->db->join($this->product . ' as p', 'p.id = bp.product_id AND p.status != 10');

        $this->db->join($this->branch . ' as b', 'b.id = bp.brn_id AND b.status != 10');

        $this->db->where('bp.status !=', 10);

        if (!empty($branch_id)) {

            $this->db->where('bp.brn_id', $branch_id);

        } else {

            $this->db->where_in('bp.brn_id', $this->user_data['branch_ids']);

        }



        if (!empty($item_id)) {

            $this->db->where('bp.product_id', $item_id);

        }



        return $this->db->get($this->branch_products . ' as bp')->result();

    }



    public function branch_products_stock($item_id = '') {

        $this->db->select('bp.brn_id, bp.product_id, bp.available_qty, b.branch_name, bp.branch_location');

        $this->db->join($this->branch . ' as b', 'b.id = bp.brn_id AND b.status != 10');

        $this->db->where('bp.status !=', 10);

        //$this->db->where_in('bp.brn_id', $this->user_data['branch_ids']);



        if (!empty($item_id)) {

            $this->db->where('bp.product_id', $item_id);

        }



        return $this->db->get($this->branch_products . ' as bp')->result();

    }



    /* public function get_branch_by_name($branch_name) {

      $this->db->select('id', FALSE);

      $this->db->where('branch_name',$branch_name);

      return $this->db->get($this->branch)->row();

      } */



    public function product_id($product_name) {

        $this->db->select('id', FALSE);

        $this->db->where('pdt_code', $product_name);

        return $this->db->get($this->product)->row();

    }



    public function update_serial_no($product_id, $sl_no) {

        $this->db->set('s_no', "CONCAT(s_no,',','" . $sl_no . "')", FALSE);

        $this->db->where('id', $product_id);

        $this->db->update($this->product);

        $offect = $this->db->affected_rows();



        $this->db->set('s_no', "TRIM(LEADING ',' FROM s_no)", FALSE);

        $this->db->where('id', $product_id);

        $this->db->update($this->product);



        return $offect;

    }



    public function remove_serial_no($product_id, $sl_no) {

        $this->db->set('s_no', "REPLACE(s_no,'" . $sl_no . "','')", FALSE);

        $this->db->where('id', $product_id);

        $this->db->update($this->product);

        $offect = $this->db->affected_rows();



        $this->db->set('s_no', "REPLACE(s_no,',,',',')", FALSE);

        $this->db->where('id', $product_id);

        $this->db->update($this->product);

        $offect1 = $this->db->affected_rows();



        $this->db->set('s_no', "TRIM(BOTH ',' FROM s_no)", FALSE);

        $this->db->where('id', $product_id);

        $this->db->update($this->product);



        return $offect;

    }



    public function purchase_with_purchase() {

        $this->db->select('p.id, p.item_id', FALSE);

        $this->db->select('DATE_FORMAT(p.from_date, "%d-%m-%Y") as from_date', FALSE);

        $this->db->select('DATE_FORMAT(p.to_date, "%d-%m-%Y") as to_date', FALSE);

        $this->db->select('GROUP_CONCAT(pi.offer_item_id SEPARATOR ",") as offer_item_ids', FALSE);

        $this->db->select('GROUP_CONCAT(i.pdt_code SEPARATOR ",") as pdt_codes', FALSE);

        $this->db->select('GROUP_CONCAT(i.pdt_name SEPARATOR ",") as pdt_names', FALSE);

        $this->db->select('GROUP_CONCAT(i.buying_price SEPARATOR ",") as buying_prices', FALSE);

        $this->db->select('GROUP_CONCAT(i.selling_price SEPARATOR ",") as selling_prices', FALSE);

        $this->db->select('GROUP_CONCAT(pi.offer_price SEPARATOR ",") as offer_prices', FALSE);

        $this->db->select('GROUP_CONCAT(if(c.cat_name IS NOT NULL, c.cat_name, "") SEPARATOR ",") as cat_names', FALSE);

        $this->db->join($this->purchase_with_purchase_items . ' as pi', 'pi.pwp_id = p.id AND pi.status = 1');

        $this->db->join($this->product . ' as i', 'i.id = pi.offer_item_id AND i.status = 1');

        $this->db->join($this->category . ' as c', 'c.id = i.cat_id', 'left');

        //$this->db->where('p.item_id', $item_id);

        $this->db->where('p.status', 1);

        #$this->db->where('CURDATE() BETWEEN DATE(p.from_date) AND DATE(p.to_date)');

        $this->db->where('((p.from_date IS NULL) && (CURDATE() <= DATE(p.to_date))) OR (CURDATE() BETWEEN DATE(p.from_date) AND DATE(p.to_date)) OR ((CURDATE() >= DATE(p.from_date)) && (p.to_date IS NULL))');

        $this->db->group_by('p.id');

        return $this->db->get($this->purchase_with_purchase . ' as p')->result();

    }



    public function group_buy($item_id) {

        $this->db->select('g.id', FALSE);

        $this->db->select('GROUP_CONCAT(g.group_name SEPARATOR ",") as group_names', FALSE);

        $this->db->select('DATE_FORMAT(g.from_date, "%d-%m-%Y") as from_date', FALSE);

        $this->db->select('DATE_FORMAT(g.to_date, "%d-%m-%Y") as to_date', FALSE);

        $this->db->select('GROUP_CONCAT(gi.offer_item_id SEPARATOR ",") as offer_item_ids', FALSE);

        $this->db->select('GROUP_CONCAT(gi.offer_price SEPARATOR ",") as offer_prices', FALSE);

        $this->db->select('GROUP_CONCAT(i.pdt_code SEPARATOR ",") as pdt_codes', FALSE);

        $this->db->select('GROUP_CONCAT(i.pdt_name SEPARATOR ",") as pdt_names', FALSE);

        $this->db->select('GROUP_CONCAT(i.buying_price SEPARATOR ",") as buying_prices', FALSE);

        $this->db->select('GROUP_CONCAT(i.selling_price SEPARATOR ",") as selling_prices', FALSE);

        $this->db->select('GROUP_CONCAT(if(c.cat_name IS NOT NULL, c.cat_name, "") SEPARATOR ",") as cat_names', FALSE);

        $this->db->join($this->group_buy_items . ' as gi', 'gi.group_id = g.id AND gi.status = 1');

        $this->db->join($this->product . ' as i', 'i.id = gi.offer_item_id AND i.status = 1');

        $this->db->join($this->category . ' as c', 'c.id = i.cat_id', 'left');

        //$this->db->where('p.item_id', $item_id);

        $this->db->where('g.status', 1);

        #$this->db->where('CURDATE() BETWEEN DATE(g.from_date) AND DATE(g.to_date)');

        $this->db->where('((g.from_date IS NULL) && (CURDATE() <= DATE(g.to_date))) OR (CURDATE() BETWEEN DATE(g.from_date) AND DATE(g.to_date)) OR ((CURDATE() >= DATE(g.from_date)) && (g.to_date IS NULL))');

        $this->db->group_by('g.id');

        return $this->db->get($this->group_buy . ' as g')->result();

    }



    public function buy_one_get_one() {

        $this->db->select('b.id, b.item_id', FALSE);

        $this->db->select('DATE_FORMAT(b.from_date, "%d-%m-%Y") as from_date', FALSE);

        $this->db->select('DATE_FORMAT(b.to_date, "%d-%m-%Y") as to_date', FALSE);

        $this->db->select('GROUP_CONCAT(bi.offer_item_id SEPARATOR ",") as offer_item_ids', FALSE);

        $this->db->select('GROUP_CONCAT(bi.offer_price SEPARATOR ",") as offer_prices', FALSE);

        $this->db->select('GROUP_CONCAT(i.pdt_code SEPARATOR ",") as pdt_codes', FALSE);

        $this->db->select('GROUP_CONCAT(i.pdt_name SEPARATOR ",") as pdt_names', FALSE);

        $this->db->select('GROUP_CONCAT(i.buying_price SEPARATOR ",") as buying_prices', FALSE);

        $this->db->select('GROUP_CONCAT(i.selling_price SEPARATOR ",") as selling_prices', FALSE);

        $this->db->select('GROUP_CONCAT(if(c.cat_name IS NOT NULL, c.cat_name, "") SEPARATOR ",") as cat_names', FALSE);

        $this->db->join($this->buy_one_get_one_items . ' as bi', 'bi.bogo_id = b.id AND bi.status = 1');

        $this->db->join($this->product . ' as i', 'i.id = bi.offer_item_id AND i.status = 1');

        $this->db->join($this->category . ' as c', 'c.id = i.cat_id', 'left');

        //$this->db->where('p.item_id', $item_id);

        $this->db->where('b.status', 1);

        $this->db->where('((b.from_date IS NULL) && (CURDATE() <= DATE(b.to_date))) OR (CURDATE() BETWEEN DATE(b.from_date) AND DATE(b.to_date)) OR ((CURDATE() >= DATE(b.from_date)) && (b.to_date IS NULL))');

        $this->db->group_by('b.id');

        return $this->db->get($this->buy_one_get_one . ' as b')->result();

    }



    public function discount_sale_items() {

        $this->db->select('d.id, d.offer_item_id, d.offer_price, d.offer_percentage, i.pdt_code, i.pdt_name, i.pdt_spec, i.buying_price, i.selling_price, c.cat_name', FALSE);

        $this->db->select('DATE_FORMAT(d.from_date, "%d-%m-%Y") as from_date', FALSE);

        $this->db->select('DATE_FORMAT(d.to_date, "%d-%m-%Y") as to_date', FALSE);

        $this->db->join($this->product . ' as i', 'i.id = d.offer_item_id AND i.status = 1');

        $this->db->join($this->category . ' as c', 'c.id = i.cat_id', 'left');

        //$this->db->where('p.item_id', $item_id);

        $this->db->where('d.status', 1);

        #$this->db->where('CURDATE() BETWEEN DATE(d.from_date) AND DATE(d.to_date)');

        $this->db->where('((d.from_date IS NULL) && (CURDATE() <= DATE(d.to_date))) OR (CURDATE() BETWEEN DATE(d.from_date) AND DATE(d.to_date)) OR ((CURDATE() >= DATE(d.from_date)) && (d.to_date IS NULL))');

        return $this->db->get($this->discount_sale . ' as d')->result();

    }



    public function get_item_promotions() {

        $promotions = array();

        $promotions['purchase_with_purchase'] = $this->purchase_with_purchase();

        $promotions['group_buy'] = $this->group_buy();

        $promotions['bogo'] = $this->buy_one_get_one();

        $promotions['discount_sale'] = $this->discount_sale_items();

        return $promotions;

    }



    public function search_item() {

        $this->db->select('p.id, p.pdt_code, p.pdt_name, p.pdt_spec, p.pdt_description, p.buying_price, p.bfr_gst_buying_price, p.bar_code, p.s_no, p.selling_price, c.id as cat_id, c.cat_code, c.cat_name', FALSE);

        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');

        $this->db->where('p.status', 1);

        $this->db->order_by('p.id', 'desc');

        return $this->db->get($this->product . ' as p')->result();

    }



    public function search_item_by_brand() {

        $this->db->select('p.id, p.pdt_code, p.pdt_name, p.pdt_spec, p.pdt_description, p.buying_price, p.bfr_gst_buying_price, p.bar_code, p.selling_price, c.id as cat_id, c.cat_code, c.cat_name', FALSE);

        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');

        $this->db->where('p.status', 1);

        $this->db->where('p.pdt_name !=', '');

        $this->db->order_by('c.cat_name');

        return $this->db->get($this->product . ' as p')->result();

    }



    public function search_item_by_barcode($item) {

        $this->db->select('p.*, c.id as cat_id, c.cat_code, c.cat_name, bp.available_qty', FALSE);

        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');

        $this->db->join($this->branch_product . ' as bp', 'bp.product_id = p.id AND bp.brn_id = ' . $this->user_data['branch_id'], 'left');

        $this->db->where('p.status', 1);

        $this->db->where('p.bar_code !=', '');

        $this->db->order_by('p.pdt_name');

        return $this->db->get($this->product . ' as p')->result();

    }



    public function search_item_by_serialno($item) {

        $this->db->select('p.*, c.id as cat_id, c.cat_code, c.cat_name,bp.available_qty', FALSE);

        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');

        $this->db->join($this->branch_product . ' as bp', 'bp.product_id = p.id AND bp.brn_id = ' . $this->user_data['branch_id'], 'left');

        $this->db->where('p.status', 1);

        $this->db->where('p.s_no !=', '');

        $this->db->order_by('p.pdt_name');

        return $this->db->get($this->product . ' as p')->result();

    }



    public function get_product_by_barcode($item) {

        $this->db->select('p.id, p.pdt_code,p.bar_code, p.pdt_name, p.pdt_spec, p.pdt_description, p.buying_price, p.bfr_gst_buying_price, p.selling_price, c.id as cat_id, c.cat_code, c.cat_name,bp.available_qty', FALSE);

        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');

        $this->db->join($this->branch_product . ' as bp', 'bp.product_id = p.id', 'left');

        $this->db->where('p.status', 1);

        $this->db->where('p.bar_code', $item);

        $this->db->order_by('p.pdt_name');

        return $this->db->get($this->product . ' as p')->row();

    }



    public function unit() {

        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);

        $this->db->where('status != 10');

        $this->db->where('status != 2');

        $this->db->order_by('id', 'asc');

        return $this->db->get($this->unit)->result();

    }



    public function list_all_branch() {

        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);

        $this->db->where('status != 10');

        $this->db->where('status != 2');

        $this->db->order_by('id', 'asc');

        return $this->db->get($this->branch)->result();

    }



    public function list_all() {

        $this->db->select('p.*, CASE p.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str, c.cat_name', FALSE);

        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');

        $this->db->where('p.status != 10');

        $this->db->where('p.status != 2');

        $this->db->order_by('c.cat_name');

        return $this->db->get($this->product . ' as p')->result();

    }



    public function list_active() {

        $this->db->select('p.id, p.cat_id, p.pdt_code, p.pdt_name, p.pdt_spec, p.pdt_description, p.buying_price, p.bfr_gst_buying_price, p.selling_price, SUM(b.available_qty), c.cat_name', FALSE);

        $this->db->join($this->branch_products . ' as b', 'b.product_id = p.id AND b.status = 1', 'left');

        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');

        $this->db->where('p.status', 1);

        $this->db->group_by('p.id', 'desc');

        $this->db->order_by('p.pdt_name');

        return $this->db->get($this->product . ' as p')->result();

    }



    public function get_by_id($id) {

        $this->db->select('p.*,ic.item_category_name,sc.sub_category_name, CASE p.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);

        $this->db->join($this->item_cat . ' as ic', 'ic.id = p.item_category AND ic.status = 1', 'left');

        $this->db->join($this->sub_cat . ' as sc', 'sc.id = p.sub_category AND sc.status = 1', 'left');

        $this->db->where('p.id', $id);

        return $this->db->get($this->product . ' as p')->row();

    }



    public function get_inventory_by_id($id) {

        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);

        $this->db->where('product_id', $id);

        return $this->db->get($this->branch_product)->result();

    }



    /* public function list_branch_name($id, $brn_id) {

      $this->db->select('*', FALSE);

      $this->db->where('product_id', $id);

      $this->db->where('brn_id', $brn_id);

      $this->db->where('status = 1');

      return $this->db->get($this->branch_products . ' as pl')->row();

      } */



    public function insert($data) {

        $this->db->insert($this->product, $data);

        return $this->db->insert_id();

    }



    public function update($id, $data) {

        $this->db->where('id', $id);

        $this->db->update($this->product, $data);

        return $this->db->affected_rows();

    }



    public function delete($id) {

        $this->db->where('id', $id);

        $this->db->delete($this->product);

        return $this->db->affected_rows();

    }



    public function check_product_code_exists($code) {

        $this->db->where('pdt_code', $code);

        $this->db->where('status', 1);

        $getData = $this->db->get($this->product);

        if ($getData->num_rows() > 0)

            return 1;

        else

            return 0;

    }



    public function check_product_exists($product, $id) {

        $this->db->where('pdt_code', $product);

        $this->db->where('status != 10');

        #$this->db->where('status != 2');

        if (!empty($id))

            $this->db->where('id !=', $id);

        $getData = $this->db->get($this->product);

        if ($getData->num_rows() > 0)

            return 1;

        else

            return 0;

    }



    public function check_product_desc_exists($brand, $desc, $id) {

        if (!empty($brand))

            $this->db->where('cat_id', $brand);

        else

            $this->db->where('(cat_id = "" OR cat_id IS NULL)');



        //$this->db->where('pdt_code', $pdt_code);



        $this->db->where('pdt_name', $desc);

        $this->db->where('status != 10');

        if (!empty($id))

            $this->db->where('id !=', $id);



        $getData = $this->db->get($this->product);

        return ($getData->num_rows() > 0) ? 1 : 0;

    }



    public function check_barcode_exists($barcode, $id) {

        $this->db->where('barcode', $s_no);

        $this->db->where('status != 10');

        if (!empty($id))

            $this->db->where('id !=', $id);



        $getData = $this->db->get($this->product);

        return ($getData->num_rows() > 0) ? 1 : 0;

    }



    public function check_sno_exists($s_no, $id) {

        $this->db->where('s_no', $s_no);

        $this->db->where('status != 10');

        if (!empty($id))

            $this->db->where('id !=', $id);



        $getData = $this->db->get($this->product);

        return ($getData->num_rows() > 0) ? 1 : 0;

    }



    public function insert_cost_log($data) {

        $this->db->insert($this->products_log, $data);

        return $this->db->insert_id();

    }



    public function insert_brn_material($data) {

        $this->db->insert($this->branch_products, $data);

        return $this->db->insert_id();

    }



    public function update_brn_material($branch_id, $product_id, $data, $brn_item) {

        $this->db->from($this->branch_products);

        $this->db->where('brn_id', $branch_id);

        $this->db->where('product_id', $product_id);

        if (!$this->db->count_all_results()) {

            $this->db->insert($this->branch_products, $brn_item);

            return $this->db->insert_id();

        } else {

            $this->db->where('brn_id', $branch_id);

            $this->db->where('product_id', $product_id);

            $this->db->update($this->branch_products, $data);

        }



        return $this->db->affected_rows();

    }



    public function insert_scan_product($data) {

        $this->db->insert($this->scan_products, $data);

        return $this->db->insert_id();

    }



    public function get_product_last_price_log($pid) {

        $this->db->where('product_id', $pid);

        $this->db->order_by('id', 'desc');

        $this->db->limit('1');

        return $this->db->get($this->products_log)->row();

    }



    public function get_product_page_count() {



        $this->db->select("count(p.id) as pro_id");

        $this->db->where('p.status', '1');

        return $this->db->get($this->product . ' as p')->row();

    }



    public function get_product_page_count_by_category($item_cat, $brand) {

        $this->db->select("count(p.id) as pro_id");

        $this->db->where('p.status', '1');

        if ($item_cat != '') {

            $this->db->where('p.item_category', $item_cat);

        }

        if ($brand != '') {

            $this->db->where('p.cat_id', $brand);

        }



        return $this->db->get($this->product . ' as p')->row();

//        return $this->db->last_query();

    }



    public function get_product_by_category($category_id) {

        $this->db->select('p.id, p.pdt_code, p.pdt_name, p.pdt_spec, p.pdt_description, p.buying_price, p.bfr_gst_buying_price, p.selling_price, b.available_qty, SUM(b.available_qty) as total_available_qty, u.unit_name', FALSE);

        $this->db->join($this->branch_products . ' as b', 'b.product_id = p.id AND b.status = 1', 'left');

        $this->db->join($this->unit . ' as u', 'u.id = p.unit_id', 'left');

        $this->db->where('p.status', 1);

        $this->db->where('p.cat_id', $category_id);

        $this->db->group_by('p.id');

        $this->db->order_by('p.pdt_name');

        return $this->db->get($this->product . ' as p')->result();

    }



    public function update_branch_stock($branch_id, $product_id, $quantity, $op = '-') {

        $update = array(

            'brn_id' => $branch_id,

            'product_id' => $product_id,

            'branch_id' => $this->user_data['branch_id'],

            'created_by' => $this->user_data['user_id'],

            'ip_address' => $this->ip_address

        );



        $this->db->from($this->branch_products);

        $this->db->where('brn_id', $branch_id);

        $this->db->where('product_id', $product_id);

        if (!$this->db->count_all_results()) {

            $amt = 0;

            if ($op == '+') {

                $amt +=$quantity;

            } else {

                $amt -=$quantity;

            }

            //$amt = '0'. $op .$quantity;

            //$amt =intval(intval(0) $op  intval($quantity));

            $update['available_qty'] = $amt;

            $update['created_by'] = $this->user_data['user_id'];

            $update['created_on'] = date('Y-m-d H:i:s');



            $this->db->where('brn_id', $branch_id);

            $this->db->where('product_id', $product_id);

            $this->db->insert($this->branch_products, $update);

        } else {

            $this->db->set('available_qty', 'available_qty ' . $op . $quantity, FALSE);

            $update['updated_by'] = $this->user_data['user_id'];

            $update['updated_on'] = date('Y-m-d H:i:s');

            $this->db->where('brn_id', $branch_id);

            $this->db->where('product_id', $product_id);

            $this->db->update($this->branch_products, $update);

        }



        return $this->db->affected_rows();

    }



    public function list_search_all($brand_id = '') {

        $this->db->select('p.*, CASE p.status WHEN 1 THEN "Paid" WHEN 2 THEN "Unpaid" ELSE "" END as status_str, c.cat_code, c.cat_name', FALSE);

        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');

        $this->db->where('p.status', 1);

        if ((!empty($brand_id))) {

            $this->db->where('p.cat_id', $brand_id);

        }

        $this->db->order_by('c.cat_name');

        return $this->db->get($this->product . ' as p')->result();

    }



    public function get_active_items_price_list($brand_id, $category_id, $sub_category_id) {

        $this->db->select('p.*, CASE p.status WHEN 1 THEN "Paid" WHEN 2 THEN "Unpaid" ELSE "" END as status_str, c.cat_code, c.cat_name, bp.available_qty', FALSE);

        $this->db->join($this->branch_products . ' as bp', 'bp.product_id = p.id AND bp.brn_id = ' . $this->user_data['branch_id'], 'left');

        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');

        $this->db->where('p.status', 1);

        if ((!empty($brand_id))) {

            $this->db->where('p.cat_id', $brand_id);

        }

        if ((!empty($category_id))) {

            $this->db->where('p.item_category', $category_id);

        }

        if ((!empty($sub_category_id))) {

            $this->db->where('p.sub_category', $sub_category_id);

        }

        $this->db->order_by('c.cat_name');

        //$this->db->group_by('p.id');

        return $this->db->get($this->product . ' as p')->result();

    }



    public function subcat_list() {

        $this->db->select('id,sub_category_name');

        $this->db->where('status != 10');

        $this->db->order_by('id', 'desc');

        return $this->db->get($this->sub_cat)->result();

    }



    public function active_sub_category_list() {

        $this->db->select('id, category_id, sub_category_name');

        $this->db->where('status', 1);

        $this->db->order_by('sub_category_name');

        return $this->db->get($this->sub_cat)->result();

    }



    public function get_subcat_list($cat) {

        $this->db->select('id, sub_category_name');

        $this->db->where('status != 10');

        $this->db->where('category_id', $cat);

        $this->db->order_by('id', 'desc');

        return $this->db->get($this->sub_cat)->result();

    }



    public function get_category_items($cat = '', $all_items = '', $brand = '', $page_no = '', $limit = 100) {

        $this->db->select('p.id AS pid, p.cat_id, p.item_category, p.sub_category, p.pdt_code, p.pdt_name, p.pdt_spec, p.selling_price, p.bfr_gst_buying_price, p.buying_price', FALSE);

        $this->db->join($this->item_cat . ' as c', 'c.id = p.item_category', 'left');

        $this->db->where('p.status', 1);

        if (empty($all_items)) {

            if (!empty($brand)) {

                $this->db->where('p.cat_id', $brand);

            }



            if (!empty($cat)) {

                $this->db->where('p.item_category', $cat);

            }

//            $this->db->where('c.id', $cat);

        }



        $this->db->order_by('p.pdt_name');



        $offset = 0;

        if (!empty($page_no)) {

            $offset = ($page_no - 1) * $limit;

        }



        $this->db->limit($limit, $offset);

        return $this->db->get($this->product . ' as p')->result();

    }



}



?>