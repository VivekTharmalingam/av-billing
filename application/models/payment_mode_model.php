<?php

class Payment_Mode_Model extends CI_Model {

    public function __construct() {

        parent::__construct();
        
        $this->payment_mode = TBL_PAYMENT_MODE;

    }

    /* public function list_active() {

        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);

        $this->db->where('status', 1);

        $this->db->order_by('type_name');

        return $this->db->get($this->payment_type)->result();

    } */
    
    public function list_active_payment_mode($exclude_cash = 0) {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status', 1);
		if (!empty($exclude_cash)) {
			$this->db->where('is_cash', 0);
		}
        
        $this->db->order_by('type_name');
        return $this->db->get($this->payment_mode)->result();

    }

    

    public function get_by_id($id) {

        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);

        $this->db->where('id', $id);

        return $this->db->get($this->payment_mode)->row();

    }

    /* public function insert($data) {

        $this->db->insert($this->payment_type, $data);

        return $this->db->insert_id();

    }

    public function update($id, $data){

        $this->db->where('id', $id);

        $this->db->update($this->payment_type, $data);

        return $this->db->affected_rows();

    }

    public function delete($id){

        $this->db->where('id', $id);

        $this->db->delete($this->payment_type);

        return $this->db->affected_rows();

    }

    public function already_exists($type_name, $id){

        $this->db->where('type_name',$type_name);

        $this->db->where('status != 10');

        if(!empty($id))

           $this->db->where('id !=',$id);

       

        $getData = $this->db->get($this->payment_type);

        if($getData->num_rows() > 0)

            return 1;

        else

            return 0;

    }
 */
}

?>