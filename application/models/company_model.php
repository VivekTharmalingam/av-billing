<?php
class Company_Model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->companies = TBL_COMP;
        $this->branches = TBL_BRN;
        $this->bank = TBL_BANK;
    }

    public function list_active() {
        $this->db->select('id, branch_name');
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('branch_name', 'asc');
        return $this->db->get($this->branches)->result();
    }
    
    public function get_active_bank() {
        $this->db->where('status', 1);
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->bank)->result();
    }

    public function get_company_name_by_ids($company_ids) {
        $this->db->select('id, comp_name');
        $this->db->where('status !=', 10);
        $this->db->where_in('id', $company_ids);
        return $this->db->get($this->companies)->result();
    }
    
    public function active_branch_first() {
        $branch_id = $this->user_data['branch_id'];
        $query = 'SELECT id, branch_name, address FROM ' . $this->branches . ' WHERE status = 1 AND id = ' . $branch_id;
        $query .= ' UNION SELECT id, branch_name, address FROM ' . $this->branches . ' WHERE status = 1 AND id != ' . $branch_id;
        return $this->db->query($query)->result();
    }
	
	public function branch_list_active() {
        $this->db->select('id, branch_name, address');
        $this->db->where('status', 1);
        return $this->db->get($this->branches)->result();
    }
	
	public function active_user_branches() {
        $this->db->select('id, branch_name, address');
        $this->db->where('status !=', 10);
        $this->db->where_in('id', $this->user_data['branch_ids']);
        return $this->db->get($this->branches)->result();
    }
	
    public function get_branch_name_by_ids($branch_ids) {
        $this->db->select('id, branch_name, address');
        $this->db->where('status !=', 10);
        $this->db->where_in('id', $branch_ids);
        return $this->db->get($this->branches)->result();
    }
	
	public function get_branch_by_name($branch_name) {
        $this->db->select('*', FALSE);
        $this->db->where('branch_name', $branch_name);
        return $this->db->get($this->branches)->row();
    }
	
    public function list_all() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->companies)->result();
    }

    public function get_company() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'asc');
        return $this->db->get($this->companies)->row();
    }
	
    public function branch_by_id($id) {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        if (!empty($id))
            $this->db->where('id', $id);

        $this->db->order_by('id', 'asc');
        return $this->db->get($this->branches)->row();
    }

    public function get_branch_by_id($id) {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        if (!empty($id))
            $this->db->where('company_id', $id);
		
		$this->db->where('status !=', 10);
        $this->db->order_by('id', 'asc');
        return $this->db->get($this->branches)->result();
    }

    public function list_active_by_company_id($id) {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);

        if (!empty($id)){
            $this->db->where('id', $id);
        }
        $this->db->where('status', 1);
        $this->db->order_by('id', 'asc');
        return $this->db->get($this->branches)->result();
    }

    public function get_last_row() {
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'desc');
        $this->db->limit('1');
        return $this->db->get($this->companies)->row();
    }

    public function insert($data) {
        $this->db->insert($this->companies, $data);
        return $this->db->insert_id();
    }

    public function insert_branches($data) {
        $this->db->insert($this->branches, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->companies, $data);
        return $this->db->affected_rows();
    }

    public function update_branches($update_id, $val) {
        $this->db->where('id', $update_id);
        $this->db->update($this->branches, $val);
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->companies);
        return $this->db->affected_rows();
    }

    public function row_delete($brnId) {
        $this->db->where('id', $brnId);
        $this->db->delete($this->branches);
    }

    public function check_companies_exists($comp, $id) {
        $this->db->where('comp_name', $comp);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        if (!empty($id))
            $this->db->where('id !=', $id);
		
        $getData = $this->db->get($this->companies);
		
        if ($getData->num_rows() > 0)
            return 1;
        else
            return 0;
    }
	
    public function get_company_details() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->companies)->row();
    }
}
?>