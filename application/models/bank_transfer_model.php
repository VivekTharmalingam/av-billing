<?php

class Bank_Transfer_Model extends CI_Model {

    public function __construct(){
        parent::__construct();
        $this->bank = TBL_BANK;		
        $this->bank_transfer = TBL_BANK_TRANSFER;
        $this->payment = TBL_PAYMENT_MODE;
    }    

   
    public function list_all() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->bank_transfer)->result();
    }
    
    public function get_active_bank() {
        $this->db->where('status', 1);
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->bank)->result();
    }
    
    public function insert($data) {
        $this->db->insert($this->bank_transfer, $data);
        return $this->db->insert_id();
    }
    
    public function get_by_id($id) {
        $this->db->select('bt.*, b1.bank_name as bankName1, b1.account_no as accountNo1, b2.bank_name as bankName2, b2.account_no as accountNo2, p.type_name, p.is_cheque', FALSE);
        $this->db->join($this->bank . ' as b1','b1.id = bt.from_bank_id','left');
        $this->db->join($this->bank . ' as b2','b2.id = bt.to_bank_id','left');
        $this->db->join($this->payment . ' as p','p.id = bt.payment_type','left');
        $this->db->where('bt.id',$id);
        return $this->db->get($this->bank_transfer . ' as bt')->row();
    }
    
    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update($this->bank_transfer, $data);
        return $this->db->affected_rows();
    }
}

?>