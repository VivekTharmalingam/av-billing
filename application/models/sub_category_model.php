<?php

class Sub_category_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->sub_cat = TBL_SUB_CAT;
        $this->item_cat = TBL_ICT;
    }

    public function list_all() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->sub_cat)->result();
    }
    
    public function category_list() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->item_cat)->result();
    }

    public function get_by_id($id) {
        $this->db->select('sc.*,c.item_category_name, CASE sc.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->join($this->item_cat . ' as c', 'c.id = sc.category_id', 'left');
        $this->db->where('sc.id', $id);
        return $this->db->get($this->sub_cat . ' as sc')->row();
    }

    public function insert($data) {
        $this->db->insert($this->sub_cat, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->sub_cat, $data);
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->sub_cat);
        return $this->db->affected_rows();
    }

}

?>