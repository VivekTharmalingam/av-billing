<?php

class Supplier_item_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->branch_product = TBL_BRN_PDT;
        $this->product = TBL_PDT;
        $this->category = TBL_CAT;
        $this->branches = TBL_BRN;
        $this->brand = TBL_CAT;
        $this->inventory_adjustment = TBL_INVENTORY_ADJUST;
        $this->purchase_receive = TBL_PR;
        $this->purchase_receive_items = TBL_PRIT;
        $this->supplier = TBL_SUP;
        $this->item_category = TBL_ICT;
        $this->user_data = get_user_data();
    }

    public function list_supplier() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('name', 'asc');
        return $this->db->get($this->supplier)->result();
    }
    
    
    public function get_unique_supplier_lists($sup_id = '') {        
        $this->db->select('p.*,c.cat_code,c.cat_name,ic.item_category_name', FALSE);    
        $this->db->join($this->purchase_receive_items . ' as prit', 'pr.id = prit.pr_id', 'left');  
        $this->db->join($this->product . ' as p', 'p.id = prit.product_id', 'left');         
        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');  
        $this->db->join($this->item_category . ' as ic', 'ic.id = p.item_category', 'left');  
        $this->db->where('prit.status !=', 10);
        $this->db->where('pr.pr_status !=', 10);
        if ((!empty($sup_id))) {
            $this->db->where('pr.sup_id', $sup_id);
        }
        $this->db->group_by('prit.product_id');
        return $this->db->get($this->purchase_receive . ' as pr')->result();
    }
    

}

?>