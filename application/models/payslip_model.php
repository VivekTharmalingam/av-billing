<?php

class Payslip_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->payslip = TBL_PAY;
        $this->employee = TBL_EMP;
		$this->branches = TBL_BRN;
        $this->user_data = get_user_data();
    }

    public function list_all() {
        $this->db->select('*, CASE status WHEN 1 THEN "Paid" WHEN 2 THEN "Unpaid" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->order_by('id', 'desc');
        $this->db->where_in('branch_id', $this->user_data['branch_id']);
        return $this->db->get($this->payslip)->result();
    }

    public function list_employee() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->employee)->result();
    }

    public function list_account() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->account)->result();
    }

    public function update_emp($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->employee, $data);
        return $this->db->affected_rows();
    }

    public function get_by_id($id) {
        $this->db->select('*, CASE status WHEN 1 THEN "Paid" WHEN 2 THEN "Unpaid" ELSE "" END as status_str', FALSE);
        $this->db->where('id', $id);
        return $this->db->get($this->payslip)->row();
    }
	
	public function branch_by_id($id) {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('id', $id);
        return $this->db->get($this->branches)->row();
    }

    public function insert($data) {
        $this->db->insert($this->payslip, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->payslip, $data);
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->expense);
        return $this->db->affected_rows();
    }

    public function row_delete($brnId) {
        $this->db->where('id', $brnId);
        $this->db->delete($this->expense_item);
    }

    public function get_emp_id($id) {
        $this->db->where('id', $id);
        $query = $this->db->get($this->employee);
        return $query->row();
    }

    public function check_emp_exists($emp, $mnth, $year, $id) {
        $this->db->where('employee_id', $emp);
        $this->db->where('month', $mnth);
        $this->db->where('year', $year);
        $this->db->where('status != 10');
        $this->db->where('status != 2');

        if (!empty($id))
            $this->db->where('id !=', $id);
        $getData = $this->db->get($this->payslip);
        if ($getData->num_rows() > 0)
            return 1;
        else
            return 0;
    }

    function get_empby_id($id) {
        $this->db->where('id', $id);
        return $this->db->get($this->employee);
    }

    public function list_search_all($from_date = '', $to_date = '', $status_id = '') {
        $this->db->select('*, CASE status WHEN 1 THEN "Paid" WHEN 2 THEN "Unpaid" ELSE "" END as status_str', FALSE);
        if ((!empty($from_date))) {
            $this->db->where('pay_date >=', date('Y-m-d H:i:s', strtotime($from_date)));
        }
        if ((!empty($to_date))) {
            $this->db->where('pay_date <=', date('Y-m-d 23:59:59', strtotime($to_date)));
        }
        if ((!empty($status_id))) {
            $this->db->where('status', $status_id);
        }
        $this->db->where_in('branch_id', $this->user_data['branch_ids']);
        $this->db->order_by('pay_date');
        $resource = $this->db->get($this->payslip);
        return $resource->result();
    }

}

?>