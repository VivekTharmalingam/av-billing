<?php
class Purchase_Return_Model extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->suppliers            = TBL_SUP; 
        $this->branches             = TBL_BRN;
        $this->purchases            = TBL_PO;             
        $this->purchase_items       = TBL_POIT;   
        $this->items                = TBL_PDT;   
        $this->category = TBL_CAT;
        $this->units                = TBL_UNIT;   
        $this->company              = TBL_COMP;   
        $this->purchase_receive = TBL_PR;
        $this->purchase_receive_items = TBL_PRIT;        
        $this->inventory_product = TBL_INV_PDT;
        $this->branch_products = TBL_BRN_PDT;
        $this->purchase_return    = TBL_PORET;
        $this->purchase_return_items = TBL_PORETI;
    }
	
    public function list_all(){
        #$user_data = get_user_data();
        $this->db->select('por.id, por.return_date, pr.supplier_invoice_no, pr.invoice_date, SUM(pori.returned_quantity) as return_qty');
        $this->db->join($this->purchase_return_items . ' as pori','pori.po_ret_id = por.id AND pori.status != 10');
        $this->db->join($this->purchase_receive . ' as pr','pr.id = por.precv_id');
        $this->db->join($this->branches . ' as brn','brn.id = por.brn_id');
        $this->db->where('por.status !=',10);
        $this->db->order_by('por.id','desc');
        $this->db->group_by('por.id');
//        if($user_data['is_admin']!='1'){
//            $this->db->where_in('por.branch_id',$user_data['branch_ids']);
//        }
        $this->db->where_in('por.branch_id', $this->user_data['branch_id']);
        return $this->db->get($this->purchase_return . ' as por')->result();
    }
    
	public function get_purchase_receive_list() {
        $this->db->select('pr.id, pr.pr_code, DATE_FORMAT(pr.pr_date, "%d/%m/%Y") as pr_date, pr.supplier_invoice_no, DATE_FORMAT(pr.invoice_date, "%d/%m/%Y") as invoice_date, sup.name, sup.address, sup.phone_no, sup.email_id, pr.pr_status, pr.received_status', FALSE);
        #$this->db->join($this->purchase_receive . ' as pr','pr.po_id = po.id','left');
        $this->db->join($this->suppliers . ' as sup','sup.id = pr.sup_id AND sup.status != 10');
        #$this->db->where('po.grn_status',3);
        $this->db->where('pr.pr_status', 1);
        $this->db->where('pr.pr_status !=', 10);        
        $this->db->where_in('pr.branch_id', $this->user_data['branch_id']);
        $this->db->order_by('pr.id', 'desc');
        return $this->db->get($this->purchase_receive . ' as pr')->result(); 
    }
	
    public function get_po_list(){
        $user_data = get_user_data();
        $this->db->select('po.id, po.po_no,sup.name,sup.address,sup.phone_no,sup.email_id, po.pr_status, pr.received_status');
        $this->db->join($this->purchase_receive . ' as pr','pr.po_id = po.id','left');
        $this->db->join($this->suppliers . ' as sup','sup.id = po.sup_id AND sup.status != 10');
        #$this->db->where('po.grn_status',3);
        $this->db->where('po.po_status !=',10);
        $this->db->where('pr.pr_status !=',10);        
        $this->db->having('po.pr_status = 3 OR pr.received_status = 1');
        $this->db->where_in('po.brn_id',$user_data['branch_id']);
        $this->db->group_by('pr.po_id');
        $this->db->order_by('po.id','desc');
        return $this->db->get($this->purchases .' as po')->result(); 
    }
    
    public function get_warehouse_by_po($id){
        $this->db->select('po.*, ware.*,ware.id as wareid');
        $this->db->join($this->warehouses . ' as  ware','ware.id = po.ware_id');
        $this->db->where('po.id',$id);
        $this->db->where('ware.status !=',10);
        return $this->db->get($this->purchases . ' as po')->row();
    }
    
    public function get_material_by_po($po_id){
        $already_returned = ', (SELECT CASE WHEN ISNULL(pori.returned_quantity) THEN "" ELSE SUM(pori.returned_quantity) END as return_quantity FROM ' . $this->purchase_return_items . ' as pori JOIN ' . $this->purchase_return . ' as por ON por.id =  pori.po_ret_id AND pori.status != 10 AND por.status != 10 WHERE pori.product_id = mat.id) as already_returned_qty';
        $this->db->select('pri.product_id,mat.pdt_code, mat.pdt_name,cat.cat_name,SUM(pri.pr_quantity) as pri_quantity'. $already_returned , FALSE);
        $this->db->join($this->purchase_receive_items . ' as pri','pri.pr_id = pr.id AND pri.status != 10','left');
        $this->db->join($this->items . ' as  mat','mat.id = pri.product_id','left');
        $this->db->join($this->category . ' as  cat','cat.id = mat.cat_id','left');
//        $this->db->join($this->units . ' as  unit','unit.id = mat.unit_id');
        $this->db->where('pr.po_id',$po_id);
        $this->db->where('pr.pr_status !=',10);
        $this->db->group_by('pri.product_id');
        return $this->db->get($this->purchase_receive . ' as pr')->result();
    }
    
    public function insert($data){
       $this->db->insert($this->purchase_return,$data);
       return $this->db->insert_id();
    }
    
    public function insert_item($data){
       $this->db->insert($this->purchase_return_items,$data);
       return $this->db->insert_id();
    }
    
    public function get_by_id($id){
       $this->db->select('por.*, brn.branch_name, sup.name, sup.contact_name, sup.address,sup.phone_no,sup.email_id, CASE por.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" END as status_str, pr.pr_code, DATE_FORMAT(pr.pr_date, "%d/%m/%Y") as pr_date, pr.supplier_invoice_no, DATE_FORMAT(pr.invoice_date, "%d/%m/%Y") as invoice_date',FALSE);
       $this->db->join($this->purchase_receive . ' as pr','pr.id = por.precv_id');
       $this->db->join($this->purchase_return_items . ' as pori','pori.po_ret_id = por.id AND pori.status != 10');
       $this->db->join($this->branches . ' as brn','brn.id = por.branch_id');
       $this->db->join($this->suppliers . ' as sup','sup.id = pr.sup_id');
       $this->db->where('por.id', $id);
       return $this->db->get($this->purchase_return . ' as por')->row();
    }
    
    public function get_items_by_por($id){
        $already_returned = ', (SELECT CASE WHEN ISNULL(i.returned_quantity) THEN "" ELSE SUM(i.returned_quantity) END as return_quantity FROM ' . $this->purchase_return_items . ' as i JOIN ' . $this->purchase_return . ' as por ON por.id = i.po_ret_id WHERE i.id != pori.id AND i.status != 10 AND i.product_id = mat.id AND por.status != 10) as already_returned_qty';
        $this->db->select('pori.*, c.cat_name, mat.pdt_code, mat.pdt_name' . $already_returned , FALSE);
        $this->db->join($this->items . ' as mat','mat.id = pori.product_id','LEFT');
        $this->db->join($this->category . ' as c', 'c.id = mat.cat_id', 'LEFT');
        $this->db->where('pori.po_ret_id',$id);
        $this->db->where('pori.status !=',10);
        $this->db->group_by('pori.product_id');
        return $this->db->get($this->purchase_return_items . ' as pori')->result();
    }
    
    public function update($id,$data){
        $this->db->where('id',$id);
        $this->db->update($this->purchase_return,$data);
        return $this->db->affected_rows();
    }
    
    public function update_item($id,$data){
        $this->db->where('id',$id);
        $this->db->update($this->purchase_return_items,$data);
        return $this->db->affected_rows();
    }
    
    public function row_delete_item($id,$item_id) {
       $data = array('status'=>10);
	   $this->db->where('id',$item_id);
       $this->db->update($this->purchase_return_items,$data);
    }
    
    public function get_item_qty_by_mat_id($mat_id,$ware_id){
        $this->db->select('available_qty');
        $this->db->where('brn_id',$ware_id);
        $this->db->where('product_id',$mat_id);
        $this->db->where('status !=',10);
        return $this->db->get($this->branch_products)->row();
    }
    
    public function check_exists_po_return($po_id){
        $this->db->where('po_id',$po_id);
        $this->db->where('status !=',10);
        $getData = $this->db->get($this->purchase_invoice);
        if($getData->num_rows() > 0)
            return 1;
        else
            return 0;
    }
    
    public function get_old_mat_quantity($mat_id,$id){
      $this->db->select('SUM(pori.returned_quantity) as quantity');
      $this->db->where('pori.product_id',$mat_id);
      $this->db->where('pori.id',$id);
      $this->db->where('pori.status !=',10);
      return $this->db->get($this->purchase_return_items .' as pori')->row()->quantity;
    }
    
    /*public function update_warehouse_material_qty($ware_id, $material_id, $quantity, $op = '+') {
        $update = array (
            'comp_id' => $this->user_data['company_id'],
            'updated_by' => $this->user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s'),
            'ip_address' => $this->ip_address
        );
        $this->db->set('available_qty', 'available_qty ' . $op . $quantity, FALSE);
        $this->db->where('ware_id', $ware_id);
        $this->db->where('mat_id', $material_id);
        if ($op == '-') {
            $this->db->where('available_qty >= ', $quantity);
        }
        $this->db->update($this->warehouse_material, $update);
        return $this->db->affected_rows();
    }
    
    public function warehouse_materials($ware_id, $mat) {
        $this->db->select('wm.available_qty', FALSE);
        $this->db->where('wm.ware_id', $ware_id);
        $this->db->where('wm.mat_id', $mat);
        $this->db->where('wm.status', 1);
        return $this->db->get($this->warehouse_material . ' as wm');
    }*/
    
    public function get_product_for_autocomplete($po_id){
        $already_returned = ', (SELECT CASE WHEN ISNULL(pori.returned_quantity) THEN "" ELSE SUM(pori.returned_quantity) END as return_quantity FROM ' . $this->purchase_return_items . ' as pori JOIN ' . $this->purchase_return . ' as por ON por.id =  pori.po_ret_id WHERE por.precv_id = pr.id AND pori.product_id = pri.product_id AND pori.status != 10 AND por.status != 10) as already_returned_qty';
        
        //$sub = ',(SELECT SUM(pri.pr_quantity) FROM ' . $this->purchase_receive_items . ' as pri JOIN ' . $this->purchase_receive . ' as pr ON pr.id = pri.pr_id WHERE pr.po_id = pus.id AND pri.product_id = po.product_id AND pri.status != 10 AND pr.pr_status != 10 AND pri.pr_id ='.$grn.') as pr_quantity';       
        
        $this->db->select('pri.product_id, p.id, p.pdt_code, p.pdt_name, p.pdt_description, p.buying_price,p.bar_code, p.selling_price, cat.id as cat_id, cat.cat_code, cat.cat_name,SUM(pri.pr_quantity) as pr_quantity'. $already_returned , FALSE);
        $this->db->join($this->purchase_receive_items . ' as pri','pri.pr_id = pr.id AND pri.status != 10','left');
        $this->db->join($this->items . ' as  p','p.id = pri.product_id','left');
        $this->db->join($this->category . ' as  cat','cat.id = p.cat_id','left');
        $this->db->where('pr.id',$po_id);
        $this->db->where('pr.pr_status !=',10);
        $this->db->group_by('pri.product_id');
        $this->db->where('p.status', 1);
        $this->db->order_by('p.pdt_name');
        return $this->db->get($this->purchase_receive . ' as pr')->result();
    }
    
    public function get_pr_item($id) {
        $this->db->where('id', $id);
        $this->db->where('status !=', 10);
        return $this->db->get($this->purchase_return_items)->row();
    }
    
    public function delete_pr_item($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->purchase_return_items);
        return $this->db->affected_rows();
    }
    
    public function get_removed_pr_items($pr_id, $product_ids = array()) {
        $this->db->where('po_ret_id', $pr_id);
        if (!empty($product_ids))
            $this->db->where_not_in('product_id', $product_ids);

        return $this->db->get($this->purchase_return_items)->result();
    }
    
}
?>