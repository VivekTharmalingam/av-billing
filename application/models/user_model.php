<?php
class User_Model extends CI_Model {
    public function __construct(){
        parent::__construct();
        	// Don't Remove this query
        $this->db->query('SET SESSION sql_mode = ""');
        $this->users       = TBL_USER;
        $this->branches    = TBL_BRN;
        $this->user_temp   = TBL_USER_TEMP;
        $this->sales_order = TBL_SO;
        $this->quotation = TBL_QUO;
        $this->transfer_stock_grp = TBL_TRANS_STOCK_GRP;
        $this->company     = TBL_COMP;
        $this->item     = TBL_PDT;
        $this->act_log = TBL_ACT_LOG;
        $this->bank = TBL_BANK;
        
        
        /*if ((!empty($this->user_data)) && (!empty($this->user_data['time_zone_offset']))) {
            #error_log('SET time_zone = "' . $this->user_data['time_zone_offset'] . '"');
            $this->db->query('SET time_zone = "' . $this->session->user_data('time_zone_offset') . '"');
        }
        else if (defined('TIME_ZONE')) {
            $target_time_zone = new DateTimeZone(TIME_ZONE);
            $date_time = new DateTime('now', $target_time_zone);
            $this->db->query('SET time_zone = "' . $date_time->format('P') . '"');
        }*/
    }
    public function get_by_company_details(){
        $this->db->select('*');
        $this->db->where('id','1');
        return $this->db->get($this->company)->row();
    }
    
     public function user_list() {
            $this->db->select('user.*, CASE user.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
            $this->db->where('user.status != 10');            
            $this->db->order_by('user.id', 'desc');
            return $this->db->get($this->users . ' AS user')->result_array();
        }
    
	public function active_user_list() {
		$this->db->where('user.status', 1);            
		$this->db->order_by('user.name');
		return $this->db->get($this->users . ' AS user')->result();
	}
	
	public function branch_name_by_id($id) {
            $this->db->select('branch_name');
                    $this->db->where('id', $id);
            return $this->db->get($this->branches)->row()->branch_name;
        }

        public function branch_by_id($id) {
            $this->db->select('*');
            $this->db->where('id', $id);
            return $this->db->get($this->branches)->result_array();
        }
	
    public function next_auto_id($tbl, $branch_id, $prefix = '', $suffix = '') {
		$next_no = '';
		if ((!empty($branch_id)) && ($tbl != TBL_QUO && $tbl != TBL_TRANS_STOCK_GRP)) {
			$this->db->where('branch_id', $branch_id);
			$this->db->where('status !=', 10);
			$this->db->order_by('id', 'desc');
			$so_no = $this->db->get($this->sales_order)->row()->so_no;
			//$branch_name = $this->branch_name_by_id($branch_id);
			#$so_arr = explode('-', $so_no);
			#$next_no = end($so_arr) + 1;
			
			if (!empty($prefix)) {
				$so_no = str_ireplace($prefix, '', $so_no);
			}
			
			if (!empty($suffix)) {
				$so_no = str_ireplace($suffix, '', $so_no);
			}
 
			$so_no = trim($so_no);
			$next_no = $so_no + 1;
		}
		elseif((!empty($branch_id)) && ($tbl == TBL_QUO)){
			$this->db->where('branch_id', $branch_id);
			$this->db->where('status !=', 10);
			$this->db->order_by('id', 'desc');
			$so_no = $this->db->get($this->quotation)->row()->quo_no;
			//$branch_name = $this->branch_name_by_id($branch_id);
			#$so_arr = explode('-', $so_no);
			#$next_no = end($so_arr) + 1;
			if (!empty($prefix)) {
				$so_no = str_ireplace($prefix, '', $so_no);
			}
			
			if (!empty($suffix)) {
				$so_no = str_ireplace($suffix, '', $so_no);
			}
			$so_no = trim($so_no);
			$next_no = $so_no + 1;
		}
		elseif((!empty($branch_id)) && ($tbl == TBL_TRANS_STOCK_GRP)){
			$this->db->where('branch_id', $branch_id);
			$this->db->where('status !=', 10);
			$this->db->order_by('id', 'desc');
			$so_no = $this->db->get($this->transfer_stock_grp)->row()->tracking_no;
			//$branch_name = $this->branch_name_by_id($branch_id);
			#$so_arr = explode('-', $so_no);
			#$next_no = end($so_arr) + 1;
			if (!empty($prefix)) {
				$so_no = str_ireplace($prefix, '', $so_no);
			}
			
			if (!empty($suffix)) {
				$so_no = str_ireplace($suffix, '', $so_no);
			}
			$so_no = trim($so_no);
			$next_no = $so_no + 1;
		}
		else {
			$query = "SHOW TABLE STATUS LIKE '{$tbl}'";
			$next_no = $this->db->query($query)->row()->Auto_increment;
		}
        
        return $next_no;
    }
    
    public function next_auto_item_id($tbl, $prefix = '', $suffix = '') {
		$next_no = '';
		if ($tbl == TBL_PDT) {
			$this->db->where('status !=', 10);
			$this->db->order_by('id', 'desc');
			$so_no = $this->db->get($this->item)->row()->pdt_code;
			if (!empty($prefix)) {
				$so_no = str_ireplace($prefix, '', $so_no);
			}
			
			if (!empty($suffix)) {
				$so_no = str_ireplace($suffix, '', $so_no);
			}
 
			$so_no = trim($so_no);
			$next_no = $so_no + 1;
		}
		else {
			$query = "SHOW TABLE STATUS LIKE '{$tbl}'";
			$next_no = $this->db->query($query)->row()->Auto_increment;
		}
        
        return $next_no;
    }
    
    public function list_all() {
        $this->db->select('user.*,comp.comp_name, CASE user.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->join($this->company . ' AS comp', 'comp.id=user.branch_id', 'left');
        $this->db->where('user.status != 10');
        $this->db->order_by('user.id', 'desc');
        return $this->db->get($this->users . ' AS user')->result();
    }
    
    public function list_temp_all() {
        $this->db->select('utemp.*, comp.comp_name, CASE utemp.approval_status WHEN 1 THEN "No Response" WHEN 2 THEN "Approved" WHEN 3 THEN "Rejected" ELSE "" END as status_str', FALSE);
        $this->db->join($this->company . ' AS comp', 'comp.id=utemp.company_id', 'left');
        $this->db->where('utemp.approval_status != 2');
        $this->db->where('utemp.approval_status != 0');
        $this->db->order_by('utemp.id', 'desc');
        return $this->db->get($this->user_temp . ' AS utemp')->result();
    }
    
    public function update_approval_status($id, $data){
        $this->db->where('id', $id);
        $this->db->update($this->user_temp, $data);
        return $this->db->affected_rows();
    }
    
    public function approve_overall_status($id, $data){
        $this->db->where_in('id', $id);
        $this->db->update($this->user_temp, $data);
        return $this->db->affected_rows();
    }
    
    public function get_by_temp1_list_id($id) {
        $this->db->select('utemp.*, comp.comp_name, CASE utemp.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->join($this->company . ' AS comp', 'comp.id=utemp.company_id', 'left');
        $this->db->where_in('utemp.id', $id);
        return $this->db->get($this->user_temp . ' AS utemp')->row();
    }
    
    public function get_by_temp_list_id($id) {
        $this->db->select('utemp.*, comp.comp_name, CASE utemp.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->join($this->company . ' AS comp', 'comp.id=utemp.company_id', 'left');
        $this->db->where_in('utemp.id', $id);
        return $this->db->get($this->user_temp . ' AS utemp')->result();
    }
    
    public function get_by_id($id) {
        $this->db->where('id', $id);
        return $this->db->get($this->users)->row();
    }
    
    public function reject_update($id, $data){
        $this->db->where('emp_id', $id);
        $this->db->where('status !=', 10);
        $this->db->update($this->user_temp, $data);
        return $this->db->affected_rows();
    }
    
    public function insert($data) {
        $this->db->insert($this->users, $data);
        return $this->db->insert_id();
    }
    
    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update($this->users, $data);
        return $this->db->affected_rows();
    }
    
    public function delete($id){
        $this->db->where('id', $id);
        $this->db->delete($this->users);
        return $this->db->affected_rows();
    }
    
    public function authenticate_user($user_email, $password) {
        $this->db->where('( email = "'.addcslashes($user_email, '"').'" OR user_name = "'.addcslashes($user_email, '"').'" )');
//        $this->db->or_where('user_name', $user_email);
        $this->db->where('password', $password);
        $this->db->where('status', 1);
        return $this->db->get($this->users)->row();
    }
    
    public function authenticate_user_by_name($user_email) {
        $this->db->where('email', $user_email);
        return $this->db->get($this->users)->row();
    }

    public function get_company_name_by_ids($company_ids) {
        $this->db->select('id, comp_name');
        $this->db->where_in('id', $company_ids);
        return $this->db->get($this->company)->result();
    }
    
    public function get_user_permission($id) {
        $this->db->where('id', $id);
        return $this->db->get($this->users)->row()->permissions;
    }
    
    public function email_exists($email, $id) {
        if ($id != '') {
            $this->db->where('id !=', $id);
        }
        
        $this->db->where('user_email', $email);
        return ($this->db->get($this->users)->num_rows() > 0) ? 1 : 0;
    }
    
    public function check_user_name_exists($user_name,$id){
        $this->db->where('user_name',$user_name);
        $this->db->where('status != 10');
        if(!empty($id))
           $this->db->where('id !=',$id);
        $getData = $this->db->get($this->users );
        if($getData->num_rows() > 0)
            return 1;
        else
            return 0;
    }
    
    public function check_email_id_exists($email_id,$id){
        $this->db->where('email',$email_id);
        $this->db->where('status != 10');
        if(!empty($id))
           $this->db->where('id !=',$id);
        $getData = $this->db->get($this->users );
        if($getData->num_rows() > 0)
            return 1;
        else
            return 0;
    }
    
    public function insert_log($data) {
        $this->db->insert($this->act_log, $data);
        return $this->db->insert_id();
    }
    
    public function update_logtxt($data,$control,$view_id) {
        $this->db->where('controller_name', $control);
        $this->db->where('view_id', $view_id);
        $this->db->update($this->act_log, $data);
        return $this->db->affected_rows();
    }
    
    public function get_user_list($user_id) {
        $this->db->where('id !=',$user_id);
        $this->db->where('push_notify',1);
        $this->db->where('status',1);
        return $this->db->get($this->users)->result();
    }
    
    public function get_active_acc($acc_id) {
        $this->db->where('id',$acc_id);
        return $this->db->get($this->bank)->result_array();
    }
}
?>