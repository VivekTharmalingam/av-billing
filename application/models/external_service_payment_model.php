<?php

class External_service_payment_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->suppliers = TBL_SUP;
        $this->service_partners = TBL_SERPTN;
        $this->purchase_receive = TBL_PR;
        $this->exchange_order = TBL_EXR;
        $this->exchange_order_payment = TBL_EXRPM;
        $this->exter_services = TBL_EXTSER;
        $this->services_exter_item = TBL_SEREXTITM;
        $this->external_service_payment = TBL_EXTSERPAY;
        $this->services = TBL_SJST;
        $this->payment_type = TBL_PAYMENT_TYPE;
    }

    public function list_all() {
        $this->db->select('exr.id, DATE_FORMAT(exr.issued_date, "%d/%m/%Y") as issued_date_str, exr.exter_amount, COALESCE(SUM(pay.pay_amt), 0) as paid_amount, CASE exr.exter_pay_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as exter_pay_status, s.name', FALSE);
        $this->db->join($this->exter_services . ' as exr', 'exr.id = pay.external_service_id AND exr.status != 10');
        $this->db->join($this->service_partners . ' as s', 's.id = exr.service_partner_id');
        $this->db->where('pay.status !=', 10);
        $this->db->where_in('pay.brn_id', $this->user_data['branch_id']);
        $this->db->order_by('pay.id', 'desc');
        $this->db->group_by('pay.external_service_id');
        return $this->db->get($this->external_service_payment . ' AS pay')->result();
    }

    public function get_unpaid_purchases_by_service_partner($service_partner_id=NULL,$service_partner_txt=NULL) {
        $this->db->select('exr.id, exr.exter_service_no, DATE_FORMAT(exr.issued_date, "%d/%m/%Y") as issued_date_str, sjst.jobsheet_number, exr.exter_amount, COALESCE(SUM(pay.pay_amt), 0) as paid_amount, CASE exr.exter_pay_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as exter_pay_status', FALSE);
        $this->db->join($this->external_service_payment . ' as pay', 'pay.external_service_id = exr.id AND pay.status != 10', 'left');
        $this->db->join($this->services_exter_item . ' as exitm', 'exitm.external_service_id = exr.id AND exitm.status != 2 AND exitm.status != 10', 'left');
        $this->db->join($this->services . ' as sjst', 'sjst.id = exitm.service_id', 'left');
        $this->db->where('exr.status !=', 10);
        $this->db->where('exr.exter_pay_status !=', 3);
        if($service_partner_id != '') {
        $this->db->where('exr.service_partner_id', $service_partner_id);
        } else {
        $this->db->where('exr.service_partner_name', $service_partner_txt);    
        }
        $this->db->where_in('exr.branch_id', $this->user_data['branch_id']);
        $this->db->order_by('exr.issued_date', 'asc');
        $this->db->group_by('exr.id');
        return $this->db->get($this->exter_services . ' AS exr')->result();
    }
    
    public function other_service_partners_list() {
        $this->db->select('exr.id as id,exr.service_partner_name as name', FALSE);
        $this->db->where('exr.status !=', 10);
        $this->db->where('exr.service_partner_id', 0);
        $this->db->group_by('exr.service_partner_name');
        return $this->db->get($this->exter_services . ' AS exr')->result();
    }
    
    public function insert($data) {
        $this->db->insert($this->external_service_payment, $data);
        return $this->db->insert_id();
    }
    
    public function update_exc_order_payment_status($exr_id, $data) {
        $this->db->where('id', $exr_id);
        $this->db->update($this->exter_services, $data);
        return $this->db->affected_rows();
    }
    
    public function get_by_id($id) {
        $this->db->select('exr.*, DATE_FORMAT(exr.issued_date, "%d/%m/%Y") as issued_date_str, CASE exr.exter_pay_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as exter_pay_status, exr.service_partner_name as name', FALSE);
        $this->db->where('exr.id', $id);
        $this->db->join($this->service_partners . ' as s', 's.id = exr.service_partner_id', 'left');
        return $this->db->get($this->exter_services . ' AS exr')->row();
    }
    
    public function get_payment_by_excord_id($id) {
        $this->db->select('pay.*, DATE_FORMAT(pay.pay_date, "%d-%m-%Y") as payment_date, pt.type_name, pt.is_cheque', FALSE);
        $this->db->join($this->payment_type . ' as pt', 'pt.id = pay.pay_pay_type', 'left');
        $this->db->where('pay.status !=', 10);
        $this->db->where('pay.external_service_id', $id);
        $this->db->where_in('pay.brn_id', $this->user_data['branch_id']);
        $this->db->order_by('pay.id');
        return $this->db->get($this->external_service_payment . ' AS pay')->result();
    }
    
    public function get_payment_by_pay_id($id) {
        $this->db->select('pay.*, exr.exter_service_no, DATE_FORMAT(pay.pay_date, "%d-%m-%Y") as payment_date, pt.type_name, pt.is_cheque', FALSE);
        $this->db->join($this->payment_type . ' as pt', 'pt.id = pay.pay_pay_type', 'left');
        $this->db->join($this->exter_services . ' as exr', 'exr.id = pay.external_service_id', 'left');
        $this->db->where('pay.status !=', 10);
        $this->db->where('pay.id', $id);
        $this->db->where_in('pay.brn_id', $this->user_data['branch_id']);
        $this->db->order_by('pay.id');
        return $this->db->get($this->external_service_payment . ' AS pay')->row();
    }
    
    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->external_service_payment, $data);
        return $this->db->affected_rows();
    }
    
    public function get_excord_pay_by_id($id) {
		$this->db->select('pay.*, pt.type_name, pt.group_name', FALSE);
		$this->db->join($this->payment_type . ' as pt', 'pt.id = pay.pay_pay_type', 'left');
        $this->db->where('pay.id', $id);
        return $this->db->get($this->external_service_payment . ' AS pay')->row();
    }
    
}

?>