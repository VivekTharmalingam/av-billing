<?php
/* 
 * Created By : Vivek T
 * Purpose : Promotion Module - Purchase with Purchase items Model
 * Created On : 2k17-02-18 11:35:00
 */
class Purchase_with_purchase_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->product = TBL_PDT;
        $this->category = TBL_CAT;
        $this->pwp = TBL_PWP;
        $this->pwp_items = TBL_PWP_ITEMS;
    }
    
    public function list_all() {
        $this->db->select('pwp.*,p.pdt_name, CASE pwp.status WHEN 1 THEN "Active" WHEN 2 THEN "In Active" ELSE "" END as status_str', FALSE);
        $this->db->join($this->product . ' as p', 'p.id = pwp.item_id');
        //$this->db->join($this->pwp_items . ' as p_items', 'p_items.pwp_id = pwp.id');
        $this->db->where('pwp.status !=', 10);
        $this->db->order_by('pwp.id', 'desc');
        return $this->db->get($this->pwp . ' as pwp')->result();
    }
    
    public function insert($data) {
        $this->db->insert($this->pwp, $data);
        return $this->db->insert_id();
    }
    
    public function insert_item($data) {
        $this->db->insert($this->pwp_items, $data);
        return $this->db->insert_id();
    }
    
    public function get_by_id($id) {
        $this->db->select('pwp.*,p.pdt_name, CASE pwp.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->join($this->product . ' as p', 'p.id = pwp.item_id');
        $this->db->where('pwp.id', $id);
        return $this->db->get($this->pwp . ' as pwp')->row();
    }
    
    public function get_by_items($id) {
        $this->db->select('pwp_items.*,p.pdt_name, CASE pwp_items.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->join($this->product . ' as p', 'p.id = pwp_items.offer_item_id');
        $this->db->where('pwp_items.pwp_id', $id);
        $this->db->where('pwp_items.status != 10');
        return $this->db->get($this->pwp_items . ' as pwp_items')->result();
    }
    
    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update($this->pwp, $data);
        return $this->db->affected_rows();
    }
    
    public function update_item($id, $data){
        $this->db->where('id', $id);
        $this->db->update($this->pwp_items, $data);
        return $this->db->affected_rows();
    }
    
    public function delete_item($id, $data){
        $this->db->where('pwp_id', $id);
        $this->db->update($this->pwp_items, $data);
        return $this->db->affected_rows();
    }
    
    
    
}