<?php

class Purchase_Order_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->suppliers = TBL_SUP;
        $this->branches = TBL_BRN;
        $this->purchases = TBL_PO;
        $this->purchase_items = TBL_POIT;
        $this->items = TBL_PDT;
        $this->category = TBL_CAT;
        $this->units = TBL_UNIT;
        $this->company = TBL_COMP;
        $this->users   = TBL_USER;
        $this->purchase_receive = TBL_PR;
        $this->purchase_receive_items = TBL_PRIT;
        $this->purchase_payment = TBL_PPM;
        $this->purchase_email_sent = TBL_PO_EMAIL;
        $this->exchange_order = TBL_EXR;
        $this->exchange_order_items = TBL_EXRIT;
        $this->payment_type = TBL_PAYMENT_TYPE;
        $this->user_data = get_user_data();
    }

    public function dummy($args = array()) {
        return $args['id'] . '-' . $args['created_on'];
    }

    public function list_all() {
        $this->db->select('po.id, po.po_no, po.po_date,sup.name,brn.branch_name,po.po_net_amount, CASE po.po_status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->join($this->suppliers . ' as sup', 'sup.id = po.sup_id');
        $this->db->join($this->branches . ' as brn', 'brn.id = po.brn_id');
        $this->db->where('po.po_status !=', 10);
        $this->db->order_by('po.id', 'desc');
        //if($this->user_data['is_admin']!='1'){
        $this->db->where_in('po.brn_id', $this->user_data['branch_id']);
        //}
        return $this->db->get($this->purchases . ' as po')->result();
    }

    public function get_all_suppliers() {
        $this->db->where('status', 1);
        $this->db->order_by('sup.name', 'asc');
        return $this->db->get($this->suppliers . ' as sup')->result();
    }

    public function get_all_branches($cid) {
        $this->db->where('status', 1);
        $this->db->where('branch_id', $cid);
        $this->db->order_by('brn.branch_name', 'asc');
        return $this->db->get($this->branches . ' as brn')->result();
    }

    public function get_supplier_details($id) {
        $this->db->where('id', $id);
        return $this->db->get($this->suppliers . ' as sup')->row();
    }

    public function get_all_items() {
        $this->db->select('itm.*,cat.cat_name');
        $this->db->where('itm.status', 1);
        $this->db->order_by('itm.id', 'desc');
        $this->db->join($this->category . ' as cat', 'cat.id = itm.cat_id', 'left');
        return $this->db->get($this->items . ' as itm')->result();
    }

    public function get_item_details($mat_id) {
        $this->db->select('itm.*,units.unit_name');
        $this->db->where('itm.id', $mat_id);
        $this->db->join($this->units . ' as units', 'units.id = itm.unit_id', 'left');
        return $this->db->get($this->items . ' as itm')->row();
    }

    public function get_company_details($id) {
        $this->db->where('comp.id', $id);
        return $this->db->get($this->company . ' as comp')->row();
    }

    public function get_by_id($id) {
        $this->db->select('po.*,brn.branch_name, comp.comp_name, comp.comp_address, comp.comp_phone, comp.comp_email, CASE po.po_status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str, u.name as uname, sup.gst_type as sup_gst_type, sup.name as supplier_name', FALSE);
        $this->db->join($this->branches . ' as brn', 'brn.id = po.brn_id', 'left');
        $this->db->join($this->company . ' as comp', 'comp.id = brn.company_id', 'left');
        $this->db->join($this->suppliers . ' as sup', 'sup.id = po.sup_id', 'left');
		$this->db->join($this->users . ' as u','u.id = po.created_by');
        $this->db->where('po.id', $id);
        return $this->db->get($this->purchases . ' as po')->row();
    }
    
    public function get_supplier_itemcode($supp_id,$item_id) {
        
        $this->db->select('pri.*');
        $this->db->join($this->purchase_receive . ' as pr', 'pr.id = pri.pr_id and pr.sup_id = "'.$supp_id.'" and pr.pr_status != 10');
        $this->db->where('pri.product_id', $item_id);
        $this->db->where('pri.status', 1);
        $this->db->order_by('pri.id', 'desc');
        $this->db->limit(1);
        $itemcode = $this->db->get($this->purchase_receive_items . ' as pri')->row();
        
        if((count($itemcode)==0) || (count($itemcode)>0) && $itemcode->sl_itm_code=='') {
            $this->db->select('poi.*');
            $this->db->join($this->purchases . ' as po', 'po.id = poi.po_id and po.sup_id = "'.$supp_id.'" and po.po_status != 10');
            $this->db->where('poi.product_id', $item_id);
            $this->db->where('poi.status', 1);
            $this->db->order_by('poi.id', 'desc');
            $this->db->limit(1);
            $itemcode = $this->db->get($this->purchase_items . ' as poi')->row();
        }
        return $itemcode;
    }
    
    public function get_supplier_exc_itemcode($supp_id,$item_id) {
        
        $this->db->select('exri.*');
        $this->db->join($this->exchange_order . ' as exr', 'exr.id = exri.exr_id and exr.sup_id = "'.$supp_id.'" and exr.exr_status != 10');
        $this->db->where('exri.product_text', $item_id);
        $this->db->where('exri.status', 1);
        $this->db->order_by('exri.id', 'desc');
        $this->db->limit(1);
        $itemcode = $this->db->get($this->exchange_order_items . ' as exri')->row();
        
        return $itemcode;
    }

    public function get_item_by_id($id) {
        $this->db->select('poi.*,items.pdt_code,items.pdt_name,items.pdt_description,cat.cat_name');
        $this->db->join($this->items . ' as items', 'items.id = poi.product_id');
        $this->db->join($this->category . ' as cat', 'cat.id = items.cat_id', 'left');
        $this->db->where('poi.po_id', $id);
        $this->db->where('poi.status', 1);
        $this->db->order_by('poi.id', 'desc');
        return $this->db->get($this->purchase_items . ' as poi')->result();
    }
    
    public function get_po_item($po_item_id) {
        $this->db->where('id', $po_item_id);
        $this->db->where('status !=', 10);
        return $this->db->get($this->purchase_items)->row();
    }

    public function insert($data) {
        $this->db->insert($this->purchases, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->purchases, $data);
        return $this->db->affected_rows();
    }
    
    public function update_product($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->items, $data);
        return $this->db->affected_rows();
    }

    public function insert_item($data) {
        $this->db->insert($this->purchase_items, $data);
        return $this->db->insert_id();
    }

    public function update_item($data, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->purchase_items, $data);
        return $this->db->affected_rows();
    }
    
    public function update_po_item($id,$data) {
        $this->db->where_in('po_id', $id);
        $this->db->update($this->purchase_items, $data);
        return $this->db->affected_rows();
    }
    
    public function delete_po_item($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->purchase_items);
        return $this->db->affected_rows();
    }
    
    public function get_removed_po_items($po_id, $product_ids = array()) {
        $this->db->where('po_id', $po_id);
        if (!empty($product_ids))
            $this->db->where_not_in('product_id', $product_ids);
        return $this->db->get($this->purchase_items)->result();
    }

    public function row_delete($id, $item_id) {
        $data = array('status' => 10);
        $this->db->where('id', $item_id);
        $this->db->update($this->purchase_items, $data);
    }

    public function pr_no_list() {
        $rate_sup_count = ', (SELECT count(ras.rate_sup_id) as count_rate_sup FROM ' . $this->rate_analysis_sup . ' as ras WHERE ras.rate_id = ra.id AND ras.is_active= 1 GROUP BY ras.rate_id) as rate_sup_count';
        $po_sup_count = ', (SELECT count(po.sup_id) as count_po_sup FROM ' . $this->purchases . ' as po WHERE po.pr_id = pr.id AND po.status != 10 GROUP BY po.pr_id) as po_sup_count';
        $select = 'pr.id,pr.pr_no' . $rate_sup_count . ',' . $po_sup_count;
        $this->db->select($select, FALSE);
        $this->db->join($this->rate_analysis . ' as ra', 'ra.pr_id = pr.id');
        $this->db->where('pr.status', 2);
        $this->db->where('ra.status', 2);
        #$this->db->where('pr.id NOT IN (SELECT `pr_id` FROM `merp_purchase_orders` WHERE status != 10)', NULL, FALSE);
        #$this->db->where('pr.comp_id',$this->user_data['company_id']);
        #$this->db->where('ra.comp_id',$this->user_data['company_id']);
        $this->db->order_by('pr.id', 'desc');
        $this->db->having('rate_sup_count > po_sup_count OR po_sup_count IS NULL', FALSE);
        return $this->db->get($this->pur_requests . ' as pr')->result();
    }

    function get_bal_amt($args = array()) {
        //echo "<pre>"; print_r( $this->fields_val ); die;
        $fields_val = $this->fields_val;

        return str_replace(',', '', $fields_val['pi_net_amt']) - str_replace(',', '', $fields_val['pay_amt']);
        //return ( str_replace(',', '', $args['pi_net_amt'] ) - ( str_replace( ',', '', $args['pay_amt'] ) + str_replace( ',', '', $args['pi_advance_amt'] ) ) );
    }

    function get_pay_amt($args = array()) {
        $post = $this->input->post();
        $post_fields = $post['data']['fields'];
        $args['pi_advance_amt'] = !empty($args['pi_advance_amt']) ? $args['pi_advance_amt'] : 0;

        $pay_amt = $args['pay_amt'] + $args['pi_advance_amt'];

        //print_r($args); exit;

        if (!empty($post_fields)) :
            $inv_id = $args['id'];

            $this->db->select('SUM(pay_amt)+' . $args['pi_advance_amt'] . ' AS pay_amt');
            $this->db->having('(SUM(pay_amt)+' . $args['pi_advance_amt'] . ') ' . $post_fields['pay_amt_op'] . ' ' . ($post_fields['pay_amt']));
            //$this->db->having( '(SUM(pay_amt)) '. $post_fields['pay_amt_op'].' ' .$post_fields['pay_amt'] );
            $q = $this->db->get_where(TBL_PAY, array('pi_id' => $inv_id, 'status' => 1));
            //echo 'Q:'.$this->db->last_query(); die;
            $pay_amt = $q->row()->pay_amt;
            /* if( 'return eval(' . $pay_amt . $post_fields['pay_amt_op'] . $post_fields['pay_amt'] . '); ' ) :
              return $pay_amt;
              else :
              return '0.00';
              endif; */

//            if( empty( $pay_amt ) || $pay_amt == '0.00' ) :
//                $pay_amt = 'N/A';
//            endif;
//            
            return $pay_amt;
        else :
            return $pay_amt;
        endif;
    }

    public function pr_no_list_edit() {
        $this->db->select('pr.id,pr.pr_no');
        $this->db->join($this->rate_analysis . ' as ra', 'ra.pr_id = pr.id');
        $this->db->where('pr.status', 2);
        $this->db->where('ra.status', 2);
        #$this->db->where('pr.comp_id',$this->user_data['company_id']);
        #$this->db->where('ra.comp_id',$this->user_data['company_id']);
        return $this->db->get($this->pur_requests . ' as pr')->result();
    }

    public function get_warehouse_by_pr($id) {
        $this->db->select('pr.*, ware.*,ware.id as wareid');
        $this->db->join($this->warehouses . ' as  ware', 'ware.id = pr.ware_id');
        $this->db->where('pr.id', $id);
        $this->db->where('ware.status !=', 10);
        return $this->db->get($this->pur_requests . ' as pr')->row();
    }

    public function get_warehouse_list() {
        $this->db->where('status !=', 10);
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->warehouses)->result();
    }

    public function get_ware_by_pr($id) {
        $this->db->select('ware.ware_code,ware.ware_name');
        $this->db->join($this->warehouses . ' as ware', 'ware.id = pr.ware_id');
        $this->db->where('pr.id', $id);
        return $this->db->get($this->pur_requests . ' as pr')->row();
    }

    public function get_supplier_by_pr($id, $po_id) {
        $this->db->select('sup.id,sup.sup_name,sup.sup_code');
        $this->db->join($this->rate_analysis_sup . ' as ras', 'ras.rate_id = ra.id');
        //$this->db->join($this->rate_analysis_mat . ' as ram','ram.rate_id = ra.id');
        $this->db->join($this->suppliers . ' as sup', 'sup.id = ras.rate_sup_id');
        $this->db->where('ra.pr_id', $id);
        //$this->db->where('ram.rate_is_active',1);
        $this->db->where('ras.is_active', 1);
        $this->db->where('ra.status', 2);
        if (empty($po_id))
            $this->db->where('sup.id NOT IN (SELECT `sup_id` FROM `merp_purchase_orders` WHERE pr_id = ' . $id . ' AND status != 10)', NULL, FALSE);

        return $this->db->get($this->rate_analysis . ' as ra')->result();
    }

    public function get_material_by_sup($sup_id, $pr_id) {
        $this->db->select('mat.mat_name,mat.id,mat.mat_code');
        $this->db->join($this->rate_analysis_sup . ' as ras', 'ras.rate_id = ra.id');
        $this->db->join($this->rate_analysis_mat . ' as ram', 'ram.rate_analysis_sup_id = ras.id');
        $this->db->join($this->materials . ' as mat', 'mat.id = ram.rate_mat_id');
        $this->db->where('ram.rate_sup_id', $sup_id);
        $this->db->where('ra.pr_id', $pr_id);
        $this->db->where('ras.is_active', 1);
        $this->db->where('ram.rate_is_active', 1);
        $this->db->where('ra.status !=', 10);
        return $this->db->get($this->rate_analysis . ' as ra')->result();
    }

    public function tax_lists() {
        $this->db->select('id,tax_name,tax_type');
        $this->db->where('status !=', 10);
        return $this->db->get($this->tax)->result();
    }

    public function insert_charges($data) {
        $this->db->insert($this->purchase_charge, $data);
        return $this->db->insert_id();
    }

    public function insert_tax($data) {
        $this->db->insert($this->purchase_taxs, $data);
        return $this->db->insert_id();
    }

    public function get_by_result($id) {
        $this->db->select('po.*,po.id AS po_id,  pr.pr_no,SUM(poi.poit_quantity) AS poit_quantity,SUM(poi.poit_price) AS poit_price,SUM(po.po_net_amt) AS po_net_amt,SUM(po.po_sub_total_amt) AS poit_total_amt,SUM(po.po_taxes_total_amt) AS po_taxes_total_amt,unit.unit_name, mat.mat_name,mat.mat_code, ware.ware_name,sup.sup_name, ware.ware_code, CASE po.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->join($this->pur_requests . ' as pr', 'pr.id = po.pr_id');
        $this->db->join($this->warehouses . ' as ware', 'ware.id = po.ware_id');
        $this->db->join($this->suppliers . ' as sup', 'sup.id = po.sup_id');
        $this->db->join($this->purchase_items . ' as poi', 'poi.po_id = po.id');
        $this->db->join($this->units . ' as unit', 'unit.id = poi.unit_id');
        $this->db->join($this->materials . ' as mat', 'mat.id = poi.mat_id');
        $this->db->where('po.sup_id', $id);
        return $this->db->get($this->purchases . ' as po')->result();
    }

    public function get_charges_by_id($id) {
        $this->db->select('poc.id as poc_id, poc.*, tax.id, tax.tax_name, tax.tax_type');
        $this->db->join($this->tax . ' as tax', 'tax.id = poc.tax_id');
        $this->db->where('poc.po_id', $id);
        $this->db->where('poc.status !=', 10);
        return $this->db->get($this->purchase_charge . ' as poc')->result();
    }

    public function get_tax_type($id) {
        $this->db->select('tax_type');
        $this->db->where('id', $id);
        $this->db->where('status !=', 10);
        return $this->db->get($this->tax)->row();
    }

    public function get_item_vat_by_id($id) {
        $this->db->select('i.vat_type, i.vat, sum(i.vat_amount) as total_vat_amt, sum(i.poit_total_amt) as po_amount, i.poit_total_amt', FALSE);
        $this->db->where('i.po_id', $id);
        $this->db->where('i.status !=', 10);
        $this->db->where('i.vat_type', 1);
        $this->db->group_by('i.vat');
        $this->db->order_by('i.id', 'asc');
        return $this->db->get($this->purchase_items . ' as i')->result();
    }

    public function get_item_vat_amt_by_id($id) {
        $this->db->select('i.vat_type, i.vat, i.poit_total_amt, sum(i.vat_amount) as total_vat_amt, sum(i.poit_total_amt) as po_amount ', FALSE);
        $this->db->where('i.po_id', $id);
        $this->db->where('i.status !=', 10);
        $this->db->where('i.vat_type', 2);
        $this->db->group_by('i.po_id');
        $this->db->order_by('i.id', 'asc');
        return $this->db->get($this->purchase_items . ' as i')->result();
    }

    public function get_tax_by_id($id) {
        $this->db->select('poit.id as poit_id, poit.*, poit.potx_amt, tax.id, tax.tax_name, tax.tax_type');
        $this->db->join($this->tax . ' as tax', 'tax.id = poit.tax_id');
        $this->db->where('poit.po_id', $id);
        $this->db->where('poit.status !=', 10);
        return $this->db->get($this->purchase_taxs . ' as poit')->result();
    }

    public function get_tax_by_id_report($id) {
        $this->db->select('poit.id as poit_id, poit.*, SUM(poit.potx_amt) as potx_amt, tax.id, tax.tax_name, tax.tax_type');
        $this->db->join($this->tax . ' as tax', 'tax.id = poit.tax_id');
        $this->db->where('poit.po_id', $id);
        $this->db->where('poit.status !=', 10);
        return $this->db->get($this->purchase_taxs . ' as poit')->result();
    }

    public function update_charges($data, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->purchase_charge, $data);
        return $this->db->affected_rows();
    }

    public function update_tax($data, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->purchase_taxs, $data);
        return $this->db->affected_rows();
    }

    public function row_delete_tax($id, $tax_id) {
        $data = array('status' => 10);
        $this->db->where('id', $tax_id);
        $this->db->update($this->purchase_taxs, $data);
    }

    public function row_delete_charges($id, $cha_id) {
        $data = array('status' => 10);
        $this->db->where('id', $cha_id);
        $this->db->update($this->purchase_charge, $data);
    }

    public function check_po_exists_grn($po_id) {
        $this->db->where('po_id', $po_id);
        $this->db->where('pr_status !=', 10);
        $getData = $this->db->get($this->purchase_receive);
        if ($getData->num_rows() > 0)
            return 1;
        else
            return 0;
    }

    public function list_search_all_result($from = '', $to = '', $supplier_id = '', $gst = '') {
        /*if (!empty($from) && !empty($to)) {
            $pay = ',(SELECT SUM(p.pay_amt) as paid_amt FROM ' . $this->purchase_payment . ' as p JOIN ' . $this->purchase_receive . ' as precv ON precv.id = p.precv_id WHERE precv.sup_id = pr.sup_id AND p.status != 10 AND (p.pay_date BETWEEN "' . date('Y-m-d H:i:s', strtotime($from)) . '" and "' . date('Y-m-d H:i:s', strtotime($to)) . '") ) as paid_amt';
        }
		else {*/
        $pay = ',(SELECT SUM(p.pay_amt) as paid_amt FROM ' . $this->purchase_payment . ' as p WHERE p.precv_id = pr.id AND p.status != 10) as paid_amt';
        //}
        $this->db->select('SUM(pr.pr_net_amount) as po_amt, SUM(p.pay_amt) as paid_amt, pr.supplier_invoice_no, pr.invoice_date, pr.sup_id, s.name, s.address', FALSE);
        $this->db->join($this->suppliers . ' as s', 's.id = pr.sup_id');
        $this->db->join($this->purchase_payment . ' as p', 'p.precv_id = pr.id AND p.status != 10', 'left');
        $this->db->where('pr.pr_status', 1);
        $this->db->group_by('pr.sup_id');

        if ((!empty($from))) {
            $this->db->where('pr.invoice_date >=', date('Y-m-d H:i:s', strtotime($from)));
        }
        if ((!empty($to))) {
            $this->db->where('pr.invoice_date <=', date('Y-m-d 23:59:59', strtotime($to)));
        }
        if ((!empty($supplier_id))) {
            $this->db->where('pr.sup_id', $supplier_id);
        }
        if ((!empty($gst))) {
            $this->db->where('pr.pr_gst_type', $gst);
        }

        $this->db->order_by('s.name', 'asc');
        return $this->db->get($this->purchase_receive . ' as pr')->result();
    }

    public function get_purchase_by_id($id = '', $from = '', $to = '') {
        $this->db->select('pr.sup_id, pr.pr_code, pr.supplier_invoice_no, pr.invoice_date, pr.pr_net_amount, pr.pr_date, IF(payt.is_cheque = 1 AND pp.pay_pay_no != "", CONCAT(payt.type_name, "&nbsp;/ ", pp.pay_pay_bank, "&nbsp;- ", pp.pay_pay_no), payt.type_name) as type_name, CASE pr.payment_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as status_str, pp.pay_date, SUM(pp.pay_amt) as paid_amt', FALSE);
        $this->db->join($this->purchase_payment . ' as pp', 'pp.precv_id = pr.id AND pp.status != 10', 'left');
        $this->db->join($this->payment_type . ' as payt', 'payt.id = pp.pay_pay_type', 'left');
        $this->db->where('pr.pr_status', 1);
        #$this->db->where('pr.sup_id', $id);
        $this->db->group_by('pr.id');
		$this->db->order_by('pr.id', 'desc');
        if (!empty($from) && !empty($to)) {
            #$this->db->where('(pr.pr_date BETWEEN "' . date('Y-m-d H:i:s', strtotime($from)) . '" and "' . date('Y-m-d H:i:s', strtotime($to)) . '" or pp.pay_date BETWEEN "' . date('Y-m-d H:i:s', strtotime($from)) . '" and "' . date('Y-m-d H:i:s', strtotime($to)) . '")');
			$this->db->where('pr.invoice_date BETWEEN "' . date('Y-m-d H:i:s', strtotime($from)) . '" and "' . date('Y-m-d H:i:s', strtotime($to)) . '"');
        }
        if ((!empty($id))) {
            $this->db->where('pr.sup_id', $id);
        }
		
        return $this->db->get($this->purchase_receive . ' as pr')->result();
    }

    public function get_supplier_by_id($id = '', $from = '', $to = '') {
        /*if (!empty($from) && !empty($to)) {
            $pay = ',(SELECT SUM(p.pay_amt) as paid_amt FROM ' . $this->purchase_payment . ' as p JOIN ' . $this->purchase_receive . ' as pur ON pur.id = p.precv_id WHERE pur.sup_id = pr.sup_id AND p.status != 10 AND (p.pay_date BETWEEN "' . date('Y-m-d H:i:s', strtotime($from)) . '" and "' . date('Y-m-d H:i:s', strtotime($to)) . '") ) as paid_amt';
        } else {*/
            $pay = ',(SELECT SUM(p.pay_amt) as paid_amt FROM ' . $this->purchase_payment . ' as p WHERE p.precv_id = pr.id AND p.status != 10) as paid_amt';
        //}
        $this->db->select('SUM(pr.pr_net_amount) as po_amt, pr.sup_id, s.name, s.contact_name, s.phone_no, s.email_id, s.address' . $pay, FALSE);
        $this->db->join($this->suppliers . ' as s', 's.id = pr.sup_id');
        $this->db->where('pr.pr_status', 1);
        #$this->db->where('po.sup_id', $id);
        $this->db->group_by('pr.sup_id');

        if ((!empty($from))) {
            $this->db->where('pr.invoice_date >=', date('Y-m-d H:i:s', strtotime($from)));
        }
        if ((!empty($to))) {
            $this->db->where('pr.invoice_date <=', date('Y-m-d 23:59:59', strtotime($to)));
        }
        if ((!empty($id))) {
            $this->db->where('pr.sup_id', $id);
        }

        $this->db->order_by('s.id', 'desc');
        return $this->db->get($this->purchase_receive . ' as pr')->row();
    }

    public function list_search_all($from_date = '', $to_date = '', $sup = '', $inv = '', $pay = '') {
        $paid = ', (SELECT sum(pay.pay_amt) FROM ' . $this->purchase_payment . ' as pay WHERE pay.po_id = pur.id AND pay.status != 10) as paid_amt';
        $this->db->select('pur.*, CASE pur.pr_status WHEN 1 THEN "Unfulfilled" WHEN 2 THEN "Started" WHEN 3 THEN "Fulfilled" ELSE "Deleted" END as inv_sts,CASE pur.payment_status WHEN 1 THEN "Un Paid" WHEN 2 THEN "Part Paid" WHEN 3 THEN "Paid" ELSE "Deleted" END as pay_sts,sup.name' . $paid, FALSE);
        $this->db->select("DATE_FORMAT(pur.po_date, '%d/%m/%Y') as po_date_str", FALSE);
        if ((!empty($from_date))) {
            $this->db->where('pur.po_date >=', date('Y-m-d H:i:s', strtotime($from_date)));
        }
        if ((!empty($to_date))) {
            $this->db->where('pur.po_date <=', date('Y-m-d H:i:s', strtotime($to_date)));
        }
        if ((!empty($sup))) {
            $this->db->where('pur.sup_id', $sup);
        }
        if ((!empty($inv))) {
            $this->db->where('pur.pr_status', $inv);
        }
        if ((!empty($pay))) {
            $this->db->where('pur.payment_status', $pay);
        }
        $this->db->where_in('pur.brn_id', $this->user_data['branch_ids']);
        $this->db->where('pur.po_status', 1);
        $this->db->order_by('name');
        $this->db->group_by('pur.id');
        $this->db->join($this->purchase_payment . ' as pp', 'pp.po_id = pur.id', 'left');
        $this->db->join($this->purchase_receive . ' as pr', 'pr.po_id = pur.id', 'left');
        $this->db->join($this->suppliers . ' as sup', 'sup.id = pur.sup_id');
        return $this->db->get($this->purchases . ' AS pur')->result();
    }
    
    /*public function get_current_address($id) {
        $this->db->where('id', $id);
        return $this->db->get($this->branches)->row();
    }*/
	
    public function supplier_address_by_id($id) {
        $this->db->where('id', $id);
        return $this->db->get($this->suppliers)->row();
    }
	
	public function insert_purchase_email_sent($data) {
		$this->db->insert($this->purchase_email_sent, $data);
        return $this->db->insert_id();
	}
}
?>