<?php

class Delivery_Order_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->delivery_order = TBL_DO;
        $this->delivery_order_item = TBL_DO_ITM;
        $this->sales_order = TBL_SO;
        $this->sales_order_item = TBL_SO_ITM;
        $this->items = TBL_PDT;
        $this->category = TBL_CAT;
        $this->ship_address = TBL_SHIP_ADR;
        $this->company = TBL_COMP;
        $this->customer = TBL_CUS;
        //$this->inventory_product = TBL_INV_PDT;
        $this->branch_products = TBL_BRN_PDT;
        $this->exchange_order_items = TBL_EXRIT;
        $this->user_data = get_user_data();
    }

    public function list_all() {
        $user_data = get_user_data();
        $sales_quantity = '(SELECT SUM(quantity) FROM ' . $this->sales_order_item . ' as soi WHERE soi.so_id = do.so_id) as so_quantity';
        $this->db->select('do.id, do.so_id, so.so_no, so.so_date, CASE so.status WHEN 1 THEN "New" WHEN 2 THEN "Partially Delivered" WHEN 3 THEN "Delivered" ELSE "" END as status_str, CASE so.so_type WHEN 1 THEN c.name ELSE so.customer_name END as name, SUM(quantity) as do_quantity, ' . $sales_quantity, FALSE);
        $this->db->join($this->sales_order . ' as so', 'so.id = do.so_id', 'left');
        $this->db->join($this->delivery_order_item . ' as doi', 'doi.do_id = do.id', 'left');
        $this->db->join($this->customer . ' as c', 'c.id = so.customer_id', 'left');
        $this->db->where('do.status !=', 10);
        $this->db->group_by('do.so_id');
//        if ($user_data['is_admin'] != '1') {
//            $this->db->where_in('do.branch_id', $user_data['branch_ids']);
//        }        
        $this->db->where_in('do.branch_id', $this->user_data['branch_id']);
        $this->db->order_by('do.so_id', 'desc');
        return $this->db->get($this->delivery_order . ' as do')->result();
    }
	
	public function get_by_invoice_id($inv_id) {
		$this->db->select('do.so_id, GROUP_CONCAT(do.id SEPARATOR ",") as do_id, GROUP_CONCAT(DISTINCT do.do_no SEPARATOR " / ") as do_no, GROUP_CONCAT(DATE_FORMAT(do.do_date, "%d-%m-%Y") SEPARATOR " / ") as do_date, so.status, CASE so.status WHEN 1 THEN "New" WHEN 2 THEN "Partially Delivered" WHEN 3 THEN "Delivered" ELSE "" END as status_str', FALSE);
        $this->db->join($this->delivery_order . ' as do', 'do.so_id = so.id AND do.status != 10');
        $this->db->where('so.id', $inv_id);
        return $this->db->get($this->sales_order . ' as so')->row();
	}
	
	public function delivered_items_by_inv_id($inv_id) {
            $sales_order = $this->get_soqty_by_id($inv_id);
            $this->db->join($this->delivery_order_item . ' as doi', 'doi.do_id = do.id', 'left');
            $this->db->join($this->sales_order_item . ' as soi', 'soi.so_id = do.so_id AND soi.item_id = doi.item_id');
            if($sales_order->invoice_from == 3) {
            $this->db->select('soi.item_id, soi.product_description, soi.quantity as so_quantity, SUM(doi.quantity) as quantity, exr.product_text as pdt_code, exr.product_description as pdt_description');
            $this->db->join($this->exchange_order_items . ' as exr', 'exr.id = doi.item_id');
            } else {
            $this->db->select('soi.item_id, soi.product_description, soi.quantity as so_quantity, SUM(doi.quantity) as quantity, i.pdt_code');
            $this->db->join($this->items . ' as i', 'i.id = doi.item_id');    
            }
            $this->db->where('do.so_id', $inv_id);
            $this->db->where('do.status !=', 10);
            $this->db->group_by('doi.item_id');
            return $this->db->get($this->delivery_order . ' as do')->result();
	}
        
	public function delivered_items_by_inv_allid($inv_id) {
            $this->db->select('soi.item_id, soi.product_description, soi.quantity as so_quantity, SUM(doi.quantity) as quantity, soi.so_item_codetxt as pdt_code');
            $this->db->join($this->delivery_order_item . ' as doi', 'doi.do_id = do.id', 'left');
            $this->db->join($this->sales_order_item . ' as soi', 'soi.so_id = do.so_id AND soi.item_id = doi.item_id');
            $this->db->where('do.so_id', $inv_id);
            $this->db->where('do.status !=', 10);
            $this->db->group_by('doi.item_id');
            return $this->db->get($this->delivery_order . ' as do')->result();
	}
	
    public function get_by_id($id) {
        $this->db->select('do.id, do.do_no, DATE_FORMAT(do.do_date, "%d-%m-%Y") as do_date, do.sub_total, do.gst, do.gst_amount, do.gst_percentage, do.total_amt, do.status, CASE do.status WHEN 1 THEN "New" WHEN 2 THEN "Partially Delivered" WHEN 3 THEN "Delivered" ELSE "" END as status_str', FALSE);
        //$this->db->join($this->customer . ' as c', 'c.id = so.customer_id');
        $this->db->where('do.id', $id);
        return $this->db->get($this->delivery_order . ' as do')->row();
    }
    
    public function get_by_id_withInv($id) {
        $this->db->select('do.id, do.do_no, so.invoice_from, so.exchange_order_id, DATE_FORMAT(do.do_date, "%d-%m-%Y") as do_date, do.sub_total, do.gst, do.gst_amount, do.gst_percentage, do.total_amt, do.status, CASE do.status WHEN 1 THEN "New" WHEN 2 THEN "Partially Delivered" WHEN 3 THEN "Delivered" ELSE "" END as status_str', FALSE);
        $this->db->join($this->sales_order . ' as so', 'so.id = do.so_id');
        $this->db->where('do.id', $id);
        return $this->db->get($this->delivery_order . ' as do')->row();
    }

    public function get_soqty_by_id($id) {
        $sales_quantity = '(SELECT SUM(quantity) FROM ' . $this->sales_order_item . ' as soi WHERE soi.so_id = so.id) as so_quantity';
        $this->db->select('so.id, so.so_no, so.invoice_from, so.exchange_order_id, DATE_FORMAT(so.so_date, "%d-%m-%Y") as so_date, so.sub_total, so.gst, so.gst_amount,' . $sales_quantity, FALSE);
        $this->db->where('so.id', $id);
        return $this->db->get($this->sales_order . ' as so')->row();
    }

    public function get_doqty_by_id($do_id) {
        $this->db->select('do.id, do.do_no,do.so_id, do.do_date,SUM(doit.quantity) as qty,doit.total,doit.price, doit.item_id, p.pdt_code, p.pdt_name,p.selling_price, p.pdt_description, doit.category_id, c.cat_code, c.cat_name');
        $this->db->join($this->delivery_order_item . ' as doit', 'doit.do_id = do.id', 'left');
        $this->db->join($this->items . ' as p', 'p.id = doit.item_id', 'left');
        $this->db->join($this->category . ' as c', 'c.id = doit.category_id', 'left');
        $this->db->where('do.so_id', $do_id);
        $this->db->where('do.status !=', 10);

        return $this->db->get($this->delivery_order . ' as do')->row();
    }

    public function get_products_by_po($do_id) {
        $this->db->select('do.id, do.do_no,do.so_id, do.do_date,SUM(doit.quantity) as qty,doit.total,doit.price, doit.item_id, p.pdt_code, p.pdt_name,p.selling_price, p.pdt_description, doit.category_id, c.cat_code, c.cat_name');
        $this->db->join($this->delivery_order_item . ' as doit', 'doit.do_id = do.id', 'left');
        $this->db->join($this->items . ' as p', 'p.id = doit.item_id', 'left');
        $this->db->join($this->category . ' as c', 'c.id = doit.category_id', 'left');
        $this->db->where('do.so_id', $do_id);
        $this->db->group_by('doit.category_id');
        $this->db->group_by('doit.item_id');
        $this->db->where('do.status !=', 10);

        return $this->db->get($this->delivery_order . ' as do')->result();
    }
	
    public function items_by_do_id($do_id) {
        $sales_order = $this->get_by_id_withInv($do_id);
        $this->db->join($this->delivery_order_item . ' as doi', 'doi.do_id = do.id', 'left');
        $this->db->join($this->sales_order_item . ' as soi', 'soi.so_id = do.so_id AND soi.item_id = doi.item_id', 'left');
        if($sales_order->invoice_from == 3) {
        $this->db->select('soi.item_id, soi.product_description, soi.quantity, doi.quantity as delivered_qty, exr.product_text as pdt_code, exr.product_description as pdt_description', FALSE);
        $this->db->join($this->exchange_order_items . ' as exr', 'exr.id = soi.item_id', 'left');
        } else {
        $this->db->select('soi.item_id, soi.product_description, soi.quantity, doi.quantity as delivered_qty, i.pdt_code', FALSE);
        $this->db->join($this->items . ' as i', 'i.id = doi.item_id', 'left');    
        }
        $this->db->where('do.id', $do_id);
        $this->db->where('do.status !=', 10);

        return $this->db->get($this->delivery_order . ' as do')->result();
    }
    
    public function items_by_alldo_id($do_id) {
        $this->db->select('soi.item_id, soi.product_description, soi.quantity, doi.quantity as delivered_qty, soi.so_item_codetxt as pdt_code', FALSE);
        $this->db->join($this->delivery_order_item . ' as doi', 'doi.do_id = do.id', 'left');
        $this->db->join($this->sales_order_item . ' as soi', 'soi.so_id = do.so_id AND soi.item_id = doi.item_id', 'left');
        $this->db->where('do.id', $do_id);
        $this->db->where('do.status !=', 10);
        return $this->db->get($this->delivery_order . ' as do')->result();
    }

    public function get_products_details($do_id) {
        $this->db->select('do.id, do.do_no,do.so_id, do.do_date,doit.quantity,doit.total,doit.price, doit.item_id, p.pdt_code, p.pdt_name,p.selling_price, p.pdt_description, doit.category_id, c.cat_code, c.cat_name');
        $this->db->join($this->delivery_order_item . ' as doit', 'doit.do_id = do.id', 'left');
        $this->db->join($this->items . ' as p', 'p.id = doit.item_id', 'left');
        $this->db->join($this->category . ' as c', 'c.id = doit.category_id', 'left');
        $this->db->where('do.so_id', $do_id);
        $this->db->where('do.status !=', 10);

        return $this->db->get($this->delivery_order . ' as do')->result();
    }

    /* public function get_products_by_po($do_id){
      $this->db->select('doit.id, doit.price, doit.unit_cost, doit.quantity, doit.total, doit.item_id, p.pdt_code, p.pdt_name, p.pdt_description, doit.category_id, c.cat_code, c.cat_name');
      $this->db->join($this->items . ' as p','p.id = doit.item_id', 'left');
      $this->db->join($this->category . ' as c','c.id = doit.category_id', 'left');
      $this->db->where('doit.do_id', $do_id);
      $this->db->where('doit.status !=', 10);
      return $this->db->get($this->delivery_order_item . ' as doit')->result();
      } */

    public function undelivered_sales_orders() {
        $this->db->select('SUM(soi.quantity) as so_quantity', FALSE);
        $this->db->select('SUM(doi.quantity) as do_quantity', FALSE);
        $this->db->select('so.id, so.so_no', FALSE);
        #$this->db->join($this->customer . ' as c', 'c.id = so.customer_id');
        $this->db->join($this->sales_order_item . ' as soi', 'soi.so_id = so.id', 'left');
        $this->db->join($this->delivery_order . ' as do', 'do.so_id = so.id', 'left');
        $this->db->join($this->delivery_order_item . ' as doi', 'doi.do_id = do.id AND doi.item_id = soi.item_id', 'left');
        $this->db->where_not_in('so.status', array(10));
        $this->db->where('so.invoice_from != 4');
        $this->db->where_in('so.branch_id', $this->user_data['branch_id']);
        $this->db->group_by('so.id');
        $this->db->order_by('so.id', 'desc');
        $this->db->having('so_quantity > do_quantity OR do_quantity IS NULL');
        
        return $this->db->get($this->sales_order . ' as so')->result();
    }

    public function item_category_by_so_id($so_id) {
        $delivered_qty = '(SELECT SUM(doi.quantity) FROM ' . $this->delivery_order . ' as do ';
        $delivered_qty .= ' JOIN ' . $this->delivery_order_item . ' as doi ON doi.do_id = do.id';
        $delivered_qty .= ' WHERE do.status != 10 AND do.so_id = ' . $so_id . ' AND doi.category_id = soi.category_id) as delivered_qty';
        $this->db->select('soi.category_id, c.cat_code, c.cat_name, ' . $delivered_qty, FALSE);
        //$this->db->join($this->items . ' as p', 'p.id = soi.item_id', 'left');
        $this->db->join($this->category . ' as c', 'c.id = soi.category_id', 'left');
        $this->db->where('soi.so_id', $so_id);
        $this->db->group_by('soi.category_id');
        return $this->db->get($this->sales_order_item . ' as soi')->result();
    }

    public function items_by_so_id($so_id, $category_id = '') {
        $sales_order = $this->get_soqty_by_id($so_id);
        $delivered_qty = '(SELECT SUM(doi.quantity) FROM ' . $this->delivery_order . ' as do ';
        $delivered_qty .= ' JOIN ' . $this->delivery_order_item . ' as doi ON doi.do_id = do.id ';
        $delivered_qty .= ' WHERE do.status != 10 AND do.so_id = ' . $so_id . ' AND doi.item_id = soi.item_id) as delivered_qty';
        if($sales_order->invoice_from == 3) {
        $this->db->select('soi.id, soi.price, soi.unit_cost, soi.quantity, soi.discount, soi.discount_type, soi.discount_amount, soi.total, soi.item_id, soi.product_description, exr.product_text as pdt_code, exr.product_description as pdt_description,soi.category_id AS cid,' . $delivered_qty, FALSE);
        $this->db->join($this->exchange_order_items . ' as exr', 'exr.id = soi.item_id', 'left');
        } else {
        $this->db->select('soi.id, soi.price, soi.unit_cost, soi.quantity, soi.discount, soi.discount_type, soi.discount_amount, soi.total, soi.item_id, soi.product_description, p.pdt_code, p.pdt_name,c.cat_name,c.id AS cid,' . $delivered_qty, FALSE);
        $this->db->join($this->items . ' as p', 'p.id = soi.item_id', 'left');
        $this->db->join($this->category . ' as c', 'c.id = soi.category_id', 'left');
        }
        $this->db->where('soi.so_id', $so_id);
        if (!empty($category_id))
            $this->db->where('soi.category_id', $category_id);
		
        return $this->db->get($this->sales_order_item . ' as soi')->result();
    }

    public function items_by_allso_id($so_id, $category_id = '') {
        $delivered_qty = '(SELECT SUM(doi.quantity) FROM ' . $this->delivery_order . ' as do ';
        $delivered_qty .= ' JOIN ' . $this->delivery_order_item . ' as doi ON doi.do_id = do.id ';
        $delivered_qty .= ' WHERE do.status != 10 AND do.so_id = ' . $so_id . ' AND doi.item_id = soi.item_id) as delivered_qty';
        $this->db->select('soi.id, soi.price, soi.unit_cost, soi.quantity, soi.discount, soi.discount_type, soi.discount_amount, soi.total, soi.item_id, soi.product_description, soi.so_item_codetxt as pdt_code, soi.category_id AS cid,' . $delivered_qty, FALSE);
        $this->db->where('soi.so_id', $so_id);
        if (!empty($category_id))
            $this->db->where('soi.category_id', $category_id);
		
        return $this->db->get($this->sales_order_item . ' as soi')->result();
    }
    
    public function get_branch_list() {
        $this->db->where('status', 1);
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->branches)->result();
    }

    public function get_products_by_so($so_id) {
        $this->db->select('soit.id, soit.price, soit.unit_cost, soit.quantity, soit.discount, soit.discount_type, soit.discount_amount, soit.total, soit.item_id, p.pdt_code, p.pdt_name, soit.category_id, c.cat_code, c.cat_name');
        $this->db->join($this->items . ' as p', 'p.id = soit.item_id', 'left');
        $this->db->join($this->category . ' as c', 'c.id = soit.category_id', 'left');
        $this->db->where('soit.so_id', $so_id);
        $this->db->where('soit.status !=', 10);
        return $this->db->get($this->sales_order_item . ' as soit')->result();
    }

    public function shipping_address_by_id($address_id) {
        $this->db->where('id', $address_id);
        return $this->db->get($this->ship_address)->row();
    }

    public function get_item_details($mat_id, $so_id) {
        $this->db->select('soit.*');
        $this->db->where('soit.product_id', $mat_id);
        $this->db->where('soit.so_id', $so_id);
        return $this->db->get($this->purchase_items . ' as soit')->row();
    }

    public function check_mat_already_exists($mat_id, $so_id) {
        $this->db->select('SUM(prit.pri_quantity) as act_quantity');
        $this->db->join($this->purchase_receive_items . ' as prit', 'prit.pr_id = pr.id', 'left');
        $this->db->where('prit.product_id', $mat_id);
        $this->db->where('pr.so_id', $so_id);
        $this->db->where('pr.pr_status !=', 10);
        return $this->db->get($this->purchase_receive . ' as pr')->row();
    }

    public function insert($data) {
        $this->db->insert($this->delivery_order, $data);
        return $this->db->insert_id();
    }

    public function insert_item($data) {
        $this->db->insert($this->delivery_order_item, $data);
        return $this->db->insert_id();
    }

    public function branch_materials($brn, $prt) {
        $this->db->select('bp.available_qty', FALSE);
        $this->db->where('bp.brn_id', $brn);
        $this->db->where('bp.product_id', $prt);
        $this->db->where('bp.status', 1);
        return $this->db->get($this->branch_products . ' as bp');
    }

    public function delivery_orders_by_so($id) {
        $this->db->select('do.id, do.so_id');
        $this->db->where('do.so_id', $id);
        return $this->db->get($this->delivery_order . ' as do')->result();
    }

    public function insert_brn_material($data) {
        $this->db->insert($this->branch_products, $data);
        return $this->db->insert_id();
    }

    public function update_items($data, $id) {
        $this->db->where('do_id', $id);
        $this->db->update($this->delivery_order_item, $data);
        return $this->db->affected_rows();
    }
    
    public function update_item($data, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->sales_order_item, $data);
        return $this->db->affected_rows();
    }

    public function delete_so_item($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->sales_order_item);
        return $this->db->affected_rows();
    }

    public function update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->delivery_order, $data);
        return $this->db->affected_rows();
    }
    
    public function update_soid($data, $id) {
        $this->db->where('so_id', $id);
        $this->db->update($this->delivery_order, $data);
        return $this->db->affected_rows();
    }

    public function get_so_item($so_item_id) {
        $this->db->where('id', $so_item_id);
        $this->db->where('status !=', 10);
        return $this->db->get($this->sales_order_item)->row();
    }

}

?>