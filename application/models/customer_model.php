<?php

class Customer_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->customer = TBL_CUS;
        $this->ship_address = TBL_SHIP_ADR;
        $this->cust_group = TBL_CUST_GROUP;
        $this->payment_type = TBL_PAYMENT_TYPE;
    }

    public function list_active() {
        $this->db->select('id, name, contact_name, phone_no, address, payment_term');
        $this->db->where('status', 1);
        $this->db->order_by('name', 'asc');
        return $this->db->get($this->customer)->result();
    }
    public function list_customer_group() {
        $this->db->where('status', 1);
        return $this->db->get($this->cust_group)->result();
    }

    public function list_all() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->customer)->result();
    }

    public function get_address_by_id($id) {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('customer_id', $id);
        $this->db->where('status', 1);
        return $this->db->get($this->ship_address)->result();
    }

    public function get_by_id($id) {
        $this->db->select('cus.*, pt.type_name, CASE cus.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str,grp.customer_group AS cust_group', FALSE);
        $this->db->join($this->cust_group . ' as grp', 'grp.id = cus.group_id', 'left');
        $this->db->join($this->payment_type . ' as pt','pt.id = cus.payment_term','left'); 
        $this->db->where('cus.id', $id);
        return $this->db->get($this->customer.' AS cus')->row();
    }

    public function get_last_row() {
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'desc');
        $this->db->limit('1');
        return $this->db->get($this->companies)->row();
    }

    public function insert($data) {
        $this->db->insert($this->customer, $data);
        return $this->db->insert_id();
    }

    public function insert_ship_address($data) {
        $this->db->insert($this->ship_address, $data);
        return $this->db->insert_id();
    }

    public function reset_customer_default_address($customer_id, $id, $data) {
        $this->db->where('customer_id', $customer_id);
        $this->db->where_not_in('id', array($id));
        $this->db->update($this->ship_address, $data);
        return $this->db->affected_rows();
    }

    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->customer, $data);
        return $this->db->affected_rows();
    }

    public function update_address($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->ship_address, $data);
        return $this->db->affected_rows();
    }

    public function delete_address($id) {
        $this->db->where('customer_id', $id);
        $this->db->delete($this->ship_address);
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->companies);
        return $this->db->affected_rows();
    }

    public function check_customer_exists($customer, $id) {
        $this->db->where('name', $customer);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        if (!empty($id))
            $this->db->where('id !=', $id);
        $getData = $this->db->get($this->customer);
        if ($getData->num_rows() > 0)
            return 1;
        else
            return 0;
    }

    public function list_search_all($from_date = '', $to_date = '',$grp = '') {
        $this->db->select('cus.*, CASE cus.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str,grp.customer_group AS cust_group', FALSE);
        if(!empty($grp)){
            $this->db->where('grp.id',$grp);
        }
        if ((!empty($from_date))) {
            $this->db->where('cus.created_on >=', date('Y-m-d H:i:s', strtotime($from_date)));
        }
        if ((!empty($to_date))) {
            $this->db->where('cus.created_on <=', date('Y-m-d 23:59:59', strtotime($to_date)));
        }
        
        $this->db->join($this->cust_group . ' as grp', 'grp.id = cus.group_id', 'left');
        $this->db->where('cus.status', 1);
        $this->db->order_by('cus.name');
        return $this->db->get($this->customer.' AS cus')->result();
    }

}

?>