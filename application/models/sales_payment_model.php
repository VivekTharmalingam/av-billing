<?php
class Sales_Payment_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->suppliers = TBL_SUP;
        $this->purchases = TBL_PO;
        $this->sales_payment = TBL_SPM;
        $this->customer = TBL_CUS;
        $this->sales_order = TBL_SO;
        $this->sales_order_item = TBL_SO_ITM;
        $this->payment_type = TBL_PAYMENT_TYPE;
        $this->user_data = get_user_data();
    }

    public function list_all() {
        $user_data = get_user_data();
        $this->db->select('pay.*, SUM(pay.pay_amt) as paid_amount,so.so_no,CASE pay.payment_status WHEN 1 THEN "Paid" WHEN 2 THEN "Not Paid" WHEN 3 THEN "Cancelled" WHEN 4 THEN "Partially Paid" ELSE "" END as status_str, CASE so.so_type WHEN 1 THEN c.name ELSE so.customer_name END as name', FALSE);
        $this->db->join($this->sales_order . ' as so', 'so.id = pay.so_id');
        $this->db->join($this->customer . ' as c', 'c.id = so.customer_id', 'left');
        $this->db->where('pay.status != 10');
        $this->db->order_by('pay.id', 'desc');
        $this->db->group_by('pay.so_id');
//        if($user_data['is_admin']!='1'){
//            $this->db->where_in('pay.brn_id',$user_data['branch_ids']);
//        }
        $this->db->where_in('pay.brn_id', $this->user_data['branch_id']);
        return $this->db->get($this->sales_payment . ' AS pay')->result();
    }

    public function list_sales_order_no($id=NULL) {
        $this->db->select('so.*,cus.name', FALSE);
        $this->db->join($this->customer . ' as cus', 'cus.id = so.customer_id', 'left');
        $this->db->where('so.status != 10'); 
        if(!empty($id)){
          $this->db->where('(so.payment_status != 3 OR so.id='.$id.')');
        }else{
          $this->db->where('so.payment_status != 3');  
        }
        $this->db->where_in('so.branch_id', $this->user_data['branch_id']);
        $this->db->where('so.id NOT IN (SELECT so_id FROM ' . $this->sales_payment . ' WHERE status != 10 and payment_status = 1)');
		$this->db->order_by('so.id', 'desc');
        return $this->db->get($this->sales_order . ' AS so')->result();
    }
    
    public function get_unpaid_invice_by_customer($custid=NULL) {
        $this->db->select('so.*,DATE_FORMAT(so.so_date, "%d/%m/%Y") as so_date_str,so.payment_group_type as credit_notes, "0" as paid_amount, cus.name', FALSE);
        $this->db->join($this->customer . ' as cus', 'cus.id = so.customer_id', 'left');
        $this->db->where('so.status != 10'); 
        if($custid == 'cash'){
          $this->db->where('so.so_type', 2);  
        }else{
          $this->db->where('so.so_type', 1);    
          $this->db->where('so.customer_id', $custid);  
        }
        $this->db->where_in('so.branch_id', $this->user_data['branch_id']);
        $this->db->where('so.id NOT IN (SELECT so_id FROM ' . $this->sales_payment . ' WHERE status != 10 and payment_status = 1)');
        $this->db->order_by('so.id', 'desc');
        return $this->db->get($this->sales_order . ' AS so')->result();
    }
    
    public function update_status($id,$data){
       $this->db->where('id',$id);
       $this->db->update($this->sales_order,$data);
       return $this->db->affected_rows();
    }

    public function get_supplier_details($pi_no) {
        //$this->db->select('pinv.po_id', FALSE);
        $this->db->where('pinv.id', $pi_no);
        return $this->db->get($this->purchases . ' as pinv')->row();
    }

    public function get_sales_order_details($sid) {
        $this->db->select('so.*, CASE WHEN so.customer_id IS NOT NULL THEN c.name ELSE so.customer_name END as name, c.address', FALSE);
        $this->db->join($this->customer . ' as c', 'c.id = so.customer_id','left');
        $this->db->where('so.id', $sid);
        return $this->db->get($this->sales_order . ' as so')->result();
    }

    public function get_paid_amount($pi_no) {
        $this->db->select('SUM(pay.pay_amt) as paid_amount,po.po_net_amount', FALSE);
        $this->db->join($this->purchases . ' as po', 'po.id = pay.po_id');
        $this->db->where('pay.po_id', $pi_no);
        $this->db->where('pay.status != 10');
        return $this->db->get($this->sales_payment . ' as pay')->row();
    }

    public function get_by_id($id) {
        $this->db->select('pay.*, so.total_amt, so.so_no,CASE WHEN so.customer_id IS NOT NULL THEN c.name ELSE so.customer_name END as name,c.address,CASE pay.payment_status WHEN 1 THEN "Paid" WHEN 2 THEN "Not Paid" WHEN 3 THEN "Cancelled" WHEN 4 THEN "Partially Paid" ELSE "" END as status_str, pt.type_name, pt.is_cheque', FALSE);
        $this->db->join($this->sales_order . ' as so', 'so.id = pay.so_id','left');
        $this->db->join($this->customer . ' as c', 'c.id = so.customer_id','left');
        $this->db->join($this->payment_type . ' as pt', 'pt.id = pay.pay_pay_type', 'left');
        $this->db->where('pay.id', $id);
        //$this->db->where('pay.status != 10');
        return $this->db->get($this->sales_payment . ' AS pay')->row();
    }
    
    public function get_by_so_id($id) {
        $this->db->select('pay.*,so.so_no,c.name,c.address,CASE pay.payment_status WHEN 1 THEN "Paid" WHEN 2 THEN "Not Paid" WHEN 3 THEN "Cancelled" WHEN 4 THEN "Partially Paid" ELSE "" END as status_str, pt.type_name, pt.is_cheque, pt.group_name', FALSE);
        $this->db->join($this->sales_order . ' as so', 'so.id = pay.so_id','left');
        $this->db->join($this->customer . ' as c', 'c.id = so.customer_id','left');
        $this->db->join($this->payment_type . ' as pt', 'pt.id = pay.pay_pay_type', 'left');
        $this->db->where('pay.so_id', $id);
        $this->db->where('pay.status != 10');
        return $this->db->get($this->sales_payment . ' AS pay')->row();
    }
    
    public function get_by_inv($id) {
        $this->db->select('pinv.*', FALSE);
        $this->db->where('pinv.id', $id);
        $this->db->where('pinv.po_status != 10');
        return $this->db->get($this->purchases . ' AS pinv')->result();
    }

    public function get_by_amount($id) {
        $this->db->select('SUM(pay.pay_amt) as paid_amount', FALSE);
        $this->db->where('pay.po_id', $id);
        $this->db->where('pay.status != 10');
        return $this->db->get($this->sales_payment . ' AS pay')->row();
    }

    public function insert($data) {
        $this->db->insert($this->sales_payment, $data);
        return $this->db->insert_id();
    }

    public function update_payment($id, $data) {
        $this->db->where('po_id', $id);
        $this->db->update($this->sales_payment, $data);
        return $this->db->affected_rows();
    }
	
	public function update_payment_status($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->sales_order, $data);
        return $this->db->affected_rows();
    }

    public function get_by_payment_details($id) {
        $this->db->select('pay.*', FALSE);
        $this->db->where('pay.po_id', $id);
        $this->db->where('pay.status != 10');
        return $this->db->get($this->sales_payment . ' AS pay')->result();
    }

    public function get_pur_order_payment_details($po_id) {
        $this->db->select('po.*,s.name,s.address', FALSE);
        $this->db->join($this->suppliers . ' as s', 's.id = po.sup_id');
        $this->db->where('po.id', $po_id);
        return $this->db->get($this->purchases . ' as po')->row();
    }

    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->sales_payment, $data);
        return $this->db->affected_rows();
    }
    
    public function get_sales_payment_details($id) {
        $this->db->select('pay.*', FALSE);
        $this->db->where('pay.id', $id);
        return $this->db->get($this->sales_payment . ' AS pay')->result_array();
    }
    
    /*public function paid_amount($id) {
        $this->db->select('sum(pay_amt) as paid_amount');
        $this->db->where('id', $id);
        return $this->db->get($this->sales_payment)->row()->paid_amount;
    }*/

    public function inv_id($id) {
        $this->db->select('po_id');
        $this->db->where('id', $id);
        return $this->db->get($this->sales_payment)->row()->po_id;
    }

    public function order_payment_by_id($id) {
        $this->db->select('po_net_amount');
        $this->db->where('id', $id);
        return $this->db->get($this->purchases)->row();
    }

    public function update_po_status($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->purchases, $data);
        return $this->db->affected_rows();
    }
    
    public function get_payment($so_id) {
        $this->db->where('so_id', $so_id);
        return $this->db->get($this->sales_payment)->row();
    }
}

?>