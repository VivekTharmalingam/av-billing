<?php
class Purchase_payment_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->suppliers = TBL_SUP;
        $this->purchase_receive = TBL_PR;
        $this->purchase_payment = TBL_PPM;
		$this->payment_type = TBL_PAYMENT_TYPE;
    }

    public function list_all() {
		$this->db->select('pr.id, IF(pr.supplier_invoice_no IS NULL or pr.supplier_invoice_no = "", "N/A", pr.supplier_invoice_no) as supplier_invoice_no, DATE_FORMAT(pr.invoice_date, "%d/%m/%Y") as invoice_date, pr.pr_net_amount, COALESCE(SUM(pay.pay_amt), 0) as paid_amount, CASE pr.payment_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as payment_status, s.name', FALSE);
        $this->db->join($this->purchase_receive . ' as pr', 'pr.id = pay.precv_id AND pr.pr_status != 10');
        $this->db->join($this->suppliers . ' as s', 's.id = pr.sup_id');
		$this->db->where('pay.status !=', 10);
		$this->db->where_in('pay.brn_id', $this->user_data['branch_id']);
        $this->db->order_by('pay.id', 'desc');
		$this->db->group_by('pay.precv_id');
        return $this->db->get($this->purchase_payment . ' AS pay')->result();
    }
	
	public function get_payment_by_precv_id($id) {
		$this->db->select('pay.*, DATE_FORMAT(pay.pay_date, "%d-%m-%Y") as payment_date, pt.type_name, pt.is_cheque', FALSE);
		$this->db->join($this->payment_type . ' as pt', 'pt.id = pay.pay_pay_type', 'left');
		$this->db->where('pay.status !=', 10);
		$this->db->where('pay.precv_id', $id);
		$this->db->where_in('pay.brn_id', $this->user_data['branch_id']);
        $this->db->order_by('pay.id');
        return $this->db->get($this->purchase_payment . ' AS pay')->result();
    }
	public function get_payment_by_pay_id($id) {
		$this->db->select('pay.*, pr.pr_code, DATE_FORMAT(pay.pay_date, "%d-%m-%Y") as payment_date, pt.type_name, pt.is_cheque', FALSE);
		$this->db->join($this->payment_type . ' as pt', 'pt.id = pay.pay_pay_type', 'left');
		$this->db->join($this->purchase_receive . ' as pr', 'pr.id = pay.precv_id');
		$this->db->where('pay.status !=', 10);
		$this->db->where('pay.id', $id);
		$this->db->where_in('pay.brn_id', $this->user_data['branch_id']);
        $this->db->order_by('pay.id');
        return $this->db->get($this->purchase_payment . ' AS pay')->row();
    }
	
    public function get_unpaid_purchases_by_supplier($supplier_id) {
        $this->db->select('pr.id, IF(pr.supplier_invoice_no IS NULL or pr.supplier_invoice_no = "", "N/A", pr.supplier_invoice_no) as supplier_invoice_no, DATE_FORMAT(pr.invoice_date, "%d/%m/%Y") as invoice_date, pr.pr_net_amount, COALESCE(SUM(pay.pay_amt), 0) as paid_amount, CASE pr.payment_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as payment_status, pr.credit_notes, pr.credit_amount', FALSE);
        $this->db->join($this->purchase_payment . ' as pay', 'pay.precv_id = pr.id AND pay.status != 10', 'left');
        $this->db->where('pr.pr_status !=', 10);
		$this->db->where('pr.payment_status !=', 3);
		$this->db->where('pr.sup_id', $supplier_id);
		$this->db->where_in('pr.branch_id', $this->user_data['branch_id']);
        $this->db->order_by('pr.invoice_date', 'asc');
		$this->db->group_by('pr.id');
        return $this->db->get($this->purchase_receive . ' AS pr')->result();
	}
	
	public function update_purchase_receive_payment_status($prev_id, $data) {
		$this->db->where('id', $prev_id);
        $this->db->update($this->purchase_receive, $data);
        return $this->db->affected_rows();
	}
    
	public function get_by_id($id) {
		$this->db->select('pay.*, pt.group_name', FALSE);
		$this->db->join($this->payment_type . ' as pt', 'pt.id = pay.pay_pay_type', 'left');
        $this->db->where('pay.id', $id);
        return $this->db->get($this->purchase_payment . ' AS pay')->row();
    }
	
	public function total_paid_by_purchase_recv_id($precv_id) {
        $this->db->select('SUM(pay.pay_amt) as paid_amount, pr.pr_net_amount, pr.credit_amount', FALSE);
        $this->db->join($this->purchase_receive . ' as pr', 'pr.id = pay.precv_id');
        $this->db->where('pay.precv_id', $precv_id);
        $this->db->where('pay.status != 10');
        return $this->db->get($this->purchase_payment . ' as pay')->row();
    }
	
	public function insert($data) {
        $this->db->insert($this->purchase_payment, $data);
        return $this->db->insert_id();
    }
	
	public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->purchase_payment, $data);
        return $this->db->affected_rows();
    }
	
	/*public function list_purchase_no() {
        $user_data = get_user_data();
        $this->db->select('po.*,sup.name', FALSE);
        $this->db->join($this->suppliers . ' as sup', 'sup.id = po.sup_id', 'left');
        $this->db->where('po.po_status != 10');
        $this->db->where('po.po_status != 3');
        $this->db->where('po.payment_status != 3');
        $this->db->where_in('po.brn_id',$user_data['branch_id']);
        return $this->db->get($this->purchases . ' AS po')->result();
    }
	
	public function get_supplier_details($pi_no) {
        //$this->db->select('pinv.po_id', FALSE);
        $this->db->where('pinv.id', $pi_no);
        return $this->db->get($this->purchases . ' as pinv')->row();
    }

    public function get_pur_order_details($po_id) {
        $this->db->select('po.*,s.name,s.address', FALSE);
        $this->db->join($this->suppliers . ' as s', 's.id = po.sup_id');
        $this->db->where('po.id', $po_id);
        return $this->db->get($this->purchases . ' as po')->result();
    }

    public function get_paid_amount($pi_no) {
        $this->db->select('SUM(pay.pay_amt) as paid_amount,po.po_net_amount', FALSE);
        $this->db->join($this->purchases . ' as po', 'po.id = pay.po_id');
        $this->db->where('pay.po_id', $pi_no);
        $this->db->where('pay.status != 10');
        return $this->db->get($this->purchase_payment . ' as pay')->row();
    }

    public function get_by_id($id) {
        $this->db->select('pay.*', FALSE);
        $this->db->where('pay.id', $id);
        $this->db->where('pay.status != 10');
        return $this->db->get($this->purchase_payment . ' AS pay')->row();
    }

    public function get_by_inv($id) {
        $this->db->select('pinv.*', FALSE);
        $this->db->where('pinv.id', $id);
        $this->db->where('pinv.po_status != 10');
        return $this->db->get($this->purchases . ' AS pinv')->result();
    }

    public function get_by_amount($id) {
        $this->db->select('SUM(pay.pay_amt) as paid_amount', FALSE);
        $this->db->where('pay.po_id', $id);
        $this->db->where('pay.status != 10');
        return $this->db->get($this->purchase_payment . ' AS pay')->row();
    }

    public function update_payment($id, $data) {
        $this->db->where('po_id', $id);
        $this->db->update($this->purchase_payment, $data);
        return $this->db->affected_rows();
    }

    public function get_by_payment_details($id) {
        $this->db->select('pay.*, pt.type_name, pt.is_cheque', FALSE);
        $this->db->join($this->payment_type . ' as pt', 'pt.id = pay.pay_pay_type', 'left');
        $this->db->where('pay.po_id', $id);
        $this->db->where('pay.status != 10');
        return $this->db->get($this->purchase_payment . ' AS pay')->result();
    }

    public function get_pur_order_payment_details($po_id) {
        $this->db->select('po.*,s.name,s.address', FALSE);
        $this->db->join($this->suppliers . ' as s', 's.id = po.sup_id');
        $this->db->where('po.id', $po_id);
        return $this->db->get($this->purchases . ' as po')->row();
    }

    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->purchase_payment, $data);
        return $this->db->affected_rows();
    }

    public function paid_amount($id) {
        $this->db->select('sum(pay_amt) as paid_amount');
        $this->db->where('id', $id);
        return $this->db->get($this->purchase_payment)->row()->paid_amount;
    }
	
	public function total_paid_by_id($id) {
        $this->db->select('sum(pay_amt) as paid_amount');
        $this->db->where('po_id', $id);
		 $this->db->where('status !=',10) ;
        return $this->db->get($this->purchase_payment)->row()->paid_amount;
    }

    public function inv_id($id) {
        $this->db->select('po_id');
        $this->db->where('id', $id);
        return $this->db->get($this->purchase_payment)->row()->po_id;
    }

    public function order_payment_by_id($id) {
        $this->db->select('po_net_amount');
        $this->db->where('id', $id);
        return $this->db->get($this->purchases)->row();
    }

    public function update_po_status($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->purchases, $data);
        return $this->db->affected_rows();
    }

	public function update_status($id,$data){
	   $this->db->where('id',$id);
	   $this->db->update($this->purchases,$data);
	   return $this->db->affected_rows();
	}*/
}
?>