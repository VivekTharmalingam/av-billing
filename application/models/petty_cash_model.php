<?php
class Petty_Cash_Model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->petty_cash = TBL_PETTY_CASH;
        $this->petty_cash_items = TBL_PETTY_CASH_ITEMS;
        $this->payment_type = TBL_PAYMENT_TYPE;
        $this->payment_mode = TBL_PAYMENT_MODE;
		$this->sales_order = TBL_SO;
        $this->sales_payment = TBL_SPM;
        $this->expense = TBL_EXP;
        $this->payment = TBL_PAYM;
        $this->deduction = TBL_DEDC;
        $this->deposit = TBL_DEPS;
        $this->purchase_payments = TBL_PPM;
        $this->exchange_order_payments = TBL_EXRPM;
        $this->sales_return = TBL_SO_RET;
        $this->external_service_payments = TBL_EXTSERPAY;
    }

    public function list_all() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        //$this->db->where('status != 2');
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->petty_cash)->result();
    }

    public function get_by_id($id) {
        $this->db->select('*', FALSE);
        $this->db->where('id', $id);
        return $this->db->get($this->petty_cash)->row();
    }
	
	public function get_items_by_id($id) {
        $this->db->select('*', FALSE);
        $this->db->where('petty_cash_id', $id);
        $this->db->where('type', 1);
        return $this->db->get($this->petty_cash_items)->result();
    }
	
	public function get_prev_day_items_by_id($id) {
        $this->db->select('*', FALSE);
        $this->db->where('petty_cash_id', $id);
        $this->db->where('type', 2);
        return $this->db->get($this->petty_cash_items)->result();
    }
	
    public function insert($data) {
        $this->db->insert($this->petty_cash, $data);
        return $this->db->insert_id();
    }
	
	public function insert_item($data) {
        $this->db->insert($this->petty_cash_items, $data);
        return $this->db->insert_id();
    }
	
    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->petty_cash, $data);
        return $this->db->affected_rows();
    }
	
    public function update_item($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->petty_cash_items, $data);
        return $this->db->affected_rows();
    }
	
    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->petty_cash);
        return $this->db->affected_rows();
    }

    public function date_already_exists($date, $id) {
		$this->db->where('branch_id', $this->user_data['branch_id']);
        $this->db->where('date = STR_TO_DATE("' . $date . '", "%d/%m/%Y")');
        $this->db->where('status != 10');
		
        if (!empty($id)) 
            $this->db->where('id !=', $id);
		
        return $this->db->get($this->petty_cash)->num_rows();
    }
	
	public function opening_balance($date) {
		$opening_balance = 0;
		$this->db->select('closing_balance');
		$this->db->where('status', 1);
		$this->db->where('branch_id', $this->user_data['branch_id']);
		$this->db->where('DATE(date) = DATE_SUB(STR_TO_DATE("' . $date . '", "%d/%m/%Y"), INTERVAL 1 DAY)');
		$resource = $this->db->get($this->petty_cash);
		
		if ($resource->num_rows() > 0)
            $opening_balance = $resource->row()->closing_balance;
        
		return $opening_balance;
	}
	
	public function unpaid_invoice_amount($date) {
		$total_amt = 0;
		$this->db->select('SUM(total_amt) as total_amt');
		$this->db->where('status', 1);
		$this->db->where('payment_status', 2);
		$this->db->where('branch_id', $this->user_data['branch_id']);
		$this->db->where('DATE(created_on) = STR_TO_DATE("' . $date . '", "%d/%m/%Y")');
		$resource = $this->db->get($this->sales_order);
		if ($resource->num_rows() > 0)
            $total_amt = $resource->row()->total_amt;
        
		return $total_amt;
	}
	
	public function previous_unpaid_invoice_amount($date) {
		$total_amt = 0;
		$this->db->select('SUM(total_amt) as total_amt');
		$this->db->where('status', 1);
		$this->db->where('payment_status', 2);
		$this->db->where('branch_id', $this->user_data['branch_id']);
		$this->db->where('DATE(created_on) < STR_TO_DATE("' . $date . '", "%d/%m/%Y")');
		$resource = $this->db->get($this->sales_order);
		if ($resource->num_rows() > 0)
            $total_amt = $resource->row()->total_amt;
        
		return $total_amt;
	}
	
	public function prev_sales_amount($date) {
		/*$this->db->select('SUM(so.total_amt) as total_amt', FALSE);
		$this->db->where('so.status !=', 10);
		$this->db->where('so.branch_id', $this->user_data['branch_id']);
		$this->db->where('DATE(so.created_on) < STR_TO_DATE("' . $date . '", "%d/%m/%Y")');
		return $this->db->get($this->sales_order . ' as so')->row()->total_amt;*/
		
		$this->db->select('SUM(sp.pay_amt) as total_amt', FALSE);
		$this->db->join($this->sales_order . ' as so', 'so.id = sp.so_id AND DATE(so.created_on) < DATE(sp.created_on)');
		$this->db->where('sp.status', 1);
		$this->db->where_not_in('sp.payment_status', array(2, 3));
		$this->db->where('sp.brn_id', $this->user_data['branch_id']);
		$this->db->where('DATE(sp.created_on) = STR_TO_DATE("' . $date . '", "%d/%m/%Y")');
		return $this->db->get($this->sales_payment . ' as sp')->row()->total_amt;
	}
	
	public function total_sales_amount($date) {
		$this->db->select('SUM(so.total_amt) as total_amt', FALSE);
		$this->db->where('so.status !=', 10);
		$this->db->where('so.branch_id', $this->user_data['branch_id']);
		$this->db->where('DATE(so.created_on) = STR_TO_DATE("' . $date . '", "%d/%m/%Y")');
		return $this->db->get($this->sales_order . ' as so')->row()->total_amt;
	}
	
	public function sales_payment($date) {
		$this->db->select('pt.id, pt.type_name, pt.group_name, (SELECT SUM(sp.pay_amt) FROM ' . $this->sales_payment . ' as sp JOIN ' . $this->sales_order . ' as so ON so.id = sp.so_id AND DATE(so.created_on) = DATE(sp.created_on) WHERE sp.pay_pay_type = pt.id AND sp.status = 1 AND sp.payment_status NOT IN(2, 3) AND DATE(sp.created_on) = STR_TO_DATE("' . $date . '", "%d/%m/%Y") AND sp.brn_id = ' . $this->user_data['branch_id'] . ') as total_amt', FALSE);
		$this->db->where('pt.group_name != 3');
		$this->db->where('pt.status', 1);
		$this->db->group_by('pt.id');
		return $this->db->get($this->payment_type . ' as pt')->result();
	}
	
	public function previous_day_sales_payment($date) {
		$this->db->select('pt.id, pt.type_name, pt.group_name, (SELECT SUM(sp.pay_amt) FROM ' . $this->sales_payment . ' as sp JOIN ' . $this->sales_order . ' as so ON so.id = sp.so_id AND DATE(so.created_on) < DATE(sp.created_on) WHERE sp.pay_pay_type = pt.id AND sp.status = 1 AND sp.payment_status NOT IN(2, 3) AND DATE(sp.created_on) = STR_TO_DATE("' . $date . '", "%d/%m/%Y") AND sp.brn_id = ' . $this->user_data['branch_id'] . ') as total_amt', FALSE);
		
		$this->db->where('pt.group_name != 3');
		$this->db->where('pt.status', 1);
		$this->db->group_by('pt.id');
		return $this->db->get($this->payment_type . ' as pt')->result();
	}
	
	public function expense_amount($date) {
		$this->db->select('IF (SUM(e.amount) IS NOT NULL, SUM(e.amount), 0) as total_amt', FALSE);
		$this->db->where('e.branch_id', $this->user_data['branch_id']);
		$this->db->where('e.status', 1);
		$this->db->where('DATE(e.created_on) = STR_TO_DATE("' . $date . '", "%d/%m/%Y")');
		return $this->db->get($this->expense . ' as e')->row()->total_amt;
	}
	
	public function payment_amount($date) {
		$this->db->select('IF (SUM(p.pay_amount) IS NOT NULL, SUM(p.pay_amount), 0) as total_amt', FALSE);
		$this->db->join($this->payment_mode . ' as pm', 'pm.id = p.payment_type AND pm.is_cash = 1');
		$this->db->where('p.status', 1);
		$this->db->where('p.branch_id', $this->user_data['branch_id']);
		$this->db->where('DATE(p.created_on) = STR_TO_DATE("' . $date . '", "%d/%m/%Y")');
		return $this->db->get($this->payment . ' as p')->row()->total_amt;
	}
	
	public function deduction_amount($date) {
		$this->db->select('IF (SUM(d.ded_amount) IS NOT NULL, SUM(d.ded_amount), 0) as total_amt', FALSE);
		$this->db->join($this->payment_mode . ' as pm', 'pm.id = d.payment_type AND pm.is_cash = 1');
		$this->db->where('d.status', 1);
		$this->db->where('d.branch_id', $this->user_data['branch_id']);
		$this->db->where('DATE(d.created_on) = STR_TO_DATE("' . $date . '", "%d/%m/%Y")');
		return $this->db->get($this->deduction . ' as d')->row()->total_amt;
	}
	
	public function purchase_payments($date) {
		$this->db->select('IF (SUM(p.pay_amt) IS NOT NULL, SUM(p.pay_amt), 0) as total_amt', FALSE);
		$this->db->join($this->payment_type . ' as pt', 'pt.id = p.pay_pay_type AND pt.group_name = 1');
		$this->db->where('p.status', 1);
		$this->db->where('p.brn_id', $this->user_data['branch_id']);
		$this->db->where('DATE(p.created_on) = STR_TO_DATE("' . $date . '", "%d/%m/%Y")');
		return $this->db->get($this->purchase_payments . ' as p')->row()->total_amt;
	}
	
	public function eo_payments($date) {
		$this->db->select('IF (SUM(p.pay_amt) IS NOT NULL, SUM(p.pay_amt), 0) as total_amt', FALSE);
		$this->db->join($this->payment_type . ' as pt', 'pt.id = p.pay_pay_type AND pt.group_name = 1');
		$this->db->where('p.status', 1);
		$this->db->where('p.brn_id', $this->user_data['branch_id']);
		$this->db->where('DATE(p.created_on) = STR_TO_DATE("' . $date . '", "%d/%m/%Y")');
		return $this->db->get($this->exchange_order_payments . ' as p')->row()->total_amt;
	}
	
	public function external_service_payment($date) {
		$this->db->select('IF (SUM(p.pay_amt) IS NOT NULL, SUM(p.pay_amt), 0) as total_amt', FALSE);
		$this->db->join($this->payment_type . ' as pt', 'pt.id = p.pay_pay_type AND pt.group_name = 1');
		$this->db->where('p.status', 1);
		$this->db->where('p.brn_id', $this->user_data['branch_id']);
		$this->db->where('DATE(p.created_on) = STR_TO_DATE("' . $date . '", "%d/%m/%Y")');
		return $this->db->get($this->external_service_payments . ' as p')->row()->total_amt;
	}
	
	public function refund_amount($date) {
		$this->db->select('IF (SUM(sr.refund_amount) IS NOT NULL, SUM(sr.refund_amount), 0) as total_amt', FALSE);
		$this->db->where('sr.status', 1);
		$this->db->where('sr.brn_id', $this->user_data['branch_id']);
		$this->db->where('DATE(sr.created_on) = STR_TO_DATE("' . $date . '", "%d/%m/%Y")');
		return $this->db->get($this->sales_return . ' as sr')->row()->total_amt;
	}
	
	public function deposite_amount($date) {
		$this->db->select('IF (SUM(d.dep_amount) IS NOT NULL, SUM(d.dep_amount), 0) as total_amt', FALSE);
		$this->db->join($this->payment_mode . ' as pm', 'pm.id = d.payment_type AND pm.is_cash = 1');
		$this->db->where('d.status', 1);
		$this->db->where('d.branch_id', $this->user_data['branch_id']);
		$this->db->where('DATE(d.created_on) = STR_TO_DATE("' . $date . '", "%d/%m/%Y")');
		return $this->db->get($this->deposit . ' as d')->row()->total_amt;
	}
}
?>