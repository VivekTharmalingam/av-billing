<?php
class Category_Model extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->category = TBL_CAT;
    }    
    public function list_active() {
        $this->db->select('id, cat_code, cat_name');
        $this->db->where('status', 1);
        $this->db->order_by('cat_name');
        return $this->db->get($this->category)->result();
    }
    
    /*public function get_company_name_by_ids($company_ids) {
        $this->db->select('id, comp_name');
        $this->db->where('status !=', 10);
        $this->db->where_in('id', $company_ids);
        return $this->db->get($this->companies)->result();
    }*/
    
    public function list_all() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->category)->result();
    }    
    public function get_by_id($id) {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('id', $id);
        return $this->db->get($this->category)->row();
    }
    public function insert($data) {
        $this->db->insert($this->category, $data);
        return $this->db->insert_id();
    }
    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update($this->category, $data);
        return $this->db->affected_rows();
    }
    public function delete($id){
        $this->db->where('id', $id);
        $this->db->delete($this->category);
        return $this->db->affected_rows();
    }
    public function check_category_exists($category,$id){
        $this->db->where('cat_name',$category);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        if(!empty($id))
           $this->db->where('id !=',$id);
        $getData = $this->db->get($this->category);
		
        if($getData->num_rows() > 0)
            return 1;
        else
            return 0;
    }
}
?>