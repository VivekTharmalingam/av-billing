<?php

class External_Services_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->exter_services = TBL_EXTSER;
        $this->service_partner = TBL_SERPTN;	
        $this->customer = TBL_CUS;
        $this->suppliers = TBL_SUP;
        $this->users = TBL_USER;
        $this->branches = TBL_BRN;
        $this->company = TBL_COMP;
        $this->serv_category = TBL_SCAT;
        $this->serv_items = TBL_SITM;
        $this->services = TBL_SJST;
        $this->services_items = TBL_SJSTITM;
        $this->services_inventory_items = TBL_SEXINITM;
        $this->product = TBL_PDT;
        $this->exchange_order = TBL_EXR;
        $this->exchange_order_items = TBL_EXRIT;
        $this->sales_order = TBL_SO;
        $this->user_data = get_user_data();
    }

    public function list_all() {
        $this->db->select('extser.*, DATE_FORMAT(extser.issued_date, "%d/%m/%Y") as issued_date_str, DATE_FORMAT(extser.received_date, "%d/%m/%Y") as received_date_str, CASE extser.exter_pay_status WHEN 1 THEN "Unpaid" WHEN 2 THEN "Paid" ELSE "" END as exter_pay_status_str, CASE extser.status WHEN 1 THEN "Issued" WHEN 2 THEN "Received" ELSE "" END as status_str', FALSE);
        $this->db->join($this->branches . ' as brn', 'brn.id = extser.branch_id');
        $this->db->where('extser.status !=', 10);
        $this->db->order_by('extser.id', 'desc');
        $this->db->where_in('extser.branch_id', $this->user_data['branch_id']);
        return $this->db->get($this->exter_services . ' as extser')->result();
    }
    
    public function get_all_service_partner() {
        $this->db->where('serv.status', 1);
        $this->db->order_by('serv.name', 'asc');
        return $this->db->get($this->service_partner . ' as serv')->result();
    }
    
    public function insert($data) {
        $this->db->insert($this->exter_services, $data);
        return $this->db->insert_id();
    }
    
    public function get_by_id($id) {
        $this->db->select('extser.*, DATE_FORMAT(extser.issued_date, "%d/%m/%Y") as issued_date_str, DATE_FORMAT(extser.received_date, "%d/%m/%Y") as received_date_str, CASE extser.exter_pay_status WHEN 1 THEN "Unpaid" WHEN 2 THEN "Paid" ELSE "" END as exter_pay_status_str, CASE extser.status WHEN 1 THEN "Issued" WHEN 2 THEN "Received" ELSE "" END as status_str',FALSE);
        $this->db->join($this->service_partner . ' as supl', 'supl.id = extser.service_partner_id', 'left');
        $this->db->where('extser.id', $id);
        return $this->db->get($this->exter_services . ' as extser')->row();
    }
    
    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->exter_services, $data);
        return $this->db->affected_rows();
    }
    
    public function get_exter_services_report($from_date = '', $to_date = '',$cus = '', $engg = '', $branch = '', $type = '') {
        $this->db->select('serv.id AS srId, sale.total_amt, serv.customer_name AS cust_name, serv.jobsheet_number, DATE_FORMAT(serv.received_date, "%d/%m/%Y") as received_date_str, u.name AS eng_name, CASE serv.service_status WHEN 1 THEN "Processing" WHEN 2 THEN "Completed" ELSE "" END as service_status,serv.estimated_price_quoted as estimated_price, (SELECT SUM(sxi.total) FROM ' . $this->services_inventory_items . ' as sxi WHERE sxi.service_id = serv.id AND sxi.form_type = 3 AND sxi.status = 1) as service_cost', FALSE);
		
		$this->db->join($this->branches . ' as brn', 'brn.id = serv.branch_id', 'left');
        $this->db->join($this->users . ' as u', 'u.id = serv.engineer_id', 'left AND u.status !=', 10);
        $this->db->join($this->sales_order . ' as sale', 'sale.service_id = serv.id AND sale.status != 10', 'left');
        $this->db->join($this->customer . ' as cus', 'cus.id = serv.customer_id', 'left');
		
        if (!empty($from_date)) {
            $this->db->where('DATE(serv.received_date) >=', date('Y-m-d', strtotime($from_date)));
        }
        if (!empty($to_date)) {
        $this->db->where('DATE(serv.received_date) <=', date('Y-m-d', strtotime($to_date)));
        }
        if ((!empty($cus))) {
            $this->db->where('serv.customer_id', $cus);
        }
        if ((!empty($engg))) {
            $this->db->where('serv.engineer_id', $engg);
        }
        
        if ((!empty($branch))) {
            $this->db->where('serv.branch_id', $branch);
        }
        
        if ((!empty($type))) {
            $this->db->where('serv.service_status', $type);
        }
        
        $this->db->where('serv.status !=', 10);
        //$this->db->where('u.status !=', 10);
        $this->db->order_by('serv.id', 'desc');
        $this->db->group_by('serv.id');
        $this->db->where_in('serv.branch_id', $this->user_data['branch_ids'], FALSE);
        
        return $this->db->get($this->services . ' as serv')->result();
    }
    
}

?>