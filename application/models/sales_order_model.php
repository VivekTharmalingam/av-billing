<?php

class Sales_Order_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->suppliers = TBL_SUP;
        $this->sales_order = TBL_SO;
        $this->sales_order_item = TBL_SO_ITM;
        $this->sales_order_hold = TBL_SO_HLD;
        $this->sales_order_item_hold = TBL_SO_ITM_HLD;
        $this->services_exter_item = TBL_SEREXTITM;
        $this->items = TBL_PDT;
        $this->category = TBL_CAT;
        $this->ship_address = TBL_SHIP_ADR;
        $this->company = TBL_COMP;
        $this->customer = TBL_CUS;
        //$this->inventory_product = TBL_INV_PDT;
        $this->branch_products = TBL_BRN_PDT;
        $this->branches = TBL_BRN;
        $this->sales_payment = TBL_SPM;
        $this->payment_type = TBL_PAYMENT_TYPE;
        $this->exchange_order = TBL_EXR;
        $this->exchange_order_items = TBL_EXRIT;
        $this->services = TBL_SJST;
        $this->services_inventory_items = TBL_SEXINITM;
        $this->user = TBL_USER;
        $this->notification = TBL_NOTIFICATION;
        $this->quotation = TBL_QUO;
        $this->invoice_email_sent = TBL_SO_EMAIL;
        $this->user_data = get_user_data();
    }

    public function list_all() {
        $user_data = get_user_data();
        $this->db->select('so.id, so.so_no, so.so_date, so.total_amt, so.status, so.your_reference, CASE so.status WHEN 1 THEN "New" WHEN 2 THEN "Partially Delivered" WHEN 3 THEN "Delivered" ELSE "" END as status_str, CASE so.so_type WHEN 1 THEN c.name ELSE so.customer_name END as name', FALSE);
        $this->db->join($this->customer . ' as c', 'c.id = so.customer_id', 'left');
        $this->db->where('so.status !=', 10);
//        if($user_data['is_admin']!='1'){
//            $this->db->where_in('so.branch_id', $user_data['branch_ids']);
//        }
        $this->db->where_in('so.branch_id', $this->user_data['branch_id']);
        $this->db->order_by('so.id', 'desc');
        return $this->db->get($this->sales_order . ' as so')->result();
    }

    public function get_active_qtn_list() {
        $sales_quotation = '(SELECT COUNT(*) FROM ' . $this->sales_order . ' as soi WHERE soi.quotation_id = quo.id and soi.status != 10) as so_quotation';
        $this->db->select('quo.id, quo.quo_no, quo.quo_type, CASE quo.quo_type WHEN 1 THEN c.name ELSE quo.customer_name END as name, DATE_FORMAT(quo.so_date, "%d-%m-%Y") as so_date, DATE_FORMAT(quo.do_date, "%d-%m-%Y") as do_date, quo.leave_date, quo.sub_total, quo.gst, quo.gst_amount, quo.gst_percentage, quo.total_amt, quo.status, quo.payment_status, CASE quo.payment_status WHEN 1 THEN "Paid" WHEN 3 THEN "Cancelled" ELSE "UnPaid" END as payment_status_str, CASE quo.status WHEN 1 THEN "New" WHEN 2 THEN "Partially Delivered" WHEN 3 THEN "Delivered" ELSE "" END as status_str, quo.payment_terms, quo.shipping_address, quo.discount, quo.discount_type, quo.discount_amount, quo.your_reference, quo.remarks, quo.branch_id, quo.customer_id, c.contact_name, c.phone_no, c.email, CASE quo.quo_type WHEN 1 THEN c.address ELSE "CASH CUSTOMER" END as address, u.name AS uname, pt.type_name as payment_type, quo.inv_created, ' . $sales_quotation, FALSE);
        $this->db->join($this->customer . ' as c', 'c.id = quo.customer_id', 'left');
        $this->db->join($this->user . ' as u', 'u.id = quo.created_by', 'left');
        $this->db->join($this->sales_payment . ' as sp', 'sp.so_id = quo.id', 'left');
        $this->db->join($this->payment_type . ' as pt', 'pt.id = sp.pay_pay_type', 'left');
        $this->db->where('quo.status !=', 10);
        $this->db->where_in('quo.branch_id', $this->user_data['branch_id']);
        $this->db->order_by('quo.id', 'desc');
        $this->db->having('so_quotation <= 0');
        return $this->db->get($this->quotation . ' as quo')->result();
    }

    public function delete_hold_invoice($id) {
        $this->db->where('so_id', $id);
        $this->db->delete($this->sales_order_item_hold);

        $this->db->where('id', $id);
        $this->db->delete($this->sales_order_hold);
        return $this->db->affected_rows();
    }

    public function hold_so_list() {
        $this->db->select('so.id, so.so_no, so.so_type, so.invoice_from, so.exchange_order_id, so.service_id, CASE so.so_type WHEN 1 THEN c.name ELSE so.customer_name END as name, DATE_FORMAT(so.so_date, "%d-%m-%Y") as so_date,so.sub_total, so.gst, so.gst_amount, so.gst_percentage, so.total_amt, so.status, so.payment_status, CASE so.payment_status WHEN 1 THEN "Paid" WHEN 3 THEN "Cancelled" ELSE "UnPaid" END as payment_status_str, CASE so.status WHEN 1 THEN "New" WHEN 2 THEN "Partially Delivered" WHEN 3 THEN "Delivered" ELSE "" END as status_str, so.payment_terms, so.shipping_address, so.discount, so.discount_type, so.discount_amount, so.your_reference, so.remarks, so.branch_id, so.customer_id, c.contact_name, c.phone_no, c.email, CASE so.so_type WHEN 1 THEN c.address ELSE "CASH CUSTOMER" END as address', FALSE);
        $this->db->select('CASE so.invoice_from WHEN 1 THEN "Invoice" WHEN 2 THEN "Quotation" WHEN 3 THEN "Exchange Order" WHEN 4 THEN "Service" ELSE "" END as invoice_from_str', FALSE);
        $this->db->join($this->customer . ' as c', 'c.id = so.customer_id', 'left');
        $this->db->where_in('so.branch_id', $this->user_data['branch_id']);
        $this->db->order_by('so.id', 'desc');
        return $this->db->get($this->sales_order_hold . ' as so')->result();
    }

    public function get_invholdby_id($id) {
        $this->db->select('so.id, so.so_no, so.so_type, so.invoice_from, so.exchange_order_id, so.service_id, CASE so.so_type WHEN 1 THEN c.name ELSE so.customer_name END as name, DATE_FORMAT(so.so_date, "%d-%m-%Y") as so_date, DATE_FORMAT(so.do_date, "%d-%m-%Y") as do_date, so.sub_total, so.gst, so.gst_amount, so.gst_percentage, so.total_amt, so.status, so.payment_status, CASE so.payment_status WHEN 1 THEN "Paid" WHEN 3 THEN "Cancelled" ELSE "UnPaid" END as payment_status_str, CASE so.status WHEN 1 THEN "New" WHEN 2 THEN "Partially Delivered" WHEN 3 THEN "Delivered" ELSE "" END as status_str, so.payment_terms, so.shipping_address, so.discount, so.discount_type, so.discount_amount, so.your_reference, so.remarks, so.branch_id, so.customer_id, c.contact_name, c.phone_no, c.email, CASE so.so_type WHEN 1 THEN c.address ELSE "CASH CUSTOMER" END as address', FALSE);
        $this->db->select('CASE so.invoice_from WHEN 1 THEN "Invoice" WHEN 2 THEN "Quotation" WHEN 3 THEN "Exchange Order" WHEN 4 THEN "Service" ELSE "" END as invoice_from_str', FALSE);
        $this->db->join($this->customer . ' as c', 'c.id = so.customer_id', 'left');
        $this->db->where('so.id', $id);
        return $this->db->get($this->sales_order_hold . ' as so')->row();
    }

    public function get_holditems_by_so($so_id) {
        $this->db->select('soit.id, soit.price,soit.serial_no,soit.product_description,soit.unit_cost, soit.quantity, soit.discount, soit.discount_type, soit.discount_amount, soit.total, soit.item_id, p.pdt_code, p.pdt_name, p.pdt_description, soit.category_id, c.cat_code, c.cat_name');
        $this->db->join($this->items . ' as p', 'p.id = soit.item_id', 'left');
        $this->db->join($this->category . ' as c', 'c.id = soit.category_id', 'left');
        $this->db->where('soit.so_id', $so_id);
        $this->db->where('soit.status !=', 10);
        return $this->db->get($this->sales_order_item_hold . ' as soit')->result();
    }

    public function get_qtn_by_id($id) {
        $this->db->select('COUNT(*) as count', FALSE);
        $this->db->where('so.quotation_id', $id);
        $this->db->where('so.status !=', 10);
        return $this->db->get($this->sales_order . ' as so')->row()->count;
    }

    public function get_by_id($id) {
        $this->db->select('so.id, so.so_no, so.so_type, so.invoice_from, so.quotation_id, so.exchange_order_id, so.service_id, CASE so.so_type WHEN 1 THEN c.name ELSE so.customer_name END as name, DATE_FORMAT(so.so_date, "%d-%m-%Y") as so_date, DATE_FORMAT(so.do_date, "%d-%m-%Y") as do_date,so.payment_group_type,so.leave_date, so.sub_total, so.gst, so.gst_amount, so.gst_percentage, so.total_amt, so.status, so.payment_status, CASE so.payment_status WHEN 1 THEN "Paid" WHEN 3 THEN "Cancelled" WHEN 4 THEN "Partially Paid" ELSE "UnPaid" END as payment_status_str, CASE so.status WHEN 1 THEN "New" WHEN 2 THEN "Partially Delivered" WHEN 3 THEN "Delivered" ELSE "" END as status_str, so.payment_terms, so.shipping_address, so.discount, so.discount_type, so.discount_amount, so.your_reference, so.remarks, so.branch_id, so.customer_id, c.contact_name, c.phone_no, c.email, CASE so.so_type WHEN 1 THEN c.address ELSE "CASH CUSTOMER" END as address, u.name AS uname, pt.type_name as payment_type, pt.is_cheque, sp.pay_pay_bank, sp.pay_pay_no, DATE_FORMAT(sp.pay_pay_date, "%d-%m-%Y") as pay_date', FALSE);
        $this->db->select('CASE so.invoice_from WHEN 1 THEN "Invoice" WHEN 2 THEN "Quotation" WHEN 3 THEN "Exchange Order" WHEN 4 THEN "Service" ELSE "" END as invoice_from_str', FALSE);
        $this->db->join($this->customer . ' as c', 'c.id = so.customer_id', 'left');
        $this->db->join($this->user . ' as u', 'u.id = so.created_by', 'left');
        $this->db->join($this->sales_payment . ' as sp', 'sp.so_id = so.id', 'left');
        $this->db->join($this->payment_type . ' as pt', 'pt.id = sp.pay_pay_type', 'left');
        $this->db->where('so.id', $id);
        return $this->db->get($this->sales_order . ' as so')->row();
    }

    public function get_by_servicebased_invoice($id) {
        $this->db->select('so.*', FALSE);
        $this->db->where('so.service_id', $id);
        $this->db->where('so.status !=', 10);
        return $this->db->get($this->sales_order . ' as so')->result_array();
    }

    public function get_branch_list() {
        $this->db->where('status', 1);
        $this->db->order_by('id', 'desc');
        $this->db->where_in('id', $this->user_data['branch_ids']);
        return $this->db->get($this->branches)->result();
    }

    public function get_current_address($id) {
        $this->db->where('id', $id);
        return $this->db->get($this->branches)->row();
    }

    public function outstanding_amt($customer_id) {
        $this->db->select('SUM(so.total_amt) as total_amt', FALSE);
        $this->db->join($this->customer . ' as c', 'c.id = so.customer_id', 'left');
        $this->db->where('so.payment_status != 1');
        $this->db->where('so.status !=', 10);
        $this->db->where_in('so.branch_id', $this->user_data['branch_id']);
        return $this->db->get($this->sales_order . ' as so')->row()->total_amt;
    }

    public function get_edit_invitemids($so_id) {
        $this->db->select('soit.*');
        $this->db->where('soit.so_id', $so_id);
        $this->db->where('soit.status !=', 10);
        $inv_itm = $this->db->get($this->sales_order_item . ' as soit')->result();
        $all_id = array();
        foreach ($inv_itm as $key => $value) {
            $all_id[] = $value->id;
        }
        $ids_txt = (!empty($all_id)) ? implode(',', $all_id) : '';
        return $ids_txt;
    }

    public function get_edit_seritemids($ser_id) {
        $this->db->select('itm.*', FALSE);
        $this->db->where('itm.service_id', $ser_id);
        $this->db->where('itm.form_type = 1');
        $this->db->where('itm.status != 2');
        $this->db->where('itm.status != 10');
        $this->db->order_by('itm.id', 'asc');
        $inv_itm = $this->db->get($this->services_inventory_items . ' AS itm')->result();
        $all_id = array();
        foreach ($inv_itm as $key => $value) {
            $all_id[] = $value->id;
        }
        $ids_txt = (!empty($all_id)) ? implode(',', $all_id) : '';
        return $ids_txt;
    }

    public function get_filtered_exc($edit_page = NULL, $edit_page_id = NULL) {

        $so_qty = '(SELECT SUM(soit.quantity) FROM ' . $this->sales_order_item . ' as soit ';
        $so_qty .= ' WHERE soit.status != 10 AND soit.exc_form_type = 1';
        if ($edit_page == 'invoice') {
            $inv_itm_id = $this->get_edit_invitemids($edit_page_id);
            $so_qty .= ' AND `soit`.`id` NOT IN (' . $inv_itm_id . ')';
        }
        $so_qty .= ' AND soit.item_id = exritm.id) as invoice_qty';

        $service_qty = '(SELECT SUM(serinv.quantity) FROM ' . $this->services_inventory_items . ' as serinv ';
        $service_qty .= ' WHERE serinv.form_type = 1 AND serinv.status = 1';
        if ($edit_page == 'service') {
            $ser_itm_id = $this->get_edit_seritemids($edit_page_id);
            $service_qty .= ' AND `serinv`.`id` NOT IN (' . $ser_itm_id . ')';
        }
        $service_qty .= ' AND serinv.item_id = exritm.id) as service_qty';

        $select_txt = 'exritm.exr_id as id, excr.exr_code, sup.name, IF(exritm.exr_quantity IS NOT NULL AND exritm.exr_quantity != "", exritm.exr_quantity, "0") as exr_quantity, exritm.id as exc_item_id';

        $excitem_qry = 'SELECT ' . $select_txt . ', ' . $so_qty . ', ' . $service_qty . ' FROM (' . $this->exchange_order_items . ' as exritm)';
        $excitem_qry .= ' JOIN ' . $this->exchange_order . ' as excr ON `excr`.`id` = `exritm`.`exr_id`';
        $excitem_qry .= ' JOIN ' . $this->suppliers . ' as sup ON `sup`.`id` = `excr`.`sup_id`';
        $excitem_qry .= ' JOIN ' . $this->branches . ' as brn ON `brn`.`id` = `excr`.`branch_id`';
        $excitem_qry .= ' WHERE `excr`.`branch_id` IN (' . $this->user_data['branch_id'] . ') AND `exritm`.`status` != 2 AND `exritm`.`status` != 10';
        $excitem_qry .= ' HAVING exr_quantity > CASE WHEN invoice_qty IS NOT NULL AND service_qty IS NOT NULL THEN (invoice_qty + service_qty) WHEN invoice_qty IS NULL AND service_qty IS NOT NULL THEN service_qty WHEN service_qty IS NULL AND invoice_qty IS NOT NULL THEN invoice_qty ELSE 0 END';

        $main_qry = 'SELECT * FROM (' . $excitem_qry . ') AS exc_main GROUP BY exc_main.id';
        return $this->db->query($main_qry)->result();
    }

    public function get_soqtybyid($so_id, $itm_id) {
        $this->db->select('soit.quantity');
        $this->db->where('soit.so_id', $so_id);
        $this->db->where('soit.item_id', $itm_id);
        $this->db->where('soit.status !=', 10);
        return $this->db->get($this->sales_order_item . ' as soit')->row();
    }

    public function get_products_by_so($so_id) {
        $sales_order = $this->get_by_id($so_id);
        $this->db->select('soit.id, soit.price, soit.serial_no, soit.product_description, soit.unit_cost, soit.quantity, soit.discount, soit.discount_type, soit.discount_amount, soit.total, soit.item_id, soit.show_in_print, soit.category_id');

        if ($sales_order->invoice_from == 3) {
            $this->db->select('exr.product_text as pdt_code, exr.product_description as pdt_description');
            $this->db->join($this->exchange_order_items . ' as exr', 'exr.id = soit.item_id', 'left');
        } else {
            $this->db->select('p.pdt_code, c.cat_code, c.cat_name');
            $this->db->join($this->items . ' as p', 'p.id = soit.item_id', 'left');
            $this->db->join($this->category . ' as c', 'c.id = soit.category_id', 'left');
        }

        $this->db->where('soit.so_id', $so_id);
        $this->db->where('soit.status !=', 10);
        return $this->db->get($this->sales_order_item . ' as soit')->result();
    }

    public function get_exc_soitem_by($so_id, $type) {
        $this->db->select('soit.id, soit.price,soit.serial_no, soit.product_description,soit.unit_cost, soit.quantity, soit.discount, soit.discount_type, soit.discount_amount, soit.total, soit.show_in_print, soit.item_id, soit.category_id');
        if ($type == 1) {
            $this->db->select('excr.exr_code, exr.exr_id, exr.product_text as pdt_code, exr.product_description as pdt_description');
            $this->db->join($this->exchange_order_items . ' as exr', 'exr.id = soit.item_id', 'left');
            $this->db->join($this->exchange_order . ' as excr', 'excr.id = exr.exr_id', 'left');
        } else if ($type == 2) {
            $this->db->select('p.pdt_code, p.pdt_description');
            $this->db->join($this->items . ' as p', 'p.id = soit.item_id', 'left');
            $this->db->join($this->category . ' as c', 'c.id = soit.category_id', 'left');
        }
        $this->db->where('soit.exc_form_type', $type);
        $this->db->where('soit.so_id', $so_id);
        $this->db->where('soit.status !=', 10);
        return $this->db->get($this->sales_order_item . ' as soit')->result();
    }

    public function get_service_products_by_so($so_id, $type) {
        $this->db->select('soit.id, soit.service_form_type, soit.price, soit.serial_no, soit.product_description, soit.unit_cost, soit.quantity, soit.external_cost, soit.our_cost, soit.discount, soit.discount_type, soit.discount_amount, soit.total,  soit.show_in_print, soit.item_id');
        if ($type == 1) {
            $this->db->select('exr.id as exr_auto_id, exr.product_text as pdt_code, exr.product_description as pdt_description, excr.exr_code, soit.category_id');
            $this->db->join($this->services_inventory_items . ' as serinv', 'serinv.id = soit.item_id and serinv.form_type = 1 and serinv.status = 1');
            $this->db->join($this->exchange_order_items . ' as exr', 'exr.id = serinv.item_id', 'left');
            $this->db->join($this->exchange_order . ' as excr', 'excr.id = exr.exr_id', 'left');
        } else if ($type == 2) {
            $this->db->select('p.id as item_auto_id, p.pdt_code, p.pdt_name, p.pdt_description, soit.category_id');
            $this->db->join($this->services_inventory_items . ' as serinv', 'serinv.id = soit.item_id and serinv.form_type = 2 and serinv.status = 1');
            $this->db->join($this->items . ' as p', 'p.id = serinv.item_id', 'left');
        } else if ($type == 3) {
            $this->db->select('soit.category_id', FALSE);
        }
        $this->db->where('soit.service_form_type', $type);
        $this->db->where('soit.so_id', $so_id);
        $this->db->where('soit.status !=', 10);
        return $this->db->get($this->sales_order_item . ' as soit')->result();
    }

    public function get_external_services_item($serv_id) {
        $this->db->select('extserv.*, extserv.exter_service_desc as product_description, extserv.external_sub_amount as total', FALSE);
        $this->db->where('extserv.service_id', $serv_id);
        $this->db->where('status != 2');
        $this->db->where('status != 10');
        return $this->db->get($this->services_exter_item . ' AS extserv')->result();
    }

    public function get_exc_products_by_so($so_id) {
        $this->db->select('soit.id, soit.price,soit.serial_no,soit.product_description,soit.unit_cost, soit.quantity, soit.discount, soit.discount_type, soit.discount_amount, soit.total, soit.item_id, exr.product_text as pdt_code, exr.product_description as pdt_description, soit.category_id');
        $this->db->join($this->exchange_order_items . ' as exr', 'exr.id = soit.item_id', 'left');
        $this->db->where('soit.so_id', $so_id);
        $this->db->where('soit.status !=', 10);
        return $this->db->get($this->sales_order_item . ' as soit')->result();
    }

    public function shipping_address_by_id($address_id) {
        $this->db->where('id', $address_id);
        return $this->db->get($this->ship_address)->row();
    }

    public function get_item_details($mat_id, $so_id) {
        $this->db->select('soit.*');
        $this->db->where('soit.product_id', $mat_id);
        $this->db->where('soit.so_id', $so_id);
        return $this->db->get($this->purchase_items . ' as soit')->row();
    }

    public function check_mat_already_exists($mat_id, $so_id) {
        $this->db->select('SUM(prit.pri_quantity) as act_quantity');
        $this->db->join($this->purchase_receive_items . ' as prit', 'prit.pr_id = pr.id', 'left');
        $this->db->where('prit.product_id', $mat_id);
        $this->db->where('pr.so_id', $so_id);
        $this->db->where('pr.pr_status !=', 10);
        return $this->db->get($this->purchase_receive . ' as pr')->row();
    }

    public function insert($data) {
        $this->db->insert($this->sales_order, $data);
        return $this->db->insert_id();
    }

    public function insert_item($data) {
        $this->db->insert($this->sales_order_item, $data);
        return $this->db->insert_id();
    }

    public function insert_hold_so($data) {
        $this->db->insert($this->sales_order_hold, $data);
        return $this->db->insert_id();
    }

    public function insert_item_hold_so($data) {
        $this->db->insert($this->sales_order_item_hold, $data);
        return $this->db->insert_id();
    }

    public function branch_materials($brn, $prt) {
        $this->db->select('bp.available_qty', FALSE);
        $this->db->where('bp.brn_id', $brn);
        $this->db->where('bp.product_id', $prt);
        $this->db->where('bp.status', 1);
        return $this->db->get($this->branch_products . ' as bp');
    }

    public function insert_brn_material($data) {
        $this->db->insert($this->branch_products, $data);
        return $this->db->insert_id();
    }

    public function update_items($data, $id) {
        $this->db->where('so_id', $id);
        $this->db->update($this->sales_order_item, $data);
        return $this->db->affected_rows();
    }

    public function update_item($data, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->sales_order_item, $data);
        return $this->db->affected_rows();
    }

    public function delete_so_item($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->sales_order_item);
        return $this->db->affected_rows();
    }

    public function delete_so_hold($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->sales_order_hold);
        return $this->db->affected_rows();
    }

    public function delete_so_hold_item($id) {
        $this->db->where('so_id', $id);
        $this->db->delete($this->sales_order_item_hold);
        return $this->db->affected_rows();
    }

    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->sales_order, $data);
        return $this->db->affected_rows();
    }

    public function get_so_item($so_item_id) {
        $this->db->where('id', $so_item_id);
        $this->db->where('status !=', 10);
        return $this->db->get($this->sales_order_item)->row();
    }

    public function get_removed_so_items($so_id, $product_ids = array()) {
        $this->db->where('so_id', $so_id);
        if (!empty($product_ids))
            $this->db->where_not_in('item_id', $product_ids);

        return $this->db->get($this->sales_order_item)->result();
    }

    public function get_removed_so_excitems($so_id, $product_ids = array()) {
        $this->db->where('so_id', $so_id);
        $this->db->where('exc_form_type = 2');
        if (!empty($product_ids))
            $this->db->where_not_in('item_id', $product_ids);

        return $this->db->get($this->sales_order_item)->result();
    }

    public function get_removed_so_items_service($so_id, $product_ids = array()) {
        $this->db->where('so_id', $so_id);
        $this->db->where('service_form_type = 2');
        if (!empty($product_ids))
            $this->db->where_not_in('item_id', $product_ids);

        return $this->db->get($this->sales_order_item)->result();
    }

    public function get_removed_so_items_excbytype($type, $so_id, $product_ids = array()) {
        $this->db->where('so_id', $so_id);
        $this->db->where('exc_form_type', $type);
        if (!empty($product_ids))
            $this->db->where_not_in('id', $product_ids);

        return $this->db->get($this->sales_order_item)->result();
    }

    public function get_removed_so_items_servicebytype($type, $so_id, $product_ids = array()) {
        $this->db->where('so_id', $so_id);
        $this->db->where('service_form_type', $type);
        if (!empty($product_ids))
            $this->db->where_not_in('id', $product_ids);

        return $this->db->get($this->sales_order_item)->result();
    }

    public function get_notification_user_list() {
        $this->db->select('user.*, CASE user.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('user.push_notify', 1);
        $this->db->where('user.status != 10');
        $this->db->where_not_in('id', $this->user_data['user_id']);
        $this->db->order_by('user.id', 'desc');
        return $this->db->get($this->user . ' AS user')->result_array();
    }

    public function insert_notification_items($data) {
        $this->db->insert($this->notification, $data);
        return $this->db->insert_id();
    }

    public function update_notification_item($data, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->notification, $data);
        return $this->db->affected_rows();
    }

    public function get_notification_items($type = NULL) {
        $user_id = $this->user_data['user_id'];
        $status = ($type == 'all') ? array(1, 2) : array(1);
        $invoice_not = $this->get_notification_by_type('Invoice', '', $user_id, $status, '');
        return $invoice_not;
    }

    public function get_notification_by_type($type_txt = NULL, $type_id = NULL, $user_id = NULL, $status = NULL, $id = NULL) {
        if (!empty($type_txt) && $type_txt == 'Invoice') {
            $this->db->select('ntf.id as not_id,so.id as auto_id, so.so_no as running_num_txt, so.total_amt as amount_txt, brn.branch_name, "Invoice" as type_txt, CASE so.so_type WHEN 1 THEN c.name ELSE "Cash Customer" END as name_txt', FALSE);
            $this->db->select('"' . base_url() . 'invoice/view/" as url_txt', FALSE);
            $this->db->select("DATE_FORMAT(so.so_date, '%d/%m/%Y') as date_str", FALSE);
            $this->db->join($this->sales_order . ' as so', 'so.id = ntf.type_id');
            $this->db->join($this->customer . ' as c', 'c.id = so.customer_id', 'left');
            $this->db->join($this->branches . ' as brn', 'brn.id = so.branch_id');
        } else {
            $this->db->select('ntf.*', FALSE);
        }
        if (!empty($type_txt)) {
            $this->db->where('ntf.type_txt', $type_txt);
        }
        if (!empty($type_id)) {
            $this->db->where('ntf.type_id', $type_id);
        }
        if (!empty($user_id)) {
            $this->db->where('ntf.user_id', $user_id);
        }
        if (!empty($status)) {
            $this->db->where_in('ntf.status', $status);
        }
        if (!empty($id)) {
            $this->db->where('ntf.id', $id);
        }
        $this->db->where('ntf.status !=', 10);
        $this->db->where_in('so.branch_id', $this->user_data['branch_ids']);
        $this->db->order_by('ntf.id', 'desc');
        return $this->db->get($this->notification . ' AS ntf')->result_array();
    }

    public function list_search_all($from = '', $to = '', $customer_id = '') {
        $user_data = get_user_data();
        $this->db->select('SUM(so.total_amt) as so_amt, SUM(sp.pay_amt) as paid_amt, sp.pay_date, so.customer_id, CASE so.so_type WHEN 1 THEN c.name ELSE "Cash Customer" END as name, c.address', FALSE);
        $this->db->join($this->sales_payment . ' as sp', 'sp.so_id = so.id AND sp.payment_status = 1', 'left');
        $this->db->join($this->customer . ' as c', 'c.id = so.customer_id', 'left');
        $this->db->where('so.status !=', 10);
        $this->db->group_by('so.customer_id');
        $this->db->where_in('so.branch_id', $this->user_data['branch_ids']);
        if ((!empty($customer_id))) {
            $this->db->where('so.customer_id', $customer_id);
        }
        if (!empty($from) && !empty($to)) {
            $this->db->where('(so.so_date BETWEEN "' . date('Y-m-d H:i:s', strtotime($from)) . '" and "' . date('Y-m-d H:i:s', strtotime($to)) . '" or sp.pay_date BETWEEN "' . date('Y-m-d H:i:s', strtotime($from)) . '" and "' . date('Y-m-d H:i:s', strtotime($to)) . '")');
        }
        $this->db->order_by('name, so_amt');
        return $this->db->get($this->sales_order . ' as so')->result();
    }

//    public function list_search_all($from = '', $to = '', $customer_id = '') {
//        $user_data = get_user_data();
//        $this->db->select('SUM(so.total_amt) AS so_amt,SUM(sp.pay_amt) AS paid_amt,sp.pay_date, so.customer_id,c.name,c.address,sp.*', FALSE);
//        $this->db->join($this->sales_order . ' as so', 'sp.so_id = so.id', 'left');
//        $this->db->join($this->customer . ' as c', 'c.id = so.customer_id', 'left');
//        $this->db->where('sp.payment_status', 1);
//        $this->db->group_by('so.customer_id');
//        $this->db->where_in('so.branch_id', $this->user_data['branch_ids']);
//        if ((!empty($customer_id))) {
//            $this->db->where('so.customer_id', $customer_id);
//        }
//        if (!empty($from) && !empty($to)) {
//            $this->db->where('(so.so_date BETWEEN "' . date('Y-m-d H:i:s', strtotime($from)) . '" and "' . date('Y-m-d H:i:s', strtotime($to)) . '" or sp.pay_date BETWEEN "' . date('Y-m-d H:i:s', strtotime($from)) . '" and "' . date('Y-m-d H:i:s', strtotime($to)) . '")');
//        }
//        $this->db->order_by('c.id', 'desc');
//        return $this->db->get($this->sales_payment . ' as sp')->result();
//    }

    public function get_sales_by_id($id = '', $from = '', $to = '') {
        $this->db->select('so.id,so.so_no, so.total_amt, so.so_date,so.payment_status, IF(payt.is_cheque = 1 AND sp.pay_pay_no != "", CONCAT(payt.type_name, "&nbsp;/ ", sp.pay_pay_bank, "&nbsp;- ", sp.pay_pay_no), payt.type_name) as type_name, CASE so.payment_status WHEN 1 THEN "Paid" WHEN 3 THEN "Cancelled" WHEN 4 THEN "Partially Paid" ELSE "UnPaid" END as status_str, sp.so_id,sp.pay_date, sp.pay_amt,', FALSE);
        $this->db->join($this->sales_payment . ' as sp', 'sp.so_id = so.id AND sp.status != 10', 'left');
        $this->db->join($this->payment_type . ' as payt', 'payt.id = sp.pay_pay_type', 'left');
        $this->db->where('so.status !=', 10);
        if (!empty($from) && !empty($to)) {
            $this->db->where('(so.so_date BETWEEN "' . date('Y-m-d H:i:s', strtotime($from)) . '" and "' . date('Y-m-d H:i:s', strtotime($to)) . '" or sp.pay_date BETWEEN "' . date('Y-m-d H:i:s', strtotime($from)) . '" and "' . date('Y-m-d H:i:s', strtotime($to)) . '")');
        }
        if ((!empty($id))) {
            $this->db->where('so.customer_id', $id);
        } else {
            $this->db->where('so.so_type', 2);
        }
        return $this->db->get($this->sales_order . ' as so')->result();
    }

    public function get_customer_by_id($id = '', $from = '', $to = '') {
        $user_data = get_user_data();
        $this->db->select('SUM(so.total_amt) as so_amt, SUM(sp.pay_amt) as paid_amt, sp.pay_date, so.customer_id,c.name,c.address,c.email,c.phone_no,c.contact_name', FALSE);
        $this->db->join($this->sales_payment . ' as sp', 'sp.so_id = so.id AND sp.payment_status = 1', 'left');
        $this->db->join($this->customer . ' as c', 'c.id = so.customer_id', 'left');
        $this->db->where('so.status !=', 10);
//        if($user_data['is_admin']!='1'){
//            $this->db->where_in('so.brn_id', $user_data['branch_ids']);
//        }
        $this->db->where_in('so.branch_id', $this->user_data['branch_ids']);
        if (!empty($from) && !empty($to)) {
            $this->db->where('(so.so_date BETWEEN "' . date('Y-m-d H:i:s', strtotime($from)) . '" and "' . date('Y-m-d H:i:s', strtotime($to)) . '" or sp.pay_date BETWEEN "' . date('Y-m-d H:i:s', strtotime($from)) . '" and "' . date('Y-m-d H:i:s', strtotime($to)) . '")');
        }
        if ((!empty($id))) {
            $this->db->where('so.customer_id', $id);
            $this->db->where('c.id', $id);
            $this->db->group_by('c.id');
            $this->db->order_by('c.id', 'desc');
        } else {
            $this->db->where('so.so_type', 2);
        }
        return $this->db->get($this->sales_order . ' as so')->row();
    }

    public function list_invoice_summary($from_date = '', $to_date = '', $cus = '', $issued_by = '', $pay = '', $branch_id = '', $generated_from = '') {
        $this->db->select('so.id, so.so_no, so.sub_total, so.gst_amount, so.discount_amount,so.total_amt, CASE so.gst WHEN 1 THEN SUM(sitem.item_profit) - so.discount_amount ELSE SUM(sitem.item_profit) - so.discount_amount END AS tot_profit, CASE so.so_type WHEN 1 THEN cus.name ELSE so.customer_name END as name, brn.branch_name,CASE so.status WHEN 3 THEN "Delivered" WHEN 2 THEN "Partial Delivered" ELSE "Not Delivered" END as del_sts, CASE so.payment_status WHEN 1 THEN "Paid" WHEN 3 THEN "Cancelled" WHEN 4 THEN "Partially Paid" ELSE "UnPaid" END as pay_sts, u.name as issued_by', FALSE);
        $this->db->select("DATE_FORMAT(so.so_date, '%d/%m/%Y') as so_date_str", FALSE);
        $this->db->select("DATE_FORMAT(so.do_date, '%d/%m/%Y') as do_date_str", FALSE);
		$this->db->select('CONCAT(CASE so.invoice_from WHEN 1 THEN so.your_reference WHEN 2 THEN (SELECT quo_no FROM ' . $this->quotation . ' as q WHERE q.id = so.quotation_id) WHEN 3 THEN (SELECT exr_code FROM ' . $this->exchange_order . ' as eo WHERE eo.id = so.exchange_order_id) WHEN 4 THEN (SELECT jobsheet_number FROM ' . $this->services . ' as s WHERE s.id = so.service_id)' . ' END, " - ", so.your_reference) as your_reference', FALSE);
        if ((!empty($from_date))) {
            $this->db->where('DATE(so.so_date) >=', date('Y-m-d', strtotime($from_date)));
        }
        if ((!empty($to_date))) {
            $this->db->where('DATE(so.so_date) <=', date('Y-m-d', strtotime($to_date)));
        }

        if ((!empty($cus)) && ($cus == 'cash')) {
            $this->db->where('so.so_type', 2);
        } else if ((!empty($cus))) {
            $this->db->where('so.customer_id', $cus);
        }

        if ((!empty($issued_by))) {
            $this->db->where('so.created_by', $issued_by);
        }
        if ((!empty($pay))) {
            $this->db->where('so.payment_status', $pay);
        }

        if (!empty($branch_id)) {
            $this->db->where('so.branch_id', $branch_id);
        } else {
            $this->db->where_in('so.branch_id', $this->user_data['branch_ids'], FALSE);
        }

        if (!empty($generated_from)) {
            $this->db->where('so.invoice_from', $generated_from);
        }

        $this->db->where('so.status !=', 10);
        $this->db->order_by('so.id', 'desc');
        $this->db->group_by('so.id');
        $this->db->join($this->sales_order_item . ' as sitem', 'sitem.so_id = so.id', 'left');
        $this->db->join($this->customer . ' as cus', 'cus.id = so.customer_id', 'left');
        $this->db->join($this->branches . ' as brn', 'brn.id = so.branch_id', 'left');
        $this->db->join($this->user . ' as u', 'u.id = so.created_by', 'left');
        return $this->db->get($this->sales_order . ' AS so')->result();
    }

    public function list_daily_report_summary($from_date = '', $to_date = '', $brn = '') {
        $this->db->select('so.id, so.so_no, so.sub_total, so.gst_amount, so.discount_amount,so.total_amt, CASE so.so_type WHEN 1 THEN cus.name ELSE so.customer_name END as name,brn.branch_name, CASE so.gst WHEN 1 THEN SUM(sitem.item_profit) - so.discount_amount ELSE SUM(sitem.item_profit) - so.discount_amount END AS tot_profit,CASE so.status WHEN 3 THEN "Delivered" WHEN 2 THEN "Partial Delivered"  ELSE "Not Delivered" END as del_sts,CASE so.payment_status WHEN 1 THEN "Paid" WHEN 3 THEN "Cancelled" WHEN 4 THEN "Partially Paid" ELSE "UnPaid" END as pay_sts', FALSE);
        $this->db->select("DATE_FORMAT(so.so_date, '%d/%m/%Y') as so_date_str", FALSE);
        if ((!empty($from_date))) {
            $this->db->where('DATE(so.created_on) >=', date('Y-m-d', strtotime($from_date)));
        }
        if ((!empty($to_date))) {
            $this->db->where('DATE(so.created_on) <=', date('Y-m-d', strtotime($to_date)));
        }
        if ((!empty($brn))) {
            $this->db->where('so.branch_id', $brn);
        } else {
            $this->db->where_in('so.branch_id', $this->user_data['branch_ids']);
        }
        $this->db->where('so.status !=', 10);
        $this->db->order_by('so.id', 'desc');
        $this->db->group_by('so.id');
        $this->db->join($this->customer . ' as cus', 'cus.id = so.customer_id', 'left');
        $this->db->join($this->branches . ' as brn', 'brn.id = so.branch_id', 'left');
        $this->db->join($this->sales_order_item . ' as sitem', 'sitem.so_id = so.id', 'left');
        return $this->db->get($this->sales_order . ' AS so')->result();
    }

    public function daily_report_breakdown_paid($from_date = '', $to_date = '', $brn = '') {
        $this->db->select('id, type_name');
        $this->db->where('status !=', 10);
        $payment_types = $this->db->get($this->payment_type)->result();
        foreach ($payment_types as $key => $value) {
            $this->db->select('SUM(sp.pay_amt) as pay_amt', FALSE);
            $this->db->join($this->sales_payment . ' as sp', 'sp.so_id = so.id AND sp.status != 10 AND sp.pay_pay_type = ' . $value->id, 'left');
            $this->db->join($this->branches . ' as brn', 'brn.id = so.branch_id', 'left');
            if ((!empty($from_date))) {
                $this->db->where('DATE(so.created_on) >=', date('Y-m-d', strtotime($from_date)));
            }
            if ((!empty($to_date))) {
                $this->db->where('DATE(so.created_on) <=', date('Y-m-d', strtotime($to_date)));
            }
            if ((!empty($brn))) {
                $this->db->where('so.branch_id', $brn);
            } else {
                $this->db->where_in('so.branch_id', $this->user_data['branch_ids']);
            }
            $this->db->where('so.status !=', 10);
            $value->pay_amt = $this->db->get($this->sales_order . ' AS so')->row()->pay_amt;
        }

        return $payment_types;
    }

    public function daily_report_breakdown($from_date = '', $to_date = '', $brn = '') {
        $this->db->select('SUM(so.total_amt) as total_amt', FALSE);
        $this->db->join($this->branches . ' as brn', 'brn.id = so.branch_id', 'left');
        if ((!empty($from_date))) {
            $this->db->where('DATE(so.created_on) >=', date('Y-m-d', strtotime($from_date)));
        }
        if ((!empty($to_date))) {
            $this->db->where('DATE(so.created_on) <=', date('Y-m-d', strtotime($to_date)));
        }
        if ((!empty($brn))) {
            $this->db->where('so.branch_id', $brn);
        } else {
            $this->db->where_in('so.branch_id', $this->user_data['branch_ids']);
        }
        $this->db->where('so.status !=', 10);

        return $this->db->get($this->sales_order . ' AS so')->row()->total_amt;
    }

    public function profit_loss_summary($from_date = '', $to_date = '', $cus = '', $issued_by = '', $branch_id = '', $pay = '') {
        $this->db->select('so.id, so.so_no, so.your_reference, so.sub_total, so.gst_amount, so.discount_amount,so.total_amt,so.payment_status, CASE so.so_type WHEN 1 THEN cus.name ELSE so.customer_name END as name,brn.branch_name, CASE so.gst WHEN 1 THEN SUM(sitem.item_profit) - so.discount_amount ELSE SUM(sitem.item_profit) - so.discount_amount END AS tot_profit,CASE so.status WHEN 3 THEN "Delivered" WHEN 2 THEN "Partial Delivered"  ELSE "Not Delivered" END as del_sts,CASE so.payment_status WHEN 1 THEN "Paid" WHEN 3 THEN "Cancelled" WHEN 4 THEN "Partially Paid" ELSE "UnPaid" END as pay_sts, u.name as issued_by', FALSE);
        $this->db->select("DATE_FORMAT(so.so_date, '%d/%m/%Y') as so_date_str", FALSE);
        if (!empty($from_date)) {
            $this->db->where('DATE(so.so_date) >=', date('Y-m-d', strtotime($from_date)));
        }
        if (!empty($to_date)) {
            $this->db->where('DATE(so.so_date) <=', date('Y-m-d', strtotime($to_date)));
        }
        if ((!empty($cus))) {
            $this->db->where('so.customer_id', $cus);
        }
        if (!empty($issued_by)) {
            $this->db->where('so.created_by', $issued_by);
        }
        if (!empty($branch_id)) {
            $this->db->where('so.branch_id', $branch_id);
        }
        if (!empty($pay)) {
            $this->db->where('so.payment_status', $pay);
        }
        $this->db->where_in('so.branch_id', $this->user_data['branch_ids']);
        $this->db->where('so.status !=', 10);
        $this->db->order_by('so.id', 'desc');
        $this->db->group_by('so.id');
        $this->db->join($this->customer . ' as cus', 'cus.id = so.customer_id', 'left');
        $this->db->join($this->branches . ' as brn', 'brn.id = so.branch_id', 'left');
        $this->db->join($this->sales_order_item . ' as sitem', 'sitem.so_id = so.id', 'left');
        $this->db->join($this->user . ' as u', 'u.id = so.created_by', 'left');
        return $this->db->get($this->sales_order . ' AS so')->result();
    }

    public function get_static_pay_type_by_customer($from_date = '', $to_date = '', $cus = '', $brn = '') {
        $payment_types = payment_terms();
        $i = 0;
        $aa = array();
        foreach ($payment_types as $key => $value) {
            $aa[$key]['title'] = $value;
            $this->db->select('SUM(so.total_amt) as pay_amt', FALSE);
            $this->db->join($this->branches . ' as brn', 'brn.id = so.branch_id', 'left');
            if ((!empty($from_date))) {
                $this->db->where('so.so_date >=', date('Y-m-d H:i:s', strtotime($from_date)));
            }
            if ((!empty($to_date))) {
                $this->db->where('so.so_date <=', date('Y-m-d 23:59:59', strtotime($to_date)));
            }
            if ((!empty($cus))) {
                $this->db->where('so.customer_id', $cus);
            }
            if ((!empty($brn))) {
                $this->db->where('so.branch_id', $brn);
            } else {
                $this->db->where_in('so.branch_id', $this->user_data['branch_ids']);
            }
            $this->db->where('so.payment_status !=1 AND so.payment_status !=3');
            $this->db->where('so.status !=', 10);
            $this->db->where('so.payment_terms', $value);
            $sale_amt = $this->db->get($this->sales_order . ' AS so')->row()->pay_amt;
            $aa[$key]['amt'] = $sale_amt;
        }
        return $aa;
    }

    public function pending_account_summary($from_date = '', $to_date = '', $cus = '') {
        $this->db->select('SUM(so.total_amt) as pay_amt', FALSE);
        $this->db->join($this->branches . ' as brn', 'brn.id = so.branch_id', 'left');
        if ((!empty($from_date))) {
            $this->db->where('so.so_date >=', date('Y-m-d H:i:s', strtotime($from_date)));
        }
        if ((!empty($to_date))) {
            $this->db->where('so.so_date <=', date('Y-m-d 23:59:59', strtotime($to_date)));
        }
        if ((!empty($cus))) {
            $this->db->where('so.customer_id', $cus);
        }
        if ((!empty($brn))) {
            $this->db->where('so.branch_id', $brn);
        } else {
            $this->db->where_in('so.branch_id', $this->user_data['branch_ids']);
        }
        $this->db->where('so.payment_status !=1 AND so.payment_status !=3');
        $this->db->where('so.status !=', 10);
        // $this->db->where('so.payment_terms', $value);
        return $this->db->get($this->sales_order . ' AS so')->row()->pay_amt;
    }

    public function day_end_total() {
        $this->db->select('SUM(so.total_amt) as total_amt', FALSE);
        $this->db->where('DATE(so.created_on) >=', date('Y-m-d'));
        $this->db->where('so.created_on <=', date('Y-m-d H:i:s'));

        $this->db->where('so.status !=', 10);
        return $this->db->get($this->sales_order . ' AS so')->row()->total_amt;
    }

    public function get_static_pay_type($from_date = '', $to_date = '', $brn = '') {
        $payment_types = payment_terms();
        $i = 0;
        $aa = array();
        foreach ($payment_types as $key => $value) {
            $aa[$key]['title'] = $value;
            $this->db->select('SUM(so.total_amt) as pay_amt', FALSE);
            //$this->db->join($this->sales_payment . ' as sp', 'sp.so_id = so.id AND sp.status != 10 AND sp.pay_pay_type = ' . $value->id, 'left');
            $this->db->join($this->branches . ' as brn', 'brn.id = so.branch_id', 'left');
            if ((!empty($from_date))) {
                $this->db->where('so.so_date >=', date('Y-m-d H:i:s', strtotime($from_date)));
            }
            if ((!empty($to_date))) {
                $this->db->where('so.so_date <=', date('Y-m-d H:i:s', strtotime($to_date)));
            }
            if ((!empty($brn))) {
                $this->db->where('so.branch_id', $brn);
            } else {
                $this->db->where_in('so.branch_id', $this->user_data['branch_ids']);
            }
            $this->db->where('so.status !=', 10);
            $this->db->where('so.payment_terms', $value);
            $sale_amt = $this->db->get($this->sales_order . ' AS so')->row()->pay_amt;
            $aa[$key]['amt'] = $sale_amt;
        }
        return $aa;
    }

    public function check_your_reference($your_reference, $invoice_id) {
        if (!empty($invoice_id)) {
            $this->db->where('so.id !=', $invoice_id);
        }
        $this->db->where('so.status !=', 10);
        $this->db->where('so.your_reference', $your_reference);
        return $this->db->get($this->sales_order . ' AS so')->result();
    }

    public function insert_invoice_mail($data) {
        $this->db->insert($this->invoice_email_sent, $data);
        return $this->db->insert_id();
    }

    public function insert_notification($data) {
        $this->db->insert($this->notification, $data);
        return $this->db->insert_id();
    }

}

?>