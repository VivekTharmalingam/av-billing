<?php

class Exchange_order_payment_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->suppliers = TBL_SUP;
        $this->purchase_receive = TBL_PR;
        $this->exchange_order = TBL_EXR;
        $this->exchange_order_payment = TBL_EXRPM;
        $this->payment_type = TBL_PAYMENT_TYPE;
    }

    public function list_all() {
        $this->db->select('exr.id, DATE_FORMAT(exr.exr_date, "%d/%m/%Y") as exr_date_str, exr.exr_net_amount, COALESCE(SUM(pay.pay_amt), 0) as paid_amount, CASE exr.payment_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as payment_status, s.name', FALSE);
        $this->db->join($this->exchange_order . ' as exr', 'exr.id = pay.exr_id AND exr.exr_status != 10');
        $this->db->join($this->suppliers . ' as s', 's.id = exr.sup_id');
        $this->db->where('pay.status !=', 10);
        $this->db->where_in('pay.brn_id', $this->user_data['branch_id']);
        $this->db->order_by('pay.id', 'desc');
        $this->db->group_by('pay.exr_id');
        return $this->db->get($this->exchange_order_payment . ' AS pay')->result();
    }

    public function get_unpaid_purchases_by_supplier($supplier_id) {
        $this->db->select('exr.id, exr.exr_code, DATE_FORMAT(exr.exr_date, "%d/%m/%Y") as exr_date_str, exr.exr_net_amount, COALESCE(SUM(pay.pay_amt), 0) as paid_amount, CASE exr.payment_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as payment_status', FALSE);
        $this->db->join($this->exchange_order_payment . ' as pay', 'pay.exr_id = exr.id AND pay.status != 10', 'left');
        $this->db->where('exr.exr_status !=', 10);
        $this->db->where('exr.payment_status !=', 3);
        $this->db->where('exr.sup_id', $supplier_id);
        $this->db->where_in('exr.branch_id', $this->user_data['branch_id']);
        $this->db->order_by('exr.exr_date', 'asc');
        $this->db->group_by('exr.id');
        return $this->db->get($this->exchange_order . ' AS exr')->result();
    }
    
    public function insert($data) {
        $this->db->insert($this->exchange_order_payment, $data);
        return $this->db->insert_id();
    }
    
    public function update_exc_order_payment_status($exr_id, $data) {
        $this->db->where('id', $exr_id);
        $this->db->update($this->exchange_order, $data);
        return $this->db->affected_rows();
    }
    
    public function get_by_id($id) {
        $this->db->select('exr.*, DATE_FORMAT(exr.exr_date, "%d/%m/%Y") as exr_date_str, CASE exr.payment_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as payment_status, s.name', FALSE);
        $this->db->where('exr.id', $id);
        $this->db->join($this->suppliers . ' as s', 's.id = exr.sup_id');
        return $this->db->get($this->exchange_order . ' AS exr')->row();
    }
    
    public function get_payment_by_excord_id($id) {
        $this->db->select('pay.*, DATE_FORMAT(pay.pay_date, "%d-%m-%Y") as payment_date, pt.type_name, pt.is_cheque', FALSE);
        $this->db->join($this->payment_type . ' as pt', 'pt.id = pay.pay_pay_type', 'left');
        $this->db->where('pay.status !=', 10);
        $this->db->where('pay.exr_id', $id);
        $this->db->where_in('pay.brn_id', $this->user_data['branch_id']);
        $this->db->order_by('pay.id');
        return $this->db->get($this->exchange_order_payment . ' AS pay')->result();
    }
    
    public function get_payment_by_pay_id($id) {
        $this->db->select('pay.*, exrd.exr_code, DATE_FORMAT(pay.pay_date, "%d-%m-%Y") as payment_date, pt.type_name, pt.is_cheque', FALSE);
        $this->db->join($this->payment_type . ' as pt', 'pt.id = pay.pay_pay_type', 'left');
        $this->db->join($this->exchange_order . ' as exrd', 'exrd.id = pay.exr_id', 'left');
        $this->db->where('pay.status !=', 10);
        $this->db->where('pay.id', $id);
        $this->db->where_in('pay.brn_id', $this->user_data['branch_id']);
        $this->db->order_by('pay.id');
        return $this->db->get($this->exchange_order_payment . ' AS pay')->row();
    }
    
    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->exchange_order_payment, $data);
        return $this->db->affected_rows();
    }
    
    public function get_excord_pay_by_id($id) {
		$this->db->select('pay.*, pt.type_name, pt.group_name', FALSE);
		$this->db->join($this->payment_type . ' as pt', 'pt.id = pay.pay_pay_type', 'left');
        $this->db->where('pay.id', $id);
        return $this->db->get($this->exchange_order_payment . ' AS pay')->row();
    }
    
}

?>