<?php

class Exchange_Order_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->suppliers = TBL_SUP;
        $this->branches = TBL_BRN;
        $this->items = TBL_PDT;
        $this->category = TBL_CAT;
        $this->units = TBL_UNIT;
        $this->company = TBL_COMP;
        $this->purchases = TBL_PO;
        $this->purchase_items = TBL_POIT;
        $this->exchange_order = TBL_EXR;
        $this->exchange_order_items = TBL_EXRIT;
        $this->purchase_payment = TBL_PPM;
        //$this->inventory_product = TBL_INV_PDT;
        $this->branch_products = TBL_BRN_PDT;
        $this->sales_order = TBL_SO;
        $this->sales_order_item = TBL_SO_ITM;
        $this->services = TBL_SJST;
        $this->services_inventory_items = TBL_SEXINITM;
		$this->user = TBL_USER;
        $this->user_data = get_user_data();
    }

    public function list_all() {
        $this->db->select('exr.id, brn.branch_name, exr.exr_code, DATE_FORMAT(exr.exr_date, "%d/%m/%Y") as exr_date_str, sup.name, CASE exr.payment_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as payment_status', FALSE);
	$this->db->join($this->suppliers . ' as sup', 'sup.id = exr.sup_id');
        $this->db->join($this->branches . ' as brn', 'brn.id = exr.branch_id');
        $this->db->where('exr.exr_status !=', 10);
        $this->db->order_by('exr.id', 'desc');
        $this->db->where_in('exr.branch_id', $this->user_data['branch_id']);
        return $this->db->get($this->exchange_order . ' as exr')->result();
    }
    
    public function insert($data) {
        $this->db->insert($this->exchange_order, $data);
        return $this->db->insert_id();
    }

    public function insert_item($data) {
        $this->db->insert($this->exchange_order_items, $data);
        return $this->db->insert_id();
    }
    
    public function get_by_id($id) {
        $this->db->select('exr.*, sup.name, sup.gst_type, sup.contact_name, sup.phone_no, sup.email_id, sup.address, brn.branch_name, CASE exr.exr_status WHEN 1 THEN "Active" WHEN 2 THEN "InActive" WHEN 3 THEN "Others" ELSE "" END as status_str, CASE exr.payment_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as payment_status, DATE_FORMAT(exr.exr_date, "%d/%m/%Y") as exr_date_str, u.name AS uname',FALSE);
		$this->db->join($this->user . ' as u', 'u.id = exr.created_by', 'left');
        $this->db->join($this->branches . ' as brn', 'brn.id = exr.branch_id','left');
        $this->db->join($this->suppliers . ' as sup', 'sup.id = exr.sup_id','left');
        $this->db->where('exr.id', $id);
        return $this->db->get($this->exchange_order . ' as exr')->row();
    }
    
    public function get_item_by_id($id) {
        $this->db->select('exri.*');
        $this->db->where('exri.exr_id', $id);
        $this->db->where('exri.status !=', 2);
        $this->db->where('exri.status !=', 10);
        return $this->db->get($this->exchange_order_items . ' as exri')->result();
    }
    
    public function get_exchange_order_item_by_id($id) {
        $this->db->select('exri.*');
        $this->db->where('exri.id', $id);
        return $this->db->get($this->exchange_order_items . ' as exri')->row();
    }
    
    public function get_item_by_excids($excid) {
         $this->db->select('exri.*, exr.exr_code');
        $this->db->join($this->exchange_order . ' as exr', 'exr.id = exri.exr_id');
        $this->db->where_in('exri.exr_id', $excid);
        $this->db->where('exri.status !=', 2);
        $this->db->where('exri.status !=', 10);
        return $this->db->get($this->exchange_order_items . ' as exri')->result();
    }
    
    public function get_edit_invitemids($so_id) {
        $this->db->select('soit.*');
        $this->db->where('soit.so_id', $so_id);
        $this->db->where('soit.status !=', 10);
        $inv_itm = $this->db->get($this->sales_order_item . ' as soit')->result();
        $all_id=array();
        foreach ($inv_itm as $key => $value) {
            $all_id[]=$value->id;
        }
        $ids_txt = (!empty($all_id)) ? implode(',', $all_id) : '';
        return $ids_txt;
    }
    
    public function get_edit_seritemids($ser_id) {
        $this->db->select('itm.*', FALSE);
        $this->db->where('itm.service_id', $ser_id);
        $this->db->where('itm.form_type = 1'); 
        $this->db->where('itm.status != 2'); 
        $this->db->where('itm.status != 10'); 
        $this->db->order_by('itm.id', 'asc');
        $inv_itm = $this->db->get($this->services_inventory_items . ' AS itm')->result();
        $all_id=array();
        foreach ($inv_itm as $key => $value) {
            $all_id[]=$value->id;
        }
        $ids_txt = (!empty($all_id)) ? implode(',', $all_id) : '';
        return $ids_txt;
    }
    
    public function get_filtered_item_by_excids($excid=NULL,$edit_page=NULL,$edit_page_id=NULL) {
        
        $so_qty  = '(SELECT SUM(soit.quantity) FROM ' . $this->sales_order_item . ' as soit ';
        $so_qty .= ' WHERE soit.status != 10 AND soit.exc_form_type = 1';
        if($edit_page == 'invoice') {
            $inv_itm_id = $this->get_edit_invitemids($edit_page_id);    
            $so_qty .= ' AND `soit`.`id` NOT IN ('.$inv_itm_id.')';
        }
        $so_qty .= ' AND soit.item_id = exritm.id) as invoice_qty';
        
        $service_qty  = '(SELECT SUM(serinv.quantity) FROM ' . $this->services_inventory_items . ' as serinv ';
        $service_qty .= ' WHERE serinv.form_type = 1 AND serinv.status = 1';
        if($edit_page == 'service') {
            $ser_itm_id = $this->get_edit_seritemids($edit_page_id);    
            $service_qty .= ' AND `serinv`.`id` NOT IN ('.$ser_itm_id.')';
        }
        $service_qty .= ' AND serinv.item_id = exritm.id) as service_qty';
        
        $select_txt   = 'exritm.*, excr.exr_code, sup.name, IF(exritm.exr_quantity IS NOT NULL AND exritm.exr_quantity != "", exritm.exr_quantity, "0") as exr_quantity';
        
        $excitem_qry  = 'SELECT '.$select_txt.', '.$so_qty.', '.$service_qty.' FROM ('.$this->exchange_order_items.' as exritm)';
        $excitem_qry .= ' JOIN '.$this->exchange_order.' as excr ON `excr`.`id` = `exritm`.`exr_id`';
        $excitem_qry .= ' JOIN '.$this->suppliers.' as sup ON `sup`.`id` = `excr`.`sup_id`';
        $excitem_qry .= ' WHERE `exritm`.`exr_id` IN ('.implode(',', $excid).') AND `exritm`.`status` != 2 AND `exritm`.`status` != 10';
        $excitem_qry .= ' HAVING exr_quantity > CASE WHEN invoice_qty IS NOT NULL AND service_qty IS NOT NULL THEN (invoice_qty + service_qty) WHEN invoice_qty IS NULL AND service_qty IS NOT NULL THEN service_qty WHEN service_qty IS NULL AND invoice_qty IS NOT NULL THEN invoice_qty ELSE 0 END';
        
        return $this->db->query($excitem_qry)->result();
    }
    
    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->exchange_order, $data);
        return $this->db->affected_rows();
    }
    
    public function update_item($data, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->exchange_order_items, $data);
        return $this->db->affected_rows();
    }

    public function inactive_exc_items($excid) {
        $data = array('status' => 2);
        $this->db->where('exr_id', $excid);
        $this->db->update($this->exchange_order_items, $data);
        return $this->db->affected_rows();
    }
    
    public function delete_exc_items($excid) {
        $data = array('status' => 10);
        $this->db->where('exr_id', $excid);
        $this->db->where('status !=', 2);
        $this->db->update($this->exchange_order_items, $data);
        return $this->db->affected_rows();
    }
    
    public function get_exchange_order_report($from_date = '', $to_date = '',$supplier_id) {
        $this->db->select('exr.id, exr.exr_net_amount, brn.branch_name, exr.exr_code, DATE_FORMAT(exr.exr_date, "%d/%m/%Y") as exr_date_str, sup.name, CASE exr.payment_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as payment_status', FALSE);
        $this->db->join($this->suppliers . ' as sup', 'sup.id = exr.sup_id');
        $this->db->join($this->branches . ' as brn', 'brn.id = exr.branch_id');
        if(!empty($supplier_id)) {
        $this->db->where('exr.sup_id', $supplier_id);
        }
        if (!empty($from_date)) {
            $this->db->where('DATE(exr.exr_date) >=', date('Y-m-d', strtotime($from_date)));
        }
        if (!empty($to_date)) {
        $this->db->where('DATE(exr.exr_date) <=', date('Y-m-d', strtotime($to_date)));
        }
        $this->db->where('exr.exr_status !=', 10);
        $this->db->order_by('exr.id', 'desc');
        $this->db->where_in('exr.branch_id', $this->user_data['branch_ids'], FALSE);
        return $this->db->get($this->exchange_order . ' as exr')->result();
    }
    

}

?>