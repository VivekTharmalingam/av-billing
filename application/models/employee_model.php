<?php
class Employee_Model extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->employee = TBL_EMP;
		$this->emp_doc  = TBL_EMP_DOC;		
    }    
    public function list_active() {
        $this->db->select('id, comp_name,comp_code');
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('name', 'asc');
        return $this->db->get($this->customer)->result();
    }
   
    public function list_all() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->employee)->result();
    }    
    public function get_by_id($id) {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('id', $id);
        return $this->db->get($this->employee)->row();
    }
	public function get_doc_by_id($id) {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('employee_id', $id);
        return $this->db->get($this->emp_doc)->result();
    }
    public function get_last_row() {        
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'desc');
        $this->db->limit('1');
        return $this->db->get($this->companies)->row();
    }
    public function insert($data) {
        $this->db->insert($this->employee, $data);
        return $this->db->insert_id();
    }
	public function insert_docs($data) {
        $this->db->insert($this->emp_doc, $data);
        return $this->db->insert_id();
    }
    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update($this->employee, $data);
        return $this->db->affected_rows();
    }
	public function update_docs($id, $data){
        $this->db->where('employee_doc_id', $id);
        $this->db->update($this->emp_doc, $data);
        return $this->db->affected_rows();
    }
	public function update_docs_file($id, $data){
        $this->db->where('employee_doc_id', $id);
		$postdata = array('doc_file'=>$data);
        $this->db->update($this->emp_doc, $postdata);
        return $this->db->affected_rows();
    }
	function get_doc_name_by_id($doc_id){
		$this->db->where('employee_doc_id', $doc_id);
		return $this->db->get($this->emp_doc);
	}
    public function delete($id){
        $this->db->where('id', $id);
        $this->db->delete($this->employee);
        return $this->db->affected_rows();
    }
	public function row_delete($id){
        $this->db->where('employee_doc_id', $id);
        $this->db->delete($this->emp_doc);
        return $this->db->affected_rows();
    }
    public function check_employee_exists($emp,$id){
        $this->db->where('employee_name',$emp);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        if(!empty($id))
           $this->db->where('id !=',$id);
        $getData = $this->db->get($this->employee);
        if($getData->num_rows() > 0)
            return 1;
        else
            return 0;
    }    
}
?>