<?php

class Purchase_Receive_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->suppliers = TBL_SUP;
        $this->branches = TBL_BRN;
        $this->items = TBL_PDT;
        $this->category = TBL_CAT;
        $this->units = TBL_UNIT;
        $this->company = TBL_COMP;
        $this->purchases = TBL_PO;
        $this->purchase_items = TBL_POIT;
        $this->purchase_receive = TBL_PR;
		$this->purchase_payment = TBL_PPM;
        $this->purchase_receive_items = TBL_PRIT;
        $this->inventory_product = TBL_INV_PDT;
        $this->branch_products = TBL_BRN_PDT;
    }

    public function list_all() {
        $this->db->select('pr.id, brn.branch_name, DATE_FORMAT(pr.invoice_date, "%d/%m/%Y") as invoice_date, pr.pr_code, DATE_FORMAT(pr.pr_date, "%d/%m/%Y") as pr_date, pr.supplier_invoice_no, pr_net_amount, sup.name, CASE pr.payment_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as payment_status, pr.credit_notes', FALSE);
		$this->db->join($this->suppliers . ' as sup', 'sup.id = pr.sup_id');
        $this->db->join($this->branches . ' as brn', 'brn.id = pr.branch_id');
        $this->db->where('pr.pr_status !=', 10);
        $this->db->where('pr.branch_id', $this->user_data['branch_id']);
        $this->db->order_by('pr.id', 'desc');
        return $this->db->get($this->purchase_receive . ' as pr')->result();
    }
	
	public function list_report($from_date, $to_date, $branch_id, $gst_type, $currency) {
        $this->db->select('pr.id, brn.branch_name, DATE_FORMAT(pr.pr_date, "%d-%m-%Y") as pr_date, pr.supplier_invoice_no, pr_net_amount, sup.name as supplier_name', FALSE);
		$this->db->select('CASE pr.payment_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as payment_status', FALSE);
		$this->db->select('CASE pr.pr_gst_type WHEN 1 THEN "0.00" WHEN 2 THEN pr.pr_gst_amount ELSE "" END as pr_gst_amount', FALSE);
		$this->db->join($this->suppliers . ' as sup', 'sup.id = pr.sup_id');
        $this->db->join($this->branches . ' as brn', 'brn.id = pr.branch_id');
		
        $this->db->where('pr.pr_status !=', 10);		
		if ((!empty($from_date))) {
            $this->db->where('DATE(pr.pr_date) >=', date('Y-m-d', strtotime($from_date)));
        }
        if ((!empty($to_date))) {
            $this->db->where('DATE(pr.pr_date) <=', date('Y-m-d', strtotime($to_date)));
        }
		
		if ((!empty($branch_id))) {
            $this->db->where('pr.branch_id', $branch_id);
        }
		else {
			$this->db->where_in('pr.branch_id', $this->user_data['branch_ids']);
		}
		
		if ((!empty($gst_type))) {
            $this->db->where('pr.pr_gst_type', $gst_type);
        }
		
		if ((!empty($currency))) {
            $this->db->where('pr.currency', $currency);
        }
		
		$this->db->order_by('pr.id', 'desc');
        return $this->db->get($this->purchase_receive . ' as pr')->result();
    }
	
    public function check_received_status($po_id) {
        $this->db->where('po_id', $po_id);
        $this->db->where('pr_status !=', 10);
        $this->db->where('received_status', '1');
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        $getData = $this->db->get($this->purchase_receive);
        if ($getData->num_rows() > 0)
            return 1;
        else
            return 0;
    }

    public function get_purchase_list() {
        $user_data = get_user_data();
        $this->db->_protect_identifiers = FALSE;
        $sub = ',(SELECT SUM(pri.pr_quantity) FROM ' . $this->purchase_receive_items . ' as pri JOIN ' . $this->purchase_receive . ' as pr ON pr.id = pri.pr_id WHERE pr.po_id = po.id AND pri.status != 10 AND pr.pr_status != 10) as pri_quantity';
        $this->db->select('po.id, po.po_no, sup.name,SUM(poi.quantity) as po_quantity' . $sub, FALSE);
        $this->db->join($this->purchase_items . ' as poi', 'poi.po_id = po.id');
        $this->db->join($this->suppliers . ' as sup', 'sup.id = po.sup_id');
        $this->db->where('po.pr_status !=', 3, FALSE);
        $this->db->where('po.po_status', 1, FALSE);
        $this->db->where('po.id NOT IN (SELECT `po_id` FROM ' . $this->purchase_receive . ' WHERE received_status = 1 AND pr_status != 10)', NULL, FALSE);
        $this->db->where_in('po.brn_id', $user_data['branch_id']);
        $this->db->order_by('po.id', 'desc');
        $this->db->group_by('po.po_no');
        $this->db->having('po_quantity > pri_quantity OR pri_quantity IS NULL', FALSE);
        return $this->db->get($this->purchases . ' as po')->result();
    }

    public function get_branch_by_po($id) {
        $this->db->select('pr.*, brn.*,brn.id as brnid');
        $this->db->join($this->branches . ' as  brn', 'brn.id = pr.branch_id');
        $this->db->where('pr.id', $id);
        $this->db->where('brn.status !=', 10);
        return $this->db->get($this->purchase_receive . ' as pr')->row();
    }

    public function get_supplier_by_po($pr_id) {
        $this->db->select('sup.name, sup.contact_name, sup.phone_no, sup.email_id, sup.address');
        $this->db->join($this->suppliers . ' as sup', 'sup.id = pr.sup_id');
        $this->db->where('pr.id', $pr_id);
        return $this->db->get($this->purchase_receive . ' as pr')->row();
    }

    public function get_branch_list() {
        $this->db->where('status', 1);
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->branches)->result();
    }

    public function get_products_by_po($po_id) {
        $this->db->select('product.id,product.pdt_code,product.pdt_name,cat.cat_name');
        $this->db->join($this->items . ' as product', 'product.id = poit.product_id');
        $this->db->join($this->category . ' as cat', 'cat.id = product.cat_id');
        $this->db->where('poit.po_id', $po_id);
        $this->db->where('poit.status !=', 10);
        return $this->db->get($this->purchase_items . ' as poit')->result();
    }

    public function get_all_po_items($po_id) {
        $sub = ',(SELECT SUM(pri.pr_quantity) FROM ' . $this->purchase_receive_items . ' as pri JOIN ' . $this->purchase_receive . ' as pr ON pr.id = pri.pr_id WHERE pr.po_id = po.id AND pri.product_id = poi.product_id AND pri.status != 10 AND pr.pr_status != 10) as pr_quantity';
        $this->db->select('poi.product_id, poi.product_description, poi.sl_itm_code, poi.quantity, poi.price, poi.unit_cost, poi.total, p.pdt_code, p.bar_code, p.s_no, p.model_no', FALSE);
		$this->db->select($sub, FALSE);
        $this->db->join($this->items . ' as p', 'p.id = poi.product_id', 'left');
        //$this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');
        $this->db->join($this->purchases . ' as po', 'po.id = poi.po_id', 'left');
        $this->db->where('poi.po_id', $po_id);
        $this->db->where('poi.status !=', 10);
        $this->db->group_by('poi.product_id');
        return $this->db->get($this->purchase_items . ' as poi')->result();
    }

    public function get_item_details($mat_id, $po_id) {
        $this->db->select('poit.*,items.selling_price');
        //$this->db->join($this->units . ' as unit', 'unit.pr_id = pr.id', 'left');
        $this->db->join($this->items . ' as items', 'items.id = poit.product_id');
//        $this->db->join($this->units . ' as unit','unit.id = items.unit_id');
        $this->db->where('poit.product_id', $mat_id);
        $this->db->where('poit.po_id', $po_id);
        return $this->db->get($this->purchase_items . ' as poit')->row();
    }

    public function check_mat_already_exists($mat_id, $po_id, $pr_id) {
        $this->db->select('SUM(prit.pri_quantity) as act_quantity');
        $this->db->join($this->purchase_receive_items . ' as prit', 'prit.pr_id = pr.id', 'left');
        $this->db->where('prit.product_id', $mat_id);
        $this->db->where('prit.status', 1);
        $this->db->where('pr.po_id', $po_id);
        if (!empty($pr_id)) {
            $this->db->where('prit.pr_id !=', $pr_id);
        }
        $this->db->where('pr.pr_status !=', 10);
        return $this->db->get($this->purchase_receive . ' as pr')->row();
    }

    public function insert($data) {
        $this->db->insert($this->purchase_receive, $data);
        return $this->db->insert_id();
    }

    public function insert_item($data) {
        $this->db->insert($this->purchase_receive_items, $data);
        return $this->db->insert_id();
    }

    public function branch_materials($brn, $prt) {
        $this->db->select('bp.available_qty', FALSE);
        $this->db->where('bp.brn_id', $brn);
        $this->db->where('bp.product_id', $prt);
        $this->db->where('bp.status', 1);
        return $this->db->get($this->branch_products . ' as bp');
    }

    public function update_branch_material_qty($brn_id, $material_id, $quantity, $op = '+') {
        $update = array(
            'branch_id' => $this->user_data['branch_id'],
            'updated_by' => $this->user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s'),
            'ip_address' => $this->ip_address
        );
        $this->db->set('available_qty', 'available_qty ' . $op . $quantity, FALSE);
        $this->db->where('brn_id', $brn_id);
        $this->db->where('product_id', $material_id);
        if ($op == '-') {
            $this->db->where('available_qty >= ', $quantity);
        }
        $this->db->update($this->branch_products, $update);
        return $this->db->affected_rows();
    }

    public function insert_brn_material($data) {
        $this->db->insert($this->branch_products, $data);
        return $this->db->insert_id();
    }

    public function update_item($data, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->purchase_receive_items, $data);
        return $this->db->affected_rows();
    }

    public function update_item_by_pr($data, $id) {
        $this->db->where('pr_id', $id);
        $this->db->update($this->purchase_receive_items, $data);
        return $this->db->affected_rows();
    }

    public function row_delete($grn_id, $item_id) {
        $data = array('status' => 10);
        $this->db->where('id', $item_id);
        $this->db->update($this->purchase_receive_items, $data);
    }

    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->purchase_receive, $data);
        return $this->db->affected_rows();
    }

    public function get_po_total_quantity($po_id) {
        $this->db->select('SUM(quantity) as quantity');
        $this->db->where('po_id', $po_id);
        $this->db->where('status !=', 10);
        return $this->db->get($this->purchase_items)->row();
    }

    public function get_grn_total_quantity($po_id) {
        $this->db->select('SUM(pri.pr_quantity) as pr_quantity');
        $this->db->join($this->purchase_receive . ' as pr', 'pr.id = pri.pr_id', 'left');
        $this->db->where('pri.status !=', 10);
        $this->db->where('pr.po_id', $po_id);
        $this->db->where('pr.pr_status !=', 10);
        return $this->db->get($this->purchase_receive_items . ' as pri')->row();
    }

    public function update_product($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->items, $data);
        return $this->db->affected_rows();
    }

    public function get_grn_details_by_po_id($po_id) {
        $this->db->select('pr.*, po.po_no, brn.branch_name,CASE po.pr_status WHEN 1 THEN "New" WHEN 2 THEN "Partially Received" WHEN 3 THEN "Received" ELSE "" END as status_str', FALSE);
        $this->db->join($this->branches . ' as brn', 'brn.id = po.brn_id');
        $this->db->join($this->purchase_receive . ' as pr', 'pr.po_id = po.id', 'left');
        $this->db->where('po.id', $po_id);
        $this->db->where('pr.pr_status !=', 10);
        return $this->db->get($this->purchases . ' as po')->result();
    }

    public function get_item_by_id($id) {
        $this->db->select('pri.*,prt.pdt_code, prt.pdt_name');
        $this->db->join($this->items . ' as prt', 'prt.id = pri.product_id', 'left');
//        $this->db->join($this->units . ' as unit', 'unit.id = prt.unit_id', 'left');
        $this->db->where('pri.pr_id', $id);
        $this->db->where('pri.status !=', 10);
        return $this->db->get($this->purchase_receive_items . ' as pri')->result();
    }

    public function get_item_discount_by_id($id) {
        $this->db->select('sum(i.pri_total_amt) as po_amount', FALSE);
        $this->db->where('i.pr_id', $id);
        $this->db->where('i.status !=', 10);
        $this->db->where('i.discount_type', 1);
        $this->db->group_by('i.discount');
        $this->db->order_by('i.id', 'asc');
        return $this->db->get($this->purchase_receive_items . ' as i')->result();
    }

    public function get_item_vat_amt_by_id($id) {
        $this->db->select('i.discount_type, i.discount, i.pri_total_amt, sum(i.pri_total_amt) as po_amount, sum(i.discount_amount) as total_vat_amt ', FALSE);
        $this->db->where('i.pr_id', $id);
        $this->db->where('i.status !=', 10);
        $this->db->where('i.discount_type', 2);
        $this->db->group_by('i.pr_id');
        $this->db->order_by('i.id', 'asc');
        return $this->db->get($this->purchase_receive_items . ' as i')->result();
    }

    public function get_item_vat_by_id($id) {
        $this->db->select('i.discount_type, i.discount, sum(i.discount_amount) as total_vat_amt, sum(i.pri_total_amt) as po_amount', FALSE);
        $this->db->where('i.pr_id', $id);
        $this->db->where('i.status !=', 10);
        $this->db->where('i.discount_type', 1);
        $this->db->group_by('i.discount');
        $this->db->order_by('i.id', 'asc');
        return $this->db->get($this->purchase_receive_items . ' as i')->result();
    }

    public function get_by_id($id) {
        $this->db->select('pr.*, CASE pr.currency WHEN 1 THEN "SGD" WHEN 2 THEN "USD" ELSE "" END as currency_str, sup.name, sup.gst_type, sup.contact_name, sup.phone_no, sup.email_id, sup.address, brn.branch_name, CASE pr.pr_status WHEN 1 THEN "Active" WHEN 2 THEN "InActive" WHEN 3 THEN "Others" ELSE "" END as status_str, CASE pr.payment_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as payment_status, DATE_FORMAT(pr.invoice_date, "%d/%m/%Y") as invoice_date_str, DATE_FORMAT(pr.pr_date, "%d/%m/%Y") as pr_date_str, po.po_no',FALSE);
        $this->db->join($this->purchases . ' as po', 'po.id = pr.po_id','left');
        $this->db->join($this->branches . ' as brn', 'brn.id = pr.branch_id','left');
        $this->db->join($this->suppliers . ' as sup', 'sup.id = pr.sup_id','left');
        $this->db->where('pr.id', $id);
        return $this->db->get($this->purchase_receive . ' as pr')->row();
    }

    public function get_item_description_by_po($po_id, $mat_id) {
        $this->db->select('item_desc');
        $this->db->where('po_id', $po_id);
        $this->db->where('product_id', $mat_id);
        $this->db->where('status !=', 10);
        return $this->db->get($this->purchase_items)->row()->item_desc;
    }

    public function get_purchase_list_edit() {
        $this->db->select('po.*,sup.name', FALSE);
        $this->db->join($this->suppliers . ' as sup', 'sup.id = po.sup_id');
        $this->db->where('po.po_status', 1);
        $this->db->order_by('po.id', 'desc');
        return $this->db->get($this->purchases . ' as po')->result();
    }

    public function get_material_by_po($po_id) {
        $this->db->select('mat.id,mat.pdt_code,mat.pdt_name,cat.cat_name');
        $this->db->join($this->items . ' as mat', 'mat.id = poit.product_id');
        $this->db->join($this->category . ' as cat', 'cat.id = mat.cat_id');
        $this->db->where('poit.po_id', $po_id);
        $this->db->where('poit.status !=', 10);
        return $this->db->get($this->purchase_items . ' as poit')->result();
    }

    public function get_old_mat_quantity($mat_id, $id) {
        $this->db->select('SUM(pri.pri_quantity) as quantity');
        $this->db->where('pri.product_id', $mat_id);
        $this->db->where('pri.id', $id);
        $this->db->where('pri.status !=', 10);
        return $this->db->get($this->purchase_receive_items . ' as pri')->row()->quantity;
    }

    public function get_delete_mat_quantity($id) {
        $this->db->select('SUM(pri.pri_quantity) as quantity,product_id');
        $this->db->where('pri.id', $id);
        $this->db->where('pri.status !=', 10);
        return $this->db->get($this->purchase_receive_items . ' as pri')->row();
    }

    public function update_po_items($po_id, $mat_id, $data) {
        $this->db->where('po_id', $po_id);
        $this->db->where('product_id', $mat_id);
        $this->db->update($this->purchase_items, $data);
        return $this->db->affected_rows();
    }
    
    public function search_item_by_brand($po) {
//        if(!empty($grn)){
//           $sub = ',(SELECT SUM(pri.pr_quantity) FROM ' . $this->purchase_receive_items . ' as pri JOIN ' . $this->purchase_receive . ' as pr ON pr.id = pri.pr_id WHERE pr.po_id = pus.id AND pri.product_id = po.product_id AND pri.status != 10 AND pr.pr_status != 10 AND pri.pr_id !='.$grn.') as pr_quantity'; 
//        }else{
//        $sub = ',(SELECT SUM(pri.pr_quantity) FROM ' . $this->purchase_receive_items . ' as pri JOIN ' . $this->purchase_receive . ' as pr ON pr.id = pri.pr_id WHERE pr.po_id = pus.id AND pri.product_id = po.product_id AND pri.status != 10 AND pr.pr_status != 10) as pr_quantity';
//        }
        $sub = ',(SELECT SUM(pri.pr_quantity) FROM ' . $this->purchase_receive_items . ' as pri JOIN ' . $this->purchase_receive . ' as pr ON pr.id = pri.pr_id WHERE pr.po_id = pus.id AND pri.product_id = po.product_id AND pri.status != 10 AND pr.pr_status != 10) as pr_quantity';
        $this->db->select('po.quantity,po.price,po.unit_cost,p.id, p.pdt_code, p.pdt_name, p.pdt_description, p.buying_price,p.bar_code, p.selling_price, c.id as cat_id, c.cat_code, c.cat_name,' . $sub, FALSE);
        $this->db->join($this->items . ' as p', 'p.id = po.product_id', 'left');
        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');
        $this->db->join($this->purchases . ' as pus', 'pus.id = po.po_id', 'left');       
        $this->db->where('p.status', 1);
        $this->db->where('p.pdt_name !=', '');
        $this->db->where('po.po_id',$po);        
        $this->db->where('po.status',1);
        $this->db->order_by('c.cat_name');
        $this->db->group_by('po.product_id');
        return $this->db->get($this->purchase_items . ' as po')->result();
    }
    
    public function search_item($po) {           
//        if(!empty($grn)){
//           $sub = ',(SELECT SUM(pri.pr_quantity) FROM ' . $this->purchase_receive_items . ' as pri JOIN ' . $this->purchase_receive . ' as pr ON pr.id = pri.pr_id WHERE pr.po_id = pus.id AND pri.product_id = po.product_id AND pri.status != 10 AND pr.pr_status != 10 AND pri.pr_id !='.$grn.') as pr_quantity'; 
//        }else{
//        $sub = ',(SELECT SUM(pri.pr_quantity) FROM ' . $this->purchase_receive_items . ' as pri JOIN ' . $this->purchase_receive . ' as pr ON pr.id = pri.pr_id WHERE pr.po_id = pus.id AND pri.product_id = po.product_id AND pri.status != 10 AND pr.pr_status != 10) as pr_quantity';
//        }
        
        $sub = ',(SELECT SUM(pri.pr_quantity) FROM ' . $this->purchase_receive_items . ' as pri JOIN ' . $this->purchase_receive . ' as pr ON pr.id = pri.pr_id WHERE pr.po_id = pus.id AND pri.product_id = po.product_id AND pri.status != 10 AND pr.pr_status != 10) as pr_quantity';
        $this->db->select('po.quantity,po.price,po.unit_cost,p.id, p.pdt_code, p.pdt_name, p.pdt_description, p.buying_price,p.bar_code, p.selling_price, c.id as cat_id, c.cat_code, c.cat_name,' . $sub, FALSE);
        $this->db->join($this->items . ' as p', 'p.id = po.product_id', 'left');
        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');
        $this->db->join($this->purchases . ' as pus', 'pus.id = po.po_id', 'left');       
        $this->db->where('p.status', 1);
        $this->db->where('po.po_id',$po);
        $this->db->where('po.status',1);
        $this->db->order_by('p.pdt_name');
        $this->db->group_by('po.product_id');
        return $this->db->get($this->purchase_items . ' as po')->result();
        
    }
    
    public function get_current_grn_items($po,$grn) {        
        $this->db->select('pi.id As grni_id,pi.pr_quantity,pi.po_quantity AS quantity,pi.pr_price As price,pi.pr_unit_cost AS unit_cost,p.id, p.pdt_code, p.pdt_name, p.pdt_description, p.buying_price,p.bar_code, p.selling_price, c.id as cat_id, c.cat_code, c.cat_name', FALSE);
        $this->db->join($this->purchase_receive . ' as pr', 'pi.pr_id = pr.id', 'left');  
        $this->db->join($this->items . ' as p', 'p.id = pi.product_id', 'left');
        $this->db->join($this->category . ' as c', 'c.id = pi.category_id', 'left');     
        $this->db->where('p.status', 1);
        $this->db->where('pi.pr_id',$grn);
        $this->db->where('pr.po_id',$po);
        $this->db->where('pi.status',1);
        $this->db->order_by('p.pdt_name');
        $this->db->group_by('pi.product_id');
        return $this->db->get($this->purchase_receive_items . ' as pi')->result();        
    }
    
    public function get_current_grn_items_desc($po,$grn) {      
        $this->db->select('pi.id As grni_id,pi.pr_quantity,pi.po_quantity AS quantity,pi.pr_price As price,pi.pr_unit_cost AS unit_cost,p.id, p.pdt_code, p.pdt_name, p.pdt_description, p.buying_price,p.bar_code, p.selling_price, c.id as cat_id, c.cat_code, c.cat_name', FALSE);
        $this->db->join($this->purchase_receive . ' as pr', 'pi.pr_id = pr.id', 'left');  
        $this->db->join($this->items . ' as p', 'p.id = pi.product_id', 'left');
        $this->db->join($this->category . ' as c', 'c.id = pi.category_id', 'left');     
        $this->db->where('p.status', 1);
        $this->db->where('pi.pr_id',$grn);
        $this->db->where('pr.po_id',$po);
        $this->db->where('pi.status',1);
        $this->db->order_by('p.pdt_name');
        $this->db->group_by('pi.product_id');
        return $this->db->get($this->purchase_receive_items . ' as pi')->result();        
    }
    
    public function edit_purchase_receive_items($po,$grn) {          
       // $sub = ',(SELECT SUM(pri.pr_quantity) FROM ' . $this->purchase_receive_items . ' as pri JOIN ' . $this->purchase_receive . ' as pr ON pr.id = pri.pr_id WHERE pr.po_id = pus.id AND pri.product_id = po.product_id AND pri.status != 10 AND pr.pr_status != 10 AND pri.pr_id !='.$grn.') as pr_quantity';          
        $sub = ',(SELECT SUM(pri.pr_quantity) FROM ' . $this->purchase_receive_items . ' as pri JOIN ' . $this->purchase_receive . ' as pr ON pr.id = pri.pr_id WHERE pr.po_id = pus.id AND pri.product_id = po.product_id AND pri.status != 10 AND pr.pr_status != 10  AND pri.pr_id ='.$grn.') as pr_quantity';
        
        $this->db->select('po.quantity,po.price,po.unit_cost,p.id, p.pdt_code, p.pdt_name, p.pdt_description, p.buying_price,p.bar_code, p.selling_price, c.id as cat_id, c.cat_code, c.cat_name,' . $sub, FALSE);
        $this->db->join($this->items . ' as p', 'p.id = po.product_id', 'left');
        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');
        $this->db->join($this->purchases . ' as pus', 'pus.id = po.po_id', 'left');       
        $this->db->where('p.status', 1);
        $this->db->where('po.po_id',$po);
        $this->db->where('po.status',1);
        $this->db->order_by('p.pdt_name');
        $this->db->group_by('po.product_id');
        return $this->db->get($this->purchase_items . ' as po')->result();        
    }
    
    public function edit_purchase_receive_desc($po,$grn) {          
        $sub = ',(SELECT SUM(pri.pr_quantity) FROM ' . $this->purchase_receive_items . ' as pri JOIN ' . $this->purchase_receive . ' as pr ON pr.id = pri.pr_id WHERE pr.po_id = pus.id AND pri.product_id = po.product_id AND pri.status != 10 AND pr.pr_status != 10 AND pri.pr_id ='.$grn.') as pr_quantity';          
        $this->db->select('po.quantity,po.price,po.unit_cost,p.id, p.pdt_code, p.pdt_name, p.pdt_description, p.buying_price,p.bar_code, p.selling_price, c.id as cat_id, c.cat_code, c.cat_name,' . $sub, FALSE);
        $this->db->join($this->items . ' as p', 'p.id = po.product_id', 'left');
        $this->db->join($this->category . ' as c', 'c.id = p.cat_id', 'left');
        $this->db->join($this->purchases . ' as pus', 'pus.id = po.po_id', 'left');       
        $this->db->where('p.status', 1);
        $this->db->where('po.po_id',$po);
        $this->db->where('po.status',1);
        $this->db->order_by('p.pdt_name');
        $this->db->group_by('po.product_id');
        return $this->db->get($this->purchase_items . ' as po')->result();        
    }
    
    public function get_pr_item($id) {
        $this->db->where('id', $id);
        $this->db->where('status !=', 10);
        return $this->db->get($this->purchase_receive_items)->row();
    }
    
    public function delete_pr_item($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->purchase_receive_items);
        return $this->db->affected_rows();
    }
    
    public function update_prit($data,$id) {
        $this->db->where('id', $id);
        $this->db->update($this->purchase_receive_items, $data);
        return $this->db->affected_rows();
    }
    
    public function get_removed_pr_items($pr_id, $product_ids = array()) {
        $this->db->where('pr_id', $pr_id);
        if (!empty($product_ids))
            $this->db->where_not_in('product_id', $product_ids);

        return $this->db->get($this->purchase_receive_items)->result();
    }
	
    public function unreceived_purchase_orders() {
        //$this->db->_protect_identifiers = FALSE;
        $sub = ',(SELECT SUM(pri.pr_quantity) FROM ' . $this->purchase_receive_items . ' as pri JOIN ' . $this->purchase_receive . ' as pr ON pr.id = pri.pr_id AND pr.pr_status != 10 WHERE pr.po_id = po.id AND pri.status != 10) as pri_quantity';
        $this->db->select('po.id, po.po_no, sup.name as supplier_name, DATE_FORMAT(po_date, "%d/%m/%Y") as po_date, SUM(poi.quantity) as po_quantity' . $sub, FALSE);
        $this->db->join($this->purchase_items . ' as poi', 'poi.po_id = po.id');
        $this->db->join($this->suppliers . ' as sup', 'sup.id = po.sup_id');
        //$this->db->where('po.pr_status !=', 3, FALSE);
        $this->db->where('po.po_status', 1);
        //$this->db->where('po.id NOT IN (SELECT `po_id` FROM ' . $this->purchase_receive . ' WHERE received_status = 1 AND pr_status != 10)', NULL, FALSE);
        $this->db->where('po.brn_id', $this->user_data['branch_id']);
        $this->db->order_by('po.id', 'desc');
        $this->db->group_by('po.id');
        $this->db->having('po_quantity > pri_quantity OR pri_quantity IS NULL', FALSE);
        return $this->db->get($this->purchases . ' as po')->result();
    }
}
?>