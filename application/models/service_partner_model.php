<?php
class Service_Partner_Model extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->service_partner = TBL_SERPTN;		
    }    
    public function list_active() {
        $this->db->select('id, name');
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('name', 'asc');
        return $this->db->get($this->service_partner)->result();
    }
    public function list_all() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('name', 'asc');
        return $this->db->get($this->service_partner)->result();
    }    
    public function search_service_partner() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->service_partner)->result();
    }    
    public function get_by_id($id) {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('id', $id);
        return $this->db->get($this->service_partner)->row();
    }
    public function insert($data) {
        $this->db->insert($this->service_partner, $data);
        return $this->db->insert_id();
    }
    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update($this->service_partner, $data);
        return $this->db->affected_rows();
    }
    public function delete($id){
        $this->db->where('id', $id);
        $this->db->delete($this->service_partner);
        return $this->db->affected_rows();
    }
    public function check_service_partner_exists($service_partner,$id){
        $this->db->where('name',$service_partner);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        if(!empty($id))
           $this->db->where('id !=',$id);
        $getData = $this->db->get($this->service_partner);
        if($getData->num_rows() > 0)
            return 1;
        else
            return 0;
    }
	public function list_search_all($from_date = '', $to_date = '') {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        if((!empty($from_date)))  {
            $this->db->where('created_on >=',date('Y-m-d H:i:s', strtotime($from_date)));
        }
        if((!empty($to_date)))  {
            $this->db->where('created_on <=',date('Y-m-d 23:59:59', strtotime($to_date)));
        }
		$this->db->where('status', 1);
        $this->db->order_by('id', 'desc');
        $resource = $this->db->get($this->service_partner);
        return $resource->result();
    }
    
}
?>