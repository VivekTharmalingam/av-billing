<?php
/* 
 * Created By : Vivek T
 * Purpose : Promotion Module - Discount Sales Model
 * Created On : 2k17-02-20 02:35:00
 */
class Discount_Sales_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->product = TBL_PDT;
        $this->category = TBL_CAT;
        $this->discount_sale = TBL_DISC_SALE;
    }
    
    public function list_all() {
        $this->db->select('ds.*,p.pdt_name, CASE ds.status WHEN 1 THEN "Active" WHEN 2 THEN "In Active" ELSE "" END as status_str', FALSE);
        $this->db->join($this->product . ' as p', 'p.id = ds.offer_item_id');
        $this->db->where('ds.status !=', 10);
        $this->db->order_by('ds.id', 'desc');
        return $this->db->get($this->discount_sale . ' as ds')->result();
    }
    
    public function insert($data) {
        $this->db->insert($this->discount_sale, $data);
        return $this->db->insert_id();
    }
    
    public function get_by_id($id) {
        $this->db->select('ds.*,p.pdt_name, CASE ds.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->join($this->product . ' as p', 'p.id = ds.offer_item_id');
        $this->db->where('ds.id', $id);
        return $this->db->get($this->discount_sale . ' as ds')->row();
    }
    
    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update($this->discount_sale, $data);
        return $this->db->affected_rows();
    }
    
    
}