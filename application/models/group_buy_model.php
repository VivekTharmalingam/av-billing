<?php
/* 
 * Created By : Vivek T
 * Purpose : Promotion Module - Group Buy Model
 * Created On : 2k17-02-20 10:00:00
 */
class Group_buy_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->product = TBL_PDT;
        $this->category = TBL_CAT;
        $this->gb = TBL_GROUP_BUY;
        $this->gb_items = TBL_GROUP_BUY_ITEMS;
    }
    
    public function list_all() {
        $this->db->select('gb.*, CASE gb.status WHEN 1 THEN "Active" WHEN 2 THEN "In Active" ELSE "" END as status_str', FALSE);
        $this->db->where('gb.status !=', 10);
        $this->db->order_by('gb.id', 'desc');
        return $this->db->get($this->gb . ' as gb')->result();
    }
    
    public function insert($data) {
        $this->db->insert($this->gb, $data);
        return $this->db->insert_id();
    }
    
    public function insert_item($data) {
        $this->db->insert($this->gb_items, $data);
        return $this->db->insert_id();
    }
    
    public function get_by_id($id) {
        $this->db->select('gb.*, CASE gb.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('gb.id', $id);
        return $this->db->get($this->gb . ' as gb')->row();
    }
    
    public function get_by_items($id) {
        $this->db->select('gb_items.*,p.pdt_name, CASE gb_items.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->join($this->product . ' as p', 'p.id = gb_items.offer_item_id');
        $this->db->where('gb_items.status != 10');
        $this->db->where('gb_items.group_id', $id);
        return $this->db->get($this->gb_items . ' as gb_items')->result();
    }
    
    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update($this->gb, $data);
        return $this->db->affected_rows();
    }
    
    public function update_item($id, $data){
        $this->db->where('id', $id);
        $this->db->update($this->gb_items, $data);
        return $this->db->affected_rows();
    }
    
    public function delete_item($id, $data){
        $this->db->where('group_id', $id);
        $this->db->update($this->gb_items, $data);
        return $this->db->affected_rows();
    }
    
    
    
}