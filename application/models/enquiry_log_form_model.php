<?php
class Enquiry_log_form_Model extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->enq_log_frm = TBL_ENQ_LOG_FRM;	
        $this->log_form = TBL_LOG_FRM;
        $this->customer = TBL_CUS;	
        $this->user = TBL_USER;
        $this->branches = TBL_BRN;
    }    
    public function list_customers() {
        $this->db->select('id, name');
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'asc');
        return $this->db->get($this->customer)->result();
    }
    public function list_users() {
        $this->db->select('id, name');
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'asc');
        return $this->db->get($this->user)->result();
    }
    
    public function list_branche() {
        $this->db->select('id, branch_name');
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'asc');
        return $this->db->get($this->branches)->result();
    }
    public function list_all() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'asc');
        return $this->db->get($this->enq_log_frm)->result();
    }    
  
    public function get_by_id($id) {
        $this->db->select('elf.*,c.name, CASE elf.enquiry_status WHEN 1 THEN "Success" WHEN 2 THEN "Process" WHEN 3 THEN "Cancel" ELSE "" END as enq_status_str', FALSE);
        $this->db->join($this->customer .' as c', 'c.id = elf.company_id', 'left');
        $this->db->where('elf.id', $id);
        return $this->db->get($this->enq_log_frm.' as elf')->row();
    }
    public function insert($data) {
        $this->db->insert($this->enq_log_frm, $data);
        return $this->db->insert_id();
    }
    public function update($id, $data){
        $this->db->where('id', $id);
        $this->db->update($this->enq_log_frm, $data);
        return $this->db->affected_rows();
    }
    public function delete($id){
        $this->db->where('id', $id);
        $this->db->delete($this->enq_log_frm);
        return $this->db->affected_rows();
    }
    public function insert_enquiry_log($data) {
        $this->db->insert($this->log_form, $data);
        return $this->db->insert_id();
    }
    
    public function past_log_details($id) {
        $this->db->select('lf.log_description as log_description,lf.enquiry_status as enq_sts, CASE lf.enquiry_status WHEN 1 THEN "Success" WHEN 2 THEN "Process" WHEN 3 THEN "Cancel" ELSE "" END as enq_status_str,lf.created_on as created_date', FALSE);
        $this->db->where('lf.enquiry_log_id', $id);
        return $this->db->get($this->log_form . ' as lf')->result();
    }
    
    public function update_enq_sts($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->enq_log_frm, $data);
        return $this->db->affected_rows();
    }
    
    public function list_enq_log_summary($from_date = '', $to_date = '', $branch, $assign_to = '', $enq_sts = '', $type = '') {
        $this->db->select('elf.*,cus.name as cus_name,ur.name as picked_user, CASE elf.type WHEN 1 THEN "Sales" WHEN 2 THEN "Purchase" WHEN 3 THEN "Accounts" ELSE "Promotions" END as type, u.name as assigned_to, CASE elf.enquiry_status WHEN 1 THEN "Success" WHEN 2 THEN "Process" ELSE "Cancel" END as enq_sts', FALSE);
        if ((!empty($from_date))) {
            $this->db->where('DATE(elf.created_on) >=', date('Y-m-d', strtotime($from_date)));
        }
        if ((!empty($to_date))) {
            $this->db->where('DATE(elf.created_on) <=', date('Y-m-d', strtotime($to_date)));
        }

        if ((!empty($enq_sts))) {
            $this->db->where('elf.enquiry_status', $enq_sts);
        }
        if ((!empty($type))) {
            $this->db->where('elf.type', $type);
        }
        if ((!empty($branch))) {
            $this->db->where('elf.branch_id', $branch);
        }
        if ((!empty($assign_to))) {
            $this->db->where('FIND_IN_SET(' . $assign_to . ', elf.assigned_to)');
        }
        $this->db->where_in('elf.branch_id', $this->user_data['branch_ids'], FALSE);
        $this->db->where('elf.status !=', 10);
        $this->db->order_by('elf.id', 'desc');
        $this->db->join($this->branches . ' as brn', 'brn.id = elf.branch_id', 'left');
        $this->db->join($this->user . ' as u', 'u.id = elf.assigned_to', 'left');
        $this->db->join($this->user . ' as ur', 'ur.id = elf.picked_by', 'left');
        $this->db->join($this->customer . ' as cus', 'cus.id = elf.company_id', 'left');
        return $this->db->get($this->enq_log_frm . ' AS elf')->result();
        //echo $this->db->last_query(); exit;
    }

    
}
?>