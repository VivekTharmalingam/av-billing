<?php

class Services_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->exter_services = TBL_EXTSER;
		
        $this->services_exter_item = TBL_SEREXTITM;
		
        $this->customer = TBL_CUS;
        $this->suppliers = TBL_SUP;
        $this->users = TBL_USER;
        $this->branches = TBL_BRN;
        $this->company = TBL_COMP;
        $this->serv_category = TBL_SCAT;
        $this->category = TBL_CAT;
        $this->serv_items = TBL_SITM;
        $this->services = TBL_SJST;
        $this->services_items = TBL_SJSTITM;
		
        $this->services_inventory_items = TBL_SEXINITM;
		
        $this->services_status_log = TBL_SRSTLG;
        $this->service_status = TBL_SERSTS;
        $this->product = TBL_PDT;
        $this->exchange_order = TBL_EXR;
        $this->exchange_order_items = TBL_EXRIT;
        $this->sales_order = TBL_SO;
        $this->sales_order_item = TBL_SO_ITM;
        $this->user_data = get_user_data();
    }

    public function list_all() {
        $this->db->select('serv.*, DATE_FORMAT(serv.received_date, "%d/%m/%Y") as received_date_str, CASE serv.service_status WHEN 1 THEN "Processing" WHEN 2 THEN "Completed" WHEN 3 THEN "Delivered" ELSE "" END as service_status', FALSE);
        $this->db->join($this->branches . ' as brn', 'brn.id = serv.branch_id');
        $this->db->where('serv.status !=', 10);
        $this->db->order_by('serv.id', 'desc');
        $this->db->where_in('serv.branch_id', $this->user_data['branch_id']);
        return $this->db->get($this->services . ' as serv')->result();
    }
    
    public function service_status_list() {
        $this->db->select('serv.*', FALSE);
        $this->db->where('serv.status !=', 10);
        $this->db->order_by('serv.id', 'desc');
        $this->db->where_in('serv.branch_id', $this->user_data['branch_id']);
        return $this->db->get($this->service_status . ' as serv')->result();
    }
    
    public function service_status_log_details($ser_id) {
        $this->db->select('slog.id,slog.log_description,serst.status_name,slog.created_on as created_date', FALSE);
        $this->db->select('DATE_FORMAT(slog.created_on, "%d-%m-%Y %r") as created_date_str', FALSE);
        $this->db->join($this->service_status . ' as serst', 'serst.id = slog.service_status_id', 'left');
        $this->db->where('slog.service_id', $ser_id);
        $this->db->where('slog.status', 1);
        return $this->db->get($this->services_status_log . ' as slog')->result();
    }
    
    public function insert_status_log($data) {
        $this->db->insert($this->services_status_log, $data);
        return $this->db->insert_id();
    }
    
    public function service_active_list() {
        $sales_qty = '(SELECT COUNT(*) FROM ' . $this->sales_order . ' as sl_ord ';
        $sales_qty .= ' WHERE sl_ord.status != 10 AND sl_ord.service_id = serv.id) as sales_count';
        $this->db->select('serv.*, DATE_FORMAT(serv.received_date, "%d/%m/%Y") as received_date_str, CASE serv.service_status WHEN 1 THEN "Processing" WHEN 2 THEN "Completed" WHEN 3 THEN "Delivered" ELSE "" END as service_status, '.$sales_qty, FALSE);
        $this->db->join($this->branches . ' as brn', 'brn.id = serv.branch_id');
//        $this->db->where('serv.exc_inventory_itemadded = 1');
        $this->db->where('serv.status !=', 10);
        $this->db->order_by('serv.id', 'desc');
        $this->db->where_in('serv.branch_id', $this->user_data['branch_id']);
        $this->db->having('sales_count = 0');
        return $this->db->get($this->services . ' as serv')->result();
    }
    
    public function all_service_active_list() {
        $this->db->select('serv.*, DATE_FORMAT(serv.received_date, "%d/%m/%Y") as received_date_str, CASE serv.service_status WHEN 1 THEN "Processing" WHEN 2 THEN "Completed" WHEN 3 THEN "Delivered" ELSE "" END as service_status', FALSE);
        $this->db->join($this->branches . ' as brn', 'brn.id = serv.branch_id');
        $this->db->where('serv.status !=', 10);
        $this->db->order_by('serv.id', 'desc');
        $this->db->where_in('serv.branch_id', $this->user_data['branch_id']);
        return $this->db->get($this->services . ' as serv')->result();
    }
    
    public function get_external_service($serv_id) {
        $exter_qty = 'SELECT ext_itm.external_service_id FROM ' . $this->services_exter_item . ' as ext_itm ';
        $exter_qty .= ' WHERE ext_itm.service_id != '.$serv_id.' AND ext_itm.status != 10 AND ext_itm.status != 2';
        $service_qty  = 'SELECT extserv.* FROM ' . $this->exter_services . ' as extserv';
        $service_qty .= ' WHERE extserv.id NOT IN ('.$exter_qty.') AND extserv.status != 10';
        return $this->db->query($service_qty)->result();
    }
    
    public function get_services_exter_item($serv_id) {
        $this->db->select('extserv.*', FALSE);
        $this->db->where('extserv.service_id',$serv_id);        
        $this->db->where('extserv.status != 2');
        $this->db->where('extserv.status != 10');
        return $this->db->get($this->services_exter_item . ' AS extserv')->result_array();
    }
    
    public function get_services_exter_item_view($serv_id) {
        $this->db->select('extserv.*, ex_ser.exter_service_no,ex_ser.service_partner_name', FALSE);
        $this->db->join($this->exter_services . ' AS ex_ser','ex_ser.id=extserv.external_service_id');
        $this->db->where('extserv.service_id',$serv_id);        
        $this->db->where('extserv.status != 2');
        $this->db->where('extserv.status != 10');
        return $this->db->get($this->services_exter_item . ' AS extserv')->result();
    }
    
    public function get_exter_service_data($id) {
        $this->db->select('extserv.*', FALSE);
        $this->db->where('extserv.id',$id);             
        return $this->db->get($this->exter_services . ' AS extserv')->result_array();
    }
    
    public function customer_list() {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('status != 10');
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->customer)->result_array();
    }
    
    public function getcustomer_data($cust_id) {
        $this->db->select('*, CASE status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('id', $cust_id);
        return $this->db->get($this->customer)->row();
    }
    
    public function user_list() {
        $this->db->select('user.*, CASE user.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE);
        $this->db->where('user.status != 10');            
        $this->db->order_by('user.id', 'desc');
        return $this->db->get($this->users . ' AS user')->result_array();
    }
    
    public function service_category() {
        $this->db->select('scat.*', FALSE);
        $this->db->where('scat.status != 10');            
        return $this->db->get($this->serv_category . ' AS scat')->result_array();
    }
    
    public function service_items($cat) {
        $this->db->select('sitm.*', FALSE);
        $this->db->where('sitm.cat_id', $cat);
        $this->db->where('sitm.status != 10');          
        $this->db->order_by('sitm.sequence_no', 'asc');
        return $this->db->get($this->serv_items . ' AS sitm')->result_array();
    }
    
    public function insert($data) {
        $this->db->insert($this->services, $data);
        return $this->db->insert_id();
    }

    public function insert_item($data) {
        $this->db->insert($this->services_items, $data);
        return $this->db->insert_id();
    }
    
    public function insert_exc_inventory_item($data) {
        $this->db->insert($this->services_inventory_items, $data);
        return $this->db->insert_id();
    }
    
    public function insert_external_item($data) {
        $this->db->insert($this->services_exter_item, $data);
        return $this->db->insert_id();
    }
    
    public function getsheet_data($sheet_number) {
        $this->db->select('serv.*, usr.name as engineer_name, CASE serv.service_status WHEN 1 THEN "Processing" WHEN 2 THEN "Completed" WHEN 3 THEN "Delivered" ELSE "" END as service_status_str, DATE_FORMAT(serv.estimated_delivery_date, "%d/%m/%Y") as estimated_delivery_date_str, DATE_FORMAT(serv.received_date, "%d/%m/%Y") as received_date_str',FALSE);
        $this->db->join($this->users . ' as usr', 'usr.id = serv.engineer_id', 'left');
        $this->db->where('serv.jobsheet_number', $sheet_number);
        $this->db->where('serv.status != 10');     
        return $this->db->get($this->services . ' as serv')->row();
    }
    
    public function get_by_id($id) {
        $this->db->select('serv.*, c.cat_name,usr.name as engineer_name, CASE serv.service_status WHEN 1 THEN "Processing" WHEN 2 THEN "Completed" WHEN 3 THEN "Delivered" ELSE "" END as service_status_str, DATE_FORMAT(serv.estimated_delivery_date, "%d/%m/%Y") as estimated_delivery_date_str, DATE_FORMAT(serv.received_date, "%d/%m/%Y") as received_date_str, DATE_FORMAT(serv.delivered_date, "%d/%m/%Y") as delivered_date_str',FALSE);
        $this->db->join($this->users . ' as usr', 'usr.id = serv.engineer_id', 'left');
        $this->db->join($this->category . ' as c', 'c.id = serv.cat_id', 'left');
        $this->db->where('serv.id', $id);
        return $this->db->get($this->services . ' as serv')->row();
    }
    
    public function service_items_byid($cat, $ser_id) {
        $this->db->select('itm.*, sitm.item_id, sitm.others_txt', FALSE);
        $this->db->join($this->services_items . ' as sitm', 'sitm.item_id = itm.id and sitm.service_id="'.$ser_id.'" and sitm.status != 2 and sitm.status != 10', 'left');
        $this->db->where('itm.cat_id', $cat);
        $this->db->where('itm.status != 10'); 
        $this->db->order_by('itm.sequence_no', 'asc');
        return $this->db->get($this->serv_items . ' AS itm')->result_array();
    }
    
    public function service_other_cat($cat, $ser_id) {
        $this->db->select('itm.*', FALSE);
        $this->db->where('itm.cat_id', $cat);
        $this->db->where('itm.service_id', $ser_id);
        $this->db->where('itm.status != 2'); 
        $this->db->where('itm.status != 10'); 
        $this->db->order_by('itm.id', 'asc');
        return $this->db->get($this->services_items . ' AS itm')->result_array();
    }
    
    public function service_item_check($ser_id,$cat,$item) {
        $this->db->select('itm.*', FALSE);
        $this->db->where('itm.cat_id', $cat);
        if($item != '') {
            $this->db->where('itm.item_id', $item);
        }
        $this->db->where('itm.service_id', $ser_id);
        $this->db->where('itm.status != 10'); 
        $this->db->order_by('itm.id', 'asc');
        return $this->db->get($this->services_items . ' AS itm')->result_array();
    }
    
    public function get_item_by_id($service_id) {
        $this->db->select('servitm.*, ct.cat_name, ct.is_other as cat_other, itm.item_name, itm.is_other as item_other');
        $this->db->select('IF(ct.is_other = 0, GROUP_CONCAT(IF(itm.is_other = 1, CONCAT(itm.item_name, "-", servitm.others_txt), itm.item_name) SEPARATOR ","), servitm.others_txt) as item_name_str', FALSE);
        $this->db->join($this->serv_category . ' as ct', 'ct.id = servitm.cat_id', 'left');
        $this->db->join($this->serv_items . ' as itm', 'itm.id = servitm.item_id', 'left');
        $this->db->where('servitm.service_id', $service_id);
        $this->db->where('servitm.status !=', 2);
        $this->db->where('servitm.status !=', 10);
        $this->db->group_by('servitm.cat_id');
        return $this->db->get($this->services_items . ' as servitm')->result();
    }
    
    public function get_service_item_byid($id) {
        $this->db->select('servitm.*');
        $this->db->where('servitm.id', $id);
        return $this->db->get($this->services_inventory_items . ' as servitm')->row();
    }
    
    public function get_item_all($id) {
        $this->db->select('servitm.*');
        $this->db->where('servitm.service_id', $service_id);
        $this->db->where('servitm.status !=', 10);
        return $this->db->get($this->services_items . ' as servitm')->result();
    }
    
    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->services, $data);
        return $this->db->affected_rows();
    }
    
    public function update_item($data, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->services_items, $data);
        return $this->db->affected_rows();
    }

    public function inactive_exc_items($servid) {
        $data = array('status' => 2);
        $this->db->where('service_id', $servid);
        $this->db->update($this->services_items, $data);
        return $this->db->affected_rows();
    }
    
    public function delete_exc_items($servid) {
        $data = array('status' => 10);
        $this->db->where('service_id', $servid);
        $this->db->where('status !=', 2);
        $this->db->update($this->services_items, $data);
        return $this->db->affected_rows();
    }
    
    public function service_costitems_byid($ser_id) {
        $this->db->select('itm.*', FALSE);
        $this->db->where('itm.service_id', $ser_id);
        $this->db->where('itm.form_type = 3'); 
        $this->db->where('itm.status != 2'); 
        $this->db->where('itm.status != 10'); 
        $this->db->order_by('itm.id', 'asc');
        return $this->db->get($this->services_inventory_items . ' AS itm')->result();
    }
    
    public function service_inventoryitems_byid($ser_id) {
        $this->db->select('itm.*, p.pdt_code, p.pdt_name, p.pdt_description', FALSE);
        $this->db->join($this->product . ' as p', 'p.id = itm.item_id and p.status = 1', 'left');
        $this->db->where('itm.service_id', $ser_id);
        $this->db->where('itm.form_type = 2'); 
        $this->db->where('itm.status != 2'); 
        $this->db->where('itm.status != 10'); 
        $this->db->order_by('itm.id', 'asc');
        return $this->db->get($this->services_inventory_items . ' AS itm')->result();
    }
    
    public function service_excitems_byid($ser_id,$exc_id=NULL) {
        $this->db->select('itm.*, ext.product_text, ext.product_description as exc_product_description, excr.exr_code, sup.name', FALSE);
        $this->db->join($this->exchange_order_items . ' as ext', 'ext.id = itm.item_id', 'left');
        $this->db->join($this->exchange_order . ' as excr', 'excr.id = itm.exchange_order_id', 'left');
        $this->db->join($this->suppliers . ' as sup', 'sup.id = excr.sup_id', 'left');
        $this->db->where('itm.service_id', $ser_id);
        if($exc_id != "") {
        $this->db->where('itm.exchange_order_id', $exc_id);
        }
        $this->db->where('itm.form_type = 1'); 
        $this->db->where('itm.status != 2'); 
        $this->db->where('itm.status != 10'); 
        $this->db->order_by('itm.id', 'asc');
        if($exc_id == "") {
        $this->db->group_by('itm.exchange_order_id');
        }
        return $this->db->get($this->services_inventory_items . ' AS itm')->result();
    }
    
    public function service_excitems_all($ser_id) {
        $this->db->select('itm.*, ext.product_text, ext.product_description as exc_product_description, excr.exr_code, sup.name', FALSE);
        $this->db->join($this->exchange_order_items . ' as ext', 'ext.id = itm.item_id', 'left');
        $this->db->join($this->exchange_order . ' as excr', 'excr.id = itm.exchange_order_id', 'left');
        $this->db->join($this->suppliers . ' as sup', 'sup.id = excr.sup_id', 'left');
        $this->db->where('itm.service_id', $ser_id);
        $this->db->where('itm.form_type = 1'); 
        $this->db->where('itm.status != 2'); 
        $this->db->where('itm.status != 10'); 
        $this->db->order_by('itm.id', 'asc');
        return $this->db->get($this->services_inventory_items . ' AS itm')->result();
    }
    
    public function inactive_ser_excinventory_items($servid) {
        $data = array('status' => 2);
        $this->db->where('service_id', $servid);
        $this->db->update($this->services_inventory_items, $data);
        return $this->db->affected_rows();
    }
    
    public function inactive_ser_external_items($servid) {
        $data = array('status' => 2);
        $this->db->where('service_id', $servid);
        $this->db->update($this->services_exter_item, $data);
        return $this->db->affected_rows();
    }
    
    public function update_excinventory_item($data, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->services_inventory_items, $data);
        return $this->db->affected_rows();
    }
    
    public function update_external_item($data, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->services_exter_item, $data);
        return $this->db->affected_rows();
    }
    
    public function get_services_report($from_date = '', $to_date = '',$cus = '', $engg = '', $branch = '', $type = '', $service_from = '') {
        $home_date = '';
        if($service_from == 'home') {
            $datetime = new DateTime('tomorrow');
            $home_date = $datetime->format('Y-m-d H:i:s');
        }
        
        $this->db->select('serv.id AS srId, so.id as so_id, so.total_amt, serv.customer_name AS cust_name, serv.jobsheet_number, DATE_FORMAT(serv.received_date, "%d/%m/%Y") as received_date_str, u.name AS eng_name, CASE serv.service_status WHEN 1 THEN "Processing" WHEN 2 THEN "Completed" WHEN 3 THEN "Delivered" ELSE "" END as service_status,serv.estimated_price_quoted as estimated_price', FALSE);
		
		$this->db->select('((SELECT IFNULL(SUM(sxi.total), 0) FROM ' . $this->services_inventory_items . ' as sxi WHERE sxi.service_id = serv.id AND sxi.form_type = 3 AND sxi.status = 1) + (SELECT IFNULL(SUM(exts.external_sub_amount), 0) FROM ' . $this->services_exter_item . ' as exts WHERE exts.service_id = serv.id AND exts.status = 1)) as service_cost', FALSE);
		
		$this->db->select('((SELECT IFNULL(SUM(sxi.total), 0) FROM ' . $this->services_inventory_items . ' as sxi WHERE sxi.service_id = serv.id AND sxi.form_type = 3 AND sxi.status = 1) + (SELECT SUM(exts.our_cost) FROM ' . $this->services_exter_item . ' as exts WHERE exts.service_id = serv.id AND exts.status = 1)) as our_cost', FALSE);
		
		$this->db->select('(SELECT CASE so.gst WHEN 1 THEN SUM(soi.item_profit) - so.discount_amount ELSE SUM(soi.item_profit) - so.discount_amount END FROM ' . $this->sales_order_item . ' as soi WHERE soi.so_id = so.id AND soi.service_form_type IN(1, 2)) as item_profit', FALSE);
		#$this->db->select('(SELECT SUM(soi.item_profit) FROM ' . $this->sales_order_item . ' as soi WHERE soi.so_id = so.id AND soi.service_form_type IN(1, 2)) as item_profit', FALSE);
		
		$this->db->join($this->branches . ' as brn', 'brn.id = serv.branch_id', 'left');
        $this->db->join($this->users . ' as u', 'u.id = serv.engineer_id', 'left AND u.status !=', 10);
        $this->db->join($this->sales_order . ' as so', 'so.service_id = serv.id AND so.status != 10', 'left');
        $this->db->join($this->customer . ' as cus', 'cus.id = serv.customer_id', 'left');
		
        if (!empty($from_date)) {
            $this->db->where('DATE(serv.received_date) >=', date('Y-m-d', strtotime($from_date)));
        }
        if (!empty($to_date)) {
        $this->db->where('DATE(serv.received_date) <=', date('Y-m-d', strtotime($to_date)));
        }
        if ((!empty($cus))) {
            $this->db->where('serv.customer_id', $cus);
        }
        if ((!empty($engg))) {
            $this->db->where('serv.engineer_id', $engg);
        }
        
        if ((!empty($branch))) {
            $this->db->where('serv.branch_id', $branch);
        }
        
        if ((!empty($type))) {
            $this->db->where('serv.service_status', $type);
        }
        
        if ((!empty($home_date))) {
            $this->db->where('serv.estimated_delivery_date <=', date('Y-m-d H:i:s', strtotime($home_date)));
            $this->db->where('serv.service_status != 3');
        }
        $this->db->where('serv.status !=', 10);
        //$this->db->where('u.status !=', 10);
        $this->db->order_by('serv.id', 'desc');
        $this->db->group_by('serv.id');
        $this->db->where_in('serv.branch_id', $this->user_data['branch_ids'], FALSE);
        
        return $this->db->get($this->services . ' as serv')->result();
    }
    
    public function list_branche() {
        $this->db->select('id, branch_name');
        $this->db->where('status != 10');
        $this->db->where('status != 2');
        $this->db->order_by('id', 'asc');
        return $this->db->get($this->branches)->result();
    }


}

?>