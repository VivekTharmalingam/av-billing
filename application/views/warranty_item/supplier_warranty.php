<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo base_url(); ?>invoice_return_warrantyitem/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                            <h2>Update Supplier Warranty</h2>
                            </span>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="" action="<?php echo $form_action; ?>" method="post" id="log_form" name="log_form" enctype="multipart/form-data">
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    
                                    <div class="smart-form">  
                                            <fieldset>
                                                 <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>
                                            <div class="row">
                                                <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <section style="margin-left: 10px; margin-right: 10px;">
                                                        <label class="label">Supplier<span style="color: red">*</span></label>
                                                        <label class="select">
                                                            <select name="warranty_supplier_id" class="select2 warranty_supplier_id" id="warranty_supplier_id">
                                                                <option value="">Select Supplier name</option>
                                                                <?php foreach ($suppliers as $key => $sup) { ?>
                                                                    <option value="<?php echo $sup->id; ?>" <?php echo ((!empty($warranty_items[0]['warranty_supplier_id'])) && ($warranty_items[0]['warranty_supplier_id'] == $sup->id)) ? 'selected' : '';?>><?php echo $sup->name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                    <section style="margin-left: 10px; margin-right: 10px;">
                                                        <label class="label">Date<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="warranty_supplier_date" id="warranty_supplier_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php echo (!empty($warranty_items[0]['warranty_supplier_date_str'])) ? $warranty_items[0]['warranty_supplier_date_str'] : date('d/m/Y'); ?>" />
                                                        </label>
                                                    </section>
                                                    <section style="margin-left: 10px; margin-right: 10px;">
                                                        <label class="label">Serial No. </label>
                                                        <label class="input"><i class="icon-append fa fa-key"></i>
                                                            <input type="text" name="warranty_item_sl_no" id="warranty_item_sl_no" value="<?php echo (!empty($warranty_items[0]['warranty_item_sl_no'])) ? $warranty_items[0]['warranty_item_sl_no'] : $warranty_items[0]['serial_nos_txt']; ?>" />
                                                        </label>
                                                    </section>
                                                    
                                                </div>
                                                <div class="col-md-6">
                                                    <section class="col-md-12" style="padding-top: 2px;">
                                                        <section class="alert alert-info" style="border-top-width: 5px!important; border-left-width:0px!important;border-bottom: 0px; background: rgb(249, 236, 205);border-color: #8b1a0d;">
                                                            <label class="label" style="font-size: 11px;font-style: italic;"><b style="border-bottom: 1px solid;">Invoice Return Details :-</b></label>
                                                            <label class="label" style="font-size: 11px;">Invoice No.: <span><?php echo $warranty_items[0]['inv']; ?> </span></label>
                                                            <label class="label" style="font-size: 11px;">Invoice Date: <span><?php echo $warranty_items[0]["so_date_txt"]; ?></span></label>
                                                            <label class="label" style="font-size: 11px;">Invoice Return Date: <span><?php echo $warranty_items[0]["ret_date"]; ?></span></label>
                                                            <label class="label" style="font-size: 11px;">Item Code: <span><?php echo $warranty_items[0]['pdt_code']; ?></span></label>
                                                            <label class="label" style="font-size: 11px;">Item Description: <span><?php echo $warranty_items[0]['pdt_desc']; ?></span></label>
                                                            <label class="label" style="font-size: 11px;">Quantity: <span><?php echo $warranty_items[0]['return_qty']; ?></span></label>
                                                        </section>
                                                    </section>
                                                </div>
                                                </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <section style="margin-left: 10px; margin-right: 10px;">
                                                            <label class="label">Warranty Limitations <span style="color: red">*</span></label>
                                                            <label class="textarea">
                                                                <textarea rows="5" class="custom-scroll lowercase warranty_limitations" name="warranty_limitations"><?php echo (!empty($warranty_items[0]['warranty_limitations'])) ? $warranty_items[0]['warranty_limitations'] : ""; ?></textarea>
                                                            </label>
                                                        </section>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <footer>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                                            </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript">
$(document).ready(function () {
    
    var $orderForm = $("#log_form").validate({
        // Rules for form validation
        rules : {
                warranty_supplier_id : {
                        required : true
                },
                warranty_supplier_date : {
                        required : true
                },
                warranty_limitations : {
                        required : true
                }
        },

        // Messages for form validation
        messages : {
                warranty_supplier_id: {
                        required : 'Please Select Supplier'
                },
                warranty_supplier_date: {
                        required : 'Please Select Supplier'
                },
                warranty_limitations: {
                        required : 'Please Enter Warranty Limitations'
                }
        },
        // Do not change code below
        errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
        }
    });

});
</script>
