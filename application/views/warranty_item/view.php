<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo $list_link; ?>" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-file-text-o"></i> </span>
                            <h2>Invoice Return Item with warranty View</h2>
<!--                             <span class="widget-toolbar" rel="tooltip" data-placement="bottom" data-original-title="Purchase Order List" onclick="location.href='<?php echo base_url(); ?>purchase_orders/'">
                                <i class="fa fa-list" aria-hidden="true"></i>
                            </span>-->
                             </span>
                            
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body">
                                <?php //print_r($warranty_items); ?>
                                <!--<span id="watermark">Malar ERP.</span>-->
                                <table id="user" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Invoice Number</td>
                                            <td ><?php echo $warranty_items[0]['inv']; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Return Date</td>
                                            <td ><?php echo $warranty_items[0]["ret_date"]; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="color: #A90329;"><b>Item Details</b></td>
                                        </tr>
                                        
                                         <tr>
                                            <td style="font-weight: bold;">Item code</td>
                                            <td ><?php echo $warranty_items[0]['pdt_code']; ?></td>
                                        </tr>
                                         <tr>
                                            <td style="font-weight: bold;">Item Description</td>
                                            <td ><?php echo $warranty_items[0]['pdt_desc']; ?></td>
                                        </tr>
                                        
                                         <tr>
                                            <td style="font-weight: bold;">Returned Quantity</td>
                                            <td ><?php echo $warranty_items[0]['return_qty']; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Warranty</td>
                                            <td ><?php echo $warranty_items[0]['warranty_cleared_str']; ?></td>
                                        </tr>
                                        <?php if($warranty_items[0]['warranty_cleared']==2) { ?>
                                        <tr>
                                            <td style="font-weight: bold;">Warranty Cleared Date</td>
                                            <td ><?php echo date('d/m/Y', strtotime($warranty_items[0]["warranty_cleared_date"])); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Warranty Cleared Remark</td>
                                            <td ><?php echo nl2br($warranty_items[0]['warranty_cleared_remark']); ?></td>
                                        </tr>
                                        <?php } ?>
                                        <?php if(!empty($warranty_items[0]['warranty_supplier_id'])) { ?>
                                        <tr>
                                            <td style="font-weight: bold;">Supplier Name</td>
                                            <td ><?php echo $warranty_items[0]['sup_name_txt']; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Serial No.</td>
                                            <td ><?php echo $warranty_items[0]['warranty_item_sl_no']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
