<table width="1000" border="0" class="lowercase" align="center" style="font-family:Times New Roman; font-size:20px;">
         <tr><td colspan="4" style="text-align: center;font-size:26px;">Warranty Clearance</td></tr>
         <tr><td >&nbsp;</td></tr>
         <tr>
             <td width="210">Supplier Name </td>
             <td width="10">:</td>
             <td width="380" style="height: 35px;font-size:18px;vertical-align: middle;"><?php echo strtoupper($supplier->name); ?></td>
             <td width="400" style="height: 35px;font-size:18px;vertical-align: middle;text-align: right;padding-right: 5px;"><span style="font-size: 20px;">Date</span> : <?php echo $warranty_items[0]['warranty_supplier_date_str']; ?></td>
         </tr>
         <tr>
             <td width="210">Contact Person </td>
             <td width="10">:</td>
             <td width="380" style="height: 35px;font-size:18px;vertical-align: middle;"><?php echo text($supplier->contact_name); ?></td>
             <td width="400" >&nbsp;</td>
         </tr>
         <tr>
             <td width="210">Contact Number </td>
             <td width="10">:</td>
             <td width="380" style="height: 35px;font-size:18px;vertical-align: middle;"><?php echo text($supplier->phone_no); ?></td>
             <td width="400" >&nbsp;</td>
         </tr>
         <tr>
             <td width="210">Address </td>
             <td width="10">:</td>
             <td width="380" style="height: 35px;font-size:18px;vertical-align: middle;"><?php echo $supplier->address; ?></td>
             <td width="400" >&nbsp;</td>
         </tr>
         <tr><td >&nbsp;</td></tr>
         <tr><td colspan="4" style="font-size:24px;">Item Details :-</td></tr>
         <tr><td width="1000" colspan="4">
         <table width="1000" style="border-collapse:collapse; vertical-align:top; color:#000000;font-family:Times New Roman;">
             <tr>
                 <td style="font-size:20px;border-right:1px solid #090808;border-top:1px solid #090808;border-left:1px solid #090808;border-bottom:1px solid #090808;vertical-align:middle; padding:25px 15px;" height="30"  width="180">Serial No.</td>
                 <td style="font-size:20px;border-right:1px solid #090808;border-top:1px solid #090808;border-bottom:1px solid #090808;vertical-align:middle;padding:25px 15px;" width="320"><?php echo $warranty_items[0]['warranty_item_sl_no']; ?></td>
                 <td style="font-size:20px;border-right:1px solid #090808;border-top:1px solid #090808;border-bottom:1px solid #090808;vertical-align:middle;padding:25px 15px;" width="180">Quantity</td>
                 <td style="font-size:20px;border-right:1px solid #090808;border-top:1px solid #090808;border-bottom:1px solid #090808;vertical-align:middle;padding:25px 15px;" width="320"><?php echo $warranty_items[0]['return_qty']; ?></td>
             </tr>
             <tr>
                 <td style="font-size:20px;border-right:1px solid #090808;border-left:1px solid #090808;border-bottom:1px solid #090808;vertical-align:middle; padding:25px 15px;" height="30"  width="180">Item Code</td>
                 <td style="font-size:20px;border-right:1px solid #090808;border-bottom:1px solid #090808;vertical-align:middle;padding:25px 15px;" width="320"><?php echo $warranty_items[0]['pdt_code']; ?></td>
                 <td style="font-size:20px;border-right:1px solid #090808;border-bottom:1px solid #090808;vertical-align:middle;padding:25px 15px;" width="180">Item Description</td>
                 <td style="font-size:20px;border-right:1px solid #090808;border-bottom:1px solid #090808;vertical-align:middle;padding:25px 15px;" width="320"><?php echo $warranty_items[0]['pdt_desc']; ?></td>
             </tr>
         </table>
         </td></tr>
         <tr><td >&nbsp;</td></tr>
         <tr><td colspan="4" style="font-size:24px;">Warranty Limitations :-</td></tr>
         <tr><td colspan="4">
                <?php echo nl2br($warranty_items[0]['warranty_limitations']); ?>
         </td></tr>
</table>