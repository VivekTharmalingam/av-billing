<style>
    /*
    * Customize vertical scroll bar - Start
    */
    
   .plot_no_cat {
       max-height: 410px;
       overflow: auto;
   }
   
   .plot_no_cat::-webkit-scrollbar {
       width: 15px;
   } 
   
   .plot_no_cat::-webkit-scrollbar-thumb:vertical {
     background-color: #e0a726;
     background-clip: padding-box;
     border: 4px solid transparent;
     //border-width: 2px 2px 2px 6px;
     border-radius: 25px;
     min-height: 28px;
     padding: 100px 0 0;
     box-shadow: inset 1px 1px 0 rgb(220, 172, 0), inset 0 -1px 0 rgb(220, 172, 0);
   }
   
   /*
    * Customize vertical scroll bar - End
    */
</style>
<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo base_url(); ?>invoice_return_warrantyitem/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                            <h2>Update Warranty Status</h2>
                            </span>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="" action="<?php echo $form_action; ?>" method="post" id="log_form" name="log_form" enctype="multipart/form-data">
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    
                                    <div class="smart-form">  
                                            <fieldset>
                                                <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Warranty Status Log Details</legend>
                                                <div class="col-md-12">
                                                <div class="col-md-6 plot_no_cat" style="max-height: 300px;overflow-y: auto;overflow-x: hidden;">
                                                    <div class="col-md-12" style="border-radius: 5px; background-color: #FFF; margin-bottom: 5px;">
                                                            <div style="padding-top: 10px;padding-bottom: 5px; padding-left: 10px; margin-left: 0px;color: #06ad9c;" class="col-md-12"><b>Status : </b>&nbsp;Invoice Return</div>
                                                            <div style="padding-top: 10px;padding-bottom: 10px; padding-left: 10px; margin-left: 0px;" class="col-md-12"><b>Log Description : </b>&nbsp;Invoice was return on <?php echo date('d/m/Y',strtotime($warranty_items[0]["return_date"])); ?></div>
                                                            <div class="col-md-12" style="margin-left: 0px; padding-bottom: 6px; padding-left: 10px; background-color: rgba(204, 204, 204, 0.2);">
                                                                <div class="col-md-6"><b style="font-style: italic">Created On :</b>&nbsp;&nbsp;<span style="font-style: italic"><?php echo date('d-m-Y h:i:s A',strtotime($warranty_items[0]["return_date"])); ?></span></div>
                                                            </div>
                                                    </div>
                                                    <?php foreach ($warranty_status_log as $key => $row) { ?>
                                                        <div class="col-md-12" style="border-radius: 5px; background-color: #FFF; margin-bottom: 5px;">
                                                            <div style="padding-top: 10px;padding-bottom: 5px; padding-left: 10px; margin-left: 0px;color: #06ad9c;" class="col-md-12"><b>Status : </b>&nbsp;<?php echo ($row->warranty_status_txt != '') ? $row->warranty_status_txt : 'N/A'; ?></div>
                                                            <div style="padding-top: 10px;padding-bottom: 10px; padding-left: 10px; margin-left: 0px;" class="col-md-12"><b>Log Description : </b>&nbsp;<?php echo text($row->log_description); ?></div>
                                                            <div class="col-md-12" style="margin-left: 0px; padding-bottom: 6px; padding-left: 10px; background-color: rgba(204, 204, 204, 0.2);">
                                                                <div class="col-md-6"><b style="font-style: italic">Created On :</b>&nbsp;&nbsp;<span style="font-style: italic"><?php echo $row->created_date_str; ?></span></div>
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                                </div>
                                                <div class="col-md-6">
                                                    <section class="col-md-12" style="padding-left: 6px;padding-top: 2px;">
                                                        <section class="alert alert-info" style="border-top-width: 5px!important; border-left-width:0px!important;border-bottom: 0px; background: rgb(249, 236, 205);border-color: #8b1a0d;">
                                                            <label class="label" style="font-size: 11px;font-style: italic;"><b style="border-bottom: 1px solid;">Invoice Return Details :-</b></label>
                                                            <label class="label" style="font-size: 11px;">Invoice No.: <span><?php echo $warranty_items[0]['inv']; ?> </span></label>
                                                            <label class="label" style="font-size: 11px;">Invoice Date: <span><?php echo $warranty_items[0]["so_date_txt"]; ?></span></label>
                                                            <label class="label" style="font-size: 11px;">Invoice Return Date: <span><?php echo $warranty_items[0]["ret_date"]; ?></span></label>
                                                            <label class="label" style="font-size: 11px;">Item Code: <span><?php echo $warranty_items[0]['pdt_code']; ?></span></label>
                                                            <label class="label" style="font-size: 11px;">Item Description: <span><?php echo $warranty_items[0]['pdt_desc']; ?></span></label>
                                                            <label class="label" style="font-size: 11px;">Returned Quantity: <span><?php echo $warranty_items[0]['return_qty']; ?></span></label>
                                                        </section>
                                                    </section>
                                                </div>
                                                </div>
                                            </fieldset>
                                            
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Status<span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-flag-o"></i>
                                                        <input type="text" name="warranty_status_txt" id="warranty_status_txt" value="" />
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Log Description </label>
                                                    <label class="textarea"><textarea rows="3" name="log_description" id="log_description" maxlength="500"></textarea> </label> 
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Warranty<span style="color: red">*</span></label>
                                                    <label class="select">
                                                        <select name="warranty_status" id="warranty_status" class="select2 warranty_status" >
                                                            <option value="1" <?php echo ($warranty_items[0]['warranty_cleared']==0 || $warranty_items[0]['warranty_cleared']==1) ? "selected" : ""; ?>>Processing</option>
                                                            <option value="2" <?php echo ($warranty_items[0]['warranty_cleared']==2) ? "selected" : ""; ?>>Cleared</option>
                                                        </select>  
                                                    </label>
                                                </section>
                                            </div>
                                           
                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript">
$(document).ready(function () {
    
    var $orderForm = $("#log_form").validate({
        // Rules for form validation
        rules : {
                warranty_status_txt : {
                        required : true
                }
        },

        // Messages for form validation
        messages : {
                warranty_status_txt: {
                        required : 'Please Enter Status'
                }
        },
        // Do not change code below
        errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
        }
    });

});
</script>
