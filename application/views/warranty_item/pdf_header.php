    <html>
	<head>
		<style>
			@page {
			  size: portrait;
			  header: html_header;
			  footer: html_footerPageNo;
			  margin-footer: 0px;
			  margin-bottom: 30mm;
			}
			
			.barcode {
				padding: 1.5mm;
				margin-top: 10px;margin-bottom: 10px;
				vertical-align: top;
				color: #424242;
				width:200px;
			}
		</style>
	</head>
	<body>
        <htmlpagefooter name="footerPageNo" style="display:none">
                <table width="100%" style="vertical-align: bottom; font-family: calibri; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
                        <tr>
                                <td width="100%" align="center">Page {PAGENO}/{nbpg}</td>
                        </tr>
                </table>
        </htmlpagefooter>
        <htmlpageheader name="header" style="display:block">
            <table width="100%" border="0" class="lowercase" align="center" style="font-family:Times New Roman; font-size:16px;">
             <?php $comp_logo = ( !empty( $company->comp_logo ) && file_exists( UPLOADS . $company->comp_logo ) ) ? base_url() . UPLOADS . $company->comp_logo : 'assets/images/logo.png'; ?>
             <tr>
                 <td width="15%"><img src="<?php echo $comp_logo;?>" height="60"/></td>
                 <td width="45%" colspan="4" style="text-align: center;font-size:12px;"><span style="font-size:18px;"><?php echo $company->comp_name;?></span><br><?php echo $company->comp_address.' <br>Tel:&nbsp;'.$company->comp_phone; ?> Email: <?php echo $company->comp_email; ?></td>
                 <td width="20%" style="font-style: italic; text-align: center;">&nbsp;</td>
             </tr>
             <tr><td colspan="6" style="text-align:center; font-size:10px; font-weight:bold; border-bottom:2px solid #0009FF;">&nbsp;</td></tr>
            </table>
        </htmlpageheader>