<table border="0" cellpadding="3" cellspacing="0" style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:14px; width: 1000px;">
    <?php echo $head; ?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>
<?php $colspan = 8;?>
<table width="100%" border="1" style="border-collapse:collapse; margin-left:10px" cellpadding="5" cellspacing="0">
	<thead>
	<tr style="font-family:Times new roman;font-weight:bold;">
		<th width="5%">S.No.</th>
		<th width="11%">Inv No.</th>
		<th width="10%">Inv Date</th>
		<th width="12%">Issued By</th>
		<th width="25%">Customer</th>
		<th width="20%">Customer Ref.</th>
		<th width="10%">Amount</th>
		<th width="12%">Pay Status</th>
		<?php if(in_array(7, $permission)) {?>
			<?php $colspan ++;?>
			<th width="12%">Profit</th>
		<?php }?>
	</tr>
	</thead>
	<tbody>
	<?php if (count($invoices)) { ?>
		<?php $total_invoice_amt = 0;?>
		<?php $total_profit = 0;?>
		<?php foreach ($invoices as $key => $row) { ?>
			<?php $total_invoice_amt += $row->total_amt;?>
			<?php $total_profit += $row->tot_profit;?>
			<tr>
				<td><?php echo ++ $key; ?></td>
				<td><?php echo text($row->so_no); ?></td>
				<td><?php echo $row->so_date_str; ?></td>
				<td><?php echo strtoupper($row->issued_by); ?></td>
				<td><?php echo strtoupper(text($row->name)); ?></td>
				<td><?php echo strtoupper(text($row->your_reference)); ?></td>
				<td align="right"><?php echo number_format($row->total_amt, 2); ?></td>
				<td><?php echo text($row->pay_sts); ?></td>
				<?php if(in_array(7, $permission)) {?>
					<td align="right"><?php echo number_format($row->tot_profit, 2); ?></td>
				<?php }?>
			</tr>
		<?php } ?>
		
			<tr>
				<td style="text-align: right;" colspan="6">Total</td>
				<td align="right"><?php echo number_format($total_invoice_amt, 2); ?></td>
				<td></td>
				<?php if(in_array(7, $permission)) {?>1
					<td align="right"><?php echo number_format($total_profit, 2); ?></td>
				<?php }?>
			</tr>
		
	<?php }
	else { ?>
		<tr>
			<td colspan="<?php echo $colspan;?>" align="center">No Record Found</td>
		</tr>
	<?php } ?>
	</tbody>
</table>