<?php
$ti = time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Invoice_Summary_report" . $ti . ".xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<?php $colspan = 7;?>
<table width="100%" border="1" style="border-collapse:collapse; margin-left:10px" cellpadding="5">
	<thead style="font-family: Times new roman;">
	<tr>
		<th width="10%">Inv No.</th>
		<th width="10%">Inv Date</th>
		<th width="15%">Issued By</th>
		<th width="25%">Customer</th>
		<th width="15%">Customer Ref.</th>
		<th width="15%">Amount</th>
		<th width="10%">Pay Status</th>
		<?php if(in_array(7, $permission)) {?>
			<?php $colspan ++;?>
			<th width="10%">Profit</th>
		<?php }?>
	</tr>
	</thead>
	<tbody>
	<?php if (count($invoices)) { ?>
		<?php $total_invoice_amt = 0;?>
		<?php $total_profit = 0;?>
		<?php foreach ($invoices as $key => $row) { ?>
			<?php $total_invoice_amt += $row->total_amt;?>
			<?php $total_profit += $row->tot_profit;?>
			<tr>
				<td style="padding:8px;"><?php echo text($row->so_no); ?></td>
				<td style="padding:8px;"><?php echo $row->so_date_str; ?></td>
				<td><?php echo $row->issued_by; ?></td>
				<td style="padding:8px;"><?php echo strtoupper(text($row->name)); ?></td>
				<td style="padding:8px;"><?php echo strtoupper(text($row->your_reference)); ?></td>
				<td style="padding:8px; text-align: right;"><?php echo number_format($row->total_amt,2); ?></td>
				<td style="padding:8px;"><?php echo text($row->pay_sts); ?></td>
				<?php if(in_array(7, $permission)) {?>
					<td style="padding:8px; text-align: right;"><?php echo number_format($row->tot_profit, 2); ?></td>
				<?php }?>
			</tr>
		<?php } ?>
		<tr>
			<td style="text-align: right;" colspan="5">Total</td>
			<td align="right"><?php echo number_format($total_invoice_amt, 2); ?></td>
			<td></td>
			<?php if(in_array(7, $permission)) {?>
				<td align="right"><?php echo number_format($total_profit, 2); ?></td>
			<?php }?>
		</tr>
	<?php } else { ?>
		<tr>
			<td colspan="<?php echo $colspan;?>" align="center">No Record Found</td>
		</tr>
	<?php } ?>
	</tbody>
</table>