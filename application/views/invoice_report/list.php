<script>window.localStorage.clear();</script>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment">
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span>
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">&nbsp;</div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="widget-body">
                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false"	>
                            <header>
                                <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                <h2>Invoice Summary</h2>
                            </header>

                            <div>
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="box-body table-responsive">
                                        <div class="date-range"></div>
                                        <form name="order_search" action="<?php echo $form_action; ?>" method="post" class="search_form validate_form" style="display: inline-block; padding: 15px 10px;">
                                            <div class="col-md-12">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="search_date" data-placeholder="dd/mm/yyyy - dd/mm/yyyy" id="date_alloc" style="padding: 6px 12px;margin-right:5%; width: 100%;"  id="search_date" name="search_date" value="<?php echo (!empty($search_date)) ? $search_date : ''; ?>" autocomplete="off" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-user"></i>
                                                            </div>
                                                            <select name="issued_by" class="select2 issued_by">
                                                                <option value="">Issued By - ALL</option>
                                                                <?php foreach ($users as $key => $user) { ?>
                                                                    <option value="<?php echo $user->id; ?>" <?php echo ($user->id == $issued_by) ? 'selected' : ''; ?>><?php echo $user->name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-shopping-cart"></i>
                                                            </div>
                                                            <select name="cus_name" class="select2 cus_name">
                                                                <option value="">Customer - ALL</option>
                                                                <option value="cash" <?php echo ($custom == 'cash') ? 'selected' : ''; ?>>Cash</option>
                                                                <?php foreach ($customers as $key => $customer) { ?>
                                                                    <option value="<?php echo $customer->id; ?>" <?php echo ($customer->id == $custom) ? 'selected' : ''; ?>><?php echo $customer->name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-dollar"></i>
                                                            </div>
                                                            <select name="payment_status" class="select2 payment_status">
                                                                <option value="">Payment status - All</option>
                                                                <option value="1" <?php echo ($pay_sts == 1) ? 'selected' : ''; ?>>Paid</option>
                                                                <option value="2" <?php echo ($pay_sts == 2) ? 'selected' : ''; ?>>Unpaid</option>
                                                                                                                                        <!--<option value="3" <?php echo ($pay_sts == 3) ? 'selected' : ''; ?>>Cancelled</option>-->
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-dollar"></i>
                                                            </div>
                                                            <select name="branch_id" class="select2">
                                                                <option value="">Branch - ALL</option>
                                                                <?php foreach ($branches as $key => $branch) { ?>
                                                                    <option value="<?php echo $branch->id; ?>" <?php echo ($branch->id == $branch_id) ? 'selected' : ''; ?>><?php echo $branch->branch_name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-dollar"></i>
                                                            </div>
                                                            <select name="generated_from" class="select2">
                                                                <option value="">Generated From - All</option>
                                                                <option value="1" <?php echo ($generated_from == 1) ? 'selected' : ''; ?>>Invoice</option>
                                                                <option value="2" <?php echo ($generated_from == 2) ? 'selected' : ''; ?>>Quotation</option>
                                                                <option value="3" <?php echo ($generated_from == 3) ? 'selected' : ''; ?>>Exchange Order</option>
                                                                <option value="4" <?php echo ($generated_from == 4) ? 'selected' : ''; ?>>Service</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-3" style="text-align: right;">
                                                    <input type="submit" name="submit" value="Reset" id="reset" class="btn btn-success btn-warning" />&nbsp;&nbsp;
                                                    <input type="submit" name="submit" value="Search" class="btn btn-success btn-primary"/>
                                                </div>
                                                <div class="col-md-1">
                                                    <input type="submit" name="submit" value="excel" class="submit_excel" />
                                                </div>
                                                <div class="col-md-1">
                                                    <input type="submit" name="submit" value="pdf" class="submit_pdf" />
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                    <?php $colspan = 9; ?>
                                    <table id="" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                            <tr>
                                                <th width="5%" data-class="expand">S.No.</th>
                                                <th width="10%" ><i class="fa fa-fw fa-fax text-muted hidden-md hidden-sm hidden-xs"></i> Inv No.</th>
                                                <th width="9%" data-hide="phone"><i class="fa fa-fw fa-calendar text-muted hidden-md hidden-sm hidden-xs"></i> Inv Date</th>
                                                <th width="10%" data-hide="phone,tablet"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Issued By</th>
                                                <th width="15%" data-hide="phone,tablet"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Customer</th>
                                                <th width="13%" data-hide="phone"><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Customer Ref.</th>
                                                <th width="10%" data-hide="phone"><i class="fa fa-fw fa-dollar text-muted hidden-md hidden-sm hidden-xs"></i> Amount</th>
                                                <th width="10%" data-hide="phone,tablet"><i class="fa fa-fw fa-flag-o text-muted hidden-md hidden-sm hidden-xs"></i>Pay Status</th>
                                                <?php if (in_array(7, $permission)) { ?>
                                                    <?php $colspan = 10; ?>
                                                    <th width="8%" data-hide="phone"><i class="fa fa-fw fa-dollar text-muted hidden-md hidden-sm hidden-xs"></i>Profit</th>
                                                <?php } ?>
                                                <th width="10%" data-hide="phone" style="text-align: center;"><i class="fa fa-fw fa-cog txt-color-blue hidden-md hidden-sm hidden-xs"></i> Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (!empty($invoices)) {
                                                $total_invoice_amt = 0;
                                                $total_profit = 0;
                                                foreach ($invoices as $key => $row) {
                                                    ?>
                                                    <?php $total_invoice_amt += $row->total_amt; ?>
                                                    <?php $total_profit += $row->tot_profit; ?>
                                                    <tr>
                                                        <td><?php echo ++$key; ?></td>
                                                        <td><?php echo text($row->so_no); ?></td>
                                                        <td><?php echo $row->so_date_str; ?></td>
                                                        <td><?php echo $row->issued_by; ?></td>
                                                        <td><?php echo text($row->name); ?></td>
                                                        <td><?php echo text($row->your_reference); ?></td>
                                                        <td align="right"><?php echo number_format($row->total_amt, 2); ?></td>
                                                        <td><?php echo text($row->pay_sts); ?></td>
                                                        <?php if (in_array(7, $permission)) { ?>
                                                            <?php $colspan ++; ?>
                                                            <td align="right"><?php echo number_format($row->tot_profit, 2); ?></td>
                                                        <?php } ?>
                                                        <td align="center">
                                                            <?php if (in_array(1, $permission)) { ?>
                                                                <a class="label btn btn-warning view" href="<?php echo $view_link . $row->id ?>"><span>View</span></a>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                <tr>
                                                    <td style="text-align: right;" colspan="6">Total</td>
                                                    <td align="right"><?php echo number_format($total_invoice_amt, 2); ?></td>
                                                    <td></td>
                                                    <?php if (in_array(7, $permission)) { ?>
                                                        <td align="right"><?php echo number_format($total_profit, 2); ?></td>
                                                    <?php } ?>
                                                    <td></td>
                                                </tr>
                                            <?php } else {
                                                ?>
                                                <tr>
                                                    <td style="text-align: center;" colspan="<?php echo $colspan; ?>">No Record Found!</td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- end widget content -->
                            </div>
                        </div>
                    </div>
                </article><!-- /.row -->
            </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function () {




        $('#reset').click(function () {
            //location.reload();
        });
        /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            'iDisplayLength': 25,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            }
        });

        /* END BASIC */
        $('#date_alloc').daterangepicker({
            autoUpdateInput: false,
            opens: 'center',
            startDate: moment().startOf('month'),
            endDate: moment().endOf('month'),
            locale: {
                format: 'MM/DD/YYYY h:mm A',
                cancelLabel: 'Cancel'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            //alert(start + '|' + end + '|' + label);
            // getResults();
        });

        $(function () {
            $('#search_date').daterangepicker({format: 'DD/MM/YYYY'});
        });
    });

</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/customer.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/custom.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datepicker/daterangepicker.js"></script>