<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo base_url(); ?>bank_transfer/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                            </span>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="" action="<?php echo $form_action; ?>" method="post" id="transfer_form" name="transfer_form" enctype="multipart/form-data">

                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">                                   
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>

                                            <div class="row">
                                                <input type="hidden" class="form-control" name="frm_account_id" value="<?php echo $transfer->from_bank_id;?>" />
                                                <input type="hidden" class="form-control" name="to_account_id" value="<?php echo $transfer->to_bank_id;?> "/ >
                                                <div class="col col-6">                                            
                                                    <section>
                                                        <section>
                                                            <label class="label">Debit Account No. <span style="color: red;">*</span> <span style="position: absolute;right: 15px;" class="frm_crnt_bln"></span></label>
                                                            <label class="select">
                                                                <select name="frm_account_id_dis" class="frm_account_id select2" id="frm_account_id" disabled="">  
                                                                    <option value="" data-frmcrntbln="" >Select Account Number</option>
                                                                    <?php foreach ($active_bank as $key => $bank) { ?>
                                                                        <option value="<?php echo $bank->id; ?>" data-bnktext="<?= $bank->bank_name ?>" data-frmcrntbln="<?= $bank->current_balance ?>" <?= ($bank->id==$transfer->from_bank_id) ? "selected" : ""; ?>><?php echo $bank->bank_name.' / '.$bank->account_no; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </label>
                                                        </section>
                                                    </section>
                                                    
                                                    <section>
                                                        <label class="label">Amount <span style="color: red">*</span></label>
                                                        <label class="input"><i class="icon-append fa fa-money"></i>
                                                            <input type="text" data-placeholder="100" name="trans_amount" id="trans_amount" class="float_value trans_amount" value="<?php echo $transfer->amount;?>" readonly="" />
                                                        </label>
                                                    </section>
                                                    
                                                    <section>
                                                        <label class="label">Reason</label>
                                                        <label class="textarea textarea-expandable"><textarea rows="3" name="reason" id="reason" maxlength="500"><?php echo $transfer->reason;?></textarea> </label> <div class="note">
                                                            <strong>Note:</strong> expands on focus.
                                                        </div>
                                                    </section>
                                                </div>
                                           
                                                <div class="col col-6"> 
                                                    <section>
                                                        <section>
                                                            <label class="label">Credit Account No. <span style="color: red;">*</span> <span style="position: absolute;right: 15px;" class="to_crnt_bln"></span></label>
                                                            <label class="select">
                                                                <select name="to_account_id_dis" class="to_account_id select2" id="to_account_id" disabled="">  
                                                                    <option value="" data-tocrntbln="" >Select Account Number</option>
                                                                    <?php foreach ($active_bank as $key => $bank) { ?>
                                                                        <option value="<?php echo $bank->id; ?>" data-tocrntbln="<?= $bank->current_balance ?>" <?=($bank->id==$transfer->to_bank_id) ? "selected" : ""; ?>><?php echo $bank->bank_name.' / '.$bank->account_no; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </label>
                                                        </section>
                                                    </section>
                                                    
                                                    <section>
                                                        <label class="label">Payment Type <span style="color: red">*</span></label>
                                                        <label class="select">
                                                            <select name="payment_type"  class="select2 payment_type" id="payment_type">
                                                                <option value="">Select Payment Type</option>
                                                                <?php foreach($payment_types as $key => $value) {?>
                                                                    <option value="<?php echo $value->id;?>" data-is_cheque="<?php echo $value->is_cheque;?>" <?= ($value->id==$transfer->payment_type) ? "selected" : ""; ?>><?php echo $value->type_name;?></option>
                                                                <?php }?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                    
                                                    <section class="cheque_details">
                                                        <label class="label">Bank Name <span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="form-control cheque_bank_name" name="cheque_bank_name" data-placeholder="SBI" maxlength="100" value="<?php echo $transfer->cheque_bank_name;?>" />
                                                        </label>
                                                    </section>
                                                    
                                                    <section class="cheque_details">
                                                        <label class="label">Cheque No <span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="form-control" name="cheque_acc_no" data-placeholder="Cheque No" maxlength="100" value="<?php echo $transfer->cheque_acc_no;?>" />
                                                        </label>
                                                    </section>
                                                    
                                                     <section class="cheque_details">
                                                        <label class="label">Cheque Date <span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="bootstrap-datepicker-strdatecls" name="cheque_date" data-placeholder="01/01/2016" maxlength="100" value="<?php echo date('d-m-Y',strtotime($transfer->cheque_date));?>" />
                                                        </label>
                                                    </section>
                                                </div>                                                
                                            </div>
											
                                            <div class="row">
                                                <!--<section class="col col-6">
                                                    <label class="label">Status <span style="color: red">*</span></label>
                                                    <label class="select">
                                                        <select name="status" id="status" class="select2" >
                                                            <option value="1">Active</option>
                                                            <option value="2">Inactive</option>
                                                        </select>  </label>
                                                </section>-->
                                            </div>

                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/bank_transfer.js"></script>
