<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                     <a href="<?php echo base_url(); ?>bank_transfer/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>Bank Transfer - View</h2>	
<!--                             <span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Users List"><a href="<?php echo $list_link; ?>" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a></span>-->
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body"> 
                                <span id="watermark"><?php echo WATER_MARK; ?></span>
                                 <table id="user" class="table table-bordered table-striped" style="clear: both;">
                                    <tbody>
                                        <tr>
                                            <td><b>Debit Account No.</b></td>
                                            <td><?php echo $transfer->bankName1 . ' / ' . $transfer->accountNo1;?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Credit Account No.</b></td>
                                            <td><?php echo $transfer->bankName2 . ' / ' . $transfer->accountNo2;?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Amount</b></td>
                                            <td><?php echo $transfer->amount;?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Payment Type</b></td>
                                            <td><?php echo $transfer->type_name;?></td>
                                        </tr>
                                        <?php if($transfer->is_cheque == 1) {?>
                                            <tr>
                                                <td><b>Cheque Bank Name</b></td>
                                                <td><?php echo $transfer->cheque_bank_name;?></td>
                                            </tr>
                                            <tr>
                                                <td><b>Cheque No.</b></td>
                                                <td><?php echo $transfer->cheque_acc_no;?></td>
                                            </tr>
                                            <tr>
                                                <td><b>Cheque Date</b></td>
                                                <td><?php echo date('d-m-Y',strtotime($transfer->cheque_date));?></td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <td><b>Amount</b></td>
                                            <td><?php echo $transfer->amount;?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Reason</b></td>
                                            <td><?php echo $transfer->reason;?></td>
                                        </tr>
                                    </tbody>
                                </table>                                
                        </div>
                        <!-- end widget div -->

                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
