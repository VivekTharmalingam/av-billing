<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php
        echo breadcrumb_links();
        $user_data = get_user_data();
        ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>exchange_order/" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="List"><i class="glyphicon glyphicon-list"></i></button></a>
                    <a href="<?php echo $exc_order_pdf; ?>" class="large_icon_des" style=" color: white;margin-right: 80px;" ><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "  title="Download"><i class="glyphicon glyphicon-save"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                        <header>
                            <h2>Exchange Order View</h2>
                        </header>
                        <!-- widget div-->
                        <div role="content">
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body">
                                <!--<span id="watermark">Malar ERP.</span>-->
                                <table id="user" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                        <tr>
                                            <td width="30%" style="font-weight: bold;">Exc-Order No.</td>
                                            <td width="70%" ><?php echo text($exc_order_details->exr_code); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">YOUR INFORMATION</td>
                                            <td ><?php echo text($exc_order_details->exr_your_info); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Payment Status</td>
                                            <td ><?php echo text($exc_order_details->payment_status); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Status</td>
                                            <td ><?php echo ($exc_order_details->received_status == 1) ? 'Received' : 'Not Received'; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="color: #A90329;"><b>Supplier Details</b></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; padding-left: 25px;">Name</td>
                                            <td ><?php echo text($exc_order_details->name); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; padding-left: 25px;">Contact Person</td>
                                            <td ><?php echo text($exc_order_details->contact_name); ?></td>
                                        </tr>

                                        <tr>
                                            <td style="font-weight: bold; padding-left: 25px;">Contact No</td>
                                            <td ><?php echo text($exc_order_details->phone_no); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; padding-left: 25px;">Address</td>
                                            <td ><?php echo nl2br($exc_order_details->address); ?></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="text-left">
                                    <span class="onoffswitch-title">
                                        <h3><b style="color: #8e233b;">Items Details </b></h3>
                                    </span>
                                </div>
                                <?php $cols = 5; ?>
                                <div id="widget-tab-1" class="table-responsive">
                                    <table id="dt_basic_2" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
                                        <thead>
                                            <tr>
                                                <th data-class="expand" style="font-weight: bold; text-align:center;">S.No</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Item Code</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Item Description</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Quantity</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Selling Price</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Cost Price</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Discount</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Total (S$)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $total_item_amt = 0;
                                            $total_discount = 0;
                                            $total_unit_cost = 0;
                                            foreach ($exc_order_item as $key => $item) {
                                                ?>
                                            <?php $total_item_amt += $item->total; ?>
                                            <?php $total_discount += $item->discount; ?>
                                            <?php $total_unit_cost += $item->exr_unit_cost; ?>
                                                <tr>
                                                    <td><?php echo ++$key; ?></td>
                                                    <td><?php echo $item->product_text; ?></td>
                                                    <td><?php echo $item->product_description; ?>
                                                    <?php if (!empty($item->sl_itm_code)) {?>
                                                        <br />
                                                        <b>Supplier Item Code</b>&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;
                                                        <span><?php echo $item->sl_itm_code; ?></span>
                                                    <?php }?>
                                                    <?php if (!empty($item->sl_no)) {?>
                                                        <br />
                                                        <b>Serial No</b>&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;
                                                        <span><?php echo $item->sl_no; ?></span>
                                                    <?php }?>
                                                    </td>
                                                    <td align="center"><?php echo $item->exr_quantity; ?></td>
                                                    <td align="right"><?php echo $item->exr_price; ?></td>
                                                    <td align="right"><?php echo $item->exr_unit_cost; ?></td>
                                                    <td align="right"><?php echo $item->discount; ?></td>
                                                    <td align="right"><?php echo $item->total; ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td align="right" colspan="<?php echo $cols; ?>"><b>Total</b></td>
                                                <td align="right"><?php echo number_format(($total_unit_cost), 2, '.', ''); ?></td>
                                                <td align="right"><?php echo number_format(($total_discount), 2, '.', ''); ?></td>
                                                <td align="right"><?php echo number_format(($total_item_amt), 2, '.', ''); ?></td>
                                            </tr>

                                            <?php if ($exc_order_details->exr_total_discount > 0) { ?>
                                                <tr>
                                                    <td colspan="<?php echo $cols; ?>" align="right"><b>
                                                            Discount (<?php echo ($exc_order_details->exr_discount_type != '1') ? 'S$' : $exc_order_details->exr_discount . ' %'; ?>)</b>
                                                    </td>
                                                    <td align="right" colspan="3">
                                                <?php echo number_format($exc_order_details->exr_total_discount, 2, '.', ''); ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>

                                            <?php if ($exc_order_details->exr_gst_type == '2' && $exc_order_details->gst_type != '2') { ?>
                                                <tr>
                                                    <td colspan="<?php echo $cols; ?>" align="right"><b>Total Amount</b></td>
                                                    <td align="right" colspan="3"><?php echo number_format(($exc_order_details->exr_sub_total - $exc_order_details->exr_total_discount), 2, '.', ''); ?></td>
                                                </tr>

                                                <tr>
                                                    <td colspan="<?php echo $cols; ?>" align="right"><b>Add GST <?php echo $exc_order_details->exr_gst_val; ?>%</b></td>
                                                    <td align="right" colspan="3"><?php echo number_format($exc_order_details->exr_gst_amount, 2, '.', ''); ?></td>
                                                </tr>
                                            <?php } ?>

                                            <tr>
                                                <td colspan="<?php echo $cols; ?>" align="right"><b>Net Amount</b></td>
                                                <td align="right" colspan="3"><?php echo $exc_order_details->exr_net_amount; ?></td>
                                            </tr>
                                        
                                            <?php if ($exc_order_details->exr_gst_type == '1' && $exc_order_details->gst_type != '2') { ?>
                                                <tr>
                                                    <td colspan="<?php echo $cols + 3; ?>"><b>[INCLUSIVE OF GST <?php echo $exc_order_details->exr_gst_val; ?> % : $ <?php echo number_format($exc_order_details->exr_gst_amount, 2, '.', ''); ?>]%</b></td>
                                                </tr>
                                            <?php } ?>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
