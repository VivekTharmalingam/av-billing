<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->
        <?php
        echo breadcrumb_links();
        $user_data = get_user_data();
        ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) {
                    ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>

        <form class="add_form" action="<?php echo $form_action; ?>" method="post" name="exchange_order" id="exchange_order_form">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                        <a href="<?php echo base_url(); ?>exchange_order/" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="List"><i class="glyphicon glyphicon-list"></i></button></a>
                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Exchange Order Add </h2>
                            </header>
                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" >
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                            <input type="hidden" class="form-control exr_id" name="exr_id" value="" />
                                            <input type="hidden" class="form-control supplier_id" name="supplier_id" value="" />
                                            <input type="hidden" class="form-control supplier_gst" name="supplier_gst" value="" />

                                            <div class="row">
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Supplier <span style="color: red;">*</span></label>
                                                        <label class="input"><i class="icon-append fa fa-user"></i>
                                                            <input type="text" class="supplier_name" name="supplier_name" id="supplier_name" maxlength="150">
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Date <span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="exr_date" id="exr_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php echo date('d/m/Y'); ?>" />
                                                        </label>
                                                    </section>
                                                </section> 
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Your Information<span style="color: red;"></span></label>
                                                        <label class="textarea">
                                                            <textarea rows="5" class="custom-scroll your_info" data-placeholder="Your Information" name="your_info"></textarea>
                                                        </label>
                                                    </section>
                                                </section> 
                                            </div>
                                        </fieldset>

                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Items Details</legend>
                                            
                                            <div class="row material-container product_details">
                                                <div class="items_head col-xs-12 clr">
                                                    <section class="col col-2">
                                                        <label class="label">
                                                            <b>Item Code</b>
                                                        </label>
                                                    </section>
                                                    <section class="col col-4">
                                                        <label class="label"><b>Brand Name - Description</b></label>
                                                    </section>
                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="label"><b>Qty</b></label>
                                                    </section>
                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="label"><b>Cost Price</b></label>
                                                    </section>
                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="label"><b>Selling Price</b></label>
                                                    </section>
                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="label"><b>Discount</b></label>
                                                    </section>
                                                    <section class="col col-2" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="label"><b>Amount</b></label>
                                                    </section>
                                                </div>

                                                <div class="items col-xs-12 clr">
                                                    <section class="col col-2">
                                                        <label class="input">
                                                            <input type="text" name="product_code[0]" class="product_code" data-value="" />
                                                        </label>
                                                        <span class="product_code_label">Selected Item code already exist!</span>
                                                    </section>

                                                    <section class="col col-4">
                                                        <label class="input">
                                                            <input type="text" name="product_description[0]" class="product_description" data-value="" />
                                                        </label>
                                                        <span class="error_description">Selected Item description already exist!</span>
                                                        <span class="item-serialnum-popup btn btn-primary" style="padding:3px 4px 3px 4px;margin-top: 2px;" title="Quantity should not be empty to enter Serial No.">
                                                            <i class="fa fa-plus-square-o" style="font-size: 14px;"> </i> Serial No
                                                        </span>
                                                        <input type="hidden" class="item_sl_no" name="sl_no[0]" />
                                                        <span style="float: right; margin-top: 1px;">
                                                            <b style="color: #333;font-size: 11px; display: inline-block;">Supplier<br>Item Code</b>
                                                            <label class="input" style="float: right; width: 70%;">
                                                                <input type="text" class="sl_itm_code" name="sl_itm_code[0]" />
                                                            </label>
                                                        </span>
                                                    </section>

                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="input">
                                                            <input type="text" name="quantity[0]" class="quantity float_value" />
                                                        </label>
                                                        <span class="unit_label"></span>
                                                    </section>

                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="input" style="margin-top: 1px;">
                                                            <!--<i class="icon-prepend fa fa-dollar"></i>-->
                                                            <input type="text" name="cost[0]" class="cost float_value" />
                                                        </label>
                                                    </section>

                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="input">
                                                            <!--<i class="icon-prepend fa fa-dollar"></i>-->
                                                            <input type="text" name="price[0]" class="price float_value" />
                                                        </label>
                                                    </section>
                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <div class="input-group">
                                                            <input type="text" name="item_discount[0]" class="form-control item_discount float_value" style="height: 30px; padding-right: 5px;" />
                                                            <input type="hidden" name="item_discount_amt[0]" class="item_discount_amt" />
                                                            <span>S$ <span class="item_discount_amt">0</span></span>
                                                            <span class="onoffswitch" style="float: right;">
                                                                <input type="checkbox" name="item_discount_type[0]" class="onoffswitch-checkbox item_discount_type" id="item_discount_type-0" value="1" checked />
                                                                <label class="onoffswitch-label" for="item_discount_type-0"> 
                                                                    <span class="onoffswitch-inner" data-swchon-text="%" data-swchoff-text="S$"></span> 
                                                                    <span class="onoffswitch-switch"></span> 
                                                                </label> 
                                                            </span>
                                                        </div>
                                                    </section>

                                                    <section class="col col-2" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="input" style="width: 35%; text-align: center; float: right; margin-top: 2px;">
                                                            <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                        </label>
                                                        <label class="input" style="width: 65%; float: left;">
                                                            <i class="icon-prepend fa fa-dollar"></i>
                                                            <input type="text" name="amount[0]" class="amount float_value" readonly="" />
                                                        </label>
                                                    </section>
                                                </div>
                                            </div>

                                            <div class="row" style="margin-top: 10px;">
                                                <section class="col col-3">
                                                </section>
                                                <section class="col col-3">
                                                </section>
                                                <section class="col col-3">
                                                    <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Sub Total</b></label>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="sub_total" class="sub_total" readonly="" />
                                                    </label>
                                                </section>
                                                <section class="col col-1">
                                                </section>
                                            </div>

                                            <div class="row">
                                                <section class="col col-6">
                                                        <!--<section class="row credit_amount_container">
                                                                <section class="col col-3">
                                                                        <label class="label">Credit Amount</label>
                                                                </section>
                                                                <section class="col col-5">
                                                                        <label class="input">
                                                                                <i class="icon-prepend fa fa-dollar"></i>
                                                                                <input type="text" name="credit_amount" id="credit_amount" value="" />
                                                                        </label>
                                                                </section>
                                                        </section>-->
                                                </section>

                                                <section class="col col-3">
                                                    <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Discount</b></label>
                                                </section>
                                                <section class="col col-2">
                                                    <!--<label class="input">
                                                        <i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="sub_total" class="sub_total" readonly="" data-placeholder="0.00" />
                                                    </label>-->

                                                    <div class="input-group">
                                                        <input type="text" name="discount" class="form-control discount float_value" value="0.00" />
                                                        <span class="input-group-addon">
                                                            <span class="onoffswitch">
                                                                <input type="checkbox" name="discount_type" class="onoffswitch-checkbox discount_type" id="discount_type-0" value="1" checked />
                                                                <label class="onoffswitch-label" for="discount_type-0"> 
                                                                    <span class="onoffswitch-inner" data-swchon-text="%" data-swchoff-text="S$"></span> 
                                                                    <span class="onoffswitch-switch"></span> 
                                                                </label> 
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <input type="hidden" name="discount_amt" class="discount_amt" />
                                                    <span class="discount_container">
                                                        <span class="currency">S$</span>
                                                        <span class="discount_amount">0.00</span>
                                                    </span>
                                                </section>
                                                <section class="col col-1">
                                                </section>
                                            </div>

                                            <div class="row gst_details">
                                                <section class="col col-6">&nbsp;</section>
                                                <section class="col col-3">
                                                    <section>
                                                        <div class="inline-group">
                                                            <label class="radio">
                                                                <input type="radio" class="gst_type without_gst" name="gst_type" value="1" <?php echo (empty($company->gst_percent)) ? 'checked' : ''; ?> /><i></i>Include GST
                                                            </label>
                                                            <label class="radio">
                                                                <input type="radio" class="gst_type with_gst" name="gst_type" value="2" <?php echo (!empty($company->gst_percent)) ? 'checked' : ''; ?> /><i></i>Exclude GST
                                                            </label>
                                                        </div>
                                                    </section>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="input change_perc_disamt"><i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="gst_amount" class="gst_amount float_value" readonly="" />
                                                        <input type="hidden" value="<?php echo $company->gst_percent; ?>" name="hidden_gst" id="hidden_gst" />
                                                    </label>
                                                    <span class="gst_val" style="display: <?php echo (!empty($company->gst_percent)) ? 'block' : 'none'; ?>">GST @ <span class="gst_percentage"><?php echo $company->gst_percent; ?></span> %</span>
                                                </section>
                                            </div>

                                            <div class="row">
                                                <section class="col col-6">
                                                </section>
                                                <section class="col col-3">
                                                    <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Total Amount</b></label>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="total_amt" class="total_amt" readonly="" />
                                                    </label>
                                                </section>
                                                <section class="col col-1">
                                                </section>
                                            </div>                                           

                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn btn-primary" >Submit</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                    <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/exchange_order.js"></script>