    <?php $height = 400; ?>
    <table width="100%" border="1" style="border-collapse:collapse; font-family: calibri;" cellpadding="5">
        <thead style="font-size: 13px;">
            <tr>
                <th width="15%" style="background:#EBEBEB;">Item Code</th>
                <th width="30%" style="background:#EBEBEB;">Description</th>                   
                <th width="15%" style="background:#EBEBEB;">Quantity</th>                    
                <th width="10%" style="background:#EBEBEB;">Cost Price</th>
                <th width="15%" style="background:#EBEBEB;">Discount</th>
                <th width="15%" style="background:#EBEBEB;">Amount</th>
            </tr>
        </thead>
		<tfoot style="display: table-footer-group">
			<tr>
				<td colspan="6" style="border-left: none;border-right: none;border-bottom: none;font-size: 15px; text-align: center;"></td>
			</tr>
		</tfoot>
        <tbody style="font-size: 11px;">
            <?php foreach ($exc_order_item as $key => $item) { ?>
                <tr style="vertical-align:top;">
                    <td style="border-top: 0; border-bottom: none;"><?php echo strtoupper($item->product_text); ?></td>
                    <td style="border-top: 0;border-bottom: none;"><?php echo nl2br(strtoupper($item->product_description)); ?>
                        <?php if(!empty($item->sl_itm_code)) { ?><br /><b>Supplier Item Code</b>&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;
                        <span><?php echo $item->sl_itm_code; ?></span> <?php } ?>
                        <?php if(!empty($item->sl_no)) { ?><br /><b>Serial No</b>&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;
                        <span><?php echo $item->sl_no; ?></span> <?php } ?>
                    </td>
                    <td align="center" style="border-top: 0;border-bottom: none;"><?php echo $item->exr_quantity; ?> </td>
                    <td align="right" style="border-top: 0;border-bottom: none;"><?php echo $item->exr_unit_cost; ?></td>
                    <td align="right" style="border-top: 0;border-bottom: none;"><?php echo $item->discount; ?></td>
                    <td align="right" style="border-top: 0;border-bottom: none;"><?php echo $item->total; ?></td>
                </tr>
                <?php $height -= 55; ?>
            <?php } ?>
            <?php $height -= ($exc_order->exr_discount > 0) ? 30 : 0; ?>
            <?php $height -= ($exc_order->exr_gst_amount > 0) ? 30 : 0; ?>
            <tr>
                <td height="<?php echo $height; ?>" style="border-top: 0;"></td>
                <td style="border-top: 0;"></td>
                <td style="border-top: 0;"></td>
                <td style="border-top: 0;"></td>
                <td style="border-top: 0;"></td>
                <td style="border-top: 0;"></td>
            </tr>
            <!--</table>-->

            <?php
            //$row_span = ($exc_order->gst_amount > 0) ? 3 : 2;
            $row_span = 2;
            if ($exc_order->exr_gst_amount > 0 && $exc_order->exr_discount > 0)
                $row_span = 4;
            else if ($exc_order->exr_gst_amount > 0 || $exc_order->exr_discount > 0)
                $row_span = 3;
            ?>
    <!--<table width="100%" style="border-collapse:collapse; border-top:none;" cellpadding="5">-->
            <tr>
                <td align="left" style="vertical-align: top; border-bottom: none; border-right: none; padding-right: 10px;" colspan="4">
                    <?php echo (!empty($exc_order->exr_your_info)) ? '<p style="font-size: 12px;"><b style="font-size: 12px;">Remarks</b> <br />' . strtoupper($exc_order->exr_your_info) . '</p>' : ''; ?>
                </td>
                <td colspan="2" style="vertical-align: top; padding: 0; margin: 0; border: 0;">
                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                        <?php if (($exc_order->exr_discount > 0) || ($exc_order->exr_gst_amount > 0)) { ?>
                        <tr>
                            <td style="height: 30px; width: 109px; border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; padding-left: 10px;">Sub Total</td>
                            <td align="right" style="width: 108px;;font-weight:normal; border-right: 1px solid #000; border-bottom: 1px solid #000; padding-right: 5px;"><?php echo $exc_order->exr_sub_total; ?></td>
                        </tr>
                        <?php } ?>
                        
                        <?php if ($exc_order->exr_discount > 0) { ?>
                            <tr>
                                <td style="height: 30px; border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; padding-left: 10px;">Discount  <?php echo ($exc_order->exr_discount_type == 1 && $exc_order->exr_discount > 0) ? '' . $exc_order->exr_discount . '%' . '' : ''; ?></td>
                                <td align="right" style="font-weight:normal; border-bottom: 1px solid #000; border-right: 1px solid #000; padding-right: 5px;"><?php echo $exc_order->exr_total_discount; ?></td>
                            </tr>
                        <?php } ?>

                        <?php if ($exc_order->exr_gst_amount > 0) { ?>
                            <tr>
                                <td style="height: 30px; border-left: 1px solid #000;border-bottom: 1px solid #000; border-right: 1px solid #000; padding-left: 10px;"><?= ($exc_order->exr_gst_type == '2') ? "ADD" : ""; ?> GST <?php echo $exc_order->exr_gst_val; ?> %</td>
                                <td align="right" style="font-weight:normal; border-right: 1px solid #000;border-bottom: 1px solid #000; padding-right: 5px;"><?php echo $exc_order->exr_gst_amount; ?></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td style="height: 30px; width: 109px; border-left: 1px solid #000;border-bottom: 1px solid #000; border-right: 1px solid #000; padding-left: 10px;">Total</td>
                            <td align="right" style="width: 108px;font-weight:normal; border-right: 1px solid #000; border-bottom: 1px solid #000; padding-right: 5px;"><?php echo $exc_order->exr_net_amount; ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
    </table>