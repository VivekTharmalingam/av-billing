<script>
    window.localStorage.clear();
</script>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4"></div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8"></div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="widget-body">
                        <div id="myTabContent1" class="tab-content padding-10">
                            <a href="<?php echo $add_link; ?>" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-pencil"></i></button></a>
                            <div class="tab-pane fade in active" id="s1">
                                <!-- Widget ID (each widget will need unique ID)-->                                
                                <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false"	>
                                    <header><span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                        <h2>Inventory Adjustment</h2></header>
                                    <div>
                                        <div class="jarviswidget-editbox">
                                            <!-- This area used as dropdown edit box -->
                                        </div>
                                        <div class="widget-body no-padding">
                                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                                <thead>			                
                                                    <tr>
                                                        <th style="width: 5%;" data-hide="phone" data-class="expand">S.No</th>
                                                        <th style="width: 10%;" data-hide="phone"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Date</th>
                                                        <th style="width: 10%;"><i class="fa fa-fw fa-key"></i> Item Code</th>
                                                        <th style="width: 15%;" ><i class="fa fa-fw fa-key text-muted hidden-md hidden-sm hidden-xs"></i> Bar Code</th>
                                                        <th style="width: 15%;" ><i class="fa fa-fw fa-key text-muted hidden-md hidden-sm hidden-xs"></i> Srl No.</th>
                                                        <th style="width: 15%;" data-hide="phone"><i class="fa fa-fw fa-sitemap" ></i>Brand Name</th>
                                                        <th style="width: 15%;" data-hide="phone">Item Description</th>
                                                        <th style="width: 10%;" >Quantity</th>
                                                        <th style="width: 15%;" data-hide="phone">Adjust Type</th>
                                                        <th style="width: 15%;" data-hide="phone,tablet">Remarks</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <!-- end widget content -->

                                    </div>
                                    <!-- end widget div -->

                                </div>
                                <!-- end widget -->
                            </div>
                        </div> 
                    </div>
                </article>
                <!-- /.row -->
            </div>
        </section>
    </div>
</div>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/your_style.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/datatables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/inventory.js"></script>
