<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) {
                    ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>

        <form class="add_form" action="<?php echo $form_action; ?>" method="post" id="inv_form">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                        <a href="<?php echo $list_link; ?>" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Adjust Stock Add </h2>
                            </header>
							
                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" >
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                            <div class="row">
                                                <section class="col col-6">
                                                <section>
                                                        <label class="label">Scan Bar Code / Enter Item Code/Description</label>
                                                        <label class="input">
                                                                <input type="text" name="search_item" value="" class="form-control search_item lowercase" />
                                                        </label>
                                                </section>
                                                    <section>
                                                        <label class="label">Branch Name<span style="color: red;">*</span></label>
                                                        <label class="select">
                                                            <select name="branch_name" class="select2 branch_name">
                                                                <?php foreach ($branches as $key => $brn) { ?>
                                                                    <option value="<?php echo $brn->id;?>" <?php echo ($brn->id == $branch_id) ? 'selected' : '';?>><?php echo $brn->branch_name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Item Code / Description<span style="color: red;">*</span></label>
                                                        <label class="select">
                                                            <select name="product_id" class="select2" id="product_id">
                                                                <option value="">Select Item</option>
                                                            </select>
                                                        </label>
                                                    </section>
													<section>
														<label class="label">Remarks</label>
														<label class="input textarea-expandable">
															<textarea class="form-control show_txt_count" name="remarks" maxlength="500" data-placeholder="Remarks" rows="2" ></textarea>
														</label>
													</section>
                                                </section>
                                                <section class="col col-6">
													<section>
                                                        <label class="label">Adjustment Type</label>
                                                        <div class="inline-group">
                                                            <label class="radio">
                                                                <input type="radio" class="adjust_type" name="adjust_type" value="1" Checked/><i></i>Increament
                                                            </label>
                                                            <label class="radio">
                                                                <input type="radio" class="adjust_type" name="adjust_type" value="2" /><i></i>Decrement
                                                            </label>
                                                        </div>
                                                    </section>
                                                    
                                                    <section>
                                                        <label class="label">Adjust Quantity<span style="color: red;">*</span>
															<span style="position: absolute; right: 15px;">Current Stcok : <b style="color: #3276B1; text-align: right; min-width: 25px; display: inline-block;" class="current-stock">0</b></span>
														</label>
                                                        <label class="input">
                                                            <input type="text" name="quantity" class="form-control float_value" value="" maxlength="15" />
                                                        </label>
                                                    </section>
                                                </section>
                                            </div>
                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary" >Submit</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                    <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/inventory.js"></script>