<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo base_url(); ?>services/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                            <h2>Service Status Log</h2>
                            </span>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="" action="<?php echo $form_action; ?>" method="post" id="log_form" name="log_form" enctype="multipart/form-data">
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    
                                    <div class="smart-form">  
                                            <fieldset>
                                                <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Log Details</legend>
                                                <div class="col-md-12">
                                                <div class="col-md-6" style="max-height: 500px;overflow-y: auto;overflow-x: hidden;">
                                                    <div class="col-md-12" style="border-radius: 5px; background-color: #FFF; margin-bottom: 5px;">
                                                            <div style="padding-top: 10px;padding-bottom: 5px; padding-left: 10px; margin-left: 0px;color: #06ad9c;" class="col-md-12"><b>Status : </b>&nbsp;Service Creation</div>
                                                            <div style="padding-top: 10px;padding-bottom: 10px; padding-left: 10px; margin-left: 0px;" class="col-md-12"><b>Log Description : </b>&nbsp;Service was created on <?php echo date('d/m/Y',strtotime($service->created_on)); ?></div>
                                                            <div class="col-md-12" style="margin-left: 0px; padding-bottom: 6px; padding-left: 10px; background-color: rgba(204, 204, 204, 0.2);">
                                                                <div class="col-md-6"><b style="font-style: italic">Created On :</b>&nbsp;&nbsp;<span style="font-style: italic"><?php echo date('d-m-Y h:i:s A',strtotime($service->created_on)); ?></span></div>
                                                            </div>
                                                    </div>
                                                    <?php foreach ($service_status_log as $key => $row) { ?>
                                                        <div class="col-md-12" style="border-radius: 5px; background-color: #FFF; margin-bottom: 5px;">
                                                            <div style="padding-top: 10px;padding-bottom: 5px; padding-left: 10px; margin-left: 0px;color: #06ad9c;" class="col-md-12"><b>Status : </b>&nbsp;<?php echo ($row->status_name != '') ? $row->status_name : 'Estimated delivery date changed'; ?></div>
                                                            <div style="padding-top: 10px;padding-bottom: 10px; padding-left: 10px; margin-left: 0px;" class="col-md-12"><b>Log Description : </b>&nbsp;<?php echo text($row->log_description); ?></div>
                                                            <div class="col-md-12" style="margin-left: 0px; padding-bottom: 6px; padding-left: 10px; background-color: rgba(204, 204, 204, 0.2);">
                                                                <div class="col-md-6"><b style="font-style: italic">Created On :</b>&nbsp;&nbsp;<span style="font-style: italic"><?php echo $row->created_date_str; ?></span></div>
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                                </div>
                                                <div class="col-md-6">
                                                    <section class="col-md-12" style="padding-left: 6px;padding-top: 2px;">
                                                        <section class="alert alert-info" style="border-top-width: 5px!important; border-left-width:0px!important;border-bottom: 0px; background: rgb(249, 236, 205);border-color: #8b1a0d;">
                                                            <label class="label" style="font-size: 11px;font-style: italic;"><b style="border-bottom: 1px solid;">Service Details :-</b></label>
                                                            <label class="label" style="font-size: 11px;">Job Sheet Number: <span><?php echo $service->jobsheet_number; ?> </span></label>
                                                            <label class="label" style="font-size: 11px;">Engineer Name: <span><?= $service->engineer_name; ?></span></label>
                                                            <label class="label" style="font-size: 11px;">Received Date: <span><?= $service->received_date_str; ?></span></label>
                                                            <label class="label" style="font-size: 11px;">Estimated Delivery Date: <span><?= $service->estimated_delivery_date_str; ?></span></label>
                                                            <label class="label" style="font-size: 11px;">Company Name: <span><?= $service->customer_name; ?></span></label>
                                                            <label class="label" style="font-size: 11px;">Contact Person: <span><?= $service->contact_person; ?></span></label>
                                                            <label class="label" style="font-size: 11px;">Contact No: <span><?= $service->contact_number; ?></span></label>
                                                            <label class="label" style="font-size: 11px;">Email Address: <span><?= $service->email_address; ?></span></label>
                                                        </section>
                                                    </section>
                                                </div>
                                                </div>
                                            </fieldset>
                                            
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Service Status<span style="color: red">*</span></label>
                                                    <label class="select">
                                                        <select name="service_status" id="service_status" class="select2 service_status" >
                                                            <option value="" data-stdescription="">Select Service Status</option>
                                                            <?php foreach ($status_list as $exkey => $status) { ?>
                                                            <option value="<?php echo $status->id; ?>" data-stdescription="<?php echo $status->status_desc; ?>"><?php echo $status->status_name; ?></option>
                                                            <?php } ?>   
                                                        </select>  
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Log Description <span style="color: red">*</span></label>
                                                    <label class="textarea"><textarea rows="3" name="log_description" id="log_description" maxlength="500"></textarea> </label> 
                                                </section>
                                            </div>
                                           
                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript">
$(document).ready(function () {
    
    $(document).on('change', 'select[name="service_status"]', function () {
        $('#log_description').val($(this).find('option:selected').data('stdescription'));
        $('#log_form').validate().element(this);
    });
    
    var $orderForm = $("#log_form").validate({
        // Rules for form validation
        rules : {
                service_status : {
                        required : true
                }
        },

        // Messages for form validation
        messages : {
                service_status: {
                        required : 'Please select Service Status'
                }
        },
        // Do not change code below
        errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
        }
    });

});
</script>
