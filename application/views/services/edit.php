<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->
        <?php
        echo breadcrumb_links();
        ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) {
                    ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>

        <form class="edit_form" action="<?php echo $form_action; ?>" method="post" name="services" id="services_form" enctype="multipart/form-data">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                        <a href="<?php echo base_url(); ?>services/" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="List"><i class="glyphicon glyphicon-list"></i></button></a>
                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">

                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Services Edit </h2>
                            </header>

                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" >
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                            <input type="hidden" class="form-control serv_id" name="serv_id" value="<?php echo $service->id; ?>" />   
                                            <div class="row">
                                                <section class="col col-6">
                                                    <section >
                                                        <label class="label">Job Sheet Number <span style="color: red;">*</span></label>
                                                        <label class="input"><i class="icon-append fa fa-key"></i>
                                                            <input type="text" name="jobsheet_number" id="jobsheet_number" value="<?= $service->jobsheet_number; ?>" readonly=""/>
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Company Name <span style="color: red;">*</span></label>
                                                        <label class="select">
                                                            <select name="customer_id" class="select2 customer_id">
                                                                <option value="">Select Company</option>
                                                                <?php foreach ($customer_list as $key => $value) { ?>
                                                                    <option value="<?php echo $value['id']; ?>" <?= ($value['id']==$service->customer_id) ? "selected" : ""; ?>><?php echo $value['name']; ?></option>
                                                                <?php } ?>
                                                                <option value="0" <?= ($service->customer_id=="0") ? "selected" : ""; ?>> Others </option>
                                                            </select>
                                                        </label>
                                                    </section>
                                                    <section class="customer_name_div">
                                                        <label class="label">Customer Name <span style="color: red;">*</span></label>
                                                        <label class="input"><i class="icon-append fa fa-user"></i>
                                                            <input type="text" class="customer_name" name="customer_name" id="customer_name" value="<?= $service->customer_name; ?>"/>
                                                            <input type="hidden" class="customer_name_txt" name="customer_name_txt" id="customer_name_txt" value="<?= $service->customer_name; ?>"/>
                                                        </label>
                                                    </section>
                                                    <section >
                                                        <label class="label">Contact Person </label>
                                                        <label class="input"><i class="icon-append fa fa-user-md"></i>
                                                            <input type="text" class="contact_person" name="contact_person" id="contact_person" value="<?= $service->contact_person; ?>"/>
                                                        </label>
                                                    </section>
                                                    <section >
                                                        <label class="label">Contact Number </label>
                                                        <label class="input"><i class="icon-append fa fa-phone"></i>
                                                            <input type="text" class="contact_number" name="contact_number" id="contact_number" value="<?= $service->contact_number; ?>"/>
                                                        </label>
                                                    </section>
                                                    <section >
                                                        <label class="label">Email Address </label>
                                                        <label class="input"><i class="icon-append fa fa-envelope-o"></i>
                                                            <input type="text" class="email_address lowercase" name="email_address" id="email_address" value="<?= $service->email_address; ?>"/>
                                                        </label>
                                                    </section>
                                                </section> 
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Engineer Name <span style="color: red;">*</span></label>
                                                        <label class="select">
                                                            <select name="engineer_id" class="select2 engineer_id">
                                                                <option value="">Select Engineer</option>
                                                                <?php foreach ($user_list as $key => $value) { ?>
                                                                    <option value="<?php echo $value['id']; ?>" <?= ($value['id']==$service->engineer_id) ? "selected" : ""; ?>><?php echo $value['name']; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Guessing Problem </label>
                                                        <label class="textarea">
                                                            <textarea rows="3" class="custom-scroll guessing_problem" name="guessing_problem"><?= $service->guessing_problem; ?></textarea>
                                                        </label>
                                                    </section>
                                                    <section >
                                                        <label class="label">Estimated Price Quoted <span style="color: red;">*</span></label>
                                                        <label class="input"><i class="icon-append fa fa-money"></i>
                                                            <input type="text" class="float_value" name="estimated_price_quoted" id="estimated_price_quoted" value="<?= $service->estimated_price_quoted; ?>"/>
                                                        </label>
                                                    </section>
                                                    <input type="hidden" name="estimated_delivery_date_temp" id="estimated_delivery_date_temp" class="estimated_delivery_date_temp" value="<?= $service->estimated_delivery_date_str; ?>" />
                                                    <input type="hidden" name="estimated_date_change" id="estimated_date_change" class="estimated_date_change" value="no" />
                                                    <section>
                                                        <label class="label">Estimated Delivery Date <span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="estimated_delivery_date" id="estimated_delivery_date" class="form-control bootstrap-datepicker-comncls" placeholder="DD/MM/YYYY" value="<?= $service->estimated_delivery_date_str; ?>" />
                                                        </label>
                                                    </section>
                                                    <section class="send_sms_cls">
                                                        <div class="inline-group" style="margin-bottom: 5px;">
                                                            <p style="float: left; font-weight: bold; margin-right: 10px;">Send SMS To Customer </p>
                                                            <label class="radio">
                                                                <input type="radio" name="sms_type" class="sms_type" value="yes">
                                                                    <i></i>Yes
                                                            </label>
                                                            <label class="radio">
                                                                <input type="radio" name="sms_type" class="sms_type" value="no">
                                                                    <i></i>No
                                                            </label>
                                                        </div>
                                                    </section>
                                                </section> 
                                            </div>
                                            </fieldset>
                                        
                                            <fieldset>
                                                <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Customer Instructions</legend>
                                                <div class="row">
                                                    <section class="col col-6">
                                                        <section>
                                                            <label class="label" style="height: 25px;"><span style="float: left;">Brand Name</span>
                                                                <span class="new-window-popup btn btn-primary" style="float: right;padding:3px 4px 3px 4px;margin-top: 2px;" data-iframe-src="<?php echo $add_category_link; ?>" data-cmd="brand" data-title="Brand Add">
                                                                    <i class="fa fa-plus-square-o" style="font-size: 16px;"> </i> Add Brand
                                                                </span>
                                                            </label>
                                                            <label class="select">
                                                                <select name="cat_name" class="cat_name select2" id="cat_name">  
                                                                    <option value="">Select Brand</option>
                                                                    <?php foreach ($category as $key => $val) { ?>
                                                                        <option value="<?php echo $val->id; ?>" <?php echo ($val->id == $service->cat_id) ? 'selected' : ''; ?> ><?php echo $val->cat_name; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </label>
                                                        </section>
                                                        <section >
                                                            <label class="label" style="height: 25px;"><span style="float: left;">Product Name </span>
                                                            <div class="inline-group" style="float: right;">
                                                                    <label class="radio">
                                                                        <input type="radio" class="pro_text" name="pro_text" checked="" value="Laptop">
                                                                             <i></i>Laptop
                                                                     </label>
                                                                     <label class="radio">
                                                                         <input type="radio" class="pro_text" name="pro_text" value="Desktop">
                                                                             <i></i>Desktop
                                                                     </label>
                                                                     <label class="radio">
                                                                         <input type="radio" class="pro_text others_cls" name="pro_text" value="">
                                                                             <i></i>Others
                                                                     </label>
                                                             </div>
                                                            </label>
                                                            <label class="input"><i class="icon-append fa fa-times-circle remove_pro_txt" style="color: red;font-size: 18px;cursor: pointer;"></i>
                                                                <input type="text" name="material_name" id="material_name" value="<?= $service->material_name; ?>"/>
                                                            </label>
                                                        </section>
                                                        <section >
                                                            <label class="label">Product Type / Model no </label>
                                                            <label class="input"><i class="icon-append fa fa-cubes"></i>
                                                                <input type="text" name="material_type" id="material_type" value="<?= $service->material_type; ?>"/>
                                                            </label>
                                                        </section>
                                                        
                                                    </section>
                                                    <section class="col col-6">
                                                        <section>
                                                            <label class="label">Problem Description<span style="color: red;"></span></label>
                                                            <label class="textarea">
                                                                <textarea rows="6" class="custom-scroll problem_desc" name="problem_desc"><?= $service->problem_desc; ?></textarea>
                                                            </label>
                                                        </section>
                                                        <section>
                                                            <label class="label">Received Date <span style="color: red;">*</span></label>
                                                            <label class="input">
                                                                <i class="icon-append fa fa-calendar"></i>
                                                                <input type="text" name="received_date" id="received_date" class="form-control bootstrap-datepicker-comncls" placeholder="DD/MM/YYYY" value="<?= $service->received_date_str; ?>" />
                                                            </label>
                                                        </section>
                                                    </section>
                                            </fieldset> 
                                            
                                            <fieldset>
                                                <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Parts Details</legend>

                                                <div class="row material-container product_details">
                                                    <div class="items_head col-xs-12 clr" style="margin-bottom: 10px;">
                                                       <?php for($k=0;$k<count($category_list);$k++) {
                                                        $items  = $this->services_model->service_items_byid($category_list[$k]['id'], $service->id);     
                                                        $cat_other_stl = ($category_list[$k]['is_other']==1) ? "display: inline;float: left;" : "";
                                                        ?>
                                                        <div class="row" style="margin-bottom: 5px;">
                                                        <section class="col col-2">
                                                            <div class="row per-blck" style="padding: 3px;margin-top: 10px;">
                                                                <div>
                                                                    <label class="checkbox state-error" style="font-weight: bold;"><?= $category_list[$k]['cat_name'];?> <span style="float: right;">:</span></label>
                                                                    <input type="hidden" name="cat_other[<?php echo $k; ?>]" value="<?= $category_list[$k]['is_other'];?>"/>
                                                                    <input type="hidden" name="cat_ids[<?php echo $k; ?>]" value="<?= $category_list[$k]['id'];?>"/>
                                                                </div>
                                                            </div>
                                                        </section>
                                                        <section class="col col-10">
                                                            <div class="row per-blck" style="margin-right: 0; padding: 3px;">
                                                                <?php if(count($items)>0) { for($t=0;$t<count($items);$t++) { ?>
                                                                <input type="hidden" name="item_other[<?php echo $k; ?>][<?= $items[$t]['id'];?>]" value="<?= $items[$t]['is_other'];?>"/>
                                                                <?php if($items[$t]['is_other']==1) { ?>
                                                                <div class="col others_top" style=" min-width: 120px; margin-top: 10px;height: 35px;">
                                                                    <label class="checkbox state-error" style="display: inline;float: left;"><input name="ser_item[<?php echo $k; ?>][]" class="permission acts lvl lvl-3 others_chk_bok" <?= ($items[$t]['id']==$items[$t]['item_id']) ? "checked" : ""; ?> value="<?= $items[$t]['id']; ?>" type="checkbox"><i></i><?= $items[$t]['item_name']; ?></label>
                                                                    <label class="input item_others_txt"  style="display: inline;float: right; margin-left: 5px;">
                                                                        <input type="text" name="item_others_txt[<?php echo $k; ?>][<?= $items[$t]['id'];?>]" id="item_others_txt" value="<?= $items[$t]['others_txt']; ?>"/>
                                                                    </label>
                                                                </div>
                                                                <?php } else { ?>
                                                                <div class="col" style=" min-width: 120px;margin-top: 10px;height: 35px;">
                                                                    <label class="checkbox state-error"><input name="ser_item[<?php echo $k; ?>][]" class="permission acts lvl lvl-3" value="<?= $items[$t]['id']; ?>" <?= ($items[$t]['id']==$items[$t]['item_id']) ? "checked" : ""; ?> type="checkbox"><i></i><?= $items[$t]['item_name']; ?></label>
                                                                </div>
                                                                <?php } ?>
                                                                <?php } ?>
                                                                <?php } else if($category_list[$k]['is_other']==1) {  ?>
                                                                <?php $other_cat  = $this->services_model->service_other_cat($category_list[$k]['id'], $service->id); ?>
                                                                 <div class="col col-4" style=" min-width: 120px; margin-top: 10px;">
                                                                    <label class="input others_inputboxs">
                                                                        <input type="text" name="cat_others_txt[<?php echo $k; ?>]" id="cat_others_txt" value="<?= (count($other_cat)>0) ? $other_cat[0]['others_txt'] : ""; ?>"/>
                                                                    </label>
                                                                </div>       
                                                                <?php } ?>
                                                            </div>
                                                        </section>
                                                        </div>
                                                       <?php } ?>

                                                    </div>
                                                </div>



                                                <div class="row">&nbsp;</div>

                                                <div class="row">
                                                    <section class="col col-6">
                                                            <section >
                                                                <label class="label">Physical Condition </label>
                                                                <label class="input"><i class="icon-append fa fa-puzzle-piece"></i>
                                                                    <input type="text" name="physical_condition" id="physical_condition" value="<?= $service->physical_condition; ?>"/>
                                                                </label>
                                                            </section>
                                                            <section >
                                                                <label class="label">Photo Upload </label>
                                                                <?php $img_url = ($service->upload_photo != "") ? BASE_URL .'uploads/'.$service->upload_photo : BASE_URL . NO_IMG; ?>
                                                                <label class="input">
                                                                    <div class="parallel thumb preview" style="width: 9%;">
                                                                        <img src="<?php echo $img_url; ?>" />
                                                                    </div>
                                                                    <input type="file" class="form-control btn btn-default parallel" name="upload_photo" value="" data-default="<?php echo $img_url; ?>" style="width: 90%; height: 45px;"/>
                                                                </label>
                                                            </section>
                                                            <section>
                                                                <label class="label">Stage </label>
                                                                <label class="select">
                                                                    <select name="service_status" id="service_status" class="select2 service_status" >
                                                                        <option value="1" <?php echo ($service->service_status == 1) ? 'selected' : ''; ?>>Processing</option>
                                                                        <option value="2" <?php echo ($service->service_status == 2) ? 'selected' : ''; ?>>Completed</option>
                                                                        <option value="3" <?php echo ($service->service_status == 3) ? 'selected' : ''; ?>>Delivered</option>
                                                                    </select>  
                                                                </label>
                                                            </section> 
                                                    </section>
                                                    <section class="col col-6">
                                                            <section>
                                                                <label class="label">Remarks <span style="color: red;"></span></label>
                                                                <label class="textarea">
                                                                    <textarea rows="5" class="custom-scroll remarks" name="remarks"><?= $service->remarks; ?></textarea>
                                                                </label>
                                                            </section>
                                                            <section class="delivered_date_div_cls">
                                                                <label class="label">Delivered Date <span style="color: red;">*</span></label>
                                                                <label class="input">
                                                                    <i class="icon-append fa fa-calendar"></i>
                                                                    <input type="text" name="delivered_date" id="delivered_date" class="form-control bootstrap-datepicker-comncls" placeholder="DD/MM/YYYY" value="<?= $service->delivered_date_str; ?>" />
                                                                </label>
                                                            </section>
                                                    </section>
                                                </div>
                                            </fieldset>
                                            
                                            <footer>
                                                <button type="submit" class="btn btn-primary" >Update</button>
                                                <button type="reset" class="btn btn-default" >Cancel</button>
                                            </footer>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                    <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/services.js"></script>