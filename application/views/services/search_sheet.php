<style>
.services_cls .jarviswidget > div {
    background-color: #FFF !important;
}
.data_fnt_size{
    font-size:14px;
    vertical-align: bottom;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->
        <?php
        echo breadcrumb_links();
        $user_data = get_user_data();
        ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) {
                    ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>

        <form class="add_form" action="<?php echo $form_action; ?>" method="post" name="services" id="services_form" enctype="multipart/form-data">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-search"></i> </span>
                                <h2>Search Services Sheet </h2>
                            </header>
                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" >
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                            <input type="hidden" class="form-control serv_id" name="serv_id" value="" />                                            
                                            <div class="row">
                                                <section class="col col-3">&nbsp;</section>
                                                <section class="col col-6">
                                                    <section >
                                                        <label class="label">Scan Job Sheet Number <span style="color: red;">*</span></label>
                                                        <label class="input"><i class="icon-append fa fa-key"></i>
                                                            <input type="text" class="jobsheet_number" name="jobsheet_number" id="jobsheet_number" value=""/>
                                                        </label>
                                                    </section>
                                                </section> 
                                                <section class="col col-3">&nbsp;</section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-12">
                                                    <div class="jobsheet_data">
                                                        &nbsp;
                                                    </div>
                                                </section>
                                            </div>
                                            
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                    <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script>
  $(document).ready(function () {
      $(document).on('keyup', '.jobsheet_number', function () {
            var sheet_number = $(this).val();
            var ajaxURL = baseUrl + 'scan_service/getsheet_data';
            var ajaxData = {
                sheet_number: sheet_number
            }
            var data = ajaxRequest(ajaxURL, ajaxData);
            $('.jobsheet_data').html(data.jobsheet);
      });
  });  
</script>