<style>
    .exc-add-row-top, .exc-remove-row-top {
       color: #A90329;
       padding-left: 3px;
    }
    .inv-add-row, .serv-add-row {
       padding-right: 3px;
    }
    .service_details{
        background: #afd2ce;
    }
    .external-remove-row{
        padding-left: 3px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->
        <?php
        echo breadcrumb_links();
        $user_data = get_user_data();
        ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) {
                    ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>

            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                        <a href="<?php echo base_url(); ?>services/" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="List"><i class="glyphicon glyphicon-list"></i></button></a>
                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                            <header>
                                <ul class="nav nav-tabs pull-left in">
                                    <li class="active">
                                        <a data-toggle="tab" class="tabcls1" href="#tab-1"> <span class="widget-icon"> <i class="fa fa-pencil"></i> </span> <span class="hidden-mobile hidden-tablet"> Add Service Items </span> </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" class="tabcls2" href="#tab-2">
                                            <i class="fa fa-link"></i>
                                            <span class="hidden-mobile hidden-tablet"> External Service</span>
                                        </a>
                                    </li>
                                </ul>
                            </header>
                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="tab-content">
                                    <div class="tab-pane active" id="tab-1">
                                        <form class="add_form" action="<?php echo $form_action; ?>" method="post" name="service_item" id="service_item_form">    
                                        <div class="smart-form" >
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                            <input type="hidden" class="form-control exr_id" name="exr_id" value="" />
                                            
                                            <div class="row">
                                                <section class="col col-6">
                                                    <table id="user" class="table table-bordered table-striped" style="clear: both;">
                                                        <tbody>
                                                            <tr>
                                                                <td style="width:35%;"><b>Job Sheet Number</b></td>
                                                                <td style="width:65%;"><?php echo $service->jobsheet_number; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width:35%;"><b>Company Name</b></td>
                                                                <td style="width:65%;"><?php echo $service->customer_name; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width:35%;"><b>Contact Person</b></td>
                                                                <td style="width:65%;"><?php echo $service->contact_person; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width:35%;"><b>Contact Number</b></td>
                                                                <td style="width:65%;"><?php echo $service->contact_number; ?></td>
                                                            </tr>
                                                            
                                                        </tbody>
                                                    </table>
                                                </section> 
                                                <section class="col col-6">
                                                    <table id="user" class="table table-bordered table-striped" style="clear: both;">
                                                    <tbody>
                                                            <tr>
                                                                <td style="width:35%;"><b>Email Address</b></td>
                                                                <td style="width:65%;"><?php echo $service->email_address; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width:35%;"><b>Material Name/Type</b></td>
                                                                <td style="width:65%;"><?php echo $service->material_name.' / '.$service->material_type;; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width:35%;"><b>Received Date</b></td>
                                                                <td style="width:65%;"><?php echo $service->received_date_str; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width:35%;"><b>Engineer Name</b></td>
                                                                <td style="width:65%;"><?php echo $service->engineer_name; ?></td>
                                                            </tr>
                                                        </tbody>
                                                     </table>
                                                </section> 
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Service Cost</legend>
                                            <div class="row service-material-container service_details">
                                                <div class="items_head col-xs-12 clr">
                                                    <section class="col col-9">
                                                        <label class="label"><b>Description</b></label>
                                                    </section>
                                                    <section class="col col-3" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="label"><b>Amount</b></label>
                                                    </section>
                                                </div>

                                                <div class="service-items col-xs-12 clr">
                                                    <section class="col col-9">
                                                        <label class="textarea textarea-expandable">
                                                            <i class="icon-append fa fa-comment"></i>
                                                            <textarea rows="1" class="custom-scroll serv_product_code product_code_valcls" name="serv_product_code[0]"></textarea>
                                                        </label>
                                                    </section>
                                                    <section class="col col-3" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="input" style="width: 35%; text-align: center; float: right; margin-top: 2px;">
                                                            <i class="fa fa-2x fa-plus-circle serv-add-row"></i>
                                                        </label>
                                                        <label class="input" style="width: 65%; float: left;">
                                                            <i class="icon-prepend fa fa-dollar"></i>
                                                            <input type="text" name="serv_amount[0]" class="serv_amount amount float_value" />
                                                        </label>
                                                    </section>
                                                </div>
                                            </div>                                        
                                        </fieldset>    
                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Items From Exchange Order</legend>
                                            <div class="exchange_order_items">
                                            <div class="row">
                                                <div class="col-xs-12 clr"  style="padding-top: 10px;background: #9bb9b6;">
                                                <section class="col col-2">
                                                    <label class="label">
                                                            <b>Exchange Order</b>
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                        <label class="select">
                                                            <select name="exchange_order_id[0]" id="exchange_order_id" class="select2 exchange_order_id" >
                                                                <option value="" > --- Select Exchange Order ---</option>
                                                                <?php foreach ($exchange_order_list as $key => $exchange_order) { ?> 
                                                                    <option value="<?php echo $exchange_order->id; ?>" ><?php echo $exchange_order->exr_code.' / '.$exchange_order->name; ?></option>
                                                                <?php } ?>
                                                            </select>  
                                                        </label>
                                                </section>
                                                <section class="col col-3">
                                                    &nbsp;
                                                </section>
                                                <section class="col col-1">
                                                    <label class="input" style="margin-left: 8px;margin-top: 2px;">
                                                        <i class="fa fa-2x fa-plus-circle exc-add-row-top"></i>
                                                    </label>
                                                </section>
                                                </div>
                                            </div>
                                            <div class="row material-container exc_product_details">
                                                <div class="items_head col-xs-12 clr">
                                                    <section class="col col-2">
                                                        <label class="label">
                                                            <b>Item Code</b>
                                                        </label>
                                                    </section>
                                                    <section class="col col-4">
                                                        <label class="label"><b>Brand Name - Description</b></label>
                                                    </section>
                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="label"><b>Qty</b></label>
                                                    </section>
                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="label"><b>Selling Price</b></label>
                                                    </section>
                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="label"><b>Cost Price</b></label>
                                                    </section>
                                                    <section class="col col-3" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="label"><b>Amount</b></label>
                                                    </section>
                                                </div>

                                                <div class="exc-items calculate_items col-xs-12 clr">
                                                    <section class="col col-2">
                                                        <label class="input">
                                                            <input type="hidden" name="exc_product_id[0][0]" class="exc_product_id" value="" />
                                                            <input type="text" name="exc_product_code[0][0]" class="exc_product_code product_code_valcls" data-value="" />
                                                        </label>
                                                        <span class="product_code_label">Selected Item code already exist!</span>
                                                    </section>

                                                    <section class="col col-4">
                                                        <label class="input">
                                                            <input type="text" name="exc_product_description[0][0]" class="exc_product_description" data-value="" />
                                                        </label>
                                                        <span class="error_description">Selected Item description already exist!</span>
                                                    </section>

                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="input">
                                                            <input type="text" name="exc_quantity[0][0]" class="exc_quantity quantity float_value" />
                                                        </label>
                                                        <span class="unit_label"></span>
                                                    </section>

                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="input">
                                                            <!--<i class="icon-prepend fa fa-dollar"></i>-->
                                                            <input type="text" name="exc_price[0][0]" class="exc_price price float_value" />
                                                        </label>
                                                    </section>
                                                    
                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="input" style="margin-top: 1px;">
                                                            <!--<i class="icon-prepend fa fa-dollar"></i>-->
                                                            <input type="text" name="exc_cost[0][0]" class="exc_cost cost float_value" />
                                                        </label>
                                                    </section>

                                                    <section class="col col-3" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="input" style="width: 35%; text-align: center; float: right; margin-top: 2px;">
                                                            <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                        </label>
                                                        <label class="input" style="width: 65%; float: left;">
                                                            <i class="icon-prepend fa fa-dollar"></i>
                                                            <input type="text" name="exc_amount[0][0]" class="exc_amount amount float_value" readonly="" />
                                                        </label>
                                                    </section>
                                                </div>
                                            </div>
                                            </div>                                           
                                        </fieldset>
                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Items From Inventory</legend>
                                            <div class="row product_search">
                                                <section class="col col-5">
                                                    <div class="input hidden-mobile">
                                                        <input name="search_product" class="form-control search_product lowercase" type="text" id="search_product" tabindex="-1" autofocus />
                                                    </div>
                                                </section>
                                            </div>
                                            <div class="row inv-material-container product_details">
                                                <div class="items_head col-xs-12 clr">
                                                    <section class="col col-2">
                                                        <label class="label">
                                                            <b>Item Code</b>
                                                        </label>
                                                    </section>
                                                    <section class="col col-4">
                                                        <label class="label"><b>Brand Name - Description</b></label>
                                                    </section>
                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="label"><b>Qty</b></label>
                                                    </section>
                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="label"><b>Price</b></label>
                                                    </section>
                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="label"><b>Cost</b></label>
                                                    </section>
                                                    <section class="col col-3" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="label"><b>Amount</b></label>
                                                    </section>
                                                </div>

                                                <div class="inv-items items calculate_items col-xs-12 clr">
                                                    <section class="col col-2">
                                                        <label class="input">
                                                            <input type="hidden" name="category_id[0]" class="category_id" value="" />
                                                            <input type="hidden" name="product_id[0]" class="product_id" value="" />
                                                            <input type="text" name="product_code[0]" class="product_code product_code_valcls" data-value="" />
                                                        </label>
                                                        <span class="product_code_label">Selected Item code already exist!</span>
                                                    </section>

                                                    <section class="col col-4">
                                                        <label class="input">
                                                            <input type="text" name="product_description[0]" class="product_description" data-value="" />
                                                        </label>
                                                        <span class="error_description">Selected Item description already exist!</span>
                                                    </section>

                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="input">
                                                            <input type="text" name="quantity[0]" class="inv_quantity quantity float_value" />
                                                        </label>
                                                        <span class="unit_label"></span>
                                                    </section>

                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="input">
                                                            <!--<i class="icon-prepend fa fa-dollar"></i>-->
                                                            <input type="text" name="price[0]" class="inv_price price float_value" />
                                                        </label>
                                                    </section>
                                                    
                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="input" style="margin-top: 1px;">
                                                            <!--<i class="icon-prepend fa fa-dollar"></i>-->
                                                            <input type="text" name="cost[0]" class="inv_cost cost float_value" />
                                                        </label>
                                                    </section>

                                                    <section class="col col-3" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="input" style="width: 35%; text-align: center; float: right; margin-top: 2px;">
                                                            <i class="fa fa-2x fa-plus-circle inv-add-row"></i>
                                                        </label>
                                                        <label class="input" style="width: 65%; float: left;">
                                                            <i class="icon-prepend fa fa-dollar"></i>
                                                            <input type="text" name="amount[0]" class="inv_amount amount float_value" readonly="" />
                                                        </label>
                                                    </section>
                                                </div>
                                            </div>                                        
                                        </fieldset>
                                        <fieldset>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <input type="hidden" name="sub_total" class="sub_total" readonly="" />
                                                </section>
                                                <section class="col col-3">
                                                    <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Total Amount</b></label>
                                                </section>
                                                <section class="col col-3">
                                                    <label class="input" style="width: 65%; float: left;">
                                                        <i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="total_amt" class="total_amt" readonly="" />
                                                    </label>
                                                </section>
                                            </div>                                           
                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn btn-primary" >Submit</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>
                                        </form>    
                                    </div>
                                    
                                    <div class="tab-pane" id="tab-2">
                                        <form class="edit_form" id="exchangeorder_form" name="exchangeorder_form" action="<?php echo $external_form_action; ?>" method="post" data-form_submit="0">
                                            <div class="smart-form" id="sales_form">
                                                <fieldset>
                                                    <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                                    <input type="hidden" class="form-control exr_id" name="exr_id" value="" />

                                                    <div class="row exter-material-container exter_details">
                                                        <div class="items_head col-xs-12 clr">
                                                            <section class="col col-3">
                                                                <label class="label" ><b>External Service</b> <span style="color: red;">*</span></label>
                                                            </section>
                                                            <section class="col col-4">
                                                                <label class="label" ><b>Description</b></label>
                                                            </section>
                                                            <section class="col col-1" style="padding-left: 0px;">
                                                                <label class="label" ><b>Service Cost</b> <span style="color: red;">*</span></label>
                                                            </section>
                                                            <section class="col col-1">
                                                                <label class="label" ><b>Our Cost</b></label>
                                                            </section>
                                                            <section class="col col-2">
                                                                <label class="label" ><b>Total</b></label>
                                                            </section>
                                                            <section class="col col-1">
                                                                <label class="label" >&nbsp;</label>
                                                            </section>
                                                        </div>
                                                        <?php $external_items_count = count($service_external_items); ?>
                                                        <?php if($external_items_count>0) { foreach ($service_external_items as $key => $external_items) { ?>
                                                        <div class="exter_items col-xs-12 clr">
                                                            <section class="col col-3">
                                                                <input type="hidden" name="ser_external_id[<?= $key; ?>]" class="ser_external_id" value="<?= $external_items['id']; ?>" />
                                                                <label class="select sup_name_list_div">
                                                                        <select name="external_service_id[<?= $key ?>]" class="select2 external_service_id" id="external_service_id">
                                                                            <option value="">Select Supplier name</option>
                                                                            <?php foreach ($external_service as $exkey => $external) { ?>
                                                                                <option value="<?php echo $external->id; ?>" <?= ($external_items['external_service_id'] == $external->id) ? "selected" : ""; ?>><?php echo $external->exter_service_no.' / '.$external->service_partner_name; ?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                </label> 
                                                            </section>
                                                            <section class="col col-4" style="padding-left: 5px;padding-right: 5px;">
                                                                <label class="textarea textarea-expandable">
                                                                    <i class="icon-append fa fa-comment"></i>
                                                                    <textarea rows="1" class="custom-scroll exter_service_desc" name="exter_service_desc[<?= $key ?>]"> <?= $external_items['exter_service_desc']; ?></textarea>
                                                                </label>
                                                            </section>
                                                            <section class="col col-1" style="padding-left: 5px;padding-right: 5px;">
                                                                <label class="input">
                                                                    <input type="text" name="external_cost[<?= $key ?>]" class="external_cost float_value" value="<?= $external_items['external_cost']; ?>" style="padding-left: 2px; padding-right: 2px;"/>
                                                                </label>
                                                            </section>
                                                            <section class="col col-1" style="padding-left: 5px;padding-right: 5px;">
                                                                <label class="input">
                                                                    <input type="text" name="our_cost[<?= $key ?>]" class="our_cost float_value" value="<?= $external_items['our_cost']; ?>" style="padding-left: 2px; padding-right: 2px;"/>
                                                                </label>
                                                            </section>
                                                            <section class="col col-2" style="padding-left: 5px;padding-right: 5px;">
                                                                <label class="input">
                                                                    <i class="icon-prepend fa fa-dollar"></i>
                                                                    <input type="text" name="external_sub_amount[<?= $key ?>]" class="external_sub_amount float_value" value="<?= $external_items['external_sub_amount']; ?>" readonly="" />
                                                                </label>
                                                            </section>
                                                            <section class="col col-1">
                                                                <label class="input">
                                                                    <?php if ($external_items_count == ($key + 1)) { ?>
                                                                        <i class="fa fa-2x fa-plus-circle external-add-row" style="margin-top: 5px;"></i>
                                                                    <?php } ?>
                                                                    <?php if ($external_items_count > 1) { ?>
                                                                        <i class="fa fa-2x fa-minus-circle external-remove-row" style="margin-top: 5px;"></i>
                                                                    <?php } ?>
                                                                    
                                                                </label>
                                                            </section>
                                                        </div> 
                                                        <?php } } else { ?>
                                                        <div class="exter_items col-xs-12 clr">
                                                            <section class="col col-3">
                                                                <label class="select sup_name_list_div">
                                                                        <select name="new_external_service_id[0]" class="select2 external_service_id" id="external_service_id">
                                                                            <option value="">Select Supplier name</option>
                                                                            <?php foreach ($external_service as $key => $external) { ?>
                                                                                <option value="<?php echo $external->id; ?>"><?php echo $external->exter_service_no.' / '.$external->service_partner_name; ?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                </label> 
                                                            </section>
                                                            <section class="col col-4" style="padding-left: 5px;padding-right: 5px;">
                                                                <label class="textarea textarea-expandable">
                                                                    <i class="icon-append fa fa-comment"></i>
                                                                    <textarea rows="1" class="custom-scroll exter_service_desc" name="new_exter_service_desc[0]"></textarea>
                                                                </label>
                                                            </section>
                                                            <section class="col col-1" style="padding-left: 5px;padding-right: 5px;">
                                                                <label class="input">
                                                                    <input type="text" name="new_external_cost[0]" class="external_cost float_value" value="" style="padding-left: 2px; padding-right: 2px;"/>
                                                                </label>
                                                            </section>
                                                            <section class="col col-1" style="padding-left: 5px;padding-right: 5px;">
                                                                <label class="input">
                                                                    <input type="text" name="new_our_cost[0]" class="our_cost float_value" value="" style="padding-left: 2px; padding-right: 2px;"/>
                                                                </label>
                                                            </section>
                                                            <section class="col col-2" style="padding-left: 5px;padding-right: 5px;">
                                                                <label class="input">
                                                                    <i class="icon-prepend fa fa-dollar"></i>
                                                                    <input type="text" name="new_external_sub_amount[0]" class="external_sub_amount float_value" value="<?php echo $item->total; ?>" readonly="" />
                                                                </label>
                                                            </section>
                                                            <section class="col col-1">
                                                                <label class="input">
                                                                    <i class="fa fa-2x fa-plus-circle external-add-row" style="margin-top: 5px;"></i>
                                                                </label>
                                                            </section>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </fieldset>
                                                <fieldset>
                                                    <div class="row">
                                                        <section class="col col-6">
                                                            &nbsp;
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Net Amount</b></label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="input" style="width: 65%; float: left;">
                                                                <i class="icon-prepend fa fa-dollar"></i>
                                                                <input type="text" name="total_exter_amt" class="total_exter_amt" value="<?php echo $service->external_service_amount; ?>" readonly="" />
                                                            </label>
                                                        </section>
                                                    </div>                                           
                                                </fieldset>
                                                <footer>
                                                    <button type="submit" class="btn btn-primary" >Submit</button>
                                                    <button type="reset" class="btn btn-default" >Cancel</button>
                                                </footer>
                                            </div>
                                        </form>     
                                    </div>    
                                        
                                    </div>    
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                    <!-- /.row -->
                </div>
            </section>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/service_item.js"></script>