<table width="100%" border="0" style="border-collapse:collapse; font-size: 14px;text-align:center;" cellpadding="5">
        <tr>
            <td width="17%" colspan="2" style="border-bottom:2px dotted #090808;height: 35px;">&nbsp;</td>
            <td width="33%" colspan="2" style="height: 35px;">&nbsp;</td>
            <td width="30%" colspan="2" style="border-bottom:2px dotted #090808;height: 35px;">&nbsp;</td>
        </tr>
        <tr>
            <td width="17%" colspan="2" style="height: 35px;">Customer Signature</td>
            <td width="33%" colspan="2" style="height: 35px;">&nbsp;</td>
            <td width="30%" colspan="2" style="height: 35px;">Engineer Signature</td>
        </tr>
        <tr><td colspan="6" style="height: 20px;"></td></tr>
</table>
<table width="100%" border="0" style="border-collapse:collapse; font-size: 12px;text-align:center;" cellpadding="5">
        <tr><td><?= $company->comp_name;?></td></tr>
        <tr><td><?= $company->comp_address.' Tel: '.$company->comp_phone;?></td></tr>
        <tr><td>Email: <?= $company->comp_email; ?> Website: <?= $company->comp_website; ?> </td></tr>
</table>
<table width="100%" style="vertical-align: bottom; font-family: calibri; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
	<tr>
		<td width="100%" align="center">Page {PAGENO}/{nbpg}</td>
	</tr>
</table>
</body>
</html>
