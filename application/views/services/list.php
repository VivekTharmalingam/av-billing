<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>

        <!-- Main content -->
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo $add_link; ?>" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-pencil"></i></button></a>
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Services List</h2>                             
                        </header>
                        <div>
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <div class="widget-body no-padding">
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>			                
                                        <tr>
                                            <th style="width: 5%;" data-hide="phone" data-class="expand">S.No</th>
                                            <th style="width: 15%;" ><i class="fa fa-fw fa-key txt-color-blue hidden-md hidden-sm hidden-xs"></i>JobSheet No.</th>
                                            <th style="width: 18%;" data-hide="phone"><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i>Customer</th>
                                            <th style="width: 13%;" data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i>Received Date</th>
                                            <th style="width: 15%;" data-hide="phone"><i class="fa fa-fw fa-user-secret txt-color-blue hidden-md hidden-sm hidden-xs"></i>Engineer </th>
                                            <th style="width: 10%;" data-hide="phone,tablet"><i class="fa fa-fw fa-warning txt-color-blue hidden-md hidden-sm hidden-xs"></i>Stage</th>
                                            <th style="width: 12%;" data-hide="phone,tablet"><i class="fa fa-fw fa-pencil-square-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> Updates</th>
                                            <th style="width: 12%;" style="text-align: center;"><i class="fa fa-fw fa-cog txt-color-blue hidden-md hidden-xs"></i> Actions</th>
                                        </tr>
                                    </thead>

                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                </article>
                <!-- /.row -->
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/your_style.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/datatables.js"></script>
<script type="text/javascript">
    $(document).on('click', '#smart-mod-eg2', function (e) {
        $('#rackNumber .error_msg').text('');
        $('#rackNumber .delivered_date_cls').hide();
        $('#delivered_date').val('');
        $('#rackNumber').modal();
        var $action = $(this).attr('data-href');
        var $id = $(this).attr('data-id');
        $('#rackNumber form').attr('action',$action);
        var ajaxURL = baseUrl + 'services/get_rack_no';
        var ajaxData = {
            id: $id
        }
        var details = ajaxRequest(ajaxURL, ajaxData, '');
        if(details.service_status != '' && details.service_status != null) {
            $('#rackNumber input.rack_number_txt').val(details.rack_number);
            $('#service_status').select2('val', details.service_status);
            if(details.service_status == '3') {
                $('#rackNumber .delivered_date_cls').show();
                $('#delivered_date').val(details.delivered_date_str);
            }
        }

    });
    
    $(document).on('change', '#service_status', function () {
        var service_status = $(this).val();
        $('#delivered_date').val('');
        if(service_status=='3') {
            $('.delivered_date_cls').show();
        } else {
            $('.delivered_date_cls').hide();
        }
    });
    
    $(document).on('click', '#rackNumber .ok', function (e) {
        if($('#rackNumber .rack_number_txt').val() == '') {
           $('#rackNumber .error_msg').text('Please enter Rank No.!');
           return false;
        } else {
            $('#rackNumber .error_msg').text('');
        }
    });
    
    $(document).on('keyup', '#rackNumber input.rack_number_txt', function (e) {
        $('#rackNumber .error_msg').text('');
    });
</script>