<table width="100%" border="0" class="lowercase" align="center" style="font-family:Times New Roman; font-size:16px;">
    <?php $comp_logo = ( !empty( $company->comp_logo ) && file_exists( UPLOADS . $company->comp_logo ) ) ? base_url() . UPLOADS . $company->comp_logo : 'assets/images/logo.png'; ?>
         <tr><td colspan="6">&nbsp;</td></tr>
         <tr>
             <td width="22%">Company Name </td>
             <td width="1%">:</td>
             <td width="52%" colspan="2"  style="border-bottom:1px solid #090808;height: 35px;font-size:12px;vertical-align: bottom;"><?= $service->customer_name; ?></td>
             <td width="1%">&nbsp;</td>
             <td width="24%" rowspan="5" style="border-top:1px solid #090808;border-left:1px solid #090808;border-right:1px solid #090808;border-bottom:1px solid #090808;text-align: center;font-weight: bold;color: gray;">JOB SHEET NO <br> 
                 <b class="jobsheet_no"><?= $service->jobsheet_number; ?></b> <br>
                <div class="barcodecell">
                        <barcode code="<?php echo strtoupper($service->jobsheet_number); ?>" type="C39" height="0.55" class="barcode" />
                </div>
                 <?= $service->engineer_name; ?>
             </td>
         </tr>
         <tr>
             <td width="22%">Contact Person </td>
             <td width="1%">:</td>
             <td width="52%" colspan="2"  style="border-bottom:1px solid #090808;height: 35px;font-size:12px;vertical-align: bottom;"><?= $service->contact_person; ?></td>
             <td width="1%">&nbsp;</td>
         </tr>
         <tr>
             <td width="22%">Contact Number </td>
             <td width="1%">:</td>
             <td width="52%" colspan="2"  style="border-bottom:1px solid #090808;height: 35px;font-size:12px;vertical-align: bottom;"><?= $service->contact_number; ?></td>
             <td width="1%">&nbsp;</td>
         </tr>
         <tr>
             <td width="22%">Email Address </td>
             <td width="1%">:</td>
             <td width="52%" colspan="2"  style="border-bottom:1px solid #090808;height: 35px;font-size:12px;vertical-align: bottom;"><?= $service->email_address; ?></td>
             <td width="1%">&nbsp;</td>
         </tr>
         <tr>
             <td colspan="5">&nbsp;</td>
         </tr>
         <tr><td colspan="6" style="height: 10px;"></td></tr>
         <tr>
             <td width="22%">Brand Name </td>
             <td width="1%">:</td>
             <td width="77%" colspan="4"  style="border-bottom:1px solid #090808;height: 35px;font-size:12px;vertical-align: bottom;"><?= $service->cat_name; ?></td>
         </tr>
         <tr>
             <td width="22%">Product Name/Type </td>
             <td width="1%">:</td>
             <td width="77%" colspan="4"  style="border-bottom:1px solid #090808;height: 35px;font-size:12px;vertical-align: bottom;"><?= $service->material_name.' / '.$service->material_type; ?></td>
         </tr>
         <tr><td colspan="6" style="height: 15px;"></td></tr>
         <?php $img_url = ($service->upload_photo != "") ? BASE_URL .'uploads/'.$service->upload_photo : BASE_URL . NO_IMG; ?>
         <tr>
             <td width="63%" colspan="3" style="border-bottom:1px solid #090808;"><span style="padding-right: 28px;">Problem Description &nbsp;</span>:&nbsp;<span style="height: 35px;padding-left: 5px;font-size:12px;"><?= $service->problem_desc; ?></span></td>
             <td width="1%">&nbsp;</td>
             <td width="36%" colspan="2" rowspan="7" style="border-top:1px solid #090808;border-left:1px solid #090808;border-right:1px solid #090808;border-bottom:1px solid #090808;text-align: center;"><img style="width: 210;" src="<?php echo $img_url; ?>" /></td>
         </tr>
         <tr><td colspan="4" style="height: 15px;"></td></tr>
         <tr>
             <td width="63%" colspan="3" style="border-bottom:1px solid #090808;"><span>Guessing Problem &nbsp;&nbsp;&nbsp;&nbsp;</span>:&nbsp;<span style="height: 35px;padding-left: 5px;font-size:12px;"><?= $service->guessing_problem; ?></span></td>
             <td width="1%">&nbsp;</td>
         </tr>
         <tr><td colspan="4" style="height: 15px;"></td></tr>
         <tr>
             <td width="22%">Received Items </td>
             <td width="1%">:</td>
             <td width="40%" style="height: 35px;">&nbsp;</td>
             <td width="1%">&nbsp;</td>
         </tr>
         <tr>
             <td width="63%" colspan="3"  style="border-bottom:1px solid #090808;height: 35px;font-size:14px;vertical-align: bottom;">
                 <?php
                 if(count($service_item)>0) { $sn=1; foreach ($service_item as $key => $value) {
                         echo $sn.'.&nbsp;'.$value->item_name_str.'<br>'; $sn++;
                     }
                 }
                 ?>
             </td>
             <td width="1%">&nbsp;</td>
         </tr>
         <tr><td colspan="4" style="height: 20px;"></td></tr>
         
         <tr><td colspan="6" style="border-bottom:1px solid #090808;height: 35px;"><span style="padding-right: 65px;">Other Remarks &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>:&nbsp;<span style="font-size:12px;padding-left: 5px;"><?= $service->remarks; ?></span></td></tr>
         <tr><td colspan="6" style="height: 30px;"></td></tr>
         <tr><td colspan="6" width="100%"><table width="100%" class="lowercase" style="font-family:Times New Roman; font-size:16px;">
              <tr>
                  <td width="23%" style="vertical-align: bottom;">Estimated Quoted Price &nbsp;&nbsp;:</td>
                 <td width="27%"  style="border-bottom:1px solid #090808;height: 35px;font-size:14px;vertical-align: bottom;">S$&nbsp;<?= $service->estimated_price_quoted; ?></td>
                 <td width="23%" style="vertical-align: bottom;">Estimated Delivery Date &nbsp;&nbsp;:</td>
                 <td width="27%"  style="border-bottom:1px solid #090808;height: 35px;font-size:14px;vertical-align: bottom;">&nbsp;<?= $service->estimated_delivery_date_str; ?></td>
             </tr>   
          </table></td></tr>
         <tr><td colspan="6" style="height: 30px;"></td></tr>
         <tr><td colspan="6">I acknowledge that I have read and agree to the above Information and update the job status notification to me.</td></tr>
         <tr><td colspan="6" style="height: 30px;"></td></tr>
</table>