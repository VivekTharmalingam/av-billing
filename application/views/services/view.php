<style>
.services_cls .jarviswidget > div {
    background-color: #FFF !important;
}
.data_fnt_size{
    font-size:14px;
    vertical-align: bottom;
}
</style>
<link href="<?php echo base_url(); ?>/assets/css/plugin/barcode.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/plugin/JsBarcode/EAN_UPC.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/plugin/JsBarcode/CODE128.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/plugin/JsBarcode/JsBarcode.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".barcode3").JsBarcode(
                                $('.jobsheet_no').html(),
                                {
                                        width: 1, height: 30, displayValue: false, color: '424242', fontSize: 20
                                }
                        );
    });
</script>
<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php
        echo breadcrumb_links();
        $user_data = get_user_data();
        ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>services/" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="List"><i class="glyphicon glyphicon-list"></i></button></a>
                    <a href="<?php echo $service_pdf; ?>" class="large_icon_des" style=" color: white;margin-right: 80px;" ><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "  title="Download"><i class="glyphicon glyphicon-save"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                        <header>
                            <ul class="nav nav-tabs pull-left in">
                                <li class="active">
                                    <a data-toggle="tab" class="tabcls1" href="#tab-1"> <span class="widget-icon"> <i class="fa fa-pencil"></i> </span> <span class="hidden-mobile hidden-tablet"> Service View </span> </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" class="tabcls2" href="#tab-2">
                                        <i class="fa fa-link"></i>
                                        <span class="hidden-mobile hidden-tablet"> Service Item View </span>
                                    </a>
                                </li>
                            </ul>
                        </header>
                        <!-- widget div-->
                        <div role="content">
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <div class="widget-body">
                                 <div class="tab-content">
                                     <div class="tab-pane active" id="tab-1">
                                         <table width="80%" border="0" class="lowercase" align="center" style="font-family:Times New Roman; font-size:18px;">
                                            <?php $comp_logo = ( !empty( $company->comp_logo ) && file_exists( UPLOADS . $company->comp_logo ) ) ? base_url() . UPLOADS . $company->comp_logo : 'assets/images/logo.png'; ?>
                                                 <tr>
                                                     <td width="15%"><img src="<?php echo $comp_logo;?>" height="80"/></td>
                                                     <td width="45%" colspan="4" style="font-size:22px; text-align: center;">SERVICE - JOB SHEET</td>
                                                     <td width="20%" style="font-style: italic; text-align: center;">Customer Copy</td>
                                                 </tr>
                                                 <tr><td colspan="6">&nbsp;</td></tr>
                                                 <tr>
                                                     <td width="15%">Company Name </td>
                                                     <td width="2%">:</td>
                                                     <td width="42%" colspan="2" class="data_fnt_size" style="border-bottom:1px solid #090808;height: 35px;"><?= $service->customer_name; ?></td>
                                                     <td width="1%">&nbsp;</td>
                                                     <td width="20%" rowspan="5" style="border-top:1px solid #090808;border-left:1px solid #090808;border-right:1px solid #090808;border-bottom:1px solid #090808;text-align: center;font-weight: bold;color: gray;">JOB SHEET NO <br> <b class="jobsheet_no"><?= $service->jobsheet_number; ?></b> <br><img class="barcode3" style="margin-top: 10px;margin-bottom: 10px;" width="100%" height="30px"/><br> <?= $service->engineer_name; ?></td>
                                                 </tr>
                                                 <tr>
                                                     <td width="15%">Contact Person </td>
                                                     <td width="2%">:</td>
                                                     <td width="42%" colspan="2" class="data_fnt_size" style="border-bottom:1px solid #090808;height: 35px;"><?= $service->contact_person; ?></td>
                                                     <td width="1%">&nbsp;</td>
                                                 </tr>
                                                 <tr>
                                                     <td width="15%">Contact Number </td>
                                                     <td width="2%">:</td>
                                                     <td width="42%" colspan="2" class="data_fnt_size" style="border-bottom:1px solid #090808;height: 35px;"><?= $service->contact_number; ?></td>
                                                     <td width="1%">&nbsp;</td>
                                                 </tr>
                                                 <tr>
                                                     <td width="15%">Email Address </td>
                                                     <td width="2%">:</td>
                                                     <td width="42%" colspan="2" class="data_fnt_size" style="border-bottom:1px solid #090808;height: 35px;"><?= $service->email_address; ?></td>
                                                     <td width="1%">&nbsp;</td>
                                                 </tr>
                                                 <tr>
                                                     <td colspan="5">&nbsp;</td>
                                                 </tr>
                                                 <tr><td colspan="6">&nbsp;</td></tr>
                                                 <tr>
                                                     <td width="15%">Brand Name </td>
                                                     <td width="2%">:</td>
                                                     <td width="63%" colspan="4" class="data_fnt_size" style="border-bottom:1px solid #090808;height: 35px;"><?= $service->cat_name; ?></td>
                                                 </tr>
                                                 <tr>
                                                     <td width="15%">Product Name/Type </td>
                                                     <td width="2%">:</td>
                                                     <td width="63%" colspan="4" class="data_fnt_size" style="border-bottom:1px solid #090808;height: 35px;"><?= $service->material_name.' / '.$service->material_type; ?></td>
                                                 </tr>
                                                 <tr><td colspan="6" style="height: 15px;"></td></tr>
                                                 <?php $img_url = ($service->upload_photo != "") ? BASE_URL .'uploads/'.$service->upload_photo : BASE_URL . NO_IMG; ?>
                                                 <tr>
                                                     <td width="63%" colspan="3" style="border-bottom:1px solid #090808;"><span style="padding-right: 28px;">Problem Description </span>:<span class="data_fnt_size" style="height: 35px;padding-left: 5px;"><?= $service->problem_desc; ?></span></td>
                                                     <td width="1%">&nbsp;</td>
                                                     <td width="30%" colspan="2" rowspan="7" style="border-top:1px solid #090808;border-left:1px solid #090808;border-right:1px solid #090808;border-bottom:1px solid #090808;"><img style="width: 100%;" src="<?php echo $img_url; ?>" /></td>
                                                 </tr>
                                                 <tr><td colspan="4" style="height: 15px;"></td></tr>
                                                 <tr>
                                                     <td width="63%" colspan="3" style="border-bottom:1px solid #090808;"><span style="padding-right: 44px;">Guessing Problem </span>:<span class="data_fnt_size" style="height: 35px;padding-left: 5px;"><?= $service->guessing_problem; ?></span></td>
                                                     <td width="1%">&nbsp;</td>
                                                 </tr>
                                                 <tr><td colspan="4" style="height: 15px;"></td></tr>
                                                 <tr>
                                                     <td width="15%">Received Items </td>
                                                     <td width="2%">:</td>
                                                     <td width="32%" style="height: 35px;">&nbsp;</td>
                                                     <td width="1%">&nbsp;</td>
                                                 </tr>
                                                 <tr>
                                                     <td width="49%" colspan="3" class="data_fnt_size" style="border-bottom:1px solid #090808;height: 35px;">
                                                         <?php
                                                         if(count($service_item)>0) { $sn=1; foreach ($service_item as $key => $value) {
                                                                 echo $sn.'.&nbsp;'.$value->item_name_str.'<br>'; $sn++;
                                                             }
                                                         }
                                                         ?>
                                                     </td>
                                                     <td width="1%">&nbsp;</td>
                                                 </tr>
                                                 <tr><td colspan="4" style="height: 20px;"></td></tr>
                                                 <tr><td colspan="6" style="border-bottom:1px solid #090808;height: 35px;"><span style="padding-right: 65px;">Other Remarks </span>:<span  class="data_fnt_size" style=" padding-left: 5px;"><?= $service->remarks; ?></span></td></tr>
                                                 <tr><td colspan="6" style="height: 50px;"></td></tr>
                                                 <tr><td colspan="6" width="80%"><table width="100%" class="lowercase">
                                                      <tr>
                                                          <td width="23%" style="vertical-align: bottom;">Estimated Quoted Price &nbsp;&nbsp;:</td>
                                                         <td width="27%" class="data_fnt_size" style="border-bottom:1px solid #090808;height: 35px;">&nbsp;<?= $service->estimated_price_quoted; ?></td>
                                                         <td width="23%" style="vertical-align: bottom;">Estimated Delivery Date &nbsp;&nbsp;:</td>
                                                         <td width="27%" class="data_fnt_size" style="border-bottom:1px solid #090808;height: 35px;">&nbsp;<?= $service->estimated_delivery_date_str; ?></td>
                                                     </tr>   
                                                  </table></td></tr>
                                                 <tr><td colspan="6" style="height: 50px;"></td></tr>
                                                 <tr><td colspan="6">I acknowledge that I have read and agree to the above Information and update the job status notification to me.</td></tr>
                                                 <tr><td colspan="6" style="height: 30px;"></td></tr>
                                                 <tr>
                                                     <td width="17%" colspan="2" style="border-bottom:2px dotted #090808;height: 35px;">&nbsp;</td>
                                                     <td width="33%" colspan="2" style="height: 35px;">&nbsp;</td>
                                                     <td width="30%" colspan="2" style="border-bottom:2px dotted #090808;height: 35px;">&nbsp;</td>
                                                 </tr>
                                                 <tr>
                                                     <td width="17%" colspan="2" style="height: 35px;">Customer Signature</td>
                                                     <td width="33%" colspan="2" style="height: 35px;">&nbsp;</td>
                                                     <td width="30%" colspan="2" style="height: 35px;">Engineer Signature</td>
                                                 </tr>
                                                 <tr><td colspan="6" style="height: 70px;"></td></tr>
                                                 <tr><td colspan="6" >
                                                     <table width="100%" style="text-align:center; font-size: 14px;">
                                                         <tr><td><?= $company->comp_name;?></td></tr>
                                                         <tr><td><?= $company->comp_address.' Tel: '.$company->comp_phone;?></td></tr>
                                                         <tr><td>Email: <?= $company->comp_email; ?> Website: <?= $company->comp_website; ?> </td></tr>
                                                     </table>
                                                  </td></tr>
                                        </table>
                                     </div>
                                     <div class="tab-pane" id="tab-2">
                                        <?php if(count($service_external_items)>0) { ?> 
                                         <div class="text-left">
                                            <span class="onoffswitch-title">
                                                <h3><b style="color: #8e233b;">External Service Item Details </b></h3>
                                            </span>
                                        </div>
                                        <div id="widget-tab-1" class="table-responsive">
                                            <table id="dt_basic_1" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
                                                <thead>
                                                    <tr>
                                                        <th data-class="expand" style="font-weight: bold; width: 5%; text-align:left;">S.No</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 20%;text-align:left;">External Service</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 30%;text-align:left;">Description</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 10%;text-align:left;">Service Cost</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 10%;text-align:left;">Our Cost</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 15%;text-align:right;" style="border-right: 1px solid #ddd;">Amount (S$)</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($service_external_items as $key => $excitems) { ?>
                                                        <tr>
                                                            <td><?php echo ($key + 1); ?></td>
                                                            <td><?php echo $excitems->exter_service_no.' / '.$excitems->service_partner_name; ?></td>
                                                            <td><?php echo $excitems->exter_service_desc; ?></td>
                                                            <td><?php echo $excitems->external_cost; ?></td>
                                                            <td><?php echo $excitems->our_cost; ?></td>
                                                            <td align="right" style="border-right: 1px solid #ddd;"><?php echo $excitems->external_sub_amount; ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php } ?>  
                                        <?php if(count($service_cost)>0) { ?> 
                                         <div class="text-left">
                                            <span class="onoffswitch-title">
                                                <h3><b style="color: #8e233b;">Service Cost Details </b></h3>
                                            </span>
                                        </div>
                                        <div id="widget-tab-1" class="table-responsive">
                                            <table id="dt_basic_1" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
                                                <thead>
                                                    <tr>
                                                        <th data-class="expand" style="font-weight: bold; width: 9%; text-align:left;">S.No</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 72%;text-align:left;">Description</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 19%;text-align:right;" style="border-right: 1px solid #ddd;">Amount (S$)</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($service_cost as $key => $cost) { ?>
                                                        <tr>
                                                            <td><?php echo ($key + 1); ?></td>
                                                            <td><?php echo $cost->product_description; ?></td>
                                                            <td align="right" style="border-right: 1px solid #ddd;"><?php echo $cost->total; ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php } ?>
                                        <?php if(count($exc_items_group)>0) { ?> 
                                         <div class="text-left">
                                            <span class="onoffswitch-title">
                                                <h3><b style="color: #8e233b;">Exchange Order Item Details </b></h3>
                                            </span>
                                        </div>
                                        <div id="widget-tab-1" class="table-responsive">
                                            <table id="dt_basic_1" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
                                                <thead>
                                                    <tr>
                                                        <th data-class="expand" style="font-weight: bold; width: 5%; text-align:left;">S.No</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 20%;text-align:left;">Exc / Item</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 30%;text-align:left;">Description</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 10%;text-align:left;">Quantity</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 10%;text-align:left;">Price</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 10%;text-align:left;">Unit-Cost</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 15%;text-align:right;" style="border-right: 1px solid #ddd;">Amount (S$)</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($exc_items_group as $key => $items_group) { ?>
                                                    <?php $exc_items = $this->services_model->service_excitems_byid($items_group->service_id,$items_group->exchange_order_id); ?>
                                                    <?php foreach ($exc_items as $key => $excitems) { ?>
                                                        <tr>
                                                            <td><?php echo ($key + 1); ?></td>
                                                            <td><?php echo $items_group->exr_code.' / '.$excitems->product_text; ?></td>
                                                            <td><?php echo $excitems->product_description; ?></td>
                                                            <td><?php echo $excitems->quantity; ?></td>
                                                            <td><?php echo $excitems->price; ?></td>
                                                            <td><?php echo $excitems->unit_cost; ?></td>
                                                            <td align="right" style="border-right: 1px solid #ddd;"><?php echo $excitems->total; ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php } ?> 
                                         <?php if(count($inventory_items)>0) { ?> 
                                         <div class="text-left">
                                            <span class="onoffswitch-title">
                                                <h3><b style="color: #8e233b;">Inventory Item Details </b></h3>
                                            </span>
                                        </div>
                                        <div id="widget-tab-1" class="table-responsive">
                                            <table id="dt_basic_1" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
                                                <thead>
                                                    <tr>
                                                        <th data-class="expand" style="font-weight: bold; width: 5%; text-align:left;">S.No</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 20%;text-align:left;">Item</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 30%;text-align:left;">Description</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 10%;text-align:left;">Quantity</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 10%;text-align:left;">Price</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 10%;text-align:left;">Unit-Cost</th>
                                                        <th data-hide="phone,tablet" style="font-weight: bold;width: 15%;text-align:right;" style="border-right: 1px solid #ddd;">Amount (S$)</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($inventory_items as $key => $inventory) { ?>
                                                        <tr>
                                                            <td><?php echo ($key + 1); ?></td>
                                                            <td><?php echo $inventory->pdt_code; ?></td>
                                                            <td><?php echo $inventory->product_description; ?></td>
                                                            <td><?php echo $inventory->quantity; ?></td>
                                                            <td><?php echo $inventory->price; ?></td>
                                                            <td><?php echo $inventory->unit_cost; ?></td>
                                                            <td align="right" style="border-right: 1px solid #ddd;"><?php echo $inventory->total; ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php } ?> 
                                     </div>    
                                 </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
