<style>
.services_cls .jarviswidget > div {
    background-color: #FFF !important;
}
.data_fnt_size{
    font-size:14px;
    vertical-align: bottom;
}
</style>
<link href="<?php echo base_url(); ?>/assets/css/plugin/barcode.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/plugin/JsBarcode/EAN_UPC.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/plugin/JsBarcode/CODE128.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/plugin/JsBarcode/JsBarcode.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".barcode3").JsBarcode(
                                $('.jobsheet_no').html(),
                                {
                                        width: 1, height: 30, displayValue: false, color: '424242', fontSize: 20
                                }
                        );
    });
</script>
<table width="80%" align="center" border="0" class="lowercase" align="center" style="margin: auto;font-family:Times New Roman; font-size:18px;">
    <?php $comp_logo = (!empty($company->comp_logo) && file_exists(UPLOADS . $company->comp_logo) ) ? base_url() . UPLOADS . $company->comp_logo : 'assets/images/logo.png'; ?>
    <tr>
        <td width="15%"><img src="<?php echo $comp_logo; ?>" height="80"/></td>
        <td width="45%" colspan="4" style="font-size:22px; text-align: center;">SERVICE - JOB SHEET</td>
        <td width="20%" style="font-style: italic; text-align: center;">Rack No. : <?= text($service->rack_number,'--');  ?><br>Status : <?= $service->service_status_str;  ?></td>
    </tr>
    <tr><td colspan="6">&nbsp;</td></tr>
    <tr>
        <td width="15%">Company Name </td>
        <td width="2%">:</td>
        <td width="42%" colspan="2" class="data_fnt_size" style="border-bottom:1px solid #090808;height: 35px;"><?= $service->customer_name; ?></td>
        <td width="1%">&nbsp;</td>
        <td width="20%" rowspan="5" style="border-top:1px solid #090808;border-left:1px solid #090808;border-right:1px solid #090808;border-bottom:1px solid #090808;text-align: center;font-weight: bold;color: gray;">JOB SHEET NO <br> <b class="jobsheet_no"><?= $service->jobsheet_number; ?></b> <br><img class="barcode3" style="margin-top: 10px;margin-bottom: 10px;" width="100%" height="30px"/><br> <?= $service->engineer_name; ?></td>
    </tr>
    <tr>
        <td width="15%">Contact Person </td>
        <td width="2%">:</td>
        <td width="42%" colspan="2" class="data_fnt_size" style="border-bottom:1px solid #090808;height: 35px;"><?= $service->contact_person; ?></td>
        <td width="1%">&nbsp;</td>
    </tr>
    <tr>
        <td width="15%">Contact Number </td>
        <td width="2%">:</td>
        <td width="42%" colspan="2" class="data_fnt_size" style="border-bottom:1px solid #090808;height: 35px;"><?= $service->contact_number; ?></td>
        <td width="1%">&nbsp;</td>
    </tr>
    <tr>
        <td width="15%">Email Address </td>
        <td width="2%">:</td>
        <td width="42%" colspan="2" class="data_fnt_size" style="border-bottom:1px solid #090808;height: 35px;"><?= $service->email_address; ?></td>
        <td width="1%">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="5">&nbsp;</td>
    </tr>
    <tr><td colspan="6">&nbsp;</td></tr>
    <tr>
        <td width="15%">Product Name/Type </td>
        <td width="2%">:</td>
        <td width="63%" colspan="4" class="data_fnt_size" style="border-bottom:1px solid #090808;height: 35px;"><?= $service->material_name . ' / ' . $service->material_type; ?></td>
    </tr>
    <tr><td colspan="6" style="height: 15px;"></td></tr>
    <?php $img_url = ($service->upload_photo != "") ? BASE_URL . 'uploads/' . $service->upload_photo : BASE_URL . NO_IMG; ?>
    <tr>
        <td width="63%" colspan="3" style="border-bottom:1px solid #090808;"><span >Problem Description </span>:<span class="data_fnt_size" style="height: 35px;padding-left: 5px;"><?= $service->problem_desc; ?></span></td>
        <td width="1%">&nbsp;</td>
        <td width="30%" colspan="2" rowspan="7" style="border-top:1px solid #090808;border-left:1px solid #090808;border-right:1px solid #090808;border-bottom:1px solid #090808;"><img style="width: 100%;" src="<?php echo $img_url; ?>" /></td>
    </tr>
    <tr><td colspan="4" style="height: 15px;"></td></tr>
    <tr>
        <td width="63%" colspan="3" style="border-bottom:1px solid #090808;"><span style="padding-right: 18px;">Guessing Problem </span>:<span class="data_fnt_size" style="height: 35px;padding-left: 5px;"><?= $service->guessing_problem; ?></span></td>
        <td width="1%">&nbsp;</td>
    </tr>
    <tr><td colspan="4" style="height: 15px;"></td></tr>
    <tr>
        <td width="15%">Received Items </td>
        <td width="2%">:</td>
        <td width="32%" style="height: 35px;">&nbsp;</td>
        <td width="1%">&nbsp;</td>
    </tr>
    <tr>
        <td width="49%" colspan="3" class="data_fnt_size" style="border-bottom:1px solid #090808;height: 35px;">
            <?php
            if (count($service_item) > 0) {
                $sn = 1;
                foreach ($service_item as $key => $value) {
                    echo $sn . '.&nbsp;' . $value->item_name_str . '<br>';
                    $sn++;
                }
            }
            ?>
        </td>
        <td width="1%">&nbsp;</td>
    </tr>
    <tr><td colspan="4" style="height: 20px;"></td></tr>
    <tr><td colspan="6" style="border-bottom:1px solid #090808;height: 35px;"><span style="padding-right: 40px;">Other Remarks </span>:<span  class="data_fnt_size" style=" padding-left: 5px;"><?= $service->remarks; ?></span></td></tr>
    <tr><td colspan="6" style="height: 50px;"></td></tr>
    <tr><td colspan="6" width="80%"><table width="100%" class="lowercase">
                <tr>
                    <td width="23%" style="vertical-align: bottom;">Estimated Quoted Price &nbsp;&nbsp;:</td>
                    <td width="27%" class="data_fnt_size" style="border-bottom:1px solid #090808;height: 35px;">&nbsp;<?= $service->estimated_price_quoted; ?></td>
                    <td width="23%" style="vertical-align: bottom;">Estimated Delivery Date &nbsp;&nbsp;:</td>
                    <td width="27%" class="data_fnt_size" style="border-bottom:1px solid #090808;height: 35px;">&nbsp;<?= $service->estimated_delivery_date_str; ?></td>
                </tr>   
            </table></td></tr>
    <tr><td colspan="6" style="height: 50px;"></td></tr>
    <tr><td colspan="6">I acknowledge that I have read and agree to the above Information and update the job status notification to me.</td></tr>
    <tr><td colspan="6" style="height: 30px;"></td></tr>
    <tr>
        <td width="17%" colspan="2" style="border-bottom:2px dotted #090808;height: 35px;">&nbsp;</td>
        <td width="33%" colspan="2" style="height: 35px;">&nbsp;</td>
        <td width="30%" colspan="2" style="border-bottom:2px dotted #090808;height: 35px;">&nbsp;</td>
    </tr>
    <tr>
        <td width="17%" colspan="2" style="height: 35px;">Customer Signature</td>
        <td width="33%" colspan="2" style="height: 35px;">&nbsp;</td>
        <td width="30%" colspan="2" style="height: 35px;">Engineer Signature</td>
    </tr>
    <tr><td colspan="6" style="height: 70px;"></td></tr>
    <tr><td colspan="6" >
            <table width="100%" style="text-align:center; font-size: 14px;">
                <tr><td><?= $company->comp_name; ?></td></tr>
                <tr><td><?= $company->comp_address . ' Tel: ' . $company->comp_phone; ?></td></tr>
                <tr><td>Email: <?= $company->comp_email; ?> Website: <?= $company->comp_website; ?> </td></tr>
            </table>
        </td></tr>
</table>