<?php
$ti = time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Banking_Report_" . $ti . ".xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<?php $colspan = 7;?>
<table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; margin-left:10px" cellpadding="5">
    <tr style="font-family:Times new roman;">
        <th width="25%" > Transaction Type</th>
        <th width="17%" > <?php echo ($is_cheque=='cheque') ? "Cheque" : "Transaction";  ?> Date</th>
        <th width="13%" > Date</th>
        <th width="15%" > Credit</th>
        <th width="15%" > Debit</th>
        <th width="15%" > Balance</th>
    </tr>
    <?php if (!empty($banking_rept)) { 
        $balance_amt = 0;
        foreach ($banking_rept as $key => $row) { ?>
            <?php $balance_amt += $row->credit_amt;?>
            <?php $balance_amt -= $row->debit_amt;?>
          <tr>
              <td><?php echo text($row->trns_type); ?></td>
              <td><?php echo $row->created_on_str; ?></td>
              <td><?php echo $row->date_str; ?></td>
              <td align="right"><?php echo number_format($row->credit_amt, 2); ?></td>
              <td align="right"><?php echo number_format($row->debit_amt, 2); ?></td>
              <td align="right"><?php echo number_format($balance_amt, 2); ?></td>
          </tr>
  <?php } } else { ?>
          <tr>
              <td colspan="7" align="center">No Record Found</td>
          </tr>
  <?php } ?>      
</table>