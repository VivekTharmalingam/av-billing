<script>window.localStorage.clear();</script>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">&nbsp;</div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="widget-body">
                        <!-- Widget ID (each widget will need unique ID)-->                                
                        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false"	>
                            <header>
                                <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                <h2>Banking Reports</h2>
                            </header>

                            <div>
                                <!--<div class="date-range"></div>-->
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="box-body table-responsive">
                                        <form name="order_search" action="<?php echo $form_action; ?>" method="post" class="search_form validate_form">
                                            <div class="col-xs-12">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="search_date" data-placeholder="dd/mm/yyyy - dd/mm/yyyy" id="date_alloc" style="padding: 6px 12px;margin-right:5%; width: 100%;" name="search_date" value="<?php echo (!empty($search_date)) ? $search_date : ''; ?>" autocomplete="off" >
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-bank"></i>
                                                            </div>
                                                            <select name="bank_acc_id" class="select2 bank_acc_id">
                                                                <?php foreach ($all_bank as $key => $bank) { ?>  
                                                                    <option value="<?php echo $bank->id; ?>" <?php echo ($bank->id == $bank_acc_id) ? 'selected' : ''; ?>><?php echo $bank->bank_name.' / '.$bank->account_no; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                        <span style="margin-left: 10px;">
                                                            <label style="width:28%;"></label>
                                                            <label class="checkbox state-error"><input name="is_cheque" id="is_cheque" class="acts lowercase lvl lvl-3" value="cheque" <?php echo ($is_cheque=='cheque') ? "checked" : "";  ?> type="checkbox"><i></i>Cheque</label>
                                                        </span>
                                                </div>
                                                <div class="col-md-1" style="padding-left: 0px; padding-right: 0px;width: 108px;margin-left: -20px;">
                                                    <div class="form-group cheque_text_div">
                                                        <label style="padding-top: 5px;"></label>
                                                        <select name="cheque_text" class="select2 cheque_text">
                                                            <option value="all" <?php echo ($cheque_text=='all') ? "selected" : ""; ?> >All</option>
                                                            <option value="1" <?php echo ($cheque_text=='1') ? "selected" : ""; ?> >Receivable</option>
                                                            <option value="2" <?php echo ($cheque_text=='2') ? "selected" : ""; ?> >Payment</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3" style="padding-right: 3px;">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <input type="submit" name="submit" value="Reset" id="reset" class="btn btn-success btn-warning" />&nbsp;&nbsp;<input type="submit" name="submit" value="Search" class="btn btn-success btn-primary"/>
                                                            <input type="submit" name="submit" value="excel" class="submit_excel" style="margin-left: 10px;"/><input type="submit" name="submit" value="pdf" class="submit_pdf" style="margin-left: 10px;"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover" id="dt_basic" width="100%">
                                        <thead>			                
                                            <tr>
                                                <th width="5%" data-class="expand">S.No</th>
                                                <th width="17%" data-hide="phone"><i class="fa fa-fw fa-calendar text-muted hidden-md hidden-sm hidden-xs"></i> <?php echo ($is_cheque=='cheque') ? "Cheque" : "Transaction";  ?> Date</th>
                                                <th width="30%" ><i class="fa fa-fw fa-bookmark-o text-muted hidden-md hidden-sm hidden-xs"></i> Description</th>
                                                <th width="15%" data-hide="phone,tablet"><i class="fa fa-fw fa-money text-muted hidden-md hidden-sm hidden-xs"></i> Credit</th>
                                                <th width="15%" data-hide="phone,tablet"><i class="fa fa-fw fa-money text-muted hidden-md hidden-sm hidden-xs"></i> Debit</th>
                                                <th width="17%" data-hide="phone"><i class="fa fa-fw fa-money text-muted hidden-md hidden-sm hidden-xs"></i> Balance</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php if (!empty($banking_rept)) {
											$sno = 0;
											$balance_amt = 0;
											foreach ($banking_rept as $key => $row) { ?>
												<?php $balance_amt += $row->credit_amt;?>
												<?php $balance_amt -= $row->debit_amt;?>
                                                <tr>
                                                    <td><?php echo ++ $sno; ?></td>
                                                    <td><?php echo $row->created_on_str; ?></td>
                                                    <td><?php echo text($row->trns_type); ?></td>
                                                    <td align="right"><?php echo number_format($row->credit_amt, 2); ?></td>
                                                    <td align="right"><?php echo number_format($row->debit_amt, 2); ?></td>
                                                    <td align="right"><?php echo number_format($balance_amt, 2); ?></td>
                                                </tr>
                                        <?php } } ?>
                                                       
                                        </tbody>
                                    </table>
                                </div>
                                <!-- end widget content -->
                            </div>
                        </div> 
                    </div>
                </article>
                <!-- /.row -->
            </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function () {
        
        checkbox_fun();
        
        $('#is_cheque').change(function () {
            checkbox_fun();
        });
        
        $('#reset').click(function () {
            //location.reload();
        });
        /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            'iDisplayLength': 25,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            }
        });

        /* END BASIC */
        $('#date_alloc').daterangepicker({
            autoUpdateInput: false,
            opens: 'center',
            locale: {
                format: 'MM/DD/YYYY h:mm A',
                cancelLabel: 'Cancel'
            },
            ranges: {
                'ALL': [moment().subtract(2, 'days'), moment()],
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            if (label.toLowerCase() == 'all') {
                $('#date_alloc').val('');
                return false;
            }
            // getResults();
        });

        if ($('#date_alloc').length) {
            if ($('#date_alloc').val() == '') {
                $('.daterangepicker .ranges li').each(function (key, element) {
                    if ($(element).html().toLowerCase() == 'all') {
                        $(element).addClass('active');
                    }
                    else {
                        $(element).removeClass('active');
                    }
                });
            }
        }

        $(function () {
            $('#search_date').daterangepicker({format: 'DD/MM/YYYY'});
        });
    });
    
    function checkbox_fun(){
        if($("#is_cheque").is(':checked'))
            $('.cheque_text_div').show();  
        else
            $('.cheque_text_div').hide();
    }
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/customer.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/custom.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datepicker/daterangepicker.js"></script>