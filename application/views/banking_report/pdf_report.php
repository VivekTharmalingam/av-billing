<table border="0" cellpadding="3" cellspacing="0" style="font-family: calibri; font-size:14px; width: 1000px;">
    <?php echo $head; ?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>

<table width="100%" border="1" style="font-family: calibri; border-collapse:collapse;" cellpadding="3">
	<thead>
		<tr style="font-size:16px;">
			<th width="7%" > S.No</th>
			<th width="18%" > Transaction Type</th>
                        <th width="17%" > <?php echo ($is_cheque=='cheque') ? "Cheque" : "Transaction";  ?> Date</th>
                        <th width="13%" > Date</th>
                        <th width="15%" > Credit</th>
                        <th width="15%" > Debit</th>
                        <th width="15%" > Balance</th>
		</tr>
	</thead>
        <tbody>
	<?php if (!empty($banking_rept)) { 
            $balance_amt = 0;
            foreach ($banking_rept as $key => $row) { ?>
                <?php $balance_amt += $row->credit_amt;?>
                <?php $balance_amt -= $row->debit_amt;?>
              <tr>
                  <td><?php echo ++$key; ?></td>
                  <td><?php echo text($row->trns_type); ?></td>
                  <td><?php echo $row->created_on_str; ?></td>
                  <td><?php echo $row->date_str; ?></td>
                  <td align="right"><?php echo number_format($row->credit_amt, 2); ?></td>
                  <td align="right"><?php echo number_format($row->debit_amt, 2); ?></td>
                  <td align="right"><?php echo number_format($balance_amt, 2); ?></td>
              </tr>
      <?php } } else { ?>
              <tr>
                  <td colspan="7" align="center">No Record Found</td>
              </tr>
      <?php } ?>   
      </tbody>        
</table>