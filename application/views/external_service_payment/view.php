<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo base_url(); ?>external_service_payments/" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-file-text-o"></i> </span>
                            <h2>Exchange Order Payment View </h2>	
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body">
                                <table id="user" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                        <tr>
                                            <td><b>Service Partner Name</b></td>
                                            <td><?php echo text($external_service->name); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;"><b>External Service No.</b></td>
                                            <td style="width:65%"><?php echo text($external_service->exter_service_no); ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Issue Date</b></td>
                                            <td><?php echo text($external_service->issued_date_str); ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>External Service Amount</b></td>
                                            <td>$ <?php echo number_format($external_service->exter_amount, 2); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> 
                            <!-- end widget content -->
                            <div class="row">
                                <section class="col col-6 clr">

                                    <label style=" font-weight: bold; text-align: left; font-size: 15px; color: #A90329;">&nbsp;&nbsp;&nbsp;Payment Details</label>
                                    <br/>
                                </section>
                            </div>
                            <div class="box-body table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="10%">S.No</th>
                                            <th width="15%">Payment Date</th>
                                            <th width="25%">Payment Type</th>
                                            <th width="15%">Paid Amount</th>
                                            <th width="15%">Balance Amount</th>
                                            <th width="15%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (count($external_service_payment) > 0) {
                                            $balance_amount = $external_service->exter_amount;
                                            foreach ($external_service_payment as $key => $row) {
                                                $balance_amount -= $row->pay_amt;
                                                ?>
                                                <tr>
                                                    <td><?php echo ++$key; ?></td>
                                                    <td><?php echo $row->payment_date; ?></td>
                                                    <td>
                                                        <?php echo text($row->type_name); ?>
                                                        <?php if ($row->is_cheque == '1') { ?>
                                                            <div class="btn-group">
                                                                <a href="javascript:void(0);" style="text-decoration: none;" class="dropdown-toggle" data-toggle="dropdown" rel="popover" data-placement="top" data-content="<?php //echo $toolboxtxt;  ?>" data-html="true"><i class="fa fa-info-circle" ></i></a>
                                                                <ul class="dropdown-menu" role="menu">
                                                                    <li style="width: 300px;">
                                                                        <label>Bank Name&nbsp;:&nbsp;</label><?php echo text($row->pay_pay_bank); ?>
                                                                    </li>
                                                                    <li style="width: 300px;">
                                                                        <label>Cheque No&nbsp;:&nbsp;</label><?php echo text($row->pay_pay_no); ?>
                                                                    </li>
                                                                    <li style="width: 300px;">
                                                                        <label>Cheque Date&nbsp;:&nbsp;</label><?php echo text_date($row->pay_pay_date, 'd/m/Y'); ?>
                                                                    </li> 
                                                                </ul>
                                                            </div>
                                                        <?php } ?>
                                                    </td>
                                                    <td align="right"><?php echo number_format($row->pay_amt, 2); ?></td>
                                                    <td align="right"><?php echo number_format($balance_amount, 2); ?></td>
                                                    <td align="center">
                                                            <a class="label btn btn-danger pop_up_confirm delete smart-mod-delete" href="<?php echo $delete_link . '/' . $row->id ?>" data-bindid="<?php echo $row->id; ?>" data-bindtext="<?php echo $row->pay_amt; ?>">
                                                                <span>Delete</span>
                                                            </a>
                                                    </td>
                                                </tr>
                                        <?php } ?>
                                        <td colspan="4" align="right">TOTAL PAID AMOUNT</td>
                                        <td align="right"><?php echo number_format($external_service->exter_amount - $balance_amount, 2); ?></td>
                                        <td></td>
                                        <?php } else { ?>
                                        <tr>
                                            <td colspan="6" align="center">No Record Found</td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end widget div -->
                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript">
    $(document).ready(function () {
        /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;
        var responsiveHelper_datatable_fixed_column = undefined;
        var responsiveHelper_datatable_col_reorder = undefined;
        var responsiveHelper_datatable_tabletools = undefined;

        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            }
        });

        /* END BASIC */
        $(".smart-mod-delete").click(function (e) {
            var $bindId = $(this).data('bindid');
            var $bindText = $(this).data('bindtext');
            $.SmartMessageBox({
                title: "Are you sure?",
                content: "You will not be able to recover this Exchange Order Payment Details!",
                buttons: '[No][Yes]'
            }, function (ButtonPressed) {
                if (ButtonPressed === "Yes") {
                    window.location.replace(baseUrl + "external_service_payments/delete/" + $bindId);
                }
                if (ButtonPressed === "No") {
                    $.smallBox({
                        title: "Sorry ! ...",
                        content: "<i class='fa fa-info-circle'></i> <i>You just selected NO...<br>So The action of DELETE is disallowed for the " + $bindText + " details.. </i>",
                        color: "#C46A69",
                        iconSmall: "fa fa-times fa-2x fadeInRight animated",
                        timeout: 4000
                    });
                }
            });
            e.preventDefault();
        })
    });
</script>