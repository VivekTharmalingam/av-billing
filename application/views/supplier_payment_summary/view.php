<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                     <a href="<?php echo base_url(); ?>supplier_payment_summary/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>Supplier Payment Summary - View</h2>
							<span class="widget-toolbar" rel="tooltip" data-placement="bottom" data-original-title="Print Pdf">
                                <a href="<?php echo base_url(); ?>supplier_payment_summary/pdf/<?php echo $id;?>/<?php echo $from_date; ?>/<?php echo $to_date; ?>" style="color:#fff;">
                                    <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                </a>
                            </span>
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body"> 
                                <span id="watermark"><?php echo WATER_MARK; ?></span>
                                 <table id="user" class="table table-bordered table-striped" style="clear: both;">
                                    <tbody>
                                        <tr>
                                            <td style="width:35%;"><b>Supplier Name</b></td>
											<td style="width:65%;"><?php echo text($supplier->name);?></td>
										</tr>	
                                        <tr>
                                            <td><b>Contact Name</b></td>
                                            <td><?php echo text($supplier->contact_name);?></td>
                                        </tr>                                        
                                        <tr>
                                            <td><b>Phone No</b></td>
                                            <td><?php echo text($supplier->phone_no);?></td>
                                        </tr>
										<tr>
                                            <td><b>Email</b></td>
                                            <td><?php echo text($supplier->email_id);?></td>
                                        </tr>
										<tr>
                                            <td><b>Address</b></td>
                                            <td><?php echo nl2br($supplier->address);?></td>
                                        </tr>
										<tr>
                                            <td><b>Total Invoice Amount</b></td>
                                            <td><?php echo number_format($supplier->po_amt, 2);?></td>
                                        </tr>
										<tr>
                                            <td><b>Total Paid Amount</b></td>
                                            <td><?php echo number_format($supplier->paid_amt, 2);?></td>
                                        </tr>
                                    </tbody>
                                </table>

								<div class="text-left">
									<span class="onoffswitch-title">
									  <h3><b style="color: #8e233b;">Payment Details</b></h3>
									</span>
								</div>
								
								<div id="widget-tab-1" class="table-responsive">
									<table id="dt_basic_2" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
										<thead>
											<tr>
												<th width="5%" data-class="expand" style="font-weight: bold; text-align:center;">S.No</th>
												<th width="12%" data-hide="phone,tablet" style="text-align:center;">Invoice No.</th>
												<th width="12%" data-hide="phone,tablet" style="text-align:center;">Invoice Date</th>
												<!--<th width="15%" data-hide="phone,tablet" style="text-align:center;">GR No.</th>-->
												<th width="25%" data-hide="phone,tablet" style="text-align:center;">Payment Type</th>
												<th width="15%" data-hide="phone,tablet" style="text-align:center;">Amount (S$)</th>
												<th width="15%" data-hide="phone,tablet" style="text-align:center;">Paid Amount (S$)</th>
												<th width="16%" data-hide="phone,tablet" style="text-align:center;">Balance Amt (S$)</th>
											</tr>
										<thead>
										<tbody>
										   <?php foreach($purchase as $key=>$val){?>
												<tr>
													<td align="center"><?php echo ++$key;?></td>
													<td><?php echo text($val->supplier_invoice_no);?></td>
													<td align="center"><?php echo text_date($val->invoice_date);?></td>
													<!--<td><?php echo text($val->pr_code);?></td>-->
													<td><?php echo text($val->type_name);?></td>
													<td align="right"><?php echo number_format($val->pr_net_amount, 2);?></td>
													<td align="right"><?php echo number_format($val->paid_amt, 2);?></td>
													<td align="right"><?php echo number_format($val->pr_net_amount - $val->paid_amt, 2);?></td>
												</tr>
										   <?php } ?>
										</tbody>
									</table>
                            </div>
                        </div>
                        <!-- end widget div -->
                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->