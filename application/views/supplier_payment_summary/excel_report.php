<?php
$ti=time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Supplier_Payment_Summary_Report".$ti.".xls");
header("Pragma: no-cache");
header("Expires: 0");
#echo "<pre>";print_r($item);echo "</pre>";
#echo "</br> : ".count($item);


?>
<table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; margin-left:10px">
	<tr style="font-family:Times new roman; font-weight:bold;">
		<th width="25%">Supplier Name</th>
		<th width="35%">Address</th>
		<th width="15%">Total Amount</th>
		<th width="15%">Paid Amount</th>
		<th width="15%">Balance Amount</th>
	</tr>
	<?php if (count($purchase)) {?>
	<?php foreach($purchase as $key => $row) {?>                    
		<tr>
			<td style="padding:8px;"><?php echo text($row->name);?></td>
			<td style="padding:8px;"><?php echo text($row->address);?></td>
			<td style="padding:8px;" align="right"><?php echo number_format($row->po_amt, 2); ?></td>
			<td style="padding:8px;" align="right"><?php echo number_format($row->paid_amt, 2); ?></td>
			<td style="padding:8px;" align="right"><?php echo number_format($row->po_amt - $row->paid_amt, 2); ?></td>
		</tr>
	<?php }?>
	<?php } else {?>
		<tr>
			<td colspan="5" align="center">No Record Found</td>
		</tr>
	<?php }?>
</table>