<?php //print_r($from_date);exit;?>
<table border="0" cellpadding="3" cellspacing="0" style="font-family:calibri; font-size:14px; width: 1000px;">
    <?php echo $head;?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>
<table width="100%" border="1" style="font-family:calibri; border-collapse:collapse;" cellpadding="3">
	<tr style="font-family:calibri; font-size:16px; font-weight:bold;">
		<th width="5%">S.NO</th>
		<th width="27%">Supplier Name</th>
		<th width="30%">Address</th>
		<th width="12%">Total Amt</th>
		<th width="12%">Paid Amt</th>
		<th width="14%">Balance Amt</th>
	</tr>
	<?php if (count($purchase)) {?>
	<?php foreach($purchase as $key => $row) {?>                    
		<tr>
			<td><?php echo ++ $key;?></td>
			<td><?php echo strtoupper(text($row->name));?></td>
			<td><?php echo strtoupper(text($row->address));?></td>
			<td align="right"><?php echo number_format($row->po_amt, 2); ?></td>
			<td align="right"><?php echo number_format($row->paid_amt, 2); ?></td>
			<td align="right"><?php echo number_format($row->po_amt - $row->paid_amt, 2); ?></td>
		</tr>
	<?php }?>
	<?php } else {?>
		<tr>
			<td colspan="6" align="center">No Record Found</td>
		</tr>
	<?php }?>
</table>