<?php //print_r($from_date);exit;?>
<table border="0" cellpadding="3" cellspacing="0" style="font-family:calibri; font-size:14px; width: 1000px;">
    <?php echo $head;?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
	
	<tr>
		<td><b>Supplier Name</b></td>
		<td>:  <?php echo text($supplier->name);?></td>
	</tr>	
	<tr>
		<td><b>Contact Name</b></td>		
		<td>:  <?php echo text($supplier->contact_name);?></td>
	</tr>                                        
	<tr>
		<td><b>Phone No</b></td>
		<td>:  <?php echo text($supplier->phone_no);?></td>
	</tr>
	<tr>
		<td><b>Email</b></td>
		<td>: <?php echo text($supplier->email_id);?></td>
	</tr>
	<tr>
		<td><b>Address</b></td>
		<td>:  <?php echo text($supplier->address);?></td>
	</tr>
	<tr>
		<td><b>Total Invoice Amount</b></td>
		<td>:  <?php echo number_format($supplier->po_amt, 2);?></td>
	</tr>
	<tr>
		<td><b>Total Paid Amount</b></td>
		<td>:  <?php echo number_format($supplier->paid_amt, 2);?></td>
	</tr>
	
	<tr>
        <td colspan="2" height="20"></td>
    </tr>
</table>
<table width="100%" border="1" style="font-family: calibri; border-collapse:collapse; font-size:15px;" cellpadding="3">
	<tr>
		<th width="5%">S.NO</th>
		<th width="20%">Invoice No</th>
		<th width="15%">Invoice Date</th>
		<th width="15%">GR No.</th>
		<th width="15%">Amount</th>
		<th width="15%">Paid Amt</th>
		<th width="15%">Bal Amt</th>
	</tr>
	<?php if (count($purchase)) {?>
	<?php foreach($purchase as $key => $val) {?>                    
		<tr>
			<td><?php echo ++ $key;?></td>
			<td><?php echo text($val->supplier_invoice_no);?></td>
			<td><?php echo text_date($val->invoice_date);?></td>
			<td><?php echo text($val->pr_code);?></td>
			<td align="right"><?php echo number_format($val->pr_net_amount, 2);?></td>
			<td align="right"><?php echo number_format($val->paid_amt, 2);?></td>
			<td align="right"><?php echo number_format($val->pr_net_amount - $val->paid_amt, 2);?></td>
		</tr>
	<?php }?>
	<?php } else {?>
		<tr>
			<td colspan="7" align="center">No Record Found</td>
		</tr>
	<?php }?>
</table>