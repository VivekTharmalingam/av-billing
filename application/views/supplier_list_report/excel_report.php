<?php
$ti=time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Supplier_list_report".$ti.".xls");
header("Pragma: no-cache");
header("Expires: 0");
#echo "<pre>";print_r($item);echo "</pre>";
#echo "</br> : ".count($item);


?>
<table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; margin-left:10px">
	<tr style="font-family:Times new roman;font-size:16px;font-weight:bold;">
		<th width="6%" style="padding:10px">S.NO</th>
		<th width="20%">Supplier Name</th>
		<th width="20%">Contact Name</th>
		<th width="15%">Phone No</th>
		<th width="12%">Email</th>
		<th width="25%">Address</th>
	</tr>
	<?php if (count($supplier)) {?>
	<?php foreach($supplier as $key => $row) {?>                    
		<tr>
			<td style="padding:8px;" ><?php echo ++ $key;?></td>
			<td style="padding:8px;"><?php echo text($row->name);?></td>
			<td style="padding:8px;"><?php echo text($row->contact_name);?></td>
			<td style="padding:8px;"><?php echo text($row->phone_no);?></td>
			<td style="padding:8px;"><?php echo text($row->email_id);?></td>
			<td style="padding:8px;"><?php echo nl2br($row->address);?></td>
		</tr>
	<?php }?>
	<?php } else {?>
		<tr>
			<td colspan="6" align="center">No Record Found</td>
		</tr>
	<?php }?>
</table>