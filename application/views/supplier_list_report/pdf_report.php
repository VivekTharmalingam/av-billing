<?php //print_r($from_date);exit;?>
<table border="0" cellpadding="3" cellspacing="0" style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:14px; width: 1000px;">
    <?php echo $head;?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>
<table width="100%" border="1" style="border-collapse:collapse; margin-left:10px">
	<tr style="font-family:Times new roman;font-size:16px;font-weight:bold;">
		<th width="6%" style="padding:10px">S.NO</th>
		<th width="20%">Supplier Name</th>
		<th width="20%">Contact Name</th>
		<th width="15%">Phone No</th>
		<th width="12%">Email</th>
		<th width="25%">Address</th>
	</tr>
	<?php if (count($supplier)) {?>
	<?php foreach($supplier as $key => $row) {?>                    
		<tr>
			<td style="padding:8px;" ><?php echo ++ $key;?></td>
			<td style="padding:8px;"><?php echo strtoupper(text($row->name));?></td>
			<td style="padding:8px;"><?php echo strtoupper(text($row->contact_name));?></td>
			<td style="padding:8px;"><?php echo text($row->phone_no);?></td>
			<td style="padding:8px;"><?php echo text($row->email_id);?></td>
			<td style="padding:8px;"><?php echo strtoupper(nl2br($row->address));?></td>
		</tr>
	<?php }?>
	<?php } else {?>
		<tr>
			<td colspan="6" align="center">No Record Found</td>
		</tr>
	<?php }?>
</table>