<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if(!empty($success)) {?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                            <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                            <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php }else if(!empty($error)) {?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error;?>
                    </div>
                <?php }?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
					<a href="<?php echo base_url(); ?>payment_type/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
					
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
								<h2><?php echo $page_title;?></h2>
                            </span>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form action="<?php echo $form_action;?>" method="post" id="payment_type">
                                <input type="hidden" name="type_id" id="type_id" value="<?php echo $payment_type->id;?>" />
                            <!-- widget content -->
                            <div class="widget-body no-padding">
                                <div class="smart-form">                                   
                                    <fieldset>
									<legend style=" font-size: 12px; color: red;">
										<span style="color: red">*</span>Fields are mandatory
									</legend>
											
                                        <div class="row">                                       
                                        <section class="col col-6">
                                            <label class="label">Payment Type <span style="color: red">*</span></label>
                                            <label class="input"> <i class="icon-append fa fa-list-alt"></i>
                                                 <input type="text" data-placeholder="Cash" name="type_name" id="type_name" maxlength="150" value="<?php echo $payment_type->type_name;?>" />
                                            </label>
                                        </section>
                                        </div>
                                        
                                        <div class="row">
                                        <section class="col col-6">
                                            <label class="label">Is Cheque </label>
                                            <div class="inline-group">
                                                <label class="radio">
                                                    <input type="radio" name="is_cheque" value="0" <?php echo ($payment_type->is_cheque == '0') ? 'checked' : ''; ?> /><i></i> No
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" name="is_cheque" value="1" <?php echo ($payment_type->is_cheque == '1') ? 'checked' : ''; ?> /><i></i> Yes
                                                </label>
                                            </div>
                                        </section>
                                        </div>
										
										<div class="row">
                                            <section class="col col-6">
                                                <label class="label">Status<span style="color: red">*</span></label>
                                                <label class="select">
                                                   <select name="status" id="status" class="select2">
                                                    <option value="1" <?php echo ($payment_type->status == '1') ? 'selected' : ''; ?>>Active</option>
                                                    <option value="2"<?php echo ($payment_type->status == '2') ? 'selected' : ''; ?>>Inactive</option>
                                                </select></label>
                                            </section>
										</div>
                                      
                                    </fieldset>
                                    
                                    <footer>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn btn-default">Cancel</button>
                                    </footer>
                                </div>
                            </div>
                            <!-- end widget content -->
                            </form> 
                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/app/payment_type.js"></script>