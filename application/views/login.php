<!DOCTYPE html>
<html class="bg-myclr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo $title; ?></title>
        <?php
        //echo base_url() . DIR_FAVICON . $fav; 
        //echo DIR_FAVICON; exit;
        if ((!empty($company->comp_favicon)) && (file_exists(FCPATH . DIR_FAVICON . $company->comp_favicon))) {
            $favicon_url = base_url() . DIR_FAVICON . $company->comp_favicon;
        } else {
            $favicon_url = base_url() . 'assets/images/favicon/favicon.ico';
        }
        //echo $favicon_url; exit;
        ?>
        <link rel="shortcut icon" href="<?php echo $favicon_url; ?>" type="image/x-icon">
        <!--<link rel="shortcut icon" href="<?php echo BASE_URL; ?>assets/images/favicon/faviconuc.ico" type="image/x-icon">-->

        <style type="text/css">
            * {padding:0; margin:0;}
            body {font-family: "Open Sans",Arial,Helvetica,Sans-Serif; font-size:14px;}
            .login_div {margin:7% auto; width:370px; height:310px; border:5px solid #f4f4f4; border-radius:10px; box-shadow:0px 0px 10px 0px rgba(0,0,0,0.2); padding:2% 4%; z-index: 1000;}
            .login_div h1 {text-align:center; margin-bottom:5%;}
            .username_div {float:left; width:98%;}
            .username_div input {position: relative; z-index: 1000; width:100%; height:30px; padding:5px; border-radius:5px; box-shadow:0px 0px 6px 0px rgba(0,0,0,0.2); border:1px solid #F4F4F4; font-size:14px;}
            .password_div {float:left; width:98%; margin-top:20px;}
            .password_div input {position: relative; z-index: 1000; width:100%; height:30px; padding:5px; border-radius:5px; box-shadow:0px 0px 6px 0px rgba(0,0,0,0.2); border:1px solid #F4F4F4; font-size:14px;}
            .submit_div {float:right; width:100%; margin-top:15px; text-align: center;}
            .submit_div button {/*float:right;*/ width:100px; height:50px; font-size:14px;background: #E3E3E3; /* For browsers that do not support gradients */
                background: -webkit-linear-gradient(#E3E3E3, #F7F7F7); /* For Safari 5.1 to 6.0 */
                background: -o-linear-gradient(#E3E3E3, #F7F7F7); /* For Opera 11.1 to 12.0 */
                background: -moz-linear-gradient(#E3E3E3, #F7F7F7); /* For Firefox 3.6 to 15 */
                background: linear-gradient(#E3E3E3, #F7F7F7); /* Standard syntax */
                border:1px solid #f4f4f4; border-radius:5px; cursor:pointer; position: relative; z-index: 1000;}
            .submit_div button:hover {background: #E3E3E3; /* For browsers that do not support gradients */
                                      background: -webkit-linear-gradient(#F7F7F7, #E3E3E3); /* For Safari 5.1 to 6.0 */
                                      background: -o-linear-gradient(#F7F7F7, #E3E3E3); /* For Opera 11.1 to 12.0 */
                                      background: -moz-linear-gradient(#F7F7F7, #E3E3E3); /* For Firefox 3.6 to 15 */
                                      background: linear-gradient(#F7F7F7, #E3E3E3);}
            .submit_div a {float:right; position: relative; z-index: 1000; margin:18px; color:#000; text-decoration:none;}
            .submit_div a:hover {text-decoration:underline;}
            .footer {width:100%; position:absolute; bottom:0;}
            .footer_img {float:left; width:100%; opacity:.5; margin-top:-130px;}
            .footer_img img {background-size:contain; width:100%;}

            @media only screen and (max-width:900px){
                .login_div {width:80%; margin:12% auto;}
                .footer_img {margin-top:0px;}
            }
            @media only screen and (max-width:340px){  
                .login_div h1 img {width: 220px;}
            }

            .error_container {
                float: left;
                display: block;
                text-align: center;
                color: red;
                width: 100%;
            }
        </style>
        <script src="<?php echo BASE_URL; ?>assets/js/jquery.min.js"></script>
        <script type="text/javascript">
            function form_reset() {
                document.getElementById("wrapper_container").innerHTML = "";
            }

            function validate() {
                var emailID = document.getElementById("userid").value;
                atpos = emailID.indexOf("@");
                dotpos = emailID.lastIndexOf(".");
                if (document.getElementById("userid").value == "") {
                    document.getElementById("wrapper_container").innerHTML = '<p style="font-size:15px; color:red; width:100%; float: left; text-align: center; margin-top: 7px;">Please enter your Email Address / User Name</p>';
                    document.getElementById("userid").focus();
                    return false;
                }
                /* else if (atpos < 1 || ( dotpos - atpos < 2 )) {
                 document.getElementById("wrapper_container").innerHTML = "Invalid Email Address / User Name";
                 document.getElementById("userid").focus();
                 return false;
                 } */

                if (document.getElementById("password").value == "") {
                    document.getElementById("wrapper_container").innerHTML = '<p style="font-size:15px; color:red; width:100%; float: left; text-align: center; margin-top: 7px;">Please enter your Password</p>';
                    document.getElementById("password").focus();
                    return false;
                }

                return true;
            }
        </script>
    </head>

    <body>
        <form action="<?php echo $form_action; ?>" method="post" class="user" onSubmit="return validate()">
            <div class="login_div">
                <h1>
<?php
if ((!empty($company->login_logo)) && (file_exists(FCPATH . DIR_COMPANY_LOGO . $company->login_logo))) {
    $image_url = base_url() . DIR_COMPANY_LOGO . $company->login_logo;
} else {
    $image_url = base_url() . 'assets/images/logo.png';
}
?>
                    <img src="<?php echo $image_url; ?>" style="width: 110px;" />
                </h1>
                <div class="username_div">
                    <input type="text" name="user_name" id="userid" placeholder="Enter Your UserName OR Email ID" autofocus />
                </div>
                <div class="password_div">
                    <input type="password" name="password" id="password" placeholder="Enter Your Password" />
                </div>
                <div id="wrapper_container" class="error_container">
<?php if ($this->session->flashdata('message')) { ?>
                        <p style="font-size:15px; color:red; width:100%; float: left; text-align: center; margin-top: 7px;"><?php echo $this->session->flashdata('message'); ?></p>
                    <?php } ?>
                </div>
                <div class="submit_div">
                    <button type="reset" value="Login" style="margin-right:10px;">Reset</button>
                    <button type="submit" value="Login">Sign in</button>
                    <!--                <a href="#">Forget Password?</a>-->
                </div>
            </div>
            <div class="footer">
                <!--<div class="footer_img"><img src="<?php echo BASE_URL ?>assets/images/login/m_footer.png" /></div>-->
            </div>
        </form>
    </body>

</html>