<script>
    window.localStorage.clear();
</script>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">&nbsp;</div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="widget-body">
                        <!-- Widget ID (each widget will need unique ID)-->                                
                        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false"	>

                            <header><span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                <h2>Service Report</h2>
                            </header>                                  
                            <div>
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="box-body table-responsive">
                                            <div class="date-range"></div>
                                                <form name="order_search" action="<?php echo $form_action; ?>" method="post" class="search_form validate_form">
                                                    <div class="col-xs-12">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label style="width:28%; padding-top: 5px;"></label>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                    <input type="text" class="search_date" data-placeholder="dd/mm/yyyy - dd/mm/yyyy" id="date_alloc" style="padding: 6px 12px;margin-right:5%; width: 100%;"  id="search_date" name="search_date" value="<?php echo (!empty($search_date)) ? $search_date:''; ?>" autocomplete="off" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label style="width:28%; padding-top: 5px;"></label>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-male"></i>
                                                                    </div>
                                                                    <select name="customer" class="select2 customer">
                                                                        <option value="">Customer - ALL</option>
                                                                        <?php foreach($customer_list as $key => $customer) {?>
                                                                                <option value="<?php echo $customer['id'];?>" <?php echo ($customer['id'] == $customer_id)? 'selected' : '';?>><?php echo $customer['name'];?></option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label style="width:28%; padding-top: 5px;"></label>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-user-circle-o"></i>
                                                                    </div>
                                                                    <select name="engineer" class="select2 engineer">
                                                                        <option value="">Engineer - ALL</option>
                                                                        <?php foreach($users as $key => $user) {?>
                                                                                <option value="<?php echo $user->id;?>" <?php echo ($user->id == $engineer_id)? 'selected' : '';?>><?php echo $user->name;?></option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label style="width:28%; padding-top: 5px;"></label>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-building"></i>
                                                                    </div>
                                                                    <select name="branch" class="select2 branch">
                                                                        <option value="">Branch - ALL</option>
                                                                        <?php foreach($branches as $key => $branch) {?>
                                                                                <option value="<?php echo $branch->id;?>" <?php echo ($branch->id == $branch_id)? 'selected' : '';?>><?php echo $branch->branch_name;?></option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label style="width:28%; padding-top: 5px;"></label>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </div>
                                                                    <select name="status" class="select2 status">
                                                                        <option value="">Type - All</option>
                                                                        <option value="1" <?php echo ($type == 1)?'selected':'';?>>Processing</option>
                                                                        <option value="2" <?php echo ($type == 2)?'selected':'';?>>Completed</option>
																		<option value="3" <?php echo ($type == 3)?'selected':'';?>>Delivered</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label style="width:28%; padding-top: 5px;"></label>
                                                                <div class="input-group">
                                                                    <input type="submit" name="submit" value="Reset" id="reset" class="btn btn-success btn-warning" />&nbsp;&nbsp;<input type="submit" name="submit" value="Search" class="btn btn-success btn-primary"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1" style="float: right;" >
                                                            <div class="form-group">
                                                                <label style="width:28%; padding-top: 5px;"></label>
                                                                <div class="input-group">
                                                                    <input type="submit" name="submit" value="pdf" class="submit_pdf" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1" style="float: right;">
                                                            <div class="form-group">
                                                                <label style="width:28%; padding-top: 5px;"></label>
                                                                <div class="input-group">
                                                                    <input type="submit" name="submit" value="excel" class="submit_excel" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                    <table id="dt_basic1" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>			                
                                            <tr>
                                                <th style="width: 5%;" data-hide="phone" data-class="expand">S.No</th>
                                                <th style="width: 12%;" ><i class="fa fa-fw fa-key txt-color-blue hidden-md hidden-sm hidden-xs"></i>JobSheet No.</th>
                                                <th style="width: 9%;" data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i>Rec Date</th>
												<th style="width: 18%;" data-hide="phone"><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i>Customer</th>
                                                <th style="width: 12%;" data-hide="phone"><i class="fa fa-fw fa-user-secret txt-color-blue hidden-md hidden-sm hidden-xs"></i>Engineer </th>
                                                <th style="width: 9%;" data-hide="phone,tablet"><i class="fa fa-fw fa-warning txt-color-blue hidden-md hidden-sm hidden-xs"></i>Status</th>
                                                <th style="width: 11%;" data-hide="phone,tablet"><i class="fa fa-fw fa-dollar txt-color-blue hidden-md hidden-sm hidden-xs"></i>Service Amt</th>
                                                <th style="width: 10%;" data-hide="phone,tablet"><i class="fa fa-fw fa-dollar txt-color-blue hidden-md hidden-sm hidden-xs"></i> Our Cost</th>
												<th style="width: 10%;" data-hide="phone,tablet"><i class="fa fa-fw fa-dollar txt-color-blue hidden-md hidden-sm hidden-xs"></i> Item Profit</th>
                                                <th style="width: 9%;" data-hide="phone,tablet"><i class="fa fa-fw fa-dollar txt-color-blue hidden-md hidden-sm hidden-xs"></i> Inv Amt</th>
                                                <th style="width: 5%;" style="text-align: center;"><i class="txt-color-blue hidden-md hidden-xs"></i> Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(count($service_report) > 0) { ?>
                                            <?php
												$total_service_amt = 0;
												$total_our_cost = 0;
												$total_item_profit = 0;
                                                $total_inv_amount = 0;
                                            foreach ($service_report as $key => $row) {
													$total_service_amt += $row->service_cost;
													$total_our_cost += $row->our_cost;
													$total_item_profit += $row->item_profit;
													$total_inv_amount += $row->total_amt;
                                                ?>
                                                <tr>
                                                    <td><?php echo ++$key; ?></td>
                                                    <td><?php echo text($row->jobsheet_number); ?></td>
                                                    <td><?php echo text($row->received_date_str); ?></td>
                                                    <td><?php echo text($row->cust_name); ?></td>
                                                    <td><?php echo text($row->eng_name); ?></td>
                                                    <td><?php echo text($row->service_status); ?></td>
                                                    <td align="right"><?php echo text_amount($row->service_cost); ?></td>
                                                    <td align="right"><?php echo text_amount($row->our_cost);?></td>
                                                    <td align="right"><?php echo text_amount($row->item_profit);?></td>
                                                    <td align="right"><?php echo text_amount($row->total_amt); ?></td>
                                                    <td align="center">
                                                        <?php if (in_array(1, $permission)) { ?>
                                                            <a class="label btn btn-warning view" href="<?php echo $view_link . $row->srId ?>"><span>View</span></a>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                                <tr>
                                                    <td style="text-align: right;" colspan="6">Total</td>
                                                    <td align="right"><?php echo text_amount($total_service_amt); ?></td>
                                                    <td align="right"><?php echo text_amount($total_our_cost); ?></td>
                                                    <td align="right"><?php echo text_amount($total_item_profit); ?></td>
                                                    <td align="right"><?php echo text_amount($total_inv_amount); ?></td>
                                                    <td></td>
                                                </tr>
                                            <?php  } else { ?>
                                                    <tr>
                                                        <td style="text-align: center;" colspan="11">NO DATA AVAILABLE IN TABLE</td>
                                                    </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- end widget content -->
                            </div>
                        </div> 
                    </div>
                </article>
                <!-- /.row -->
            </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function () {
        /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            'iDisplayLength': 25,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            }
        });

        /* END BASIC */
        $('#date_alloc').daterangepicker({
            autoUpdateInput: false,
            opens: 'center',
            locale: {
                format: 'MM/DD/YYYY h:mm A',
                cancelLabel: 'Cancel'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            //alert(start + '|' + end + '|' + label);
            // getResults();
        });

        $(function () {
            $('#search_date').daterangepicker({format: 'DD/MM/YYYY'});
        });
    });
</script>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/custom.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datepicker/daterangepicker.js"></script>