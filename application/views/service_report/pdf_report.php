<?php //print_r($from_date);exit;?>
<table border="0" cellpadding="3" cellspacing="0" style="font-family: calibri; font-size:14px; width: 1000px;">
    <?php echo $head;?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>
<!--                <tr style="boder-top:0px;">
                    <td align="right" width="50%">Assigned To</td>
                    <td align="right" width="50%"><?php echo text($assign_to, '--'); ?></td>
                </tr>-->
<!--                <tr style="boder-top:0px;">
                    <td align="right" width="50%">Enq Status</td>
                    <td align="right" width="50%"><?php echo text($enq_sts_str, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">Type</td>
                    <td align="right" width="50%"><?php echo text($type_str, '--'); ?></td>
                </tr>-->
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>
<table width="100%" border="1" style="font-family: calibri; border-collapse:collapse;" cellpadding="3">
	<thead>
        <tr>
            <th style="width: 5%;">S.NO</th>
            <th style="width: 12%;">JobSheet No.</th>
            <th style="width: 10%;">Received Date</th>
            <th style="width: 15%;">Customer</th>
            <th style="width: 12%;">Engineer </th>
            <th style="width: 8%;">Status</th>
            <th style="width: 12%;">Service Amount</th>
            <th style="width: 9%;">Service Cost</th>
            <th style="width: 8%;">Item Profit</th>
            <th style="width: 9%;">Invoice Amount</th>
        </tr>
	</thead>
	<tbody>
	<?php if (count($service_report)) {
		$total_service_amt = 0;
		$total_our_cost = 0;
		$total_item_profit = 0;
		$total_inv_amount = 0;
		
		foreach($service_report as $key => $row) {?>  
            <?php $total_service_amt += $row->service_cost;
				$total_our_cost += $row->our_cost;
				$total_item_profit += $row->item_profit;
				$total_inv_amount += $row->total_amt;
			?>
		<tr>
			<td><?php echo ++$key; ?></td>
			<td><?php echo text($row->jobsheet_number); ?></td>
			<td><?php echo text($row->received_date_str); ?></td>
			<td><?php echo text($row->cust_name); ?></td>
			<td><?php echo text($row->eng_name); ?></td>
			<td><?php echo text($row->service_status); ?></td>
			<td align="right"><?php echo text_amount($row->service_cost); ?></td>
			<td align="right"><?php echo text_amount($row->our_cost);?></td>
			<td align="right"><?php echo text_amount($row->item_profit);?></td>
			<td align="right"><?php echo text_amount($row->total_amt); ?></td>
		</tr>
	<?php }?>
		<tr>
			<td style="text-align: right;" colspan="6">Total</td>
			<td align="right"><?php echo text_amount($total_service_amt); ?></td>
			<td align="right"><?php echo text_amount($total_our_cost); ?></td>
			<td align="right"><?php echo text_amount($total_item_profit); ?></td>
			<td align="right"><?php echo text_amount($total_inv_amount); ?></td>
		</tr>
	<?php }
	else {?>
		<tr>
			<td colspan="10" align="center">No Record Found</td>
		</tr>
	<?php }?>
	</tbody>
</table>