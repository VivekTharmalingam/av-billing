<?php
$ti=time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Service_report".$ti.".xls");
header("Pragma: no-cache");
header("Expires: 0");
#echo "<pre>";print_r($item);echo "</pre>";
#echo "</br> : ".count($item);


?>
<table width="100%" border="1" style="border-collapse:collapse; margin-left:10px" cellpadding="5" cellspacing="0">
	<tr style="font-family: calibri; font-weight:bold;">
            <th style="width: 5%;">S.NO</th>
            <th style="width: 12%;">JobSheet No.</th>
            <th style="width: 10%;">Received Date</th>
            <th style="width: 15%;">Customer</th>
            <th style="width: 12%;">Engineer </th>
            <th style="width: 8%;">Status</th>
            <th style="width: 12%;">Service Amount</th>
            <th style="width: 9%;">Service Cost</th>
            <th style="width: 8%;">Item Profit</th>
            <th style="width: 9%;">Invoice Amount</th>
	</tr>
	<?php if (count($service_report)) {
		$total_service_amt = 0;
		$total_our_cost = 0;
		$total_item_profit = 0;
		$total_inv_amount = 0;?>
	<?php foreach($service_report as $key => $row) {?> 
        <?php $total_service_amt += $row->service_cost;
			$total_our_cost += $row->our_cost;
			$total_item_profit += $row->item_profit;
			$total_inv_amount += $row->total_amt;
		?>
            <tr>
                <td><?php echo ++$key; ?></td>
                <td><?php echo text($row->jobsheet_number); ?></td>
                <td><?php echo text($row->received_date_str); ?></td>
                <td><?php echo text($row->cust_name); ?></td>
                <td><?php echo text($row->eng_name); ?></td>
                <td><?php echo text($row->service_status); ?></td>
                <td align="right"><?php echo text_amount($row->service_cost); ?></td>
				<td align="right"><?php echo text_amount($row->our_cost);?></td>
				<td align="right"><?php echo text_amount($row->our_cost);?></td>
				<td align="right"><?php echo text_amount($row->total_amt); ?></td>
            </tr>
	<?php }?>
		<tr>
			<td style="text-align: right;" colspan="6">Total</td>
			<td align="right"><?php echo text_amount($total_service_amt); ?></td>
			<td align="right"><?php echo text_amount($total_our_cost); ?></td>
			<td align="right"><?php echo text_amount($total_item_profit); ?></td>
			<td align="right"><?php echo text_amount($total_inv_amount); ?></td>
		</tr>
	<?php } else {?>
		<tr>
			<td colspan="10" align="center">No Record Found</td>
		</tr>
	<?php }?>
</table>