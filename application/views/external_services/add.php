<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->
        <?php
        echo breadcrumb_links();
        $user_data = get_user_data();
        ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) {
                    ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>

        <form class="add_form" action="<?php echo $form_action; ?>" method="post" name="external_services" id="external_services_form" enctype="multipart/form-data">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                        <a href="<?php echo base_url(); ?>external_services/" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="List"><i class="glyphicon glyphicon-list"></i></button></a>
                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>External Services Add </h2>
                            </header>
                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" >
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                            <input type="hidden" class="form-control serv_id" name="serv_id" value="" />
                                            
                                            <div class="row">
                                                <section class="col col-6">
                                                    <section >
                                                        <label class="label">External Service No. <span style="color: red;">*</span></label>
                                                        <label class="input"><i class="icon-append fa fa-key"></i>
                                                            <input type="text" name="exter_service_no" id="exter_service_no" value="<?= $exter_service_number; ?>" readonly=""/>
                                                        </label>
                                                    </section>
                                                    <section class="sup_main_div">
                                                        <label class="label">Service Partner Name <span style="color: red;">*</span>
                                                            <span class="btn btn-primary pull-right add-user" style="margin: 0 0 2px 0;padding: 3px;" data-title="Other Service Partner Name">
                                                                <i class="fa fa-user" style="font-size: 16px;"> </i> Other Service Partner
                                                            </span>
                                                            <span class="btn btn-primary pull-right user-list" style="margin: 0 0 2px 0;padding: 3px;display: none;" data-title="Select Service Partner">
                                                                <i class="fa fa-indent" style="font-size: 16px;"> </i> Service Partner List
                                                            </span>
                                                        </label>
                                                        <label class="select sup_name_list_div">
                                                            <select name="service_partner_id" class="select2 service_partner_id" id="service_partner_id">
                                                                <option value="">Select Service Partner name</option>
                                                                <?php foreach ($service_partners as $key => $sup) { ?>
                                                                    <option value="<?php echo $sup->id; ?>"><?php echo $sup->name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                        <label class="input sup_name_new_div" style="display: none;">
                                                            <input type="text" name="service_partner_name_txt" class="form-control sup_name_new" data-placeholder="Mr. Xxxyyy" value="" />
                                                        </label>
                                                        <input type="hidden" name="service_partner_name" class="service_partner_name" value="" />
                                                    </section>
                                                    <section>
                                                        <label class="label">Issued Date <span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="issued_date" id="issued_date" class="form-control bootstrap-datepicker-comncls" placeholder="DD/MM/YYYY" value="<?= date('d/m/Y'); ?>" />
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Status </label>
                                                        <label class="select">
                                                            <select name="status" id="status" class="select2" >
                                                                <option value="1">Issued</option>
                                                                <option value="2">Received</option>
                                                            </select>  
                                                        </label>
                                                    </section> 
                                                </section> 
                                                <section class="col col-6">
                                                    <section >
                                                        <label class="label">Amount <span style="color: red;">*</span></label>
                                                        <label class="input"><i class="icon-append fa fa-money"></i>
                                                            <input type="text" class="float_value" name="exter_amount" id="exter_amount"/>
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Description <span style="color: red;">*</span></label>
                                                        <label class="textarea">
                                                            <textarea rows="3" class="show_txt_count custom-scroll exter_desc" name="exter_desc" maxlength="300"></textarea>
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Received Date </label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="received_date" id="received_date" class="form-control bootstrap-datepicker-comncls" placeholder="DD/MM/YYYY" value="" />
                                                        </label>
                                                    </section>
                                                </section> 
                                            </div>
                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn btn-primary" >Submit</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                    <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/external_services.js"></script>