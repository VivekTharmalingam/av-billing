
<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php
        echo breadcrumb_links();
        $user_data = get_user_data();
        ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>external_services/" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="List"><i class="glyphicon glyphicon-list"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>External Service View</h2>
                        </header>
                        <!-- widget div-->
                        <div role="content">
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <div class="widget-body">
                                <table id="user" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                        <tr>
                                            <td width="30%" style="font-weight: bold;">External Service No.</td>
                                            <td width="70%" ><?php echo text($external_service->exter_service_no); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Service Partner Name</td>
                                            <td ><?php echo text($external_service->service_partner_name); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Issued Date</td>
                                            <td ><?php echo text($external_service->issued_date_str); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Amount</td>
                                            <td ><?php echo text($external_service->exter_amount); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Description</td>
                                            <td ><?php echo nl2br($external_service->exter_desc); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Received Date</td>
                                            <td ><?php echo text($external_service->received_date_str); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Status</td>
                                            <td ><?php echo text($external_service->status_str); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Payment Status</td>
                                            <td ><?php echo text($external_service->exter_pay_status_str); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
