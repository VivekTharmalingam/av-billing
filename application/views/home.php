<!-- Content Wrapper. Contains page content -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/home.css">
<div id="main" role="main">

    <div id="ribbon">
        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->

    </div>
    <div id="content">
        <?php if (!empty($success)) { ?>
            <div class="alert alert-success fade in" >
                <button class="close" data-dismiss="alert">×</button>
                <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                <strong>Success</strong> <?php echo $success; ?>
            </div>
        <?php } ?>
        <div class="box-body table-responsive">
            <form name="order_search" action="<?php echo $form_action; ?>" method="post" class="search_form validate_form">
                <div class="col-xs-12">
                    <div class="col-md-7"></div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label style="width:28%; padding-top: 5px;"></label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="search_date" id="date_alloc" style="padding: 6px 12px;margin-right:5%; width: 100%;" name="search_date" value="<?php echo $search_date; ?>" autocomplete="off" >
                            </div>
                        </div>
                    </div>															
                    <div class="col-md-1">
                        <div class="form-group">
                            <label style="width:28%; padding-top: 5px;"></label>
                            <div class="input-group">
                                <input type="submit" value="Search" class="btn btn-success btn-primary" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- Main row -->
        <div class="row">

            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
                &nbsp;
            </section>
            <!-- /.Left col -->
        </div>

        <div class="row">
            <?php if ((array_key_exists('invoice', $permissions)) && (in_array(1, $permissions['invoice']))) { ?>
            <section class="col-xs-12 col-sm-6 col-md-4">
                <a  href="<?php echo base_url(); ?>invoice/add" title="Go to Sale" style="text-decoration: none;">  
                    <div class="info-box">
                        <span class="info-box-icon bg-red" style="background-color: #00AEED !important;"><i class="fa fa-line-chart"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Invoice Count</span><br>
                            <span class="purch-info-box-number"><i class="fa fa-thumbs-o-up"></i> <?php echo $sales_det->count; ?></span>
							<br />
                            <span class="info-box-text">Invoice Amount</span>
                            <span class="purch-info-box-number"><i class="fa fa-dollar"></i> <?php echo number_format($sales_det->total_amt, 2); ?></span>
							<br />
							<span class="info-box-text">Profit Amount</span>
                            <span class="purch-info-box-number"><i class="fa fa-dollar"></i> <?php echo number_format($sales_profit, 2); ?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </a> 
            </section>           
            <?php } ?>
            <?php if ((array_key_exists('services', $permissions)) && (in_array(1, $permissions['services']))) { ?>
            <section class="col-xs-12 col-sm-6 col-md-4">
                <a  href="<?php echo base_url(); ?>services/add" title="Go to Services" style="text-decoration: none;">  
                    <div class="info-box">
                        <span class="info-box-icon bg-red" style="background-color: #06ad9c !important;"><i class="fa fa-cogs"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Services Count</span><br>
                            <span class="purch-info-box-number"><i class="fa fa-thumbs-o-up"></i> <?php echo $service_list->count; ?></span><br>
                            <span class="info-box-text"><a href="<?php echo base_url(); ?>scan_service">Scan Service</a></span><br>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </a> 
            </section>           
            <?php } ?>
            <?php if ((array_key_exists('exchange_order', $permissions)) && (in_array(1, $permissions['exchange_order']))) { ?>
            <section class="col-xs-12 col-sm-6 col-md-4">
                <a  href="<?php echo base_url(); ?>exchange_order/add" title="Go to Exchange Order" style="text-decoration: none;">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua" style="background-color: #f50cea !important;"><i class="fa fa-handshake-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Exchange Order Count</span><br>
                            <span class="purch-info-box-number"><i class="fa fa-handshake-o"></i> <?php echo $exchange_order->count; ?></span><br>
                            <span class="info-box-text">Exchange Order Amount</span><br>
                            <span class="purch-info-box-number"><i class="fa fa-dollar"></i> <?php echo number_format($exchange_order->total_amount, 2); ?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </a>
            </section>
            <?php } ?>
            
            <!-- /.col -->
        </div>
        <div class="row">
           
            <?php if ((array_key_exists('user', $permissions)) && (in_array(1, $permissions['user']))) { ?>
            <section class="col-xs-12 col-sm-6 col-md-4">    
                    <div class="info-box">
                        <span class="info-box-icon bg-green" style="background-color: #512758 !important;"><i class="fa fa-money"></i></span>

                        <div class="info-box-content">
                            <span class="banking_reprt_cls" data-bnkrept="TODREC" style="padding-bottom: 3px;display: inline-block;cursor: pointer;"><span class="info-box-text" style="color: #0588b7;">Today Cheque Receivable</span>
                            <span class="purch-info-box-number" style="color: #00AEED;"><i class="fa fa-dollar"></i> <?php echo number_format($tod_rec, 2); ?></span></span>
                                                        <br />
                            <span class="banking_reprt_cls" data-bnkrept="TMRREC" style="padding-bottom: 3px;display: inline-block;cursor: pointer;"><span class="info-box-text" style="color: #0588b7;">Tmrw Cheque Receivable</span>
                            <span class="purch-info-box-number" style="color: #00AEED;"><i class="fa fa-dollar"></i> <?php echo number_format($tmr_rec, 2); ?></span></span>
							<br />
                            <span class="banking_reprt_cls" data-bnkrept="TODPAY" style="padding-bottom: 3px;display: inline-block;cursor: pointer;"><span class="info-box-text">Today Cheque Payment</span>
                            <span class="purch-info-box-number"><i class="fa fa-dollar"></i> <?php echo number_format($tod_pay, 2); ?></span></span>
							<br />
                            <span class="banking_reprt_cls" data-bnkrept="TMRPAY" style="cursor: pointer;"><span class="info-box-text">Tmrw Cheque Payment</span>
                            <span class="purch-info-box-number"><i class="fa fa-dollar"></i> <?php echo number_format($tmr_pay, 2); ?></span></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
            </section> 
            <?php } ?>
            
            <?php if ((array_key_exists('enquiry_log_form', $permissions)) && (in_array(1, $permissions['enquiry_log_form']))) { ?>
            <section class="col-xs-12 col-sm-6 col-md-4">
                <a  href="<?php echo base_url(); ?>enquiry_log_form/add" title="Go to Enquiry Log Form" style="text-decoration: none;">    
                    <div class="info-box">
                        <span class="info-box-icon bg-green" style="background-color: #8B1A0D !important;"><i class="fa fa-envelope-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Enquiry Log Form Count</span><br>
                            <span class="purch-info-box-number"><i class="fa fa-thumbs-o-up"></i> <?php echo $enq_log_list->count; ?></span><br>
                            <span class="info-box-text">Completed Enquiry Log Form</span><br>
                            <span class="purch-info-box-number"><i class="fa fa-envelope-o"></i> <?php echo $compl_enq_log_list->count; ?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </a>    
            </section>  
            <?php } ?>
            <?php if ((array_key_exists('goods_receive', $permissions)) && (in_array(1, $permissions['goods_receive']))) { ?>
            <section class="col-xs-12 col-sm-6 col-md-4">
                <a  href="<?php echo base_url(); ?>goods_receive/add" title="Go to Goods Recieve" style="text-decoration: none;">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua" style="background-color: #b02773 !important;"><i class="fa fa-shopping-cart"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Goods Receive Count</span><br>
                            <span class="purch-info-box-number"><i class="fa fa-shopping-cart"></i> <?php echo $receive_det->count; ?></span><br>
                            <span class="info-box-text">Goods Receive Amount</span><br>
                            <span class="purch-info-box-number"><i class="fa fa-dollar"></i> <?php echo $receive_det->pr_net_amount; ?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </a>  
            </section>
            <?php } ?>
            <!-- /.col -->
        </div>
        <!-- /.row (main row) -->
        <section id="widget-grid" class="">
            <?php if ((array_key_exists('invoice', $permissions)) && (in_array(1, $permissions['invoice']))) { ?>
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                    <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" style="min-height: 50px; max-height: 400px; overflow-y: scroll;">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-money"></i> </span>
                            <h2 class="hidden-xs">Payment Alert (Unpaid Invoices)</h2> 
                            <span class="widget-toolbar" rel="tooltip" data-placement="bottom" data-original-title="View More Unpaid Invoice" onclick="location.href = '<?php echo base_url(); ?>invoice_reports/index/2'">
                                <i class="fa fa-list" aria-hidden="true"></i>
                            </span>
                        </header>

                        <div>
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <div class="widget-body no-padding">
                                <table id="dt_basic_5" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>			                
                                        <tr>
                                            <th style="width: 5%;" data-class="expand">S.No</th>
                                            <th style="width: 15%;" data-hide="phone,tablet"><i class="fa fa-fw fa-calander txt-color-blue hidden-md hidden-sm hidden-xs"></i>Invoice Date</th>
                                            <th style="width: 20%;" data-hide="phone,tablet"><i class="fa fa-fw fa-key txt-color-blue hidden-md hidden-sm hidden-xs"></i>Invoice No.</th>
                                            <th style="width: 20%;" data-hide="phone,tablet"><i class="fa fa-fw fa-building txt-color-blue hidden-md hidden-sm hidden-xs"></i>Customer Name</th>
                                            <th style="width: 15%;" data-hide="phone,tablet"><i class="fa fa-fw fa-phone-square txt-color-blue hidden-md hidden-sm hidden-xs"></i>Contact No</th>
                                            <th style="width: 15%;" data-hide="phone,tablet"><i class="fa fa-fw fa-money txt-color-blue hidden-md hidden-sm hidden-xs"></i>Amount (S$)</th>

                                            <th style="width: 10%;" data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i>Days Ago</th>
                                            <!--<th style="width: 15%;" data-hide="phone,tablet"><i class="fa fa-fw fa-exclamation-triangle txt-color-blue hidden-md hidden-sm hidden-xs"></i>Status</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($sales_unpaid as $key => $row) { ?>
                                            <?php
                                            $date1 = text_date($row->created_on);
                                            $date2 = date('Y-m-d');

                                            $diff = abs(strtotime($date2) - strtotime($date1));

                                            $years = floor($diff / (365 * 60 * 60 * 24));
                                            $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                                            $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
                                            //printf("%d years, %d months, %d days\n", $years, $months, $days);
                                            //$days = ("%d days\n", $days); 
                                            ?>

                                            <tr>
                                                <td><?php echo ++$key; ?></td>
                                                <td><?php echo text_date($row->so_date); ?></td>
                                                <td><?php echo text($row->so_no); ?></td>
                                                <td><?php echo text($row->name); ?></td>
                                                <td><?php echo text($row->phone_no); ?></td>
                                                <td align="right"><?php echo text($row->total_amt); ?></td>
                                                <td><?php if ($days == 1) {
                                            echo $days . '&nbsp;Day Ago';
                                        } else {
                                            echo $days . '&nbsp;Days Ago';
                                        } ?></td>
                                        <input type="hidden" class="view" value="<?php echo $row->id; ?>">
                                        <!--<td><?php echo $row->status_str; ?></td>-->
                                        </tr>
<?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>

                </article>

                <!-- /.row -->
            </div>
            <?php } ?>
            <?php if ((array_key_exists('services', $permissions)) && (in_array(1, $permissions['services']))) { ?>
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                    <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" style="min-height: 50px; max-height: 400px; overflow-y: scroll;">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-cogs"></i> </span>
                            <h2 class="hidden-xs">Services</h2> 
                            <span class="widget-toolbar" rel="tooltip" data-placement="bottom" data-original-title="View More Services" onclick="view_services_fun()">
                                <i class="fa fa-list" aria-hidden="true"></i>
                            </span>
                        </header>

                        <div>
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <div class="widget-body no-padding">
                                <table id="dt_basic_3" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>			                
                                        <tr>
                                            <th style="width: 5%;" data-class="expand">S.No</th>
                                            <th style="width: 12%;" data-hide="phone,tablet"><i class="fa fa-fw fa-key txt-color-blue hidden-md hidden-sm hidden-xs"></i>Jobsheet No.</th>
                                            <th style="width: 14%;" data-hide="phone,tablet"><i class="fa fa-fw fa-building txt-color-blue hidden-md hidden-sm hidden-xs"></i>Customer</th>
                                            <th style="width: 12%;" data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i>Delivery Date</th>
                                            <th style="width: 14%;" data-hide="phone,tablet"><i class="fa fa-fw fa-user-secret txt-color-blue hidden-md hidden-sm hidden-xs"></i>Engineer </th>
                                            <th style="width: 8%;" data-hide="phone,tablet"><i class="fa fa-fw fa-fw fa-warning txt-color-blue hidden-md hidden-sm hidden-xs"></i>Status</th>
                                            <th style="width: 14%;" data-hide="phone,tablet"><i class="fa fa-fw fa-dollar txt-color-blue hidden-md hidden-sm hidden-xs"></i>Service Amount</th>
                                            <th style="width: 12%;" data-hide="phone,tablet"><i class="fa fa-fw fa-dollar txt-color-blue hidden-md hidden-sm hidden-xs"></i> Our Cost</th>
                                            <th style="width: 10%;" data-hide="phone,tablet"><i class="fa fa-fw fa-dollar txt-color-blue hidden-md hidden-sm hidden-xs"></i> Inv Amt</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($estimated_service as $key => $row) {
                                            $date1 = new DateTime($row->estimated_delivery_date);
                                            $date2 = new DateTime('tomorrow');
                                            $diff = $date2->diff($date1)->format("%d");
                                            ?>
                                            <tr>
                                                <td><?php echo $key+1; ?></td>
                                                <td><?php echo text($row->jobsheet_number); ?></td>
                                                <td><?php echo text($row->customer_name); ?></td>
                                                <td><?php echo text_date($row->estimated_delivery_date); ?></td>
                                                <td><?php echo text($row->engineer_name); ?></td>
                                                <td><?php echo text($row->service_status_str); ?></td>
                                                <td align="right"><?php echo text_amount($row->service_cost); ?></td>
                                                <td align="right"><?php echo text_amount($row->our_cost); ?></td>
                                                <td align="right"><?php echo text_amount($row->total_amt); ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>

                </article>

                <!-- /.row -->
            </div>
            <?php } ?>
            <?php if ((array_key_exists('enquiry_log_form', $permissions)) && (in_array(1, $permissions['enquiry_log_form']))) { ?>
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                    <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" style="min-height: 50px; max-height: 400px; overflow-y: auto;">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-envelope-o"></i> </span>
                            <h2 class="hidden-xs">Unpicked Enquiries</h2> 
                            <span class="widget-toolbar" rel="tooltip" data-placement="bottom" data-original-title="View More Enquiry Log Details" onclick="location.href = '<?php echo base_url(); ?>enquiry_log_form'">
                                <i class="fa fa-list" aria-hidden="true"></i>
                            </span>
                        </header>

                        <div>
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <div class="widget-body no-padding">
                                <table id="dt_basic_4" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>			                
                                        <tr>
                                            <th width="5%" data-hide="phone"  data-class="expand">S.No</th>
                                            <th width="20%"><i class="fa fa-fw fa-building text-muted hidden-md hidden-sm hidden-xs"></i> Company Name</th>
                                            <th width="15%" data-hide="phone"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Contact Person</th>
                                            <th width="15%" data-hide="phone,tablet"><i class="fa fa-fw fa-tablet text-muted hidden-md hidden-sm hidden-xs"></i>Description</th>
                                            <th width="10%" data-hide="phone,tablet"><i class="fa fa-fw fa-envelope-o text-muted hidden-md hidden-sm hidden-xs"></i>Type</th>
                                            <th width="15%" data-hide="phone,tablet"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Assigned To</th>
                                            <th width="10%" data-hide="phone,tablet"><i class="fa fa-fw fa-flag-o hidden-md hidden-sm hidden-xs"></i>Enquiry Status</th>
                                            <th width="15%" style="text-align: center;"><i class="fa fa-fw fa-cog  txt-color-blue hidden-md hidden-sm hidden-xs"></i> Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            <?php foreach ($unpicked_enq_log as $key => $row) { ?>
                                            <tr>
                                                <td><?php echo ++$key; ?></td>
                                                <td><?php echo text($row->name); ?></td>
                                                <td><?php echo text($row->contact_person); ?></td>
                                                <td><?php echo text($row->description); ?></td>
                                                <?php if ($row->type == 1) { ?>
                                                    <td><?php echo 'Sales'; ?></td>    
                                                <?php } else if ($row->type == 2) { ?>
                                                    <td><?php echo 'Purchase'; ?></td>   
                                                    <?php } else if ($row->type == 3) { ?>
                                                    <td><?php echo 'Accounts'; ?></td>   
                                                    <?php } else if ($row->type == 4) { ?>
                                                    <td><?php echo 'Promotions'; ?></td>  
                                                    <?php } ?>
                                                <td>
                                                    <?php
                                                    $usr_names = '';
                                                    $usr_id = explode(',', $row->assigned_to);
                                                    $tot_count = count($usr_id);
                                                    $int_val = 1;
                                                    foreach ($user as $key => $value) :
                                                        if (in_array($value->id, $usr_id)) :
                                                            if ($int_val == $tot_count) :
                                                                $usr_names .= $value->name;
                                                            else :
                                                                $usr_names .= $value->name . ',<br />';
                                                            endif;
                                                            $int_val++;
                                                        endif;
                                                    endforeach;
                                                    echo $usr_names;
                                                    ?>
                                                </td>
    <!--                                                <td><?php echo text($row->assigned_to); ?></td>-->
                                                <td><?php echo text($row->enq_status_str); ?></td>
                                                <td style="text-align: center;"><a class="label btn btn-info pickedup" data-id="<?php echo $row->id; ?>" data-href="<?php echo base_url() . 'home/picked_status/' . $row->id ?>"><span>Pick</span></a></td>
                                            </tr>
<?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>

                </article>
                <!-- /.row -->
            </div>
		<?php } ?>
                <?php if ((array_key_exists('invoice', $permissions)) && (in_array(1, $permissions['invoice']))) { ?>
			<div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                    <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" style="min-height: 50px; max-height: 400px; overflow-y: scroll;">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-line-chart"></i> </span>
                            <h2 class="hidden-xs">Invoices</h2> 
                            <span class="widget-toolbar" rel="tooltip" data-placement="bottom" data-original-title="View More Invoice" onclick="location.href = '<?php echo base_url(); ?>invoice'">
                                <i class="fa fa-list" aria-hidden="true"></i>
                            </span>
                        </header>

                        <div>
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <div class="widget-body no-padding">
                                <table id="dt_basic_2" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>			                
                                        <tr>
                                            <th style="width: 5%;" data-class="expand">S.No</th>
                                            <th style="width: 10%;" data-hide="phone,tablet"><i class="fa fa-fw fa-calander txt-color-blue hidden-md hidden-sm hidden-xs"></i>Invoice Date</th>
                                            <th style="width: 10%;" data-hide="phone,tablet"><i class="fa fa-fw fa-key txt-color-blue hidden-md hidden-sm hidden-xs"></i>Invoice No.</th>

                                            <th style="width: 20%;" data-hide="phone,tablet"><i class="fa fa-fw fa-building txt-color-blue hidden-md hidden-sm hidden-xs"></i>Customer Name</th>
                                            <th style="width: 15%;" data-hide="phone,tablet"><i class="fa fa-fw fa-money txt-color-blue hidden-md hidden-sm hidden-xs"></i>Total Amount (S$)</th>
                                            <!--<th style="width: 15%;" data-hide="phone,tablet"><i class="fa fa-fw fa-exclamation-triangle txt-color-blue hidden-md hidden-sm hidden-xs"></i>Status</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($sales as $key => $row) { ?>
                                            <tr>
                                                <td><?php echo ++$key; ?></td>
                                                <td><?php echo text_date($row->so_date); ?></td>
                                                <td><?php echo text($row->so_no); ?></td>
                                                <td><?php echo text($row->name); ?></td>
                                                <td align="right"><?php echo text($row->total_amt); ?></td>
                                        <input type="hidden" class="view" value="<?php echo $row->id; ?>">
                                        <!--<td><?php echo $row->status_str; ?></td>-->
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>

                </article>

                <!-- /.row -->
            </div>
            <?php } ?>
            <?php if ((array_key_exists('goods_receive', $permissions)) && (in_array(1, $permissions['goods_receive']))) { ?>
                <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" style="min-height: 50px; max-height: 400px; overflow-y: scroll;">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-shopping-cart"></i> </span>
                            <h2 class="hidden-xs">Good Receives</h2> 
                            <span class="widget-toolbar" rel="tooltip" data-placement="bottom" data-original-title="View More Good Receive" onclick="location.href = '<?php echo base_url(); ?>goods_receive'">
                                <i class="fa fa-list" aria-hidden="true"></i>
                            </span>
                        </header>

                        <div>
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <div class="widget-body no-padding">
                                <table id="dt_basic_1" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;" data-class="expand">S.No</th>
                                            <th style="width: 10%;" data-hide="phone,tablet"><i class="fa fa-fw fa-calander txt-color-blue hidden-md hidden-sm hidden-xs"></i>Inv Date</th>
                                            <th style="width: 10%;" data-hide="phone,tablet"><i class="fa fa-fw fa-key txt-color-blue hidden-md hidden-sm hidden-xs"></i>Inv No</th>
                                            <!--<th style="width: 20%;" data-hide="phone,tablet"><i class="fa fa-fw fa-university txt-color-blue hidden-md hidden-sm hidden-xs"></i>Branch Name</th>-->
                                            <th style="width: 15%;" data-hide="phone,tablet"><i class="fa fa-fw fa-shopping-cart txt-color-blue hidden-md hidden-sm hidden-xs"></i>Supplier Name</th>
                                            <th style="width: 15%;" data-hide="phone,tablet"><i class="fa fa-fw fa-money txt-color-blue hidden-md hidden-sm hidden-xs"></i>Total Amount (S$)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($receive as $key => $pr) { ?>
                                            <tr>                                    
                                                <td><?php echo ++$key; ?></td>
                                                <td><?php echo date('d/m/Y', strtotime($pr->invoice_date)); ?></td>
                                                <td><?php echo text($pr->supplier_invoice_no); ?></td>
                                                <!--<td><?php // echo text($po->branch_name);   ?></td>-->
                                                <td><?php echo text($pr->name); ?></td>
                                                <td align="right"><?php echo $pr->pr_net_amount; ?></td>
                                        <input type="hidden" class="view" value="<?php echo $pr->id; ?>">
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                </article>
                <!-- /.row -->
            </div>
            <?php } ?>
            
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" style="min-height: 50px; max-height: 400px; overflow-y: scroll;">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-shopping-cart"></i> </span>
                            <h2 class="hidden-xs">Low Stock Item List</h2> 
                            <span class="widget-toolbar" rel="tooltip" data-placement="bottom" data-original-title="View Current Stock List" onclick="location.href = '<?php echo base_url(); ?>inventory'">
                                <i class="fa fa-list" aria-hidden="true"></i>
                            </span>
                        </header>

                        <div>
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <div class="widget-body no-padding">
                                <table id="dt_basic_6" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;" data-class="expand">S.No</th>
                                            <th style="width: 10%;" data-hide="phone,tablet"><i class="fa fa-fw fa-calander txt-color-blue hidden-md hidden-sm hidden-xs"></i>Item Code</th>
                                            <th style="width: 10%;" data-hide="phone,tablet"><i class="fa fa-fw fa-key txt-color-blue hidden-md hidden-sm hidden-xs"></i>Brand Name</th>
                                            <th style="width: 20%;" data-hide="phone,tablet"><i class="fa fa-fw fa-university txt-color-blue hidden-md hidden-sm hidden-xs"></i>Description</th>
                                            <th style="width: 15%;" data-hide="phone,tablet"><i class="fa fa-fw fa-money txt-color-blue hidden-md hidden-sm hidden-xs"></i>Quantity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($lowstack_list as $key => $pr) { ?>
                                            <tr>                                    
                                                <td><?php echo ++$key; ?></td>
                                                <td><?php echo $pr->code; ?></td>
                                                <td><?php echo text($pr->cat); ?></td>
                                                <td><?php echo text($pr->product); ?></td>
                                                <td align="right"><?php echo $pr->quantity; ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                </article>
                <!-- /.row -->
            </div>
            
        </section>
    </div>
</div>
<!-- /.content-wrapper -->

<script>
    $(document).ready(function () {
        $('.pickedup').on('click', function () {
            $('#picked_status').modal();
        });
        
        $('.banking_reprt_cls').on('click', function () {
            var bnkrept_txt = $(this).data('bnkrept');
            view_bank_fun(bnkrept_txt);
        });

        $('.pick_confirmation').on('click', function () {
            var $action = $('.pickedup').attr('data-href');
            var $id = $('.pickedup').attr('data-id');
            var ajaxURL = baseUrl + 'home/pick_status';
            var ajaxData = {
                id: $id
            }
            var details = ajaxRequest(ajaxURL, ajaxData, '');
            if (details != '0' && details != null) {
                window.top.location = baseUrl + 'enquiry_log_form';
            }
        });


        /* BASIC ;*/
        var responsiveHelper_dt_basic_1 = undefined;
        var responsiveHelper_dt_basic_2 = undefined;
        var responsiveHelper_dt_basic_3 = undefined;
        var responsiveHelper_dt_basic_4 = undefined;
        var responsiveHelper_dt_basic_5 = undefined;
        var responsiveHelper_dt_basic_6 = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $('#dt_basic_1').dataTable({
            "sDom": "",
            "autoWidth": true,
            "iDisplayLength": 100,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic_1) {
                    responsiveHelper_dt_basic_1 = new ResponsiveDatatablesHelper($('#dt_basic_1'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic_1.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic_1.respond();
            },
        });

        $('#dt_basic_2').dataTable({
            "sDom": "",
            "autoWidth": true,
            "iDisplayLength": 100,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic_2) {
                    responsiveHelper_dt_basic_2 = new ResponsiveDatatablesHelper($('#dt_basic_2'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic_2.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic_2.respond();
            }
        });

        $('#dt_basic_3').dataTable({
            "sDom": "",
            "autoWidth": true,
            "iDisplayLength": 100,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic_3) {
                    responsiveHelper_dt_basic_3 = new ResponsiveDatatablesHelper($('#dt_basic_3'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic_3.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic_3.respond();
            }
        });
        $('#dt_basic_4').dataTable({
            "sDom": "",
            "autoWidth": true,
            "iDisplayLength": 100,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic_4) {
                    responsiveHelper_dt_basic_4 = new ResponsiveDatatablesHelper($('#dt_basic_4'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic_4.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic_4.respond();
            }
        });
        
        $('#dt_basic_5').dataTable({
            "sDom": "",
            "autoWidth": true,
            "iDisplayLength": 100,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic_5) {
                    responsiveHelper_dt_basic_5 = new ResponsiveDatatablesHelper($('#dt_basic_5'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic_5.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic_5.respond();
            }
        });
        
        var responsiveHelper_dt_basic_6 = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $('#dt_basic_6').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            'iDisplayLength': 5,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic_6) {
                   responsiveHelper_dt_basic_6 = new ResponsiveDatatablesHelper($('#dt_basic_6'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
               responsiveHelper_dt_basic_6.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
               responsiveHelper_dt_basic_6.respond();
            }
        });

        $('#dt_basic_1 tr td').on('click', function (event) {
            var href = $(this).parents('tr').find('.view').val();
            if (typeof href != 'undefined' && href != '#') {
                ahref = baseURL + 'goods_receive/view/' + href;
                window.open(ahref, '_self');
            }

        });
        $('#dt_basic_2 tr td').on('click', function (event) {
            var href = $(this).parents('tr').find('.view').val();
            if (typeof href != 'undefined' && href != '#') {
                ahref = baseURL + 'invoice/view/' + href;
                window.open(ahref, '_self');
            }
        });
        $('#dt_basic_3 tr td').on('click', function (event) {
            var href = $(this).parents('tr').find('.view').val();
            if (typeof href != 'undefined' && href != '#') {
                ahref = baseURL + 'invoice_reports/view/' + href;
                window.open(ahref, '_self');
            }
        });

        $('table').css('cursor', 'pointer');

        /* END BASIC */
        $('#date_alloc').daterangepicker({
            autoUpdateInput: false,
            opens: 'center',
            locale: {
                format: 'MM/DD/YYYY h:mm A',
                cancelLabel: 'Cancel'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            //alert(start + '|' + end + '|' + label);
            // getResults();
        });

    });

    function view_services_fun(){
            var url = "<?php echo base_url().'service_reports/'; ?>";
            var form = document.createElement("form");
            $(form).attr("action", url)
                   .attr("method", "post");
            $(form).html('<input type="hidden" name="service_from" value="home" />');
            document.body.appendChild(form);
            $(form).submit();
            document.body.removeChild(form); 
    }
    
    function view_bank_fun(bnk_text){
            var url = "<?php echo base_url().'banking_report/'; ?>";
            var form = document.createElement("form");
            $(form).attr("action", url)
                   .attr("method", "post");
            $(form).html('<input type="hidden" name="home_to_bankrept" value="'+bnk_text+'" />');
            document.body.appendChild(form);
            $(form).submit();
            document.body.removeChild(form); 
    }
</script>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/custom.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datepicker/daterangepicker.js"></script>