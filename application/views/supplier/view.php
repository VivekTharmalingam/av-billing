<?php
    
    /*
     * Author Name  : Jai K
     * Purpose      : The view page for showing the existing user.
     * Created On   : 2016-04-21 18:46
     */

?>
<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                     <a href="<?php echo base_url(); ?>supplier/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>Supplier - View</h2>	
<!--                             <span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Users List"><a href="<?php echo $list_link; ?>" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a></span>-->
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body"> 
                                <span id="watermark"><?php echo WATER_MARK; ?></span>
                                 <table id="user" class="table table-bordered table-striped" style="clear: both;">
                                    <tbody>
                                        <tr>
                                            <td style="width:35%;"><b>Supplier Name</b></td>
											<td style="width:65%;"><?php echo $supplier->name;?></td>
										</tr>	
                                        <tr>
                                            <td><b>Address</b></td>
                                            <td><?php echo $supplier->address;?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Phone No</b></td>
                                            <td><?php echo $supplier->phone_no;?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Fax</b></td>
                                            <td><?php echo $supplier->fax;?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Contact Name</b></td>
                                            <td><?php echo $supplier->contact_name;?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Email</b></td>
                                            <td><?php echo $supplier->email_id;?></td>
                                        </tr>        
                                        <tr>
                                            <td><b>Website</b></td>
                                            <td><?php echo $supplier->website;?></td>
                                        </tr>
                                        <tr>
                                            <td><b>GST</b></td>
                                            <td><?php echo ($supplier->gst_type == 1) ? "With GST" : "Without GST";?></td>
                                        </tr>
										<tr>
                                            <td><b>Payment Term</b></td>
                                            <td><?php if($supplier->payment_terms == 1){
													echo 'COD';
												}elseif($supplier->payment_terms == 2) {
													echo '14-Days';
												}elseif($supplier->payment_terms == 3) {
													echo '30-Days';
												}elseif($supplier->payment_terms == 4) {
													echo '45-Days';
												}else{
													echo 'Others';
												}  ?></td>
                                        </tr>	
                                        <tr>
                                            <td><b>Key Word</b></td>
                                            <td><?php echo $supplier->key_word;?></td>
                                        </tr>
					<tr>
                                            <td><b>Status</b></td>
                                            <td><?php echo ( $supplier->status_str === 'Active' ) ? 'Active' : 'Inactive'; ?></td>
                                        </tr>
                                    </tbody>
                                </table>                                
                        </div>
                        <!-- end widget div -->

                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
