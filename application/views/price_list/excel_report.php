<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Item_Price_List".time().".xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; margin-left:10px">
	<tr style="font-family:Times new roman; font-weight:bold;">
		<th width="15%">Item Code</th>
		<th width="20%">Brand Name</th>
		<th width="30%">Item Description</th>
		<th width="10%">Quantity</th>
		<th width="12%">Cost</th>
		<th width="13%">Price</th>
	</tr>
	<?php if (count($product)) {?>
	<?php foreach($product as $key => $row) {?>                    
		<tr>
			<td style="padding:8px;" ><?php echo text($row->pdt_code);?></td>
			<td style="padding:8px;"><?php echo text($row->cat_name);?></td>
			<td style="padding:8px;"><?php echo text($row->pdt_name);?></td>
			<td style="padding:8px; text-align: right;"><?php echo $row->available_qty;?></td>
			<td style="padding:8px; text-align: right;"><?php echo number_format($row->buying_price, 2);?></td>
			<td style="padding:8px; text-align: right;"><?php echo number_format($row->selling_price, 2);?></td>
		</tr>
	<?php }?>
	<?php } else {?>
		<tr>
			<td colspan="4" align="center">No Record Found</td>
		</tr>
	<?php }?>
</table>