<table border="0" cellpadding="3" cellspacing="0" style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:14px; width: 1000px;">
    <?php echo $head;?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>
<table width="100%" border="1" style="border-collapse:collapse; margin-left:10px">
	<thead>
		<tr style="font-family:Times new roman;font-size:16px;font-weight:bold;">
			<th width="5%" style="padding:10px">S.NO</th>
			<th width="15%">Item Code</th>
			<th width="15%">Brand Name</th>
			<th width="30%">Item Description</th>
			<th width="10%">Quantity</th>
			<th width="15%">Cost</th>
			<th width="15%">Price</th>
		</tr>
	</thead>
	<tbody>
	<?php if (count($product)) {?>
	<?php foreach($product as $key => $row) {?>                    
		<tr>
			<td style="padding:8px;" ><?php echo ++ $key;?></td>
			<td style="padding:8px;"><?php echo text($row->pdt_code);?></td>
			<td style="padding:8px;"><?php echo strtoupper(text($row->cat_name));?></td>
			<td style="padding:8px;"><?php echo strtoupper(text($row->pdt_name));?></td>
			<td style="padding:8px;"><?php echo $row->available_qty;?></td>
			<td style="padding:8px; text-align: right;"><?php echo number_format($row->buying_price, 2);?></td>
			<td style="padding:8px; text-align: right;"><?php echo number_format($row->selling_price, 2);?></td>
		</tr>
	<?php }?>
	<?php } else {?>
		<tr>
			<td colspan="6" align="center">No Record Found</td>
		</tr>
	<?php }?>
	</tbody>
</table>