<script>
    window.localStorage.clear();
</script>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">&nbsp;</div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="widget-body">
						<!-- Widget ID (each widget will need unique ID)-->                                
						<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false">
							<header><span class="widget-icon"> <i class="fa fa-table"></i> </span>
								<h2>Price List Reports</h2>
							</header>
							
							<!--<div class="date-range"></div>-->
							<div>
								<div class="jarviswidget-editbox">
									<!-- This area used as dropdown edit box -->
								</div>
								<div class="widget-body no-padding">
									<div class="box-body table-responsive">
										<form name="order_search" action="<?php echo $form_action;?>" method="post" class="search_form validate_form">
											<div class="col-xs-12">
												<div class="col-md-3" style="margin: 15px 0px;">
													<select  name="brand_id" class="brand_id select2">  
														<option value="">ALL BRANDS</option>
														<?php foreach($brand as $key => $val){ ?>
															<option value="<?php echo $val->id;?>" <?php echo ($val->id == $brand_id) ? 'selected' : '' ?>><?php echo $val->cat_name;?></option>
														<?php } ?>
													</select>
												</div>
												<div class="col-md-3" style="margin: 15px 0px;">
													<select name="category_id" class="select2" id="category_id" data-sub_category_id="<?php echo $sub_category_id;?>">  
														<option value="">All Category</option>
														<?php foreach ($item_category as $key => $val) { ?>
															<option value="<?php echo $val->id; ?>" <?php echo ($val->id == $category_id) ? 'selected' : ''; ?>><?php echo $val->item_category_name; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="col-md-3" style="margin: 15px 0px;">
													<select name="sub_category_id" class="select2" id="sub_category_id">  
														<option value="">Select Sub Category</option>
													</select>
												</div>
												<div class="col-md-2" style="margin: 15px 0px;">
													<input type="submit" name="reset_btn" value="Reset" id="reset" class="btn btn-success btn-warning" />&nbsp;&nbsp;<input type="submit" name="submit" value="Search" class="btn btn-success btn-primary" />
												</div>														
												<div class="col-md-1" style="margin: 15px 0px;">
													<input type="submit" name="submit" value="pdf" class="submit_pdf" style="margin-left: 10px;" />
													<input type="submit" name="submit" value="excel" class="submit_excel" />
												</div>
											</div>
										</form>
									</div>
									
									<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
									   <thead>			                
											<tr>
												<th width="10%" data-class="expand">S.No</th>
												<th width="10%" data-hide="phone,tablet"><i class="fa fa-fw fa-key text-muted hidden-md hidden-sm hidden-xs"></i>Item Code</th>
												<th width="15%" data-hide="phone,tablet"><i class="fa fa-fw fa-sitemap text-muted hidden-md hidden-sm hidden-xs"></i>Brand Name</th> 
												<th width="29%" data-hide="phone,tablet"><i class="fa fa-fw fa-archive text-muted hidden-md hidden-sm hidden-xs"></i> Item Description</th>
												<th width="10%" data-hide="phone,tablet"><i class="fa fa-fw fa-cube text-muted hidden-md hidden-sm hidden-xs"></i>Quantity</th>
												<th width="8%" data-hide="phone,tablet"><i class="fa fa-fw fa-dollar text-muted hidden-md hidden-sm hidden-xs"></i>Cost</th>
												<th width="8%" data-hide="phone,tablet"><i class="fa fa-fw fa-dollar text-muted hidden-md hidden-sm hidden-xs"></i>Price</th>
												<th width="10%" data-hide="phone" style="text-align: center;"><i class="fa fa-fw fa-cog txt-color-blue hidden-md hidden-sm hidden-xs"></i> Actions</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($product as $key => $row) { ?>
												<tr>
													<td><?php echo ++$key; ?></td>
													<td><?php echo text($row->pdt_code); ?></td>
													<td><?php echo text($row->cat_name);?></td>
													<td><?php echo text($row->pdt_name); ?></td>
													<td><?php echo $row->available_qty; ?></td>
													<td align="right"><?php echo number_format($row->buying_price, 2); ?></td>
													<td align="right"><?php echo number_format($row->selling_price, 2); ?></td>
													<td align="center">
														<?php if (in_array(1, $permission)) {?>
															<a class="label btn btn-warning view" href="<?php echo $view_link . $row->id ?>"><span>View</span></a>
														<?php }?>
													</td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
								<!-- end widget content -->
                            </div>
                        </div> 
                    </div>
                </article>
                <!-- /.row -->
            </div>
        </section>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var responsiveHelper_dt_basic = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"  +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            'iDisplayLength': 25,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            }
        });
    });
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/report.js"></script>