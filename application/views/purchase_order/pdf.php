    <table width="100%" border="1" style="font-family: calibri; border-collapse:collapse;font-size: 13px;" cellpadding="5">
        <thead>
            <tr>
                <th width="15%" style="background:#EBEBEB;">Item Code</th>
                <th width="40%" style="background:#EBEBEB;">Description</th>
                <th width="15%" style="background:#EBEBEB;">Quantity</th>                    
                <th width="15%" style="background:#EBEBEB;">Unit Price ($)</th>
                <th width="15%" style="background:#EBEBEB;">Amount ($)</th>
            </tr>
        </thead>
		<tfoot style="display: table-footer-group">
			<tr>
				<td colspan="5" style="border-left: none;border-right: none;border-bottom: none;font-size: 15px; text-align: center;"></td>
			</tr>
		</tfoot>
		<tbody>
		<?php $height = 410; ?>
        <?php foreach ($po_items as $key => $item) { ?>
            <tr style="vertical-align:top;">
                <td style="border-top: 0; border-bottom: none;"><?php echo strtoupper($item->pdt_code); ?></td>
                <td style="border-top: 0;border-bottom: none;"><?php echo nl2br(strtoupper($item->product_description)); ?>
                    <?php if(!empty($item->sl_itm_code)) { ?><br /><b>Supplier Item Code</b>&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;
                    <span><?php echo $item->sl_itm_code; ?></span> <?php } ?>
                </td>
                <td align="center" style="border-top: 0;border-bottom: none;"><?php echo $item->quantity; ?> </td>

                <td align="right" style="border-top: 0;border-bottom: none;"><?php echo $item->unit_cost; ?></td>
                <td align="right" style="border-top: 0;border-bottom: none;"><?php echo $item->total; ?></td>
            </tr>
            <?php $height -= 55; ?>
        <?php } ?>
        <?php $height -= ($po->po_discount > 0) ? 30 : 0; ?>
        <?php $height -= ($po->gst_total > 0) ? 30 : 0; ?>
        <tr>
            <td height="<?php echo $height; ?>" style="border-top: 0;"></td>
            <td style="border-top: 0;"></td>
            <td style="border-top: 0;"></td>
            <td style="border-top: 0;"></td>
            <td style="border-top: 0;"></td>
        </tr>
        <!--</table>-->

        <?php
        //$row_span = ($po->gst_amount > 0) ? 3 : 2;
        $row_span = 2;
        if ($po->gst_total > 0 && $po->po_discount > 0)
            $row_span = 4;
        else if ($po->gst_total > 0 || $po->po_discount > 0)
            $row_span = 3;
        ?>
    <!--<table width="100%" style="border-collapse:collapse; border-top:none;" cellpadding="5">-->
        <tr>
            <td align="left" style="vertical-align: top; border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; padding-right: 10px;" colspan="3">
                <?php if (!empty($po->po_remarks)) {?>
					<b>Remarks</b>
					<div>
						<?php echo strtoupper(nl2br($po->po_remarks));?>
					</div>
				<?php }?>
            </td>
			<td colspan="2" style="vertical-align: top; padding: 0;">
				<table style="width: 100%;" cellpadding="0" cellspacing="0">
					<tr>
						<th style="text-align: right; height: 30px; width: 109px; border-left: none; border-bottom: 1px solid #000; border-right: 1px solid #000; padding-right: 10px;">Sub Total</th>
						<td align="right" style="width: 108px; border-right: none; border-bottom: 1px solid #000; padding-right: 5px;"><?php echo number_format($po->po_sub_total, 2); ?></td>
					</tr>
					<?php if ($po->po_discount > 0) { ?>
						<tr>
							<th style="height: 30px; border-left: none; border-bottom: 1px solid #000; border-right: 1px solid #000; padding-right: 0px; padding-left: 0px;">Discount  <?php echo ($po->po_discount_type == 1 && $po->po_discount > 0) ? '' . $po->po_discount . '%' . '' : ''; ?></th>
							<td align="right" style="border-bottom: 1px solid #000;border-right: none; padding-right: 5px;"><?php echo number_format($po->po_total_discount, 2); ?></td>
						</tr>
					<?php } ?>
					
					<?php if ($po->gst_total > 0) { ?>
						<tr>
							<th align="right" style="height: 30px; border-left: none; border-bottom: 1px solid #000; border-right: 1px solid #000; padding-right: 10px;"><?php echo ($purchase->gst_type == '2') ? 'ADD GST ' : 'GST '; ?>  <?php echo $po->gst_val; ?> %</th>
							<td align="right" style="border-right: none; border-bottom: 1px solid #000; padding-right: 5px;"><?php echo number_format($po->gst_total, 2); ?></td>
						</tr>
					<?php } ?>
					<tr>
						<th align="right" style="height: 30px; border-left: none; border-bottom: none; border-right: 1px solid #000; padding-right: 10px;">Total</th>
						<td align="right" style="font-weight:normal; border-right: none; border-bottom: none; padding-right: 5px;"><?php echo number_format($po->po_net_amount, 2); ?></td>
					</tr>
				</table>
			</td>
        </tr>
		</tbody>
    </table>