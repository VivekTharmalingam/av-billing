<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>purchase_orders/" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="List"><i class="glyphicon glyphicon-list"></i></button></a>
                    <a target="_blank" href="<?php echo base_url(); ?>purchase_orders/pdf/<?php echo $purchase->id; ?>" class="large_icon_des" style=" color: white;margin-right: 80px;" ><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="Download"><i class="glyphicon glyphicon-save"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-file-text-o"></i> </span>
								<h2>Purchase Order View</h2>
                            </span>
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body">
                                <!--<span id="watermark">Malar ERP.</span>-->
                                <table id="user" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">PO Number</td>
                                            <td ><?php echo $purchase->po_no; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">PO Date</td>
                                            <td ><?php echo date('d/m/Y', strtotime($purchase->po_date)); ?></td>
                                        </tr>                                        
                                        <tr>
                                            <td style="font-weight: bold;">Branch Name</td>
                                            <td ><?php echo $purchase->branch_name; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Supplier Name</td>
                                            <td ><?php echo $purchase->supplier_name; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Contact Person</td>
                                            <td ><?php echo $purchase->contact_name; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Contact No</td>
                                            <td ><?php echo $purchase->contact_no; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Address</td>
                                            <td ><?php echo $purchase->supplier_address; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Status</td>
                                            <td ><?php echo $purchase->status_str; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Remarks</td>
                                            <td ><?php echo (!empty($purchase->po_remarks)) ? nl2br($purchase->po_remarks) : 'N/A'; ?></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="text-left">
                                    <span class="onoffswitch-title">
                                        <h3><b style="color: #8e233b;">Item Details</b></h3>
                                    </span>
                                </div>
                                <div id="widget-tab-1" class="table-responsive">
                                    <table id="dt_basic_2" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
                                        <thead>
                                            <tr>
                                                <th data-class="expand" style="font-weight: bold; text-align:center;">S.No</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Item Code</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Item Description</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Quantity</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Cost (S$)</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Amount (S$)</th>
                                            </tr>
                                        <thead>
                                        <tbody>
                                            <?php foreach ($po_item as $key => $item) { ?>
                                                <tr>
                                                    <td><?php echo ++$key; ?></td>
                                                    <td><?php echo $item->pdt_code; ?></td>
                                                    <td><?php echo nl2br($item->product_description); ?>
                                                        <?php if (!empty($item->sl_itm_code)) {?>
                                                            <br />
                                                            <b>Supplier Item Code</b>&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;
                                                            <span><?php echo $item->sl_itm_code; ?></span>
                                                        <?php }?>
                                                    </td>
                                                    <td align="center"><?php echo $item->quantity; ?></td>
                                                    <td align="right"><?php echo $item->unit_cost; ?></td>   
                                                    <td align="right"><?php echo $item->total; ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>                                                
                                            <tr>
                                                <td colspan="8">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <section class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                            <span style="float: right;">
                                                                <label><b>Item Amount</b></label>
                                                            </span>
                                                        </section>
                                                        <section class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                            <span style="float: right; margin-right: -25px;"><b>S$&nbsp; <?php echo number_format(($purchase->po_sub_total), 2, '.', ''); ?></b></span>
                                                        </section>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="8">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <section class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                            <span style="float: right;">
                                                                <label><b>Total Discount Amount <?php echo ($purchase->po_discount_type == 1) ? ' (' . $purchase->po_discount . ' %' . ' )' : ''; ?></b></label>
                                                            </span>
                                                        </section>
                                                        <section class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                            <span style="float: right; margin-right: -25px;"><b>S$&nbsp; <?php echo $purchase->po_total_discount; ?></b></span>
                                                        </section>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="8">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <section class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                            <span style="float: right;">
                                                                <label style="color: #A90329;"><b>Total Amount (after Discount)</b></label>
                                                            </span>
                                                        </section>
                                                        <section class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                            <span style="float: right; margin-right: -25px;"><b>S$&nbsp;<?php echo $purchase->po_total_with_discount; ?></b></span>
                                                        </section> 
                                                    </div>
                                                </td>
                                            </tr>                                               
                                            <?php if ($purchase->gst_total > 0) { ?>
                                            <tr>
                                                <td colspan="8">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <section class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                            <span style="float: right;">
                                                                <label><b><?php echo ($purchase->gst_type == '2') ? 'ADD GST' : 'GST'; ?> (<?php echo $purchase->gst_val; ?> %)</b></label>
                                                            </span>
                                                        </section>
                                                        <section class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                            <span style="float: right; margin-right: -25px;"><b>S$&nbsp;<?php echo $purchase->gst_total; ?></b></span>
                                                        </section>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php } ?>

<!--                                               <tr>
   <td colspan="8">
     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
       <section class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
            <span style="float: right;">
                <label style="color: #A90329;"><b>Total Amount (with GST)</b></label>
            </span>
        </section>
        <section class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
            <span style="float: right; margin-right: -25px;"><b>S$&nbsp;<?php echo $purchase->po_net_amount; ?></b></span>
        </section>
     </div>
   </td>
</tr>-->


                                            <tr>
                                                <td colspan="8">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <section class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                            <span style="float: right;">
                                                                <label style="color: #A90329;"><b>Net Amount</b></label>
                                                            </span>
                                                        </section>
                                                        <section class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                            <span style="float: right; margin-right: -25px;"><b>S$&nbsp;<?php echo $purchase->po_net_amount; ?></b></span>
                                                        </section>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8">
													<?php if ($purchase->gst_type == '1' && $purchase->sup_gst_type != '2') {?>
														<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        [<?php echo '<b style="font-size: 12px;">Inclusive of GST '.$purchase->gst_val.' %</b> : $ ' . number_format($purchase->gst_total, 2);?>]
														</div>
													<?php }?>
                                                </td>
                                            </tr>


                                        </tfoot>
                                    </table><br><br><br>

                                </div>


                            </div>                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->

