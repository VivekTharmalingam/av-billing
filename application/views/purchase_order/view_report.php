<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-file-text-o"></i> </span>
                            <h2>Supplier Wise Report View</h2>	
                             <span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Supplier Wise Report List"><a href="<?php echo base_url(); ?>supplier_wise_bills_payable_reports/" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a>
                             </span>
                            
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body">
                                <!--<span id="watermark">Malar ERP.</span>--><?php //echo '<pre>'; print_r($purchase); exit; ?>
                                <table id="user" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                        <tr>
                                            <td style="font-weight: bold;">Supplier Name</td>
                                            <td ><?php echo $sup_id->sup_name; ?></td>
                                        </tr>
                                        <tr>
                                           <td colspan="2">
                                                <div>
                                                   <div class="text-left">
                                                        <span class="onoffswitch-title">
                                                          <h3><b style="color: #8e233b;">Material Details</b></h3>
                                                        </span>
                                                        <span class="onoffswitch">
                                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="show-tabs" checked="checked">
                                                            <label class="onoffswitch-label" for="show-tabs"> 
                                                                <span class="onoffswitch-inner" data-swchon-text="True" data-swchoff-text="NO"></span> 
                                                                <span class="onoffswitch-switch"></span> 
                                                            </label> 
                                                        </span>
                                                    </div>
                                                    <div id="widget-tab-1">
                                                        <table id="user" class="table table-bordered table-striped" style="clear: both">
                                                           <thead>
                                                              <tr>
                                                                   <th width="5%"  style="font-weight: bold; text-align:center;">S.No</th>
                                                                   <th width="20%" style="font-weight: bold;text-align:center;">Material Name</th>
                                                                   <th width="15%" style="font-weight: bold;text-align:center;">Units</th>
                                                                   <th width="14%" style="font-weight: bold;text-align:center;">Quantity</th>
                                                                   <th width="17%" style="font-weight: bold;text-align:center;">Rate</th>
                                                                   <th width="17%" style="font-weight: bold;text-align:center;">Price</th>
                                                              </tr>
                                                           <thead>
                                                            <tbody>
                                                               <?php foreach($purchase as $key=>$item){ ?>
                                                                    <tr> 
                                                                        <td><?php echo ++$key;?></td>
                                                                        <td><?php echo $item->mat_name;?></td>
                                                                        <td><?php echo $item->unit_name;?></td>
                                                                        <td align="center"><?php echo $item->poit_quantity;?></td>
                                                                        <td align="right"><b>₹&nbsp;</b><?php echo $item->poit_price;?></td>
                                                                        <td align="right"><b>₹&nbsp;</b><?php echo $item->poit_total_amt;?></td>
                                                                    </tr>
                                                               <tr>
                                                                  <td colspan="5" align="right"><b>Sub Total</b></td>
                                                                  <td align="right"><b>₹&nbsp;</b><?php echo $item->poit_total_amt;?></td>
                                                               </tr>
                                                              
                                                               <?php 
                                                               //foreach($po_tax as $key=>$tax){
                                                                   //$potx_type = ($tax->potx_type == 1) ? '<b>%</b>' : '';
                                                                   //$tax_type = ($tax->tax_type == 2) ? '-' : '+';
                                                                   ?>
                                                               <tr>
<!--                                                                  <td colspan="5" align="right"><b><?php echo $tax->tax_name . ' (' . $tax->potx_percentage . $potx_type  . ')';?></b></td>
                                                                  <td align="right"><?php echo $tax_type;?><b>&nbsp;₹&nbsp;</b><?php echo $tax->potx_amt;?></td>-->
                                                               </tr>
                                                               <?php #} ?>
                                                               <?php //$minus = (strpos($sup_id->po_taxes_total_amt, '-') !== false) ? '-' : '';
                                                                //$po_taxes_total_amt = (strpos($sup_id->po_taxes_total_amt, '-') !== false) ? (str_replace('-','',$sup_id->po_taxes_total_amt)) : $sup_id->po_taxes_total_amt;
                                                                ?>
                                                               <tr>
                                                                  <td colspan="5" align="right"><b>Total Tax Amount</b></td>
                                                                  <td align="right"><?php echo $minus;?><b>&nbsp;₹&nbsp;</b><?php echo $item->po_taxes_total_amt;?></td>
                                                               </tr>
                                                               <tr>
                                                                  <td colspan="5" align="right"><b>Net Amount</b></td>
                                                                  <td align="right"><b>₹&nbsp;</b><?php echo $item->po_net_amt;?></td>
                                                               </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                           </td>
                                       </tr>                                        
                                    </tbody>
                                </table>
                            </div>                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->

<script type="text/javascript">
    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    $(document).ready(function() {
        // tab - pills toggle
        $('#show-tabs').click(function() {
            $this = $(this);
            if($this.prop('checked')){
                $("#widget-tab-1").slideDown('fast');
            } else {
                $("#widget-tab-1").slideUp('fast');
            }
            
        });
    });
</script>