<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) {
                    ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>
        <form class="edit_form" action="<?php echo $form_action; ?>" method="post" name="po" id="po_form">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	 
<!--                        <a href="<?php echo base_url(); ?>purchase_orders/" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>-->
                        <a href="<?php echo base_url(); ?>purchase_orders/" class="large_icon_des" style=" color: white; right: 46px;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="List"><i class="glyphicon glyphicon-list"></i></button></a>
                        <!--<a target="_blank" href="<?php echo base_url(); ?>purchase_orders/purchase_pdf/<?php echo $purchase->id; ?>" class="large_icon_des" style=" color: white;margin-right: 80px;" ><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="Download"><i class="glyphicon glyphicon-save"></i></button></a>-->
                        
                        

                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Purchase Order Edit </h2>
                                <!--<span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Purchase Order List"><a href="<?php echo base_url(); ?>purchase_orders/" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a>-->
                                </span>
                            </header>

                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">

                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" >
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>                                            
                                            <div class="row">
                                                <section class="col col-6">
                                                    <input type="hidden" class="form-control po_id" name="po_id" value="<?php echo $purchase->id; ?>" />
                                                    <input type="hidden" class="form-control supplier_gst" name="supplier_gst" value="<?php echo $purchase->sup_gst_type; ?>" />
                                                    <section>
                                                        <label class="label">Supplier Name<span style="color: red;">*</span>
<!--                                                        <span class="new-window-popup btn btn-primary pull-right" style="margin: 0 0 2px 0;" data-cmd="customer" data-title="Customer Add" data-iframe-src="<?php echo $add_supplier_link; ?>">
                                                                <i class="fa fa-plus-square-o" style="font-size: 16px;"> </i> Add Supplier
                                                            </span>-->
                                                        </label>
                                                        <label class="select">
                                                            <select name="sup_name" class="select2 sup_name" id="sup_name">
                                                                <option value="">Select Supplier name</option>
                                                                <?php foreach ($suppliers as $key => $sup) { ?>
                                                                    <option value="<?php echo $sup->id; ?>" <?php echo ($sup->id == $purchase->sup_id) ? 'selected' : ''; ?>><?php echo $sup->name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                        <input type="hidden" name="hidden_sup" class="hidden_sup" value="" />
                                                    </section>
                                                </section>

<!--                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Branch Name<span style="color: red;">*</span></label>
                                                        <label class="select">
                                                            <select name="branch_name" class="select2 branch_name">
                                                                <option value="">Select Branch</option>
                                                                <?php foreach ($branches as $key => $brn) { ?>
                                                                    <option value="<?php echo $brn->id; ?>" <?php echo ($brn->id == $purchase->brn_id) ? 'selected' : ''; ?>><?php echo $brn->branch_name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                        <input type="hidden" name="hidden_branch" class="hidden_branch" value="" />
                                                    </section>
                                                </section>  -->
                                            </div>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Contact Person<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-male"></i>
                                                            <input type="text" name="contact_person" id="contact_person" class="form-control contact_person" value="<?php echo $purchase->contact_name; ?>" />
                                                        </label>
                                                    </section>
                                                </section>

                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Contact Number<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-phone"></i>
                                                            <input type="text" name="contact_no" id="contact_no" class="form-control float_value contact_no" value="<?php echo $purchase->contact_no; ?>" maxlength="12"/>
                                                        </label>
                                                    </section>
                                                </section>
                                            </div>

                                            <div class="row">                                             
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Address<span style="color: red;"></span></label>
                                                        <label class="textarea">
                                                            <textarea rows="4" class="custom-scroll supplier_address" name="supplier_address"><?php echo $purchase->supplier_address; ?></textarea>
                                                        </label>
                                                    </section>
                                                </section> 
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Date<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="po_date" id="po_date" class="form-control bootstrap-datepicker-comncls" value="<?php echo date('d/m/Y', strtotime($purchase->po_date)); ?>" />
                                                        </label>
                                                    </section>
                                                </section>
                                            </div>




                                        </fieldset>
                                        
                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Item Details</legend>
                                            <div class="row product_search">
                                                <section class="col col-5">
                                                    <div class="input hidden-mobile">
                                                        <input name="search_product" class="form-control search_product lowercase" type="text" data-placeholder="Scan / Search Item by code or description" id="search_product" />
                                                    </div>
                                                </section>
                                            </div>
                                            <div class="row material-container product_details">
                                                <div class="col-xs-12">
                                                    <section class="col">
                                                        <span class="new-window-popup btn btn-primary" style="margin: 0 0 2px 0;" data-iframe-src="<?php echo $add_item_link; ?>" data-cmd="item" data-title="Item Add">
                                                            <i class="fa fa-plus-square-o" style="font-size: 16px;"> </i> Add Item
                                                        </span>
                                                    </section>
                                                </div>
                                                <div class="items_head col-xs-12 clr">
                                                    <section class="col col-2">
                                                        <label class="label"><b>Item Code</b></label>
                                                    </section>
                                                    <section class="col col-4">
                                                        <label class="label"><b>Brand Name - Description</b></label>
                                                    </section>
                                                    <section class="col col-1">
                                                        <label class="label"><b>Quantity</b></label>
                                                    </section>
                                                    <?php $unit_cost = (in_array(7, $permission)); ?>
						<?php if ($unit_cost) { ?>
                                                    <section class="col col-1">
                                                        <label class="label"><b>Selling Price</b></label>
                                                    </section>
						<?php } ?>
                                                    
                                                        <section class="col col-<?php echo ($unit_cost) ? '1' : '2'; ?>">
                                                            <label class="label"><b>Unit Cost</b></label>
                                                        </section>
                                                    

                                                    <section class="col col-2">
                                                        <label class="label"><b>Amount</b></label>
                                                    </section>
                                                </div>

                                                <?php $item_count = count($po_item); ?>
                                                <?php foreach ($po_item as $item_key => $item) { ?>
                                                    <div class="items col-xs-12 clr">
                                                        <input type="hidden" name="hidden_soitem_id[<?php echo $item_key; ?>]" value="<?php echo $item->id; ?>" />
                                                        <section class="col col-2">
                                                            <label class="input">
                                                                <input type="hidden" name="category_id[<?php echo $item_key; ?>]" class="category_id" value="<?php echo $item->category_id; ?>" />
                                                                <input type="hidden" name="product_id[<?php echo $item_key; ?>]" class="product_id" value="<?php echo $item->product_id; ?>" />
                                                                <input type="text" name="product_code[<?php echo $item_key; ?>]" class="product_code" value="<?php echo $item->pdt_code; ?>" data-value="<?php echo $item->pdt_code; ?>" data-placeholder="ITM_TV001" />
                                                            </label>
                                                            <span class="product_code_label">Selected Item code already exist!</span>
                                                        </section>

                                                        <section class="col col-4">
                                                            <label class="input">
                                                                <input type="text" name="product_description[<?php echo $item_key; ?>]" class="product_description" data-placeholder="Sony Sony Braviya TV" data-value="<?php echo form_prep($item->product_description); ?>" value="<?php echo form_prep($item->product_description); ?>" />
                                                            </label>
                                                            <span class="error_description">Selected Item description already exist!</span>

                                                            <span class="brand_label" style="display: none;">Item Code: <span class="brand_name"><?php echo $item->cat_name; ?></span></span>
                                                            <span style="float: right; margin-top: 1px;">
                                                                <b style="color: #333;font-size: 11px;">Supplier Item Code</b>
                                                                <label class="input" style="float: right; width: 63%;">
                                                                    <input type="text" class="sl_itm_code" name="sl_itm_code[<?php echo $item_key; ?>]" value="<?php echo $item->sl_itm_code; ?>" />
                                                                </label>
                                                            </span>
                                                        </section>

                                                        <section class="col col-1">
                                                            <label class="input">
                                                                <input type="text" name="quantity[<?php echo $item_key; ?>]" class="quantity float_value" value="<?php echo $item->quantity; ?>" data-placeholder="0" />

                                                                <input type="hidden" name="old_quantity[<?php echo $item_key; ?>]" class="quantity float_value" value="<?php echo $item->quantity; ?>" data-placeholder="0" />
                                                            </label>
                                                            <span class="unit_label"></span>
                                                        </section>

                                                        <section class="col col-<?php echo ($unit_cost) ? '1' : '2'; ?>" style="padding-left: 5px; padding-right: 5px;">
                                                            <label class="input">
                                                                <!--<i class="icon-prepend fa fa-dollar"></i>-->
                                                                <input type="text" name="price[<?php echo $item_key; ?>]" class="price float_value" value="<?php echo $item->price; ?>" data-placeholder="0.00" />
                                                            </label>
                                                        </section>

                                                        <section class="col col-1" style="padding-left: 5px; padding-right: 5px; display: <?php echo ($unit_cost) ? 'block' : 'none'; ?>;">
                                                            <label class="input" style="margin-top: 1px;">
                                                                <!--<i class="icon-prepend fa fa-dollar"></i>-->
                                                                <input type="text" name="cost[<?php echo $item_key; ?>]" class="cost float_value" value="<?php echo $item->unit_cost; ?>" data-placeholder="0.00" />
                                                            </label>
                                                        </section>

                                                        <section class="col col-2">
                                                            <label class="input">
                                                                <i class="icon-prepend fa fa-dollar"></i>
                                                                <input type="text" name="amount[<?php echo $item_key; ?>]" class="amount float_value" value="<?php echo $item->total; ?>" data-placeholder="0.00" readonly="" />
                                                            </label>
                                                        </section>

                                                        <section class="col col-1">
                                                            <label class="input">
                                                                <?php if ($item_count == ($item_key + 1)) { ?>
                                                                    <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                                <?php } ?>
                                                                <?php #if ($item_count > 1) { ?>
                                                                <i class="fa fa-2x fa-minus-circle remove-row"></i>
                                                                <?php #} ?>
                                                            </label>
                                                        </section>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <div class="row" style="margin-top: 2%;">
                                                <section class="col col-6">
                                                </section>
                                                <section class="col col-3">
                                                    <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Sub Total</b></label>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="sub_total" class="sub_total" value="<?php echo $purchase->po_sub_total; ?>" readonly="" data-placeholder="0.00" />
                                                    </label>
                                                </section>
                                                <section class="col col-1">
                                                </section>
                                            </div>

                                            <div class="row" style="margin-top: 2%;">
                                                <section class="col col-6">
                                                </section>
                                                <section class="col col-3">
                                                    <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Discount</b></label>
                                                </section>
                                                <section class="col col-2">
                                                    <!--<label class="input">
                                                        <i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="sub_total" class="sub_total" readonly="" data-placeholder="0.00" />
                                                    </label>-->

                                                    <div class="input-group">
                                                        <input type="text" name="discount" class="form-control discount float_value" value="<?php echo $purchase->po_discount; ?>" data-placeholder="0.00" />
                                                        <span class="input-group-addon">
                                                            <span class="onoffswitch">
                                                                <input type="checkbox" name="discount_type" class="onoffswitch-checkbox discount_type" id="discount_type-0" value="1" <?php echo ($purchase->po_discount_type) ? 'checked' : ''; ?> />
                                                                <label class="onoffswitch-label" for="discount_type-0"> 
                                                                    <span class="onoffswitch-inner" data-swchon-text="%" data-swchoff-text="S$"></span> 
                                                                    <span class="onoffswitch-switch"></span> 
                                                                </label> 
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <input type="hidden" name="discount_amt" class="discount_amt" value="<?php echo $purchase->po_total_discount; ?>" />
                                                    <span class="discount_container">
                                                        <span class="currency">S$</span>
                                                        <span class="discount_amount"><?php echo (!empty($purchase->po_total_discount))?$purchase->po_total_discount:'0.00'; ?></span>
                                                    </span>
                                                </section>
                                                <section class="col col-1">
                                                </section>
                                            </div>

                                            <div class="row gst_details">
                                                <section class="col col-4">
                                                </section>
                                                <section class="col col-2">&nbsp;</section>
                                                <section class="col col-3">
                                                    <section>
                                                        <label class="label">&nbsp;</label>
                                                        <div class="inline-group">
                                                            <label class="radio">
                                                                <input type="radio" class="gst_type without_gst" name="gst_type" value="1" <?php echo ($purchase->gst_type == 1) ? 'checked' : ''; ?> /><i></i>Include GST
                                                            </label>
                                                            <label class="radio">
                                                                <input type="radio" class="gst_type with_gst" name="gst_type" value="2" <?php echo ($purchase->gst_type == 2) ? 'checked' : ''; ?> /><i></i>Exclude GST
                                                            </label>
                                                        </div>
                                                    </section>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="label">&nbsp;</label>
                                                    <label class="input change_perc_disamt"><i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="gst_amount" class="gst_amount float_value" value="<?php echo $purchase->gst_total; ?>" data-placeholder="0.00" readonly="" />
                                                        <input type="hidden" value="<?php echo $purchase->gst_val; ?>" name="hidden_gst" id="hidden_gst" />
                                                    </label>
                                                    <span class="gst_val" style="display: <?php echo (!empty($purchase->gst_val)) ? 'block' : 'none'; ?>">GST @ <span class="gst_percentage"><?php echo $purchase->gst_val; ?></span> %</span>
                                                </section>
                                            </div>

                                            <div class="row" style="margin-top: 2%;">
                                                <section class="col col-6">
                                                </section>
                                                <section class="col col-3">
                                                    <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Total Amount</b></label>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="total_amt" class="total_amt" value="<?php echo $purchase->po_net_amount; ?>" readonly="" data-placeholder="0.00" />
                                                    </label>
                                                </section>
                                                <section class="col col-1">
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Remarks</label>
                                                    <label class="input textarea-expandable">
                                                        <textarea class="form-control po_remarks show_txt_count" name="po_remarks" maxlength="500" data-placeholder="Remarks" rows="2" ><?php echo (!empty($purchase->po_remarks)) ? $purchase->po_remarks : ''; ?></textarea>
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Status<span style="color: red;">*</span></label>
                                                    <label class="select">
                                                        <select  name="status" class="select2">
                                                            <option value="1" <?php echo($purchase->po_status == 1) ? 'selected' : ''; ?>>Active</option>
                                                            <option value="2" <?php echo($purchase->po_status == 2) ? 'selected' : ''; ?>>Inactive</option>
                                                        </select>
                                                    </label>
                                                </section>

                                            </div>
                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn btn-primary" >Submit</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                    <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/purchase_order.js"></script>
