<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>deposit/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2><?php echo 'Deposit Edit'; ?></h2>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="edit_form" action="<?php echo $form_action; ?>" method="post" id="deposit_form" name="deposit_form" enctype="multipart/form-data">
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">

                                        <fieldset>                                      
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>
                                            <div class="row">  
                                                <input type="hidden" class="form-control deposit_id" name="deposit_id" value="<?php echo $deposit->id; ?>" />
                                                <input type="hidden" class="form-control old_dep_amount" name="old_dep_amount" value="<?php echo $deposit->dep_amount; ?>" />
                                                <input type="hidden" class="form-control" name="bank_acc_id" value="<?php echo $deposit->bank_acc_id; ?>" />
                                                <div class="col col-6">
                                                    <section>
                                                        <label class="label">Account No. <span style="color: red">*</span> <span style="position: absolute;right: 15px;" class="crnt_bln"></span></label>
                                                        <select name="bank_acc_id_dis" class="select2 bank_acc_id" id="bank_acc_id" disabled="">
                                                            <option value="" data-crntbln="" >Select Account Number</option>
                                                            <?php foreach ($all_bank as $key => $bank) { ?>
                                                            <option value="<?php echo $bank->id; ?>" data-crntbln="<?= $bank->current_balance ?>" data-bnktext="<?= $bank->bank_name ?>" <?= ($bank->id==$deposit->bank_acc_id) ? "selected" : ""; ?> ><?php echo $bank->bank_name.' / '.$bank->account_no; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </section>
                                                    <section>
                                                        <label class="label">Date <span style="color: red">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="dep_date" id="dep_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php echo $deposit->dep_date_str; ?>" />
                                                        </label>
                                                    </section> 
                                                    <section>
                                                        <label class="label">Amount <span style="color: red">*</span></label>
                                                        <label class="input"> <i class="icon-append fa fa-money"></i>
                                                            <input type="text" data-placeholder="200" class="dep_amount float_value" name="dep_amount" id="dep_amount" value="<?php echo $deposit->dep_amount; ?>" >
                                                        </label>
                                                    </section> 
                                                    <section>
                                                        <label class="label">Remarks </label>
                                                        <label class="textarea textarea-expandable"><textarea rows="3" name="dep_remarks" id="dep_remarks" maxlength="500"><?php echo $deposit->dep_remarks; ?></textarea> </label> <div class="note">
                                                            <strong>Note:</strong> expands on focus.
                                                        </div>
                                                    </section> 
                                                    <!--<section>
                                                        <label class="label">Status<span style="color: red">*</span></label>
                                                        <label class="select">
                                                            <select name="status" id="status" class="select2">
                                                                <option value="1" <?php echo ($deposit->status_str == 'Active') ? 'selected' : ''; ?>>Active</option>
                                                                <option value="2"<?php echo ($deposit->status_str == 'Inactive') ? 'selected' : ''; ?>>Inactive</option>
                                                            </select>
                                                        </label>
                                                    </section>-->
                                                </div>
                                                <div class="col col-6">
                                                    <section>
                                                        <label class="label">Payment Type <span style="color:red">*</span></label>
                                                        <label class="select">
                                                            <select name="payment_type"  class="select2 payment_type" id="payment_type">
                                                                <option value="">Select Payment Type</option>
                                                                <?php foreach($payment_types as $key => $value) {?>
                                                                    <option value="<?php echo $value->id;?>" data-is_cheque="<?php echo $value->is_cheque;?>" <?= ($value->id==$deposit->payment_type) ? "selected" : ""; ?>><?php echo $value->type_name;?></option>
                                                                <?php }?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                    <section class="cheque_details">
                                                        <label class="label">Bank Name <span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="form-control cheque_bank_name" name="cheque_bank_name" data-placeholder="SBI" maxlength="100" value="<?php echo $deposit->cheque_bank_name; ?>"/>
                                                        </label>
                                                    </section>
                                                     <section class="cheque_details">
                                                        <label class="label">Cheque No <span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="form-control" name="cheque_acc_no" data-placeholder="Cheque No" maxlength="100" value="<?php echo $deposit->cheque_acc_no; ?>" />
                                                        </label>
                                                    </section>
                                                     <section class="cheque_details">
                                                        <label class="label">Cheque Date <span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="bootstrap-datepicker-strdatecls" name="cheque_date" data-placeholder="01/01/2016" maxlength="100" value="<?php echo $deposit->cheque_date_str; ?>" />
                                                        </label>
                                                    </section> 
                                                </div>
                                                                                             
                                            </div>

                                            <div class="row">
                                                
                                            </div>										

                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/deposit.js"></script>
