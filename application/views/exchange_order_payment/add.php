<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) {
                    ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>

        <form action="<?php echo $form_action; ?>" name="exchange_order_payment_form" id="exchange_order_payment_form" method="post">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                        <a href="<?php echo $list_link; ?>" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">

                            <header>
                                <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                <h2>Exchange Order Payment Add </h2>
                            </header>
                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" >
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                            <div class="row">
                                                <input type="hidden" class="form-control pay_id" name="pay_id" value="" />
                                                <section class="col col-6">													
                                                    <section>
                                                        <label class="label">Supplier Name<span style="color:red">*</span></label>
                                                        <label class="select">
                                                            <select name="supplier" id="supplier" class="select2" autofocus>
                                                                <option value="">Select Supplier</option>
                                                                <?php foreach ($suppliers as $key => $value) { ?>
                                                                    <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                    </section>

                                                    <section>
                                                        <label class="label">Pay Date<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <div class="input-prepend input-group">
                                                                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                                <input type="text" name="pay_date" id="pay_date" class="bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php echo date('d/m/Y'); ?>" />
                                                            </div>
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Payment Type <span style="color:red">*</span></label>
                                                        <label class="select">
                                                            <select name="payment_type"  class="select2 payment_type" id="payment_type">
                                                                <option value="">Select Payment Type</option>
                                                                <?php foreach ($payment_types as $key => $value) { ?>
                                                                    <option value="<?php echo $value->id; ?>" data-is_cheque="<?php echo $value->is_cheque; ?>"><?php echo $value->type_name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                    <section class="cheque_details">
                                                        <label class="label">Bank Name<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="form-control" name="cheque_bank_name" data-placeholder="SBI" maxlength="100" value="<?php echo $bnk_acc_name; ?>" />
                                                        </label>
                                                    </section>
                                                    <section class="cheque_details">
                                                        <label class="label">Cheque No<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="form-control" name="cheque_acc_no" data-placeholder="Cheque No" maxlength="100" />
                                                        </label>
                                                    </section>
                                                    <section class="cheque_details">
                                                        <label class="label">Cheque Date<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="bootstrap-datepicker-strdatecls" name="cheque_date" data-placeholder="01/01/2016" maxlength="100" />
                                                        </label>
                                                    </section>
                                                </section>
                                                <section class="col col-6">
                                                    <div class="text-center loader" style="display: none;">
                                                        <img src="<?php echo base_url(); ?>assets/images/ajax-loader.gif" alt="Loading..." />
                                                    </div>
                                                    <fieldset id="exc_order" class="no-padding" style="width: 100%; max-height: 300px; overflow: auto;">
                                                        <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Payment Pending Exchange Orders</legend>
                                                        <div class="items_head col-xs-12 clr">
                                                            <section class="col col-1 no-padding" style="padding-left: 3px !important;">
                                                                <label class="checkbox" style="min-width: 75px;">
                                                                    <input type="checkbox" name="check_all" id="check_all" value="1" />
                                                                    <i></i><b>All</b>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label"><b>ExO. No.</b></label>
                                                            </section>
                                                            <section class="col col-2">
                                                                <label class="label"><b>ExO. Date</b></label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label" style="text-align: center;"><b>Amount</b></label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label" style="text-align: center;"><b>Pay Amount</b></label>
                                                            </section>
                                                        </div>

                                                        <div class="no-items col-xs-12 clr">
                                                            <p class="text-center">No Record Found!</p>
                                                        </div>
                                                    </fieldset>
                                                    <div class="col-xs-12 total_amt_row" style="display: none; padding: 10px 0;">
                                                        <section class="col col-9">
                                                            <label class="label text-right"><b>Total Amount</b></label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label text-right total_amount" style="padding-right: 27px;"></label>
                                                        </section>
                                                    </div>
                                                </section>
                                            </div>
                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn btn-primary" >Submit</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                    <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/exchange_order_payment.js"></script>