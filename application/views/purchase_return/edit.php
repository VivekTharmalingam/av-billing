<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if(!empty($success)) {?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                            <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                            <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php }
                else if(!empty($error)) {?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error;?>
                    </div>
                <?php }?>&nbsp;
            </div>
        </div>

        <form class="edit_form" action="<?php echo $form_action;?>" method="post" name="por" id="por_form">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                        <a href="<?php echo $list_link; ?>" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">

                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Purchase Return Edit </h2>
<!--                                <span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Purchase Return List"><a href="<?php echo base_url(); ?>purchase_returns/" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a>
                                </span>-->
                            </header>

                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" >
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <input type="hidden" class="form-control por_id" name="por_id" value="<?php echo $return->id;?>" />
                                                    <section>
                                                        <label class="label">Goods Receive No. / Invoice No.<span style="color: red;">*</span><span style="float:right;">Purchase Return No : <b style="color: #3276B1;"><?php echo $return->por_no;?></b></span></label>
                                                        <label class="select">
                                                            <select name="po_id" class="select2 po_id" disabled>
                                                                <option value="">Select Goods Receive No. / Invoice No.</option>
                                                                <?php foreach($purchases as $key=>$purchase) {?>
                                                                    <option value="<?php echo $purchase->id;?>" <?php echo ($purchase->id == $return->precv_id) ? 'selected' : '';?>>
																		<?php echo $purchase->pr_code;?>
																		<?php echo (!empty($purchase->supplier_invoice_no)) ? ' / ' . $purchase->supplier_invoice_no : ''; ?>
																	</option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                        <input type="hidden" name="hidden_po_id" class="hidden_po_id" value="<?php echo $return->precv_id;?>" />
                                                        <input type="hidden" name="hidden_brn_id" class="hidden_brn_id" value="<?php echo $user_data['branch_id'];?>" />
                                                    </section>
													
                                                    <section>
                                                        <label class="label">Date<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="por_date" id="por_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php echo date('d/m/Y',strtotime($return->return_date));?>" />
                                                        </label>
                                                    </section>
                                                </section>
                                                <section class="col col-6 sup_details" style="display: none;">
                                                    <section class="alert alert-info" id="supplier_details" style="border-top-width: 5px;border-left-width:0px;background: #d498de;border-color: #9c27b0;">
														<label class="label" style="font-size: 11px;"><b>Supplier Details</b></label>
														<label class="label" style="font-size: 11px;">Supplier Name: <span class="supplier_name"></span></label>
														<label class="label" style="font-size: 11px;">Contact Person: <span class="supplier_cname"></span></label>
														<label class="label" style="font-size: 11px;">Contact No: <span class="supplier_cno"></span></label>
														<label class="label" style="font-size: 11px;">Address: <span class="supplier_address"></span></label>
                                                    </section>
                                                 </section>
											</div>
                                        </fieldset>
                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Items Details</legend>
                                            <div class="row product_search">
                                                <section class="col col-5">
                                                    <div class="input hidden-mobile">
                                                        <input name="search_product" class="form-control search_product" type="text" data-placeholder="Scan / Search Item by code or description" id="search_product" tabindex="-1" />
                                                    </div>
                                                </section>
                                            </div>
                                            <div class="row material-container product_details">
                                                <div class="items_head col-xs-12 clr">
                                                    <section class="col col-3">
                                                        <label class="label">
                                                            <b>Item Code</b>
                                                        </label>
                                                    </section>
                                                    <section class="col col-4">
                                                        <label class="label"><b>Brand Name - Description</b></label>
                                                    </section>
                                                    <section class="col col-2">
                                                        <label class="label"><b>Received Qty</b></label>
                                                    </section>
                                                    <section class="col col-2">
                                                        <label class="label"><b>Return Qty</b></label>
                                                    </section>
                                                </div>
                                                
                                                <?php $length = count($return_items);?>
												<?php foreach($return_items as $key=> $items) {?>
												<div class="items col-xs-12 clr">
                                                    <section class="col col-3">
                                                        <label class="input">
                                                            <input type="hidden" name="pori_id[]" class="pori_id" value="<?php echo $items->id;?>" />
                                                            <input type="hidden" name="category_id[]" class="category_id" value="<?php echo $items->category_id;?>" />
                                                            <input type="hidden" name="product_id[]" class="product_id" value="<?php echo $items->product_id;?>" />
                                                            <input type="hidden" name="por_qty[]" class="por_qty" value="<?php echo $items->por_quantity;?>" />
                                                            <input type="text" name="product_code[]" class="product_code" data-value="<?php echo $items->pdt_code;?>" value="<?php echo $items->pdt_code;?>" />
                                                        </label>
                                                        <span class="product_code_label">Selected Item code already exist!</span>
                                                    </section>

                                                    <section class="col col-4">
                                                        <label class="input">
                                                            <input type="text" name="product_description[]" class="product_description" data-value="<?php echo $items->product_description;?>" value="<?php echo $items->product_description;?>" />
                                                        </label>
                                                        <span class="error_description">Selected Item description already exist!</span>
                                                    </section>
                                                    
                                                    <section class="col col-2">
                                                        <label class="input">
                                                            <input type="text" name="rec_quantity[]" class="rec_quantity float_value"  readonly="" value="<?php echo $items->received_quantity;?>"/>
                                                        </label>
                                                        <!--<span class="unit_label"></span>-->
														<span class="received_quantity" style="display: <?php echo ($items->already_returned_qty > 0) ? 'block' : 'none';?>;">Already Returned : <span class="recd_qty"><?php echo $items->already_returned_qty;?></span></span>
                                                    </section>
                                                    
                                                    <section class="col col-2">
                                                        <label class="input">
                                                            <input type="text" name="retn_quantity[]" class="retn_quantity float_value" value="<?php echo $items->returned_quantity;?>"/>
                                                        </label>
                                                        <span class="unit_label"></span>
                                                    </section>
                                                    <section class="col col-1">
                                                        <?php if ($length == ($key + 1)) {?>
                                                            <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                        <?php }?>
                                                        <?php if ($length > 1) {?>
                                                            <i class="fa fa-2x fa-minus-circle remove-row"></i>
                                                        <?php }?>
                                                    </section>
                                                </div>
                                               <?php }?>
                                            </div>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">&nbsp;</legend>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Remarks</label>
                                                    <label class="input textarea-expandable">
                                                        <textarea class="form-control por_remarks show_txt_count" name="por_remarks" maxlength="500" rows="2"><?php echo $return->po_ret_remarks;?></textarea>
                                                    </label>
                                                </section>
                                            </div>
                                        </fieldset> 

                                        <footer>
                                            <button type="submit" class="btn btn-primary" >Submit</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/app/purchase_return.js"></script>