<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) {
                    ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>

        <form class="add_form" action="<?php echo $form_action; ?>" method="post" name="por" id="por_form">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                        <a href="<?php echo $list_link; ?>" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">

                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Purchase Return Add </h2>
<!--                                <span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Purchase Return List"><a href="<?php echo base_url(); ?>purchase_returns/" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a>
                                </span>-->
                            </header>

                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" >
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <input type="hidden" class="form-control por_id" name="por_id" value="" />
                                                    <section>
                                                        <label class="label">Goods Receive No. / Invoice No.<span style="color: red;">*</span></label>
                                                        <label class="select">
                                                            <select name="po_id" class="select2 po_id" id="po_id">
                                                                <option value="">Select Goods Receive No. / Invoice No.</option>
                                                                <?php foreach ($purchases as $key => $purchase) { ?>
                                                                    <option value="<?php echo $purchase->id; ?>">
																		<?php echo $purchase->pr_code;?>
																		<?php echo (!empty($purchase->supplier_invoice_no)) ? ' / ' . $purchase->supplier_invoice_no : ''; ?>
																	</option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                        <input type="hidden" name="hidden_po_id" class="hidden_po_id" value="" />
                                                        <input type="hidden" name="hidden_brn_id" class="hidden_brn_id" value="<?php echo $user_data['branch_id']; ?>" />
                                                    </section>
													<section>
                                                        <label class="label">Date<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="por_date" id="por_date" class="form-control bootstrap-datepicker-comncls" value="<?php echo date('d/m/Y'); ?>" />
                                                        </label>
                                                    </section>
                                                </section>
                                                <section class="col col-6 sup_details" style="display: none;">
                                                    <section class="alert alert-info" id="supplier_details" style="border-top-width: 5px;border-left-width:0px;background: #d498de;border-color: #9c27b0;">
														<label class="label" style="font-size: 11px;"><b>Supplier Details</b></label>
														<label class="label" style="font-size: 11px;">Supplier Name: <span class="supplier_name"></span></label>
														<label class="label" style="font-size: 11px;">Contact Person: <span class="supplier_cname"></span></label>
														<label class="label" style="font-size: 11px;">Contact No: <span class="supplier_cno"></span></label>
														<label class="label" style="font-size: 11px;">Address: <span class="supplier_address"></span></label>
                                                    </section>
                                                 </section>
                                            </div>
                                            <!--<div class="row" style="display: none;">
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Supplier Name<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <input type="text" name="supplier_name" id="supplier_name" class="form-control supplier_name" data-placeholder="Supplier Name" value="" readonly="" />
                                                        </label>
                                                    </section>
                                                </section> 
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Contact Person<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-male"></i>
                                                            <input type="text" name="contact_person" id="contact_person" class="form-control contact_person" data-placeholder="Name" value="" readonly="" />
                                                        </label>
                                                    </section>
                                                </section>

                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Contact Number<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-phone"></i>
                                                            <input type="text" name="contact_no" id="contact_no" class="form-control contact_no" data-placeholder="Phone" value="" maxlength="12" readonly=""/>
                                                        </label>
                                                    </section>
                                                </section>
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Address<span style="color: red;"></span></label>
                                                        <label class="textarea textarea-expandable ">
                                                            <textarea rows="3" class="custom-scroll supplier_address" data-placeholder="Address" name="supplier_address" readonly=""></textarea>
                                                        </label>
                                                    </section>
                                                </section> 
                                            </div>-->                                     
                                        </fieldset>

<!--                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Items Details</legend>
                                            <div class="row material-container product_details" style="background: #ebebeb; padding-top:15px;">
                                                <div class="items clr">
                                                    <section class="col col-5">
                                                        <label class="label"><b>Item Description</b></label>
                                                        <label class="select">
                                                            <select name="mat_id[]" class="select2 mat_id">
                                                                <option value="">Select</option>
                                                            </select>
                                                        </label>
                                                    </section>

                                                    <section class="col col-2" style="display: none;">
                                                        <label class="label"><b>Unit</b></label>
                                                        <label class="input">
                                                            <input type="text" name="unit_name[]" class="unit_name" data-placeholder="TON" readonly="" />
                                                        </label>
                                                        <input type="hidden" name="unit_id[]" class="unit_id" />
                                                    </section>



                                                    <section class="col col-3">
                                                        <label class="label"><b>Received Quantity</b></label>
                                                        <label class="input">
                                                            <input type="hidden" name="grni_quantity[]" class="grni_quantity" />
                                                            <input type="text" name="received_quantity[]" class="received_quantity" data-placeholder="0" readonly="" />
                                                        </label>
                                                    </section>

                                                    <section class="col col-3">
                                                        <label class="label"><b>Returned Quantity</b></label>
                                                        <label class="input">
                                                            <input type="text" name="returned_quantity[]" class="returned_quantity float_value" data-placeholder="0" />
                                                        </label>
                                                    </section>

                                                    <section class="col col-1">
                                                        <label class="label">&nbsp;</label>
                                                        <label class="input">
                                                            <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                        </label>
                                                    </section>
                                                </div>
                                            </div>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">&nbsp;</legend>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Remarks</label>
                                                    <label class="input textarea-expandable">
                                                        <textarea class="form-control por_remarks show_txt_count" name="por_remarks" maxlength="500" data-placeholder="Remarks" rows="2" ></textarea>
                                                    </label>
                                                </section>

                                                <section class="col col-6">
                                                    <label class="label">Status<span style="color: red;">*</span></label>
                                                    <label class="select">
                                                        <select  name="status" class="select2">
                                                            <option value="1">Active</option>
                                                            <option value="2">Inactive</option>
                                                        </select>
                                                    </label>
                                                </section>

                                            </div>

                                        </fieldset>-->
                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Items Details</legend>
                                            <div class="row product_search">
                                                <section class="col col-5">
                                                    <div class="input hidden-mobile">
                                                        <input name="search_product" class="form-control search_product" type="text" data-placeholder="Scan / Search Item by code or description" id="search_product" tabindex="-1" />
                                                    </div>
                                                </section>
                                            </div>
                                            <div class="row material-container product_details">
<!--                                                <div class="col-xs-12">
                                                    <section class="col">
                                                        <span class="new-window-popup btn btn-primary" data-iframe-src="<?php echo $add_item_link; ?>" data-cmd="item" data-title="Item Add">
                                                            <i class="fa fa-plus-square-o" style="font-size: 16px;"> </i> Add Item
                                                        </span>
                                                    </section>
                                                </div>-->

                                                <div class="items_head col-xs-12 clr">
                                                    <section class="col col-3">
                                                        <label class="label">
                                                            <b>Item Code</b>
                                                        </label>
                                                    </section>
                                                    <section class="col col-4">
                                                        <label class="label"><b>Brand - Description</b></label>
                                                    </section>
                                                    <section class="col col-2">
                                                        <label class="label"><b>Received Qty</b></label>
                                                    </section>
                                                    <section class="col col-2">
                                                        <label class="label"><b>Return Qty</b></label>
                                                    </section>
                                                </div>

                                                <div class="items col-xs-12 clr">
                                                    <section class="col col-3">
                                                        <label class="input">
                                                            <input type="hidden" name="category_id[0]" class="category_id" value="" />
                                                            <input type="hidden" name="product_id[0]" class="product_id" value="" />
                                                            <input type="hidden" name="por_qty[0]" class="por_qty" value="" />
                                                            <input type="text" name="product_code[0]" class="product_code" data-value="" data-placeholder="ITM_TV001" />
                                                        </label>
                                                        <span class="product_code_label">Selected Item code already exist!</span>
                                                    </section>

                                                    <section class="col col-4">
                                                        <label class="input">
                                                            <input type="text" name="product_description[0]" class="product_description" data-value="" data-placeholder="Sony Sony Braviya TV" />
                                                        </label>
                                                        <span class="error_description">Selected Item description already exist!</span>
                                                    </section>
                                                    
                                                    <section class="col col-2">
                                                        <label class="input">
                                                            <input type="text" name="rec_quantity[0]" class="rec_quantity float_value" data-placeholder="0"  readonly=""/>
                                                        </label>
                                                        <span class="received_quantity" style="display: none;">Already Returned : <span class="recd_qty"></span></span>
                                                    </section>
                                                    
                                                     <section class="col col-2">
                                                        <label class="input">
                                                            <input type="text" name="retn_quantity[0]" class="retn_quantity float_value" data-placeholder="0"/>
                                                        </label>
                                                        <span class="unit_label"></span>
                                                    </section>

                                                    <section class="col col-1">
                                                        <label class="input">
                                                            <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                        </label>
                                                    </section>
                                                </div>
                                            </div>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">&nbsp;</legend>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Remarks</label>
                                                    <label class="input textarea-expandable">
                                                        <textarea class="form-control por_remarks show_txt_count" name="por_remarks" maxlength="500" rows="2"></textarea>
                                                    </label>
                                                </section>
                                                <!--<section class="col col-6">
                                                    <label class="label">Status<span style="color: red;">*</span></label>
                                                    <label class="select">
                                                        <select  name="status" class="select2">
                                                            <option value="1">Active</option>
                                                            <option value="2">Inactive</option>
                                                        </select>
                                                    </label>
                                                </section>-->
                                            </div>
                                        </fieldset>  
                                        <footer>
                                            <button type="submit" class="btn btn-primary" >Submit</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                    <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/purchase_return.js"></script>