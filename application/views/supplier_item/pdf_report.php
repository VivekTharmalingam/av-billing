<table border="0" cellpadding="5" cellspacing="0" style="font-family:calibri; font-size:14px; width: 1000px;">
    <?php echo $head; ?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>        
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>
<table width="100%" border="1" style="border-collapse:collapse; margin-left:10px; font-family: calibri;" cellpadding="5">
    <thead>
        <tr style="font-size:16px;">
            <th width="6%">S.NO</th>
            <th width="25%">Brand / Category</th>
<!--            <th width="10%">Code</th>-->
            <th width="17%">Item Description</th>
<!--            <th width="18%">Description</th>                    -->
            <th width="10%">Buying Price</th>
            <th width="10%">Selling Price</th>
        </tr>
    </thead>
    <tbody>
        <?php if (count($item)) { ?>
        <?php  foreach ($item as $key => $row) { ?>                   
            <tr>
                <td style="text-align:center"><?php echo ++$key; ?></td>
                <td><?php echo $row->cat_name. '&nbsp;/&nbsp;'.$row->item_category_name ; ?></td>
<!--                <td><?php //echo text($row->pdt_code); ?></td>  -->
                <td><?php echo text($row->pdt_name); ?></td>
<!--                <td><?php //echo text($row->product_description); ?></td>-->
                <td style="text-align: right;"><?php echo text($row->buying_price); ?></td>
                <td style="text-align: right;"><?php echo text($row->selling_price); ?></td>             
            </tr>
        <?php } ?>
    <?php } else { ?>
        <tr>
            <td colspan="6" align="center">No Record Found</td>
        </tr>
    <?php } ?>
    </tbody>
</table>