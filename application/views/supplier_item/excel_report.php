<?php
$ti = time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Supplier_Wise_Item_Report" . $ti . ".xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; margin-left:10px">
<!--    <tr>
        <td width="100%" style="vertical-align:top; padding: 0; text-align: center; font-weight: bold;font-size: 20px;" colspan="6" >SUPPLIERS OF <?php echo $product->pdt_code.' / '.$product->pdt_name; ?></td>
    </tr>-->
    <tr style="font-family:Times new roman; font-weight:bold;">
        <th width="6%" style="padding:10px">S.NO</th>
        <th width="25%">Brand / Category</th>
<!--        <th width="10%">Code</th>-->
        <th width="17%">Item Description</th>
<!--        <th width="18%">Description</th>                    -->
        <th width="10%">Buying Price</th>
        <th width="10%">Selling Price</th>
    </tr>
    <?php if (count($item)) { ?>
        <?php foreach ($item as $key => $row) { ?>                   
            <tr>
                <td style="text-align:center"><?php echo ++$key; ?></td>
                <td><?php echo $row->cat_name. '&nbsp;/&nbsp;'.$row->item_category_name ; ?></td>
<!--                <td><?php //echo text($row->pdt_code); ?></td>  -->
                <td><?php echo text($row->pdt_name); ?></td>
<!--                <td><?php //echo text($row->product_description); ?></td>-->
                <td style="text-align: right;"><?php echo text($row->buying_price); ?></td>
                <td style="text-align: right;"><?php echo text($row->selling_price); ?></td>               
            </tr>
        <?php } ?>
    <?php } else { ?>
        <tr>
            <td colspan="6" align="center">No Record Found</td>
        </tr>
    <?php } ?>
</table>