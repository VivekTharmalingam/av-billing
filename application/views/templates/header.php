<?php
    $user_opts = json_decode( $user_data['user']['user_opts'] );
    $themeId = (!empty($user_opts->themeId)) ? $user_opts->themeId : '';
    $current_controller = (!empty($current_controller)) ? $current_controller : '';
    $menu = (!empty($menu)) ? $menu : $current_controller;
    $menu_data = array(
        'user_data'     => $user_data,
        'permissions'   => $permissions,
        'menu'          => $menu
    );
    //echo '<pre>';print_r($get_permissions_struct);echo '</pre>';die();
    $permissions_struct = get_permissions_struct();
    
    $mod_array = array();
    $temp_cls ='dashboard_cls'; 
    foreach ($permissions_struct as $mk => $m) {
        $me = explode('`', $m['label']);
        $mslug = get_var_name($me[0], '_');
        if (count($m) > 0){ 
            $i=0;
            foreach ($m as $prefix => $item){
                if (!is_array($item)) 
                    continue;
                
                $ie = explode('`', $item['label']);
                $islug = get_var_name($ie[0], '_');
                $cntrlr = !empty($item['controller']) ? $item['controller'] : $islug;
                if($cntrlr ==$current_controller){
                    $temp_cls = $mslug.'_cls';
                }                               
                $mod_array[$mslug][$i] = $cntrlr;
                $i++;
            }            
        }
    }
	
	
?>
<script type="text/javascript">
    var current_controller = '<?php echo $current_controller; ?>';
</script>
<body class="<?php echo $user_opts->class . ' ' . $themeId . ' ' . trim($temp_cls); ?> " >
        <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
        <!-- HEADER -->
        <header id="header">
            <div id="logo-group">
				<?php  
				if ((!empty($company_data->inner_logo)) && (file_exists(FCPATH . DIR_COMPANY_LOGO . $company_data->inner_logo))) 
					$logo_url = BASE_URL . DIR_COMPANY_LOGO . $company_data->inner_logo;
				else 
					$logo_url = BASE_URL . 'assets/images/logo.png';
				?>
                <!-- PLACE YOUR LOGO HERE -->
				<a href="<?php echo base_url();?>">
                    <span id="logo" ><img src="<?php echo $logo_url;?>" alt="Universal Computers" style="width:225px; height: 50px; padding: 0;"></span>
				</a>
                <!-- END LOGO data-placeholder -->

                <!-- Note: The activity badge color changes when clicked and resets the number to 0
                Suggestion: You may want to set a flag when this happens to tick off all checked messages / notifications -->
                
                <!-- It's Important Code.. Plz Use it Govind -->
                <!--<span id="activity" class="activity-dropdown"> <i class="fa fa-user"></i> <b class="badge"> 21 </b> </span>-->

                <!-- AJAX-DROPDOWN : control this dropdown height, look and feel from the LESS variable file -->
                <div class="ajax-dropdown">

                    <!-- the ID links are fetched via AJAX to the ajax container "ajax-notifications" -->
                    <div class="btn-group btn-group-justified" data-toggle="buttons">
                        <label class="btn btn-default">
                            <input type="radio" name="activity" id="ajax/notify/mail.html">
                            Msgs (14) </label>
                        <label class="btn btn-default">
                            <input type="radio" name="activity" id="ajax/notify/notifications.html">
                            notify (3) </label>
                        <label class="btn btn-default">
                            <input type="radio" name="activity" id="ajax/notify/tasks.html">
                            Tasks (4) </label>
                    </div>

                    <!-- notification content -->
                    <div class="ajax-notifications custom-scroll">
                        <div class="alert alert-transparent">
                            <h4>Click a button to show messages here</h4>
                            This blank page message helps protect your privacy, or you can show the first message here automatically.
                        </div>

                        <i class="fa fa-lock fa-4x fa-border"></i>

                    </div>
                    <!-- end notification content -->

                    <!-- footer: refresh area -->
                    <span> Last updated on: 12/12/2013 9:43AM
                        <button type="button" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading..." class="btn btn-xs btn-default pull-right">
                            <i class="fa fa-refresh"></i>
                        </button> </span>
                    <!-- end footer -->

                </div>
                <!-- END AJAX-DROPDOWN -->
            </div>

            <!-- projects dropdown -->
            <div class="project-context hidden-xs">
<?php //print_r($user_data);?>
                <span class="label">&nbsp;</span>
                <!-- It's Important Code.. Plz Use it Govind -->
                                <!--<span class="project-selector dropdown-toggle" data-toggle="dropdown" style="font-size: 12px;">Recent projects <i class="fa fa-angle-down"></i></span>-->

                <!-- Suggestion: populate this list with fetch and push technique -->
                <ul class="dropdown-menu">
                    <li>
                        <a href="javascript:void(0);">Online e-merchant management system - attaching integration with the iOS</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">Notes on pipeline upgradee</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">Assesment Report for merchant account</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="javascript:void(0);"><i class="fa fa-power-off"></i> Clear</a>
                    </li>
                </ul>
                <!-- end dropdown-menu-->

            </div>
            <!-- end projects dropdown -->

            <!-- pulled right: nav area -->
            <div class="pull-right">
                
                <!-- collapse menu button -->
                <div id="hide-menu" class="btn-header pull-right">
                    <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
                </div>
                <!-- end collapse menu -->
                
                <!-- #MOBILE -->
                <!-- Top menu profile link : this shows only when top menu is active -->
                <ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">
                    <li class="">
                        <a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">
                            <img src="<?php echo ( !empty( $user_data['user']['image']  ) ? BASE_URL . UPLOADS . $user_data['user']['image'] : BASE_URL . DEFAULT_AVATAR ); ?>" alt="<?php echo $user_data['name']; ?>" class="online" />
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="<?php echo base_url(); ?>profile/" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i> <u>P</u>rofile</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="launchFullscreen"><i class="fa fa-arrows-alt"></i> Full <u>S</u>creen</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo base_url();?>login/logout" class="padding-10 padding-top-5 padding-bottom-5" data-action="userLogout"><i class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <!-- logout button -->
                <div id="logout" class="btn-header transparent pull-right">
                    <span> <a href="<?php echo base_url();?>login/logout" title="Sign Out" data-action="userLogout" data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i class="fa fa-sign-out"></i></a> </span>
                </div>
                <!-- end logout button -->

                <!-- search mobile button (this is hidden till mobile view port) -->
                <div id="search-mobile" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
                </div>
                <!-- end search mobile button -->

                <!-- input: search field -->
                <form action="" class="header-search pull-right">
                    <input id="search-fld" type="text" name="param" data-placeholder="Find reports and more" data-autocomplete='[]'>
                    <button type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                    <a href="javascript:void(0);" id="cancel-search-js" title="Cancel Search"><i class="fa fa-times"></i></a>
                </form>
                <!-- end input: search field -->

                <!-- fullscreen button -->
                <div id="fullscreen" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
                </div>
                <!-- end fullscreen button -->
                
                <!-- #Voice Command: Start Speech -->
                <div id="speech-btn" class="btn-header transparent pull-right hidden-sm hidden-xs">
                    <div> 
                        <!--<a href="javascript:void(0)" title="Voice Command" data-action="voiceCommand"><i class="fa fa-microphone"></i></a> -->
                        <div class="popover bottom"><div class="arrow"></div>
                            <div class="popover-content">
                                <h4 class="vc-title">Voice command activated <br><small>Please speak clearly into the mic</small></h4>
                                <h4 class="vc-title-error text-center">
                                    <i class="fa fa-microphone-slash"></i> Voice command failed
                                    <br><small class="txt-color-red">Must <strong>"Allow"</strong> Microphone</small>
                                    <br><small class="txt-color-red">Must have <strong>Internet Connection</strong></small>
                                </h4>
                                <a href="javascript:void(0);" class="btn btn-success" onclick="commands.help()">See Commands</a> 
                                <a href="javascript:void(0);" class="btn bg-color-purple txt-color-white" onclick="$('#speech-btn .popover').fadeOut(50);">Close Popup</a> 
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end voice command -->
                
                <!-- fullscreen button -->
                <div id="notificationButton" class="btn-header pull-right">
                    <span> <a href="<?php echo base_url();?>notifications" title="Push Notification">&nbsp;<i class="fa fa-bell" aria-hidden="true"></i><span class="notification_count"></span></a> </span>
                </div>

            </div>
            <!-- end pulled right: nav area -->

        </header>
        
                
<?php
    /**
     * Load Side Menu
     */
     if (!empty($left_menu)) {
         $this->load->view('templates/left_menu', $menu_data);
     }
     
    /*
     * fixed-header
     *      - YES    : fixed-header  
     *      - No     : fixed-header | fixed-navigation | fixed-ribbon
     * 
     * fixed-navigation
     *      - YES    : fixed-header | fixed-navigation && Remove: container
     *      - No     : fixed-navigation | fixed-ribbon
     * 
     * 
     * fixed-ribbon
     *      - YES    : fixed-header | fixed-navigation | fixed-ribbon && Remove: container
     *      - No     : fixed-ribbon
     * 
     * fixed-footer
     *      - YES    : fixed-page-footer
     *      - No     : fixed-page-footer
     * 
     * 
     * smart-rtl
     *      - YES    : smart-rtl
     *      - No     : smart-rtl
     * 
     * smart-topmenu
     *      - YES    : smart-rtl
     *      - No     : smart-rtl
     * 
     * 
     */
    
            
    //fixed-header fixed-navigation
    
?>
                <div id="global-msg-bar" class="err"> <div id="global-msg">Global Message</div><i class="fR fa fa-close btn clse-btn "></i></div>
                <!-- <div id="settings-panel">
                    <div class="demo"><span id="demo-setting"><i class="fa fa-cog fa-spin txt-color-blueDark"></i></span> <form><legend class="no-padding margin-bottom-10">Layout Options</legend><section><label><input name="subscription" id="smart-fixed-header" type="checkbox" class="checkbox style-0"><span>Fixed Header</span></label><label><input type="checkbox" name="terms" id="smart-fixed-navigation" class="checkbox  style-0" ><span>Fixed Navigation</span></label><label><input type="checkbox" name="terms" id="smart-fixed-ribbon" class="checkbox style-0"><span>Fixed Ribbon</span></label><label><input type="checkbox" name="terms" id="smart-fixed-footer" class="checkbox style-0"><span>Fixed Footer</span></label><label><input type="checkbox" name="terms" id="smart-fixed-container" class="checkbox style-0"><span>Inside <b>.container</b> <div class="font-xs text-right">(non-responsive)</div></span></label><label style="display:block;"><input type="checkbox" id="smart-topmenu" class="checkbox style-0"><span>Menu on <b>top</b></span></label> <span id="smart-bgimages"></span></section><section><h6 class="margin-top-10 semi-bold margin-bottom-5">Clear Localstorage</h6><a href="javascript:void(0);" class="btn btn-xs btn-block btn-primary" id="reset-smart-widget"><i class="fa fa-refresh"></i> Factory Reset</a></section> <h6 class="margin-top-10 semi-bold margin-bottom-5">SmartAdmin Skins</h6><section id="smart-styles"><a href="javascript:void(0);" id="smart-style-0" data-skinlogo="images/logo.png" class="btn btn-block btn-xs txt-color-white margin-right-5" style="background-color:#4E463F;"><i class="fa fa-check fa-fw" id="skin-checked"></i>Smart Default</a><a href="javascript:void(0);" id="smart-style-1" data-skinlogo="images/logo-white.png" class="btn btn-block btn-xs txt-color-white" style="background:#3A4558;">Dark Elegance</a><a href="javascript:void(0);" id="smart-style-2" data-skinlogo="images/logo-blue.png" class="btn btn-xs btn-block txt-color-darken margin-top-5" style="background:#fff;">Ultra Light</a><a href="javascript:void(0);" id="smart-style-3" data-skinlogo="images/logo-pale.png" class="btn btn-xs btn-block txt-color-white margin-top-5" style="background:#f78c40">Google Skin</a></section></form> </div>
                </div> -->               