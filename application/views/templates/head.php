<?php
$user_opts = json_decode($user_data['user']['user_opts']);
if (empty($user_opts)) :
    $user_opts = new stdClass();
endif;
?>
<!DOCTYPE html>
<html <?php echo!empty($user_opts->bgImg) ? 'style="background-image: url(' . BASE_URL . 'assets/images/pattern/' . $user_opts->bgImg . ');"' : ''; ?>>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>
            <?php echo (defined('PAGE_TITLE_PREFIX')) ? PAGE_TITLE_PREFIX : ''; ?>
            <?php echo (!empty($page_title)) ? ' :: ' . $page_title : ''; ?>
        </title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo BASE_URL; ?>assets/css/bootstrap.min.css">
        <!--<link rel="stylesheet" type="text/css" media="screen" href="<?php echo BASE_URL; ?>assets/css/font-awesome.min.css">-->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo BASE_URL; ?>assets/font-awesome-new/css/font-awesome.min.css">

        <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo BASE_URL; ?>assets/css/smartadmin-production.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo BASE_URL; ?>assets/css/smartadmin-skins.min.css">

        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo BASE_URL; ?>assets/css/demo.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo BASE_URL; ?>assets/css/_style.css">

        <!-- FAVICONS -->

        <?php
        if ((!empty($company_data->comp_favicon)) && (file_exists(FCPATH . DIR_FAVICON . $company_data->comp_favicon)))
            $favicon_url = BASE_URL . DIR_FAVICON . $company_data->comp_favicon;
        else
            $favicon_url = BASE_URL . 'assets/images/favicon/favicon.ico';
        ?>
        <link rel="shortcut icon" href="<?php echo $favicon_url; ?>" type="image/x-icon">
        
        <link rel="icon" img src="<?php echo $favicon_url; ?>" type="image/x-icon">
<!--<link rel="icon" href="<?php echo BASE_URL; ?>assets/images/favicon/faviconuc.ico" type="image/x-icon">-->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo BASE_URL; ?>assets/css/admin-font.css">

        <!-- Multiple Fileupload -->
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/multiple_fileupload/css/jquery.filer.css">

        <!-- GOOGLE FONT -->
        <!--<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">-->

        <link rel="apple-touch-icon" href="<?php echo BASE_URL; ?>assets/images/splash/sptouch-icon-iphone.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo BASE_URL; ?>assets/images/splash/touch-icon-ipad.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo BASE_URL; ?>assets/images/splash/touch-icon-iphone-retina.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo BASE_URL; ?>assets/images/splash/touch-icon-ipad-retina.png">

        <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

        <!-- Startup image for web apps -->
        <link rel="apple-touch-startup-image" href="<?php echo BASE_URL; ?>assets/images/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
        <link rel="apple-touch-startup-image" href="<?php echo BASE_URL; ?>assets/images/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
        <link rel="apple-touch-startup-image" href="<?php echo BASE_URL; ?>assets/images/splash/iphone.png" media="screen and (max-device-width: 320px)">

        <!--bootstrap datepicker -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo BASE_URL; ?>assets/datepicker/datepicker.min.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo BASE_URL; ?>assets/datepicker/datepicker3.min.css" />
<?php
if ($user_data['controller'] == 'bill_of_quantity') :
    ?>
            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/app/boq.css" />
            <?php
        endif;
        ?>

        <!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
        <script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo BASE_URL; ?>assets/js/plugin/pace/pace.min.js"></script>

        <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.min.js"></script>

        <script type="text/javascript">
            var baseUrl = '<?php echo base_url(); ?>';
            var baseURL = '<?php echo BASE_URL; ?>';
            var userOpts = <?php echo $user_data['user']['user_opts']; ?>;
            var maxExpoClm = <?php echo MAX_EXPORT_CLM; ?>;

            var curUserId = <?php echo $user_data['user_id']; ?>;
            if (!window.jQuery) {
                document.write('<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
            }
        </script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery-ui.min.js"></script>
        <script>
            if (!window.jQuery.ui) {
                document.write('<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
            }
        </script>

        <!-- IMPORTANT: APP CONFIG -->
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/app.config.js"></script>

        <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 

        <!-- BOOTSTRAP JS -->
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/bootstrap/bootstrap.min.js"></script>
        <!-- PAGE RELATED PLUGIN(S) -->
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/plugin/datatables/dataTables.colVis.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/plugin/datatables/dataTables.tableTools.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/default.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/extend_validator_methods.js"></script>

        <!-- Multiple Fileupload -->
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/multiple_fileupload/js/jquery.filer.min.js?v=1.0.5"></script>       
        <script type="text/javascript">
                var notification_data = get_notification_items('all');
                var not_count = (notification_data.length>0) ? notification_data.length : '&nbsp;';
                $(document).ready(function(){
                    $('.notification_count').html(not_count);
                    $('.notification_count').css('color','red');
                });
                
                function get_notification_items(itm_type) {
                    var ajaxURL = baseUrl + 'invoice/get_notification_items';
                    var itm_type = itm_type;
                    var ajaxData = {
                        itm_type: itm_type
                    }

                    return ajaxRequest(ajaxURL, ajaxData, '');
                }
                
                setTimeout(function(){
                     notification_data = get_notification_items('');
                     for(var i in notification_data) {
                         var title=notification_data[i].type_txt.trim();
                         var desc=notification_data[i].running_num_txt.trim();
                         var url=notification_data[i].url_txt.trim() + notification_data[i].auto_id.trim();
                         notifyBrowser(title,desc,url,notification_data[i].type_txt.trim(),notification_data[i].not_id.trim());
                         update_notifications(notification_data[i].not_id.trim());
                     }
                }, 4000);
                
                setInterval(function(){ 
                     notification_data = get_notification_items('');
                     var notification_data_all = get_notification_items('all');
                     var not_count = (notification_data_all.length>0) ? notification_data_all.length : '&nbsp;';
                     $('.notification_count').html(not_count);
                     $('.notification_count').css('color','red');
                     for(var i in notification_data) {
                         var title=notification_data[i].type_txt.trim();
                         var desc=notification_data[i].running_num_txt.trim();
                         var url=notification_data[i].url_txt.trim() + notification_data[i].auto_id.trim();
                         notifyBrowser(title,desc,url,notification_data[i].type_txt.trim(),notification_data[i].not_id.trim());
                         update_notifications(notification_data[i].not_id.trim());
                     }
                }, 21000);
            
              
                function notifyBrowser(title,desc,url,itm_type,itm_id) 
                {
                    if (!Notification) {
                        console.log('Desktop notifications not available in your browser..'); 
                        return;
                    }
                    
                    if (Notification.permission !== "granted")
                    {
                        Notification.requestPermission();
                    } else {
                        var notification = new Notification(title, {
                        icon: baseUrl + 'assets/images/icon_logo.png',
                        body: desc,
                        });

                        // Remove the notification from Notification Center when clicked.
                        notification.onclick = function () {
                            notification.close();
                            window.open(url);      
                        };

                        // Callback function when the notification is closed.
                        notification.onclose = function () {
                            if(itm_id != '' && itm_id != 'undefined') {    
                                update_notifications(itm_id);
                            }
                            console.log('Notification closed');
                        };
                    }
                }
                
                function update_notifications(itm_id) {
                    var ajaxURL = baseUrl + 'invoice/update_notifications';
                    var ajaxData = {
                        itm_id: itm_id
                    }
                    return ajaxRequest(ajaxURL, ajaxData, '');
                }
        </script>
    </head>