<?php

/* 
 * Author Name  : Jai K
 * Purpose      : The Header for PDF files
 * Created On   : 2016-05-12 18:57
 */ 

if( empty( $user_data ) ) :
    $user_data = get_user_data();
endif;

//echo "<pre>Data: "; print_r( $user_data ); die;

/*
 * Get company details
 */
if( !empty( $user_data['company_id'] ) ) :
    
    $q = $this->db->get_where( TBL_COMP, array( 'id' => $user_data['company_id'], 'status' => 1 ) );
    $comp = $q->row_array();
    
endif;

$bg_clr = !empty( $bg_clr ) ? $bg_clr : '#666';
$ft_clr = !empty( $ft_clr ) ? $ft_clr : '#fff';
$full_width = !empty( $full_width ) ? $full_width : '999px';

$comp_logo = ( !empty( $comp['comp_logo'] ) && file_exists( UPLOADS . $comp['comp_logo'] ) ) ? UPLOADS . $comp['comp_logo'] : 'assets/images/logo.png';

$header = '<table style="width: '. $full_width .'">'
                . '<tr>'
                    . '<td style="width: 50px;">'
                        . '<img src="'. BASE_URL . $comp_logo .'" style="width: 35px; height: 35px;">'
                    . '</td>'

                    . '<td style="text-align: left; font-size: 24px; width: 500px; ">'. ( !empty( $comp['comp_name'] ) ? $comp['comp_name'] : SITE_NAME ) .'</td>'

                    . '<td style="width: 250px; text-align: right; font-size: 20px; padding: 5px 15px;  background: '. $bg_clr .'; color: '. $ft_clr .'">'. $title .'</td>'
                . '</tr>'

                . '<tr>'
                    . '<td colspan="2">'
                        . '<div style="font-size: small; color: '. $bg_clr .'">'
                            //. '#38-Ninht Street,<br />Pi colony<br />Chennai- 600 004'
                            . ( !empty( $comp['comp_address'] ) ? ( $comp['comp_address']. ( !empty( $comp['comp_pin_code'] ) ? ' - '.$comp['comp_pin_code'] : '' ) .'<br />' ) : 'No.57, 1st Floor,<br />Pantheon Road,<br />Egmore, Chennai - 600 008 <br />' )    
                            . '<div style="font-size: 10px;">'
                            . ( !empty( $comp['comp_reg_no'] ) ? 'Reg. No: '.$comp['comp_reg_no'].'<br />' : '' )
                            . '<b>Ph:</b> ' . ( !empty( $comp['comp_phone'] ) ? $comp['comp_phone'] : COMP_PH ).'<br />'
                            . '<b>Email:</b> '.( !empty( $comp['comp_email'] ) ? $comp['comp_email'] : COMP_EMAIL ).'<br />'
                            . ( !empty( $comp['comp_fax'] ) ? '<b>Fax:</b> '.$comp['comp_fax'].'<br />' : '' )
                            . ( !empty( $comp['comp_website'] ) ? '<b>www:</b> '.$comp['comp_website'].'<br />' : '' ).'</div>'
                        . '</div>'
                    . '</td>'

                    . '<td colspan="2" align="right" style="text-align: right;">'
                        . ( !empty( $srch_html ) ? $srch_html : '&nbsp;' )
                    . '</td>'
                . '</tr>'

                . '<tr><td colspan="20" height="20" style="border-bottom: 1px solid '. $bg_clr .'; "></td></tr>'

            . '</table>';

    echo $header;