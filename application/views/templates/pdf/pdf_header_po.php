<table width="100%" cellpadding="3" cellspacing="0">
	<tr>
		<td width="50%" style="vertical-align: top;">
			<div>
                <img src="<?php echo BASE_URL;?>assets/images/layout/logo.png" width="200px" height="65px" />
            </div>
            <div>
                <strong style="font-family:Verdana, Geneva, sans-serif;font-size:12px;color:#333; text-align:center; margin-left:30px;">(201026046N)</strong>
            </div>
        </td>
        <td width="50%" style="vertical-align: top; text-align: right;">
            <div style="font-size: 16px;">
                <div>
                    <b style="color: #ee6b30; font-size: 20px;">Mekro Asia Pte. Ltd</b>
                </div>
                <div>Blk 417, #07-1017</div>
                <div>Ang Mo Kio Ave 10</div>
                <div>Singapore 560417</div>
                <div><b style="font-size: 14px;">Tel/Fax:</b> +65 65272343</div>
                <div><b style="font-size: 14px;">H/P:</b> +65 84843341</div>
                <div><b style="font-size: 14px;">Email:</b> sales@mekroasia.com</div>
            </div>
        </td>
	</tr>
</table>