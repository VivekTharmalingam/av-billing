<?php

/* 
 * Author Name  : Jai K
 * Purpose      : The Footer for PDF files
 * Created On   : 2016-05-31 10:57
 */ 


$bg_clr = !empty( $bg_clr ) ? $bg_clr : '#666';
$ft_clr = !empty( $ft_clr ) ? $ft_clr : '#fff';
$full_width = !empty( $full_width ) ? $full_width : '999px';

$footer = '<table style="color: '. $bg_clr .'; font-size: 9px; width: '. $full_width .'">'
                . '<tr><td colspan="2" height="20" style="border-bottom: 1px solid '. $bg_clr .'; "></td></tr>'
        
                . '<tr>'
                    . '<td style="text-align: left; padding: 5px 0px; width: 50%;">@ '. SITE_NAME .' - '. date("Y") .'</td>'
                    . '<td style="text-align: right; padding: 5px 0px; ">Pg No. {PAGENO} / {nb}</td>'
                . '</tr>' 

            . '</table>';

echo $footer;