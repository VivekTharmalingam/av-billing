<?php $ci = get_ci_instance();?>
<?php $branch = $ci->company_model->branch_by_id($user_data['branch_id']);?>

<div style="margin-left: 2px;"><?php echo nl2br($branch->address); ?>,</div>
<!--<div style="margin-left: 2px;">Reg No:<?php //echo nl2br($setting->reg_no); ?>,</div>-->
<div style="margin-left: 2px;">Phone: <?php echo text($setting->comp_phone, '--'); ?>,</div>
<div style="margin-left: 2px;">E-mail : <?php echo text($setting->comp_email, '--'); ?></div>

<?php if (!empty($setting->fax)) {?>
    <div style="margin-left: 2px;">Fax: <?php echo text($setting->comp_fax, '--'); ?></div>
<?php }?>

<?php if (!empty($setting->comp_website)) {?>
    <div style="margin-left: 2px;">Website: <?php echo text($setting->comp_website, '--'); ?></div>
<?php }?>
