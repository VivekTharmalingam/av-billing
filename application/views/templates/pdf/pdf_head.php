<tr>
    <td width="25%" style="border-right:1px solid #FFF;">
        <table border="0" width="100%" style="border-collapse:collapse;" cellpadding="0">
            <tr>
                <?php
                $user_data = get_session_userdata();
                $comp_font_size = '';//'font-size: 24px;';
                if ((!empty($user_data['logo'])) && (file_exists(FCPATH . DIR_COMPANY_LOGO . $user_data['logo']))) {
                    $image_url = BASE_URL . DIR_COMPANY_LOGO . $user_data['logo'];
                    $alt = $user_data['comp_name'];
                }
                else {
                    $image_url = BASE_URL . 'assets/images/logo.png';
                    $alt = 'Logo not found!';
                }
                ?>
                <td>
                    <img width="110" height="113" src="<?php echo $image_url;?>" alt="<?php echo $alt;?>" />
                </td>
                <td style="font-family:Times new roman; padding-left:20px;padding-top:14px; color:#0000ff; <?php echo $comp_font_size;?>">
                    <?php #echo text($setting->comp_name, ' '); ?>
                </td>
            </tr>
        </table>
    </td>
    <td style="font-family:Times new roman;font-size:28px;font-weight:bold;text-align: right; width: 40%; font-size:18px;"> 
        <?php echo text($title, ' ');?>
    </td>
</tr>