<?php
    $permissions_struct = get_permissions_struct();
    $branch_ids = $this->company_model->get_branch_name_by_ids($this->user_data['branch_ids']);
    $current_branch = $this->company_model->branch_by_id($this->user_data['branch_id']);
    #echo '<pre>';print_r($permissions_struct);die;
?>
<aside id="left-panel">
    <!-- User info -->
    <div class="login-info">
        <span> 
            <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                <img src="<?php echo (!empty($user_data['user']['image']) ? BASE_URL . UPLOADS . $user_data['user']['image'] : BASE_URL . DEFAULT_AVATAR ); ?>" alt="me" class="online" /> 
                <span style="margin-top: -6px;"><?php echo ucfirst($user_data['name']); ?></span><br/>
                <span style="font-size:10px; margin-left: 36px; margin-top: -25px;">
                    <?php //error_log(print_r($user_data,1));// echo $user_data['branch_id'];?>
                    <?php foreach ($branch_ids as $key => $val) { ?>
                        <?php
                        if ($val->id == $user_data['branch_id']) {
                            echo ucfirst($val->branch_name);
                        }
                        ?>
                    <?php } ?>
                </span>
            </a> 
        </span>
    </div>

    <nav id="menu">
        <ul>
           <!-- <li <?php echo ( empty($user_data['controller']) || $user_data['controller'] == 'menu' ) ? 'class="open home home_clr"' : 'class="home_clr"'; ?> >
                <a href="<?php echo base_url(); ?>menu" title="Home"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Home</span></a>
            </li> -->

<!--            <li <?php echo ( empty($user_data['controller']) || $user_data['controller'] == 'home' ) ? 'class="open dashboard dashboard_clr"' : 'class="dashboard_clr"'; ?> >
                <a href="<?php echo base_url(); ?>home" title="Dashboard"><i class="fa fa-lg fa-fw fa-dashboard"></i> <span class="menu-item-parent">Dashboard</span></a>
            </li>-->

            <?php
            $per_arr = array(
                '1' => array(
                    'label' => 'List',
                    'ico' => 'fa-indent',
                ),
                '2' => array(
                    'label' => 'Add',
                    'ico' => 'fa-pencil',
                ),
                    /* '3'     => array(
                      'label' => 'Edit',
                      'ico'   => 'fa-edit',
                      ), */

                    //'4'     => 'Delete',
                    //'5'     => 'Export Excel',
                    //'6'     => 'Print PDF',
                    //'10'    => 'Approve'    
            );

            $per_arr_slug = array();
            foreach ($per_arr as $k => $p) :
                $per_arr_slug[$k] = (!empty($p['slug']) ? $p['slug'] : get_var_name($p['label'], '_') );
            endforeach;

            $split = 0; //floor( 12 / count( $per_arr ) );
            //error_log(print_r($user_data,1));
            if(empty($user_data['permissions']['home'])){
              unset($permissions_struct['dashboard']);  
            }
            
            if(empty($user_data['permissions']['settings'])){
              unset($permissions_struct['settings']);  
            }
            
            foreach ($permissions_struct as $mk => $m) :
                $me = explode('`', $m['label']);
                $mslug = get_var_name($me[0], '_');

                /* if( $permissions[$mslug] != 'on' || empty( $permissions[$mslug] ) ) :
                  // Skip Main Menu Items
                  continue;
                  endif; */
                $cls_menu = (!empty($m['cls_menu'])) ? $m['cls_menu'] : '';                
                //error_log($me.'--'.$mslug.'--'.$cls_menu);
                
                ?>
                      
                <?php if($mslug=='dashboard'){?>
                    <li <?php echo ( empty($user_data['controller']) || $user_data['controller'] == 'home' ) ? 'class="open dashboard dashboard_clr"' : 'class="dashboard_clr"'; ?> >
                        <a href="<?php echo base_url(); ?>home" title="Dashboard"><i class="fa fa-lg fa-fw fa-dashboard"></i> <span class="menu-item-parent">Dashboard</span></a>
                    </li>
                <?php } elseif($mslug=='settings') {?> 
                    <li <?php echo ( empty($user_data['controller']) || $user_data['controller'] == 'settings' ) ? 'class="open settings settings_clr"' : 'class="settings_clr"'; ?> >
                        <a href="<?php echo base_url(); ?>settings" title="Settings"><i class="fa fa-lg fa-fw fa-gears"></i> <span class="menu-item-parent">Settings</span></a>
                    </li>
                <?php }else{ ?>
                <li class="mstr-menu <?php echo $cls_menu; ?>" data-clr="<?php echo $cls_menu; ?>">
                    <a href="#"><i class="fa fa-lg fa-fw <?php echo $me[1]; ?>"></i> <span class="menu-item-parent "><?php echo $me[0]; ?></span></a>

                    <ul class="">

                        <?php
                        unset($m['label']);
                        unset($m['cls_menu']);

                        if (count($m) > 0) :
                            foreach ($m as $prefix => $item) :
                                if (isset($item['show_in_menu']) && !$item['show_in_menu']) :
                                    continue;
                                endif;

                                $ie = explode('`', $item['label']);
                                $islug = get_var_name($ie[0], '_');
                                $cntrlr = !empty($item['controller']) ? $item['controller'] : $islug;

                                switch ($mk) {
                                    case 'reports':
                                        $menu_href = base_url() . $cntrlr . '/';
                                        break;

                                    default:
                                        $menu_href = (($cntrlr == 'company') || ($cntrlr == 'update_item') || ($cntrlr == 'scan_product') || ($cntrlr == 'inventory') || ($cntrlr == 'movement_history') || ($cntrlr == 'invoice_return_warrantyitem') || ($cntrlr == 'count_sheet') || ($cntrlr == 'scan_service')) ? base_url() . $cntrlr . '/' : '#';
                                        break;
                                }

                                if (empty($permissions[$cntrlr])) :
                                    // Skip Sub Menu Items
                                    continue;
                                endif;

                                $open = '';
                                if ($cntrlr == $user_data['controller']) :
                                    switch ($cntrlr) {
                                        case 'company':
                                        case 'update_item':
                                        case 'scan_product':
                                        case 'movement_history':
                                        case 'invoice_return_warrantyitem':
                                        case 'count_sheet':
                                        case 'scan_service':
                                        case 'inventory':
                                        case 'purchase_order_reports':
                                        case 'invoice_reports':
                                        case 'customer_payment_summary':
                                        case 'customer_list_reports':
                                        case 'supplier_payment_summary':
                                        case 'supplier_list_reports':
                                        case 'inventory_by_location':
                                        case 'item_price_list':
                                        case 'expense_detail_reports':
                                        case 'payslip_generation_reports':

                                            $open = 'class="open active"';
                                            break;

                                        default:
                                            $open = 'class="open"';
                                            break;
                                    }
                                endif;
                                ?>
                                <li <?php echo $open; ?> >
                                    <a href="<?php echo $menu_href; ?>"><i class="fa fa-lg fa-fw <?php echo!empty($ie[1]) ? $ie[1] : 'fa-caret-right'; ?>"></i> <?php echo $ie[0]; ?></a>
                                    <?php
                                    /*if (($mk != 'reports') && ($cntrlr != 'company') && ($cntrlr != 'inventory') && ($cntrlr != 'scan_product') && ($cntrlr != 'movement_history') && ($cntrlr != 'finished_services')) :*/
                                    if (($mk != 'reports') && ($cntrlr != 'company') && ($cntrlr != 'update_item') && ($cntrlr != 'inventory') && ($cntrlr != 'scan_product') && ($cntrlr != 'movement_history') && ($cntrlr != 'invoice_return_warrantyitem') && ($cntrlr != 'count_sheet') && ($cntrlr != 'scan_service')) :
                                        ?>
                                        <ul>
                                            <?php
                                            unset($item['label']);
                                            $per_arrs = $per_arr;

                                            if (!empty($item['extra_per'])) :

                                                foreach ($item['extra_per'] as $ek => $eka) :
                                                    if (empty($eka['label'])) :
                                                        unset($item['extra_per'][$ek]);
                                                    endif;

                                                    unset($per_arrs[$ek]);
                                                endforeach;

                                                $per_arrs = ( $per_arrs + $item['extra_per'] );
                                            //echo "<pre>Data: "; print_r( $per_arrs ); die;

                                            endif;

                                            foreach ($per_arrs as $pk => $per) :

                                                if (empty($permissions[$cntrlr][$pk]) || ( isset($per['show_in_menu']) && !$per['show_in_menu'] )) :
                                                    // Skip Sub Menu Sub Items
                                                    continue;
                                                endif;

                                                $url = $cntrlr . '/' . ( ( $pk !== 1 ) ? $per_arr_slug[$pk] . '/' : '' );
                                                if (!empty($per['url'])) :
                                                    // Replace Controller
                                                    $url = preg_replace('/\%controller\%/i', $cntrlr, $per['url']);
                                                endif;
                                                ?>
                                                <li <?php echo (!empty($open) && ( ( ( $user_data['method'] == 'index' ) && $pk == 1 ) || ( $per_arr_slug[$pk] == $user_data['method'] ) ) ? 'class="active"' : '' ); ?> >
                                                    <a href="<?php echo base_url() . $url; ?>"><?php echo '<i class="fa ' . $per['ico'] . '"></i> ' . $per['label']; ?></a>
                                                </li>
                                                <?php
                                            endforeach;
                                            ?>

                                        </ul>
                                        <?php
                                    endif;
                                    ?>
                                </li>
                                <?php
                            endforeach;

                        endif;
                        ?>

                    </ul>    


                </li>
                <?php }?>
                <?php
            endforeach;
            ?>

        </ul>
    </nav>

    <span class="minifyme" data-action="minifyMenu"> 
        <i class="fa fa-arrow-circle-left hit"></i> 
    </span>

</aside>