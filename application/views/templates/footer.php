    <div class="page-footer">
        <div class="row">
                <div class="col-xs-6 col-sm-6">
                    <span class="txt-color-white"><?php echo $switch_branch;?>@ AV</span>
                </div>
            <div class="col-xs-6 col-sm-6 text-right">
                 <div class="txt-color-white inline-block">
                    <img src="<?php echo BASE_URL ?>assets/images/porweredby.png" />
                 </div>
            </div>
        </div>
    </div>
    <?php
    $branch_class = '';
    $branch_update = '';
    if (!empty($switch_branch)) {
        if ($switch_branch == 'S') {
            $branch_class = 'branch_switched';
            $branch_update = $switch_branch;
        }
        else {
            $branch_class = '';
            $branch_update = $switch_branch;
        }
    }?>
    <div id="shortcut" class="<?php echo $branch_class;?>" data-branch_update="<?php echo $branch_update;?>">
        <ul>
            <?php foreach($branches as $key => $value) {?>
                <li>
                    <a href="<?php echo base_url();?>user/switch_branch/<?php echo $value->id;?>" data-branch_id="<?php echo $value->id;?>" class="jarvismetro-tile big-cubes <?php echo ($value->id == $user_data['branch_id']) ? 'selected bg-color-pinkDark bg-active-branch' : 'bg-color-purple bg-branch';?>"> <div class="iconbox"> <span style="text-align: center; font-size: 20px; font-weight: bold; margin: 30px 5px 0 5px;"><?php echo $value->branch_name;?></span> <!--<span><?php echo $value->address;?></span>--></div> </a>
                </li>
            <?php }?>
        </ul>
    </div>
    
	<div class="stock-window">
		<div class="panel panel-default">
			<div class="panel-heading"><h3 class="title" style="margin: 0;font-size: 14px;">Available Quantity</h3></div>
			<div class="panel-body stock-window-content">
				<div class="col-md-12 no-item" style="padding: 0;">
					<div class="col-md-8" style="padding: 0;"><label class="branch-name">UCS</label></div>
					<div class="col-md-4 available-qty">10</div>
				</div>
			</div>
		</div>
	</div>
	
    <!-- Modal -->
    <div class="modal fade" id="Modal_Box" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Title</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="button" class="btn btn-primary ok">
                        Ok
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <!-- Modal -->
    <div class="modal fade" id="sendEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static" style="margin-top: 14.8%;">
        <div class="modal-dialog">
            <div class="modal-content" style="background: #00AEED; color: #fff; border-radius: 0px;">
                <div class="modal-header" style="border-bottom: 0px;">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                    <h4 class="modal-title" id="myModalLabel" style="text-align: center; letter-spacing: -1px; font-size: 24px; font-weight: 300;">Send Email</h4>
                    <p style="margin-left: 25%;">Do you want to send email for the Quotation?</p>
                    <div class="error_msg" style="padding-left: 17%; color: red; font-weight: bold;"></div>
                </div>
                <div class="modal-body">
                    
                    <form method="post" class="mail_form" id="mail_form" name="mail_form">
                        <div style="width: 70%; padding: 5px; margin-left: 15%; margin-top:-7%;">
                            <input class="form-control lowercase mail_txt" type="text" name="to_mail" id="mail_txt" value="">
                        </div>
                        <div style="border-top: 0px; text-align: center; margin-top: 5%;">
                            <input type="reset" class="btn btn-default btn-sm quotation_mail_cancel" data-dismiss="modal" style="margin-right: 7px; padding-left: 15px; padding-right: 15px; font-size: 14px;font-weight: 700;" value="Cancel">
                            <input type="submit" class="btn btn-default btn-sm ok" style="margin-right: 7px; padding-left: 15px; padding-right: 15px; font-size: 14px;font-weight: 700;" value="Ok">
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    
    <!-- Modal -->
    <div class="modal fade" id="purchase_sendEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static" style="margin-top: 14.8%;">
        <div class="modal-dialog">
            <div class="modal-content" style="background: #B02773; color: #fff; border-radius: 0px;">
                <div class="modal-header" style="border-bottom: 0px;">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                    <h4 class="modal-title" id="myModalLabel" style="text-align: center; letter-spacing: -1px; font-size: 24px; font-weight: 300;">Send Email</h4>
                    <p style="margin-left: 25%;">Do you want to send email for the Purchase Order?</p>
                    <div class="error_msg" style="padding-left: 17%; color: red; font-weight: bold;"></div>
                </div>
                <div class="modal-body">
                    <form method="post" class="mail_form" id="mail_form" name="mail_form">
                        <div style="width: 70%; padding: 5px; margin-left: 15%; margin-top:-7%;">
                            <input class="form-control lowercase mail_txt" type="text" name="to_mail" id="mail_txt" value="">
                        </div>
                        <div style="border-top: 0px; text-align: center; margin-top: 5%;">
                            <input type="reset" class="btn btn-default btn-sm purchase_mail_cancel" data-dismiss="modal" style="margin-right: 7px; padding-left: 15px; padding-right: 15px; font-size: 14px;font-weight: 700;" value="Cancel">
                            <input type="submit" class="btn btn-default btn-sm ok" style="margin-right: 7px; padding-left: 15px; padding-right: 15px; font-size: 14px;font-weight: 700;" value="Ok">
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    
    <!-- Modal -->
    <div class="modal fade" id="ImportEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static" style="margin-top: 14.8%;">
        <div class="modal-dialog">
            <div class="modal-content" style="background: #1F88C9; color: #fff; border-radius: 0px;">
                <div class="modal-header" style="border-bottom: 0px;">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                    <h4 class="modal-title" id="myModalLabel" style="text-align: center; letter-spacing: -1px; font-size: 24px; font-weight: 300;">Import Database</h4>
                    <p style="margin-left: 32.5%;">Do you want to Import Database?</p>
                    <div class="error_msg" style="padding-left: 17%; color: #00fffd; font-weight: bold;"></div>
                </div>
                <div class="modal-body">
                    <form method="post" class="import_form" id="import_form" name="import_form" enctype="multipart/form-data">
                        <div style="width: 70%; padding: 5px; margin-left: 15%; margin-top:-7%;">
                            <input class="form-control import_db_file" type="file" name="import_db_file" id="import_db_file">
                            <input class="form-control hidden_file" type="hidden" id="hidden_file">
                        </div>
                        <div style="border-top: 0px; text-align: center; margin-top: 5%;">
                            <input type="reset" class="btn btn-default btn-sm imp_cancel" data-dismiss="modal" style="margin-right: 7px; padding-left: 15px; padding-right: 15px; font-size: 14px;font-weight: 700;" value="Cancel">
                            <input type="submit" class="btn btn-default btn-sm ok" style="margin-right: 7px; padding-left: 15px; padding-right: 15px; font-size: 14px;font-weight: 700;" value="Import">
                            <span class="load_img" style="display: none;"><img id="loader" src="<?php echo base_url();?>assets/images/load1.gif" title="Loading"></span>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <!-- Modal -->
    <div class="modal fade" id="rackNumber" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static" style="margin-top: 14.8%;">
        <div class="modal-dialog">
            <div class="modal-content" style="background: #00AEED; color: #fff; border-radius: 0px;">
                <div class="modal-header" style="border-bottom: 0px;">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                    <h4 class="modal-title" id="myModalLabel" style="text-align: center; letter-spacing: -1px; font-size: 24px; font-weight: 300;">Update Rack Number</h4>
                    <p style="margin-left: 20%;">Do you want to Update Rack Number for the Service Sheet?</p>
                    <div class="error_msg" style="padding-left: 17%; color: red; font-weight: bold;"></div>
                </div>
                <div class="modal-body">
                    
                    <form method="post" class="rack_number_form" id="rack_number_form" name="rack_number_form">
                        <div style="width: 70%; padding: 5px; margin-left: 15%; margin-top:-7%;">
                            <input class="form-control lowercase rack_number_txt" type="text" name="rack_number_txt" id="rack_number_txt" value="">
                        </div>
                        <div style="width: 70%; padding: 5px; margin-left: 15%;">
                            <select name="service_status" id="service_status" class="select2 service_status" >
                                <option value="1">Processing</option>
                                <option value="2">Completed</option>
                                <option value="3">Delivered</option>
                            </select>  
                        </div>
                        <div style="width: 70%; padding: 5px; margin-left: 15%;" class="delivered_date_cls">
                            <label class="input">
                                <input type="text" name="delivered_date" id="delivered_date" class="form-control bootstrap-datepicker-comncls" style="width: 380px;" placeholder="DD/MM/YYYY" value="<?= $service->delivered_date_str; ?>" />
                            </label>
                        </div>
                        <div style="border-top: 0px; text-align: center; margin-top: 5%;">
                            <input type="reset" class="btn btn-default btn-sm" data-dismiss="modal" style="margin-right: 7px; padding-left: 15px; padding-right: 15px; font-size: 14px;font-weight: 700;" value="Cancel">
                            <input type="submit" class="btn btn-default btn-sm ok" style="margin-right: 7px; padding-left: 15px; padding-right: 15px; font-size: 14px;font-weight: 700;" value="Ok">
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <!-- Modal -->
    <div class="modal fade" id="warrantyItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static" style="margin-top: 14.8%;">
        <div class="modal-dialog">
            <div class="modal-content" style="background: #00AEED; color: #fff; border-radius: 0px;">
                <div class="modal-header" style="border-bottom: 0px;">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                    <h4 class="modal-title" id="myModalLabel" style="text-align: center; letter-spacing: -1px; font-size: 24px; font-weight: 300;">Clear Warranty</h4>
                    <p style="margin-left: 20%;">Do you want to Clear Warranty for the Item?</p>
                    <div class="error_msg" style="padding-left: 17%; color: red; font-weight: bold;"></div>
                </div>
                <div class="modal-body">
                    
                    <form method="post" class="warrantyItem_form" id="warrantyItem_form" name="warrantyItem_form">
                        <div class="warrantyItem_insert">
                            <div style="width: 70%; padding: 5px; margin-left: 15%; margin-top:-7%;">
                                <label class="input">
                                    <input type="text" name="war_clear_date" id="war_clear_date" required="" class="form-control bootstrap-datepicker-comncls" style="width: 380px;" placeholder="DD/MM/YYYY" value="<?php echo date('d/m/Y'); ?>" />
                                </label>
                            </div>
                            <div style="width: 70%; padding: 5px; margin-left: 15%;">
                                <textarea class="form-control lowercase warrantyItem_txt" type="text" name="warrantyItem_txt" id="warrantyItem_txt"></textarea>
                            </div>
                            <div style="border-top: 0px; text-align: center; margin-top: 5%;">
                                <input type="reset" class="btn btn-default btn-sm" data-dismiss="modal" style="margin-right: 7px; padding-left: 15px; padding-right: 15px; font-size: 14px;font-weight: 700;" value="Cancel">
                                <input type="submit" class="btn btn-default btn-sm ok" style="margin-right: 7px; padding-left: 15px; padding-right: 15px; font-size: 14px;font-weight: 700;" value="Ok">
                            </div>
                        </div>
                        <div class="warrantyItem_cleared">
                            <span>Sorry!.. This Item was already cleared..  </span>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	
	<!-- New Window Modal -->
    <div class="modal fade" id="new_window" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add New Item</h4>
                </div>
                <div class="modal-body">
					<iframe src="" style="height: 800px; width: 100%; max-width: 980px;"></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <div class="modal fade" id="grn_serial_no" tabindex="-1" role="dialog" aria-hidden="true" style="width: 40%; margin: auto; overflow: hidden;">
        <div class="modal-dialog" style="width: 100%;">
            <div class="modal-content" style="max-height: 600px; min-height: 200px; overflow-y: auto;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h3 class="modal-title" id="myModalLabel">Update Serial No.</h3>
					
                </div>
                <div class="modal-body">
                            <div class="panel panel-default item_with_ser_nos">
                                <div class="panel-heading"><span class="serial_no_title" style=" font-weight: bold;">Add to Serial No for item name</span></div>
                                    <div class="panel-body promo_items_container">
                                            <div class="item_ser_nos"></div>
                                    </div>
                            </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default serial_no_close" data-dismiss="modal">Ok</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
	<!-- Promotion Modal -->
    <div class="modal fade" id="promotion" tabindex="-1" role="dialog" aria-hidden="true" style="width: 80%; margin: auto; overflow: hidden;">
        <div class="modal-dialog" style="width: 100%;">
            <div class="modal-content" style="max-height: 600px; min-height: 200px; overflow-y: auto;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h3 class="modal-title" id="myModalLabel">Promotions</h3>
					
                </div>
                <div class="modal-body">
					<div class="added-item-legend" style="text-align: right;">
						<label class="btn">&nbsp;</label>Already added to invoice
					</div>
					
					<div class="panel panel-default purchase_with_purchase">
						<div class="panel-heading"><h3 class="promotion_title">Purchase with Purchase</h3></div>
						<div class="panel-body promo_items_container">
							<div class="col-md-12 promo_item_head">
								<div class="col-md-3"><label>Item Code</label></div>
								<div class="col-md-3"><label>Brand - Description</label></div>
								<div class="col-md-2"><label>Selling Price</label></div>
								<div class="col-md-2"><label>Offered Price</label></div>
								<div class="col-md-2"><label>Actions</label></div>
							</div>
							<div class="promo_items"></div>
						</div>
					</div>
					
					<div class="panel panel-default discount_sale">
						<div class="panel-heading"><h3 class="promotion_title">Discount Sales</h3></div>
						<div class="panel-body promo_items_container">
							<div class="col-md-12 promo_item_head">
								<div class="col-md-3"><label>Item Code</label></div>
								<div class="col-md-3"><label>Brand - Description</label></div>
								<div class="col-md-2"><label>Selling Price</label></div>
								<div class="col-md-2"><label>Offered Price</label></div>
								<div class="col-md-2"><label>Actions</label></div>
							</div>
							<div class="promo_items"></div>
						</div>
					</div>
					
					<div class="panel panel-default buy_one_get_one">
						<div class="panel-heading"><h3 class="promotion_title">Buy One Get One</h3></div>
						<div class="panel-body promo_items_container">
							<div class="col-md-12 promo_item_head">
								<div class="col-md-3"><label>Item Code</label></div>
								<div class="col-md-3"><label>Brand - Description</label></div>
								<div class="col-md-2"><label>Selling Price</label></div>
								<div class="col-md-2"><label>Offered Price</label></div>
								<div class="col-md-2"><label>Actions</label></div>
							</div>
							<div class="promo_items"></div>
						</div>
					</div>
					<div class="panel panel-default group_buy">
						<div class="panel-heading"><h3 class="promotion_title">Group Buy</h3></div>
						<div class="panel-body group-buy-content"></div>
					</div>
					<div class="added-item-legend" style="text-align: right;">
						<label class="btn">&nbsp;</label>Already added to invoice
					</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Modal -->
   <div class="modal fade" id="picked_status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static" style="margin-top: 14.8%;">
       <div class="modal-dialog">
           <div class="modal-content" style="background: #34922e ; color: #fff; border-radius: 0px;">
               <div class="modal-header" style="border-bottom: 0px;">
                   <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                   <h4 class="modal-title" id="myModalLabel" style="text-align: center; letter-spacing: -1px; font-size: 24px; font-weight: 300;">Pick Enquiry</h4>
                   <p style="margin-left: 33%;">Do you want to Pick this Enquiry?</p>
               </div>
               <div class="modal-body">
                    <div style="border-top: 0px; text-align: center; ">
                       <input type="reset" class="btn btn-default btn-sm" data-dismiss="modal" style="margin-right: 7px; padding-left: 15px; padding-right: 15px; font-size: 14px;font-weight: 700;" value="NO">
                       <input type="button" class="btn btn-default btn-sm ok pick_confirmation" style="margin-right: 7px; padding-left: 15px; padding-right: 15px; font-size: 14px;font-weight: 700;" value="YES">
                    </div>
               </div>
           </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
   </div><!-- /.modal -->
	
    <!-- CUSTOM NOTIFICATION -->
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/notification/SmartNotification.min.js"></script>

    <!-- JARVIS WIDGETS -->
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>

    <!-- EASY PIE CHARTS -->
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

    <!-- SPARKLINES -->
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>

    <!-- JQUERY VALIDATE -->
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>

    <!-- JQUERY MASKED INPUT -->
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

    <!-- JQUERY SELECT2 INPUT -->
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/plugin/select2/select2.min.js"></script>

    <!-- JQUERY UI + Bootstrap Slider -->
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

    <!-- browser msie issue fix -->
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

    <!-- FastClick: For mobile devices -->
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
    
    <!--bootstrap datepicker -->
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/datepicker/bootstrap-datepicker.min.js"></script>
     <!--bootstrap timepicker -->
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/timepicker/bootstrap-timepicker.min.js"></script>
     <!--bootstrap clockpicker -->
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/plugin/clockpicker/clockpicker.min.js"></script>
    <!--[if IE 8]>

    <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

    <![endif]-->

    <!-- Demo purpose only -->
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/demo.min.js"></script>
    
    <!-- MAIN APP JS FILE -->
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/app.min.js"></script>
    
    <!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
    <!-- Voice command : plugin -->
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/speech/voicecommand.min.js"></script>
    </body>
</html>