<script type="text/javascript">
    $(document).ready(function (e) {
        $('.home_menu').on('click', function () {
            $('.home_submenu:not(#' + submenu_id + ')').slideUp();
            var submenu_id = $(this).closest('section').data('submenu_id');
            var sub_menu_text = $('#' + submenu_id).html();
            $(this).closest('.row').next().html(sub_menu_text);
            $(this).closest('.row').next().find('section').addClass(submenu_id);
            $(this).closest('.row').next().slideDown();

            if (!$('#' + submenu_id).hasClass('open')) {
                $('#' + submenu_id).addClass('open');
            }
            else {
                $('#' + submenu_id).removeClass('open');
            }
        });
    });
</script>

<?php $permissions_struct = get_permissions_struct(); ?>

<!-- Content Wrapper. Contains page content -->
<div style="margin:0 auto; width:90%;">
    <div id="main" role="main" style="width: 100%; margin-left:0px;">
        <!--<div id="ribbon">
        <!-- breadcrumb -->
        <?php #echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
        <!--</div>-->
        <div id="content">
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <section class="col-lg-12 connectedSortable">
                    &nbsp;
                </section>
                <!-- /.Left col -->
            </div>

            <div class="row">
<!--                <section class="col-xs-12 col-sm-6 col-md-3 dashboard_clr">
                    <a href="<?php echo base_url(); ?>home" title="Go to Dashboard" style="text-decoration: none; color:#FFF;">
                        <div class="info-box" style="text-align:center; padding:50px 0px;">
                            <span class="info-box-icon bg-aqua" style="font-size:60px;"><i class="fa fa-lg fa-fw fa-dashboard"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text" style="font-size:20px;">Dashboard</span>
                            </div>
                             /.info-box-content 
                        </div>
                    </a>  
                </section>-->
                <?php
                $menu_count = count($permissions_struct);
                if (empty($user_data['permissions']['home'])) {
                    unset($permissions_struct['dashboard']);
                }
                $i = 1;
                foreach ($permissions_struct as $mk => $m) {
                    $me = explode('`', $m['label']);
                    $mslug = get_var_name($me[0], '_');
                    $cls_menu = (!empty($m['cls_menu'])) ? $m['cls_menu'] : '';
                    $dv = 5;
                    if ($mslug == 'dashboard') {
                        $dv = 5;
                    }
                    if (($i % $dv) == 0) {
                        ?>
                    </div>
                    <div class="row home_submenu"></div>
                    <div class="row">
    <?php } ?>
                    <section class="col-xs-12 col-sm-6 col-md-3 <?php echo $cls_menu; ?>" data-submenu_id="<?php echo $cls_menu; ?>">                      
                        <a href="<?php if ($mslug == 'dashboard') { ?><?php echo base_url(); ?>home<?php } else { ?>javascript:void(0);<?php } ?>" class="home_menu" title="Go to <?php echo $me[0] . $i; ?>" style="text-decoration: none; color:#FFF;">                          
                            <div class="info-box" style="text-align:center; padding:50px 0px;">
                                <span class="info-box-icon bg-red" style="font-size:60px;"><i class="fa fa-lg fa-fw <?php echo $me[1]; ?>"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text" style="font-size:20px;"><?php echo $me[0]; ?></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        </a> 
                    </section>
                        <?php if ($mslug != 'dashboard') { ?>
                        <div class="sub_menu" id ="<?php echo $cls_menu; ?>">
                            <?php
                            unset($m['label']);
                            unset($m['cls_menu']);

                            if (count($m) > 0) {
                                foreach ($m as $prefix => $item) {
                                    if (isset($item['show_in_menu']) && !$item['show_in_menu'])
                                        continue;

                                    $ie = explode('`', $item['label']);
                                    $islug = get_var_name($ie[0], '_');
                                    $cntrlr = !empty($item['controller']) ? $item['controller'] : $islug;

                                    if (empty($permissions[$cntrlr])) :
                                        // Skip Sub Menu Items
                                        continue;
                                    endif;

                                    $menu_href = base_url() . $cntrlr . '/';
                                    ?>
                                    <section class="col-xs-12 col-sm-6 col-md-3">
                                        <a href="<?php echo $menu_href; ?>" title="Go to <?php echo $ie[0]; ?>" style="text-decoration: none; color:#FFF;">  
                                            <div class="info-box">
                                                <span class="info-box-icon bg-red" style="float:left; margin:10px;"><i class="fa fa-lg fa-fw <?php echo!empty($ie[1]) ? $ie[1] : 'fa-caret-right'; ?>"></i></span>

                                                <div class="info-box-content" style="float:left; margin:10px 10px 10px 0px;">
                                                    <span class="info-box-text"><?php echo $ie[0]; ?></span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>
                                        </a> 
                                    </section>
                            <?php }
                        }
                        ?>
                        </div>
                    <?php } ?>
                    <?php
                    $i ++;
                }
                ?>
            </div>
            <div class="row home_submenu"></div>
        </div>
    </div>
</div>
<!-- /.content-wrapper -->