<!-- MAIN PANEL -->

<div id="main" role="main">
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment">
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span>
        </span>
        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>company/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                        </header>
                        <!-- widget div-->
                        <div>
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <form class="edit_form" action="<?php echo $form_action; ?>" method="post">
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">

                                        <fieldset>
                                            <div class="row"> &nbsp;</div>

                                            <div class="row">
                                                <section class="col col-3">
                                                    <label class="label">Brand</label>
                                                    <label class="select">
                                                        <select name="search_brand" class="select2" id="brand" <?php
                                                        if (isset($_SESSION['all_files']) && $_SESSION['all_files'] == 1): echo "disabled";
                                                        endif;
                                                        ?>>
                                                            <option value="">Select Brand</option>
                                                            <?php foreach ($category as $key => $row) { ?>
                                                                <option value="<?php echo $row->id; ?>" data-title="<?php echo $row->cat_name; ?>" <?php
                                                                if (isset($_SESSION['brandvalues']) && $_SESSION['brandvalues'] == $row->id): echo "selected";
                                                                endif;
                                                                ?>><?php echo $row->cat_name; ?></option>
                                                                    <?php } ?>
                                                        </select>
                                                    </label>
                                                </section>
                                                <section class="col col-3">
                                                    <label class="label">Item Category </label>
                                                    <label class="select">
                                                        <select name="item_cat" class="item_cat select2" id="item_cat" <?php
                                                        if (isset($_SESSION['all_files']) && $_SESSION['all_files'] == 1): echo "disabled";
                                                        endif;
                                                        ?>>
                                                            <option value="">Select Item Category</option>
                                                            <?php foreach ($item_category as $key => $val) { ?>
                                                                <option value="<?php echo $val->id; ?>" data-title="<?php echo $val->item_category_name; ?>" <?php
                                                                if (isset($_SESSION['itemcategory']) && $_SESSION['itemcategory'] == $val->id): echo "selected";
                                                                endif;
                                                                ?>><?php echo $val->item_category_name; ?></option>
                                                                    <?php } ?>
                                                        </select>
                                                    </label>
                                                </section>
<!--                                                <section class="col col-5">
                                                    <label class="label">Sub Category </label>
                                                    <label class="select">
                                                        <select name="sub_cat" class="sub_cat select2" id="sub_cat">
                                                            <option value="">Select Sub Category</option>
                                                        </select>
                                                    </label>
                                                </section>-->
                                                <section class="col col-2">
                                                    <label class="label">&nbsp;</label>
                                                    <label class="checkbox" style="min-width: 75px;">
                                                        <input type="checkbox" name="check_all" value="1" <?php
                                                        if (isset($_SESSION['all_files']) && $_SESSION['all_files'] == 1): echo "checked";
                                                        endif;
                                                        ?>/>
                                                        <i></i><b>All ITEMS</b>

                                                    </label>
                                                </section>
                                                <input type="hidden" name="page_count_numbers" id="pagecountnunbers"/>
                                                <section class="col col-1">
                                                    <label class="label">Page No.</label>
                                                    <label class="select">
                                                        <i class="icon-append"></i>
                                                        <select name="page_no" class="" id="page_no">
                                                            <option value="">Select</option>
                                                            <?php
                                                            if (isset($_SESSION['pagecountnunbers']) && $_SESSION['pagecountnunbers'] != ''):
                                                                $explodevalues = explode(',', $_SESSION['pagecountnunbers']);
                                                                for ($i = 1; $i <= count($explodevalues); $i++):
                                                                    ?>
                                                                    <option value="<?php echo $i; ?>" <?php
                                                                    if ($_SESSION['updated_page_no'] == $i): echo "selected";
                                                                    endif;
                                                                    ?>><?php echo $i; ?></option>
                                                                            <?php
                                                                        endfor;
                                                                    endif;
                                                                    ?>
                                                        </select>
                                                </section>
                                                <section class = "col col-3">
                                                    <label class = "label">&nbsp;
                                                    </label>
                                                    <label class = "input actions">
                                                        <button type="button" id="get_items" class = "btn btn-warning" style = "padding: 7px;width: 50%;">GET</button>
                                                    </label>
                                                    <label class = "loader" style = "display: none;">
                                                    <!--<img src = "<?php #echo images_url('ajax-loader.gif');?>" alt = "Loading..." /> -->
                                                        Please wait while getting item details...
                                                    </label>
                                                </section>
                                            </div>
                                            <div class = "row" style = "padding: 0px 15px;">
                                                <div class = "category" style = "display: none;">
                                                    <legend style = "font-weight: bold; font-size: 15px; color: #A90329;"></legend>
                                                    <fieldset style = "background:#d09ed2;padding-top: 10px;">
                                                        <div class = "items_head col-xs-12 clr">
                                                            <section class = "col col-3">
                                                                <label class = "label" style = "text-align: center;"><b>Brand</b></label>
                                                            </section>
                                                            <section class = "col col-3">
                                                                <label class = "label" style = "text-align: center;"><b>Category</b></label>
                                                            </section>
                                                            <section class = "col col-2">
                                                                <label class = "label" style = "text-align: center;"><b>Sign</b></label>
                                                            </section>
                                                            <section class = "col col-3">
                                                                <label class = "label" style = "text-align: center;"><b>Value</b></label>
                                                            </section>
                                                            <section class = "col col-1">
                                                                <label class = "label" style = "text-align: left;"><b></b></label>
                                                            </section>
                                                        </div>
                                                    </fieldset>
                                                    <fieldset style = "background:#dab7d9;">
                                                        <div class = "row category_details">
                                                            <section class = "col col-3">
                                                                <div id = "brand_name" style = "text-align: center;"></div>
                                                            </section>
                                                            <section class = "col col-3">
                                                                <div id = "category_name" style = "text-align: center;"></div>
                                                            </section>
                                                            <section class = "col col-3">
                                                                <label class = "select">
                                                                    <i class = "icon-append"></i>
                                                                    <select class = "form-control cat_sign" name = "cat_sign" id = "cat_sign">
                                                                        <option value = "+">+</option>
                                                                        <option value = "-">-</option>
                                                                        <option value = "*">*</option>
                                                                        <option value = "/">/</option>
                                                                    </select>
                                                                </label>
                                                            </section>

                                                            <section class = "col col-3">
                                                                <label class = "input"><i class = "icon-append fa fa-dollar"></i>
                                                                    <input type = "text" name = "cat_val" id = "cat_val" class = "lowercase float_value cat_val" value = "0.00" maxlength = "150" />
                                                                </label>
                                                            </section>
                                                            <!--<section class = "col col-1">
                                                            <label class = "input"><i class = "fa fa-2x fa-minus-circle remove-row"></i>
                                                            <i class = "fa fa-2x fa-bullhorn promotion"></i>
                                                            </label>
                                                            </section> -->
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class = "row itemsFrame" style = "display: none;padding:0px 15px;">
                                                <legend style = "font-weight: bold; font-size: 15px; color: #A90329;">Category Item List</legend>
                                                <fieldset style = "background:#d09ed2;">
                                                    <div class = "items_head col-xs-12 clr">
                                                        <section class="col col-2">
                                                            <label class="label" style = "text-align: center;"><b>Brand</b></label>
                                                        </section>
                                                        <section class="col col-2">
                                                            <label class="label" style = "text-align: center;"><b>Category</b></label>
                                                        </section>
                                                        <section class="col col-2">
                                                            <label class="label" style = "text-align: center;"><b>Sub Category</b></label>
                                                        </section>
                                                        <section class="col col-2">
                                                            <label class="label" style = "text-align: center;"><b>Item Description</b></label>
                                                        </section>
                                                        <section class="col col-1">
                                                            <label class="label" style = "text-align: center;"><b>Cost</b></label>
                                                        </section>
                                                        <section class = "col col-1">
                                                            <label class = "label" style = "text-align: center;"><b>Sign</b></label>
                                                        </section>
                                                        <section class = "col col-1">
                                                            <label class = "label" style = "text-align: center;"><b>Value</b></label>
                                                        </section>
                                                        <section class = "col col-1">
                                                            <label class = "label" style = "text-align: center;"><b>Selling Price</b></label>
                                                        </section>
                                                    </div>
                                                </fieldset>
                                                <fieldset style = "background:#dab7d9;">
                                                    <div class = "items"></div>
                                                </fieldset>

                                                <footer>
                                                    <button type="submit" class="btn btn-primary" >Submit</button>
                                                    <button type="reset" class = "btn btn-default">Cancel</button>
                                                </footer>
                                            </div>
                                    </div>
                                </div>
                                <!--end widget content -->
                            </form>

                        </div><!--end widget div -->
                    </div><!--end widget -->
                </article><!--END COL -->
            </div><!--END ROW -->
        </section><!--end widget grid -->
    </div><!--END MAIN CONTENT -->
</div><!--END MAIN PANEL -->
<?php
unset($_SESSION['updated_page_no']);
unset($_SESSION['itemcategory']);
unset($_SESSION['brandvalues']);
unset($_SESSION['pagecountnunbers']);
unset($_SESSION['all_files']);
?>
<script type = "text/javascript">
    var brands = '<?php echo json_encode($category, JSON_UNESCAPED_SLASHES); ?>';
    brands = $.parseJSON(brands);
    var item_categories = '<?php echo json_encode($item_category, JSON_UNESCAPED_SLASHES); ?>';
    item_categories = $.parseJSON(item_categories);
    console.log(item_categories);
    var sub_categories = '<?php echo json_encode($sub_category, JSON_UNESCAPED_SLASHES); ?>';
    sub_categories = $.parseJSON(sub_categories);
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/item_price.js"></script>