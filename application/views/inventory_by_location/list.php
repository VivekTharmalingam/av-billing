<script>
    window.localStorage.clear();
</script>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">&nbsp;</div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="widget-body">
                        <!-- Widget ID (each widget will need unique ID)-->                                
                        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false"	>

                            <header><span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                <h2>Inventory By Location Reports List</h2>
                            </header>

                            <div class="date-range"></div>
                            <div>
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="box-body table-responsive">
                                        <form name="order_search" action="<?php echo $form_action; ?>" method="post" class="search_form validate_form">
                                            <div class="col-xs-12">
                                                <div class="col-md-2">
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-random"></i>
                                                            </div>
                                                            <select  name="branch_id" class="branch_id select2">  
                                                                <option value="">Select Branch Name</option>
                                                                <?php foreach ($branch as $key => $val) { ?>
                                                                    <option value="<?php echo $val->id; ?>" <?php echo ($val->id == $branch_id) ? 'selected' : '' ?> ><?php echo $val->branch_name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>	 
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-sitemap"></i>
                                                            </div>
                                                            <select  name="brand_id" class="brand_id select2">  
                                                                <option value="">Select Brand Name</option>
                                                                <?php foreach ($brand as $key => $val) { ?>
                                                                    <option value="<?php echo $val->id; ?>" <?php echo ($val->id == $brand_id) ? 'selected' : '' ?>><?php echo $val->cat_name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <input type="submit" name="reset_btn" value="Reset" id="reset" class="btn btn-success btn-warning" />&nbsp;&nbsp;<input type="submit" value="Search" class="btn btn-success btn-primary" />
                                                        </div>
                                                    </div>
                                                </div>														
                                                <div class="col-md-1" style="margin-left:34px;">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <input type="submit" name="submit" value="pdf" class="submit_pdf" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="margin-left:-34px;">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <input type="submit" name="submit" value="excel" class="submit_excel" />
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </form>
                                    </div>

                                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>			                
                                            <tr>
                                                <th data-class="expand">S.No</th>
                                                <th ><i class="fa fa-fw fa-sitemap text-muted hidden-md hidden-sm hidden-xs"></i> Branch Name</th>
                                                <th data-hide="phone"> Item Code</th>
                                                <th data-hide="phone"> Brand Name</th>
                                                <th data-hide="phone"> Product Description</th>
                                                <th data-hide="phone,tablet"  style="width:20%;"><i class="fa fa-fw fa-stack-overflow text-muted hidden-md hidden-sm hidden-xs"></i>Available Quantity</th>
                                                <!--<th data-hide="phone" style="text-align: center;"><i class="fa fa-fw fa-cog txt-color-blue hidden-md hidden-sm hidden-xs"></i> Actions</th>-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($inventory as $key => $row) { ?>
                                                <tr>
                                                    <td><?php echo ++$key; ?></td>
                                                    <td><?php echo $row->branch_name; ?> </td>
                                                    <td><?php echo $row->pdt_code; ?> </td>
                                                    <td><?php echo $row->cat_name; ?> </td>
                                                    <td><?php echo $row->pdt_name; ?> </td>	 
                                                    <td><?php echo $row->available_qty; ?> </td>
                                                    <!--<td align="center">
                                                    <?php if (in_array(1, $permission)) { ?>
                                                                        <a class="label btn btn-warning view" href="<?php //echo $view_link . $row->id  ?>"><span>View</span></a>
                                                    <?php } ?>
                                                    </td>-->
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- end widget content -->
                            </div>
                        </div> 
                    </div>
                </article>
                <!-- /.row -->
            </div>
        </section>
    </div>

</div>

<script>
    $(document).ready(function () {

        /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            'iDisplayLength': 25,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            }
        });

        /* END BASIC */
        $('#date_alloc').daterangepicker({
            autoUpdateInput: false,
            opens: 'center',
            locale: {
                format: 'MM/DD/YYYY h:mm A',
                cancelLabel: 'Cancel'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            alert(start + '|' + end + '|' + label);
            // getResults();
        });

        $(function () {
            $('#search_date').daterangepicker({format: 'DD/MM/YYYY'});
        });
    });
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/customer.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/custom.css">
<script src="<?php echo BASE_URL; ?>assets/js/moment.min2.js"></script>
<script src="<?php echo BASE_URL; ?>assets/js/datepicker/daterangepicker.js"></script>