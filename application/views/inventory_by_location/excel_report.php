<?php
$ti=time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Inventory_byLocation_report".$ti.".xls");
header("Pragma: no-cache");
header("Expires: 0");
#echo "<pre>";print_r($item);echo "</pre>";
#echo "</br> : ".count($item);
?>
<table border="0" cellpadding="3" cellspacing="0" style="font-family:calibri; font-size:14px; width: 1000px;">
    <tr>
        <td colspan="2" width="100%">
            <table width="100%" border="1" style="border-collapse:collapse; margin-left:10px">
                <tr style="font-family:calibri; font-size:16px; font-weight:bold;">
                    <th width="6%" style="padding:10px">S.NO</th>
					<th width="25%">Branch Name</th>
                    <th width="25%">Brand Name</th>
                    <th width="20%">Item Description</th>
                    <th width="22%">Available Quantity</th>
                </tr>
                <?php if (count($inventory)) {?>
                <?php foreach($inventory as $key => $row) {?>                   
                    <tr>
                        <td style="padding:8px;"><?php echo ++ $key;?></td>
                        <td style="padding:8px;"><?php echo $row->branch_name; ?></td>
                        <td style="padding:8px;"><?php echo $row->cat_name; ?></td>
                        <td style="padding:8px;"><?php echo $row->pdt_name; ?> </td>
                        <td style="padding:8px;"><?php echo $row->available_qty; ?></td>
                    </tr>
                <?php }?>
                <?php } else {?>
                    <tr>
                        <td colspan="5" align="center">No Record Found</td>
                    </tr>
                <?php }?>
            </table>
        </td>
    </tr>
</table>