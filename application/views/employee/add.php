<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo base_url(); ?>employee/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                            </span>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="" action="<?php echo $form_action; ?>" method="post" id="employee_form" name="employee_form" enctype="multipart/form-data">

                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">                                   
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>

                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Employee Name <span style="color: red">*</span></label>
                                                    <label class="input"> <i class="icon-append fa fa-user-md"></i>
                                                        <input type="text" data-placeholder="XYZ"  name="emp_name" id="emp_name" maxlength="150">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Designation <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-user-md"></i>
                                                        <input type="text" data-placeholder="Software Engineer"  name="designation" id="designation" maxlength="50">
                                                    </label>
                                                </section>

                                            </div>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Contact No <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-phone"></i>
                                                        <input type="text" data-placeholder="9898989898"  name="phone_no" id="phone_no" maxlength="15">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Pass Type<span style="color: red">*</span></label>
                                                    <label class="select">
                                                        <select name="pass_type" id="pass_type" class="select2">
                                                            <option value="Citizen" selected="selected">Citizen</option>
                                                            <option value="PR">PR</option>
                                                            <option value="EP">EP</option>
                                                            <option value="S Pass">S Pass</option>
                                                            <option value="Work Permit">Work Permit</option>
                                                            <option value="LOC">LOC</option>
                                                        </select>  </label>
                                                </section>                                             
                                            </div>

                                            <div class="row"> 
                                                <section class="col col-6">
                                                    <label class="label">Salary Type<span style="color: red">*</span></label>
                                                    <label class="select">
                                                        <select name="salary_type" id="salary_type" class="select2 salary_type">
                                                            <option value="Local" selected="selected">Local</option>
                                                            <option value="Foreign">Foreign</option>
                                                        </select> </label>
                                                </section> 

                                                <section class="col col-6">
                                                    <label class="label">Payroll Number</label>
                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                        <input type="text" data-placeholder=" 5464646A" name="payroll_no" id="payroll_no" autocomplete="off">
                                                    </label>
                                                </section>                                             
                                            </div>

                                            <div class="row"> 
                                                <section class="col col-6">
                                                    <label class="label">Employee NRIC/FIN <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                        <input type="text" name="nric_no" id="nric_no"   data-placeholder=" 150AD00" autocomplete="off">
                                                    </label>
                                                </section> 

                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">NRIC/FIN Expiry Date </label>
                                                        <label class="input">
                                                            <div class="input-prepend input-group">
                                                                <input type="text" name="nric_date" id="nric_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="" />
                                                                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                            </div>
                                                        </label>
                                                    </section>
                                                </section>
                                            </div>

                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Passport No <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                        <input type="text" name="passport_no" id="passport_no"   data-placeholder=" AD150AD00" autocomplete="off">
                                                    </label>
                                                </section> 

                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Passport Expiry Date<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <div class="input-prepend input-group">
                                                                <input type="text" name="passport_expiry" id="passport_expiry" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="" />
                                                                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                            </div>
                                                        </label>
                                                    </section>
                                                </section>  
                                            </div>

                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Salary Amount <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-money"></i>
                                                        <input type="text" data-placeholder=" 150.00" name="salary" id="salary" autocomplete="off" class="float_value">
                                                    </label>
                                                </section> 

                                                <section class="col col-6">
                                                    <label class="label">CPF from employer</label>
                                                    <label class="input cpf_val"><i class="icon-append fa fa-money"></i>
                                                        <input type="text" name="cpf_val" id="cpf_val" class="float_value">
                                                    </label>
                                                </section>  
                                            </div>

                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">CPF from employee</label>
                                                    <label class="input cpf_employee"><i class="icon-append fa fa-money"></i>
                                                        <input type="text" name="cpf_employee" id="cpf_employee" class="float_value">
                                                    </label>
                                                </section> 

                                                <section class="col col-6">
                                                    <label class="label">Overtime amount per hour<span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-money"></i>
                                                        <input type="text"  name="overtime" id="overtime"   data-placeholder=" 10.00" autocomplete="off" class="float_value">
                                                    </label>
                                                </section>  
                                            </div>

                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Salary Account Number </label>
                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                        <input type="text" name="salary_acc_no" id="salary_acc_no"   data-placeholder=" AD12435768" autocomplete="off">
                                                    </label>
                                                </section> 

                                                <section class="col col-6">
                                                    <label class="label">Salary Account Bank </label>
                                                    <label class="input"><i class="icon-append fa fa-bank"></i>
                                                        <input type="text" name="salary_acc_bank" id="salary_acc_bank"   data-placeholder=" Federal Bank" autocomplete="off">
                                                    </label>
                                                </section>  
                                            </div>

                                            <div class="row">
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Status<span style="color: red">*</span></label>
                                                        <label class="select">
                                                            <select  name="status" class="select2">
                                                                <option value="1">Active</option>
                                                                <option value="2">Inactive</option>
                                                            </select>
                                                        </label>
                                                    </section>
                                                </section>
                                            </div>                                      
                                        </fieldset>

                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Document Upload</legend>
                                            <div class="row material-container product_details">
                                                <div class="upload clr">												 
                                                    <section class="col col-5">
                                                        <label class="label"><b>Document Title</b></label>
                                                        <label class="input">
                                                            <input type="text" name="doc_title[]" class="doc_title" data-placeholder="Document Title" />
                                                        </label>                                                       
                                                    </section>

                                                    <section class="col col-6 section-group">
                                                        <label class="label"><b>Upload File</b></label>
                                                        <label for="file" class="input input-file">
                                                            <input type="file" class="form-control btn btn-default parallel img_file" name="logo[]" value="" style="height: 40px;"  data-allowed-types="png|jpg|jpeg|jif">

                                                        </label><span class="img_preview" style="padding-top: 4px;float: right;"></span>
                                                    </section>

                                                    <section class="col col-1">
                                                        <label class="label">&nbsp;</label>
                                                        <label class="input">
                                                            <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                        </label>
                                                    </section>
                                                </div>			
                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/employee.js"></script>
