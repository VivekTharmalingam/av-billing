<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
				<a href="<?php echo base_url(); ?>employee/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
				
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-file-text-o"></i> </span>
                            <h2><?php echo $page_title;?></h2>	
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body">  
                                <span id="watermark"><?php echo WATER_MARK; ?></span>
                                 <table id="user" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Employee Code</td>
                                            <td style="width:65%"><?php echo $employee->employee_code;?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Company Name</td>
                                            <td><?php echo $employee->employee_name;?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Designation</td>
                                            <td><?php echo $employee->designation;?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Contact No</td>
                                            <td><?php echo $employee->contact_number;?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Pass Type</td>
                                            <td><?php echo $employee->pass_type;?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Payroll Number</td>
                                            <td><?php echo $employee->payroll_no;?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Employee NRIC/FIN</td>
                                            <td><?php echo $employee->nric_no;?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">NRIC/FIN Expiry Date</td>
                                            <td><?php echo date('d/m/Y',strtotime($employee->nric_expiry_date));?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Passport No</td>
                                            <td><?php echo $employee->passport_no;?></td>
                                        </tr>
										<tr>
                                            <td style="font-weight: bold;">Passport Expiry Date</td>
                                            <td><?php echo date('d/m/Y',strtotime($employee->passport_expiry_date));?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Salary Amount</td>
                                            <td><?php echo $employee->salary_amount; ?></td>
                                        </tr>
										<tr>
                                            <td style="font-weight: bold;">CPF from employer </td>
                                            <td><?php echo $employee->cpf_from_employer; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">CPF from employee</td>
                                            <td><?php echo $employee->cpf_from_employee; ?></td>
                                        </tr>
										<tr>
                                            <td style="font-weight: bold;">Overtime amount per hour </td>
                                            <td><?php echo $employee->overtime_amt_per_hour; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Salary Account Number</td>
                                            <td><?php echo $employee->salary_account_no; ?></td>
                                        </tr>
										<tr>
                                            <td style="font-weight: bold;">Salary Account Bank</td>
                                            <td><?php echo $employee->salary_account_bank; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Status</td>
                                            <td><?php echo $employee->status_str;?></td>
                                        </tr>                                        
                                    </tbody>
                                </table>
								
								<div class="text-left">
									<span class="onoffswitch-title">
									  <h3><b style="color: #8e233b;">Document Upload</b></h3>
									</span>
								</div>
								
								<div id="widget-tab-1" class="table-responsive">
                                        <table id="dt_basic_2" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
                                           <thead>
                                              <tr>
                                                   <th data-class="expand" style="font-weight: bold; text-align:center;">S.No</th>
                                                   <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Document Title</th>                                                   
                                                   <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Upload File</th>                                        
                                              </tr>
                                           </thead>
                                           <tbody>
                                               <?php foreach($emp_doc as $key=>$doc){?>
                                                    <tr>
                                                        <td align="center"><?php echo ++$key;?></td>
                                                        <td align="right"><?php echo $doc->doc_title;?></td>
                                                        <td align="right"><a style="text-decoration: none;" target="blank" title="Click to View File" href="<?php echo upload_url().$doc->doc_file;?>"><?php echo text($doc->doc_file);?></a></td>
                                                    </tr>
                                               <?php } ?>
                                           </tbody>                                           
                                        </table>
								</div>							
                            </div>                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
