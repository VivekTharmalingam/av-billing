<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="widget-body">
                        <a href="<?php echo base_url(); ?>item/add" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-pencil"></i></button></a>
                        <!-- Widget ID (each widget will need unique ID)-->                                
                        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false"	>
                            <header><span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                <h2>Item List</h2></header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body no-padding">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>			                
                                            <tr>
                                                <th style="width: 5%;" data-hide="phone"  data-class="expand">S.No</th>
                                                <th style="width: 10%;" ><i class="fa fa-fw fa-key text-muted hidden-md hidden-sm hidden-xs"></i> Item Code</th>
                                                <th style="width: 15%;" ><i class="fa fa-fw fa-key text-muted hidden-md hidden-sm hidden-xs"></i> Bar Code</th>
                                                <th style="width: 15%;" ><i class="fa fa-fw fa-key text-muted hidden-md hidden-sm hidden-xs"></i> Srl No.</th>
                                                <th style="width: 15%;" data-hide="phone,tablet"><i class="fa fa-fw fa-sitemap text-muted hidden-md hidden-sm hidden-xs" ></i>Brand Name</th>
                                                <th style="width: 35%;" data-hide="phone"><i class="fa fa-fw fa-archive text-muted hidden-md hidden-sm hidden-xs"></i> Item Description</th>
                                                <th style="width: 10%;" data-hide="phone,tablet"><i class="fa fa-fw fa-flag-o text-muted hidden-md hidden-sm hidden-xs"></i>Status</th>
                                                <th style="width: 25%;" data-hide=""><i class="fa fa-fw fa-cog txt-color-blue hidden-md hidden-sm hidden-xs"></i> Actions</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <!-- end widget content -->
                            </div>
                        </div> 
                    </div>
                </article>
                <!-- /.row -->
            </div>
        </section>
    </div>
</div>
<iframe id="thermal_content" src="" style="display: none;"></iframe>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/your_style.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/datatables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/product.js"></script>
<script type="text/javascript">
    var barcode_print_timer;
	$(document).on('click', '.print-barcode', function (e) {
		var hrf = $(this).attr('data-href');
        $('#thermal_content').attr('src', hrf);
		barcode_print_timer = setInterval(print_barcode, 1000);
		/*var w = window.open();
		var content = '<html>';
		content += '<body style="margin: 0;">';
		//content += '<div  style="float:left; width: 288px;">';
		//for (var i = 1; i <= Value; i++) {
			content += $('#thermal_content').contents().find('.barcode').html();
			//content += $('#thermal_content').contents().find('.barcode').html();
		//}
		//content += '</div>';
		content += '</body>';
		content += '</html>';
		w.document.write(content);  //only part of the page to print, using jquery
		//w.document.close(); //this seems to be the thing doing the trick
		//w.focus();
		//w.print();
		//w.close();
		$('#myModal').modal('hide');*/
	});
	
    /*$(document).on('click', '#smart-mod-eg2', function (e) {
        var hrf = $(this).attr('data-href');
        $('#thermal_content').attr('src', hrf);
        $.SmartMessageBox({
            title: "Item Barcode",
            content: "How Many Barcode need to print?",
            buttons: "[Cancel][Submit]",
            input: "text",
            inputValue: '1',
            placeholder: "How many barcode copy you need?",
        }, function (ButtonPress, Value) {
            if (ButtonPress == "Cancel") {
                return 0;
            }
            if (ButtonPress == "Submit") {
				//if ($('#thermal_content').contents().find('.barcode').html() == '') {
				//if(true) {
				//  alert('Barcode print not yet ready. Please wait!');
				//	setTimeout(print_barcode, 2000)
				//}
				//else {
					var w = window.open();
					var content = '<html>';
					content += '<body style="margin: 0;">';
					//content += '<div  style="float:left; width: 288px;">';
					//for (var i = 1; i <= Value; i++) {
						content += $('#thermal_content').contents().find('.barcode').html();
						//content += $('#thermal_content').contents().find('.barcode').html();
					//}
					//content += '</div>';
					content += '</body>';
					content += '</html>';
					w.document.write(content);  //only part of the page to print, using jquery
					//w.document.close(); //this seems to be the thing doing the trick
					//w.focus();
					//w.print();
					//w.close();
					$('#myModal').modal('hide');
				//}
            }
        });
        e.preventDefault();
    });*/
	
	function print_barcode() {
		if ($('#thermal_content').contents().find('.barcode').html() == '') {
			return false;
		}
		
		clearInterval(barcode_print_timer);
		var w = window.open();
		var content = '<html>';
		content += '<body style="margin: 0;">';
		//content += '<div  style="float:left; width: 288px;">';
		//for (var i = 1; i <= Value; i++) {
			content += $('#thermal_content').contents().find('.barcode').html();
			//content += $('#thermal_content').contents().find('.barcode').html();
		//}
		//content += '</div>';
		content += '</body>';
		content += '</html>';
		w.document.write(content);  //only part of the page to print, using jquery
		w.document.close(); //this seems to be the thing doing the trick
		w.focus();
		w.print();
		w.close();
	}
</script>