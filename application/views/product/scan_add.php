<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo base_url(); ?>item/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                            </span>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="" action="<?php echo $form_action; ?>" method="post" id="product_form" name="product_form" enctype="multipart/form-data">

                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">  
                                        
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>
                                            
                                            <div class="row">                                                

                                                <section class="col col-6">
                                                    <label class="label">Scan Barcode No </label>
                                                    <label class="input"><i class="icon-append fa  fa-barcode"></i>
                                                        <input type="text" data-placeholder="Scan BarCode"  name="search_barcode" id="search_barcode" maxlength="50" class="search_barcode">
                                                    </label>
                                                </section>
                                            </div> 
                                            <input type="hidden" name="method" id="method" value="1">
                                        </fieldset>
                                        <fieldset class="new_product" style="display: none; border-top:0px;">                                                   <legend style=" font-size: 13px; color:#00897b;">This is new. Please add product for this barcode.</legend>
                                            
                                            <div class="row"></div>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Brand Name</label>
                                                        <label class="select">
                                                            <select name="cat_name" class="cat_name" class="select2">  
                                                                <option value="">Select Brand</option>
                                                                <?php foreach ($category as $key => $val) { ?>
                                                                    <option value="<?php echo $val->id; ?>"><?php echo $val->cat_name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                </section>

                                                <section class="col col-6">
                                                    <label class="label">Barcode No </label>
                                                    <label class="input"><i class="icon-append fa  fa-sort-numeric-asc"></i>
                                                        <input type="text" data-placeholder=" ABC123"  name="bar_code" id="bar_code" maxlength="50">
                                                    </label>
                                                </section>
                                            </div>
                                            
                                            <div class="row">       
                                                <section class="col col-6">
                                                    <label class="label">Serial No </label>
                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                        <input type="text" data-placeholder=" 123"  name="s_no" id="s_no" maxlength="15">
                                                    </label>
                                                </section>

                                                <section class="col col-6">
                                                    <label class="label">Item Code </label>
                                                    <label class="input"><i class="icon-append fa fa-key"></i>
                                                        <input type="text" data-placeholder=" PDT_LEN006"  name="pdt_code" id="pdt_code" maxlength="15" >
                                                    </label>
                                                </section>
                                            </div>

                                            <div class="row">       
                                                <section class="col col-6">
                                                    <label class="label">Item Description </label>
                                                    <label class="textarea textarea-expandable"><textarea rows="3" name="pdt_desc" id="pdt_desc" maxlength="500"></textarea> </label> <div class="note">
                                                        <strong>Note:</strong> expands on focus.
                                                    </div>
                                                </section>

                                                <section class="col col-6">
                                                    <label class="label">Selling Price </label>
                                                    <label class="input"><i class="icon-append fa fa-money float_value"></i>
                                                        <input type="text" data-placeholder="11.00"  name="sel_price" id="sel_price" maxlength="15" class="float_value">
                                                    </label>
                                                </section>
                                            </div>

                                            <div class="row"> 
                                                <section class="col col-6">
                                                    <label class="label">Cost Price </label>
                                                    <label class="input"><i class="icon-append fa fa-money"></i>
                                                        <input type="text" data-placeholder="10.00"  name="buy_price" id="buy_price" maxlength="15" class="float_value">
                                                    </label>
                                                </section> 

                                                <section class="col col-6">
                                                    <label class="label">Initial Quantity</label>
                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                        <input type="text" data-placeholder="1.00"  name="initial_quantity" id="initial_quantity" maxlength="15" class="float_value" value="1">
                                                    </label>
                                                </section>  
                                            </div>                                            

                                        </fieldset>
                                        
                                        <fieldset class="old_product">
                                            <div class="row">       
                                                <section class="col col-6">
                                                    <label class="label">Quantity</label>
                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                        <input type="text" data-placeholder=" 123"  name="new_quantity" id="new_quantity" class="new_quantity" maxlength="15">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <section class="alert alert-info product_details" id="product_details" style="display: none;">
                                                        <label class="label">
                                                            <b>Item Details</b>
                                                        </label>
                                                        <input type="hidden" name="hidden_pdt_id" id="hidden_pdt_id">
                                                        <label class="label" >Item Code: <span class="product_code" style="color:#00897b;font-weight: bold;" >customer_name</span></label>
                                                        
                                                        <label class="label">Item Description: <span class="product_desc" style="color:#00897b;font-weight: bold;" >Description</span></label>
                                                        
                                                        <label class="label">Brand Name: <span class="brand_name" style="color:#00897b;font-weight: bold;" >Brand Name</span></label>
                                                        <label class="label">Available Quantity: <span class="avail_qty" style="color:#00897b;font-weight: bold;" >Quantity</span></label>
                                                    </section>
                                                 </section>
                                               
                                            </div>  

                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/scan_product.js"></script>
