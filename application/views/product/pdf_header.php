<html>
    <head>
        <style>
            @page {
                size: portrait;
                header: html_header;
                footer: html_footerPageNo;
                margin-footer: 0px;
                margin-bottom: 61mm;
            }
            .barcode {
                padding: 1.5mm;
                margin: 0;
                vertical-align: top;
                color: #424242;
            }
            .title{
                font-size: 8px;
            }
            .barcodecell, .title{
                text-align:center;
            }
            .tot{
                float: left;
                width:1000px;
            }
            .sec{
                float: left;
                width:48%; 
                margin-top: 20px; 
            }
        </style>
    </head>
    <body>
    <htmlpagefooter name="footerPageNo" style="display:none">
        <table width="100%" style="vertical-align: bottom; font-family: calibri; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
            <tr>
                <td width="100%" align="center">Page {PAGENO}/{nbpg}</td>
            </tr>
        </table>
    </htmlpagefooter>
    <htmlpageheader name="header" style="display:block">
        <div class="tot">
            <?php
            if (!empty($nos)) {
                for ($i = 1; $i <= $nos; $i++) {
                    ?>
                    <div class="sec">
                        <div class="barcodecell">
                            <barcode code="<?php echo strtoupper($product->pdt_code); ?>" type="C39" height="0.55" class="barcode" />
                        </div>
                        <div class="title"><?php echo strtoupper($product->pdt_code); ?></div>
                    </div>
                <?php
                }
            }
            ?>
        </div>
    </htmlpageheader>