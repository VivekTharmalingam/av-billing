<script type="text/javascript">
    $(document).ready(function () {
        $('#print').focus();
    });
</script>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) {
                    ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>

<!--<form class="print_form" action="<?php echo $form_action; ?>" method="post">-->
        <!-- Main content -->
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo $list_link; ?>" class="large_icon_des" style=" color: white;">
                        <button type="button" class="btn btn-primary btn-circle btn-xl temp_color ">
                            <i class="glyphicon glyphicon-list"></i>
                        </button>
                    </a>
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>Item Barcode </h2>
                        </header>

                        <div style="padding: 0px 0px 0px 0px;">
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <div class="widget-body" style="padding-bottom: 0px;">
                                <div class="smart-form" id ="sales_form">
                                    <fieldset>
                                        <div class="row">
                                            <section class="col col-md-12" style="text-align: center;">

                                            </section>

                                            <section class="col col-md-12" style="text-align: center; margin: 7% 0;">
                                                <input type="hidden" class="form-control" name="so_id" value="<?php echo $so_id; ?>" />
                                                <p style="margin-bottom: 60px; font-size: 30px;">Do you want to take a print ?</p>
                                                <!--<a href="#" id="smart-mod-eg2" class="btn btn-primary"> With Input </a>-->
                                                <button type="button" class="btn btn-lg btn-default" onclick="location.href = '<?php echo $list_link; ?>'">Cancel</button>
                                                <!--<a href="<?php echo $redirect_link; ?>" id="smart-mod-eg2"><button type="button" name="print" id="print" class="btn btn-lg btn-primary" style="margin-left: 100px;" onclick="window.open('<?php echo $invoice_link; ?>', '_blank')">Print Barcode</button></a>-->                                              
                                                <a href="<?php echo $redirect_link; ?>" id="smart-mod-eg2"><button type="button" name="print" id="print" class="btn btn-lg btn-primary" style="margin-left: 100px;" >Print Barcode</button></a>                                              

                                            </section>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
        <!--</form>-->
    </div>
</div>

<iframe id="thermal_content" src="<?php echo $thermal_src; ?>" style="display: none;"></iframe>
<script type="text/javascript">
    $("#smart-mod-eg2").click(function (e) {
        $.SmartMessageBox({
            title: "Item Barcode",
            content: "How Many Barcode need to print?",
            buttons: "[Cancel][[Accept]",
            input: "text",
            inputValue:'1',
            placeholder: "How many barcode copy you need?",           
        }, function (ButtonPress, Value) {
            if (ButtonPress == "Cancel") {
                return 0;
            }
            if (ButtonPress == "Accept") {
               // alert(Value);
               window.location.replace('<?php echo $invoice_link; ?>/'+ Value);
            }            
        });
        e.preventDefault();
    })

    function printResults(id, browser_txt) {
        var w = window.open();
        var content = '<html>';
        content += '<body>';
        content += $('#thermal_content').contents().find('body').html();
        content += '</body>';
        content += '</html>';
        w.document.write(content);  //only part of the page to print, using jquery
        w.document.close(); //this seems to be the thing doing the trick
        w.focus();
        w.print();
        w.close();
        $('#myModal').modal('hide');
    }
</script>