
<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                     <a href="<?php echo base_url(); ?>item/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>Item - View</h2>	
<!--                             <span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Users List"><a href="<?php echo $list_link; ?>" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a></span>-->
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body"> 
                                <span id="watermark"><?php echo WATER_MARK; ?></span>
                                 <table id="user" class="table table-bordered table-striped" style="clear: both;">
                                    <tbody>
                                        <tr>
                                            <td><b>Barcode No</b></td>
                                            <td><?php echo $product->bar_code;?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;"><b>Item Code</b></td>
											<td style="width:65%;"><?php echo $product->pdt_code;?></td>
										</tr>
                                        <tr>
                                            <td><b>Brand Name</b></td>
                                            <td><?php foreach($category as $key => $val){ ?>
                                                      <?php if ($val->id == $product->cat_id ){ echo $val->cat_name; }?>
                                                <?php } ?></td>
                                        </tr>
                                        <!--<tr>
                                            <td><b>Model No</b></td>
                                            <td><?php echo $product->model_no;?></td>
                                        </tr>-->
                                        <tr>
                                            <td><b>Item Category</b></td>
                                            <td><?php echo text($product->item_category_name);?></td>
                                        </tr> 
                                        <tr>
                                            <td><b>Sub Category</b></td>
                                            <td><?php echo text($product->sub_category_name);?></td>
                                        </tr> 
                                        <tr>
                                            <td><b>Item Description</b></td>
                                            <td><?php echo nl2br($product->pdt_name);?></td>
                                        </tr> 
                                        <tr>
                                            <td><b>Cost Price</b></td>
                                            <td><?php echo text_amount($product->buying_price);?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Selling Price</b></td>
                                            <td><?php echo text_amount($product->selling_price);?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Serial No</b></td>
                                            <td><?php echo $product->s_no;?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Warranty</b></td>
                                            <td><?php echo $product->warranty;?></td>
                                        </tr>
                                        <?php if(!empty($product->image)){ ?>
                                        <tr>
                                            <td><b>Image</b></td>
                                            <td><img src="<?php echo ( !empty( $product->image  ) ? BASE_URL . UPLOADS . $product->image : BASE_URL . NO_IMG ); ?>" width="35" height="35"/></td>
                                        </tr>        
                                        <?php } ?>
                                        <tr>
                                            <td><b>Status</b></td>
                                            <td><?php echo $product->status_str; ?></td>
                                        </tr>
                                    </tbody>
                                </table> 
								
                                <div class="text-left">
                                        <span class="onoffswitch-title">
                                          <h3><b style="color: #8e233b;">Inventory Details</b></h3>
                                        </span>
                                </div>
									
                                <div id="widget-tab-1" class="table-responsive">
                                    <table id="dt_basic_2" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">											  
                                       <thead>
                                        <tr>
                                            <th data-class="expand" style="font-weight: bold; text-align:center;">S.No</th>
                                            <th data-hide="phone,tablet" style="font-weight: bold;text-align:left;">Branch Name</th> 
                                            <th data-hide="phone,tablet" style="font-weight: bold;text-align:left;">Item Location</th>
                                            <th data-hide="phone,tablet" style="font-weight: bold;text-align:left;">Branch Address</th> 
                                            <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Quantity</th>
                                            <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                   <?php foreach($inventory as $key=>$inv){?>
                                        <tr>
                                            <td align="center"><?php echo ++$key;?></td>
                                            <td><?php foreach($branch as $key => $val){ ?>
                                            <?php if ($val->id == $inv->brn_id ){ echo $val->branch_name; }?>
                                            <?php } ?></td>
                                            <td>
                                            <?php echo $inv->branch_location; ?>
                                            </td>
                                            <td><?php foreach($branch as $key => $val){ ?>
                                            <?php if ($val->id == $inv->brn_id ){ echo $val->address; }?>
                                            <?php } ?></td>
                                            <td align="center"><?php echo $inv->available_qty;?></td>
                                            <td align="center"><?php echo $inv->status_str;?></td>
                                        </tr>
                                   <?php } ?>
                                        </tbody>                                        
                                    </table>
                                </div>
                        </div>
                        <!-- end widget div -->

                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/product.js"></script>