<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/plugin/JsBarcode/EAN_UPC.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/plugin/JsBarcode/CODE128.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/plugin/JsBarcode/JsBarcode.js"></script>       
    </head>
    <body>
        <div class="barcode">
            <div style="float:left; width: 120px; height: 84px; margin-left: 14px; margin-top: 0px; border:1px solid; border-radius:10px;">
                <div class="pcode" style="display: none;"><?php echo $product->pdt_code; ?></div>
                <div style="width:100%; text-align: center;">
                    <div style="text-align: center; font-size: 11px; margin: 1px 0px;font-weight: bold; height: 24px;"><?php echo $product->pdt_name; ?></div>
                    <img class="barcode3" style="height: 22px; width: 100%;" />
                    <div style="font-size: 11px; margin: 1px 0px; text-align: center;"><?php echo ($product->pdt_code) ? $product->pdt_code : ''; ?></div>
                    <div style="font-size: 14px; text-align: center;font-weight: bold;"><?php echo ($product->selling_price) ? '$ ' . number_format($product->selling_price, 2) : ''; ?></div>
                    <script>$(".barcode3").JsBarcode($('.pcode').html(), {width: 2, height: 25, displayValue: false, fontSize: 12});</script>
                </div>
            </div>
        </div>
    </body>
</html>
