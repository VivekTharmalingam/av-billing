<!-- MAIN PANEL -->

<div id="main" role="main">    

    <!-- RIBBON -->

    <div id="ribbon">

        <span class="ribbon-button-alignment"> 

            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">

                <i class="fa fa-refresh"></i>

            </span> 

        </span>

        <!-- breadcrumb -->				

        <?php echo breadcrumb_links(); ?>

    </div><!-- END RIBBON -->    

    <!-- MAIN CONTENT -->

    <div id="content">

        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <?php if (!empty($success)) { ?>

                    <div class="alert alert-success fade in" >

                        <button class="close" data-dismiss="alert">×</button>

                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>

                        <strong>Success</strong> <?php echo $success; ?>

                    </div>

                <?php } else if (!empty($error)) { ?>

                    <div class="alert alert-danger alert-dismissable" >

                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>

                    </div>

                <?php } ?>&nbsp;

            </div>

            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">

            </div>

        </div>

        <!-- widget grid -->

        <section id="widget-grid" class="">

            <!-- START ROW -->

            <div class="row">

                <!-- NEW COL START -->

                <article class="col-sm-12 col-md-12 col-lg-12">

                    <a href="<?php echo base_url(); ?>item/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <!-- Widget ID (each widget will need unique ID)-->

                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						

                        <header>

                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>

                            <h2><?php echo $page_title; ?></h2>

                        </header>				

                        <!-- widget div-->

                        <div>			

                            <!-- widget edit box -->

                            <div class="jarviswidget-editbox">

                                <!-- This area used as dropdown edit box -->				

                            </div>

                            <!-- end widget edit box -->

                            <form class="edit_form" action="<?php echo $form_action; ?>" method="post" id="item_form" name="item_form" enctype="multipart/form-data">

                                <!-- widget content -->

                                <div class="widget-body no-padding">

                                    <div class="smart-form">



                                        <fieldset>                                      

                                            <legend style=" font-size: 12px; color: red;">

                                                <span style="color: red">*</span>Fields are mandatory

                                            </legend>

                                            <div class="row">  

                                                <input type="hidden" class="form-control copy_product_id" name="copy_product_id" id="copy_product_id" value="" />

                                                <input type="hidden" class="form-control product_id" name="product_id" id="product_id" value="<?php echo $product->id; ?>" />



                                                <section class="col col-6">

                                                    <label class="label">Barcode No </label>

                                                    <label class="input"><i class="icon-append fa fa-barcode"></i>

                                                        <input type="text" name="bar_code" id="bar_code" maxlength="50" value="<?php echo $product->bar_code; ?>" class="ignore" />

                                                    </label>

                                                </section>



                                                <section class="col col-6">

                                                    <label class="label">Serial No </label>

                                                    <label class="input"><i class="icon-append fa fa-barcode"></i>

                                                        <input type="text" name="s_no" id="s_no" maxlength="15" value="<?php echo $product->s_no; ?>">

                                                    </label>

                                                </section>

                                            </div>



                                            <div class="row">

                                                <!--<section class="col col-6">

                                                    <label class="label">Model No </label>

                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>

                                                        <input type="text" data-placeholder="ABC-123" name="model_no" id="model_no" maxlength="15" value="<?php echo $product->model_no; ?>" />

                                                    </label>

                                                </section>-->



                                                <section class="col col-6">

                                                    <section>

                                                        <label class="label">Brand Name

                                                            <span class="new-window-popup btn btn-primary pull-right" style="margin-bottom: 3px;" data-iframe-src="<?php echo $add_category_link; ?>" data-cmd="brand" data-title="Brand Add">

                                                                <i class="fa fa-plus-square-o" style="font-size: 16px;"> </i> Add Brand

                                                            </span>

                                                        </label>

                                                        <label class="select">

                                                            <select id="cat_name" name="cat_name" class="cat_name select2">

                                                                <option value="">Select Brand</option>

                                                                <?php foreach ($category as $key => $val) { ?>

                                                                    <option value="<?php echo $val->id; ?>" <?php

                                                                    if ($val->id == $product->cat_id) {

                                                                        echo 'selected';

                                                                    }

                                                                    ?> > <?php echo $val->cat_name; ?></option>

<?php } ?>

                                                            </select>

                                                        </label>

                                                    </section>

                                                </section>



                                                <section class="col col-6">

                                                    <label class="label">Selling Price </label>

                                                    <label class="input"><i class="icon-append fa fa-money float_value"></i>

                                                        <input type="text" data-placeholder="11.00"  name="sel_price" id="sel_price" maxlength="15" value="<?php echo $product->selling_price; ?>" class="float_value">

                                                    </label>

                                                </section>

                                            </div>



                                            <div class="row">

                                                <section class="col col-6">

                                                    <label class="label">Item Category </label>

                                                    <label class="select">

                                                        <select name="item_cat" class="item_cat select2" id="item_cat">  

                                                            <option value="">Select Item Category</option>

                                                            <?php foreach ($item_category as $key => $val) { ?>

                                                                <option value="<?php echo $val->id; ?>" <?php

                                                                        if ($val->id == $product->item_category) {

                                                                            echo 'selected';

                                                                        }

                                                                        ?> > <?php echo $val->item_category_name; ?></option>

<?php } ?>

                                                        </select>

                                                    </label>

                                                </section>



                                                <section class="col col-6">

                                                    <label class="label">Cost Price </label>

                                                    <label class="input"><i class="icon-append fa fa-money"></i>

                                                        <input type="text" data-placeholder="10.00"  name="buy_price" id="buy_price" maxlength="15" value="<?php echo $product->buying_price; ?>" class="float_value">

                                                    </label>

                                                </section>

                                            </div>

                                            <div class="row">

                                                <section class="col col-6">

                                                    <label class="label">Sub Category </label>

                                                    <label class="select">

                                                        <select name="sub_cat" class="sub_cat select2" id="sub_cat">  

                                                            <option value="">Select Sub Category</option>

                                                                    <?php foreach ($subcat as $key => $value) { ?>

                                                                <option value="<?php echo $value->id; ?>" <?php

                                                                    if ($value->id == $product->sub_category) {

                                                                        echo 'selected';

                                                                    }

                                                                    ?> > <?php echo $value->sub_category_name; ?></option>

<?php } ?>

                                                        </select>

                                                    </label>

                                                </section>

                                                <section class="col col-6">

                                                    <label class="label">Image</label>

                                                    <div class="parallel thumb preview" style="width: 9%; position: absolute;" >

                                                        <img src="<?php echo (!empty($product->image) ? BASE_URL . UPLOADS . $product->image : BASE_URL . NO_IMG ); ?>" />

                                                    </div>

                                                    <label class="" style="width: 50%; height: 50px; margin-left: 10%;">

                                                        <input type="file" class="btn btn-default parallel" name="file" data-actual="<?php echo $product->image; ?>" data-url="<?php echo $list_link . 'remove/image/' . $product->id; ?>"  data-default="<?php echo BASE_URL . NO_IMG; ?>" value="" style=" width: 176%; padding: 10px 0px 0px 10px;"/>

                                                    </label>



                                                </section>



                                            </div>

                                            <div class="row">

                                                <section class="col col-6">

                                                    <label class="label">Item Description <span style="position: absolute;right: 15px;">Item Code : <b style="color: #3276B1;"><?php echo $product->pdt_code; ?></b></span></label>

                                                    <label class="textarea textarea-expandable"><textarea rows="3" name="pdt_desc" id="pdt_desc" maxlength="500"><?php echo $product->pdt_name; ?></textarea> </label> <div class="note">

                                                        <strong>Note:</strong> expands on focus.

                                                    </div>

                                                </section>
                                                
                                                <section class="col col-6">
                                                    <label class="label">Item Specification</label></label>
                                                    <label class="textarea textarea-expandable"><textarea rows="3" name="pdt_spec" id="pdt_spec" maxlength="500"><?php echo $product->pdt_spec; ?></textarea> </label> 
                                                </section>
                                            </div>

                                            <div class="row">

                                                <section class="col col-6">

                                                    <label class="label">Warranty</label>

                                                    <label class="input"><i class="icon-append fa fa-bookmark"></i>

                                                        <input type="text" name="warranty" id="warranty" class="warranty" value="<?php echo $product->warranty; ?>">

                                                    </label>

                                                </section>
                                                
                                                <section class="col col-6">

                                                    <label class="label">Item Location</label>

                                                    <label class="input">

													<?php foreach ($branches as $key => $branch) { ?>

                                                        <input type="hidden" name="branch_id[]" value="<?php echo $branch->brn_id; ?>" class="branch_id" maxlength="15">

														<label class="col-md-4"><?php echo str_replace(' ', '&nbsp', $branch->branch_name); ?>

														<input type="text" name="branch_location[]" style="width: 50%; margin-top: 2%;" value="<?php echo (!empty($branch->branch_location)) ? $branch->branch_location : ''; ?>" class="branch_location" maxlength="200">

														</label>

													<?php } ?>

                                                    </label>

                                                </section>

                                            </div>
                                            
                                            <div class="row">
                                            	<section class="col col-6">

                                                    <label class="label">Status</label>

                                                    <label class="select">

                                                        <select name="status" id="status" class="select2">

                                                            <option value="1" <?php echo ($product->status_str == 'Active') ? 'selected' : ''; ?>>Active</option>

                                                            <option value="2"<?php echo ($product->status_str == 'Inactive') ? 'selected' : ''; ?>>Inactive</option>

                                                        </select>  

                                                    </label>

                                                </section>
                                            </div>

                                        </fieldset>

                                        <footer>

                                            <button type="submit" name="update_item" class="btn btn-primary">Submit</button>

                                            <button type="reset" class="btn btn-default">Cancel</button>

                                        </footer>

                                    </div>

                                </div>

                                <!-- end widget content -->

                            </form>

                        </div><!-- end widget div -->

                    </div><!-- end widget -->

                </article><!-- END COL -->

            </div><!-- END ROW -->				

        </section><!-- end widget grid -->

    </div><!-- END MAIN CONTENT -->

</div><!-- END MAIN PANEL -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/product.js"></script>