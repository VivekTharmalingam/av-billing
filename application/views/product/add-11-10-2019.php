<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo base_url(); ?>item/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                            </span>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form action="<?php echo $form_action; ?>" method="post" id="item_form" name="item_form" enctype="multipart/form-data">
                                <input type="hidden" class="form-control copy_product_id" name="copy_product_id" id="copy_product_id" value="<?php echo $product->id; ?>" />
                                <input type="hidden" class="form-control copy_subcat_id" name="copy_subcat_id" id="copy_subcat_id" value="<?php echo $product->sub_category; ?>" />
                                <input type="hidden" class="form-control product_id" name="product_id" id="product_id" value="" />
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">                                   
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>
                                            
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Scan Barcode No 
														<span style="float: right; padding:3px 4px 3px 4px; margin-top: 2px;" class="alert-warning item-not-exists">Item not available</span>
													</label>
                                                    <label class="input"><i class="icon-append fa  fa-barcode"></i>
                                                        <input type="text" name="search_barcode" id="search_barcode" maxlength="50" class="search_barcode" autofocus />
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label" style="height: 25px;"><span style="float: left;">Scan Serial No </span>
                                                        <span class="item-serialnum-popup btn btn-primary" style="float: right;padding:3px 4px 3px 4px;margin-top: 2px;" title="Quantity should not be empty to enter Serial No.">
                                                            <i class="fa fa-plus-square-o" style="font-size: 14px;"> </i> Serial No
                                                        </span>
                                                    </label>
                                                    <label class="input"><i class="icon-append fa  fa-barcode"></i>
                                                        <input type="text" data-placeholder="" readonly="" name="s_no" id="s_no"  class="serial_no item_sl_no">
                                                    </label>
                                                </section>
                                            </div> 
                                            <input type="hidden" name="is_update" id="is_update" value="" />
                                            <div class="row">
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label" style="height: 25px;"><span style="float: left;">Brand Name</span>
                                                            <span class="new-window-popup btn btn-primary pull-right" style="margin-bottom: 3px;float: right;" data-iframe-src="<?php echo $add_category_link; ?>" data-cmd="brand" data-title="Brand Add">
                                                                <i class="fa fa-plus-square-o" style="font-size: 16px;"> </i> Add Brand
                                                            </span>
                                                        </label>
                                                        <label class="select">
                                                            <select name="cat_name" class="cat_name select2" id="cat_name">  
                                                                <option value="">Select Brand</option>
                                                                <?php foreach ($category as $key => $val) { ?>
                                                                    <option value="<?php echo $val->id; ?>" <?php echo ($val->id == $product->cat_id) ? 'selected' : ''; ?> ><?php echo $val->cat_name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--<section class="col col-6">
                                                    <label class="label">Model No </label>
                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                        <input type="text" data-placeholder="ABC-123" name="model_no" id="model_no" maxlength="15">
                                                    </label>
                                                </section>-->

                                                <section class="col col-6" style="display: none">
                                                    <label class="label">Barcode No </label>
                                                    <label class="input"><i class="icon-append fa  fa-sort-numeric-asc"></i>
                                                        <input type="text" data-placeholder=" ABC123"  name="bar_code" id="bar_code" maxlength="50">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Selling Price </label>
                                                    <label class="input"><i class="icon-append fa fa-money float_value"></i>
                                                        <input type="text" data-placeholder="11.00"  name="sel_price" id="sel_price" maxlength="15" class="float_value" value="<?php echo $product->selling_price; ?>">
                                                    </label>
                                                </section>
                                            </div>

                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Item Category </label>
                                                    <label class="select">
                                                        <select name="item_cat" class="item_cat select2" id="item_cat">  
                                                            <option value="">Select Item Category</option>
                                                            <?php foreach ($item_category as $key => $val) { ?>
                                                                <option value="<?php echo $val->id; ?>" <?php echo ($val->id == $product->item_category) ? 'selected' : ''; ?>><?php echo $val->item_category_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Cost Price </label>
                                                    <label class="input"><i class="icon-append fa fa-money"></i>
                                                        <input type="text" data-placeholder="10.00"  name="buy_price" id="buy_price" maxlength="15" class="float_value" value="<?php echo $product->buying_price; ?>">
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Sub Category </label>
                                                    <label class="select">
                                                        <select name="sub_cat" class="sub_cat select2" id="sub_cat">  
                                                            <option value="">Select Sub Category</option>
                                                        </select>
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Quantity
                                                        <span style="position: absolute;right: 15px; top: 0; display: none;" class="avail_qty_cls"><b style="color: #3276B1;">Available Quantity : <span class="avail_qty">0</span></b></span></label>
                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                        <input type="text" data-placeholder=" 123"  name="new_quantity" id="new_quantity" class="new_quantity" maxlength="15">
                                                        <input type="hidden" name="old_itm_quantitytxt" id="old_itm_quantitytxt" class="old_itm_quantitytxt">
                                                    </label>
                                                </section>
                                            </div>

                                            <div class="row">
                                                <!--<section class="col col-6">
                                                    <label class="label">Item Code</label>
                                                    <label class="input"><i class="icon-append fa fa-key"></i>
                                                        <input type="text" data-placeholder=" ITM_LEN006" name="pdt_code"  id="pdt_code" value="<?php //echo $so_no;  ?>" readonly maxlength="50" />
                                                    </label>
                                                </section>-->
                                                <section class="col col-6">
                                                    <label class="label">Item Description <span style="position: absolute; top: 0px; right: 15px;">Item Code : <b class="item-code" style="color: #3276B1;"></b></span></label></label>
													
                                                    <label class="textarea textarea-expandable"><textarea rows="3" name="pdt_desc" id="pdt_desc" maxlength="500"><?php echo $product->pdt_name; ?></textarea> </label> 
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Image</label>
                                                    <label class="input">
                                                        <div class="parallel thumb preview" style="width: 9%;">
                                                            <img src="<?php echo BASE_URL . NO_IMG; ?>" />
                                                        </div>
                                                        <input type="file" class="form-control btn btn-default parallel" name="file" value="" data-default="<?php echo BASE_URL . NO_IMG; ?>" style="width: 90%; height: 45px;"/>
                                                    </label>
                                                </section>
                                            </div>

                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Warranty</label>
                                                    <label class="input"><i class="icon-append fa fa-bookmark"></i>
                                                        <input type="text" name="warranty" id="warranty" class="warranty" value="<?php echo $product->warranty; ?>">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Item Location</label>
                                                    <label class="input">
                                                        <?php foreach ($branches as $key => $branch) { ?>
                                                            <label class="col-md-4 branch_location"><?php echo str_replace(' ', '&nbsp', $branch->branch_name); ?>&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="branch_location[]" value="" class="branch_location" style="width: 50%; margin-top: 2%;" maxlength="200" />
                                                                <input type="hidden" name="branch_id[]" value="<?php echo $branch->brn_id; ?>" class="branch_id" /></label>
                                                        <?php } ?>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Status</label>
                                                    <label class="select">
                                                        <select name="status" id="status" class="select2">
                                                            <option value="1">Active</option>
                                                            <option value="2">Inactive</option>
                                                        </select>
                                                    </label>
                                                </section>
                                                <section class="col col-6">&nbsp;
                                                </section>
                                            </div>
                                        </fieldset>

                                        <footer>
                                            <button type="submit" name="add_item" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/product.js"></script>