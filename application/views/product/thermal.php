<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/plugin/JsBarcode/EAN_UPC.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/plugin/JsBarcode/CODE128.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/plugin/JsBarcode/JsBarcode.js"></script>       
    </head>
    <body>
        <div class="barcode">
            <div style="width: 155px; height: 80px; margin: 10px 7px;">
                <div class="pcode" style="display: none;"><?php echo $product->pdt_code; ?></div>
                <div style="width:90%; text-align: center;margin: auto;">
                    <div style="text-align: center; font-size: 11px; margin: 1px 0px;font-weight: bold; height: 25px; overflow: hidden;"><?php echo $product->pdt_name; ?></div>
                    <img class="barcode3" style="height: 25px; width: 90%;" />
                    <span style="font-size: 12px; margin: 3px 0px; text-align: center;"><?php echo ($product->pdt_code) ? $product->pdt_code : ''; ?> </span>
                    <span style="height: 15px; font-size: 14px; text-align: center;font-weight: bold;"><?php echo ($product->selling_price) ? '$ ' . number_format($product->selling_price, 2) : ''; ?></span>
                    <script>$(".barcode3").JsBarcode($('.pcode').html(), {width: 2, height: 25, displayValue: false, fontSize: 12});</script>
                </div>
            </div>
        </div>
    </body>
</html>
