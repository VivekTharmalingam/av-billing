<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if(!empty($success)) {?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                            <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                            <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php }else if(!empty($error)) {?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error;?>
                    </div>
                <?php }?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>discount_sales/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2><?php echo $page_title;?></h2>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="edit_form" action="<?php echo $form_action; ?>" method="post" id="ds_form" name="ds_form" enctype="multipart/form-data">

                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">                                   
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>

                                            <div class="row">
												<div class="col col-6">
													<section>
														<label class="label">Item <span style="color: red">*</span></label>
														<label class="select">
															<select name="item" id="item" class="select2 item">
																<option value="">Item Code / Item name</option>
																<?php foreach ($items as $key => $item) { ?>
																	<option value="<?php echo $item->id; ?>" <?php echo ($item->id == $ds->offer_item_id) ? 'selected' : ''; ?> data-item-price="<?php echo $item->selling_price; ?>"><?php echo $item->pdt_code.' / '.$item->pdt_name; ?></option>
																<?php } ?>
															</select>
														</label>
													</section>
													<section>
														<label class="label">Offered Percentage <span style="color: red"></span></label>
														<label class="input">
															<i class="icon-append fa fa-percent"></i>
															<input type="text" name="offer_percentage" id="offer_percentage" value="<?php echo $ds->offer_percentage; ?>" class="form-control offer_percentage" tabindex="-1" />
														</label>
													</section>
													<section>
														<label class="label">From Date</label>
														<label class="input">
															<i class="icon-append fa fa-calendar-check-o"></i>
															<input type="text" name="from_date" id="from_date" class="form-control bootstrap-datepicker-comncls" tabindex="-1" value="<?php echo ($ds->from_date != '') ? date('d/m/Y', strtotime($ds->from_date)) : ''; ?>" />
														</label>
													</section>
													<section>
														<label class="label">To Date</label>
														<label class="input">
															<i class="icon-append fa fa-calendar-check-o"></i>
															<input type="text" name="to_date" id="to_date" class="form-control bootstrap-datepicker-comncls" tabindex="-1" value="<?php echo ($ds->to_date != '') ? date('d/m/Y', strtotime($ds->to_date)) : ''; ?>" />
														</label>
													</section>
												</div>
												<div class="col col-6">
													<section>
														<label class="label">Selling Price &nbsp;:&nbsp; <span class="currency">S$</span>&nbsp;<span style="color: #75001b;" class="s_price"><?php echo $ds->selling_price; ?></span></label>
														<label class="label">Offered Price &nbsp;:&nbsp; <span class="currency">S$</span>&nbsp;<span style="color: #75001b;" class="o_price"><?php echo $ds->offer_price; ?></span></label>
														
														<input type="hidden" name="selling_price" id="selling_price" class="form-control selling_price" value="<?php echo $ds->selling_price; ?>" readonly="" tabindex="-1" />
														<input type="hidden" name="offer_price" id="offer_price" value="<?php echo $ds->offer_price; ?>" class="form-control offer_price" tabindex="-1" />
													</section>
													<section>
														<label class="label">Remarks </label>
														<label class="textarea">
															<textarea rows="3" name="remarks" id="remarks" maxlength="500"><?php echo $ds->remarks;?></textarea>
														</label>
													</section>
													<section>
														<label class="label">Status</label>
														<label class="select">
															<select name="status" class="select2" id="status">
																<option value="1" <?php echo ($ds->status_str == 'Active') ? 'selected' : ''; ?>>Active</option>
																<option value="2"<?php echo ($ds->status_str == 'Inactive') ? 'selected' : ''; ?>>Inactive</option>
															</select>
														</label>
													</section>
												</div>
                                            </div>
                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 
                            
                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/app/discount_sales.js"></script>