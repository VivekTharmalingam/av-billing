<?php
$ti = time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Warranty_Report_" . $ti . ".xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<?php $colspan = 7;?>
<table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; margin-left:10px" cellpadding="5">
    <tr style="font-family:Times new roman;">
        <th width="5%">S.No</th>
        <th width="15%">Return Date</th>
        <th width="17%">Inv No.</th>
        <th width="12%">Item</th>
        <th width="18%">Description</th>
        <th width="10%">Returned Quantity</th>
        <th width="10%">Warranty</th>
    </tr>
    <?php if (!empty($warranty_rept)) {
            $sno = 0;
          foreach ($warranty_rept as $key => $row) { ?>
            <tr>
                <td><?php echo ++ $sno; ?></td>
                <td><?php echo $row->ret_date; ?></td>
                <td><?php echo $row->inv; ?></td>
                <td><?php echo text($row->pdt_name); ?></td>
                <td><?php echo text($row->pdt_desc); ?></td>
                <td><?php echo $row->return_qty; ?></td>
                <td><?php echo $row->warranty_cleared_str; ?></td>
            </tr>
  <?php } } else { ?>
          <tr>
              <td colspan="7" align="center">No Record Found</td>
          </tr>
  <?php } ?>      
</table>