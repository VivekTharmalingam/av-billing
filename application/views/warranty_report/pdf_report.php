<table border="0" cellpadding="3" cellspacing="0" style="font-family: calibri; font-size:14px; width: 1000px;">
    <?php echo $head; ?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>

<table width="100%" border="1" style="font-family: calibri; border-collapse:collapse;" cellpadding="3">
	<thead>
		<tr style="font-size:16px;">
			<th width="5%">S.No</th>
                        <th width="15%">Return Date</th>
                        <th width="17%">Inv No.</th>
                        <th width="12%">Item</th>
                        <th width="18%">Description</th>
                        <th width="10%">Returned Quantity</th>
                        <th width="10%">Warranty</th>
		</tr>
	</thead>
        <tbody>
        <?php if (!empty($warranty_rept)) {
            $sno = 0;
          foreach ($warranty_rept as $key => $row) { ?>
            <tr>
                <td><?php echo ++ $sno; ?></td>
                <td><?php echo $row->ret_date; ?></td>
                <td><?php echo $row->inv; ?></td>
                <td><?php echo text($row->pdt_name); ?></td>
                <td><?php echo text($row->pdt_desc); ?></td>
                <td><?php echo $row->return_qty; ?></td>
                <td><?php echo $row->warranty_cleared_str; ?></td>
            </tr>
        <?php } } else { ?>
                <tr>
                    <td colspan="7" align="center">No Record Found</td>
                </tr>
        <?php } ?>    
      </tbody>        
</table>