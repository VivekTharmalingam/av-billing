<?php
$ti = time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Exchange_Order_Report_" . $ti . ".xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<?php $colspan = 7;?>
<table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; margin-left:10px" cellpadding="5">
    <tr style="font-family:Times new roman;">
        <th width="5%">S.No</th>
        <th width="17%">Ex-Order No.</th>
        <th width="15%">Ex-Order Date</th>
        <th width="20%">Supplier</th>
        <th width="15%">Total Amount</th>
        <th width="15%">Payment Status</th>
    </tr>
    <?php if (!empty($exchange_order_rept)) {
        $sno = 0;
      foreach ($exchange_order_rept as $key => $row) { ?>
        <tr>
            <td><?php echo ++ $sno; ?></td>
            <td><?php echo $row->exr_code; ?></td>
            <td><?php echo $row->exr_date_str; ?></td>
            <td><?php echo text($row->name); ?></td>
            <td align="right"><?php echo number_format($row->exr_net_amount, 2); ?></td>
            <td><?php echo $row->payment_status; ?></td>
        </tr>
  <?php } } else { ?>
          <tr>
              <td colspan="6" align="center">No Record Found</td>
          </tr>
  <?php } ?>      
</table>