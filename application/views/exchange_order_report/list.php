<script>window.localStorage.clear();</script>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">&nbsp;</div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="widget-body">
                        <!-- Widget ID (each widget will need unique ID)-->                                
                        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false"	>
                            <header>
                                <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                <h2>Exchange Order Reports</h2>
                            </header>

                            <div>
                                <!--<div class="date-range"></div>-->
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="box-body table-responsive">
                                        <form name="order_search" action="<?php echo $form_action; ?>" method="post" class="search_form validate_form">
                                            <div class="col-xs-12">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="search_date" data-placeholder="dd/mm/yyyy - dd/mm/yyyy" id="date_alloc" style="padding: 6px 12px;margin-right:5%; width: 100%;" name="search_date" value="<?php echo (!empty($search_date)) ? $search_date : ''; ?>" autocomplete="off" >
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-user"></i>
                                                            </div>
                                                            <select name="supplier_id" class="select2 supplier_id">
                                                                <option value=""> Select Supplier</option>
                                                                <?php foreach ($all_suppliers as $key => $supplier) { ?>  
                                                                    <option value="<?php echo $supplier->id; ?>" <?php echo ($supplier->id == $supplier_id) ? 'selected' : ''; ?>><?php echo $supplier->name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <input type="submit" name="submit" value="Reset" id="reset" class="btn btn-success btn-warning" />&nbsp;&nbsp;<input type="submit" name="submit" value="Search" class="btn btn-success btn-primary"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <input type="submit" name="submit" value="excel" class="submit_excel"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <input type="submit" name="submit" value="pdf" class="submit_pdf" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover" id="dt_basic" width="100%">
                                        <thead>			                
                                            <tr>
                                                <th width="5%" data-class="expand">S.No</th>
                                                <th width="17%" data-hide="phone"><i class="fa fa-fw fa-calendar text-muted hidden-md hidden-sm hidden-xs"></i> Ex-Order No.</th>
                                                <th width="15%" ><i class="fa fa-fw fa-calendar text-muted hidden-md hidden-sm hidden-xs"></i> Ex-Order Date</th>
                                                <th width="20%" data-hide="phone,tablet"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Supplier</th>
                                                <th width="15%" data-hide="phone,tablet"><i class="fa fa-fw fa-money text-muted hidden-md hidden-sm hidden-xs"></i> Total Amount</th>
                                                <th width="15%" data-hide="phone,tablet"><i class="fa fa-fw fa-warning text-muted hidden-md hidden-sm hidden-xs"></i> Payment Status</th>
                                                <th width="12%" data-hide="phone"><i class="fa fa-fw fa-cog text-muted hidden-md hidden-sm hidden-xs"></i> Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php if (!empty($exchange_order_rept)) {
                                                $sno = 0;
                                              foreach ($exchange_order_rept as $key => $row) { ?>
                                                <tr>
                                                    <td><?php echo ++ $sno; ?></td>
                                                    <td><?php echo $row->exr_code; ?></td>
                                                    <td><?php echo $row->exr_date_str; ?></td>
                                                    <td><?php echo text($row->name); ?></td>
                                                    <td align="right"><?php echo number_format($row->exr_net_amount, 2); ?></td>
                                                    <td><?php echo $row->payment_status; ?></td>
                                                    <td align="center">
                                                        <?php if (in_array(1, $permission)) { ?>
                                                                <a class="label btn btn-warning view" href="<?php echo $view_link . $row->id ?>"><span>View</span></a>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                        <?php } } ?>        
                                        </tbody>
                                    </table>
                                </div>
                                <!-- end widget content -->
                            </div>
                        </div> 
                    </div>
                </article>
                <!-- /.row -->
            </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#reset').click(function () {
            //location.reload();
        });
        /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            'iDisplayLength': 25,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            }
        });

        /* END BASIC */
        $('#date_alloc').daterangepicker({
            autoUpdateInput: false,
            opens: 'center',
            locale: {
                format: 'MM/DD/YYYY h:mm A',
                cancelLabel: 'Cancel'
            },
            ranges: {
                'ALL': [moment().subtract(2, 'days'), moment()],
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            if (label.toLowerCase() == 'all') {
                $('#date_alloc').val('');
                return false;
            }
            // getResults();
        });

        if ($('#date_alloc').length) {
            if ($('#date_alloc').val() == '') {
                $('.daterangepicker .ranges li').each(function (key, element) {
                    if ($(element).html().toLowerCase() == 'all') {
                        $(element).addClass('active');
                    }
                    else {
                        $(element).removeClass('active');
                    }
                });
            }
        }

        $(function () {
            $('#search_date').daterangepicker({format: 'DD/MM/YYYY'});
        });
    });
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/customer.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/custom.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datepicker/daterangepicker.js"></script>