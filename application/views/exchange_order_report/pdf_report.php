<table border="0" cellpadding="3" cellspacing="0" style="font-family: calibri; font-size:14px; width: 1000px;">
    <?php echo $head; ?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>

<table width="100%" border="1" style="font-family: calibri; border-collapse:collapse;" cellpadding="3">
	<thead>
		<tr style="font-size:16px;">
			<th width="8%" > S.No</th>
			<th width="17%">Ex-Order No.</th>
                        <th width="15%">Ex-Order Date</th>
                        <th width="25%">Supplier</th>
                        <th width="17%">Total Amount</th>
                        <th width="18%">Payment Status</th>
		</tr>
	</thead>
        <tbody>
        <?php if (!empty($exchange_order_rept)) {
              $sno = 0;
            foreach ($exchange_order_rept as $key => $row) { ?>
              <tr>
                  <td><?php echo ++ $sno; ?></td>
                  <td><?php echo $row->exr_code; ?></td>
                  <td><?php echo $row->exr_date_str; ?></td>
                  <td><?php echo text($row->name); ?></td>
                  <td align="right"><?php echo number_format($row->exr_net_amount, 2); ?></td>
                  <td><?php echo $row->payment_status; ?></td>
              </tr>
        <?php } } else { ?>
                <tr>
                    <td colspan="6" align="center">No Record Found</td>
                </tr>
        <?php } ?>   
      </tbody>        
</table>