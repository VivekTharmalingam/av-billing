<?php
$ti = time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Inventory_Details_report" . $ti . ".xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; margin-left:10px">
	<tr style="font-family:Times new roman; font-weight:bold;">
		<th width="6%" style="padding:10px">S.NO</th>
		<th width="17%">Branch Name</th>
		<th width="25%">Item Code</th>
		<th width="17%">Brand Name</th>                    
		<th width="25%">Item Description</th>
		<th width="9%">Quantity</th>
		<th width="13%">Price</th>
		<th width="13%">Total Price</th>
	</tr>
<?php if (count($inventory)) { ?>
		<?php foreach ($inventory as $key => $row) { ?>                   
			<tr>
				<td style="text-align:center"><?php echo ++$key; ?></td>
				<td><?php echo $row->branch_name; ?></td>
				<td><?php echo nl2br($row->pdt_code); ?></td>
				<td><?php echo $row->cat_name; ?></td>                            
				<td><?php echo nl2br($row->pdt_name); ?></td>
				<td><?php echo $row->available_qty; ?></td>
				<td align="right"><?php echo number_format($row->selling_price, 2); ?></td>
					<?php
					if ($row->available_qty > 0) {
					$price = $row->available_qty * $row->selling_price;
					} else {
					$price = 0;
					}
					?>
				<td align="right"><?php echo number_format($price, 2); ?></td>
			</tr>
		<?php } ?>
	<?php } else { ?>
		<tr>
			<td colspan="8" align="center">No Record Found</td>
		</tr>
	<?php } ?>
</table>
