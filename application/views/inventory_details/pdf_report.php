<table border="0" cellpadding="5" cellspacing="0" style="font-family:calibri; font-size:14px; width: 1000px;">
    <?php echo $head; ?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>        
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>
<table width="100%" border="1" style="border-collapse:collapse; margin-left:10px; font-family: calibri;" cellpadding="5">
<thead>
	<tr style="font-size:16px;">
		<th width="6%">S.NO</th>
		<th width="16%">Item Code / Brand</th>                   
		<th width="25%">Item Description</th>
		<th width="9%">Quantity</th>
		<th width="10%">Cost</th>
		<th width="12%">Total Cost</th>
		<th width="10%">Price</th>
		<th width="12%">Total Price</th>
	</tr>
	</thead>
	<?php if (count($inventory)) { ?>
	<tbody>
		<?php $total_cost = 0;?>
		<?php $total_price = 0;?>
		<?php foreach ($inventory as $key => $row) {
			if ($row->available_qty > 0) {
				$price = $row->available_qty * $row->selling_price;
				$total_price += $price;
				
				$cost = $row->available_qty * $row->buying_price;
				$total_cost += $cost;
			}
			else {
				$price = 0;
				$cost = 0;
			}
			?>
			<tr>
				<td style="text-align:center;" ><?php echo ++$key; ?></td>
				<td><?php echo strtoupper($row->pdt_code).strtoupper($row->cat_name); ?></td>
				<td><?php echo strtoupper($row->pdt_name); ?></td>
				<td style="text-align:center;"><?php echo $row->available_qty; ?></td>
				<td align="right"><?php echo $row->buying_price; ?></td>
				<td align="right"><?php echo $cost; ?></td>
				<td align="right">
					<?php echo number_format($row->selling_price * 1, 2, '.', ','); ?>
				</td>
				<td align="right"><?php echo number_format($price, 2); ?></td>
			</tr>
<?php } ?>
</tbody>
<tfoot>
	<tr>
		<td colspan="5" align="right">Total</td>
		<td align="right"><?php echo number_format($total_cost, 2); ?></td>
		<td align="right">--</td>
		<td align="right"><?php echo number_format($total_price, 2); ?></td>
	</tr>
</tfoot>
<?php } else { ?>
<tbody>
		<tr>
			<td colspan="8" align="center">No Record Found</td>
		</tr>
		</tbody>
<?php } ?>

</table>