<script>
    window.localStorage.clear();
</script>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">&nbsp;</div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="widget-body">
                        <!-- Widget ID (each widget will need unique ID)-->                                
                        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false"	>

                            <header><span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                <h2>Inventory Details Reports List</h2>
                            </header>

                            <div>
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="box-body table-responsive">
										<div class="date-range"></div>
                                        <form name="order_search" action="<?php echo $form_action; ?>" method="post" class="search_form validate_form">
                                            <div class="col-xs-12">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-random"></i>
                                                            </div>
                                                            <select  name="branch_id" class="branch_id select2">  
                                                                <option value="">ALL BRANCHES</option>
                                                                <?php foreach ($branch as $key => $val) { ?>
                                                                    <option value="<?php echo $val->id; ?>" <?php echo ($val->id == $branch_id) ? 'selected' : '' ?> ><?php echo $val->branch_name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
												<div class="col-md-2" style="margin: 15px 0px;">
													<select  name="brand_id" class="brand_id select2">  
														<option value="">ALL BRANDS</option>
														<?php foreach($brand as $key => $val){ ?>
															<option value="<?php echo $val->id;?>" <?php echo ($val->id == $brand_id) ? 'selected' : '' ?>><?php echo $val->cat_name;?></option>
														<?php } ?>
													</select>
												</div>
												<div class="col-md-2" style="margin: 15px 0px;">
													<select name="category_id" class="select2" id="category_id" data-sub_category_id="<?php echo $sub_category_id;?>">  
														<option value="">All Category</option>
														<?php foreach ($item_category as $key => $val) { ?>
															<option value="<?php echo $val->id; ?>" <?php echo ($val->id == $category_id) ? 'selected' : ''; ?>><?php echo $val->item_category_name; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="col-md-2" style="margin: 15px 0px;">
													<select name="sub_category_id" class="select2" id="sub_category_id">  
														<option value="">Select Sub Category</option>
													</select>
												</div>
												<div class="col-md-2" style="margin: 15px 0px;">
													<input type="submit" name="reset_btn" value="Reset" id="reset" class="btn btn-success btn-warning" />&nbsp;&nbsp;<input type="submit" name="submit" value="Search" class="btn btn-success btn-primary" />
												</div>														
												<div class="col-md-1" style="margin: 15px 0px;">
													<input type="submit" name="submit" value="pdf" class="submit_pdf" style="margin-left: 10px;" />
													<input type="submit" name="submit" value="excel" class="submit_excel" />
												</div>
												
                                            </div>
                                        </form>
                                    </div>

                                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>			                
                                            <tr>
                                                <th width="5%" data-class="expand">S.No</th>
                                                <th width="18%" data-hide="phone"><i class="fa fa-fw fa-archive text-muted hidden-md hidden-sm hidden-xs"></i> Item Code / Brand</th>
                                                <th width="25%" data-hide="phone"><i class="fa fa-fw fa-archive text-muted hidden-md hidden-sm hidden-xs"></i> Item Description</th>
                                                <th width="10%" data-hide="phone,tablet"><i class="fa fa-fw fa-stack-overflow text-muted hidden-md hidden-sm hidden-xs"></i>Quantity</th>
												<th width="9%" data-hide="phone,tablet"><i class="fa fa-fw fa-money text-muted hidden-md hidden-sm hidden-xs"></i>Cost</th>
												<th width="12%" data-hide="phone,tablet"><i class="fa fa-fw fa-money text-muted hidden-md hidden-sm hidden-xs"></i>Total Cost</th>
                                                <th width="9%" data-hide="phone,tablet"><i class="fa fa-fw fa-money text-muted hidden-md hidden-sm hidden-xs"></i>Price</th>
                                                <th width="12%" data-hide="phone,tablet"><i class="fa fa-fw fa-money text-muted hidden-md hidden-sm hidden-xs"></i>Total Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
											<?php $total_cost = 0;?>
											<?php $total_price = 0;?>
                                            <?php foreach ($inventory as $key => $row) {
                                                    if ($row->available_qty > 0) {
														$price = $row->available_qty * $row->selling_price;
														$total_price += $price;
														
														$cost = $row->available_qty * $row->buying_price;
														$total_cost += $cost;
													}
													else {
														$price = 0;
														$cost = 0;
													}
                                                    ?>
                                                <tr>
                                                    <td><?php echo ++$key; ?></td>
                                                    <td><?php echo $row->pdt_code . ' / ' . $row->cat_name; ?></td>
                                                    <td><?php echo $row->pdt_name; ?></td>
                                                    <td align="center"><?php echo $row->available_qty; ?></td>	 
                                                    <td align="right"><?php echo $row->buying_price; ?></td>
                                                    <td align="right"><?php echo $cost; ?></td>
                                                    <td align="right">
														<?php echo number_format($row->selling_price * 1, 2, '.', ','); ?>
													</td>
                                                    <td align="right"><?php echo number_format($price, 2); ?></td>
                                                </tr>
											<?php } ?>
                                        </tbody>
										<tfoot>
											<tr>
												<td colspan="5" align="right">Total</td>
												<td align="right"><?php echo number_format($total_cost, 2); ?></td>
												<td align="right">--</td>
												<td align="right"><?php echo number_format($total_price, 2); ?></td>
											</tr>
										</tfoot>
                                    </table>
                                </div>
                                <!-- end widget content -->
                            </div>
                        </div>
                    </div>
                </article>
                <!-- /.row -->
            </div>
        </section>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/report.js"></script>
<script>
    $(document).ready(function () {
        /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            'iDisplayLength': 25,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            }
        });

        /* END BASIC */
        $('#date_alloc').daterangepicker({
            autoUpdateInput: false,
            opens: 'center',
            locale: {
                format: 'MM/DD/YYYY h:mm A',
                cancelLabel: 'Cancel'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            alert(start + '|' + end + '|' + label);
            // getResults();
        });

        $(function () {
            $('#search_date').daterangepicker({format: 'DD/MM/YYYY'});
        });
    });
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/customer.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/custom.css">
<script src="<?php echo BASE_URL; ?>assets/js/moment.min2.js"></script>
<script src="<?php echo BASE_URL; ?>assets/js/datepicker/daterangepicker.js"></script>