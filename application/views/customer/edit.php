<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>customer/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="edit_form" action="<?php echo $form_action; ?>" method="post" id="customer_form" name="customer_form" enctype="multipart/form-data">
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">

                                        <fieldset>                                      
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>

                                            <input type="hidden" class="form-control customer_id" name="customer_id" value="<?php echo $customer->id; ?>" />

                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Customer Name <span style="color: red">*</span></label>
                                                    <label class="input"> <i class="icon-append fa fa-user-md"></i>
                                                        <input type="text" data-placeholder=" Alastair" name="customer_name" id="customer_name" maxlength="150" tabindex="1" value="<?php echo $customer->name; ?>" />
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Customer Group </label>
                                                    <label class="select">
                                                        <select name="cust_group" tabindex="2" class="select2">
                                                            <option value="">Select Customer Group</option>
                                                            <?php foreach ($customer_group as $key => $value) { ?>
                                                                <option value="<?php echo $value->id; ?>" <?php echo ($customer->group_id == $value->id) ? 'selected' : ''; ?>><?php echo $value->customer_group; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </label>
                                                </section>
                                                
                                            </div>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Contact Name </label>
                                                    <label class="input"><i class="icon-append fa fa-user-md"></i>
                                                        <input type="text" data-placeholder="Catarina" name="name" id="name" maxlength="50" tabindex="3" value="<?php echo $customer->contact_name; ?>" />
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Address <span style="color: red">*</span></label>
                                                    <label class="textarea">
                                                        <textarea rows="3" name="address" id="address" tabindex="4" maxlength="500"><?php echo $customer->address; ?></textarea>
                                                    </label>
                                                    <!--<div class="note">
            <strong>Note:</strong> expands on focus.
       </div>-->
                                                </section>

                                                
                                            </div>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Mobile No </label>
                                                    <label class="input"><i class="icon-append fa fa-phone"></i>
                                                        <input type="text" data-placeholder=" 65324323"  name="phone_no" id="phone_no" maxlength="15" tabindex="5" value="<?php echo $customer->phone_no; ?>" />
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Telephone </label>
                                                    <label class="input"><i class="icon-append fa fa-phone"></i>
                                                        <input type="text" data-placeholder="65324323" name="telephone" id="telephone" maxlength="15"  tabindex="6" value="<?php echo $customer->telephone; ?>" />
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                
                                                <section class="col col-6">
                                                    <label class="label">Email </label>
                                                    <label class="input"><i class="icon-append fa fa-envelope-o"></i>
                                                        <input type="text" class="lowercase" name="email" id="email" maxlength="70" tabindex="7" value="<?php echo $customer->email; ?>" />
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Payment Term </label>
                                                    <label class="select">
                                                        <select name="payment_term" tabindex="8" class="select2">
                                                            <option value="">Select Payment Term</option>
                                                            <?php foreach ($payment_types as $key => $type) { ?>
                                                                <option value="<?php echo $type->id; ?>" <?php echo ($customer->payment_term == $type->id) ? 'selected' : ''; ?>><?php echo $type->type_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </label>
                                                </section>
                                                
                                            </div>

                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Fax</label>
                                                    <label class="input"><i class="icon-append fa fa-fax"></i>
                                                        <input type="text" data-placeholder=" 65236432" name="fax" id="fax" maxlength="15" tabindex="9" value="<?php echo $customer->fax; ?>" />
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Website </label>
                                                    <label class="input"><i class="icon-append fa fa-globe"></i>
                                                        <input type="text" data-placeholder="http://www.example.com"  name="website" id="website" maxlength="150" tabindex="10" value="<?php echo $customer->website; ?>" />
                                                    </label>
                                                </section>
                                            </div>
                                        </fieldset>

                                        <fieldset>
                                            <legend style="font-weight: bold; padding-bottom: 10px; font-size: 15px; color: #A90329; border-bottom: 1px dashed rgba(0,0,0,.2);">Shipping Address</legend>
                                            <div class="row material-container product_details" style="border-radius:5px; border: 1px outset #eee; padding: 10px; margin: 0px 10px 20px 10px; box-shadow: 3px 3px 3px #eee; background: #fcfcfc; ">
                                                <?php $length = count($ship_address); ?>
                                                <?php foreach ($ship_address as $key => $addr) { ?>
                                                    <input type="hidden" name="edit_addr_id[<?php echo $key; ?>]" class="edit_addr_id" value="<?php echo $addr->id; ?>" />
                                                    <div class="shipping_address clr" >	
                                                        <input type="hidden" name="addr_id[<?php echo $key; ?>]" class="addr_id" value="<?php echo $addr->id; ?>" />

                                                        <section class="col col-4">
                                                            <label class="label">Location Name <span style="color: red">*</span></label>
                                                            <label class="input"> <i class="icon-append fa fa-map-marker"></i>
                                                                <input type="text" data-placeholder=" XYZ"  name="location_name[<?php echo $key; ?>]" class="location_name" maxlength="150" value="<?php echo $addr->location_name; ?>">
                                                            </label>                                                           
                                                        </section>

                                                        <section class="col col-5">
                                                            <label class="label">Address <span style="color: red">*</span></label>
                                                            <label class="textarea textarea-expandable"><textarea rows="3" name="ship_address[<?php echo $key; ?>]" class="ship_address" maxlength="500"><?php echo $addr->address; ?></textarea> </label> <div class="note">
                                                                <strong>Note:</strong> expands on focus.
                                                            </div>
                                                        </section>

                                                        <section class="col col-1" style="width: 15%;">
                                                            <label class="label"><b>&nbsp;</b></label>
                                                            <label class="radio"></i>
                                                                <input type="radio" name="is_default[<?php echo $key; ?>]" class="is_default"  value="1" <?php echo (!empty($addr->is_default)) ? 'checked' : ''; ?> onclick="unchecked_fields(this);"/>
                                                                <i></i>Is Default
                                                            </label>
                                                        </section>

                                                        <section class="col col-1">
                                                            <label class="label">&nbsp;</label>
                                                            <?php if ($length == ($key + 1)) { ?>
                                                                <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                            <?php } ?>
                                                            <?php if ($length > 1) { ?>
                                                                <i class="fa fa-2x fa-minus-circle remove-row"></i>
                                                            <?php } ?>
                                                        </section>

                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </fieldset>

                                        <fieldset>										
                                            <div class="row" >											
                                                <section class="col col-6">
                                                    <label class="label">Remarks </label>
                                                    <label class="textarea textarea-expandable"><textarea rows="3" name="remarks" id="remarks" maxlength="500"><?php echo $customer->remarks; ?></textarea> </label> <div class="note">
                                                        <strong>Note:</strong> expands on focus.
                                                    </div>
                                                </section>

                                                <section class="col col-6">
                                                    <label class="label">Status </label>
                                                    <label class="select">
                                                        <select name="status" class="select2" id="status">
                                                            <option value="1" <?php echo ($customer->status_str == 'Active') ? 'selected' : ''; ?>>Active</option>
                                                            <option value="2"<?php echo ($customer->status_str == 'Inactive') ? 'selected' : ''; ?>>Inactive</option>
                                                        </select>  </label>
                                                </section> 

                                            </div>                                        

                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/customer.js"></script>