<?php //print_r($from_date);exit;?>
<table border="0" cellpadding="3" cellspacing="0" style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:14px; width: 1000px;">
    <?php echo $head;?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>
<table width="100%" border="1" style="border-collapse:collapse; margin-left:10px">
	<tr style="font-family:Times new roman;font-size:16px;font-weight:bold;">
		<th width="5%" style="padding:10px">S.NO</th>
		<th width="15%">Paid Date</th>
		<th width="30%">Employee Name</th>
		<th width="15%">Year</th>
		<th width="15%">Month</th>
		<th width="20%">Total Amount</th>
	</tr>
	<?php if (count($payslip)) {?>
	<?php foreach($payslip as $key => $row) {?>                    
		<tr>
			<td><?php echo date('d/m/Y', strtotime($row->pay_date));?></td>
			<td style="padding:8px;"><?php foreach($employee as $key => $val){ ?>
						<?php if($val->id == $row->employee_id){ echo $val->employee_name; } else { echo '';} ?>
					 <?php } ?></td>
			<td style="padding:8px;"><?php echo text($row->year);?></td>
			<td style="padding:8px;"><?php foreach($month as $key => $val){ ?>
					<?php if($key == $row->month){ echo $val; } else { echo '';} ?>
				<?php } ?></td>
			<td style="padding:8px; text-align: right;"><?php echo number_format($row->net_amount, 2);?></td>
		</tr>
	<?php }?>
	<?php } else {?>
		<tr>
			<td colspan="6" align="center">No Record Found</td>
		</tr>
	<?php }?>
</table>