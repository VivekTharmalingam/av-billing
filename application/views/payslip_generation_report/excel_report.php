<?php
$ti=time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Payslip_Generation_report".$ti.".xls");
header("Pragma: no-cache");
header("Expires: 0");
#echo "<pre>";print_r($item);echo "</pre>";
#echo "</br> : ".count($item);


?>
<table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; margin-left:10px">
	<tr style="font-family:Times new roman; font-weight:bold;">
		<th width="15%">Paid Date</th>
		<th width="30%">Employee Name</th>
		<th width="15%">Year</th>
		<th width="20%">Month</th>
		<th width="20%">Total Amount</th>
	</tr>
	<?php if (count($payslip)) {?>
	<?php foreach($payslip as $key => $row) {?>                    
		<tr>
			<td><?php echo date('d/m/Y', strtotime($row->pay_date));?></td>
			<td style="padding:8px;"><?php foreach($employee as $key => $val){ ?>
						<?php if($val->id == $row->employee_id){ echo $val->employee_name; } else { echo '';} ?>
					 <?php } ?></td>
			<td style="padding:8px;"><?php echo text($row->year);?></td>
			<td style="padding:8px;"><?php foreach($month as $key => $val){ ?>
					<?php if($key == $row->month){ echo $val; } else { echo '';} ?>
				<?php } ?></td>
			<td style="padding:8px; text-align: right;"><?php echo number_format($row->net_amount, 2);?></td>
		</tr>
	<?php }?>
	<?php } else {?>
		<tr>
			<td colspan="6" align="center">No Record Found</td>
		</tr>
	<?php }?>
</table>