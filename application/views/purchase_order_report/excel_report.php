<?php
$ti = time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Purchase_Order_report" . $ti . ".xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="0" cellpadding="3" cellspacing="0" style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:14px; width: 1000px;">
    <tr>
        <td colspan="2" width="100%">
            <table width="100%" border="1" style="border-collapse:collapse; margin-left:10px">
                <tr style="font-family:Times new roman;font-size:16px;font-weight:bold;">
                    <th width="5%" style="padding:10px">PO No</th>
                    <th width="20%">Inventory Status</th>
                    <th width="20%">Payment Status</th>
                    <th width="15%">Supplier</th>
                    <th width="10%">PO Date</th>
                    <th width="10%">Total Amount(S$)</th>
                    <th width="10%">Paid Amount (S$)</th>
                    <th width="10%">Balance Amount (S$)</th>
                </tr>
                <?php if (count($purchase)) { ?>
                    <?php foreach ($purchase as $key => $row) { ?>                    
                        <tr>
                            <td style="padding:8px;"><?php echo text($row->po_no); ?></td>
                            <td style="padding:8px;"><?php echo text($row->inv_sts); ?></td>
                            <td style="padding:8px;"><?php echo text($row->pay_sts); ?></td>
                            <td style="padding:8px;"><?php echo text($row->name); ?></td>
                            <td style="padding:8px;"><?php echo nl2br($row->po_date_str); ?></td>
                            <td style="padding:8px;"><?php echo number_format($row->po_net_amount,2); ?></td>
                            <td style="padding:8px;"><?php echo number_format($row->paid_amt,2); ?></td>
                            <td style="padding:8px;"><?php echo number_format(($row->po_net_amount - $row->paid_amt),2); ?></td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="6" align="center">No Record Found</td>
                    </tr>
                <?php } ?>
            </table>
        </td>
    </tr>
</table>