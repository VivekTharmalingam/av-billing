<?php //print_r($from_date);exit; ?>
<table border="0" cellpadding="3" cellspacing="0" style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:14px; width: 1000px;">
    <?php echo $head; ?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>
<!--                <tr style="boder-top:0px;">
                    <td align="right" width="50%">Vendor name</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>                -->
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
    <tr>
        <td colspan="2" width="100%">
            <table width="100%" border="1" style="border-collapse:collapse; margin-left:10px">
                <tr style="font-family:Times new roman;font-size:16px;font-weight:bold;">
                    <th width="5%" style="padding:10px">PO No</th>
                    <th width="20%">Inv Status</th>
                    <th width="20%">Pay Status</th>
                    <th width="15%">Supplier</th>
                    <th width="10%">PO Date</th>
                    <th width="10%">Total (S$)</th>
                    <th width="10%">Paid (S$)</th>
                    <th width="10%">Balance (S$)</th>
                </tr>
                <?php if (count($purchase)) { ?>
                    <?php foreach ($purchase as $key => $row) { ?>                    
                        <tr>
                            <td style="padding:8px;"><?php echo text($row->po_no); ?></td>
                            <td style="padding:8px;"><?php echo text($row->inv_sts); ?></td>
                            <td style="padding:8px;"><?php echo text($row->pay_sts); ?></td>
                            <td style="padding:8px;"><?php echo text($row->name); ?></td>
                            <td style="padding:8px;"><?php echo nl2br($row->po_date_str); ?></td>
                            <td style="padding:8px;"><?php echo number_format($row->po_net_amount,2); ?></td>
                            <td style="padding:8px;"><?php echo number_format($row->paid_amt,2); ?></td>
                            <td style="padding:8px;"><?php echo number_format(($row->po_net_amount - $row->paid_amt),2); ?></td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="6" align="center">No Record Found</td>
                    </tr>
                <?php } ?>
            </table>
        </td>
    </tr>
</table>