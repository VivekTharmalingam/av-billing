<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo base_url(); ?>enquiry_log_form/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                            </span>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="" action="<?php echo $form_action; ?>" method="post" id="enquiry_log_form" name="enquiry_log_form" enctype="multipart/form-data">

                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">                                   
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>

                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Company Name<span style="color: red">*</span></label>
                                                    <label class="select">
                                                        <select name="company_id" id="company_id" class="select2 company_id" >
                                                            <option value="">Select Company Name</option>
                                                            <?php foreach ($customer as $key => $value) { ?>
                                                                <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>  
                                                            <?php } ?>
                                                                <option value="others">Others</option>             
                                                        </select> </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Type<span style="color: red">*</span></label>
                                                    <label class="select">
                                                        <select name="type" id="type" class="select2" >
                                                            <option value="">Select Type</option>
                                                            <option value="1">Sales</option>
                                                            <option value="2">purchase</option>
                                                            <option value="3">Accounts</option>
                                                            <option value="4">Promotions</option>       
                                                        </select>  </label>
                                                </section>
                                            </div>
                                            <div class="row" id="div" style="display: none;">
                                                <section class="col col-6">
                                                    <label class="input"><i class="icon-append fa fa-building"></i>
                                                        <input type="text" data-placeholder="XYZ"  name="other_name" id="other_name" maxlength="50">
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Contact Person <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-user-md"></i>
                                                        <input type="text" data-placeholder="XYZ"  name="contact_person" id="contact_person" maxlength="50">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Assigned To<span style="color: red">*</span></label>
                                                    <label class="select"><span style="float: right;margin: 8px 10px -22px 0px;z-index: 1;" class="glyphicon glyphicon-chevron-down"></span>
                                                        <select name="user_ids[]" multiple class="select2 user_ids">
                                                            <?php
                                                            foreach ($user as $key => $value) :
                                                                ?>
                                                                <option value="<?php echo $value->id ?>"><?php echo $value->name; ?></option>
                                                                <?php
                                                            endforeach;
                                                            ?>

                                                        </select>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row"> 
                                                <section class="col col-6">
                                                    <label class="label">Contact No <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-phone"></i>
                                                        <input type="text" data-placeholder="9898989898"  name="contact_no" id="contact_no" maxlength="15">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Enquiry By <span style="color: red">*</span></label>
                                                    <div class="inline-group">
                                                        <label class="radio">
                                                            <input type="radio" name="enquiry_by" id="phone" value="1" checked /><i></i> Phone
                                                        </label>
                                                        <label class="radio">
                                                            <input type="radio" name="enquiry_by" id="email" value="2" /><i></i> Email
                                                        </label>
                                                    </div>
                                                </section>                                             
                                            </div>

                                            <div class="row"> 
                                                <section class="col col-6">
                                                    <label class="label">Email <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-envelope-o"></i>
                                                        <input type="text" data-placeholder="abc@info.com" class="lowercase" name="email" id="email" maxlength="70">
                                                    </label>
                                                </section> 
<!--                                                <section class="col col-6">
                                                    <label class="label">Status<span style="color: red"></span></label>
                                                    <label class="select">
                                                        <select name="status" id="status" class="select2" >
                                                            <option value="1">Active</option>
                                                            <option value="2">Inactive</option>
                                                        </select>  </label>
                                                </section>                                 -->
                                            </div>

                                            <div class="row"> 
                                                <section class="col col-6">
                                                    <label class="label">Description <span style="color: red">*</span></label>
                                                    <label class="textarea"><textarea rows="3" name="description" id="description" maxlength="500"></textarea> </label> 
                                                </section>
                                                  
                                            </div>
                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/enquiry_log_form.js"></script>
