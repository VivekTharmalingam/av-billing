<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo base_url(); ?>enquiry_log_form/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                            <h2>Add Log Description</h2>
                            </span>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="" action="<?php echo $form_action; ?>" method="post" id="log_form" name="log_form" enctype="multipart/form-data">
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    
                                    <div class="smart-form">  
                                            <fieldset>
                                                <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Log Details</legend>
                                                <div class="col-md-12">
                                                <div class="col-md-6" style="max-height: 500px;overflow-y: auto;overflow-x: hidden;">
                                                    <?php foreach ($past_log_details as $key => $row) { ?>
                                                        <div class="col-md-12" style="border-radius: 5px; background-color: #FFF; margin-bottom: 5px;">
                                                                <div style="padding-top: 10px;padding-bottom: 10px; padding-left: 10px; margin-left: 0px;" class="col-md-12"><b>Log Description : </b>&nbsp;<?php echo text($row->log_description); ?></div>
                                                            <div class="col-md-12" style="margin-left: 0px; padding-bottom: 6px; padding-left: 10px; background-color: rgba(204, 204, 204, 0.2);">
                                                                <div class="col-md-6"><b style="font-style: italic">Created On :</b>&nbsp;&nbsp;<span style="font-style: italic"><?php echo date('d-m-Y h:i:s A',strtotime($row->created_date)); ?></span></div>
<!--                                                                <div class="col-md-6"><b style="font-style: italic">Enquiry Status :</b>&nbsp;&nbsp;
                                                                    <?php if($row->enq_sts == 1) { ?>
                                                                    <span style="font-style: italic; color: #4caf50;"><?php echo text($row->enq_status_str); ?></span>
                                                                    <?php } else if($row->enq_sts == 2) { ?>
                                                                    <span style="font-style: italic; color: #ea900b;"><?php echo text($row->enq_status_str); ?></span>
                                                                    <?php } else if($row->enq_sts == 3) { ?>
                                                                    <span style="font-style: italic; color: #fb1505;"><?php echo text($row->enq_status_str); ?></span>
                                                                    <?php }?>
                                                                </div>-->
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                                </div>
                                                <div class="col-md-6">
                                                    <section class="col-md-12" style="padding-left: 6px;padding-top: 2px;">
                                                        <section class="alert alert-info" style="border-top-width: 5px!important; border-left-width:0px!important;border-bottom: 0px; background: rgb(249, 236, 205);border-color: #8b1a0d;">
                                                            <label class="label" style="font-size: 11px;"><b>Enquiry Log Details</b></label>
                                                            <label class="label" style="font-size: 11px;">Company Name: <span><?php if($enquiry->company_id != 0) { echo $enquiry_log->name; } else { echo 'Others&nbsp;('. $enquiry_log->other_name.')'; }?> </span></label>
                                                            <label class="label" style="font-size: 11px;">Contact Person: <span><?php echo $enquiry_log->contact_person;?></span></label>
                                                            <label class="label" style="font-size: 11px;">Contact No: <span><?php echo $enquiry_log->contact_no;?></span></label>
                                                            <label class="label" style="font-size: 11px;">Email Address: <span><?php echo $enquiry_log->email;?></span></label>
                                                        </section>
                                                    </section>
                                                </div>
                                                </div>
                                            </fieldset>
                                            
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>

                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Log Description <span style="color: red">*</span></label>
                                                    <label class="textarea"><textarea rows="3" name="log_description" id="log_description" maxlength="500"></textarea> </label> 
                                                </section>
                                            </div>
                                           
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Enquiry Status<span style="color: red">*</span></label>
                                                    <label class="select">
                                                        <select name="enquiry_status" id="enquiry_status" class="select2 enquiry_status" >
                                                            <option value="">Select Enquiry Status</option>
                                                            <option value="1">Success</option>
                                                            <option value="2">Process</option>
                                                            <option value="3">Cancel</option>      
                                                        </select>  </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-6" id="reason" style="display: none;">
                                                    <label class="label">Reason <span style="color: red"></span></label>
                                                    <label class="input"><i class="icon-append fa fa-pencil-square-o"></i>
                                                        <input type="text" data-placeholder="Reason"  name="reason" id="reason" maxlength="70">
                                                    </label>
                                                </section>
                                            </div>
<!--                                            <div class="row">     
                                                <section class="col col-6">
                                                    <label class="label">Status<span style="color: red"></span></label>
                                                    <label class="select">
                                                        <select name="status" id="status" class="select2" >
                                                            <option value="1">Active</option>
                                                            <option value="2">Inactive</option>
                                                        </select>  </label>
                                                </section>                                 
                                            </div>-->
                                            
                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/enquiry_log_form.js"></script>
