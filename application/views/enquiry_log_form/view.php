<?php
    
    /*
     * Author Name  : Jai K
     * Purpose      : The view page for showing the existing user.
     * Created On   : 2016-04-21 18:46
     */

?>
<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                     <a href="<?php echo base_url(); ?>enquiry_log_form/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>Enquiry Log Form - View</h2>	
<!--                             <span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Users List"><a href="<?php echo $list_link; ?>" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a></span>-->
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body"> 
                                <span id="watermark"><?php echo WATER_MARK; ?></span>
                                 <table id="user" class="table table-bordered table-striped" style="clear: both;">
                                    <tbody>
                                        <tr>
                                            <td style="width:35%;"><b>Company Name</b></td>
                                            <?php if($enquiry->company_id == 0) { ?>
                                            <td style="width:65%;"><?php echo 'Others&nbsp;('.$enquiry->other_name.')';?></td>    
                                            <?php } else { ?>
                                            <td style="width:65%;"><?php echo $enquiry->name;?></td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td><b>Contact Person</b></td>
                                            <td><?php echo $enquiry->contact_person;?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Contact No</b></td>
                                            <td><?php echo $enquiry->contact_no;?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Email Address</b></td>
                                            <td><?php echo $enquiry->email;?></td>
                                        </tr>
<!--                                        <tr>
                                            <td><b>Description</b></td>
                                            <td><?php //echo $enquiry->description;?></td>
                                        </tr>-->
                                        <tr>
                                            <td><b>Type</b></td>
                                            <td><?php if($enquiry->type == 1){
                                                    echo 'Sales';
                                            }elseif($enquiry->type == 2) {
                                                    echo 'Purchase';
                                            }elseif($enquiry->type == 3) {
                                                    echo 'Accounts';
                                            }elseif($enquiry->type == 4) {
                                                    echo 'Promotions';
                                            } ?></td>
                                        </tr>  
                                        <tr>
                                            <td><b>Enquiry By</b></td>
                                            <td><?php if($enquiry->enquiry_by == 1){
                                                    echo 'Phone';
                                            }elseif($enquiry->enquiry_by == 2) {
                                                    echo 'Email';
                                            }?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Assigned To</b></td>
                                            <td style="width:65%">
                                                <?php 
                                                    $usr_names = '';
                                                    $usr_id = explode(',', $enquiry->assigned_to );
                                                    $tot_count = count($usr_id);
                                                    $int_val = 1;
                                                    foreach( $user as $key => $value ) : 
                                                        if( in_array( $value->id, $usr_id  ) ) :
                                                            if($int_val == $tot_count) :
                                                                $usr_names .= $value->name ;
                                                            else :
                                                                $usr_names .= $value->name .',<br />';
                                                            endif;
                                                            $int_val++;
                                                        endif;
                                                    endforeach;
                                                    echo $usr_names;
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><b>Picked By</b></td>
                                            <td><?php if($enquiry->picked_status == 1){
                                                    echo 'Not Picked';
                                            }elseif($enquiry->picked_status == 2) {
                                                    echo 'Picked';
                                            }?></td>
                                        </tr>
															
					<tr>
                                            <td><b>Enquiry Status</b></td>
                                            <td><?php echo $enquiry->enq_status_str; ?></td>
                                        </tr>
                                    </tbody>
                                </table>                                
                        </div>
                            <div class="smart-form">  
                            <fieldset>
                                <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Log Details</legend>
                                <div class="col-md-12">
                                <div class="col-md-6" style="max-height: 500px;overflow-y: auto;overflow-x: hidden;">
                                    <?php foreach ($past_log_details as $key => $row) { ?>
                                        <div class="col-md-12" style="border-radius: 5px; background-color: #FFF; margin-bottom: 5px;">
                                                <div style="padding-top: 10px;padding-bottom: 10px; padding-left: 10px; margin-left: 0px;" class="col-md-12"><b>Log Description : </b>&nbsp;<?php echo text($row->log_description); ?></div>
                                            <div class="col-md-12" style="margin-left: 0px; padding-bottom: 6px; padding-left: 10px; background-color: rgba(204, 204, 204, 0.2);">
                                                <div class="col-md-6"><b style="font-style: italic">Created On :</b>&nbsp;&nbsp;<span style="font-style: italic"><?php echo date('d-m-Y h:i:s A',strtotime($row->created_date)); ?></span></div>
<!--                                                <div class="col-md-6"><b style="font-style: italic">Enquiry Status :</b>&nbsp;&nbsp;
                                                    <?php if($row->enq_sts == 1) { ?>
                                                    <span style="font-style: italic; color: #4caf50;"><?php echo text($row->enq_status_str); ?></span>
                                                    <?php } else if($row->enq_sts == 2) { ?>
                                                    <span style="font-style: italic; color: #ea900b;"><?php echo text($row->enq_status_str); ?></span>
                                                    <?php } else if($row->enq_sts == 3) { ?>
                                                    <span style="font-style: italic; color: #fb1505;"><?php echo text($row->enq_status_str); ?></span>
                                                    <?php }?>
                                                </div>-->
                                            </div>
                                        </div>
                                    <?php }?>
                                </div>
                                
                                </div>
                            </fieldset>
                            </div>
                        <!-- end widget div -->

                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
