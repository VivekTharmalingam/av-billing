<table width="100%" border="0" class="lowercase" align="center" style="font-family:Times New Roman; font-size:14px;">
         <tr><td colspan="6">&nbsp;</td></tr>
         <tr>
             <td width="20%">Tracking No.</td>
             <td width="1%">:</td>
             <td width="29%" style="height: 35px;font-size:12px;"><?php echo $transfer_stock_grp->tracking_no; ?></td>
             <td width="20%">Transfer Date</td>
             <td width="1%">:</td>
             <td width="29%" style="height: 35px;font-size:12px;"><?php echo $transfer_stock_grp->transfer_date_str; ?></td>
         </tr>
         <tr>
             <td width="20%">From Branch</td>
             <td width="1%">:</td>
             <td width="29%" style="height: 35px;font-size:12px;"><?php echo $transfer_stock_grp->frm_branch_name; ?></td>
             <td width="20%">To Branch</td>
             <td width="1%">:</td>
             <td width="29%" style="height: 35px;font-size:12px;"><?php echo $transfer_stock_grp->to_branch_name; ?></td>
         </tr>
</table>
<table width="100%" border="1" style="border-collapse:collapse; font-family: calibri;" cellpadding="5">
    <thead>
        <tr>
            <th colspan="5" style="background:#EBEBEB; font-size: 15px;">Items</th> 
        </tr>
        <tr>
            <th width="16%" style="font-size: 15px;">Item Code</th>
            <th width="45%" style="font-size: 15px;">Description</th>
            <th width="15%" style="font-size: 15px;">Quantity</th>
            <th width="24%" style="font-size: 15px;">Reason</th>
        </tr>
    </thead>
    <tfoot style="display: table-footer-group">
        <tr>
            <td colspan="4" style="border-left: none;border-right: none;border-bottom: none;font-size: 15px; text-align: center;"></td>
        </tr>
    </tfoot>
    <tbody>
        <?php $height = 410;  ?>
        <?php foreach ($transfer_stock as $key => $item) { ?>
            <tr style="vertical-align:top;">
                <td style="border-top: 0; border-bottom: none; font-size: 14px;"><?php echo strtoupper($item->pdt_code); ?></td>
                <td style="border-top: 0;border-bottom: none; font-size: 14px;"><?php echo nl2br(strtoupper($item->product_description)); ?></td>
                <td style="border-top: 0;border-bottom: none; font-size: 14px;"><?php echo $item->received_qty; ?></td>
                <td style="border-top: 0;border-bottom: none; font-size: 14px;"><?php echo $item->reason; ?></td>
            </tr>
            <?php $height -= 55; ?>
        <?php } ?>
        <tr>
            <td height="<?php echo $height; ?>" style="border-top: 0;"></td>
            <td style="border-top: 0;"></td>
            <td style="border-top: 0;"></td>
            <td style="border-top: 0;"></td>
        </tr>
    </tbody>
</table>