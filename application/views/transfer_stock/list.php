<script>
    window.localStorage.clear();
</script>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="widget-body">
                        <a href="<?php echo base_url(); ?>transfer_stock/add" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-pencil"></i></button></a>
                        <!-- Widget ID (each widget will need unique ID)-->                                
                        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false"	>

                            <header>
                                <ul class="nav nav-tabs pull-left in">
                                    <li class="active">
                                        <a data-toggle="tab" class="tabcls1" href="#tab-1"> <span class="widget-icon"> <i class="fa fa-table"></i> </span> <span class="hidden-mobile hidden-tablet"> Transfer Stock List </span> </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" class="tabcls2" href="#tab-2">
                                            <i class="fa fa-lg fa-bell"></i>
                                        <b class="badge bg-color-gray bounceIn animated"><?php echo count($transfer_stock);?></b>
                                        <span class="hidden-mobile hidden-tablet"> Awaiting for Approval </span>
                                        </a>
                                    </li>
                                </ul>
                            </header>

                            <div>
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="tab-content">
                                    <div class="tab-pane active" id="tab-1">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>			                
                                            <tr>
                                                <th width="5%" data-class="expand">S.No</th>
                                                <th width="15%"><i class="fa fa-fw fa-key text-muted hidden-md hidden-sm hidden-xs"></i>Tracking No.</th>
                                                <th width="15%"><i class="fa fa-fw fa-bank text-muted hidden-md hidden-sm hidden-xs"></i>From Branch</th>
                                                <th width="15%"><i class="fa fa-fw fa-bank text-muted hidden-md hidden-sm hidden-xs"></i>To Branch</th>
                                                <th width="10%" data-hide="phone"><i class="fa fa-fw fa-archive text-muted hidden-md hidden-sm hidden-xs"></i> Date</th>
                                                <th width="15%" data-hide="phone,tablet"><i class="fa fa-fw fa-stack-overflow text-muted hidden-md hidden-sm hidden-xs"></i>Item Count</th>														
                                                <th width="10%" data-hide="phone,tablet"><i class="fa fa-fw fa-flag-o hidden-md hidden-sm hidden-xs"></i> Status</th>
                                                <th width="15%" data-hide="phone" style="text-align: center;"><i class="fa fa-fw fa-cog  txt-color-blue hidden-md hidden-sm hidden-xs"></i> Actions</th>
                                            </tr>
                                        </thead>
                                    </table>
                                    </div>
                                    <div class="tab-pane" id="tab-2"> 
                                    <table id="dt_basic_new" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>			                
                                            <tr>
                                                <th width="5%" data-class="expand">S.No</th>
                                                <th width="15%"><i class="fa fa-fw fa-key text-muted hidden-md hidden-sm hidden-xs"></i>Tracking No.</th>
                                                <th width="15%"><i class="fa fa-fw fa-bank text-muted hidden-md hidden-sm hidden-xs"></i>From Branch</th>
                                                <th width="15%"><i class="fa fa-fw fa-bank text-muted hidden-md hidden-sm hidden-xs"></i>To Branch</th>
                                                <th width="10%" data-hide="phone"><i class="fa fa-fw fa-archive text-muted hidden-md hidden-sm hidden-xs"></i> Date</th>
                                                <th width="15%" data-hide="phone,tablet"><i class="fa fa-fw fa-stack-overflow text-muted hidden-md hidden-sm hidden-xs"></i>Item Count</th>														
                                                <th width="10%" data-hide="phone,tablet"><i class="fa fa-fw fa-flag-o hidden-md hidden-sm hidden-xs"></i> Status</th>
                                                <th width="15%" data-hide="phone" style="text-align: center;"><i class="fa fa-fw fa-cog  txt-color-blue hidden-md hidden-sm hidden-xs"></i> Actions</th>
                                            </tr>
                                        </thead>
                                    </table>
                                   </div>   
                                   </div>   
                                </div>
                                <!-- end widget content -->
                            </div>
                        </div> 
                    </div>
                </article>
                <!-- /.row -->
            </div>
        </section>
    </div>
</div>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/your_style.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/datatables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/customer.js"></script>
