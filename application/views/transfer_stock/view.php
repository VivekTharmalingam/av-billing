<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>transfer_stock/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                    <a href="<?php echo $transfer_stock_pdf; ?>" class="large_icon_des" style=" color: white;margin-right: 80px;" ><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "  title="Download"><i class="glyphicon glyphicon-save"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>Transfer Stock - View</h2>	
<!--                             <span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Users List"><a href="<?php echo $list_link; ?>" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a></span>-->
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body"> 
                                <span id="watermark"><?php echo WATER_MARK; ?></span>
                                <table id="user" class="table table-bordered table-striped" style="clear: both;">
                                    <tbody>
                                        <tr>
                                            <td><b>Tracking No.</b></td>
                                            <td><?php echo $transfer_stock_grp->tracking_no; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>From Branch</b></td>
                                            <td><?php echo $transfer_stock_grp->frm_branch_name; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>To Branch</b></td>
                                            <td><?php echo $transfer_stock_grp->to_branch_name; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Date</b></td>
                                            <td><?php echo $transfer_stock_grp->transfer_date_str; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Item Count</b></td>
                                            <td><?php echo $transfer_stock_grp->tot_product; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Total Quantity</b></td>
                                            <td><?php echo $transfer_stock_grp->tot_received_qty; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Remark</b></td>
                                            <td><?php echo $transfer_stock_grp->remark; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Status</b></td>
                                            <td><?php echo $transfer_stock_grp->approval_status_str; ?>  <?php if(($this->user_data['branch_id'] == $transfer_stock_grp->to_brn_id) && $transfer_stock_grp->approval_status != 2) { ?> <a class="label btn btn-info delete delete-confirm" href="#" data-confirm-content="You want to approve this Transfer Stock!" data-redirect-url="<?php echo base_url(); ?>transfer_stock/status_update/2/<?php echo $transfer_stock_grp->id; ?>" data-bindtext="Transfer Stock"><span>Approve</span></a> <?php if($transfer_stock_grp->approval_status != 3) { ?><a class="label btn btn-danger delete delete-confirm" href="#" data-confirm-content="You want to reject this Transfer Stock!" data-redirect-url="<?php echo base_url(); ?>transfer_stock/status_update/3/<?php echo $transfer_stock_grp->id; ?>" data-bindtext="Transfer Stock"><span>Reject</span></a> <?php } } ?></td>
                                        </tr>
                                    </tbody>
                                </table>  
                                
                                <div class="text-left">
                                    <span class="onoffswitch-title">
                                        <h3><b style="color: #8e233b;">Items Details </b></h3>
                                    </span>
                                </div>
                                <div id="widget-tab-1" class="table-responsive">
                                    <table id="dt_basic_2" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
                                        <?php if(count($transfer_stock)>0) { ?>
                                        <thead>
                                            <tr>
                                                <th data-class="expand" style="font-weight: bold; text-align:left;">S.No</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:left;">Item Code</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:left;">Description</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Quantity</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:left;">Reason</th>
                                            </tr>
                                        </thead>
                                        <?php } ?>
                                        <tbody>
                                            <?php if(count($transfer_stock)>0) { foreach ($transfer_stock as $key => $item) { ?>
                                                <tr>
                                                    <td><?php echo ++$key; ?></td>
                                                    <td><?php echo $item->pdt_code; ?></td>
                                                    <td><?php echo nl2br($item->product_description); ?></td>
                                                    <td align="center"><?php echo $item->received_qty; ?></td>
                                                    <td ><?php echo $item->reason; ?></td>
                                                </tr>
                                            <?php } ?>
                                            <?php } else { ?>
                                                <tr>
                                                    <td colspan="5" style="text-align: center;"> no record found!.. </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                            <!-- end widget div -->

                        </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
