<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>transfer_stock/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="edit_form" action="<?php echo $form_action; ?>" method="post" id="transfer_stock_form" name="transfer_stock_form" enctype="multipart/form-data">
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">

                                        <fieldset>                                      
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>
                                            <div class="row">  
                                                <input type="hidden" class="form-control transfer_stock_id" name="transfer_stock_id" value="<?php echo $transfer_stock_grp->id; ?>" />
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">From Branch<span style="color: red;">*</span></label>
                                                        <label class="select">
                                                            <select  name="frm_branch_id" class="frm_branch_id select2">  
                                                                <option value="">Select Branch</option>
                                                                <?php foreach ($my_branch as $branch_list) { ?>
                                                                            <option value="<?php echo $branch_list->id; ?>" <?= ($transfer_stock_grp->from_brn_id==$branch_list->id) ? "selected" : ""; ?> ><?php echo $branch_list->branch_name; ?></option>                                    
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Date<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="transfer_date" id="transfer_date" class="form-control bootstrap-datepicker-comncls" tabindex="-1" data-placeholder="DD/MM/YYYY" value="<?php echo $transfer_stock_grp->transfer_date_str; ?>" />
                                                        </label>
                                                    </section>
                                                </section>
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">To Branch<span style="color: red;">*</span></label>
                                                        <label class="select">
                                                            <select  name="to_branch_id" class="to_branch_id select2">  
                                                                <option value="">Select Branch</option>
                                                                    <?php foreach ($branches as $key => $val) { ?>
                                                                    <option value="<?php echo $val->id; ?>" <?= ($transfer_stock_grp->to_brn_id==$val->id) ? "selected" : ""; ?> ><?php echo $val->branch_name; ?></option>
                                                                    <?php } ?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Tracking No.</label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-key"></i>
                                                            <input type="text" name="tracking_no" id="tracking_no" class="form-control" readonly="" value="<?php echo $transfer_stock_grp->tracking_no; ?>" />
                                                        </label>
                                                    </section>
                                                </section>
                                            </div>
                                            </fieldset>
                                            <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Items Details</legend>
                                            <div class="row product_search">
                                                <section class="col col-5">
                                                    <div class="input hidden-mobile">
                                                        <input name="search_product" class="form-control search_product lowercase" type="text" id="search_product" tabindex="-1" autofocus />
                                                    </div>
                                                </section>
                                            </div>
                                            <div class="row material-container product_details">
                                                <div class="items_head col-xs-12 clr">
                                                    <section class="col col-2">
                                                        <label class="label" style="text-align: center;"><b>Item Code</b></label>
                                                    </section>
                                                    <section class="col col-4">
                                                        <label class="label" style="text-align: center;"><b>Brand - Description</b></label>
                                                    </section>
                                                    <section class="col col-2">
                                                        <label class="label" style="text-align: center;"><b>Quantity</b></label>
                                                    </section>
                                                    <section class="col col-3">
                                                        <label class="label" style="text-align: center;"><b>Reason</b></label>
                                                    </section>
                                                    <section class="col col-1">
                                                        <label class="label" style="text-align: center;">&nbsp;</label>
                                                    </section>
                                                </div>
                                                <?php $item_count = count($transfer_stock); ?>
                                                <?php foreach ($transfer_stock as $item_key => $item) { ?>
                                                <div class="items col-xs-12 clr">
                                                        <input type="hidden" name="hidden_soitem_id[<?php echo $item_key; ?>]" value="<?php echo $item->id; ?>" />
                                                        <section class="col col-2" style="padding-right: 0px;">
                                                            <label class="input">
                                                                <input type="hidden" name="product_id[<?php echo $item_key; ?>]" class="product_id" value="<?php echo $item->product_id; ?>" />
                                                                <input type="text" name="product_code[<?php echo $item_key; ?>]" class="product_code" value="<?php echo $item->pdt_code; ?>" data-value="<?php echo $item->pdt_code; ?>" />
                                                            </label>
                                                            <span class="product_code_label">Selected Item code already exist!</span>
                                                            <span class="available_qty_label">Selected Item code already exist!</span>
                                                        </section>
                                                        <section class="col col-4">
                                                            <section style="margin-bottom: 0px;">
                                                                <label class="input">
                                                                    <input type="text" name="product_description[<?php echo $item_key; ?>]" class="product_description" data-value="<?php echo form_prep($item->product_description); ?>" value="<?php echo form_prep($item->product_description); ?>" />
                                                                </label>
                                                                <span class="error_description">Selected Item description already exist!</span>
                                                            </section>
                                                        </section>
                                                        <section class="col col-2">
                                                            <label class="input">
                                                                <input type="text" name="quantity[<?php echo $item_key; ?>]" class="quantity float_value" value="<?php echo $item->received_qty; ?>" />
                                                            </label>
                                                            <span class="unit_label"></span>
                                                        </section>

                                                        <section class="col col-3">
                                                            <label class="textarea textarea-expandable" >
                                                                <i class="icon-append fa fa-comment"></i>
                                                                <textarea class="form-control reason_txt" name="reason_txt[<?php echo $item_key; ?>]" rows="2" placeholder="" ><?php echo $item->reason; ?></textarea>
                                                            </label>
                                                        </section>

                                                        <section class="col col-1">
                                                            <label class="input">
                                                                <?php if ($item_count == ($item_key + 1)) { ?>
                                                                    <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                                <?php } ?>
                                                                <?php if ($item_count > 1) { ?>
                                                                <i class="fa fa-2x fa-minus-circle remove-row"></i>
                                                                <?php } ?>
                                                            </label>
                                                        </section>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <div class="row">
                                                <br>
                                                <section class="col col-6">
                                                    <label class="label">Remark <span style="color: red">*</span></label>
                                                    <label class="textarea textarea-expandable"><textarea rows="3" name="remark" id="remark" maxlength="500"><?= $transfer_stock_grp->remark; ?></textarea> </label> <div class="note">
                                                        <strong>Note:</strong> expands on focus.
                                                    </div>
                                                </section> 

                                                <section class="col col-6">
                                                    <?php if($this->user_data['branch_id'] == $transfer_stock_grp->to_brn_id) { ?>
                                                    <label class="label">Status </label>
                                                    <label class="select">
                                                        <select name="approval_status" id="approval_status" class="select2">
                                                            <option value=""> Select Approval Status</option>
                                                            <option value="2" <?php echo ($transfer_stock_grp->approval_status == 2) ? 'selected' : ''; ?>>Approve</option>
                                                            <option value="3" <?php echo ($transfer_stock_grp->approval_status == 3) ? 'selected' : ''; ?>>Reject</option>
                                                        </select>
                                                    </label>
                                                    <?php } ?>
                                                </section>  
                                            </div>
                                        </fieldset> 
                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/transfer_stock.js"></script>
