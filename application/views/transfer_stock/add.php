<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo base_url(); ?>transfer_stock/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                            </span>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="" action="<?php echo $form_action; ?>" method="post" id="transfer_stock_form" name="transfer_stock_form" enctype="multipart/form-data">

                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">                                   
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>

                                            <div class="row"> 
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">From Branch<span style="color: red;">*</span></label>
                                                        <label class="select">
                                                            <select  name="frm_branch_id" class="frm_branch_id select2">  
                                                                <option value="">Select Branch</option>
                                                                <?php foreach ($my_branch as $branch_list) { ?>
                                                                            <option value="<?php echo $branch_list->id; ?>" <?= ($this->user_data['branch_id']==$branch_list->id) ? "selected" : ""; ?> ><?php echo $branch_list->branch_name; ?></option>                                    
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                     <section>
                                                        <label class="label">Date<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="transfer_date" id="transfer_date" class="form-control bootstrap-datepicker-comncls" tabindex="-1" data-placeholder="DD/MM/YYYY" value="<?php echo date('d/m/Y'); ?>" />
                                                        </label>
                                                    </section>
                                                </section>
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">To Branch<span style="color: red;">*</span></label>
                                                        <label class="select">
                                                            <select  name="to_branch_id" class="to_branch_id select2">  
                                                                <option value="">Select Branch</option>
                                                                    <?php foreach ($branches as $key => $val) { ?>
                                                                    <option value="<?php echo $val->id; ?>"><?php echo $val->branch_name; ?></option>
                                                                    <?php } ?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                </section>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Items Details</legend>
                                            <div class="row product_search">
                                                <section class="col col-5">
                                                    <div class="input hidden-mobile">
                                                        <input name="search_product" class="form-control search_product lowercase" type="text" id="search_product" tabindex="-1" autofocus />
                                                    </div>
                                                </section>
                                            </div>
                                            <div class="row material-container product_details">
                                                <div class="items_head col-xs-12 clr">
                                                    <section class="col col-2">
                                                        <label class="label" style="text-align: center;"><b>Item Code</b></label>
                                                    </section>
                                                    <section class="col col-4">
                                                        <label class="label" style="text-align: center;"><b>Brand - Description</b></label>
                                                    </section>
                                                    <section class="col col-2">
                                                        <label class="label" style="text-align: center;"><b>Quantity</b></label>
                                                    </section>
                                                    <section class="col col-3">
                                                        <label class="label" style="text-align: center;"><b>Reason</b></label>
                                                    </section>
                                                    <section class="col col-1">
                                                        <label class="label" style="text-align: center;">&nbsp;</label>
                                                    </section>
                                                </div>
                                                <div class="items col-xs-12 clr">
                                                        <section class="col col-2" style="padding-right: 0px;">
                                                            <label class="input">
                                                                <input type="hidden" name="product_id[0]" class="product_id" value="" />
                                                                <input type="text" name="product_code[0]" class="product_code" data-value="" />
                                                            </label>
                                                            <span class="product_code_label">Selected Item code already exist!</span>
                                                            <span class="available_qty_label">Selected Item code already exist!</span>
                                                        </section>
                                                        <section class="col col-4">
                                                            <section style="margin-bottom: 0px;">
                                                                <label class="input">
                                                                    <input type="text" name="product_description[0]" class="product_description" data-value="" />
                                                                </label>
                                                                <span class="error_description">Selected Item description already exist!</span>
                                                            </section>
                                                        </section>
                                                        <section class="col col-2">
                                                            <label class="input">
                                                                <input type="text" name="quantity[0]" class="quantity float_value" />
                                                            </label>
                                                            <span class="unit_label"></span>
                                                        </section>

                                                        <section class="col col-3">
                                                            <label class="textarea textarea-expandable" >
                                                                <i class="icon-append fa fa-comment"></i>
                                                                <textarea class="form-control reason_txt" name="reason_txt[0]" rows="2" placeholder="" ></textarea>
                                                            </label>
                                                        </section>

                                                        <section class="col col-1">
                                                            <label class="input">
                                                                <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                                <i class="fa fa-2x fa-bullhorn promotion"></i>
                                                            </label>
                                                        </section>
                                                    </div>
                                            </div>
                                            <div class="row"><br>
                                                <section class="col col-6">
                                                    <label class="label">Remarks </label>
                                                    <label class="textarea"><textarea rows="3" name="remark" id="remark" maxlength="500"></textarea> </label> <div class="note">
                                                        <strong>Note:</strong> expands on focus.
                                                    </div>
                                                </section> 

                                                <section class="col col-6">
                                                    &nbsp;
                                                </section>  
                                            </div>
                                        </fieldset>    
                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/transfer_stock.js"></script>
