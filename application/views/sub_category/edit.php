<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>sub_category/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="edit_form" action="<?php echo $form_action; ?>" method="post" id="sub_cat_form" name="sub_cat_form" enctype="multipart/form-data">
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">

                                        <fieldset>                                      
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>
                                            <div class="row">  
                                                <input type="hidden" class="form-control sub_cat_id" name="sub_cat_id" value="<?php echo $sub_cat->id; ?>" />

                                                <section class="col col-6">
                                                    <label class="label">Item Category Name <span style="color: red">*</span></label>
                                                    <label class="select">
                                                        <select name="item_category" id="item_category" class="select2">
                                                            <option value="">Select Category Name</option>
                                                            <?php foreach ($category as $key => $value) { ?>
                                                            <option value="<?php echo $value->id; ?>" <?php echo ($value->id == $sub_cat->category_id) ? 'selected' : ''; ?>><?php echo $value->item_category_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Sub Category Name<span style="color: red">*</span>
                                                    </label>
                                                    <label class="input"><i class="icon-append fa fa-list-alt"></i>
                                                        <input type="text" data-placeholder="XYZ"  name="sub_category" id="sub_category" maxlength="50" value="<?php echo $sub_cat->sub_category_name; ?>">
                                                    </label>
                                                </section>
                                                
                                            </div>
                                            <div class="row"> 
                                                <section class="col col-6">
                                                    <label class="label">Status<span style="color: red">*</span></label>
                                                    <label class="select">
                                                        <select name="status" id="status" class="select2">
                                                            <option value="1" <?php echo ($sub_cat->status_str == 'Active') ? 'selected' : ''; ?>>Active</option>
                                                            <option value="2"<?php echo ($sub_cat->status_str == 'Inactive') ? 'selected' : ''; ?>>Inactive</option>
                                                        </select></label>
                                                </section>
                                                
                                            </div>

                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/sub_category.js"></script>
