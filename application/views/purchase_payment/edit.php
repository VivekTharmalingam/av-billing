<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if(!empty($success)) {?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                            <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                            <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php }
                else if(!empty($error)) {?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error;?>
                    </div>
                <?php }?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>

        <form class="" action="<?php echo $form_action;?>" method="post" name="purchase_payment_form" id="purchase_payment_form" enctype="multipart/form-data">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">

                            <header>
                                <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                <h2>Purchase Payment Edit </h2>		
                                <span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Purchase Payment List"><a href="<?php echo $list_link; ?>" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a>
                                </span>
                            </header>

                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" >
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                            <div class="row">
                                                 <input type="hidden" class="form-control pay_id" name="pay_id" value="" />
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Purchase Invoice No<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <select name="purchase_inv_no"  class="select2 purchase_inv_no" id="purchase_inv_no">
                                                                <option value="">Select Invoice No</option>
                                                                <?php 
                                                                foreach ($invoice_no as $key => $value) { ?>
                                                                    <option value="<?php echo $value->id; ?>" <?php echo ($value->id == $purchase_payment->pi_id) ? 'selected' : '';?>><?php echo $value->pi_no; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Supplier Name <span style="color:red">*</span></label>
                                                        <label class="input"><i class="icon-append fa fa-black-tie"></i>
                                                            <input type="text" class="form-control sup_name" name="sup_name" data-placeholder="Supplier" maxlength="100" />
                                                        </label>
                                                    </section>
                                                    
                                                    <section>
                                                         <label class="label">Supplier Address</label>
                                                         <label class="input textarea-expandable"">
                                                             <textarea class="form-control sup_address" rows="4" name="sup_address" maxlength="500"></textarea>   
                                                         </label>
                                                    </section>

                                                    <section>
                                                        <label class="label">Total Amount<span style="color: red;">*</span></label>
                                                        <label class="input"><i class="icon-append fa fa-money"></i>
                                                            <input type="text" class="form-control total_amnt" name="total_amnt" data-placeholder="Total amnt" maxlength="50" />
                                                        </label>
                                                    </section>
                                                    
                                                    <section>
                                                        <label class="label">Paid Amount<span style="color: red;">*</span></label>
                                                        <label class="input"><i class="icon-append fa fa-money"></i>
                                                            <input type="text" class="form-control paid_amnt" name="paid_amnt" data-placeholder="Paid amnt" maxlength="50" />
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Amount To Pay<span style="color: red;">*</span></label>
                                                        <label class="input"><i class="icon-append fa fa-money"></i>
                                                            <input type="text" class="form-control amnt-to_pay" name="amnt-to_pay" value="<?php echo $purchase_payment->pay_amt; ?>" data-placeholder="Amount Pay" maxlength="50" />
                                                        </label>
                                                    </section>
                                                </section>
                                                 <section class="col col-6">
                                                    <section>
                                                         <label class="label">Pay Date<span style="color:red">*</span></label>
                                                         <label class="input">
                                                             <div class="input-prepend input-group">
                                                                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                                <input type="text" name="pay_date" id="pay_date" value="<?php echo date($purchase_payment->pay_date); ?>" class="bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" />
                                                             </div>
                                                         </label>
                                                     </section>
                                                    <section>
                                                        <label class="label">Payment Type <span style="color:red">*</span></label>
                                                        <label class="select">
                                                            <select name="payment_type"  class="select2 payment_type" id="payment_type">
                                                                <option value="">Select Payment Type</option>
                                                                <option value="1" <?php echo ($purchase_payment->pay_pay_type == '1') ? 'selected' : '';?>>Cash</option>
                                                                <option value="2" <?php echo ($purchase_payment->pay_pay_type == '2') ? 'selected' : '';?>>Cheque</option>
                                                                <option value="3" <?php echo ($purchase_payment->pay_pay_type == '3') ? 'selected' : '';?>>DD</option>
                                                                <option value="4" <?php echo ($purchase_payment->pay_pay_type == '4') ? 'selected' : '';?>>Net Banking</option>
                                                            </select>
                                                        </label>
                                                    </section>
                                                     <section class="cheque_details">
                                                        <label class="label">Bank Name<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="form-control" name="cheque_bank_name" value="<?php echo $purchase_payment->pay_amt; ?>" data-placeholder="Bank Name" maxlength="100" />
                                                        </label>
                                                    </section>
                                                    <section class="cheque_details">
                                                        <label class="label">Cheque No<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="form-control" name="cheque_acc_no" value="<?php echo $purchase_payment->pay_amt; ?>" data-placeholder="Account No" maxlength="100" />
                                                        </label>
                                                    </section>
                                                     <section class="cheque_details">
                                                        <label class="label">Cheque Date<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="bootstrap-datepicker-comncls" name="cheque_date" value="<?php echo date($purchase_payment->pay_amt); ?>" data-placeholder="Cheque Date" maxlength="100" />
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Status <span style="color:red">*</span></label>
                                                        <label class="select">
                                                            <select  name="status" class="select2">
                                                            <option value="1" <?php echo ($purchase_payment->status == 1) ? 'selected' : '';?>>Active</option>
                                                            <option value="2" <?php echo ($purchase_payment->status == 2) ? 'selected' : '';?>>Inactive</option>
                                                            </select>
                                                        </label>
                                                    </section>
                                                </section>
                                            </div>
                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn btn-primary" >Submit</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/app/purchase_payment.js"></script>

