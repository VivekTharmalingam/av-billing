<!-- MAIN PANEL -->
<style>
.MessageBoxContainer {
    background-color: #F1520C;
}
.page_loader {
    position: fixed;
    left: 50%;
    top: 50%;
    width: 100%;
    height: 100%;
    z-index: 9999;
}
</style>
<div id="main" role="main">
    <div class="page_loader" style="display: none;"><img id="loader" src="<?php echo base_url();?>assets/images/load1.gif" title="Loading"></div>
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <input class="hidden_msg" value="<?php echo (!empty($success)) ? $success : '';?>" type="hidden" />
            <input class="bckup_name" value="<?php echo (!empty($backup_name)) ? $backup_name : '';?>" type="hidden" />
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>Settings</h2>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="edit_form" action="<?php echo $form_setting_data; ?>" method="post" id="setting_form" name="setting_form" enctype="multipart/form-data">
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">

                                        <fieldset>                                      
                                            <legend style=" font-size: 12px; color: red;"></legend>
                                          
                                            <div class="row">
                                                <!--<section class="col col-6">
                                                    <label class="label">Database Backup</label>
                                                    <label class="input">
                                                        <a href="" style="color: #F1520C;"><i class="fa fa-download" style="color: #F1520C;"></i> <b>Backup</b></a>
                                                    </label>
                                                </section>-->
                                                <?php if(in_array(7,$permissions)) { ?>
                                                    <div class="col col-6">
                                                        <section style="width: 70%;">
                                                            <div class="info-box" style="min-height: 50px; box-shadow: 1px 1px 1px rgba(241, 82, 12, 0.32);">
                                                                <a class="formpost-confirm" href="#" title="Take Database Backup" data-confirm-content="Do you want to take Database Backup !" data-redirect-url="<?php echo  base_url();?>settings/db_backup" data-bindtext="Database Backup" data-action="DATABASE BACKUP" style="display: inline-block; margin-top: 5px; text-decoration: none;" target="_blank">
                                                                    <span class="info-box-icon bg-aqua" style="background-color: #F1520C !important; height: 50px; width: 50px;font-size: 32px; line-height: 56px;"><i class="fa fa-download"></i></span>

                                                                    <div class="info-box-content" style="padding-top: 16px">
                                                                        <span class="info-box-text" style="color: #F1520C;"><b>Database Backup</b></span><br>
                                                                        <span class="purch-info-box-number"></span>
                                                                    </div>
                                                                </a> 
                                                                <!-- /.info-box-content -->
                                                            </div>
                                                            <div style="color: #F1520C; word-wrap: break-word;"><b>Last Backup File Path: </b>
                                                                <div>
                                                                    <?php echo (!empty($latest_filename)) ? $latest_filename: 'Not yet done any backup.';?>
                                                                </div>
                                                            </div>
                                                        </section>
                                                    </div>
                                                <?php } ?>
                                                
                                                <?php if(in_array(8,$permissions)) { ?>
                                                    <div class="col col-6">
                                                        <section style="width: 70%;     margin-left: 10%;">
                                                            <div class="info-box" style="min-height: 50px; box-shadow: 1px 1px 1px rgba(67, 117, 69, 0.49);">
                                                                <a style="display: inline-block; margin-top: 5px; text-decoration: none;" href="#"  data-href="<?php echo  base_url();?>settings/db_import" id="smart-mod-eg2" title="Import Database">
                                                                    <span class="info-box-icon bg-aqua" style="background-color:#1F88C9 !important; height: 50px; width: 50px;font-size: 32px; line-height: 56px;"><i class="fa fa-upload"></i></span>

                                                                    <div class="info-box-content" style="padding-top: 16px">
                                                                        <span class="info-box-text" style="color: #1F88C9;"><b>Database Import</b></span><br>
                                                                        <span class="purch-info-box-number"></span>
                                                                    </div>
                                                                </a> 
                                                                <!-- /.info-box-content -->
                                                            </div>
                                                        </section>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </fieldset>
                                        <div style="background-color: #FCE9E1;">
                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">GST Settings</legend>
                                            <div class="row">
                                                <input type="hidden" class="form-control company_id" name="company_id" value="<?php echo $company->id; ?>" />
                                                <section class="col col-6">
                                                    <label class="label">GST(%) </label>
                                                    <label class="input"><i class="icon-append fa fa-money"></i>
                                                        <input type="text" data-placeholder="5"  name="gst_percent" id="gst_percent" maxlength="150" class="float_value" value="<?php echo $company->gst; ?>">
                                                    </label>
                                                </section>
                                                <section class="col col-6">&nbsp;
                                                </section>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Email Settings</legend>
                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Daily Report From Email</label>
                                                    <label class="input"><i class="icon-append fa fa-envelope-o"></i>
                                                        <input type="text" name="daily_from_email" id="daily_from_email" class="lowercase" value="<?php echo $company->daily_from_email; ?>" maxlength="150" />
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Daily Report To Email </label>
                                                    <label class="input"><i class="icon-append fa fa-envelope-o"></i>
                                                        <input type="text" name="daily_to_email" id="daily_to_email" class="lowercase"  value="<?php echo $company->daily_to_email; ?>" maxlength="150" />
                                                    </label>
                                                </section>
                                            </div>
                                        </fieldset>
                                        <footer style="background-color: #FBDBCD;">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </footer>
                                    </div>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/home.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/your_style.css">
<script type="text/javascript">
    $(function() {
        $(document).on('click', '#smart-mod-eg2', function (e) {
            $('#ImportEmail').modal();
            var $action = $(this).attr('data-href');
            $('#ImportEmail form').attr('action',$action);
        });

        /*** Download Backup Code ***/
        if($('.hidden_msg').val() != '' && $('.bckup_name').val() != '' && $('.bckup_name').val() == 2) {
            window.location.href = baseUrl + 'settings/get_last_backup_file';
        }

        $(document).on('click', '#ImportEmail .ok', function (e) {
            if($('#ImportEmail .import_db_file').val() == '') {
                $('#ImportEmail .error_msg').text('Please Upload File!');
                return false;
            } else if($('#ImportEmail .import_db_file').val() != '') {
                var val = $('#ImportEmail .import_db_file').val();
                var regex = new RegExp("(.*?)\.(sql)$");
                if(!(regex.test(val))) {
                    $('#ImportEmail .error_msg').text('File Extension is Wrong. It\'s only allowed .sql format');
                    return false;
                }
            } 
            
            $('#ImportEmail .error_msg').text('');
            $('#ImportEmail .load_img').show();
            $('#ImportEmail .ok,#ImportEmail .imp_cancel'). hide();
        });
        
        $(document).on('click', '#ImportEmail .imp_cancel', function (e) {
            //$('.import_db_file').val('');
            $('#ImportEmail .error_msg').text('');
        });
        
        $(document).on('click', '.MessageBoxContainer #bot2-Msg1', function (e) {
            $('.page_loader').show();
            //setInterval(function(){ window.location.href = baseUrl + "settings"; }, 3000);
        });
    });
</script>
