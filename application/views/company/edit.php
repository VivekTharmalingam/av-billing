<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>company/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="edit_form" action="<?php echo $form_action; ?>" method="post" id="company_form" name="company_form" enctype="multipart/form-data">
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">

                                        <fieldset>                                      
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>

                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Company Name <span style="color: red">*</span> <span style="position: absolute;right: 15px;"> </label>

                                                    <label class="input"> <i class="icon-append fa fa-briefcase"></i>
                                                        <input type="text" data-placeholder="REFULGENCE INC PTE. LTD"  name="company_name" id="company_name" maxlength="150" value="<?php echo $company->comp_name; ?>">
                                                    </label>
                                                </section>
                                                <input type="hidden" class="form-control company_id" name="company_id" value="<?php echo $company->id; ?>" />
                                                <section class="col col-6">
                                                    <label class="label">Company Reg. No <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-barcode"></i>
                                                        <input type="text" data-placeholder="ABC0001"  name="reg_no" id="reg_no" maxlength="50" value="<?php echo $company->comp_reg_no; ?>">
                                                    </label>
                                                </section>

                                            </div>
                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Address <span style="color: red">*</span></label>
                                                    <label class="textarea textarea-expandable"><textarea rows="3" name="address" id="address" maxlength="500"><?php echo $company->comp_address; ?></textarea> </label> <div class="note">
                                                        <strong>Note:</strong> expands on focus.
                                                    </div>
                                                </section> 
                                                <section class="col col-6">
                                                    <label class="label">Phone No <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-phone"></i>
                                                        <input type="text" data-placeholder="65989898"  name="phone_no" id="phone_no" maxlength="15" value="<?php echo $company->comp_phone; ?>">
                                                    </label>
                                                </section>

                                            </div>
                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Pin Code <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-map-marker"></i>
                                                        <input type="text" data-placeholder="210668"  name="pin_code" id="pin_code" maxlength="8" value="<?php echo $company->comp_pin_code; ?>">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Fax</label>
                                                    <label class="input"><i class="icon-append fa fa-fax"></i>
                                                        <input type="text" data-placeholder="99999999"  name="fax" id="fax" maxlength="15" value="<?php echo $company->comp_fax; ?>">
                                                    </label>
                                                </section>

                                            </div>
                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Email <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-envelope-o"></i>
                                                        <input type="text" data-placeholder="info@refulgenceinc.com" class="lowercase" name="email" id="email" maxlength="70" value="<?php echo $company->comp_email; ?>">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Website </label>
                                                    <label class="input"><i class="icon-append fa fa-globe"></i>
                                                        <input type="text" data-placeholder="http://www.refulgenceinc.com/" name="website" id="website" class="lowercase" maxlength="150" value="<?php echo $company->comp_website; ?>">
                                                    </label>
                                                </section>

                                            </div>
                                            <div class="row" >                                       

<!--                                                <section class="col col-6 section-group">
                                                    <label class="label">Logo</label>
                                                    <label for="file" class="input input-file">
                                                        <div class="button">
                                                        <input type="file" name="logo" class="img_file" data-allowed-types="png|jpg|jpeg|jif" >Browse</div>
                                                        data-allowed_width="500px" data-allowed_height="500px"
                                                        <input type="file" class="form-control btn btn-default parallel img_file" name="logo" value="" style="height: 40px;"  data-allowed-types="png|jpg|jpeg|jif"> 
                                                        <?php #echo $company->comp_logo; ?> 
                                                    </label>
                                                    <?php if (!empty($company->comp_logo)) { ?>
                                                        <span class="img_preview" style="padding-top: 4px;float: right;">
                                                            <img src="<?php echo upload_url() . $company->comp_logo; ?>"></span>
                                                    <?php } ?>
                                                </section>-->

                                                <!--<section class="col col-6">
                                                    <label class="label">GST(%) </label>
                                                    <label class="input"><i class="icon-append fa fa-money"></i>
                                                        <input type="text" data-placeholder="5"  name="gst_percent" id="gst_percent" maxlength="150" class="float_value" value="<?php echo $company->gst; ?>">
                                                    </label>
                                                </section>-->

                                                <section class="col col-6">
                                                    <label class="label">GST No.</label>
                                                    <label class="input"><i class="icon-append fa fa-money"></i>
                                                        <input type="text" data-placeholder="5"  name="gst_no" id="gst_percent" maxlength="150" value="<?php echo $company->comp_gst_no; ?>">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Banking Account No. <span style="color: red">*</span> <span style="position: absolute;right: 15px;" class="crnt_bln"></span></label>
                                                    <select name="bank_acc_id" class="select2 bank_acc_id" id="bank_acc_id">
                                                        <option value="" data-crntbln="" >Select Account Number</option>
                                                        <?php foreach ($active_bank as $key => $bank) { ?>
                                                            <option value="<?php echo $bank->id; ?>" data-crntbln="<?= $bank->current_balance ?>" <?= ($bank->id == $company->bank_acc_id) ? "selected" : ""; ?> ><?php echo $bank->bank_name . ' / ' . $bank->account_no; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </section>

                                            </div>                                            
                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">PDF Logo</label>
                                                    <label class="input"><div class="parallel thumb preview" style="width: 9%;">
                                                            <!--<img src="<?php echo BASE_URL . NO_IMG; ?>" />-->
                                                            <img src="<?php echo (!empty($company->comp_logo) ? BASE_URL . UPLOADS . $company->comp_logo : BASE_URL . NO_IMG ); ?>" />
                                                        </div>
                                                        <input type="file" class="form-control btn btn-default parallel" name="logo" data-allowed-types="png|jpg|jpeg|jif" data-actual="<?php echo $company->comp_logo; ?>" value="" data-default="<?php echo BASE_URL . NO_IMG; ?>" style="width: 90%; height: 45px;"/>
                                                    </label>
                                                </section>    
                                                <section class="col col-6">
                                                    <label class="label">Fav Icon</label>
                                                    <label class="input"><div class="parallel thumb preview" style="width: 9%;">
                                                            <img src="<?php echo (!empty($company->comp_favicon) ? BASE_URL . UPLOADS . $company->comp_favicon : BASE_URL . NO_IMG ); ?>" />
                                                        </div>
                                                        <input type="file" class="form-control btn btn-default parallel" name="favicon" data-allowed-types="png|jpg|jpeg|jif" data-actual="<?php echo $company->comp_favicon; ?>" value="" data-default="<?php echo BASE_URL . NO_IMG; ?>" style="width: 90%; height: 45px;"/>
                                                    </label>
                                                </section>    
                                            </div>
                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Login Logo</label>
                                                    <label class="input"><div class="parallel thumb preview" style="width: 9%;">
                                                            <img src="<?php echo (!empty($company->login_logo) ? BASE_URL . UPLOADS . $company->login_logo : BASE_URL . NO_IMG ); ?>" />
                                                        </div>
                                                        <input type="file" class="form-control btn btn-default parallel" name="login_logo" value="" data-allowed-types="png|jpg|jpeg|jif" data-actual="<?php echo $company->login_logo; ?>" data-default="<?php echo BASE_URL . NO_IMG; ?>" style="width: 90%; height: 45px;"/>
                                                    </label>
                                                </section>    
                                                <section class="col col-6">
                                                    <label class="label">Inner Logo</label>
                                                    <label class="input"><div class="parallel thumb preview" style="width: 9%;">
                                                            <img src="<?php echo (!empty($company->inner_logo) ? BASE_URL . UPLOADS . $company->inner_logo : BASE_URL . NO_IMG ); ?>" />
                                                        </div>
                                                        <input type="file" class="form-control btn btn-default parallel" name="inner_logo" data-allowed-types="png|jpg|jpeg|jif" value="" data-actual="<?php echo $company->inner_logo; ?>"  data-default="<?php echo BASE_URL . NO_IMG; ?>" style="width: 90%; height: 45px;"/>
                                                    </label>
                                                </section>    
                                            </div>
                                            
                                            <div class="row" > 
                                                
<!--                                                <section class="col col-6 section-group">
                                                    <label class="label">Favicon</label>
                                                    <label for="file" class="input input-file">
                                                        <div class="button">
                                                        <input type="file" name="logo" class="img_file" data-allowed-types="png|jpg|jpeg|jif" >Browse</div>
                                                        data-allowed_width="500px" data-allowed_height="500px"
                                                        <input type="file" class="form-control btn btn-default parallel img_file" name="favicon" value="" style="height: 40px;"  data-allowed-types="png|jpg|ico"> 
                                                        <?php #echo $company->comp_logo; ?> 
                                                    </label>
                                                    <?php if (!empty($company->comp_favicon)) { ?>
                                                        <span class="img_preview" style="padding-top: 4px;float: right;">
                                                            <img src="<?php echo upload_url() . $company->comp_favicon; ?>"></span>
                                                    <?php } ?>
                                                </section>-->
<!--                                                <section class="col col-6">
                                                    <label class="label">Account No. <span style="color: red">*</span> <span style="position: absolute;right: 15px;" class="crnt_bln"></span></label>
                                                    <select name="bank_acc_id" class="select2 bank_acc_id" id="bank_acc_id">
                                                        <option value="" data-crntbln="" >Select Account Number</option>
                                                        <?php foreach ($active_bank as $key => $bank) { ?>
                                                            <option value="<?php echo $bank->id; ?>" data-crntbln="<?= $bank->current_balance ?>" <?= ($bank->id == $company->bank_acc_id) ? "selected" : ""; ?> ><?php echo $bank->bank_name . ' / ' . $bank->account_no; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </section>-->
                                            </div>
                                            <!--<div class="row">
                                            <section class="col col-6">
                                                            <label class="label">Status</label>
                                                            <label class="select">
                                                               <select name="section_status" id="section_status">
                                                                    <option value="1" <?php echo ($company->status == 1) ? 'selected' : ''; ?>>Active</option>
                                                                    <option value="2"<?php echo ($company->status == 2) ? 'selected' : ''; ?>>Inactive</option>
                                                            </select>  </label>
                                                    </section> 
                                            </div>-->

                                        </fieldset>
                                        <!--<fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Email Settings</legend>
                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Daily Report From Email <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-envelope-o"></i>
                                                        <input type="text" name="daily_from_email" id="daily_from_email" class="lowercase" value="<?php echo $company->daily_from_email; ?>" maxlength="150" />
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Daily Report To Email </label>
                                                    <label class="input"><i class="icon-append fa fa-envelope-o"></i>
                                                        <input type="text" name="daily_to_email" id="daily_to_email" class="lowercase"  value="<?php echo $company->daily_to_email; ?>" maxlength="150" />
                                                    </label>
                                                </section>
                                            </div>
                                        </fieldset>-->
                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Branch Details</legend>
                                            <div class="row material-container product_details">
                                                <?php $length = count($branches); ?>
                                                <?php if (!empty($branches)) {
                                                    foreach ($branches as $key => $branch) { ?>
                                                        <input type="hidden" name="edit_brn_id[]" class="edit_brn_id" value="<?php echo $branch->id; ?>" />
                                                        <div class="branch clr">
                                                            <input type="hidden" name="brn_id[]" class="brn_id" value="<?php echo $branch->id; ?>" />

                                                            <section class="col col-4">
        <?php echo ($key == 0) ? '<label class="label"><b>Branch Name</b><span style="color: red">*</span></label>' : ''; ?>
                                                                <label class="input">
                                                                    <input type="text" name="brn_name[]" class="branch_name" data-placeholder="Jurong Branch" value="<?php echo $branch->branch_name; ?>" />
                                                                </label>                                                           
                                                            </section>


                                                            <section class="col col-4">
        <?php echo ($key == 0) ? '<label class="label"><b>Address</b><span style="color: red">*</span></label>' : ''; ?>
                                                                <label class="textarea">
                                                                    <textarea name="brn_address[]" class="brn_address" rows="5"><?php echo $branch->address; ?></textarea>
                                                                </label>
                                                            </section>

                                                            <section class="col col-3">
        <?php echo ($key == 0) ? '<label class="label"><b>Status</b><span style="color: red">*</span></label>' : ''; ?>        
                                                                <label class="select">
                                                                    <select  name="brn_status[]" class="select2 brn_status">
                                                                        <option value="">Select Status</option>
                                                                        <option value="1" <?php echo ($branch->status_str == 'Active') ? 'selected' : ''; ?>>Active</option>
                                                                        <option value="2" <?php echo ($branch->status_str == 'Inactive') ? 'selected' : ''; ?>>Inactive</option>
                                                                    </select>
                                                                </label>
                                                            </section>

                                                            <section class="col col-1">
                                                                <?php echo ($key == 0) ? '<label class="label">&nbsp;</label>' : ''; ?>
                                                                <?php if ($length == ($key + 1)) { ?>
                                                                    <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                                <?php } ?>
                                                                <?php if ($length > 1) { ?>
                                                                    <i class="fa fa-2x fa-minus-circle remove-row"></i>
        <?php } ?>
                                                            </section>

                                                        </div>
    <?php }
} else { ?>
                                                    <div class="branch clr">												 
                                                        <section class="col col-4">
                                                            <label class="label"><b>Branch Name</b><span style="color: red;">*</span></label>
                                                            <label class="input">
                                                                <input type="text" name="new_branch_name[0]" class="branch_name" data-placeholder="Jurong Branch" />
                                                            </label>                                                       
                                                        </section>

                                                        <section class="col col-4">
                                                            <label class="label"><b>Address </b><span style="color: red">*</span></label>
                                                            <label class="textarea textarea-expandable"><textarea rows="3" name="new_brn_address[0]" class="brn_address" maxlength="500"></textarea> </label> 
                                                        </section> 

                                                        <section class="col col-3">
                                                            <label class="label"><b>Status</b><span style="color: red;">*</span></label>
                                                            <label class="select">
                                                                <select  name="new_brn_status[0]" class="select2 brn_status">
                                                                    <option value="">Select Status</option>
                                                                    <option value="1">Active</option>
                                                                    <option value="2">Inactive</option>
                                                                </select>
                                                            </label>
                                                        </section>

                                                        <section class="col col-1">
                                                            <label class="label">&nbsp;</label>
                                                            <label class="input">
                                                                <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                            </label>
                                                        </section>
                                                    </div>	
<?php } ?>
                                            </div>
                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/company.js"></script>
