<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>company/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="" action="<?php echo $form_action; ?>" method="post" id="company_form" name="company" enctype="multipart/form-data">

                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">

                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>

                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Company Name <span style="color: red">*</span></label>

                                                    <label class="input"> <i class="icon-append fa fa-briefcase"></i>
                                                        <input type="text" data-placeholder="REFULGENCE INC PTE. LTD"  name="company_name" id="company_name" maxlength="150">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Company Reg. No <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-barcode"></i>
                                                        <input type="text" data-placeholder="ABC0001"  name="reg_no" id="reg_no" maxlength="50">
                                                    </label>
                                                </section>

                                            </div>
                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Address <span style="color: red">*</span></label>
                                                    <label class="textarea textarea-expandable"><textarea rows="3" name="address" id="address" maxlength="500"></textarea> </label> <div class="note">
                                                        <strong>Note:</strong> expands on focus.
                                                    </div>
                                                </section> 
                                                <section class="col col-6">
                                                    <label class="label">Phone No <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-phone"></i>
                                                        <input type="text" data-placeholder="98989898"  name="phone_no" id="phone_no" maxlength="15">
                                                    </label>
                                                </section>

                                            </div>
                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Pin Code <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-map-marker"></i>
                                                        <input type="text" data-placeholder="210668"  name="pin_code" id="pin_code" maxlength="8">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Fax</label>
                                                    <label class="input"><i class="icon-append fa fa-fax"></i>
                                                        <input type="text" data-placeholder="99999999"  name="fax" id="fax" maxlength="15">
                                                    </label>
                                                </section>

                                            </div>
                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Email <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-envelope-o"></i>
                                                        <input type="text" name="email" id="email" class="lowercase" maxlength="150">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Website </label>
                                                    <label class="input"><i class="icon-append fa fa-globe"></i>
                                                        <input type="text" class="lowercase" name="website" id="website" maxlength="150">
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">                                      

                                                <section class="col col-6 section-group">
                                                    <label class="label">Logo</label>
                                                    <label for="file" class="input input-file">
                                                        <!--                                                <div class="button">
                                                                                                            <input type="file" name="logo" class="img_file" data-allowed-types="png|jpg|jpeg|jif"   id="logo">Browse</div>-->
                                                        <!--                                                    data-allowed_width="50px"-->
                                                        <input type="file" class="form-control btn btn-default parallel img_file" name="logo" value="" style="height: 40px;"  data-allowed-types="png|jpg|jpeg|jif">
                                                        <!--                                                <input type="text" name="browse_logo"  readonly="" class="browse_logo">-->
                                                    </label>
                                                    <span class="img_preview" style="padding-top: 4px;float: right;"></span>

                                                </section>

                                                <section class="col col-6">
                                                    <label class="label">Status</label>
                                                    <label class="select">
                                                        <select name="section_status" class="select2" id="section_status">
                                                            <option value="1">Active</option>
                                                            <option value="2">Inactive</option>
                                                        </select>  </label>
                                                </section>	
                                            </div>
                                            
                                            <div class="row">
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">GST <span style="color:red">*</span></label>
                                                        <div class="inline-group" style="margin-bottom: 5px;">
                                                            <label class="radio">
                                                                <input type="radio" class="tax_type tax_type_add" name="gst" value="1" onclick="get_value(this);">
                                                                <i></i>With GST
                                                            </label>
                                                            <label class="radio">
                                                                <input type="radio" class="tax_type tax_type_deduct" value="0" name="gst" checked="checked" onclick="get_value(this);">
                                                                <i></i>Without GST
                                                            </label>
                                                        </div>
                                                    </section>
                                                </section>											
                                            </div>

                                            <div class="row">
                                                <section class="col col-6 gst_div" style="display:none;margin-top:-20px;">
                                                    <label class="label">GST(%) </label>
                                                    <label class="input"><i class="icon-append fa fa-money"></i>
                                                        <input type="text" data-placeholder="5"  name="gst_percent" id="gst_percent" maxlength="150" class="float_value">
                                                    </label>
                                                </section>
                                            </div>
                                        </fieldset>
										<fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Email Settings</legend>
											<div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Daily Report From Email <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-envelope-o"></i>
                                                        <input type="text" name="daily_from_email" id="daily_from_email" class="lowercase" maxlength="150" />
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Daily Report To Email </label>
                                                    <label class="input"><i class="icon-append fa fa-envelope-o"></i>
                                                        <input type="text" name="daily_to_email" id="daily_to_email" class="lowercase" maxlength="150" />
                                                    </label>
                                                </section>
                                            </div>
										</fieldset>
										
                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Branch Details</legend>
                                            <div class="row material-container product_details">
                                                <div class="branch clr">												 
                                                    <section class="col col-4">
                                                        <label class="label"><b>Branch Name</b><span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <input type="text" name="branch_name[]" class="branch_name" data-placeholder="Jurong Branch" />
                                                        </label>                                                       
                                                    </section>

                                                    <section class="col col-4">
                                                        <label class="label"><b>Address </b><span style="color: red">*</span></label>
                                                        <label class="textarea textarea-expandable"><textarea rows="3" name="brn_address[]" class="brn_address" maxlength="500"></textarea> </label> 
                                                    </section> 

                                                    <section class="col col-3">
                                                        <label class="label"><b>Status</b><span style="color: red;">*</span></label>
                                                        <label class="select">
                                                            <select  name="brn_status[]" class="select2 brn_status">
                                                                <option value="">Select Status</option>
                                                                <option value="1">Active</option>
                                                                <option value="2">Inactive</option>
                                                            </select>
                                                        </label>
                                                    </section>

                                                    <section class="col col-1">
                                                        <label class="label">&nbsp;</label>
                                                        <label class="input">
                                                            <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                        </label>
                                                    </section>
                                                </div>			
                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/company.js"></script>
