<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>company/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-file-text-o"></i> </span>
                            <h2><?php echo $page_title; ?></h2>	
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body">  
                                <span id="watermark"><?php echo WATER_MARK; ?></span>
                                <table id="user" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Company Code</td>
                                            <td style="width:65%"><?php echo $company->comp_code; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Company Name</td>
                                            <td><?php echo $company->comp_name; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Company Reg. No</td>
                                            <td><?php echo $company->comp_reg_no; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Address</td>
                                            <td><?php echo $company->comp_address; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Phone No</td>
                                            <td><?php echo $company->comp_phone; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Pin Code</td>
                                            <td><?php echo $company->comp_pin_code; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Fax</td>
                                            <td><?php echo $company->comp_fax; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Email</td>
                                            <td><?php echo $company->comp_email; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Website</td>
                                            <td><?php echo $company->comp_website; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Logo</td>
                                            <td><span class="img_preview" style="padding-top: 4px;float: left;">
                                                    <img src="<?php echo upload_url() . $company->comp_logo; ?>"></span></td>
                                        </tr>

                                        <?php if ($company->gst == '1') { ?>
                                            <tr>
                                                <td style="font-weight: bold;">GST(%)</td>
                                                <td><?php echo $company->gst; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <td style="font-weight: bold;">Status</td>
                                            <td><?php echo $company->status_str; ?></td>
                                        </tr>                                        
                                    </tbody>
                                </table>
								<div class="text-left">
                                    <span class="onoffswitch-title">
                                        <h3><b style="color: #8e233b;">Email Settings</b></h3>
                                    </span>
                                </div>
								<table class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Daily Report From Email</td>
                                            <td style="width:65%"><?php echo $company->daily_from_email; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Daily Report To Email</td>
                                            <td><?php echo $company->daily_to_email; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
								
                                <div class="text-left">
                                    <span class="onoffswitch-title">
                                        <h3><b style="color: #8e233b;">Branch Details</b></h3>
                                    </span>
                                </div>

                                <div id="widget-tab-1" class="table-responsive">
                                    <table id="dt_basic_2" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
                                        <thead>
                                            <tr>
                                                <th data-class="expand" style="font-weight: bold; text-align:center;">S.No</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Branch Name</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Address</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Status</th>                                        
                                            </tr>
                                        <thead>
                                        <tbody>
                                            <?php foreach ($branches as $key => $branch) { ?>
                                                <tr>
                                                    <td align="center"><?php echo ++$key; ?></td>
                                                    <td align="right"><?php echo $branch->branch_name; ?></td>
                                                    <td align="right"><?php echo $branch->address; ?></td>
                                                    <td align="right"><?php echo $branch->status_str; ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->