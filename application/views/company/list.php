<script>
    window.localStorage.clear();
</script>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4"></div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8"></div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="widget-body">
                       
                        <div id="myTabContent1" class="tab-content padding-10">
                            <a href="<?php echo base_url(); ?>company/add" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-pencil"></i></button></a>
                            <div class="tab-pane fade in active" id="s1">
                                <!-- Widget ID (each widget will need unique ID)-->                                
                                <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false"	>

                                    <header><span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                        <h2>Company List</h2></header>

                                    <div>
                                        <div class="jarviswidget-editbox">
                                            <!-- This area used as dropdown edit box -->
                                        </div>
                                        <div class="widget-body no-padding">
                                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
												<thead>			                
													<tr>
														<th data-class="expand">S.No</th>
														<th ><i class="fa fa-fw fa-building text-muted hidden-md hidden-sm hidden-xs"></i> Company</th>
														<th data-hide="phone"> <i class="fa fa-fw fa-barcode hidden-md hidden-sm hidden-xs"></i> Reg. No</th> 
														<!--<th data-hide="phone"> <i class="fa fa-fw fa-map-marker hidden-md hidden-sm hidden-xs"></i>Address</th>--> 
														<th data-hide="phone"> <i class="fa fa-fw fa-phone hidden-md hidden-sm hidden-xs"></i>Phone</th> 
														<th data-hide="phone"> <i class="fa fa-fw fa-envelope-o hidden-md hidden-sm hidden-xs"></i>Email</th> 
														<th data-hide="phone,tablet"><i class="fa fa-fw fa-flag-o hidden-md hidden-sm hidden-xs"></i> Status</th>
														<th data-hide="phone" style="text-align: center;"><i class="fa fa-fw fa-home hidden-md hidden-sm hidden-xs"></i> Actions</th>
														
													</tr>
												</thead>
												<tbody>
													<?php foreach ($companies as $key => $row) { ?>
														<tr>
															<td><?php echo ++$key; ?></td>
															<td><?php echo $row->comp_name; ?></td>
															<td><?php echo $row->comp_reg_no; ?></td>
															
			<!--                                                <td><?php echo $row->comp_address; ?></td>-->
															<td><?php echo $row->comp_phone; ?></td>                                                                                   <td><?php echo $row->comp_email; ?></td>
															<td><?php echo $row->status_str; ?></td> 
															<td align="center">
																<?php if (in_array(1, $permission)) {?>
																<a class="label btn btn-warning view" href="<?php echo $view_link . $row->id ?>"><span>View</span></a>
																<?php }?>

																<?php if (in_array(3, $permission)) {?>
																<a class="label btn btn-primary edit" href="<?php echo $edit_link . $row->id ?>"><span>Edit</span></a>
																<?php }?>                                                    
																
																<?php if (in_array(4, $permission)) {?>
																<a class="label btn btn-danger pop_up_confirm delete delete-confirm" href="#" data-confirm-content="You will not be able to recover <?php echo $row->comp_name;?> Company details!" data-redirect-url="<?php echo $delete_link . $row->id;?>" data-bindtext="<?php echo $row->comp_name; ?> Company">
																	<span>Delete</span>
																</a>
																<?php }?>
															</td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
                                        </div>
                                        <!-- end widget content -->

                                    </div>
                                    <!-- end widget div -->

                                </div>
                                <!-- end widget -->
                            </div>
                            
                        </div> 
                    </div>
                </article>
                <!-- /.row -->
            </div>

        </section>

    </div>

    

</div>




<script>
    $(document).ready(function () {

        /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;
        var responsiveHelper_datatable_fixed_column = undefined;
        var responsiveHelper_datatable_col_reorder = undefined;
        var responsiveHelper_datatable_tabletools = undefined;

        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            }
        });

        /* END BASIC */

        var otable = $('#datatable_fixed_column').DataTable({
            //"bFilter": false,
            //"bInfo": false,
            //"bLengthChange": false
            //"bAutoWidth": false,
            //"bPaginate": false,
            //"bStateSave": true // saves sort state using localStorage
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_datatable_fixed_column) {
                    responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_datatable_fixed_column.respond();
            }

        });

    });
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/company.js"></script>