<script type="text/javascript" src="<?php echo BASE_URL;?>assets/js/settings.js"></script>
<aside class="right-side">
    <section class="content-header">
        <div class="title_bar">
            <h1 class="major_txt_color">Settings Edit</h1>
            <div class="breadcrumb_container"><?php echo breadcrumb_links();?></div>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                    <div class="box-body table-responsive">
                        <?php if(!empty($success)) {?>
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-thumbs-o-up"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><?php echo $success;?></h4>
                            </div>
                        <?php }
                        else if(!empty($error)) {?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-thumbs-o-down"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><?php echo $error;?></h4>
                            </div>
                        <?php }?>
                    </div>
                    <!-- .box-tools -->
                </div>
            </div>
            
            <div class="col-xs-12">
                <h3 style="margin-top: 0; margin-right: 5px; width: 100%; display: inline-block;">
                    <a class="label btn btn-primary float-rht edit" href="<?php echo $view_link?>"><span>View</span></a>
                </h3>
                 <form name="settings" action="<?php echo $form_action;?>" method="post" enctype="multipart/form-data">
                    <div class="box box-danger">
                        <div class="box-body">
                            <ul class="error_container"></ul>
                            <h3 class="major_txt_color" style="margin: 10px 0 25px 0;">Company</h3>
                            <div class="col-xs-12">
                                <div class="col-md-6">   
                                    <div class="form-group">
                                        <label>Project Title<span class="required">*</span></label>
                                        <input type="text" class="form-control" name="app_title" data-placeholder="Project Title" maxlength="150" value="<?php echo form_prep($setting->app_title);?>" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Project Sub Title<span class="required">*</span></label>
                                        <input type="text" class="form-control" name="app_sub_title" data-placeholder="Project Sub Title" maxlength="150" value="<?php echo form_prep($setting->app_sub_title);?>" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Company Name<span class="required">*</span></label>
                                        <input type="text" class="form-control" name="comp_name" data-placeholder="Company Name" maxlength="150" value="<?php echo form_prep($setting->comp_name);?>" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Registration Number</label>
                                        <input type="text" class="form-control" name="reg_no" data-placeholder="Registration Number" maxlength="50" value="<?php echo form_prep($setting->reg_no);?>" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Company Email</label>
                                        <input type="text" class="form-control" name="comp_email" data-placeholder="Company Email" maxlength="150" value="<?php echo form_prep($setting->comp_email);?>" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Address<span class="required">*</span></label>
                                        <textarea class="form-control resize_none" name="address" data-placeholder="Company Address" maxlength="500"><?php echo form_prep($setting->address);?></textarea>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>GST Percentage</label>
                                        <input type="text" class="form-control" name="gst" data-placeholder="GST Number" value="<?php echo form_prep($setting->gst);?>" />
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contact Name</label>
                                        <input type="text" class="form-control" name="contact_name" data-placeholder="Contact Name" maxlength="150" value="<?php echo form_prep($setting->contact_name);?>" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Contact Email</label>
                                        <input type="text" class="form-control" name="contact_email" data-placeholder="Contact Email" maxlength="50" value="<?php echo form_prep($setting->contact_email);?>" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Contact Number<span class="required">*</span></label>
                                        <input type="text" class="form-control" name="contact_no" data-placeholder="Contact Number" maxlength="50" value="<?php echo form_prep($setting->contact_no);?>" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Contact Fax</label>
                                        <input type="text" class="form-control" name="fax" data-placeholder="Contact Fax" maxlength="20" value="<?php echo form_prep($setting->fax);?>" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Website Url</label>
                                        <input type="text" class="form-control" name="website_url" data-placeholder="Website Url" maxlength="50" value="<?php echo form_prep($setting->website_url);?>" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Logo<span class="required">*</span></label>
                                        <input type="file" class="form-control img_file" name="logo" data-allowed_size="2" data-allowed_width="216" data-allowed_height="96" data-allowed_exetnsions="<?php echo implode('|', image_extensions());?>" />
                                        <!-- Eg: data-allowed_size 2 MB -->
                                        <div class="img_preview">
                                            <?php
                                            if ((!empty($setting->logo)) && (file_exists(FCPATH . DIR_COMPANY_LOGO . $setting->logo))) {
                                                $image_url = BASE_URL . DIR_COMPANY_LOGO . $setting->logo;
                                                $alt = $setting->comp_name;
                                            }
                                            else {
                                                $image_url = BASE_URL . NO_IMG;
                                                $alt = 'No image not found!';
                                            }
                                            ?>
                                            <img src="<?php echo $image_url;?>" alt="<?php echo $alt;?>" />
                                        </div>
                                        <span class="allowed_type">File should be <b><i><?php echo strtoupper(implode(', ', image_extensions()));?></i></b> file format</span>  
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Favicon</label>
                                        <input type="file" class="form-control img_file" name="favicon" data-allowed_width="16" data-allowed_height="16" data-file_exetnsions="ico" />
                                        <!-- Eg: data-allowed_size 2 MB -->
                                        <div class="img_preview">
                                            <?php
                                            if ((!empty($setting->favicon)) && (file_exists(FCPATH . DIR_FAVICON . $setting->favicon))) {
                                                $image_url = BASE_URL . DIR_FAVICON . $setting->favicon;
                                                $alt = $setting->comp_name;
                                            }
                                            else {
                                                $image_url = BASE_URL . DEFAULT_FAVICON;
                                                $alt = 'No image not found!';
                                            }
                                            ?>
                                            <img src="<?php echo $image_url;?>" alt="<?php echo $alt;?>" style="width: 16px; height: 16px;" />
                                        </div>
                                        <span class="allowed_type">File should be <b><i>ico</i></b> file format</span>  
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="box-footer form-actions float-clear" style="text-align:center;">
                            <input class="btn reset" type="reset" value="Reset" />
                            <input type="submit" name="submit" value="Update" class="btn btn-success btn-primary" />
                        </div>
                    </div><!-- /.box -->
                </form>
            </div><!-- /.col-xs-12 -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</aside>