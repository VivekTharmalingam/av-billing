<script type="text/javascript" src="<?php echo BASE_URL;?>assets/js/settings.js"></script>
<aside class="right-side">
	<section class="content-header">
        <div class="title_bar">
            <h1 class="major_txt_color">Settings View</h1>
            <div class="breadcrumb_container"><?php echo breadcrumb_links();?></div>
        </div>
    </section>
	<section class="content">
        <div class="row">
           
            <div class="col-xs-12">
                <?php if (in_array(3, $permission)) {?>
                    <h3 style="margin-top: 0; margin-right: 5px; width: 100%; display: inline-block;">
                        <a class="label btn btn-primary float-rht edit" href="<?php echo $edit_link?>"><span>Edit</span></a>
                    </h3>
                <?php }?>
                <div class="box box-danger">
                    <div class="box-body table-responsive">
                         <h3 class="major_txt_color" style="margin: 10px 0 25px 0;">Company</h3>
                        <div class="col-md-6">     
                           <div class="form-group">
                                <span class="first-coln">Project Title</span>
                                <span class="seperator">:</span>
                                <span class="second-coln">
                                    <?php echo text($setting->app_title);?>
                                </span>
                            </div>
                            
                            <div class="form-group">
                                <span class="first-coln">Project Sub Title</span>
                                <span class="seperator">:</span>
                                <span class="second-coln">
                                    <?php echo text($setting->app_sub_title);?>
                                </span>
                            </div>
                            
                            <div class="form-group">
                                <span class="first-coln">Company Name</span>
                                <span class="seperator">:</span>
                                <span class="second-coln">
                                    <?php echo text($setting->comp_name);?>
                                </span>
                            </div>
                            
                            <div class="form-group">
                                <span class="first-coln">Registration Number</span>
                                <span class="seperator">:</span>
                                <span class="second-coln">
                                    <?php echo text($setting->reg_no);?>
                                </span>
                            </div>
                            
                            <div class="form-group">
                                <span class="first-coln">Company Email</span>
                                <span class="seperator">:</span>
                                <span class="second-coln">
                                    <?php echo text($setting->comp_email);?>
                                </span>
                            </div>
                            
                            <div class="form-group">
                                <span class="first-coln">Address</span>
                                <span class="seperator">:</span>
                                <span class="second-coln">
                                    <?php echo nl2br(text($setting->address));?>
                                </span>
                            </div>
                            
                            <div class="form-group">
                                <span class="first-coln">GST Percentage</span>
                                <span class="seperator">:</span>
                                <span class="second-coln">
                                    <?php echo text($setting->gst);?>
                                </span>
                            </div>
                        </div>
                        
                        <div class="col-md-6">                           
                            <div class="form-group">
                                <span class="first-coln">Contact Name</span>
                                <span class="seperator">:</span>
                                <span class="second-coln">
                                    <?php echo text($setting->contact_name);?>
                                </span>
                            </div>
                            
                            <div class="form-group">
                                <span class="first-coln">Contact Email</span>
                                <span class="seperator">:</span>
                                <span class="second-coln">
                                    <?php echo text($setting->contact_email);?>
                                </span>
                            </div>
                            
                            <div class="form-group">
                                <span class="first-coln">Contact Number</span>
                                <span class="seperator">:</span>
                                <span class="second-coln">
                                    <?php echo text($setting->contact_no);?>
                                </span>
                            </div>
                            
                            <div class="form-group">
                                <span class="first-coln">Contact Fax</span>
                                <span class="seperator">:</span>
                                <span class="second-coln">
                                    <?php echo text($setting->fax);?>
                                </span>
                            </div>
                           
                            <div class="form-group">
                                <span class="first-coln">Website Url</span>
                                <span class="seperator">:</span>
                                <span class="second-coln">
                                    <?php echo text($setting->website_url);?>
                                </span>
                            </div>
                            
                            <div class="form-group">
                                <span class="first-coln">Logo</span>
                                <span class="seperator">:</span>
                                <span class="second-coln">
                                    <?php
                                    if ((!empty($setting->logo)) && (file_exists(FCPATH . DIR_COMPANY_LOGO . $setting->logo))) {
                                        $image_url = BASE_URL . DIR_COMPANY_LOGO . $setting->logo;
                                        $alt = $setting->comp_name;
                                        $style="width: 60px;";
                                    }
                                    else {
                                        $image_url = BASE_URL . NO_IMG;
                                        $alt = 'No image not found!';
                                        $style="width: 60px;";
                                    }
                                    ?>
                                    <img src="<?php echo $image_url;?>" alt="<?php echo $alt;?>" style="<?php echo $style;?>" />
                                </span>
                            </div>
                            
                            <div class="form-group">
                                <span class="first-coln">Favicon</span>
                                <span class="seperator">:</span>
                                <span class="second-coln">
                                    <?php
                                    if ((!empty($setting->favicon)) && (file_exists(FCPATH . DIR_FAVICON . $setting->favicon))) {
                                        $image_url = BASE_URL . DIR_FAVICON . $setting->favicon;
                                        $alt = $setting->comp_name;
                                    }
                                    else {
                                        $image_url = BASE_URL . DEFAULT_FAVICON;
                                        $alt = 'No image not found!';
                                    }
                                    ?>
                                    <img src="<?php echo $image_url;?>" alt="<?php echo $alt;?>" />
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="box-footer float-clear"></div>
                </div>
            </div>
        </div><!-- /.row -->
    </section><!-- /.content -->
</aside>