<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->
        <?php
        echo breadcrumb_links();
        $user_data = get_user_data();
        ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) {
                    ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>

        <form class="edit_form" action="<?php echo $form_action; ?>" method="post" name="purchase_receive" id="purchase_receive_form">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                        <a href="<?php echo base_url(); ?>goods_receive/" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="List"><i class="glyphicon glyphicon-list"></i></button></a>
                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">

                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Goods Receive Edit </h2>
                            </header>

                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" >
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                            <input type="hidden" class="form-control grn_id" name="grn_id" value="<?php echo $grn->id; ?>" />   
                                            <input type="hidden" class="form-control sup_name" id="sup_name" name="sup_name" value="<?php echo $grn->sup_id; ?>" />   
                                            <input type="hidden" class="form-control supplier_gst" name="supplier_gst" value="<?php echo $grn->gst_type; ?>" />
                                            <div class="row">   
                                                <section class="col col-6">
                                                                                                        <!--<section>
                                                        <label class="label">Goods Receive No. : <b style="color: #3276B1;"><?php echo $grn->pr_code; ?></b></label>
                                                    </section>-->
                                                    <section>
                                                        <label class="label">Supplier Name<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <input type="text" name="supplier_name" id="supplier_name" class="form-control supplier_name" value="<?php echo $grn->name; ?>" readonly="" />
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Supplier Invoice Number<span style="color: red;"></span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-phone"></i>
                                                            <input type="text" name="sup_invoice" id="sup_invoice" class="form-control sup_invoice" value="<?php echo $grn->supplier_invoice_no; ?>"/>
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Your Information<span style="color: red;"></span></label>
                                                        <label class="textarea">
                                                            <textarea rows="3" class="custom-scroll your_info" name="your_info"><?php echo $grn->pr_your_info; ?></textarea>
                                                        </label>
                                                    </section>
                                                </section>

                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Goods Receive Date<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="pr_date" id="pr_date" class="form-control bootstrap-datepicker-comncls" value="<?php echo date('d/m/Y', strtotime($grn->pr_date)); ?>" />
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Invoice Date<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="inv_date" id="inv_date" class="form-control bootstrap-datepicker-comncls" value="<?php echo date('d/m/Y', strtotime($grn->invoice_date)); ?>" />
                                                        </label>
                                                    </section>
                                                </section>
                                            </div>

                                            <fieldset>
                                                <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Items Details</legend>
                                                <div class="row product_search">
                                                    <section class="col col-5">
                                                        <div class="input hidden-mobile">
                                                            <input name="search_product" class="form-control search_product lowercase" type="text" data-placeholder="Scan / Search Item by code or description" id="search_product" tabindex="-1" />
                                                        </div>
                                                    </section>
                                                </div>
                                                <div class="row material-container product_details">
                                                    <!--                                                <div class="col-xs-12">
                                                                                                        <section class="col">
                                                                                                            <span class="new-window-popup btn btn-primary" data-iframe-src="<?php echo $add_item_link; ?>" data-cmd="item" data-title="Item Add">
                                                                                                                <i class="fa fa-plus-square-o" style="font-size: 16px;"> </i> Add Item
                                                                                                            </span>
                                                                                                        </section>
                                                                                                    </div>-->

                                                    <div class="items_head col-xs-12 clr">
                                                        <section class="col col-2">
                                                            <label class="label">
                                                                <b>Item Code</b>
                                                            </label>
                                                        </section>
                                                        <section class="col col-4">
                                                            <label class="label"><b>Brand Name - Description</b></label>
                                                        </section>
                                                        <!--<section class="col col-1">
                                                            <label class="label"><b>Act.Qty</b></label>
                                                        </section>-->
                                                        <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                            <label class="label"><b>Qty</b></label>
                                                        </section>

                                                        <?php $unit_cost = (in_array(7, $permission)); ?>
                                                        <section class="col col-<?php echo ($unit_cost) ? '1' : '2'; ?>" style="padding-left: 5px; padding-right: 5px;">
                                                            <label class="label"><b>Cost Price</b></label>
                                                        </section>

                                                        <?php if ($unit_cost) { ?>
                                                            <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                                <label class="label"><b>Selling Price</b></label>
                                                            </section>
                                                        <?php } ?>

                                                        <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                            <label class="label"><b>Discount</b></label>
                                                        </section>
                                                        <section class="col col-2" style="padding-left: 5px; padding-right: 5px;">
                                                            <label class="label"><b>Amount</b></label>
                                                        </section>
                                                    </div>
                                                    <?php $item_count = count($grn_item); ?>
                                                    <?php foreach ($grn_item as $key => $item) { ?>
                                                        <div class="items col-xs-12 clr">
                                                            <section class="col col-2">
                                                                <label class="input">
                                                                    <input type="hidden" name="grni_id[<?php echo $key; ?>]" class="grni_id" value="<?php echo $item->id; ?>" />
                                                                    <input type="hidden" name="category_id[<?php echo $key; ?>]" class="category_id" value="<?php echo $item->category_id; ?>" />
                                                                    <input type="hidden" name="product_id[<?php echo $key; ?>]" class="product_id" value="<?php echo $item->product_id; ?>" />
                                                                    <input type="text" name="product_code[<?php echo $key; ?>]" class="product_code" data-value="" value="<?php echo $item->pdt_code; ?>" />
                                                                </label>
                                                                <span class="product_code_label">Selected Item code already exist!</span>
                                                            </section>

                                                            <section class="col col-4">
                                                                <label class="input">
                                                                    <input type="text" name="product_description[<?php echo $key; ?>]" class="product_description" data-value="" value="<?php echo $item->product_description; ?>" />
                                                                </label>
                                                                <span class="error_description">Selected Item description already exist!</span>
                                                                <span class="item-serialnum-popup btn btn-primary" style="padding:3px 4px 3px 4px;margin-top: 2px;" title="Quantity should not be empty to enter Serial No.">
                                                                    <i class="fa fa-plus-square-o" style="font-size: 14px;"> </i> Serial No
                                                                </span>
                                                                <input type="hidden" class="item_sl_no" name="sl_no[<?php echo $key;?>]" value="<?php echo $item->sl_no;?>"/>
                                                                <span style="float: right; margin-top: 1px;">
                                                                    <b style="color: #333;font-size: 11px; display: inline-block;">Supplier<br>Item Code</b>
                                                                    <label class="input" style="float: right; width: 70%;">
                                                                        <input type="text" class="sl_itm_code" name="sl_itm_code[<?php echo $key;?>]" value="<?php echo $item->sl_itm_code;?>" />
                                                                    </label>
                                                                </span>
                                                            </section>

                                                                        <!--<section class="col col-1">
                                                                                <label class="input">
                                                                                        <input type="text" name="po_quantity1[0]" class="po_quantity1 float_value" data-placeholder="0"  readonly=""/>
                                                                                </label>
                                                                                <span class="unit_label"></span>
                                                                        </section>-->

                                                            <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                                <label class="input">
                                                                    <input type="text" name="quantity[<?php echo $key; ?>]" class="quantity float_value" value="<?php echo $item->pr_quantity; ?>" />
                                                                </label>
                                                                <span class="unit_label"></span>
                                                                <span style="float: right; margin-top: 1px;">
                                                                    <b style="color: #333;font-size: 11px;">Cost After GST</b>
                                                                </span>
                                                            </section>

                                                            <section class="col col-<?php echo ($unit_cost) ? '1' : '2'; ?>" style="padding-left: 5px; padding-right: 5px;">
                                                                <label class="input" style="margin-top: 1px;">
                                                                        <!--<i class="icon-prepend fa fa-dollar"></i>-->
                                                                    <input type="text" name="cost[<?php echo $key; ?>]" class="cost float_value" value="<?php echo $item->pr_unit_cost; ?>" />
                                                                </label>
                                                                <span style="float: right; margin-top: 1px;">
                                                                    <label class="input" style="float: right;">
                                                                        <input type="text" name="price_after_gst[<?php echo $key; ?>]" class="price_after_gst" value="<?php echo $item->pr_price_after_gst; ?>" readonly="" />
                                                                    </label>
                                                                </span>
                                                            </section>

                                                            <section class="col col-1" style="padding-left: 5px; padding-right: 5px; display: <?php echo ($unit_cost) ? 'block' : 'none'; ?>;">
                                                                <label class="input">
                                                                        <!--<i class="icon-prepend fa fa-dollar"></i>-->
                                                                    <input type="text" name="price[<?php echo $key; ?>]" class="price float_value" value="<?php echo $item->pr_price; ?>" />
                                                                </label>
                                                            </section>

                                                            <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">
                                                                <div class="input-group">
                                                                    <input type="text" name="item_discount[<?php echo $key; ?>]" class="form-control item_discount float_value" value="<?php echo ($item->discount_type == 1) ? $item->discount_percentage : $item->discount; ?>" style="height: 30px; padding-right: 5px;" />

                                                                    <input type="hidden" name="item_discount_amt[<?php echo $key; ?>]" class="item_discount_amt" value="<?php echo $item->discount; ?>" />

                                                                    <span>S$ <span class="item_discount_amt"><?php echo $item->discount; ?></span></span>

                                                                    <span class="onoffswitch" style="float: right;">
                                                                        <input type="checkbox" name="item_discount_type[<?php echo $key; ?>]" class="onoffswitch-checkbox item_discount_type" id="item_discount_type-<?php echo $key; ?>" value="1" <?php echo ($item->discount_type == 1) ? 'checked' : ''; ?> />

                                                                        <label class="onoffswitch-label" for="item_discount_type-<?php echo $key; ?>"> 
                                                                            <span class="onoffswitch-inner" data-swchon-text="%" data-swchoff-text="S$"></span> 
                                                                            <span class="onoffswitch-switch"></span> 
                                                                        </label> 
                                                                    </span>
                                                                </div>
                                                            </section>

                                                            <section class="col col-2" style="padding-left: 5px; padding-right: 5px;">
                                                                <label class="input" style="width: 65%; float: left;">
                                                                    <i class="icon-prepend fa fa-dollar"></i>
                                                                    <input type="text" name="amount[<?php echo $key; ?>]" class="amount float_value" value="<?php echo $item->total; ?>" readonly="" />
                                                                </label>

                                                                <label class="input" style="width: 35%; text-align: center; float: right; margin-top: 2px;">
                                                                    <?php if ($item_count == ($key + 1)) { ?>
                                                                        <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                                    <?php } ?>
                                                                    <?php if ($item_count > 1) { ?>
                                                                        <i class="fa fa-2x fa-minus-circle remove-row"></i>
                                                                    <?php } ?>
                                                                </label>
                                                            </section>
                                                        </div>
                                                    <?php } ?>
                                                </div>

                                                <div class="row" style="margin-top: 10px;">
                                                    <section class="col col-3">
                                                        <label class="checkbox">
                                                            <input type="checkbox" name="credit_notes" id="credit_notes" class="checkbox" value="1" <?php echo ($grn->credit_notes) ? 'checked' : ''; ?> /> <i></i>Credit Notes
                                                        </label>
                                                    </section>
                                                    <section class="col col-3">
                                                    </section>
                                                    <section class="col col-3">
                                                        <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Sub Total</b></label>
                                                    </section>
                                                    <section class="col col-2">
                                                        <label class="input">
                                                            <i class="icon-prepend fa fa-dollar"></i>
                                                            <input type="text" name="sub_total" class="sub_total" value="<?php echo $grn->pr_sub_total; ?>" readonly="" />
                                                            <input type="hidden" name="hidden_sub_total" class="hidden_sub_total" readonly="" value="<?php echo $grn->pr_sub_total; ?>" />
                                                        </label>
                                                    </section>
                                                    <section class="col col-1">
                                                    </section>
                                                </div>

                                                <div class="row">
                                                    <section class="col col-6">
                                                                                                            <!--<section class="row credit_amount_container" style="display: none;">
                                                                                                                    <section class="col col-3">
                                                                                                                            <label class="label">Credit Amount</label>
                                                                                                                    </section>
                                                                                                                    <section class="col col-5">
                                                                                                                            <label class="input">
                                                                                                                                    <i class="icon-prepend fa fa-dollar"></i>
                                                                                                                                    <input type="text" name="credit_amount" id="credit_amount" value="<?php echo $grn->credit_amount; ?>" />
                                                                                                                            </label>
                                                                                                                    </section>
                                                                                                            </section>-->
                                                    </section>
                                                    <section class="col col-3">
                                                        <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Discount</b></label>
                                                    </section>
                                                    <section class="col col-2">
                                                        <!--<label class="input">
                                                            <i class="icon-prepend fa fa-dollar"></i>
                                                            <input type="text" name="sub_total" class="sub_total" readonly="" data-placeholder="0.00" />
                                                        </label>-->

                                                        <div class="input-group">
                                                            <input type="text" name="discount" class="form-control discount float_value" value="<?php echo $grn->pr_discount; ?>" />
                                                            <span class="input-group-addon">
                                                                <span class="onoffswitch">
                                                                    <input type="checkbox" name="discount_type" class="onoffswitch-checkbox discount_type" id="discount_type-0" value="1" <?php echo ($grn->pr_discount_type == '1') ? 'checked' : ''; ?> />
                                                                    <label class="onoffswitch-label" for="discount_type-0"> 
                                                                        <span class="onoffswitch-inner" data-swchon-text="%" data-swchoff-text="S$"></span> 
                                                                        <span class="onoffswitch-switch"></span> 
                                                                    </label> 
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <input type="hidden" name="discount_amt" class="discount_amt" value="<?php echo $grn->pr_total_discount; ?>" />
                                                        <span class="discount_container">
                                                            <span class="currency">S$</span>
                                                            <span class="discount_amount"><?php echo $grn->pr_total_discount; ?></span>
                                                        </span>
                                                    </section>
                                                    <section class="col col-1">
                                                    </section>
                                                </div>

                                                <div class="row gst_details">
                                                   <section class="col col-5">&nbsp;</section>
                                                   <section class="col col-4" style="padding: 0px 55px;">
                                                        <section>
                                                            <div class="inline-group">
                                                                <label class="radio">
                                                                    <input type="radio" class="gst_type wogst" name="gst_type" value="1" <?php echo ($grn->pr_gst_type == '1') ? 'checked' : ''; ?> /><i></i>Include GST
                                                                </label>
                                                                <label class="radio">
                                                                    <input type="radio" class="gst_type wgst" name="gst_type" value="2"  <?php echo ($grn->pr_gst_type == '2') ? 'checked' : ''; ?> /><i></i>Exclude GST
                                                                </label>
                                                                <input type="hidden" name="gst_type_hidden" class="gst_type_hidden" value="<?php echo $grn->pr_gst_type; ?>"> 
                                                                <input type="hidden" name="disc_type_hidden" class="disc_type_hidden" value="<?php echo $grn->pr_discount_type; ?>"> 
                                                           

                                                            </div>
                                                        </section>
                                                    </section>
                                                    <section class="col col-2">                                                        
                                                        <label class="input change_perc_disamt"><i class="icon-prepend fa fa-dollar"></i>
                                                            <input type="text" name="gst_amount" class="gst_amount float_value" readonly="" value="<?php echo $grn->pr_gst_amount; ?>" />
                                                            <input type="hidden" value="<?php echo $company->gst_percent; ?>" name="hidden_gst" id="hidden_gst" />
                                                        </label>
                                                        <span class="gst_val" style="display: <?php echo (!empty($company->gst_percent)) ? 'block' : 'none'; ?>">GST @ <span class="gst_percentage"><?php echo $company->gst_percent; ?></span> %</span>
                                                    </section>
                                                </div>

                                                <div class="row">
                                                    <section class="col col-6">
                                                    </section>
                                                    <section class="col col-3">
                                                        <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Total Amount</b></label>
                                                    </section>
                                                    <section class="col col-2">
                                                        <label class="input">
                                                            <i class="icon-prepend fa fa-dollar"></i>
                                                            <input type="text" name="total_amt" class="total_amt" value="<?php echo $grn->pr_net_amount; ?>" readonly="" />
                                                        </label>
                                                    </section>
                                                    <section class="col col-1">
                                                    </section>
                                                </div>
                                            </fieldset>                                         

                                            <footer>
                                                <button type="submit" class="btn btn-primary" >Update</button>
                                                <button type="reset" class="btn btn-default" >Cancel</button>
                                            </footer>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                    <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/goods_receive.js"></script>