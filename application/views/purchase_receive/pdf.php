    <?php $height = 440; ?>
    <table width="100%" border="1" style="border-collapse:collapse; font-family: calibri;" cellpadding="5" cellspacing="0">
        <thead style="font-size: 13px;">
            <tr>
                <th width="15%" style="background:#EBEBEB;">Item Code</th>
                <th width="30%" style="background:#EBEBEB;">Description</th>                   
                <th width="15%" style="background:#EBEBEB;">Quantity</th>                    
                <th width="15%" style="background:#EBEBEB;">Cost Price</th>
				<th width="10%" style="background:#EBEBEB;">Discount</th>
                <th width="15%" style="background:#EBEBEB;">Amount</th>
            </tr>
        </thead>
		<tfoot>
			<tr>
				<td colspan="6" style="border-top: 1;"></td>
			</tr>
		</tfoot>
		<tbody style="font-size: 11px;">
        <?php foreach ($grn_item as $key => $item) { ?>
            <tr style="vertical-align:top;">
                <td style="border-top: 0; border-bottom: none;"><?php echo strtoupper($item->pdt_code); ?></td>
                <td style="border-top: 0;border-bottom: none;"><?php echo nl2br(strtoupper($item->product_description)); ?>
                    <?php if(!empty($item->sl_itm_code)) { ?><br /><b>Supplier Item Code</b>&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;
                    <span><?php echo $item->sl_itm_code; ?></span> <?php } ?>
                    <?php if(!empty($item->sl_no)) { ?><br /><b>Serial No</b>&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;
                    <span><?php echo $item->sl_no; ?></span> <?php } ?>
                </td>
                <td align="center" style="border-top: 0;border-bottom: none;"><?php echo $item->pr_quantity; ?> </td>
                <td align="right" style="border-top: 0;border-bottom: none;"><?php echo $item->pr_unit_cost; ?></td>
                <td align="right" style="border-top: 0;border-bottom: none;"><?php echo $item->discount; ?></td>
                <td align="right" style="border-top: 0;border-bottom: none;"><?php echo $item->total; ?></td>
            </tr>
            <?php $height -= 75; ?>
        <?php } ?>
        <?php $height -= ($grn->pr_discount > 0) ? 30 : 0; ?>
        <?php $height -= ($grn->pr_gst_amount > 0) ? 30 : 0; ?>
		<?php if ($height > 0) {?>
			<tr>
				<td height="<?php echo $height; ?>" style="border-top: 0;"></td>
				<td style="border-top: 0;"></td>
				<td style="border-top: 0;"></td>
				<td style="border-top: 0;"></td>
				<td style="border-top: 0;"></td>
				<td style="border-top: 0;"></td>
			</tr>
		<?php }?>
        <!--</table>-->

        <?php
        //$row_span = ($grn->gst_amount > 0) ? 3 : 2;
        $row_span = 2;
        if ($grn->pr_gst_amount > 0)
            $row_span ++;
        else if ($grn->pr_discount > 0)
            $row_span ++;
        ?>
    <!--<table width="100%" style="border-collapse:collapse; border-top:none;" cellpadding="5">-->
        <tr>
            <td align="left" style="vertical-align: top; border-left: 1px solid #000; border-bottom: none; border-right: none; padding-right: 10px;" rowspan="<?php echo $row_span;?>" colspan="3">
                <?php echo (!empty($grn->pr_your_info)) ? '<p style="font-size: 12px;"><b style="font-size: 12px;">Remarks</b> <br />' . strtoupper($grn->pr_your_info) . '</p>' : ''; ?>
            </td>
            <td colspan="2" style="vertical-align: top; padding: 0; margin: 0; border-left: 1px solid #000; border-right: 1px solid #000;padding-left: 15px;">Sub Total</td>
			<td><?php echo $grn->pr_sub_total; ?></td>
        </tr>
		
		<?php if ($grn->pr_discount > 0) { ?>
			<tr>
				<td colspan="2" style="padding-left: 15px;">Discount  <?php echo ($grn->pr_discount_type == 1 && $grn->pr_discount > 0) ? '' . $grn->pr_discount . '%' . '' : ''; ?></td>
				<td><?php echo $grn->pr_total_discount; ?></td>
			</tr>
		<?php } ?>
		
		<?php if ($grn->pr_gst_amount > 0) { ?>
			<tr>
				<td colspan="2" style="padding-left: 15px;"><?= ($grn->pr_gst_type == '2') ? "ADD" : ""; ?> GST <?php echo $grn->pr_gst_val; ?> %</td>
				<td><?php echo $grn->pr_gst_amount; ?></td>
			</tr>
		<?php } ?>
		<tr>
			<td colspan="2" style="padding-left: 15px; border-bottom: none; padding-bottom: 0;">Total</td>
			<td style="border-bottom: none; padding-bottom: 0;"><?php echo $grn->pr_net_amount; ?></td>
		</tr>
    </table>