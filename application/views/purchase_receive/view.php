<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php
        echo breadcrumb_links();
        $user_data = get_user_data();
        ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>goods_receive/" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="List"><i class="glyphicon glyphicon-list"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                        <header>
                            <h2>Goods Receive View</h2>
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->


                            <div class="widget-body">
                                <!--<span id="watermark">Malar ERP.</span>-->
                                <table id="user" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
										<?php if (!empty($grn_details->po_no)) {?>
											<tr>
												<td style="font-weight: bold;">Purchase Order</td>
												<td ><?php echo $grn_details->po_no; ?></td>
											</tr>
										<?php }?>
										<tr>
                                            <td style="font-weight: bold;">SUPPLIER INVOICE NUMBER</td>
                                            <td ><?php echo text($grn_details->supplier_invoice_no); ?></td>
                                        </tr>
										<tr>
                                            <td style="font-weight: bold;">Goods Receive Date</td>
                                            <td ><?php echo text_date($grn_details->pr_date); ?></td>
                                        </tr>
										<tr>
                                            <td style="font-weight: bold;">Invoice Date</td>
                                            <td ><?php echo text_date($grn_details->invoice_date); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">YOUR INFORMATION</td>
                                            <td ><?php echo text($grn_details->pr_your_info); ?></td>
                                        </tr>
										<tr>
                                            <td style="font-weight: bold;">Currency</td>
                                            <td ><?php echo text($grn_details->currency_str); ?></td>
                                        </tr>
										<?php if($grn_details->currency == 2) {?>
											<tr>
												<td style="font-weight: bold;">Exchange Rate</td>
												<td ><?php echo text($grn_details->exchange_rate); ?></td>
											</tr>
										<?php }?>
										
										<tr>
                                            <td style="font-weight: bold;">Payment Status</td>
                                            <td ><?php echo text($grn_details->payment_status); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Status</td>
                                            <td ><?php echo ($grn_details->received_status == 1) ? 'Received' : 'Not Received'; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="color: #A90329;"><b>Supplier Details</b></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; padding-left: 25px;">Name</td>
                                            <td ><?php echo text($grn_details->name); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; padding-left: 25px;">Contact Person</td>
                                            <td ><?php echo text($grn_details->contact_name); ?></td>
                                        </tr>

                                        <tr>
                                            <td style="font-weight: bold; padding-left: 25px;">Contact No</td>
                                            <td ><?php echo text($grn_details->phone_no); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; padding-left: 25px;">Address</td>
                                            <td ><?php echo nl2br($grn_details->address); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
								
								<div class="text-left">
                                    <span class="onoffswitch-title">
                                        <h3><b style="color: #8e233b;">Items Details </b></h3>
                                    </span>
                                </div>
                                <div id="widget-tab-1" class="table-responsive">
                                    <table id="dt_basic_2" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
										<thead>
											<tr>
												<th data-class="expand" style="font-weight: bold; text-align:center;">S.No</th>
												<th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Item Code</th>
												<th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Item Description</th>
												<th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Quantity</th>
												<?php $cols = 5;
												if (in_array(7, $user_data['permissions']['goods_receive'])) {
													$cols ++; ?>
													<th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Selling Price</th>
												<?php } ?>
												<th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Cost after GST</th>
												<th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Cost Price</th>
												<th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Discount</th>
												<th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Total (S$)</th>
											</tr>
										</thead>
										<tbody>
											<?php
											//$grn_item = $this->purchase_receive_model->get_item_by_id($grn_details->id);
											$total_item_amt = 0;
											$total_discount = 0;
											$total_unit_cost = 0;
											foreach ($grn_item as $key => $item) { ?>
												<?php $total_item_amt += $item->total;?>
												<?php $total_discount += $item->discount;?>
												<?php $total_unit_cost += $item->pr_unit_cost;?>
												<tr>
													<td><?php echo ++$key; ?></td>
													<td><?php echo $item->pdt_code; ?></td>
													<td><?php echo $item->product_description; ?>
													<?php if (!empty($item->sl_itm_code)) {?>
                                                                                                            <br />
													    <b>Supplier Item Code</b>&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;
                                                                                                            <span><?php echo $item->sl_itm_code; ?></span>
													<?php }?>
													<?php if (!empty($item->sl_no)) {?>
                                                                                                            <br />
													    <b>Serial No</b>&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;
                                                                                                            <span><?php echo $item->sl_no; ?></span>
													<?php }?>
                                                                                                        </td>
													<td align="center"><?php echo $item->pr_quantity; ?></td>
													<?php if (in_array(7, $user_data['permissions']['goods_receive'])) { ?>
														<td align="right"><?php echo $item->pr_price; ?></td>
													<?php } ?>
													<td align="right"><?php echo $item->pr_price_after_gst; ?></td>
													<td align="right"><?php echo $item->pr_unit_cost; ?></td>
													<td align="right"><?php echo text_amount($item->discount); ?></td>
													<td align="right"><?php echo $item->total; ?></td>
												</tr>
											<?php } ?>
										</tbody>
										<tfoot>
											<tr>
												<td align="right" colspan="<?php echo $cols; ?>"><b>Total</b></td>
												<td align="right"><?php echo number_format(($total_unit_cost), 2, '.', '');?></td>
												<td align="right"><?php echo number_format(($total_discount), 2, '.', '');?></td>
												<td align="right"><?php echo number_format(($total_item_amt), 2, '.', '');?></td>
											</tr>
											
											<?php if($grn_details->pr_total_discount > 0){?>
												<tr>
													<td colspan="<?php echo $cols; ?>" align="right"><b>
														Discount (<?php echo ($grn_details->pr_discount_type != '1') ? 'S$' : $grn_details->pr_discount.' %';?>)</b>
													</td>
													<td align="right" colspan="3">
														<?php echo number_format($grn_details->pr_total_discount, 2, '.', ''); ?>
													</td>
												</tr>
											<?php }?>
											
											<?php if ($grn_details->pr_gst_type == '2' && $grn_details->gst_type != '2') { ?>
												<tr>
													<td colspan="<?php echo $cols; ?>" align="right"><b>Total Amount</b></td>
													<td align="right" colspan="3"><?php echo number_format(($grn_details->pr_sub_total - $grn_details->pr_total_discount), 2, '.', ''); ?></td>
												</tr>
												
												<tr>
													<td colspan="<?php echo $cols; ?>" align="right"><b>Add GST <?php echo $grn_details->pr_gst_val; ?>%</b></td>
													<td align="right" colspan="3"><?php echo number_format($grn_details->pr_gst_amount, 2, '.', ''); ?></td>
												</tr>
											<?php } ?>
											
											<tr>
												<td colspan="<?php echo $cols; ?>" align="right"><b>Net Amount</b></td>
												<td align="right" colspan="3"><?php echo $grn_details->pr_net_amount; ?></td>
											</tr>
											<?php if ($grn_details->credit_notes) {?>
												<tr>
													<td colspan="<?php echo $cols; ?>" align="right"><b>Credit Note</b></td>
													<td align="right" colspan="3"><?php echo ($grn_details->credit_notes) ? 'Yes' : 'No'; ?></td>
												</tr>
											<?php }?>
											<?php if ($grn_details->pr_gst_type == '1' && $grn_details->gst_type != '2') { ?>
												<tr>
													<td colspan="<?php echo $cols + 3;?>"><b>[INCLUSIVE OF GST <?php echo $grn_details->pr_gst_val; ?> % : $ <?php echo number_format($grn_details->pr_gst_amount, 2, '.', ''); ?>]%</b></td>
												</tr>
											<?php } ?>
										</tfoot>
									</table>
                                </div>
                            </div>
							<!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->

<script type="text/javascript">

    // DO NOT REMOVE : GLOBAL FUNCTIONS!

    $(document).ready(function () {

        /*
         * ACCORDION
         */
        //jquery accordion

        /*var accordionIcons = {
            header: "fa fa-plus", // custom icon class
            activeHeader: "fa fa-minus" // custom icon class
        };

        $("#accordion").accordion({
            autoHeight: false,
            heightStyle: "content",
            collapsible: true,
            animate: 300,
            icons: accordionIcons,
            header: "h4",
        });*/

        $(".smart-mod-delete").click(function (e) {
            var $bindId = $(this).data('bindid');
            var $bindText = $(this).data('bindtext');
            $.SmartMessageBox({
                title: "Are you sure?",
                content: "You will not be able to recover this Goods Receive Details!",
                buttons: '[No][Yes]'
            }, function (ButtonPressed) {
                if (ButtonPressed === "Yes") {
                    window.location.replace(baseUrl + "goods_receive/delete/" + $bindId);
                }
                if (ButtonPressed === "No") {
                    $.smallBox({
                        title: "Sorry ! ...",
                        content: "<i class='fa fa-info-circle'></i> <i>You just selected NO...<br>So The action of DELETE is disallowed for the " + $bindText + " details.. </i>",
                        color: "#C46A69",
                        iconSmall: "fa fa-times fa-2x fadeInRight animated",
                        timeout: 4000
                    });
                }
            });
            e.preventDefault();
        })


        /* BASIC ;*/
//             var count_item = $('.accordon_count').val();
//             for(var i = 1; i <= count_item; i++) {
//                 var responsiveHelper_dt_basic_1 = undefined;
//                 var responsiveHelper_dt_basic_2 = undefined;
//                 var responsiveHelper_dt_basic_3 = undefined;
//                 var responsiveHelper_dt_basic_i = undefined;
//                 var breakpointDefinition = {
//                    tablet : 1024,
//                    phone : 480
//                 };
//
//                 $('#dt_basic_'+ i).dataTable({
//                    "sDom": "",
//                    "autoWidth" : true,
//                    "preDrawCallback" : function() {
//                        // Initialize the responsive datatables helper once.
//                        if (!responsiveHelper_dt_basic_i) {
//                            responsiveHelper_dt_basic_i = new ResponsiveDatatablesHelper($('#dt_basic_' + i), breakpointDefinition);
//                        }
//                    },
//                    "rowCallback" : function(nRow) {
//                        responsiveHelper_dt_basic_i.createExpandIcon(nRow);
//                    },
//                    "drawCallback" : function(oSettings) {
//                        responsiveHelper_dt_basic_i.respond();
//                    }
//                });
//             }
    });

</script>