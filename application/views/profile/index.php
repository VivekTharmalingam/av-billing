<?php
/*
 * Author Name      : Jai K
 * Purpose          : The main view page for Profile 
 * Created On       : 2016-04-23 19:46
 */

?>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <?php 
            /*
             * Breadcrumbs
             */
            echo breadcrumb_links(); 
        ?>
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if( !empty( $success ) ) : ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                            <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                            <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php elseif( !empty( $error ) ) : ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error;?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>

        <form class="" action="<?php echo $form_action; ?>" method="post" name="profile_form" id="profile_form" enctype="multipart/form-data">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-sm-12">	
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">

                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Profile</h2>
                                <!-- <span style=" float: right;padding:7px;" class="btn dialog-btn" ><i class="fa fa-key"></i> Change Password</span> -->
                            </header>

                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">

                                    <div class="smart-form" >

                                        <fieldset>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Name</label>
                                                        <label class="input">
                                                            <input type="text" class="form-control" name="name" data-placeholder="Paul Jacob" maxlength="50" value="<?php echo $user_data['user']['name']; ?>" />
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">User Name</label>
                                                        <label class="input">
                                                            <input type="text" class="form-control" name="user_name" data-placeholder="jacob" maxlength="50" value="<?php echo $user_data['user']['user_name']; ?>" />
                                                        </label>
                                                    </section>
                                                    
                                                    <section>
                                                        <label class="label">Password</label>
                                                        <label class="input">
                                                            <input type="password" class="form-control" name="password" maxlength="50" value="<?php echo $user_data['user']['password']; ?>"/>
                                                        </label>
                                                    </section>

                                                </section>

                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Email</label>
                                                        <label class="input">
                                                            <input type="text" class="form-control" name="email" data-placeholder="jacob@yahoo.com" maxlength="50" value="<?php echo $user_data['user']['email']; ?>"/>
                                                        </label>
                                                    </section>
                                                    
                                                    <section>
                                                        <label class="label">Profile Photo</label>
                                                        <div class="parallel thumb preview" style="width: 9%; position: absolute; width:50px; height:45px;" >
                                                            <img src="<?php echo ( !empty( $user_data['user']['image']  ) ? BASE_URL . UPLOADS . $user_data['user']['image'] : BASE_URL . DEFAULT_AVATAR ); ?>" />
                                                        </div>
                                                        <label class="" style="width: 50%; margin-left: 10%;">
                                                            <input type="file" class="form-control parallel" name="file" data-actual="<?php echo $user_data['user']['image']; ?>" data-url="<?php echo $remove_link . 'remove/image/' . $user_data['user']['id']; ?>"  data-default="<?php echo BASE_URL . DEFAULT_AVATAR; ?>" value="" style=" width: 176%; padding: 10px 0px 0px 10px;"/>
                                                        </label>
                                                        
                                                    </section>
                                                    
                                                </section>    
                                            </div>
                                        </fieldset>
                                        
                                        <footer>
                                            <button type="submit" name="submit" class="btn btn-primary" >Update</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>

                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                    <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>

    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/app/users.js"></script>
