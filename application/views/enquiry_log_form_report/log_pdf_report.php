<?php //print_r($from_date);exit;?>
<table border="0" cellpadding="3" cellspacing="0" style="font-family: calibri; font-size:14px; width: 1000px;">
    <?php echo $head;?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table><br>
<table border="0" cellpadding="3" cellspacing="0" style="font-family: calibri; font-size:14px; width: 1000px;">
    <tr>
        <td>
            <table border="1" style="border-collapse:collapse;">
                <tr>
                    <td align="left" width="50%">Company Name</td>
                    <td align="left" width="50%"><?php echo text($enquiry_log->name); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="left" width="50%">Contact Person</td>
                    <td align="left" width="50%"><?php echo text($enquiry_log->contact_person); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="left" width="50%">Contact No</td>
                    <td align="left" width="50%"><?php echo text($enquiry_log->contact_no); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="left" width="50%">Email Address</td>
                    <td align="left" width="50%"><?php echo text($enquiry_log->email); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="left" width="50%">Type</td>
                    <td align="left" width="50%"><?php if($enquiry_log->type == 1){
                            echo 'Sales';
                    }elseif($enquiry_log->type == 2) {
                            echo 'Purchase';
                    }elseif($enquiry_log->type == 3) {
                            echo 'Accounts';
                    }elseif($enquiry_log->type == 4) {
                            echo 'Promotions';
                    } ?></td>
                </tr>
            </table>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="left" width="50%">Enquiry By</td>
                    <td><?php if($enquiry_log->enquiry_by == 1){
                            echo 'Phone';
                    }elseif($enquiry_log->enquiry_by == 2) {
                            echo 'Email';
                    }?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="left" width="50%">Assigned To</td>
                    <td align="left" width="50%">
                        <?php 
                            $usr_names = '';
                            $usr_id = explode(',', $enquiry_log->assigned_to );
                            $tot_count = count($usr_id);
                            $int_val = 1;
                            foreach( $user as $key => $value ) : 
                                if( in_array( $value->id, $usr_id  ) ) :
                                    if($int_val == $tot_count) :
                                        $usr_names .= $value->name ;
                                    else :
                                        $usr_names .= $value->name .',<br />';
                                    endif;
                                    $int_val++;
                                endif;
                            endforeach;
                            echo $usr_names;
                        ?>
                    </td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="left" width="50%">Picked By</td>
                    <td><?php if($enquiry_log->picked_status == 1){
                            echo 'Not Picked';
                    }elseif($enquiry_log->picked_status == 2) {
                            echo 'Picked';
                    }?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="left" width="50%">Enquiry Status</td>
                    <td align="left" width="50%"><?php echo text($enquiry_log->enq_status_str); ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>
<div class="col-md-12">
<div class="col-md-6" style="max-height: 500px;overflow-y: auto;overflow-x: hidden;">
    <?php foreach ($past_log_details as $key => $row) { ?>
        <div class="col-md-12" style="border-radius: 5px; background-color: #FFF; margin-bottom: 5px;">
                <div style="padding-top: 10px;padding-bottom: 10px; padding-left: 10px; margin-left: 0px;" class="col-md-12"><b>Log Description : </b>&nbsp;<?php echo text($row->log_description); ?></div>
            <div class="col-md-12" style="margin-left: 0px; padding-bottom: 6px; padding-left: 10px; background-color: rgba(204, 204, 204, 0.2);">
                <div class="col-md-6"><b style="font-style: italic">Created On :</b>&nbsp;&nbsp;<span style="font-style: italic"><?php echo date('d-m-Y h:i:s A',strtotime($row->created_date)); ?></span></div>
            </div>
        </div>
    <?php }?>
</div>
</div>