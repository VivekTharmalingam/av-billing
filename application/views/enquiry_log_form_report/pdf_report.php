<?php //print_r($from_date);exit;?>
<table border="0" cellpadding="3" cellspacing="0" style="font-family: calibri; font-size:14px; width: 1000px;">
    <?php echo $head;?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>
<!--                <tr style="boder-top:0px;">
                    <td align="right" width="50%">Assigned To</td>
                    <td align="right" width="50%"><?php echo text($assign_to, '--'); ?></td>
                </tr>-->
<!--                <tr style="boder-top:0px;">
                    <td align="right" width="50%">Enq Status</td>
                    <td align="right" width="50%"><?php echo text($enq_sts_str, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">Type</td>
                    <td align="right" width="50%"><?php echo text($type_str, '--'); ?></td>
                </tr>-->
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>
<table width="100%" border="1" style="font-family: calibri; border-collapse:collapse;" cellpadding="3">
	<thead>
        <tr>
            <th width="5%">S.No</th>
            <th width="20%"> Company Name</th>
            <th width="20%">Contact Person</th>
            <th width="15%">Type</th>
            <th width="15%">Assigned To</th>
            <th width="15%">Picked By</th>
            <th width="15%">Enq Status</th>
        </tr>
	</thead>
	<tbody>
	<?php if (count($enq_log_report)) {?>
	<?php foreach($enq_log_report as $key => $row) {?>  
		<tr>
                    <td><?php echo ++$key; ?></td>
                    <td><?php echo text($row->cus_name); ?></td>
                    <td><?php echo text($row->contact_person); ?></td>
                    <td><?php echo text($row->type); ?></td>
                    <td><?php echo text($row->assigned_to); ?></td>
                    <td><?php echo text($row->picked_user); ?></td>
                    <td><?php echo text($row->enq_sts); ?></td>
		</tr>
	<?php }?>
	<?php }
	else {?>
		<tr>
                    <td colspan="7" align="center">No Record Found</td>
		</tr>
	<?php }?>
	</tbody>
</table>