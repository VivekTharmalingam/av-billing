<?php
$ti=time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Enquiry_log_list_report".$ti.".xls");
header("Pragma: no-cache");
header("Expires: 0");
#echo "<pre>";print_r($item);echo "</pre>";
#echo "</br> : ".count($item);


?>
<table width="100%" border="1" style="border-collapse:collapse; margin-left:10px" cellpadding="5" cellspacing="0">
	<tr style="font-family:Times new roman;font-weight:bold;">
            <th width="5%" style="padding:10px">S.NO</th>
            <th width="20%"> Company Name</th>
            <th width="20%">Contact Person</th>
            <th width="15%">Type</th>
            <th width="15%">Assigned To</th>
            <th width="15%">Picked By</th>
            <th width="15%">Enq Status</th>
	</tr>
	<?php if (count($enq_log_report)) {?>
	<?php foreach($enq_log_report as $key => $row) {?> 
            <tr>
                <td><?php echo ++$key; ?></td>
                <td><?php echo text($row->cus_name); ?></td>
                <td><?php echo text($row->contact_person); ?></td>
                <td><?php echo text($row->type); ?></td>
                <td><?php echo text($row->assigned_to); ?></td>
                <td><?php echo text($row->picked_user); ?></td>
                <td><?php echo text($row->enq_sts); ?></td>
            </tr>
	<?php }?>
	<?php } else {?>
		<tr>
			<td colspan="7" align="center">No Record Found</td>
		</tr>
	<?php }?>
</table>