<script>
    window.localStorage.clear();
</script>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">&nbsp;</div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="widget-body">
                        <!-- Widget ID (each widget will need unique ID)-->                                
                        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false"	>

                            <header><span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                <h2>Enquiry Log Form Report</h2>
                            </header>                                  
                            <div>
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="box-body table-responsive">
                                            <div class="date-range"></div>
                                                <form name="order_search" action="<?php echo $form_action; ?>" method="post" class="search_form validate_form">
                                                    <div class="col-xs-12">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label style="width:28%; padding-top: 5px;"></label>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                    <input type="text" class="search_date" data-placeholder="dd/mm/yyyy - dd/mm/yyyy" id="date_alloc" style="padding: 6px 12px;margin-right:5%; width: 100%;"  id="search_date" name="search_date" value="<?php echo (!empty($search_date)) ? $search_date:''; ?>" autocomplete="off" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label style="width:28%; padding-top: 5px;"></label>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-user-circle-o"></i>
                                                                    </div>
                                                                    <select name="assigned_to" class="select2 assigned_to">
                                                                        <option value="">Assigned To - ALL</option>
                                                                        <?php foreach($users as $key => $user) {?>
                                                                                <option value="<?php echo $user->id;?>" <?php echo ($user->id == $assign_to)? 'selected' : '';?>><?php echo $user->name;?></option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label style="width:28%; padding-top: 5px;"></label>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-flag-o"></i>
                                                                    </div>
                                                                    <select name="enq_status" class="select2 enq_status">
                                                                        <option value="">Enquiry Status - ALL</option>
                                                                        <option value="1" <?php echo ($enq_sts == 1)?'selected':'';?>>Success</option>
                                                                        <option value="2" <?php echo ($enq_sts == 2)?'selected':'';?>>Process</option>     
                                                                        <option value="3" <?php echo ($enq_sts == 3)?'selected':'';?>>Cancel</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label style="width:28%; padding-top: 5px;"></label>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-building"></i>
                                                                    </div>
                                                                    <select name="branch" class="select2 branch">
                                                                        <option value="">Branch - ALL</option>
                                                                        <?php foreach($branches as $key => $branch) {?>
                                                                                <option value="<?php echo $branch->id;?>" <?php echo ($branch->id == $branch_id)? 'selected' : '';?>><?php echo $branch->branch_name;?></option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label style="width:28%; padding-top: 5px;"></label>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </div>
                                                                    <select name="type" class="select2 type">
                                                                        <option value="">Type - All</option>
                                                                        <option value="1" <?php echo ($type == 1)?'selected':'';?>>Sales</option>
                                                                        <option value="2" <?php echo ($type == 2)?'selected':'';?>>Purchase</option>     
                                                                        <option value="3" <?php echo ($type == 3)?'selected':'';?>>Accounts</option>
                                                                        <option value="4" <?php echo ($type == 4)?'selected':'';?>>Promotions</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label style="width:28%; padding-top: 5px;"></label>
                                                                <div class="input-group">
                                                                    <input type="submit" name="submit" value="Reset" id="reset" class="btn btn-success btn-warning" />&nbsp;&nbsp;<input type="submit" name="submit" value="Search" class="btn btn-success btn-primary"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1" style="float: right;" >
                                                            <div class="form-group">
                                                                <label style="width:28%; padding-top: 5px;"></label>
                                                                <div class="input-group">
                                                                    <input type="submit" name="submit" value="pdf" class="submit_pdf" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1" style="float: right;">
                                                            <div class="form-group">
                                                                <label style="width:28%; padding-top: 5px;"></label>
                                                                <div class="input-group">
                                                                    <input type="submit" name="submit" value="excel" class="submit_excel" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>			                
                                            <tr>
                                                <th width="5%" data-hide="phone"  data-class="expand">S.No</th>
                                                <th width="20%"><i class="fa fa-fw fa-building text-muted hidden-md hidden-sm hidden-xs"></i> Company Name</th>
                                                <th width="17%" data-hide="phone"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Contact Person</th>
                                                <th width="10%" data-hide="phone,tablet"><i class="fa fa-fw fa-envelope-o text-muted hidden-md hidden-sm hidden-xs"></i>Type</th>
                                                <th width="15%" data-hide="phone,tablet"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Assigned To</th>
                                                <th width="13%" data-hide="phone,tablet"><i class="fa fa-fw fa-user-circle-o text-muted hidden-md hidden-sm hidden-xs"></i>Picked By</th>
                                                <th width="10%" data-hide="phone,tablet"><i class="fa fa-fw fa-flag-o hidden-md hidden-sm hidden-xs"></i>Enq Status</th>
                                                <th width="15%" style="text-align: center;"><i class="fa fa-fw fa-cog  txt-color-blue hidden-md hidden-sm hidden-xs"></i> Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($enq_log_report as $key => $row) { ?>
                                                <tr>
                                                    <td><?php echo ++$key; ?></td>
                                                    <td><?php echo text($row->cus_name); ?></td>
                                                    <td><?php echo text($row->contact_person); ?></td>
                                                    <td><?php echo text($row->type); ?></td>
                                                    <td><?php echo text($row->assigned_to); ?></td>
                                                    <td><?php echo text($row->picked_user); ?></td>
                                                    <td><?php echo text($row->enq_sts); ?></td>
                                                    <!--<td><?php echo date('d-m-Y h:i A',strtotime($row->created_on)); ?></td>-->
                                                    <td align="center">
                                                            <a class="label btn btn-info pdf" href="<?php echo $pdf_link . $row->id ?>"><span>LOG PDF</span></a>
                                                        <?php if (in_array(1, $permission)) { ?>
                                                            <a class="label btn btn-warning view" href="<?php echo $view_link . $row->id ?>"><span>View</span></a>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- end widget content -->
                            </div>
                        </div> 
                    </div>
                </article>
                <!-- /.row -->
            </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function () {
        /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            'iDisplayLength': 25,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            }
        });

        /* END BASIC */
        $('#date_alloc').daterangepicker({
            autoUpdateInput: false,
            opens: 'center',
            locale: {
                format: 'MM/DD/YYYY h:mm A',
                cancelLabel: 'Cancel'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            //alert(start + '|' + end + '|' + label);
            // getResults();
        });

        $(function () {
            $('#search_date').daterangepicker({format: 'DD/MM/YYYY'});
        });
    });
</script>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/custom.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datepicker/daterangepicker.js"></script>