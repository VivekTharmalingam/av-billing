<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if(!empty($success)) {?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                            <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                            <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php }
                else if(!empty($error)) {?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error;?>
                    </div>
                <?php }?>&nbsp;
            </div>
        </div>

        <form class="" action="<?php echo $form_action;?>" method="post" name="client" id="client_form">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">

                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Client Add </h2>
                                <span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Client List"><a href="<?php echo base_url(); ?>client/" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a>
                                </span>                                
                            </header>

                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" >
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">* marked fields are mandatory</legend>
                                            <div class="row">
                                                <input type="hidden" class="form-control clt_id" name="clt_id" value="" />
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Client Name<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="form-control clt_name" name="clt_name" data-placeholder="XYZ" maxlength="100" />
                                                        </label>
                                                    </section>
                                                 </section>
                                                 
                                                 <section class="col col-6">  
                                                    <section>
                                                        <label class="label">Client Company Address</label>
                                                        <label class="input textarea-expandable">
                                                            <textarea class="form-control clt_address show_txt_count" rows="5" name="clt_address" maxlength="500"></textarea>
                                                        </label>
                                                    </section>
                                                 </section>
                                             </div>
                                             
                                             <div class="row">
                                                 <section class="col col-6">   
                                                    <section>
                                                        <label class="label">Phone No<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="form-control clt_phone" name="clt_phone" data-placeholder="9856789494" maxlength="50" />
                                                        </label>
                                                    </section>
                                                 </section>
                                                 
                                                 <section class="col col-6"> 
                                                    <section>
                                                        <label class="label">Pin Code</label>
                                                        <label class="input">
                                                            <input type="text" class="form-control clt_pin_code" name="clt_pin_code" data-placeholder="600001" maxlength="50" />
                                                        </label>
                                                    </section>
                                                 </section>
                                             </div>
                                             
                                             <div class="row">
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Fax</label>
                                                        <label class="input">
                                                            <input type="text" class="form-control clt_fax" name="clt_fax" data-placeholder="6782322890" maxlength="50" />
                                                        </label>
                                                    </section>
                                                </section>
                                                
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Email Address</label>
                                                        <label class="input">
                                                            <input type="text" class="form-control clt_email" name="clt_email" data-placeholder="xyz@gmail.com" maxlength="150" />
                                                        </label>
                                                    </section>
                                                </section>
                                              </div>
                                              
                                              <div class="row">
                                                  <section class="col col-6 clr">
                                                      <header style="font-size: 14px; margin-left: 0px !important;">
                                                        <label style=" font-weight: bold; text-align: left; font-size: 15px; color: #A90329;">Contact Details</label>
                                                      </header><br/>
                                                  </section>
                                              </div>
                                              
                                              <div class="row">
                                                  <section class="col col-6">
                                                      <section>
                                                          <label class="label">Contact Name<span style="color: red;">*</span></label>
                                                          <label class="input">
                                                            <input type="text" class="form-control clt_cnt_name" name="clt_cnt_name" data-placeholder="Karthick" maxlength="100" />
                                                          </label>
                                                      </section>
                                                  </section>

                                                  <section class="col col-6">
                                                      <section>
                                                      <label class="label">Contact Email</label>
                                                      <label class="input">
                                                        <input type="text" class="form-control clt_cnt_email" name="clt_cnt_email" data-placeholder="karthick@gmail.com" maxlength="150" />
                                                      </label>
                                                      </section>
                                                  </section>
                                              </div>
                                                
                                              <div class="row">
                                                  <section class="col col-6">
                                                      <section>
                                                          <label class="label">Phone No<span style="color: red;">*</span></label>
                                                          <label class="input">
                                                              <input type="text" class="form-control clt_cnt_phone" name="clt_cnt_phone" data-placeholder="9755463427" maxlength="50" />
                                                           </label>
                                                       </section>
                                                   </section>
                                                 
                                                  <section class="col col-6">
                                                     <section>
                                                         <label class="label">Remarks</label>
                                                         <label class="input textarea-expandable">
                                                             <textarea rows="3" class="form-control clt_remarks show_txt_count" name="clt_remarks" maxlength="500"></textarea>
                                                         </label>
                                                     </section>
                                                  </section>
                                               </div>
                                                
                                               <div class="row">  
                                                  <section class="col col-6">
                                                     <section>
                                                        <label class="label">Status</label>
                                                        <label class="select">
                                                            <select  name="status" class="select2">
                                                            <option value="1">Active</option>
                                                            <option value="2">Inactive</option>
                                                            </select>
                                                         </label>
                                                     </section>
                                                  </section>
                                              </div>
                                        </fieldset>
                                        
                                        <footer>
                                            <button type="submit" class="btn btn-primary" >Submit</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/app/client.js"></script>