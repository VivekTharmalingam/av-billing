<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if(!empty($success)) {?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                            <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                            <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php }else if(!empty($error)) {?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error;?>
                    </div>
                <?php }?>&nbsp;
            </div>
        </div>
        
        <!-- Main content -->
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">

                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Client List</h2> 
                            <span style=" float: right; margin-top: 6px;"><a href="<?php echo base_url(); ?>client/add" style=" color: white;"><i class="fa fa-lg fa-plus-circle" aria-hidden="true"></i></a></span>
                        </header>

                        <div>
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <div class="widget-body no-padding">
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>			                
                                        <tr>
                                            <th data-class="expand">S.No</th>
                                            <th ><i class="fa fa-fw fa-male txt-color-blue hidden-md hidden-sm hidden-xs"></i>Client Name</th>
                                            <th data-hide="phone,tablet"><i class="fa fa-fw fa-mobile txt-color-blue hidden-md hidden-sm hidden-xs"></i>Phone No</th>
                                            <th data-hide="phone,tablet"><i class="fa fa-fw  fa-fax txt-color-blue hidden-md hidden-sm hidden-xs"></i>Fax No</th>
                                            <th data-hide="phone,tablet"><i class="fa fa-fw fa-envelope-square txt-color-blue hidden-md hidden-sm hidden-xs"></i>Email</th>
                                            <th data-hide="phone" style="text-align: center;"><i class="fa fa-fw fa-cog txt-color-blue hidden-md hidden-xs"></i>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($clients as $key => $row) { ?>
                                            <tr>
                                                <td><?php echo ++$key; ?></td>
                                                <td><?php echo text($row->clt_name); ?></td>
                                                <td><?php echo text($row->clt_phone); ?></td>
                                                <td><?php echo text($row->clt_fax); ?></td>
                                                <td><?php echo text($row->clt_email); ?></td>
                                                <td align="center">
                                                    <?php if (in_array(1, $permission)) {?>
                                                    <a class="label btn btn-warning view" href="<?php echo $view_link . $row->id ?>"><span>View</span></a>
                                                    <?php }?>

                                                    <?php if (in_array(3, $permission)) {?>
                                                    <a class="label btn btn-primary edit" href="<?php echo $edit_link . $row->id ?>"><span>Edit</span></a>
                                                    <?php }?>

                                                    <?php if (in_array(4, $permission)) {?>
                                                    <a class="label btn btn-danger pop_up_confirm delete smart-mod-delete" href="#" data-bindid="<?php echo $row->id; ?>" data-bindtext="<?php echo $row->clt_name; ?>">
                                                        <span>Delete</span>
                                                    </a>
                                                    <?php }?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!-- /.col -->
                    </div>

                </article>

                <!-- /.row -->
            </div>

        </section>

        <!-- /.content -->
    </div>

    <!-- /.main -->
</div>

<script>
    $(document).ready(function() {
                    
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
	
			/* END BASIC */
                $(".smart-mod-delete").click(function(e) {
                                var $bindId = $(this).data('bindid');
                                var $bindText = $(this).data('bindtext');
                                $.SmartMessageBox({
                                        title : "Are you sure?",
                                        content : "You will not be able to recover this Client Details!",
                                        buttons : '[No][Yes]'
                                }, function(ButtonPressed) {
                                        if (ButtonPressed === "Yes") {
                                                window.location.replace(baseUrl + "client/delete/" + $bindId);
                                        }
                                        if (ButtonPressed === "No") {
                                                $.smallBox({
                                                        title : "Sorry ! ...",
                                                        content : "<i class='fa fa-info-circle'></i> <i>You just selected NO...<br>So The action of DELETE is disallowed for the "+$bindText+" details.. </i>",
                                                        color : "#C46A69",
                                                        iconSmall : "fa fa-times fa-2x fadeInRight animated",
                                                        timeout : 4000
                                                });
                                        }

                                });
                                e.preventDefault();
                        })
                    });
    </script>