<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo $list_link; ?>" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-file-text-o"></i> </span>
                            <h2>Invoice Return View</h2>
<!--                             <span class="widget-toolbar" rel="tooltip" data-placement="bottom" data-original-title="Purchase Order List" onclick="location.href='<?php echo base_url(); ?>purchase_orders/'">
                                <i class="fa fa-list" aria-hidden="true"></i>
                            </span>-->
                             </span>
                            
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body">
                                <!--<span id="watermark">Malar ERP.</span>-->
                                <table id="user" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Invoice Number</td>
                                            <td ><?php echo $return->so_no; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Return Date</td>
                                            <td ><?php echo date('d/m/Y',strtotime($return->return_date)); ?></td>
                                        </tr>
										<tr>
                                            <td style="width:35%;font-weight: bold;">Refund Amount</td>
                                            <td ><?php echo text_amount($return->refund_amount); ?></td>
                                        </tr>
                                        <!--<tr>
                                            <td style="font-weight: bold;">Branch Name</td>
                                            <td ><?php echo $return->branch_name; ?></td>
                                        </tr>-->
                                        
                                        <tr>
                                            <td colspan="2" style="color: #A90329;"><b>Customer Details</b></td>
                                        </tr>
                                        <?php if($return->customer_id != '') { ?>
                                         <tr>
                                            <td style="font-weight: bold;">Customer Name</td>
                                            <td ><?php echo $return->name; ?></td>
                                        </tr>
                                         <tr>
                                            <td style="font-weight: bold;">Customer Contact Person</td>
                                            <td ><?php echo $return->contact_name; ?></td>
                                        </tr>
                                        
                                         <tr>
                                            <td style="font-weight: bold;">Customer Contact No</td>
                                            <td ><?php echo $return->phone_no; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Customer's Address</td>
                                            <td ><?php echo nl2br($return->address); ?></td>
                                        </tr>
                                        <?php } else { ?>
                                        <tr>
                                            <td style="font-weight: bold;">Customer</td>
                                            <td ><?php echo $return->cash_cust; ?></td>
                                        </tr>   
                                        <?php } ?>
                                    </tbody>
                                </table>
								
                                   <div class="text-left">
                                        <span class="onoffswitch-title">
                                          <h3><b style="color: #8e233b;">Items Details</b></h3>
                                        </span>
                                    </div>
                                    <div id="widget-tab-1" class="table-responsive">
                                        <table id="dt_basic_1" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important;">
                                           <thead>
                                              <tr>
                                                   <th data-class="expand" style="font-weight: bold; text-align:center;">S.No</th>
                                                   <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Item code</th>
                                                   <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Description</th>
                                                   <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Serial No.</th>
                                                   <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Returned Quantity</th>
												   <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Type</th>
												   <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Re-Stock Quantity</th>
                                              </tr>
                                           <thead>
                                           <tbody>
                                              <?php foreach($return_items as $key=>$item){?>
                                                 <tr>
                                                    <td><?php echo ++ $key;?></td>
                                                    <td><?php echo $item->pdt_code;?></td>
                                                    <td><?php echo $item->pdt_desc;?></td>
                                                    <!--<td align="center"><?php echo ($item->so_quantity) - ($item->already_returned_qty);?></td>-->
                                                    <td align="center"><?php echo $item->serial_nos_txt;?></td>
                                                    <td align="center"><?php echo $item->returned_qty;?></td>
                                                    <td align="center"><?php echo $item->return_item_type_str;?></td>
                                                    <td align="center"><?php echo $item->restock_quantity;?></td>
                                                </tr>
                                           <?php } ?>
                                          </tbody>
                                       </table>
                                 </div>
                            </div>                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->

<script type="text/javascript">
    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    $(document).ready(function() {
        
        /* BASIC ;*/
            var responsiveHelper_dt_basic_1 = undefined;
            var responsiveHelper_dt_basic_2 = undefined;
            var responsiveHelper_dt_basic_3 = undefined;
            var breakpointDefinition = {
                tablet : 1024,
                phone : 480
            };

            $('#dt_basic_1').dataTable({
                "sDom": "",
                "autoWidth" : true,
                "iDisplayLength": 100,
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_dt_basic_1) {
                        responsiveHelper_dt_basic_1 = new ResponsiveDatatablesHelper($('#dt_basic_1'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_dt_basic_1.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_dt_basic_1.respond();
                }
            });
    });
</script>