<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) {
                    ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>

        <form class="add_form" id="sales_order_ret_add" action="<?php echo $form_action; ?>" method="post" data-form_submit="0">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <a href="<?php echo $list_link; ?>" class="large_icon_des" style=" color: white;">
                            <button type="button" class="btn btn-primary btn-circle btn-xl temp_color ">
                                <i class="glyphicon glyphicon-list"></i>
                            </button>
                        </a>
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Invoice Return Add </h2>
                            </header>

                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" id ="sales_form">
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <input type="hidden" class="form-control po_id" name="po_id" value="" />

                                                    <section>
                                                        <label class="label">Invoice No #</label>
                                                        <label class="select">
                                                            <select name="so_no" id="so_no" class="select2 so_no">
                                                                <option value="">Select Invoice</option>
                                                                <?php foreach ($so as $key => $sales) { ?>  <option value="<?php echo $sales->id; ?>"><?php echo $sales->so_no; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                    </section>

                                                    <section>
                                                        <label class="label">Customer Reference</label>
                                                        <label class="input">
                                                            <input type="text" name="remarks" class="form-control remarks" id="remarks" data-placeholder="Customer Reference" readonly="" />
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Return Date<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="so_date" id="so_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php echo date('d/m/Y'); ?>" />
                                                        </label>
                                                    </section>
                                                </section>

                                                <section class="col col-6">
                                                    <section class="alert alert-info" id="invoice_details">
                                                        <label class="label" style="font-size: 11px;">Sold To: <span id="customer_name">N/A</span></label>
                                                        <label class="label" style="font-size: 11px;">Contact Name: <span id="contact_name">N/A</span></label>
                                                        <label class="label" style="font-size: 11px;">Contact No: <span id="contact_no">N/A</span></label>
                                                        <label class="label" style="font-size: 11px;">Address: <span id="contact_address">N/A</span></label>
                                                    </section>
                                                </section>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Item Details</legend>                                            

                                            <div class="row material-container product_details">
                                                <div class="items_head col-xs-12 clr">
                                                    <section class="col col-2">
                                                        <label class="label"><b>Item Code</b></label>
                                                    </section>
                                                    <section class="col col-3">
                                                        <label class="label"><b>Description</b></label>
                                                    </section>
                                                    <section class="col col-2">
                                                        <label class="label"><b>Serial No.</b></label>
                                                    </section>
                                                    <section class="col col-1">
                                                        <label class="label"><b>Return Qty</b></label>
                                                    </section>
                                                    <section class="col col-2">
                                                        <label class="label"><b>Type</b></label>
                                                    </section>
                                                    <section class="col col-1">
                                                        <label class="label"><b>Re-Stock Qty</b></label>
                                                    </section>
                                                    <section class="col col-1">
                                                    </section>
                                                </div>

                                                <div class="items col-xs-12 clr">
                                                    <section class="col col-2">
                                                        <label class="input">
                                                            <input type="text" name="product_code[]" class="product_code" data-value="" value="" />
                                                            <input type="hidden" name="product_id[]" class="product_id" />
                                                            <input type="hidden" name="so_item_autoid[]" class="so_item_autoid" />
                                                            <input type="hidden" name="sold_price[]" class="sold_price" />
                                                            <span class="product_code_label">Selected Item code already exist!</span>
                                                        </label>
                                                        <label class="label sold_item" style="color: #2e3192; font-size: 11px; display: none;">Sold Quantity : <span class="inv_quantity">0</span></label>
                                                        <input type="hidden" name="inv_quantity[]" class="inv_quantity" />
                                                    </section>

                                                    <section class="col col-3">
                                                        <label class="textarea">
                                                            <textarea name="product_description[0]" class="product_description" rows="1"></textarea>
                                                        </label>
                                                        <label class="label sold_item" style="color: #2e3192; font-size: 11px; display: none;">Sold Price : <span class="sold_price">0</span></label>
                                                    </section>

                                                    <section class="col col-2">
                                                        <label class="select">
                                                            <select name="serial_nos_txt[0][0]" multiple class="select2 serial_nos_txt">
                                                            </select>
                                                        </label>
                                                    </section>
                                                    
                                                    <section class="col col-1">
                                                        <label class="input">
                                                            <input type="text" name="return_quantity[]" class="return_quantity integer_value" />
                                                        </label>
                                                        <label class="label already_return" style="color: #2e3192; font-size: 11px; display: none;">Already Return : <span class="already_return_qty">0</span></label>
                                                    </section>

                                                    <section class="col col-2">
                                                        <label class="select">
                                                            <select name="return_item_type[]" class="return_item_type select2">
                                                                <option value="1">Return</option>
                                                                <option value="2" selected>Re-Stock</option>
                                                                <option value="3">Warranty</option>
                                                            </select>
                                                        </label>
                                                    </section>

                                                    <section class="col col-1">
                                                        <label class="input">
                                                            <input type="text" name="restock_quantity[]" class="restock_quantity integer_value" />
                                                        </label>
                                                    </section>

                                                    <section class="col col-1">
                                                        <label class="input">
                                                            <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                        </label>
                                                    </section>
                                                </div>
                                            </div>

                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">&nbsp;</legend>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Remarks</label>
                                                    <label class="input textarea-expandable">
                                                        <textarea class="form-control re_remarks show_txt_count count-left-added" name="re_remarks" maxlength="500" data-placeholder="Remarks" rows="2"></textarea><div class="text-muted txt_left">Left: <b>500</b> charecters only.</div>
                                                    </label>
                                                </section>

                                                <section class="col col-6">
                                                    <label class="label">Refund Amount <span style="color: red">*</span></label>
                                                    <label class="input"> <i class="icon-append fa fa-money"></i>
                                                        <input type="text" class="refund_amount float_value" name="refund_amount" id="refund_amount" />
                                                    </label>
                                                </section>
                                            </div>
                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary" >Submit</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                    <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/sales_return.js"></script>
