<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) {
                    ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>

        <form class="edit_form" action="<?php echo $form_action; ?>" method="post" name="sor" id="sales_order_ret_add">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                        <a href="<?php echo $list_link; ?>" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">

                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Invoice Return Edit </h2>
<!--                                <span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Purchase Return List"><a href="<?php echo base_url(); ?>purchase_returns/" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a>
                                </span>-->
                            </header>

                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" >
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <input type="hidden" class="form-control hidden_sr_id" name="hidden_sr_id" value="<?php echo $return->id; ?>" />

                                                    <section>
                                                        <label class="label">Invoice No #</label>
                                                        <label class="select"><?php #print_r($so);echo $return->so_id;?>
                                                            <select name="so_no" id="so_no" class="select2 so_no" >
                                                                <option value="">Select Invoice</option>
                                                                <?php foreach ($so as $key => $sales) { ?>  
                                                                    <option value="<?php echo $sales->id; ?>" <?php echo ($sales->id == $return->so_id) ? 'selected' : ''; ?>><?php echo $sales->so_no; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                            <input type="hidden" class="form-control hidden_so_id" name="hidden_so_id" value="<?php echo $return->so_id; ?>" />
                                                        </label>

                                                    </section>

                                                    <section>
                                                        <label class="label">Customer Reference</label>
                                                        <label class="input">
                                                            <input type="text" name="remarks" class="form-control remarks" id="remarks" data-placeholder="Customer Reference" readonly="" />
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Return Date<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="so_date" id="so_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php echo date('d/m/Y', strtotime($return->return_date)); ?>" />
                                                        </label>
                                                    </section>
                                                </section>

                                                <section class="col col-6 deli_address">
                                                    <section>
                                                        <label class="label">Sold To<span style="color: red;">*</span></label>
                                                        <label class="select" style="display:none;">
                                                            <select name="customer_name" id="customer_name" class="select2" disabled="">
                                                                <option value="">Select Customer name</option>
                                                                <?php foreach ($customers as $key => $customer) { ?>  <option value="<?php echo $customer->id; ?>"><?php echo $customer->name; ?></option>
                                                                <?php } ?>
                                                            </select>

                                                        </label>
                                                        <label class="input">
                                                            <input type="text" name="cust_name" class="form-control cust_name" id="cust_name" data-placeholder="Sold To" readonly="" />
                                                            <input type="hidden" name="hidden_customer_name" class="hidden_customer_name">
                                                        </label>
                                                        </label>
                                                    </section>
                                                    <section class="cust_address" style="display: none;"></section>

                                                    <section>
                                                        <label class="label">Deliver To<span style="color: red;">*</span></label>
                                                        <label class="select" style="display:none;">
                                                            <select name="ship_to" id="ship_to" class="select2" disabled="">
                                                                <option value="">Select Location</option>
                                                            </select>

                                                        </label>
                                                        <label class="input">
                                                            <input type="text" name="shipping_to" class="form-control shipping_to" id="shipping_to" data-placeholder="Deliver To" readonly="" />
                                                        </label>
                                                        <input type="hidden" name="hidden_ship_to" class="ship_to">
                                                        </label>

                                                    </section>
                                                    <section class="ship_address" style="display: none;"></section>

                                                </section>
                                                <section class="col col-6 own_addre" style="display: none;"></section>
                                            </div>
                                        </fieldset>

                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Item Details</legend><?php //print_r($return_items); ?>
                                            <div class="row material-container product_details">
                                                <div class="items_head col-xs-12 clr">
                                                    <section class="col col-3">
                                                        <label class="label"><b>Item Code</b></label>
                                                    </section>
                                                    <section class="col col-3">
                                                        <label class="label"><b>Item Description</b></label>
                                                    </section>                                                 
                                                    <section class="col col-3">
                                                        <label class="label"><b>Sold Quantity</b></label>
                                                    </section>
                                                    <section class="col col-2">
                                                        <label class="label"><b>Return Quantity</b></label>
                                                    </section>
                                                </div>

                                                <?php $item_count = count($return_items); ?>
                                                <?php foreach ($return_items as $item_key => $item) {?>
                                                    <div class="items col-xs-12 clr">
                                                        <input type="hidden" name="hidden_soitem_id[<?php echo $item_key; ?>]" value="<?php echo $item->id; ?>" />
                                                        <input type="hidden" name="hidden_sri_id[<?php echo $item_key; ?>]" value="<?php echo $item->id; ?>" />
														<input type="hidden" name="pdt_id[<?php echo $item_key; ?>]" value="<?php echo $item->item_id; ?>" />
                                                        <section class="col col-3">
															<label class="input">
																<input type="text" name="pdt_name[<?php echo $item_key; ?>]" value="<?php echo $item->pdt_code; ?>" readonly />
															</label>
                                                            <!--<label class="select">
                                                                <select name="pdt_id[<?php echo $item_key; ?>]" class="select2 pdt_id">
                                                                    <option value="">Select</option>
                                                                   <?php foreach($items as $key => $prod) { ?>
                                                                        <?php if ($prod->id == $item->item_id) {?>
                                                                            <option value="<?php echo $prod->id;?>" data-selling_price="<?php echo $prod->selling_price;?>" data-buying_price="<?php echo $prod->buying_price;?>" <?php echo ($prod->id == $item->item_id) ? 'selected' : '';?>><?php echo $prod->pdt_code;?></option>
                                                                        <?php }?>
                                                                    <?php } ?>
                                                                </select>
                                                            </label>-->
                                                        </section>

                                                        <section class="col col-3">
                                                            <label class="textarea">
                                                                <textarea name="product_description[<?php echo $item_key;?>]" class="product_description" rows="1"><?php echo $item->pdt_desc;?></textarea>
                                                            </label>                                                            
                                                            <span class="brand_label" style="display: none;">Item Code: <span class="brand_name"><?php echo $item->cat_name;?></span></span>

                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="input">
                                                                <input type="hidden" name="grni_quantity[]" class="grni_quantity" data-placeholder="0" readonly="" value="<?php echo $item->so_quantity; ?>" />
                                                                <input type="text" name="received_quantity[]" class="received_quantity" data-placeholder="0" readonly="" value="<?php echo ($item->so_quantity) - ($item->already_returned_qty); ?>" />
                                                            </label>
                                                        </section>

                                                        <section class="col col-2">
                                                            <label class="input">
                                                                <input type="text" name="returned_quantity[]" class="returned_quantity float_value" data-placeholder="0" value="<?php echo $item->returned_qty; ?>" />
                                                            </label>
                                                        </section>

                                                        <section class="col col-1">
                                                            <!--<label class="input">
                                                                <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                            </label>-->
                                                        </section>
                                                    </div>
                                                <?php } ?>
                                            </div>

                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">&nbsp;</legend>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Remarks</label>
                                                    <label class="input textarea-expandable">
                                                        <textarea class="form-control por_remarks show_txt_count" name="por_remarks" maxlength="500" data-placeholder="Remarks" rows="2" ><?php echo $return->so_return_remarks; ?></textarea>
                                                    </label>
                                                </section>

                                                <!--<section class="col col-6">
                                                    <label class="label">Status<span style="color: red;">*</span></label>
                                                    <label class="select">
                                                        <select  name="status" class="select2">
                                                            <option value="1" <?php echo ($return->status == '1') ? 'selected' : ''; ?>>Active</option>
                                                            <option value="2" <?php echo ($return->status == '2') ? 'selected' : ''; ?>>Inactive</option>
                                                        </select>
                                                    </label>
                                                </section>-->
                                            </div>
                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn btn-primary" >Submit</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                    <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/sales_return.js"></script>