<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
            </span> 
        </span>
        
        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>
    
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if(!empty($success)) {?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                            <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                            <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php }
                else if(!empty($error)) {?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error;?>
                    </div>
                <?php }?>&nbsp;
            </div>
        </div>

        <form class="add_form" id="delivery_order" action="<?php echo $form_action;?>" method="post" data-form_submit="0">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <a href="<?php echo $list_link; ?>" class="large_icon_des" style=" color: white;">
                            <button type="button" class="btn btn-primary btn-circle btn-xl temp_color ">
                                <i class="glyphicon glyphicon-list"></i>
                            </button>
                        </a>
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Delivery Order Add </h2>
                            </header>

                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" id ="sales_form">
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Invoice No #<span style="color: red;">*</span></label>
                                                        <label class="select">
                                                            <select name="so_no" id="so_no" class="select2">
                                                                <option value="">Select Sales Order</option>
                                                                <?php foreach($sales_orders as $key => $so) {?>  <option value="<?php echo $so->id;?>"><?php echo $so->so_no;?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                    
                                                    <section>
                                                        <label class="label">Delivery Date<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="do_date" id="do_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php echo date('d/m/Y');?>" />
                                                        </label>
                                                    </section>
                                                    
                                                    <!--<section>
                                                        <label class="label">Delivery Order #</label>
                                                        <label class="input">
                                                            <input type="text" name="do_no" class="form-control" data-placeholder="" />
                                                        </label>
                                                    </section>-->
                                                 </section>
                                                 
                                                 <section class="col col-6">
                                                    <section class="alert alert-info" id="so_details" style="display: none;">
                                                        <label class="label">
                                                            <b>Invoice Details</b>
                                                        </label>
                                                        <label class="label">Customer Name: <span class="customer_name"></span></label>
                                                        
                                                        <label class="label">Address: <span class="customer_address"></span></label>
                                                        
                                                        <label class="label">Deliver To: <span class="ship_to"></span></label>
                                                        <label class="label" style="padding-left: 20px;">Address: <span class="ship_to_address"></span></label>
                                                        <label class="label" style="padding-left: 20px;">Location: <span class="ship_to_location"></span></label>
                                                        
                                                        <label class="label">Invoice Date: <span class="invoice_date"></span></label>
                                                        
                                                        <label class="label">Customer Reference: <span class="customer_reference"></span></label>
                                                    </section>
                                                 </section>
                                             </div>
                                        </fieldset>
                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Item Details</legend>
                                            <!--<div class="row product_search">
                                                <section class="col col-5">
                                                    <div class="input hidden-mobile">
                                                        <input name="search_product" class="form-control" type="text" data-placeholder="Scan / Search product by code or name" id="search_product" />
                                                    </div>
                                                </section>
                                            </div>-->
                                            
                                             <div class="row material-container product_details">
                                                <div class="items_head col-xs-12 clr">
                                                    <section class="col col-3">
                                                        <label class="label"><b>Item Code</b></label>
                                                    </section>
                                                    <section class="col col-4">
                                                        <label class="label"><b>Description</b></label>
                                                    </section>
                                                    <section class="col col-2">
                                                        <label class="label"><b>SO Qty</b></label>
                                                    </section>
                                                    
                                                    <section class="col col-2">
                                                        <label class="label"><b>Quantity</b></label>
                                                    </section>
                                                    
                                                    <!--<section class="col col-2">
                                                        <label class="label"><b>Amount</b></label>
                                                    </section>-->
                                                </div>
                                                
                                                 <div class="items col-xs-12 clr">
                                                    <section class="col col-3">
                                                        <label class="select">
                                                            <select name="pdt_id[0]" class="select2 pdt_id">
                                                                <option value="">Select Item Code</option>
                                                            </select>
                                                            <input type="hidden" name="category_id[0]" class="category_id">
                                                        </label>
                                                    </section>
                                                    
                                                    <section class="col col-4">
                                                        <label class="textarea textarea-expandable" >
                                                            <textarea name="product_description[0]" class="product_description"  data-placeholder="Sony Braviya TV" rows="2" readonly=""></textarea>
                                                        </label>
                                                    </section>
                                                    
                                                    <section class="col col-2">
                                                        <label class="input">
                                                            <input type="text" name="so_quantity[0]" class="so_quantity float_value" readonly />
                                                        </label>
                                                        <span class="delivered_quantity"></span>
                                                    </section>
                                                    
                                                    <section class="col col-2">
                                                        <label class="input">
                                                            <input type="text" name="quantity[0]" class="quantity float_value" />
                                                        </label>
                                                    </section>
                                                    
                                                    <!--<section class="col col-2">
                                                        <label class="input">
                                                            <i class="icon-prepend fa fa-dollar"></i>
                                                            <input type="text" name="amount[0]" class="amount" data-placeholder="0.00" readonly="" />
                                                        </label>
                                                    </section>-->
                                                    
                                                     <section class="col col-1">
                                                        <label class="input">
                                                            <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                        </label>
                                                    </section>
                                                </div>
                                            </div>
                                            
                                            <!--<div class="row" style="margin-top: 2%;">
                                               <section class="col col-6">
                                                </section>
                                                <section class="col col-3">
                                                <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Sub Total</b></label>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="sub_total" class="sub_total" readonly="" data-placeholder="0.00" />
                                                    </label>
                                                </section>
                                                <section class="col col-1">
                                                </section>
                                            </div>
                                            
                                            <div class="row" style="margin-top: 2%;">
                                               <section class="col col-6">
                                                </section>
                                                <section class="col col-3">
                                                <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Discount</b></label>
                                                </section>
                                                <section class="col col-2">
                                                    <div class="input-group">
                                                        <input type="text" name="discount" class="form-control discount" value="0.00" data-placeholder="0.00" />
                                                        <span class="input-group-addon">
                                                            <span class="onoffswitch">
                                                                <input type="checkbox" name="discount_type" class="onoffswitch-checkbox discount_type" id="discount_type-0" value="1" checked />
                                                                <label class="onoffswitch-label" for="discount_type-0"> 
                                                                    <span class="onoffswitch-inner" data-swchon-text="%" data-swchoff-text="S$"></span> 
                                                                    <span class="onoffswitch-switch"></span> 
                                                                </label> 
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <input type="hidden" name="discount_amt" class="discount_amt" />
                                                    <span class="discount_container">
                                                        <span class="currency">S$</span>
                                                        <span class="discount_amount">0.00</span>
                                                    </span>
                                                </section>
                                                <section class="col col-1">
                                                </section>
                                            </div>
                                            
                                            <div class="row" style="margin-top: 2%;">
                                               <section class="col col-6">
                                                </section>
                                                <section class="col col-3">
                                                <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Total Amount</b></label>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="total_amt" class="total_amt" readonly="" data-placeholder="0.00" />
                                                    </label>
                                                </section>
                                                <section class="col col-1">
                                                </section>
                                            </div>
                                            
                                            <div class="row gst_details">
                                                <section class="col col-4">
                                                </section>
                                                <section class="col col-2">&nbsp;</section>
                                                <section class="col col-3">
                                                    <section>
                                                        <label class="label">&nbsp;</label>
                                                        <div class="inline-group">
                                                            <label class="radio">
                                                                <input type="radio" class="gst_type" name="gst_type" value="1" /><i></i>Include GST
                                                             </label>
                                                            <label class="radio">
                                                                <input type="radio" class="gst_type" name="gst_type" value="2" /><i></i>Exclude GST
                                                           </label>
                                                        </div>
                                                    </section>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="label">&nbsp;</label>
                                                    <label class="input change_perc_disamt"><i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="gst_amount" class="gst_amount float_value" data-placeholder="0.00" readonly="" />
                                                        <input type="hidden" value="" name="hidden_gst" id="hidden_gst" />
                                                    </label>
                                                    <span class="gst_val">GST @ <span class="gst_percentage"></span> %</span>
                                                </section>
                                            </div>-->
                                        </fieldset>
                                        
                                        <footer>
                                            <button type="submit" class="btn btn-primary" >Submit</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/app/delivery_order.js"></script>
