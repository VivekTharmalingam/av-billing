<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
            </span> 
        </span>
        
        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>
    
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if(!empty($success)) {?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                            <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                            <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php }
                else if(!empty($error)) {?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error;?>
                    </div>
                <?php }?>&nbsp;
            </div>
        </div>
        
        <!--<form class="print_form" action="<?php echo $form_action;?>" method="post">-->
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <a href="<?php echo $list_link; ?>" class="large_icon_des" style=" color: white;">
                            <button type="button" class="btn btn-primary btn-circle btn-xl temp_color ">
                                <i class="glyphicon glyphicon-list"></i>
                            </button>
                        </a>
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Sales Order Print </h2>
                            </header>

                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" id ="sales_form">
                                        <fieldset>
                                            <div class="row">
                                                <section class="col col-md-12" style="text-align: center;     margin: 10% 0;">
                                                    <input type="hidden" class="form-control" name="so_id" value="<?php echo $so_id;?>" />
                                                    
                                                    <button type="button" class="btn btn-lg btn-default" onclick="location.href='<?php echo $list_link; ?>'">Cancel</button>
                                                    
                                                    <a href="<?php echo $add_link; ?>">
                                                    <button type="submit" class="btn btn-lg btn-primary" style="margin-left: 100px;" onclick="window.open('<?php echo $pdf_link; ?>', '_blank')">Print</button>
                                                    </a>
                                                </section>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>
        <!--</form>-->
    </div>
</div>