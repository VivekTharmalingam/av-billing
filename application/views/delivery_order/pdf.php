<style>
    .barcode {
        padding: 1.5mm;
        margin: 0;
        vertical-align: top;
        color: #424242;
        width:200px;
    }
</style>
<?php $height = 410; ?>
<table width="100%" border="1" style="border-collapse:collapse; font-family: calibri;" cellpadding="5">
    <thead>
        <tr>
            <th width="25%" style="background:#EBEBEB; font-size: 15px;">Item Code</th>
            <th width="60%" style="background:#EBEBEB; font-size: 15px;">Description</th>
            <th width="15%" style="background:#EBEBEB; font-size: 15px;">Quantity</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $quantity = 0;
        foreach ($do_items as $key => $item) {
            ?>
            <tr style="vertical-align:top;">
                <td style="border-top: 0; border-bottom: none; font-size: 14px;"><?php echo strtoupper($item->pdt_code); ?></td>
                <td style="border-top: 0;border-bottom: none; font-size: 14px;"><?php echo nl2br(strtoupper($item->product_description)); ?></td>
                <td style="border-top: 0;border-bottom: none; font-size: 14px;" align="center"><?php echo $item->quantity; ?></td>
            </tr>
            <?php $height -= 55; ?>
            <?php $quantity += $item->quantity; ?>
        <?php } ?>
        <tr>
            <td height="<?php echo $height; ?>" style="border-top: 0;"></td>
            <td style="border-top: 0;"></td>
            <td style="border-top: 0;"></td>
        </tr>

        <tr>
            <td colspan="2" style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; padding-right: 30px; font-size: 14px;">
                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="520" align="left">
                            <?php if (!empty($sales_order->remarks)) { ?>
                                <b style="font-size: 15px;">Remarks</b>
                                <div>
                                    <?php echo strtoupper(nl2br($sales_order->remarks)); ?>
                                </div>
                            <?php } ?>
                        </td>
                        <th style="text-align: left; vertical-align: top;">Total</th>
                    </tr>
                </table>
            </td>
            <td align="center" style="border-right: 1px solid #000; border-bottom: 1px solid #000; padding-right: 5px; vertical-align: top; font-size: 14px;"><?php echo $quantity; ?></td>
        </tr>
    </tbody>
</table>
