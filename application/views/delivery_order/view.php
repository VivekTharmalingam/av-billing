<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>delivery_orders/" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="List"><i class="glyphicon glyphicon-list"></i></button></a>
                    <!--<a target="_blank" href="<?php echo base_url(); ?>invoice/pdf/<?php echo $sales_order->id; ?>" class="large_icon_des" style=" color: white;margin-right: 80px;" ><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="Download"><i class="glyphicon glyphicon-save"></i></button></a>-->

<!--<a target="_blank" href="<?php echo base_url(); ?>sales_orders/pdf/<?php echo $sales_order->id; ?>" class="large_icon_des" style=" color: white;padding-right: 80px;" ><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="Download"><i class="glyphicon glyphicon-save"></i></button></a>-->

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-file-text-o"></i> </span>
                            <h2>Delivery Order View</h2>
                        </header>

                        <!-- widget div-->
                        <div role="content">
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body">
                                <!--<span id="watermark">Malar ERP.</span>-->
                                <table id="user" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Invoice Number</td>
                                            <td ><?php echo $sales_order->so_no; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Invoice Date</td>
                                            <td ><?php echo text_date($sales_order->so_date); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Status</td>
                                            <td ><?php echo $sales_order->status_str; ?></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="text-left">
                                    <h3><b style="color: #8e233b;">Delivery Details</b></h3>
                                </div>
                                <div id="widget-tab-1" class="table-responsive">
                                    <table id="dt_basic_2" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
                                        <thead>
                                            <tr>
                                                <th style="width: 5%; text-align:center;">S.No</th>
                                                <th style="width: 20%;">Item Code</th>
                                                <th style="width: 45%;">Description</th>
												<th style="width: 15%; text-align:center;">Inv Quantity</th>
                                                <th style="width: 15%; text-align:center;">Quantity</th>
                                            </tr>
                                        <thead>
                                        <tbody>
                                            <?php
											$quantity = 0;
											$so_quantity = 0;
                                            foreach ($do_items as $key => $item) {
												$quantity += $item->quantity;
												$so_quantity += $item->so_quantity;?>
                                                <tr>
                                                    <td><?php echo ++$key; ?></td>
                                                    <td><?php echo $item->pdt_code; ?></td>
                                                    <td><?php echo strtoupper($item->product_description); ?></td>
													<td align="center"><?php echo $item->so_quantity; ?></td>
                                                    <td align="center"><?php echo $item->quantity; ?></td>
                                                </tr>
											<?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3" style="text-align: right; padding-right: 20px;">Total</th>
                                                <th style="text-align: center;"><?php echo $so_quantity; ?></th>
												<th style="text-align: center;"><?php echo $quantity; ?></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>


                            <div class="widget-body">				
                                <div class="panel-group smart-accordion-default" id="accordion">
									<?php foreach ($delivery_orders as $key => $delivery_order) { ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">													
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $key; ?>" <?php echo ( $key != 0) ? 'class="collapsed"' : '' ?> ><i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i> <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Delivery Order : <?php echo $delivery_order->do_no . ' (' . $delivery_order->do_date . ')'; ?> </a>
												</h4>
                                            </div>
                                            <div id="collapse<?php echo $key; ?>" class="panel-collapse collapse <?php echo ( $key == 0) ? 'in' : '' ?>">
                                                <div class="panel-body no-padding <?php echo ( $key == 0) ? 'no-padding' : '' ?>" >
                                                    <table class="table table-bordered table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 5%; text-align:center;">S.No</th>
																<th style="width: 20%;">Item Code</th>
																<th style="width: 50%;">Description</th>
																<th style="width: 25%; text-align:center;">Quantity</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
														<?php
														$quantity = 0;
														foreach($delivery_order->items as $index => $item) {
															$quantity += $item->quantity;?>
                                                            <tr>
                                                                <td align="center"><?php echo ++ $key; ?></td>
                                                                <td><?php echo $item->pdt_code; ?></td>
                                                                <td><?php echo $item->product_description; ?></td>
                                                                <td align="center"><?php echo $item->quantity; ?></td>
                                                            </tr>
														<?php }?>
														<tr>
															<th colspan="3" style="text-align: right; padding-right: 20px;">Total</td>
															<th style="text-align: center;"><?php echo $quantity; ?></th>
														</tr>
                                                        </tbody>
                                                    </table>	
                                                </div>
                                            </div>
                                        </div>
									<?php } ?>
                                </div>				
                            </div>								
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT --><br><br>
</div><!-- END MAIN PANEL -->