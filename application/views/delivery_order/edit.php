<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
            </span> 
        </span>
        
        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>
    
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if(!empty($success)) {?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                            <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                            <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php }
                else if(!empty($error)) {?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error;?>
                    </div>
                <?php }?>&nbsp;
            </div>
        </div>

        <form class="edit_form" id="sales_order" action="<?php echo $form_action;?>" method="post" data-form_submit="0">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <a href="<?php echo $list_link; ?>" class="large_icon_des" style=" color: white;">
                            <button type="button" class="btn btn-primary btn-circle btn-xl temp_color ">
                                <i class="glyphicon glyphicon-list"></i>
                            </button>
                        </a>
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Sales Order Edit </h2>
                            </header>

                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" id ="sales_form">
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <input type="hidden" class="form-control" name="so_id" value="<?php echo $sales_order->id;?>" />
                                                    <input type="hidden" class="form-control" name="payment_status" value="<?php echo $sales_order->payment_status;?>" />
                                                    
                                                    <section>
                                                        <label class="label">Bill To<span style="color: red;">*</span></label>
                                                        <label class="select">
                                                            <select name="customer_name" id="customer_name" class="select2">
                                                                <option value="">Select Customer name</option>
                                                                <?php foreach($customers as $key => $value) {?>     <option value="<?php echo $value->id;?>" <?php echo ($value->id == $sales_order->customer_id) ? 'selected' : '' ;?>><?php echo $value->name;?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                    </section>
													
                                                    <div class="row">
                                                        <section class="cust_address col col-md-12" style="display: block;">
                                                            <p class="alert alert-info not-dismissable"> Address: <?php echo $customer->address;?></p>
                                                        </section>
													</div>
													
                                                    <section>
                                                        <label class="label">Ship To<span style="color: red;">*</span></label>
                                                        <label class="select">
                                                            <select name="ship_to" id="ship_to" class="select2">
                                                                <option value="">Select Location</option>
                                                                <?php foreach($addresses as $key => $value) {?>     <option value="<?php echo $value->id;?>" <?php echo ($value->id == $sales_order->shipping_address) ? 'selected' : '' ;?>><?php echo $value->location_name;?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Your Reference</label>
                                                        <label class="input">
                                                            <input type="text" name="remarks" class="form-control" value="<?php echo $sales_order->your_reference;?>" data-placeholder="Your Reference" />
                                                        </label>
                                                    </section>
                                                 </section>
                                                 
                                                 <section class="col col-6">
                                                    <section>
                                                        <label class="label">Sales Order #</label>
                                                       <label class="select">
                                                            <input type="text" name="so_no" class="form-control" value="<?php echo $sales_order->so_no;?>" data-placeholder="" readonly />
                                                        </label>
                                                    </section>
                                                    
                                                    <section>
                                                        <label class="label">Date<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="so_date" id="so_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php echo date('d/m/Y', strtotime($sales_order->so_date));?>" />
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Payment Terms</label>
                                                        <label class="input">
                                                            <input type="text" name="payment_terms" id="payment_terms" class="form-control" value="<?php echo $sales_order->payment_terms;?>" data-placeholder="Payment Terms" />
                                                        </label>
                                                    </section>
                                                 </section>
                                             </div>
                                        </fieldset>
                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Item Details</legend>
                                            <div class="row product_search">
                                                <section class="col col-5">
                                                    <div class="input hidden-mobile">
                                                        <input name="search_product" class="form-control" type="text" data-placeholder="Scan / Search product by code or name" id="search_product" />
                                                        <!--<div class="input-group-btn">
                                                            <button type="button" class="btn btn-default" onclick="return get_product_details();">
                                                                &nbsp;&nbsp;&nbsp;<i class="fa fa-fw fa-search"></i>&nbsp;&nbsp;&nbsp;
                                                            </button>
                                                        </div>-->
                                                    </div>
                                                </section>
                                            </div>
                                            <?php 
        #echo '<pre>';print_r($sales_order);print_r($sales_order_items);print_r($items);echo '</pre>';?>
                                             <div class="row material-container product_details">
                                                <div class="items_head col-xs-12 clr">
                                                    <section class="col col-3">
                                                        <label class="label"><b>Brand Name</b></label>
                                                    </section>
                                                    <section class="col col-3">
                                                        <label class="label"><b>Item Description</b></label>
                                                    </section>
                                                    <section class="col col-1">
                                                        <label class="label"><b>Quantity</b></label>
                                                    </section>
                                                    <?php $unit_cost = (in_array(7, $permission));?>
                                                    <section class="col col-<?php echo ($unit_cost) ? '1' : '2';?>">
                                                        <label class="label"><b>Unit Price</b></label>
                                                    </section>
                                                    
                                                    <?php if ($unit_cost) {?>
                                                        <section class="col col-1">
                                                            <label class="label"><b>Unit Cost</b></label>
                                                        </section>
                                                    <?php }?>
                                                    
                                                    <section class="col col-2">
                                                        <label class="label"><b>Amount</b></label>
                                                    </section>
                                                </div>
                                                
                                                <?php $item_count = count($sales_order_items);?>
                                                <?php foreach($sales_order_items as $item_key => $item) {?>
                                                    <div class="items col-xs-12 clr">
                                                        <input type="hidden" name="hidden_soitem_id[<?php echo $item_key;?>]" value="<?php echo $item->id;?>" />
                                                        <section class="col col-3">
                                                            <label class="select">
                                                                <select name="category_id[<?php echo $item_key;?>]" class="select2 category_id">
                                                                    <option value="">Select</option>
                                                                    <?php foreach($categories as $key => $category) {?>
                                                                    <option value="<?php echo $category->id;?>" <?php echo ($item->category_id == $category->id) ? 'selected' : '';?>><?php echo $category->cat_name;?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </label>
                                                        </section>
                                                        
                                                        <section class="col col-3">
                                                            <label class="select">
                                                                <select name="product_id[<?php echo $item_key;?>]" class="select2 product_id">
                                                                    <option value="">Select Item Description</option>
                                                                    <?php foreach($items as $key => $prod) {?>
                                                                        <?php if ($prod->cat_id == $item->category_id) {?>
                                                                            <option value="<?php echo $prod->id;?>" data-selling_price="<?php echo $prod->selling_price;?>" data-buying_price="<?php echo $prod->buying_price;?>" <?php echo ($prod->id == $item->item_id) ? 'selected' : '';?>><?php echo $prod->pdt_code . ' / ' . $prod->pdt_name;?></option>
                                                                        <?php }?>
                                                                    <?php } ?>
                                                                </select>
                                                            </label>
                                                        </section>
                                                        
                                                        <section class="col col-1">
                                                            <label class="input">
                                                                <input type="text" name="quantity[<?php echo $item_key;?>]" class="quantity float_value" value="<?php echo $item->quantity;?>" data-placeholder="0" />
                                                                <input type="hidden" name="old_quantity[<?php echo $item_key;?>]" class="quantity float_value" value="<?php echo $item->quantity;?>" data-placeholder="0" />
                                                            </label>
                                                            <span class="unit_label"></span>
                                                        </section>
                                                        
                                                        <section class="col col-<?php echo ($unit_cost) ? '1' : '2';?>" style="padding-left: 5px; padding-right: 5px;">
                                                            <label class="input">
                                                                <!--<i class="icon-prepend fa fa-dollar"></i>-->
                                                                <input type="text" name="price[<?php echo $item_key;?>]" class="price" value="<?php echo $item->price;?>" data-placeholder="0.00" readonly="" />
                                                            </label>
                                                        </section>
                                                        
                                                        <section class="col col-1" style="padding-left: 5px; padding-right: 5px; display: <?php echo ($unit_cost) ? 'block' : 'none';?>;">
                                                            <label class="input" style="margin-top: 1px;">
                                                                <!--<i class="icon-prepend fa fa-dollar"></i>-->
                                                                <input type="text" name="cost[<?php echo $item_key;?>]" class="cost" value="<?php echo $item->unit_cost;?>" data-placeholder="0.00" readonly="" />
                                                            </label>
                                                        </section>
                                                        
                                                        <section class="col col-2">
                                                            <label class="input">
                                                                <i class="icon-prepend fa fa-dollar"></i>
                                                                <input type="text" name="amount[<?php echo $item_key;?>]" class="amount" value="<?php echo $item->total;?>" data-placeholder="0.00" readonly="" />
                                                            </label>
                                                        </section>
                                                        
                                                         <section class="col col-1">
                                                            <label class="input">
                                                                <?php if ($item_count == ($item_key + 1)) { ?>
                                                                    <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                                <?php } ?>
                                                                <?php #if ($item_count > 1) { ?>
                                                                    <i class="fa fa-2x fa-minus-circle remove-row"></i>
                                                                <?php #} ?>
                                                            </label>
                                                        </section>
                                                    </div>
                                                <?php }?>
                                            </div>
                                            <div class="row" style="margin-top: 2%;">
                                               <section class="col col-6">
                                                </section>
                                                <section class="col col-3">
                                                <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Sub Total</b></label>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="sub_total" class="sub_total" value="<?php echo $sales_order->sub_total;?>" readonly="" data-placeholder="0.00" />
                                                    </label>
                                                </section>
                                                <section class="col col-1">
                                                </section>
                                            </div>
                                            
                                            <div class="row" style="margin-top: 2%;">
                                               <section class="col col-6">
                                                </section>
                                                <section class="col col-3">
                                                <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Discount</b></label>
                                                </section>
                                                <section class="col col-2">
                                                    <!--<label class="input">
                                                        <i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="sub_total" class="sub_total" readonly="" data-placeholder="0.00" />
                                                    </label>-->
                                                    
                                                    <div class="input-group">
                                                        <input type="text" name="discount" class="form-control discount" value="<?php echo $sales_order->discount;?>" data-placeholder="0.00" />
                                                        <span class="input-group-addon">
                                                            <span class="onoffswitch">
                                                                <input type="checkbox" name="discount_type" class="onoffswitch-checkbox discount_type" id="discount_type-0" value="1" <?php echo ($sales_order->discount_type) ? 'checked' : '';?> />
                                                                <label class="onoffswitch-label" for="discount_type-0"> 
                                                                    <span class="onoffswitch-inner" data-swchon-text="%" data-swchoff-text="S$"></span> 
                                                                    <span class="onoffswitch-switch"></span> 
                                                                </label> 
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <input type="hidden" name="discount_amt" class="discount_amt" value="<?php echo $sales_order->discount_amount;?>" />
                                                    <span class="discount_container">
                                                        <span class="currency">S$</span>
                                                        <span class="discount_amount">0.00</span>
                                                    </span>
                                                </section>
                                                <section class="col col-1">
                                                </section>
                                            </div>
                                            
                                            <div class="row" style="margin-top: 2%;">
                                               <section class="col col-6">
                                                </section>
                                                <section class="col col-3">
                                                <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Total Amount</b></label>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="total_amt" class="total_amt" value="<?php echo $sales_order->total_amt;?>" readonly="" data-placeholder="0.00" />
                                                    </label>
                                                </section>
                                                <section class="col col-1">
                                                </section>
                                            </div>
                                            
                                            <div class="row gst_details">
                                                <section class="col col-4">
                                                </section>
                                                <section class="col col-2">&nbsp;</section>
                                                <section class="col col-3">
                                                    <section>
                                                        <label class="label">&nbsp;</label>
                                                        <div class="inline-group">
                                                            <label class="radio">
                                                                <input type="radio" class="gst_type" name="gst_type" value="1" <?php echo (!empty($sales_order->gst)) ? 'checked' : '';?> /><i></i>Include GST
                                                             </label>
                                                            <label class="radio">
                                                                <input type="radio" class="gst_type" name="gst_type" value="2" <?php echo (empty($sales_order->gst)) ? 'checked' : '';?> /><i></i>Exclude GST
                                                           </label>
                                                        </div>
                                                    </section>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="label">&nbsp;</label>
                                                    <label class="input change_perc_disamt"><i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="gst_amount" class="gst_amount float_value" value="<?php echo $sales_order->gst_amount;?>" data-placeholder="0.00" readonly="" />
                                                        <input type="hidden" value="<?php echo $sales_order->gst_percentage;?>" name="hidden_gst" id="hidden_gst" />
                                                    </label>
                                                    <span class="gst_val" style="display: <?php echo (!empty($sales_order->gst)) ? 'block' : 'none';?>">GST @ <span class="gst_percentage"><?php echo $sales_order->gst_percentage;?></span> %</span>
                                                </section>
                                            </div>
                                        </fieldset>
                                        
                                        <footer>
                                            <button type="submit" class="btn btn-primary" >Submit</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>
                                    <div class="smart-form" id ="payment_form" style="display: none;">
                                        <fieldset>
                                            <div class="row">
                                                <section class="col col-md-6">
                                                    <section>
                                                        <label class="label">Total Amount</label>
                                                    </section>
                                                </section>
                                                <section class="col col-md-6">
                                                    <section>
                                                        <label class="input">S$ <span class="payment_amount"><?php echo $sales_order->total_amt;?></span></label>
                                                    </section>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-md-6">
                                                    <section>
                                                        <label class="label">Payment Type</label>
                                                    </section>
                                                </section>
                                                <section class="col col-md-6">
                                                    <section>
                                                        <label class="select">
                                                            <select name="payment_type"  class="select2 payment_type" id="payment_type">
                                                                <option value="">Select Payment Type</option>
                                                                <option value="1" <?php echo ((!empty($payment->pay_pay_type)) && $payment->pay_pay_type == 1) ? '' : '';?>>Cash</option>
                                                                <option value="2" <?php echo ((!empty($payment->pay_pay_type)) && $payment->pay_pay_type == 2) ? '' : '';?>>Cheque</option>
                                                                <option value="3" <?php echo ((!empty($payment->pay_pay_type)) && $payment->pay_pay_type == 3) ? '' : '';?>>DD</option>
                                                                <option value="4" <?php echo ((!empty($payment->pay_pay_type)) && $payment->pay_pay_type == 4) ? '' : '';?>>Net Banking</option>
                                                            </select>
                                                        </label>
                                                    </section>
                                                    <section class="cheque_details">
                                                        <label class="label">Bank Name<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="form-control" name="cheque_bank_name" value="<?php echo (!empty($payment->pay_pay_bank)) ? $payment->pay_pay_bank : '';?>" data-placeholder="Bank Name" maxlength="100" />
                                                        </label>
                                                    </section>
                                                    <section class="cheque_details">
                                                        <label class="label">Cheque No<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="form-control" name="cheque_acc_no" value="<?php echo (!empty($payment->pay_pay_no)) ? $payment->pay_pay_no : '';?>" data-placeholder="Account No" maxlength="100" />
                                                        </label>
                                                    </section>
                                                    <section class="cheque_details">
                                                        <label class="label">Cheque Date<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="bootstrap-datepicker-comncls" name="cheque_date" value="<?php echo (!empty($payment->pay_pay_date)) ? date('d/m/Y', strtotime($payment->pay_pay_date)) : '';?>" data-placeholder="Cheque Date" maxlength="100" />
                                                        </label>
                                                    </section>
                                                    <section class="dd_details">
                                                        <label class="label">DD No<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="form-control" name="dd_no" value="<?php echo (!empty($payment->pay_dd_no)) ? $payment->pay_dd_no : '';?>" data-placeholder="DD No" maxlength="100" />
                                                        </label>
                                                    </section>
                                                    <section class="dd_details">
                                                        <label class="label">DD Date<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="bootstrap-datepicker-comncls" name="dd_date" value="<?php echo (!empty($payment->pay_dd_date)) ? date('d/m/Y', strtotime($payment->pay_dd_date)) : '';?>" data-placeholder="DD Date" maxlength="100" />
                                                        </label>
                                                    </section>
                                                </section>
                                            </div>
                                        </fieldset>
                                        <footer>
                                            <button type="button" class="btn btn-default" onclick="show_sales_form();">Back</button>
                                            <button type="submit" class="btn btn-success" id="pay_now" name="payment" value="pay_now" >Pay Now</button>
                                            <button type="submit" class="btn btn-warning" id="skip_payment" name="payment" value="skip_payment" onclick="disable_payment();">Skip Payment</button>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/app/sales_order.js"></script>