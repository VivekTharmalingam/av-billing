<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" style="width: 830px;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    &times;
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Choose print type</h4>
                            </div>
                            <div class="modal-body" style="padding: 50px 0 50px 15px;">                                        
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-3" style="text-align: center;">
                                            <a target="_blank" href="<?php echo base_url(); ?>invoice/pdf/<?php echo $sales_order->id; ?>"><button type="button" class="btn btn-success btn-lg btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Print Invoice
                                                </button></a>
                                        </div>
                                        <?php if($sales_order->invoice_from == 4) { ?>
											<div class="col-md-3" style="padding-left: 0; text-align: center;">
												<a href="javascript:void(0);"><button type="button" disabled="" class="btn btn-success btn-lg btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Print Delivery Order
													</button></a>
											</div>
                                        <?php }
										else { ?>
											<div class="col-md-3" style="padding-left: 0; text-align: center;">
												<a target="_blank" href="<?php echo $do_link; ?>"><button type="button" class="btn btn-success btn-lg btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Print Delivery Order
													</button></a>
											</div>
                                        <?php } ?>
                                        <div class="col-md-3" style="text-align: center;">
                                            <a target="_blank" onclick="printResults('<?php echo $sales_order->id; ?>', '<?php echo $browser['name']; ?>')">
												<button type="button" class="btn btn-lg btn-warning"><span class="glyphicon glyphicon-floppy-disk"></span> Thermal Print</button>
											</a>
                                        </div>
										<div class="col-md-3" style="text-align: center;">
											<a target="_blank" href="<?php echo $receipt_link; ?>">
												<button type="button" class="btn btn-success btn-lg btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Print Receipt</button>
											</a>
                                        </div>
                                    </div>
                                </div>
                            </div>                                    
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
					<a href="<?php echo base_url(); ?>invoice/" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="List"><i class="glyphicon glyphicon-list"></i></button></a>
				
					<?php if (in_array(6, $permission)) {?>
						<a target="_blank" href="#" data-toggle="modal" data-target="#myModal" class="large_icon_des" style=" color: white;margin-right: 80px;" ><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="Download"><i class="glyphicon glyphicon-save"></i></button></a>
					<?php }?>
					
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                        <header>
                            <ul class="nav nav-tabs pull-left in">
                                <li class="active">
                                    <a data-toggle="tab" class="tabcls1" href="#tab-1"> <span class="widget-icon"> <i class="fa fa-file-text-o"></i> </span> <span class="hidden-mobile hidden-tablet"> Invoice View </span> </a>
                                </li>
                                <?php if(count($return_items)>0) { ?> 
                                <li>
                                    <a data-toggle="tab" class="tabcls2" href="#tab-2">
                                        <i class="fa fa-repeat"></i>
                                        <b class="badge bg-color-gray bounceIn animated"><?php echo count($return_items);?></b>
                                        <span class="hidden-mobile hidden-tablet"> Invoice Returned Items</span>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                        </header>

                        <!-- widget div-->
                        <div role="content">
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body">
                                <div class="tab-content">
                                <div class="tab-pane active" id="tab-1">
                                <!--<span id="watermark">Malar ERP.</span>-->
                                <table id="user" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Invoice Number</td>
                                            <td ><?php echo $sales_order->so_no; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Invoice Date</td>
                                            <td ><?php echo text_date($sales_order->so_date); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Leave Blank</td>
                                            <td ><?php echo (empty($sales_order->leave_date)) ? 'No' : 'Yes'; ?></td>
                                        </tr>
                                        <!--<tr>
                                            <td style="width:35%;font-weight: bold;">Customer Type</td>
                                            <td ><?php echo ($sales_order->so_type == '1') ? 'Regular Customer' : 'Cash Customer'; ?></td>
                                        </tr>-->
                                        <tr>
                                            <td style="font-weight: bold;">Sold To</td>
                                            <td ><?php echo $sales_order->name; ?></td>
                                        </tr>
                                        <?php if ($sales_order->so_type == '1') { ?>
                                            <tr>
                                                <td style="font-weight: bold;">Deliver To</td>
                                                <td >
                                                    <!--Location Name:
                                                    <?php echo $shipping_address->location_name; ?>
                                                    <br />-->
                                                    Address: <?php echo text(nl2br($shipping_address->address)); ?>
                                                </td>
                                            </tr>
                                        <?php } else { ?>
                                            <tr>
                                                <td style="font-weight: bold;">Deliver To</td>
                                                <td ><?php echo $sales_order->name; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Customer Reference</td>
                                            <td ><?php echo nl2br(text($sales_order->your_reference)); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Payment Type</td>
                                            <td ><?php echo text($sales_order->payment_type); ?></td>
                                        </tr>
                                        <?php if ($sales_order->is_cheque == 1) { ?>
                                            <tr>
                                                <td style="width:35%;font-weight: bold;">Cheque Bank Name</td>
                                                <td ><?php echo text($sales_order->pay_pay_bank); ?></td>
                                            </tr>
                                            <tr>
                                                <td style="width:35%;font-weight: bold;">Cheque No.</td>
                                                <td ><?php echo text($sales_order->pay_pay_no); ?></td>
                                            </tr>
                                            <tr>
                                                <td style="width:35%;font-weight: bold;">Cheque Date</td>
                                                <td ><?php echo $sales_order->pay_date; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Delivered On</td>
                                            <td ><?php echo text_date($sales_order->do_date); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Remarks</td>
                                            <td ><?php echo nl2br(text($sales_order->remarks)); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Payment Status</td>
                                            <td ><?php echo $sales_order->payment_status_str; ?></td>
                                        </tr>
                                        <?php
                                        $view_url_lk = 'javascript:void(0)';
                                        if($sales_order->invoice_from==2) {
                                            $view_url_lk = base_url().'quotation/view/'.$sales_order->quotation_id;
                                        }else if($sales_order->invoice_from==3) {
                                            $view_url_lk = base_url().'exchange_order/view/'.$sales_order->exchange_order_id;
                                        }else if($sales_order->invoice_from==4) {
                                            $view_url_lk = base_url().'services/view/'.$sales_order->service_id;
                                        }
                                        ?>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Invoice From</td>
                                            <td ><a href="<?php echo $view_url_lk; ?>" target="_blank" style=" text-decoration: none;"><?php echo $sales_order->invoice_from_str; ?></a></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <?php if(count($service_cost)>0) { ?>
                                <div class="text-left">
                                    <span class="onoffswitch-title">
                                        <h3><b style="color: #8e233b;">Service Cost Details </b></h3>
                                    </span>
                                </div>
                                <div id="widget-tab-1" class="table-responsive">
                                    <table id="dt_basic_1" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
                                        <thead>
                                            <tr>
                                                <th data-class="expand" style="font-weight: bold; width: 9%; text-align:left;">S.No</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;width: 72%;text-align:left;">Description</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;width: 19%;text-align:right;" style="border-right: 1px solid #ddd;">Amount (S$)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($service_cost as $key => $item) {?>
                                                <tr>
                                                    <td><?php echo ++$key; ?></td>
                                                    <td><?php echo nl2br($item->product_description); ?>
														<?php if($item->external_cost > 0) {?>
															<span class="external_cost" style="font-size: 11px; color: #f00;">External Cost: <?php echo $item->external_cost;?></span>
															<span class="our_cost" style="font-size: 11px; color: #f00;">Our Cost: <?php echo $item->our_cost;?></span>
														<?php }?>
													</td>
                                                    <td align="right" style="border-right: 1px solid #ddd;"><?php echo number_format($item->total, 2); ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php } ?>
                                <div class="text-left">
                                    <span class="onoffswitch-title">
                                        <h3><b style="color: #8e233b;">Items Details </b></h3>
                                    </span>
                                </div>
                                <div id="widget-tab-1" class="table-responsive">
                                    <table id="dt_basic_2" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
                                        <?php if(count($sales_order_items)>0) { ?>
                                        <thead>
                                            <tr>
                                                <th data-class="expand" style="font-weight: bold; text-align:left;">S.No</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:left;">Item Code</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:left;">Description</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Quantity</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:right;">Price (S$)</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Discount</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:right;" style="border-right: 1px solid #ddd;">Amount (S$)</th>
                                            </tr>
                                        </thead>
                                        <?php } ?>
                                        <tbody>
                                            <?php if(count($sales_order_items)>0) {
												foreach ($sales_order_items as $key => $item) {
												$color = (empty($item->show_in_print)) ? 'color: #f00' : '';?>
                                                <tr>
                                                    <td style="<?php echo $color;?>"><?php echo ++ $key; ?></td>
                                                    <td style="<?php echo $color;?>"><?php echo $item->pdt_code; ?></td>
                                                    <td style="<?php echo $color;?>">
														<?php echo nl2br($item->product_description); ?>
														<?php echo (!empty($item->serial_no)) ? '<br />Serial No.: ' . $item->serial_no : ''; ?></td>
                                                    <td style="<?php echo $color;?>; text-align: center;"><?php echo $item->quantity; ?></td>
                                                    <td style="<?php echo $color;?>; text-align: right;"><?php echo number_format($item->price, 2); ?></td>
                                                    <td style="<?php echo $color;?>; text-align: right;">
                                                        <?php echo (($item->discount_type == 1) && ($item->discount_amount > 0)) ? (float) $item->discount . '%' : '';?> &nbsp; 
                                                        <?php echo number_format($item->discount_amount, 2); ?>
                                                    </td>
                                                    <td style="<?php echo $color;?>; text-align: right;" style="border-right: 1px solid #ddd;"><?php echo number_format($item->total, 2); ?></td>
                                                </tr>
                                            <?php } ?>
                                            <?php } else { ?>
                                                <tr>
                                                    <td colspan="7" style="text-align: center;"> no record found!.. </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="8">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <section class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                            <span style="float: right;">
                                                                <label style="color: #A90329;"><b>Sub Total</b></label>
                                                            </span>
                                                        </section>
                                                        <section class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                            <span style="float: right; margin-right: -25px;"><b>S$&nbsp;<?php echo number_format($sales_order->sub_total, 2); ?></b></span>
                                                        </section> 
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="8">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <section class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                            <span style="float: right;">
                                                                <label><b>Discount</b></label>
                                                            </span>
                                                        </section>
                                                        <section class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                            <span style="float: right; margin-right: -25px;"><b>S$&nbsp;<?php echo ($sales_order->discount_type == 1 && $sales_order->discount > 0) ? number_format($sales_order->discount_amount, 2) . ' (' . $sales_order->discount . ' %' . ' )' : number_format($sales_order->discount_amount, 2); ?></b></span>
                                                        </section>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php if ($sales_order->gst == 2 && !empty($company->comp_gst_no)) { ?>
                                                <tr>
                                                    <td colspan="8">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <section class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                                <span style="float: right;">
                                                                    <label>
                                                                        <b>ADD GST <?php echo $sales_order->gst_percentage . ' %'; ?></b>
                                                                    </label>
                                                                </span>
                                                            </section>
                                                            <section class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                                <span style="float: right; margin-right: -25px;">
                                                                    <b>S$&nbsp; <?php echo number_format($sales_order->gst_amount, 2); ?></b>
                                                                </span>
                                                            </section>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <tr>
                                                <td colspan="8">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <section class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                            <span style="float: right;">
                                                                <label style="color: #A90329;"><b>Total Amount</b></label>
                                                            </span>
                                                        </section>
                                                        <section class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                            <span style="float: right; margin-right: -25px;"><b>S$&nbsp;<?php echo number_format($sales_order->total_amt, 2); ?></b></span>
                                                        </section>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php if ($sales_order->gst == 1 && !empty($company->comp_gst_no)) { ?>
                                                <tr>
                                                    <td colspan="8" align="right">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            [<?php echo '<b style="font-size: 12px;">Inclusive of GST ' . $sales_order->gst_percentage . ' %</b> : $ ' . number_format($sales_order->gst_amount, 2); ?>]
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>

                                        </tfoot>
                                    </table>
                                    <br />
                                    <br />
                                </div>
                                </div>
                                <?php if(count($return_items)>0) { ?>    
                                <div class="tab-pane" id="tab-2">
                                <div class="text-left">
                                    <span class="onoffswitch-title">
                                        <h3><b style="color: #8e233b;">Invoice Returned Item Details </b></h3>
                                    </span>
                                </div>
                                <div id="widget-tab-1" class="table-responsive">
                                    <table id="dt_basic_2" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
                                        <thead>
                                            <tr>
                                                <th data-class="expand" style="font-weight: bold; text-align:center;">S.No</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Return Date</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Item code</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Description</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Returned Quantity</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Type</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Re-Stock Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($return_items as $key=>$item){?>
                                                 <tr>
                                                    <td><?php echo ++ $key;?></td>
                                                    <td><?php echo $item->return_date_str;?></td>
                                                    <td><?php echo $item->pdt_code;?></td>
                                                    <td><?php echo $item->pdt_desc;?></td>
                                                    <td align="center"><?php echo $item->returned_qty;?></td>
                                                    <td align="center"><?php echo $item->return_item_type_str;?></td>
                                                    <td align="center"><?php echo $item->restock_quantity;?></td>
                                                </tr>
                                           <?php } ?>
                                        </tbody>
                                    </table>
                                    <br />
                                    <br />
                                </div>    
                                </div>  
                               <?php } ?>     
                                </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->

<iframe id="thermal_content" src="<?php echo $thermal_src; ?>" style="display: none;"></iframe>
<script type="text/javascript">
    function printResults(id, browser_txt) {
        var content = '<html>';
        content += '<body>';
        content += $('#thermal_content').contents().find('body').html();
        content += '</body>';
        content += '</html>';
        var w = window.open();

        w.document.write(content);  //only part of the page to print, using jquery
        w.document.close(); //this seems to be the thing doing the trick
        w.focus();
        w.print();
        w.close();
        $('#myModal').modal('hide');
    }
</script>