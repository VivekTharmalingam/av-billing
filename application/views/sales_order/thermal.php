<html>
    <head>
        <link href="<?php echo base_url(); ?>/assets/css/plugin/barcode.css" rel="stylesheet" type="text/css">
        <script src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/plugin/JsBarcode/EAN_UPC.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/plugin/JsBarcode/CODE128.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/plugin/JsBarcode/JsBarcode.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".barcode3").JsBarcode(
                        $('.invoice_no').html(),
                        {
                            width: 1, height: 30, displayValue: false, color: '424242', fontSize: 20
                        }
                );
            });
        </script>
    </head>
    <body>
        <?php
            $address = $branch->address;
            $tel_pos = strripos($address, 'FAX');
            $addr = trim(substr($address, 0, $tel_pos), ' ');
            $addr = trim($addr, '|');
            $telephone = substr($address, $tel_pos);
            $branch->address = $addr . "\n" . $telephone;
            ?>
        <div style="width: 302px; border: 1px solid #bbb; padding: 10px; margin: auto; font-family: calibri; font-size: 18px; color: #000;">
            <div style="text-align:center;">
                <?php
                $comp_logo = (!empty($company->comp_logo) && file_exists(UPLOADS . $company->comp_logo) ) ? base_url() . UPLOADS . $company->comp_logo : 'assets/images/logo.png';
                ?>
                <img src="<?php echo $comp_logo; ?>" width="110" height="113" alt="<?php echo $company->comp_name; ?>" />
                 <p style="text-align: center;font-size:17px; font-weight: bold;"><?php echo strtoupper($company->comp_name); ?></p>
            </div>          

            <p style="text-align: center; margin:-30px 0px 0px 0px;font-size:16px;"><?php echo strtoupper(nl2br($branch->address)); ?></p>
            <p style="margin: 0px 0px 0px 0px; text-align: center;font-size:13px;">EMAIL: <?php echo strtoupper($company->comp_email); ?></p>
            <p style="margin: 0px 0px 10px 0px; text-align: center;font-size:13px;">WEBSITE: <?php echo strtoupper($company->comp_website); ?></p>
            
            <?php if (!empty($company->comp_gst_no)) { ?>
                <p style="margin: 0px 0px 0px 0px; text-align: center;font-size:13px;">GST REG NO.: <?php echo strtoupper($company->comp_gst_no); ?></p>
            <?php } ?>

            <p style="margin: 0px 0px 0px 0px; text-align: center;font-size:13px;">BUSINESS REG NO.: <?php echo strtoupper($company->comp_reg_no); ?></p>

            <p style="margin: 0px 0px 10px 0px; border-bottom: 1px dashed #333; height: 20px;"></p>
            <h3 style="text-align: center; font-size: 18px; margin:0;">INVOICE</h3>
            <p style="margin: 10px 0px 15px 0px; border-bottom: 1px dashed #333; height: 1px;"></p>
            <input type="hidden" name="so_no" value="<?php echo strtoupper($sales_order->so_no); ?>" />
            <table style="width: 100%; border-collapse: collapse;" cellpadding="0">
                <tr>
                    <td width="50%" valign="top">INVOICE NUMBER</td>
                    <td width="5%"> : </td>
                    <td width="45%"><b class="invoice_no"><?php echo strtoupper($sales_order->so_no); ?></b></td>
                </tr>
				<?php if (!empty($job_sheet)) {?>
					<tr>
						<td valign="top">JOB SHEET NUMBER</td>
						<td> : </td>
						<td><?php echo strtoupper(text($job_sheet->jobsheet_number, '--')); ?></td>
					</tr>
					<tr>
						<td valign="top" style="font-size: 13.5px;">BRAND / PRODUCT NAME</td>
						<td> : </td>
						<td style="font-size: 12px;">
							<?php echo strtoupper(text($job_sheet->cat_name, '')); ?>
							<?php echo (!empty($job_sheet->cat_name)) ? ' / ' : ''; ?>
							<?php echo strtoupper(text($job_sheet->material_name, '')); ?>
						</td>
					</tr>
					<tr>
						<td valign="top" style="font-size: 12.5px;">PRODUCT TYPE / MODEL NO.</td>
						<td> : </td>
						<td style="font-size: 12.5px;"><?php echo strtoupper(text($job_sheet->material_type, '--')); ?></td>
					</tr>
				<?php }?>
                
                <tr>
                    <td valign="top">INVOICE DATE</td>
                    <td> : </td>
                    <td><?php echo (empty($sales_order->leave_date)) ? text_date($sales_order->so_date, '', 'd-m-Y') : ''; ?></td>
                </tr>
                <tr>
                    <td valign="top">SOLD TO</td>
                    <td valign="top"> : </td>
                    <td  style="vertical-align: top;"><?php echo strtoupper(text($sales_order->name, '--')); ?></td>
                </tr>
            </table>

            <table style="width: 100%; border-collapse: collapse;" cellpadding="0">
                <tr>
                    <td colspan="4"><p style="margin: 0px 0px 10px 0px; border-bottom: 1px dashed #333; height: 25px;"></p></td>
                </tr>
                <tr class="head">
                    <td width="55%" style="padding-right: 5px;">SERVICES / DESC</th>
                    <td width="10%" align="right" style="padding: 0 5px;">QTY</td>
                    <td width="15%" align="right" style="padding: 0 5px;">PRICE</td>
                    <td width="20%" align="right" style="padding: 0 5px;">TOTAL</td>
                </tr>

                <tr>
                    <td colspan="4"><p style="margin: 0px 0px 10px 0px; border-bottom: 1px dashed #333; height: 15px;"></p></td>
                </tr>

                <?php foreach ($so_items as $key => $item) { ?>
                    <?php $pc = ($item->quantity > 1) ? ' PCS' : ' PC'; ?>
                    <tr>
                        <td colspan="4" style="padding-right: 5px;">
                            <?php echo strtoupper($item->pdt_code); ?>
                            <?php echo (!empty($item->pdt_code) && (!empty($item->product_description))) ? ' / ' : ' '; ?>
                            <?php echo strtoupper(nl2br($item->product_description)); ?>
							<?php echo (!empty($item->serial_no)) ? '<br />Serial No.: ' . $item->serial_no : ''; ?></td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right; padding: 0 5px;">
							
							<?php echo ((!empty($item->show_in_print)) && ($item->service_form_type != '3')) ? $item->quantity . $pc : '';?>
						</td>
                        <td align="right" style="padding: 0 5px;">
							<?php echo ((!empty($item->show_in_print)) && ($item->service_form_type != '3')) ? number_format($item->price, 2) : '';?>
						</td>
                        <td align="right" style="padding: 0 5px;">
							<?php echo (!empty($item->show_in_print)) ? number_format($item->total, 2) : '';?>
						</td>
                    </tr>
                    <tr>
                        <td colspan="4" height="10"></td>
                    </tr>
                <?php } ?>
               <?php foreach ($service_cost as $key => $item) { ?>
                    <tr>
                        <td colspan="4" style="padding-right: 5px;">
                            <?php echo strtoupper(nl2br($item->product_description)); ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right; padding: 0 5px;">--</td>
                        <td align="right" style="padding: 0 5px;">--</td>
                        <td align="right" style="padding: 0 5px;"><?php echo text_amount($item->total); ?></td>
                    </tr>
                    <tr>
                        <td colspan="4" height="10"></td>
                    </tr>
                <?php } ?>     
                <tr>
                    <td colspan="4">
                        <p style="margin: 20px 0 20px 0px; border-bottom: 1px dashed #333; height: 2px;"></p>
                    </td>
                </tr>
            </table>
            <table style="width: 100%; border-collapse: collapse;" cellpadding="0">
                <?php if ($sales_order->gst == 2 || $sales_order->discount > 0) { ?>
                    <tr>
                        <td align="left" width="50%">SUB-TOTAL</td>
                        <td width="5%"> : </td>
                        <td align="right" width="45%">$ <?php echo text_amount($sales_order->sub_total); ?></td>
                    </tr>
                    <?php if ($sales_order->discount > 0) { ?>
                        <tr>
                            <td align="left">DISCOUNT <?php echo ($sales_order->discount_type == 1 && $sales_order->discount > 0) ? ' @ (' . $sales_order->discount . ' %' . ' )' : ''; ?></td>
                            <td> : </td>
                            <td align="right">$ <?php echo text_amount($sales_order->discount_amount); ?></td>
                        </tr>
                    <?php } ?>

                    <?php if ($sales_order->gst == 2) { ?>
                        <tr>
                            <td align="left">GST (<?php echo $sales_order->gst_percentage; ?> %)</td>
                            <td> : </td>
                            <td align="right">$ <?php echo text_amount($sales_order->gst_amount); ?></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td align="left" width="50%">TOTAL</td>
                        <td width="5%"> : </td>
                        <td align="right" width="45%"><b>$ <?php echo text_amount($sales_order->total_amt); ?></b></td>
                    </tr>
                <?php } else {
                    ?>
                    <tr>
                        <td align="left" width="50%">TOTAL</td>
                        <td width="5%"> : </td>
                        <td align="right" width="45%"><b>$ <?php echo text_amount($sales_order->total_amt); ?></b></td>
                    </tr>
                <?php } ?>
                <?php if ($sales_order->gst == 1 && !empty($company->comp_gst_no)) { ?>
                    <tr>
                        <td align="right" colspan="3">[<?php echo 'Inclusive of GST ' . $sales_order->gst_percentage . ' % : $ ' . text_amount($sales_order->gst_amount); ?>]
                        </td>
                    </tr>
                <?php } ?>
                <?php if (!empty($sales_order->payment_type)) { ?>
                    <tr>
                        <td colspan="3" height="5"></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">PAYMENT MODE</td>
                        <td style="vertical-align: top;"> : </td>
                        <td align="right" style="vertical-align: top;"><?php echo strtoupper($sales_order->payment_type); ?></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td colspan="3"><p style="margin-bottom: 10px; border-bottom: 1px dashed #333; height: 20px;margin:0px 0px 20px 0px;"></p></td>
                </tr>
                <tr>
                    <th colspan="3" style="font-size: 16px;">THANK YOU FOR SHOPPING WITH US</th>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: center;">
                        <p style="margin-bottom: 0; padding-top: 10px;">Also buy our products through online</p>
                        <img src="<?php echo base_url(); ?>assets/images/thermal/bottom_logo.png" style="max-width: 100%;" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" height="15"></td>
                </tr>
            </table>

            <img class="barcode3" width="100%" height="30px" />
        </div>
    </body>
</html>