<table width="100%" border="0" style="font-family: calibri; border-collapse:collapse; font-size: 11px;" cellpadding="5">

	<tr>

		<td style="text-align:left; vertical-align: bottom; width: 48%">

			<table style="font-size: 11px; width: 100%;" cellpadding="0" cellspacing="0">

				<tr>

					<td border="0">1.</td>

					<td border="0" style="padding-left: 2px;">No Warranty for Physical Damage & Burnt Case</td>

				</tr>

				<tr>

					<td border="0" style="vertical-align: top;">2.</td>

					<td border="0" style="padding-left: 2px;">Goods sold are not refundabale or non-exchangeable</td>

				</tr>

				<tr>

					<td border="0" style="vertical-align: top;">3.</td>

					<td border="0" style="padding-left: 2px;">No Warranty will be accepted without Sales Invoice</td>

				</tr>
                
                <tr>

					<td border="0" style="vertical-align: top;">4.</td>

					<td border="0" style="padding-left: 2px;">By Selling above parts, we are not responsible for any incompatibility with existing parts</td>

				</tr>

				<tr>

					<td colspan="2" border="0" style="padding-top: 30px; vertical-align: bottom;"><i>System generated invoice no signature required.</i></td>

				</tr>

			</table>

		</td>      

        <td style="width: 1%;"></td>		

		<td style="text-align: right; width: 50%;">

			<table style="font-size: 12px; width: 100%;" cellpadding="0" cellspacing="0">

				<tr>

					<th colspan="3" style="text-align: left; padding-bottom: 25px; font-size: 15px;">GOODS RECEIVED IN GOOD ORDER AND CONDITION</th>

				</tr>

				<tr>

					<td border="0" style="width: 80px; vertical-align: bottom;">Name / Contact</td>

					<td style="width: 5px; vertical-align: bottom;">:</td>

					<td border="0" style="padding-left: 5px; border-bottom: 1px solid #000;"></td>

				</tr>

				<tr>

					<td colspan="3" height="20"></td>

				</tr>

				<tr>

					<td border="0" style="vertical-align: bottom;">Date</td>

					<td style="vertical-align: bottom;">:</td>

					<td border="0" style="padding-left: 5px; border-bottom: 1px solid #000;"></td>

				</tr>

				<tr>

					<td colspan="3" height="20"></td>

				</tr>

				<tr>

					<td border="0" style="vertical-align: bottom;">STAMP & SIGN</td>

					<td style="vertical-align: bottom;">:</td>

					<td border="0" style="border-bottom: 1px solid #000; height: 40px;"></td>

				</tr>

			</table>

		</td>

	</tr>

	<tr>

		<td colspan="3" style="text-align: center">

			<div class="barcodecell">

				<barcode code="<?php echo strtoupper($sales_order->so_no); ?>" type="C39" height="0.55" class="barcode" />

                <!--EAN128C-->

			</div>

		</td>

	</tr>

	<tr>

		<td colspan="3" align="center">Page {PAGENO}/{nbpg}</td>

    </tr>

</table>

</body>

</html>