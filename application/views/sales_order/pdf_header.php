<html>

    <head>

        <style>

            @page {

                size: portrait;

                header: html_header;

                footer: html_footerPageNo;

                margin-footer: 0;

                margin-bottom: 55mm;

            }



            .barcode {

                padding: 1.5mm;

                margin: 0;

                vertical-align: top;

                color: #424242;

                width:200px;

            }

        </style>

    </head>

    <body>

    <htmlpagefooter name="footerPageNo" style="display:none">

        <table width="100%" style="vertical-align: bottom; font-family: calibri; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">

            <tr>

                <td width="100%" align="center">Page {PAGENO}/{nbpg}</td>

            </tr>

        </table>

    </htmlpagefooter>



    <htmlpageheader name="header" style="display:block">

        <table width="100%" border="0" style="font-family: calibri;">

            <tr>

                <td width="50%" style="vertical-align:top; padding: 0;">

                    <?php

                    $comp_logo = (!empty($company->comp_logo) && file_exists(UPLOADS . $company->comp_logo) ) ? base_url() . UPLOADS . $company->comp_logo : 'assets/images/logo.png';

                    ?>

                    <table width="100%" cellpadding="0" cellspacing="0">

                        <tr>

                            <td><img src="<?php echo $comp_logo; ?>" width="240" height="92" alt="<?php echo $company->comp_name; ?>" /></td>

                        </tr>

                        <tr>

                            <td style="font-size: 13px;">

                                <?php if (!empty($company->comp_gst_no)) { ?>

                                    <label style="margin-right: 10px;">GST No.: <?php echo $company->comp_gst_no; ?></label>

                                <?php } ?>

                                <label>Business Reg. No.: <?php echo $company->comp_reg_no; ?></label>

                            </td>

                        </tr>

                        <tr>

                            <td style="padding: 0;">

                                <table width="350" border="0" cellspacing="0" style="font-size: 14px;">

                                    <tr>

                                        <td height="100" style="padding:10px; border-top: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000;border-bottom: 1px solid #000; vertical-align: top;">

                                            <b style="font-size: 15px;">Sold To</b>

                                            <div><?php echo strtoupper($sales_order->name); ?></div>

                                            <?php if ($sales_order->so_type == '1') { ?>

                                                <div><?php echo strtoupper(nl2br($sales_order->address)); ?></div>

                                            <?php } ?>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td height="5"></td>

                                    </tr>

                                    <tr>

                                        <td height="100" style="padding:10px; border-top: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; vertical-align: top;">

                                            <b style="font-size: 15px;">Deliver To</b>



                                            <?php // echo strtoupper($sales_order->name); ?>

                                            <?php if ($sales_order->so_type == '1') { ?>

                                                <div><?php echo strtoupper($shipping_address->location_name); ?></div>

                                                <div><?php echo strtoupper(nl2br($shipping_address->address)); ?></div>

                                            <?php } else {

                                                ?>

                                                <div><?php echo strtoupper(nl2br($sales_order->address)); ?></div>

                                            <?php } ?>

                                        </td>

                                    </tr>



                                    <!--<tr>

                                        <td style="font-size: 14px;">

                                            <b style="padding-right: 20px;">Issued By</b>

                                            <span style="padding-left: 10px; padding-right: 10px;">:</span>

                                            <span style="font-size: 13px;"><?php echo strtoupper($sales_order->uname); ?></span>

                                        </td>

                                    </tr>-->

                                </table>

                            </td>

                        </tr>

                    </table>

                </td>

                <td width="50%" style="vertical-align:top; padding: 0;">

                    <table width="100%" cellpadding="0" cellspacing="0" style="font-size: 11px;">

						<tr>

                            <td style="font-size: 16px; color: #2AB2E7;"><b><?php echo $company->comp_name; ?></b></td>

                        </tr>

                        

                        <tr>

                            <td style="font-size: 13px;"><b><?php echo nl2br($branch->address); ?></b></td>

                        </tr>

                        <tr>

                            <td style="text-align: left;">

                                <label><b style="font-size: 12px;">Email: <?php echo $company->comp_email; ?></b>&nbsp;&nbsp; </label><br>

                                <label><b style="font-size: 12px;">Website: <?php echo $company->comp_website; ?></b></label>

                            </td>

                        </tr>

                        <tr>

                            <td align="center" style="font-style:italic; font-size:21px; padding:10px 0px;font-weight:bold;">Invoice / Delivery Order</td>

                        </tr>

                        <tr>

                            <td style="padding: 0;">

								<?php $height = (!empty($job_sheet)) ? 'height: 29px;' : 'height: 35px;';?>

                                <table width="500" border="1" cellpadding="5" cellspacing="0" style="font-size: 14px; border-collapse:collapse;">

                                    <tr>

                                        <td style="font-weight:bold; width: 175px; <?php echo $height;?>"><b style="font-size: 15px;">Invoice No.</b></td>

                                        <td><?php echo strtoupper($sales_order->so_no); ?></td>

                                    </tr>

                                    <tr>

                                        <td style="font-weight:bold; border-top: 0; <?php echo $height;?>"><b style="font-size: 15px;">Date</b></td>

                                        <td style="border-top: 0;"><?php echo (empty($sales_order->leave_date)) ? text_date($sales_order->so_date, '', 'd.m.Y') : ''; ?></td>

                                    </tr>

									<?php if (!empty($job_sheet)) {?>

										<tr>

											<td style="font-weight:bold;border-top: 0; <?php echo $height;?>"><b style="font-size: 15px;">Job Sheet No.</b></td>

											<td style="border-top: 0;"><?php echo strtoupper(text($job_sheet->jobsheet_number, '--')); ?></td>

										</tr>

										<tr>

											<td style="font-weight:bold;border-top: 0; <?php echo $height;?>"><b style="font-size: 15px;">Brand / Product Name</b></td>

											<td style="border-top: 0;">

												<?php echo strtoupper(text($job_sheet->cat_name, '')); ?>

												<?php echo (!empty($job_sheet->cat_name)) ? ' / ' : ''; ?>

												<?php echo strtoupper(text($job_sheet->material_name, '')); ?>

											</td>

										</tr>

										<tr>

											<td style="font-weight:bold;border-top: 0; <?php echo $height;?>"><b style="font-size: 15px;">Product Type / Model No.</b></td>

											<td style="border-top: 0;"><?php echo strtoupper(text($job_sheet->material_type, '--')); ?></td>

										</tr>

									<?php }?>

                                    

                                    <tr>

                                        <td style="font-weight:bold;border-top: 0; <?php echo $height;?>"><b style="font-size: 15px;">Your Reference</b></td>

                                        <td style="border-top: 0;"><?php echo strtoupper(text($sales_order->your_reference, '--')); ?></td>

                                    </tr>

                                </table>

                            </td>

                        </tr>

                    </table>

                </td>

            </tr>

        </table>

    </htmlpageheader>