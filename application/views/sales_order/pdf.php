<table width="100%" border="1" style="border-collapse:collapse; font-family: calibri;" cellpadding="5">
    <thead>
        <tr>
            <th width="16%" style="font-size: 15px;">Item Code</th>
            <th width="49%" style="font-size: 15px;">Services / Description</th>
            <th width="7%" style="font-size: 15px;">Qty</th>   
            <th width="14%" style="font-size: 15px;">Unit Price ($)</th>
            <th width="14%" style="font-size: 15px;">Amount ($)</th>
        </tr>
    </thead>
    <tfoot style="display: table-footer-group">
        <tr>
            <td colspan="5" style="border-left: none;border-right: none;border-bottom: none;font-size: 15px; text-align: center; padding:0px; height: 1px;"></td>
        </tr>
    </tfoot>
    <tbody>
        <?php $height = 410;//($sales_order->invoice_from != 4) ? 410 : 170; ?>
		<?php $itm_total = 0;?>
        <?php foreach ($so_items as $key => $item) {
			$itm_total += $item->total; ?>
            <tr style="vertical-align:top;">
                <td style="border-top: 0; border-bottom: none; font-size: 14px;"><?php echo text(strtoupper($item->pdt_code), 'SERVICE'); ?></td>
                <td style="border-top: 0;border-bottom: none; font-size: 14px;">
					<?php echo nl2br(strtoupper($item->product_description)); ?>
					<?php echo (!empty($item->serial_no)) ? '<br />Serial No.: ' . $item->serial_no : ''; ?></td>
				</td>
                <td align="center" style="border-top: 0;border-bottom: none; font-size: 14px;"><?php echo ((!empty($item->show_in_print)) && ($item->service_form_type != '3')) ? $item->quantity : ''; ?></td>
                <td align="right" style="border-top: 0;border-bottom: none; font-size: 14px;"><?php echo ((!empty($item->show_in_print)) && ($item->service_form_type != '3')) ? number_format($item->price, 2) : '';?></td>
                <td align="right" style="border-top: 0;border-bottom: none; font-size: 14px;"><?php echo (!empty($item->show_in_print)) ? number_format($item->total, 2) : '';?></td>
            </tr>
            <?php $height -= 55; ?>
        <?php } ?>
        <?php #if ($sales_order->invoice_from != 4) { ?>
        <?php $height -= ($sales_order->discount > 0) ? 30 : 0; ?>
        <?php $height -= ($sales_order->gst_amount > 0) ? 30 : 0; ?>
        <?php #} ?>    
        <tr>
            <td style="border-top: 0; padding:0px;" height="<?php echo ($height > 0) ? $height : 1; ?>"></td>
            <td style="border-top: 0; padding:0px;"></td>
            <td style="border-top: 0; padding:0px;"></td>
            <td style="border-top: 0; padding:0px;"></td>
            <td style="border-top: 0; padding:0px;"></td>
        </tr>
        <?php #if ($sales_order->invoice_from != 4) { ?>
        <tr>
            <td align="left" style="vertical-align: top; border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; padding-right: 10px; font-size: 14px;" colspan="3">
                <?php if (!empty($sales_order->remarks)) { ?>
                    <b style="font-size: 15px;">Remarks</b>
                    <div>
                        <?php echo strtoupper(nl2br($sales_order->remarks)); ?>
                    </div>
                <?php } ?>
            </td>
            <td colspan="2" style="vertical-align: top; padding: 0;">
                <table style="font-size: 12px; width: 100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <th style="text-align: right; height: 30px; width: 109px; border-left: none; border-bottom: 1px solid #000; border-right: 1px solid #000; padding-right: 10px; font-size: 15px;">Sub Total</th>
                        <td align="right" style="width: 108px; border-right: none; border-bottom: 1px solid #000; padding-right: 5px; font-size: 14px;"><?php echo number_format($sales_order->sub_total, 2); ?></td>
                    </tr>
                    <?php if ($sales_order->discount > 0) { ?>
                        <tr>
                            <th style="height: 30px; border-left: none; border-bottom: 1px solid #000; border-right: 1px solid #000; padding-right: 0px; padding-left: 0px; font-size: 15px;">Discount <span style="font-size: 12px;"><?php echo ($sales_order->discount_type == 1 && $sales_order->discount > 0) ? $sales_order->discount . ' %' : ''; ?></span></th>
                            <td align="right" style="border-bottom: 1px solid #000;border-right: none; padding-right: 5px; font-size: 14px;"><?php echo number_format($sales_order->discount_amount, 2); ?></td>
                        </tr>
                    <?php } ?>

                    <?php if ($sales_order->gst == 2) { ?>
                        <tr>
                            <th align="right" style="height: 30px; border-left: none; border-bottom: 1px solid #000; border-right: 1px solid #000; padding-right: 10px; font-size: 15px;">ADD GST <?php echo $sales_order->gst_percentage; ?> %</th>
                            <td align="right" style="border-right: none; border-bottom: 1px solid #000; padding-right: 5px; font-size: 14px;"><?php echo number_format($sales_order->gst_amount, 2); ?></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <th align="right" style="height: 30px; border-left: none; border-bottom: none; border-right: 1px solid #000; padding-right: 10px; font-size: 15px;">Total</th>
                        <td align="right" style="font-weight:normal; border-right: none; border-bottom: none; padding-right: 5px; font-size: 14px;"><?php echo number_format($sales_order->total_amt, 2); ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <?php if ($sales_order->gst == 1  && !empty($company->comp_gst_no)) { ?>
            <tr>
                <td colspan="3"></td>
                <td colspan="2" style="padding-left: 10px;">[<?php echo '<b style="font-size: 12px;">Inclusive of GST ' . $sales_order->gst_percentage . ' %</b> : $ ' . number_format($sales_order->gst_amount, 2); ?>]</td>
            </tr>
        <?php } ?>
    </tbody>
</table>