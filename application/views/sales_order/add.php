<style>

.invoice_cls .btn-hold {

    background-color: #DB6C30;

    border-color: #C66029;

    color: #FFF;

}    

</style>

<!-- Content Wrapper. Contains page content -->

<div id="main" role="main">

    <div id="ribbon">

        <span class="ribbon-button-alignment"> 

            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">

                <i class="fa fa-refresh"></i>

            </span> 

        </span>



        <!-- breadcrumb -->

        <?php echo breadcrumb_links(); ?>

        <!-- end breadcrumb -->

    </div>



    <div id="content">

        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <?php if (!empty($success)) { ?>

                    <div class="alert alert-success fade in" >

                        <button class="close" data-dismiss="alert">×</button>

                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>

                        <strong>Success</strong> <?php echo $success; ?>

                    </div>

                <?php } else if (!empty($error)) {

                    ?>

                    <div class="alert alert-danger alert-dismissable" >

                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>

                    </div>

                <?php } ?>&nbsp;

            </div>

        </div>



            <!-- Main content -->

            <section id="widget-grid" class="">

                <div class="row">

                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <a href="<?php echo $list_link; ?>" class="large_icon_des" style=" color: white;">

                            <button type="button" class="btn btn-primary btn-circle btn-xl temp_color ">

                                <i class="glyphicon glyphicon-list"></i>

                            </button>

                        </a>

                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">

                            <header>

                                <ul class="nav nav-tabs pull-left in">

                                    <li class="active">

                                        <a data-toggle="tab" class="tabcls1" href="#tab-1"> <span class="widget-icon"> <i class="fa fa-edit"></i> </span> <span class="hidden-mobile hidden-tablet"> Invoice Add </span> </a>

                                    </li>

                                    <li>

                                        <a data-toggle="tab" class="tabcls2" href="#tab-2">

                                            <i class="fa fa-handshake-o"></i>

                                            <span class="hidden-mobile hidden-tablet"> Exchange Order Invoice</span>

                                        </a>

                                    </li>

                                    <li>

                                        <a data-toggle="tab" class="tabcls3" href="#tab-3">

                                            <i class="fa fa-cogs"></i>

                                            <span class="hidden-mobile hidden-tablet"> Service Invoice</span>

                                        </a>

                                    </li>

                                    <li>

                                        <a data-toggle="tab" class="tabcls4" href="#tab-4">

                                            <i class="fa fa-file-text-o"></i>

                                            <span class="hidden-mobile hidden-tablet"> Quotation Invoice</span>

                                        </a>

                                    </li>

                                    <li>

                                        <a data-toggle="tab" class="tabcls5" href="#tab-5">

                                            <i class="fa fa-hand-paper-o"></i>

                                            <span class="hidden-mobile hidden-tablet"> Hold Invoices</span>

                                        </a>

                                    </li>

                                </ul>

                            </header>



                            <div style="padding: 0px 0px 0px 0px;">

                                <div class="jarviswidget-editbox">

                                    <!-- This area used as dropdown edit box -->

                                </div>

                                <div class="widget-body" style="padding-bottom: 0px;">

                                    <div class="tab-content">

                                    <div class="tab-pane active" id="tab-1">

                                    <form class="add_form" id="sales_order" action="<?php echo $form_action; ?>" method="post" data-form_submit="0">

                                    <input type="hidden" name="quotation_id" value="<?php echo (!empty($to_inv)) ? $quotation->id : ''; ?>" />    

                                    <input type="hidden" name="invoice_hold_id" value="<?php echo (!empty($to_hold)) ? $quotation->id : ''; ?>" />    

                                    <div class="smart-form" id="sales_form">

                                        <fieldset>

                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>

                                            

                                            <div class="row">

                                                <section class="col col-6">

                                                    <section>

                                                        <label class="label" style="display: inline-block; width: 100%;">Sold To<span style="color: red;">*</span>

                                                            <span class="new-window-popup btn btn-primary pull-right" style="margin: 0 0 2px 0;" data-cmd="customer" data-title="Customer Add" data-iframe-src="<?php echo $add_customer_link; ?>">

                                                                <i class="fa fa-plus-square-o" style="font-size: 16px;"> </i> Add Customer

                                                            </span>

                                                            <span class="new-window-popup btn btn-primary pull-right" style="margin: 0 10px 2px 0;" data-cmd="customer" data-title="Customer Add" data-iframe-src="<?php echo $list_customer_link; ?>">

                                                                <i class="fa fa-indent" style="font-size: 16px;"> </i> Customers

                                                            </span>

                                                        </label>

                                                        <label class="select">

                                                            <select name="customer_name" id="customer_name" class="select2" tabindex="1">

                                                                <!--<option value="">Select Customer name</option>-->

                                                                <option value="cash" <?php echo ($quotation->quo_type == 2 || $quotation->so_type == 2) ? 'selected' : ''; ?>>Cash</option>

                                                                <?php foreach ($customers as $key => $customer) { ?> 

                                                                    <option value="<?php echo $customer->id; ?>" <?php echo ($customer->id == $quotation->customer_id) ? 'selected' : ''; ?>><?php echo $customer->name; ?></option>

                                                                <?php } ?>

                                                            </select>

                                                        </label>

                                                    </section>



                                                    <div class="reg_customer_add" style="display: <?php echo (((!empty($to_inv)) && ($quotation->quo_type == '1')) || ((!empty($to_hold)) && ($quotation->so_type == '1'))) ? 'block' : 'none'; ?>;">

                                                        <section class="cust_address">

                                                            <p class="alert alert-info not-dismissable"> Address: <?php echo ((!empty($to_inv) && !empty($customer->address)) || (!empty($to_hold) && !empty($customer->address))) ? $customer->address : 'N/A'; ?></p>

                                                        </section>



                                                        <section>

                                                            <label class="label">Deliver To</label>

                                                            <label class="select">

                                                                <select name="ship_to" id="ship_to" class="select2">

                                                                    <option value="">Use different Location</option>

                                                                    <?php

                                                                    if ((!empty($to_inv)) || (!empty($to_hold))) {

                                                                        foreach ($addresses as $key => $value) {

                                                                            ?>

                                                                            <option value="<?php echo $value->id; ?>" data-address="<?php echo $value->address; ?>" <?php echo ($value->id == $quotation->shipping_address) ? 'selected' : ''; ?>><?php echo $value->location_name; ?></option>

                                                                        <?php

                                                                        }

                                                                    }

                                                                    ?>

                                                                </select>

                                                            </label>

                                                        </section>



                                                        <section class="ship_to_address">

<?php if ((!empty($to_inv)) || (!empty($to_hold))) { ?>

                                                                <p class="alert alert-info not-dismissable">Delivery Address: <?php echo (!empty($shipping_address->address)) ? nl2br($shipping_address->address) : 'N/A'; ?>

                                                                </p>

<?php } ?>

                                                        </section>



                                                        <div class="new_shipping_address" style="display: none;">

                                                            <section>

                                                                <label class="label">Location Name</label>

                                                                <label class="input">

                                                                    <input type="text" name="location_name" class="form-control" data-placeholder="Mayo Street" />

                                                                </label>

                                                            </section>

                                                            <section>

                                                                <label class="label">Address</label>

                                                                <label class="input">

                                                                    <input type="text" name="address" class="form-control" data-placeholder="29 Mayo Street" />

                                                                </label>

                                                            </section>

                                                        </div>

                                                    </div>



                                                    <!-- For cash customer start -->

                                                    <div class="cash_customer_name" style="display: <?php echo (((!empty($to_inv)) && ($quotation->quo_type == '2')) || ((!empty($to_hold)) && ($quotation->so_type == '2'))) ? 'block' : 'none'; ?>;">

                                                        <section>

                                                            <label class="label">Customer Name</label>

                                                            <label class="input">

																<input type="text" name="cash_customer_name" id="cash_customer_name" class="form-control" value="<?php echo (!empty($quotation->name)) ? $quotation->name : 'Cash'; ?>" />

                                                            </label>

                                                        </section>

                                                        <section>

                                                            <p class="alert alert-info not-dismissable"> Address: CASH CUSTOMER<?php //echo $address->address;   ?></p>

                                                        </section>

                                                    </div>

                                                    <!-- For cash customer end -->



                                                    <section>

                                                        <label class="label">Customer Reference</label>

                                                        <label class="input">

                                                            <input type="text" name="your_reference" id="your_reference" class="form-control" value="<?php echo (!empty($to_hold)) ? $quotation->your_reference : ''; ?>" data-focused=false; />

                                                        </label>

                                                    </section>

                                                </section>



                                                <section class="col col-6">

                                                    <section>

                                                        <label class="label">Invoice No #</label>

                                                        <label class="input">

                                                            <input type="text" name="so_no" class="form-control" tabindex="-1" value="<?php echo $so_no; ?>" />

                                                        </label>

                                                    </section>



                                                    <section>

                                                        <label class="label">Date<span style="color: red;">*</span>

															<!--<label class="checkbox pull-right"><input type="checkbox" name="leave_date" value="1" /><i></i> Leave Blank</label>-->

														</label>

                                                        <label class="input">

                                                            <i class="icon-append fa fa-calendar"></i>

                                                            <input type="text" name="so_date" id="so_date" class="form-control bootstrap-datepicker-comncls" tabindex="-1" data-placeholder="DD/MM/YYYY" value="<?php echo date('d/m/Y'); ?>" />

                                                        </label>

                                                    </section>

                                                    

                                                    <section>

                                                        <label class="label">Remarks</label>

                                                        <label class="textarea">

                                                            <textarea rows="3" class="show_txt_count" name="remarks" id="remarks" maxlength="250"><?php echo (!empty($to_hold)) ? $quotation->remarks : ''; ?></textarea>

                                                        </label>

                                                    </section>

                                                </section>

                                            </div>

                                        </fieldset>

                                        <fieldset>

                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Items Details</legend>

                                            <div class="row product_search">

                                                <section class="col col-5">

                                                    <div class="input hidden-mobile">

                                                        <input name="search_product" class="form-control search_product lowercase" type="text" id="search_product" tabindex="-1" autofocus />

                                                    </div>

                                                </section>

                                            </div>

                                            <div class="row material-container product_details">

                                                <div class="col-xs-12">

                                                    <section class="col">

                                                        <span class="new-window-popup btn btn-primary" data-iframe-src="<?php echo $list_item_link; ?>" data-cmd="item" data-title="Item Add">

                                                            <i class="fa fa-indent" style="font-size: 16px;"> </i> Items

                                                        </span>

                                                        <span class="new-window-popup btn btn-primary" data-iframe-src="<?php echo $add_item_link; ?>" data-cmd="item" data-title="Item Add">

                                                            <i class="fa fa-plus-square-o" style="font-size: 16px;"> </i> Add Item

                                                        </span>

                                                    </section>

                                                </div>



                                                <div class="items_head col-xs-12 clr">

                                                    <section class="col col-2">

                                                        <label class="label" style="text-align: center;">

                                                            <b>Item Code</b>

                                                        </label>

                                                    </section>

                                                    <section class="col col-4">

                                                        <label class="label" style="text-align: center;"><b>Brand - Description</b></label>

                                                    </section>

                                                    <section class="col col-1">

                                                        <label class="label" style="text-align: center;"><b>Qty</b></label>

                                                    </section>

													<?php $unit_cost = (in_array(7, $permission)); ?>

                                                    <section class="col col-1">

                                                        <label class="label" style="text-align: center;"><b>Price</b></label>

                                                    </section>



<?php if ($unit_cost) { ?>

                                                        <section class="col col-1">

                                                            <label class="label" style="text-align: center;"><b>Cost</b></label>

                                                        </section>

<?php } ?>

													<section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                        <label class="label"><b>Discount</b></label>

                                                    </section>

                                                    <section class="col col-<?php echo ($unit_cost) ? '1' : '2'; ?>">

                                                        <label class="label" style="text-align: center;"><b>Amount</b></label>

                                                    </section>

                                                    <section class="col col-1"></section>

                                                </div>



                                                <?php

                                                $item_count = count($quotation_items);

                                                if ($item_count) {

                                                    foreach ($quotation_items as $item_key => $item) {

                                                        ?>

                                                        <div class="items col-xs-12 clr">

                                                            <section class="col col-2" style="padding-right: 0px;">

																<label class="input" style="display: inline-block; width: 64%;">

                                                                    <input type="hidden" name="category_id[<?php echo $item_key; ?>]" class="category_id" value="<?php echo $item->category_id; ?>" />

                                                                    <input type="hidden" name="product_id[<?php echo $item_key; ?>]" class="product_id" value="<?php echo $item->item_id; ?>" />

                                                                    <input type="text" name="product_code[<?php echo $item_key; ?>]" class="product_code" value="<?php echo $item->pdt_code; ?>" data-value="<?php echo $item->pdt_code; ?>" />

                                                                </label>

																<label style="display: inline-block; width: 34%;">

																	<span class="item-serialnum-popup btn btn-primary" style="padding:3px 4px 3px 4px;" title="Quantity should not be empty to enter Serial No.">

																		<i class="fa fa-plus-square-o" style="font-size: 14px;"> </i> Sl No

																	</span>

																	<input type="hidden" name="serial_no[<?php echo $item_key; ?>]" class="form-control serial_no" />

																</label>

																<span class="product_code_label">Selected Item code already exist!</span>

                                                                <span class="available_qty_label">Available quantity : 0</span>

                                                            </section>



                                                            <section class="col col-4">

																<section style="margin-bottom: 0px;">

																	<!--<label class="input">

																		<input type="text" name="product_description[<?php echo $item_key; ?>]" class="product_description" data-value="<?php echo form_prep($item->product_description); ?>" value="<?php echo form_prep($item->product_description); ?>" />

																	</label>-->
                                                                    
                                                                    <label class="textarea textarea-expandable"><textarea rows="3" name="product_description[<?php echo $item_key; ?>]" class="product_description" data-value="<?php echo form_prep($item->product_description); ?>"><?php echo form_prep($item->product_description); ?></textarea> </label> 

																	<span class="error_description">Selected Item description already exist!</span>

																	<span class="brand_label" style="display: none;">Item Code: <span class="brand_name"><?php echo $item->cat_name; ?></span></span>

																</section>

                                                            </section>



                                                            <section class="col col-1">

                                                                <label class="input">

                                                                    <input type="text" name="quantity[<?php echo $item_key; ?>]" class="quantity float_value" value="<?php echo $item->quantity; ?>" style="padding-left: 2px; padding-right: 2px; text-align: right;" />



                                                                    <input type="hidden" name="old_quantity[<?php echo $item_key; ?>]" class="quantity float_value" value="<?php echo $item->quantity; ?>" />

                                                                </label>

                                                                <span class="unit_label"></span>

                                                            </section>



                                                            <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                                <label class="input">

                                                                        <!--<i class="icon-prepend fa fa-dollar"></i>-->

                                                                    <input type="text" name="price[<?php echo $item_key; ?>]" class="price float_value" value="<?php echo $item->price; ?>" style="padding-left: 2px; padding-right: 2px;" />

                                                                </label>

                                                            </section>



                                                            <section class="col col-1" style="padding-left: 5px; padding-right: 5px; display: <?php echo ($unit_cost) ? 'block' : 'none'; ?>;">

                                                                <label class="input" style="margin-top: 1px;">

                                                                        <!--<i class="icon-prepend fa fa-dollar"></i>-->

                                                                    <input type="text" name="cost[<?php echo $item_key; ?>]" class="cost float_value" value="<?php echo $item->unit_cost; ?>" style="padding-left: 2px; padding-right: 2px;" />

                                                                </label>

                                                            </section>

															<section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

																<div class="input-group">

																	<input type="text" name="item_discount[<?php echo $item_key;?>]" class="form-control item_discount float_value" style="height: 30px; padding-right: 5px;" value="<?php echo $item->discount; ?>" />

																	<input type="hidden" name="item_discount_amt[<?php echo $item_key;?>]" class="item_discount_amt" value="<?php echo $item->discount_amount; ?>" />

																	<span style="display: block; width: 100%; text-align: right;">S$ <span class="item_discount_amt"><?php echo $item->discount_amount; ?></span></span>

																	<span class="onoffswitch" style="float: right;">

																		<input type="checkbox" name="item_discount_type[<?php echo $item_key;?>]" class="onoffswitch-checkbox item_discount_type" id="item_discount_type-<?php echo $item_key;?>" value="1" <?php echo ($item->discount_type == 1) ? 'checked' : ''; ?> />

                                                                        

																		<label class="onoffswitch-label" for="item_discount_type-<?php echo $item_key;?>"> 

																			<span class="onoffswitch-inner" data-swchon-text="%" data-swchoff-text="S$"></span> 

																			<span class="onoffswitch-switch"></span> 

																		</label> 

																	</span>

																</div>

															</section>

                                                            <section class="col col-<?php echo ($unit_cost) ? '1' : '2'; ?>">

                                                                <label class="input">

                                                                    <input type="text" name="amount[<?php echo $item_key; ?>]" class="amount float_value" value="<?php echo $item->total; ?>" style="padding-left: 2px; padding-right: 2px;" readonly="" />

                                                                </label>

                                                            </section>



                                                            <section class="col col-1">

                                                                <label class="input">

                                                                    <?php if ($item_count == ($item_key + 1)) { ?>

                                                                        <i class="fa fa-2x fa-plus-circle add-row"></i>

                                                                    <?php } ?>

                                                                    <?php if ($item_count > 1) { ?>

                                                                        <i class="fa fa-2x fa-minus-circle remove-row"></i>

																	<?php } ?>

                                                                    <i class="fa fa-2x fa-bullhorn promotion"></i>

                                                                </label>

                                                            </section>

                                                        </div>

                                                        <?php

                                                    }

                                                } else {

                                                    ?>

                                                    <div class="items col-xs-12 clr">

                                                        <section class="col col-2" style="padding-right: 0px;">

                                                            <label class="input" style="display: inline-block; width: 64%;">

                                                                <input type="hidden" name="category_id[0]" class="category_id" value="" />

                                                                <input type="hidden" name="product_id[0]" class="product_id" value="" />

                                                                <input type="text" name="product_code[0]" class="product_code" data-value="" />

                                                            </label>

                                                            <span class="product_code_label">Selected Item code already exist!</span>

															<label style="display: inline-block; width: 34%;">

																<span class="item-serialnum-popup btn btn-primary" style="padding:3px 4px 3px 4px;" title="Quantity should not be empty to enter Serial No.">

																	<i class="fa fa-plus-square-o" style="font-size: 14px;"> </i> Sl No

																</span>

																<input type="hidden" name="serial_no[0]" class="form-control serial_no" />

															</label>

															<span class="available_qty_label">Selected Item code already exist!</span>

                                                        </section>



                                                        <section class="col col-4">

                                                            <section style="margin-bottom: 0px;">

																<!--<label class="input">

																	<input type="text" name="product_description[0]" class="product_description" data-value="" />

																</label>-->
                                                                
                                                                <label class="textarea textarea-expandable"><textarea rows="3" name="product_description[0]" class="product_description" data-value=""></textarea> </label> 

																<span class="error_description">Selected Item description already exist!</span>

															</section>

                                                        </section>



                                                        <section class="col col-1">

                                                            <label class="input">

                                                                <input type="text" name="quantity[0]" class="quantity float_value" style="padding-left: 2px; padding-right: 2px; text-align: right;" />

                                                            </label>

                                                            <span class="unit_label"></span>

                                                        </section>



                                                        <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                            <label class="input">

                                                                    <!--<i class="icon-prepend fa fa-dollar"></i>-->

                                                                <input type="text" name="price[0]" class="price float_value" style="padding-left: 2px; padding-right: 2px;" />

                                                            </label>

                                                        </section>



                                                        <section class="col col-1" style="padding-left: 5px; padding-right: 5px; display: <?php echo ($unit_cost) ? 'block' : 'none'; ?>;">

                                                            <label class="input" style="margin-top: 1px;">

                                                                    <!--<i class="icon-prepend fa fa-dollar"></i>-->

                                                                <input type="text" name="cost[0]" class="cost float_value" style="padding-left: 2px; padding-right: 2px;" />

                                                            </label>

                                                        </section>

														

														<section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

															<div class="input-group">

																<input type="text" name="item_discount[0]" class="form-control item_discount float_value" style="height: 30px; padding-right: 5px;" value="0.00" />

																<input type="hidden" name="item_discount_amt[0]" class="item_discount_amt" value="0.00" />

																<span style="display: block; width: 100%; text-align: right;">S$ <span class="item_discount_amt">0.00</span></span>

																<span class="onoffswitch" style="float: right;">

																	<input type="checkbox" name="item_discount_type[0]" class="onoffswitch-checkbox item_discount_type" id="item_discount_type-0" value="1" checked />

																	<label class="onoffswitch-label" for="item_discount_type-0"> 

																		<span class="onoffswitch-inner" data-swchon-text="%" data-swchoff-text="S$"></span> 

																		<span class="onoffswitch-switch"></span> 

																	</label> 

																</span>

															</div>

														</section>

                                                        <section class="col col-<?php echo ($unit_cost) ? '1' : '2'; ?>">

                                                            <label class="input">

                                                                <input type="text" name="amount[0]" class="amount float_value" style="padding-left: 2px; padding-right: 2px;" readonly="" />

                                                            </label>

                                                        </section>



                                                        <section class="col col-1">

                                                            <label class="input">

                                                                <i class="fa fa-2x fa-plus-circle add-row"></i>

                                                                <i class="fa fa-2x fa-bullhorn promotion"></i>

                                                            </label>

                                                        </section>

                                                    </div>

												<?php } ?>

                                            </div>



                                            <div class="row" style="margin-top: 15px;">

                                                <section class="col col-6">

                                                </section>

                                                <section class="col col-3">

                                                    <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Sub Total</b></label>

                                                </section>

                                                <section class="col col-2">

                                                    <label class="input">

                                                        <i class="icon-prepend fa fa-dollar"></i>

                                                        <input type="text" name="sub_total" class="sub_total" value="<?php echo ((!empty($to_inv)) || (!empty($to_hold))) ? $quotation->sub_total : ''; ?>" readonly="" />

                                                    </label>

                                                </section>

                                                <section class="col col-1">

                                                </section>

                                            </div>



                                            <div class="row">

                                                <section class="col col-6">

                                                </section>

                                                <section class="col col-3">

                                                    <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Discount</b></label>

                                                </section>

                                                <section class="col col-2">

                                                    <!--<label class="input">

                                                        <i class="icon-prepend fa fa-dollar"></i>

                                                        <input type="text" name="sub_total" class="sub_total" readonly="" data-placeholder="0.00" />

                                                    </label>-->



                                                    <div class="input-group">

                                                        <input type="text" name="discount" class="form-control discount float_value" value="<?php echo ((!empty($to_inv)) || (!empty($to_hold))) ? $quotation->discount : '0.00'; ?>" />

                                                        <span class="input-group-addon">

                                                            <span class="onoffswitch">

                                                                <input type="checkbox" name="discount_type" class="onoffswitch-checkbox discount_type" id="discount_type-0" value="1" <?php echo ((!empty($to_inv) && $quotation->discount_type) || (!empty($to_hold) && $quotation->discount_type)) ? 'checked' : ''; ?>  />

                                                                <label class="onoffswitch-label" for="discount_type-0"> 

                                                                    <span class="onoffswitch-inner" data-swchon-text="%" data-swchoff-text="S$"></span> 

                                                                    <span class="onoffswitch-switch"></span> 

                                                                </label> 

                                                            </span>

                                                        </span>

                                                    </div>

                                                    <input type="hidden" name="discount_amt" class="discount_amt" value="<?php echo ((!empty($to_inv)) || (!empty($to_hold))) ? $quotation->discount_amount : '0.00'; ?>" />

                                                    <span class="discount_container">

                                                        <span class="currency">S$</span>

                                                        <span class="discount_amount" style="display: inline;"><?php echo ((!empty($to_inv)) || (!empty($to_hold))) ? $quotation->discount_amount : '0.00'; ?></span>

                                                    </span>

                                                </section>

                                                <section class="col col-1">

                                                </section>

                                            </div>



                                            <div class="row gst_details" style="display:<?php echo (empty($company->comp_gst_no)) ? 'none' : 'block'; ?>">

                                                <section class="col col-4">

                                                </section>

                                                <section class="col col-2">&nbsp;</section>

                                                <section class="col col-3">

                                                    <section>

                                                        <label class="label">&nbsp;</label>

                                                        <div class="inline-group">

                                                            <label class="radio">

                                                                <input type="radio" class="gst_type" name="gst_type" value="1" <?php echo (empty($company->comp_gst_no)) ? 'checked' : ''; ?> /><i></i>Include GST

                                                            </label>

                                                            <label class="radio">

                                                                <input type="radio" class="gst_type" name="gst_type" value="2" <?php echo (!empty($company->comp_gst_no)) ? 'checked' : ''; ?> /><i></i>Exclude GST

                                                            </label>

                                                        </div>

                                                    </section>

                                                </section>

                                                <section class="col col-2">

                                                    <label class="label">&nbsp;</label>

                                                    <label class="input change_perc_disamt"><i class="icon-prepend fa fa-dollar"></i>

                                                        <input type="text" name="gst_amount" class="gst_amount float_value" data-placeholder="0.00" readonly="" />

                                                        <input type="hidden" value="<?php echo $company->gst_percent; ?>" name="hidden_gst" id="hidden_gst" />

                                                    </label>

                                                    <span class="gst_val" style="display: <?php echo (!empty($company->gst_percent)) ? 'block' : 'none'; ?>">GST @ <span class="gst_percentage"><?php echo $company->gst_percent; ?></span> %</span>

                                                </section>

                                            </div>



                                            <div class="row">

                                                <section class="col col-6">

                                                </section>

                                                <section class="col col-3">

                                                    <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Total Amount</b></label>

                                                </section>

                                                <section class="col col-2">

                                                    <label class="input">

                                                        <i class="icon-prepend fa fa-dollar"></i>

                                                        <input type="text" name="total_amt" class="total_amt" readonly="" value="<?php echo ((!empty($to_inv)) || (!empty($to_hold))) ? $quotation->total_amt : ''; ?>" />

                                                    </label>

                                                </section>

                                                <section class="col col-1">

                                                </section>

                                            </div>

                                        </fieldset>



                                        <footer>

                                            <?php if((empty($to_inv)) && (empty($to_hold))) { ?>

                                            <button type="submit" name="submit_hold" class="btn btn-hold" value="hold">Hold</button>

                                            <?php } ?>

                                            <button type="submit" name="submit_inv" class="btn btn-primary" >Submit</button>

                                            <button type="reset" name="reset_inv" class="btn btn-default" >Cancel</button>

                                        </footer>

                                    </div>

                                    <div class="smart-form" id="payment_form" style="display: none;">

                                        <fieldset>

                                            <div class="row">

                                                <section class="col col-md-6">

                                                    <section>

                                                        <label class="label">Total Amount</label>

                                                    </section>

                                                </section>

                                                <section class="col col-md-6">

                                                    <section>

                                                        <label class="input">S$ <span class="payment_amount">0.00</span></label>

                                                    </section>

                                                </section>

                                            </div>

                                            <div class="row">

                                                <section class="col col-md-6">

                                                    <section>

                                                        <label class="label">Payment Type</label>

                                                    </section>

                                                </section>

                                                <section class="col col-md-6">

                                                    <section>

                                                        <label class="select">

                                                            <select name="payment_type" class="payment_type select2" id="payment_type">

                                                                <option value="">Select Payment Type</option>

                                                                <?php

                                                                foreach ($payment_types as $key => $value) {

                                                                    echo '<option value="' . $value->id . '" data-is_cheque="' . $value->is_cheque . '" data-group="' . $value->group_name . '" >' . $value->type_name . '</option>';

                                                                }

                                                                ?>

                                                                <input type="hidden" name="data_group" id="data_group">

                                                            </select>

                                                        </label>

                                                    </section>

                                                    <div class="cheque_details">

                                                        <section>

                                                            <label class="label">Bank Name<span style="color:red">*</span></label>

                                                            <label class="input">

                                                                <input type="text" class="form-control" name="cheque_bank_name" data-placeholder="SBI" maxlength="100" value="<?php echo $bnk_acc_name; ?>" />

                                                            </label>

                                                        </section>

                                                        <section>

                                                            <label class="label">Cheque No<span style="color:red">*</span></label>

                                                            <label class="input">

                                                                <input type="text" class="form-control" name="cheque_acc_no" data-placeholder="Cheque No" maxlength="100" />

                                                            </label>

                                                        </section>

                                                        <section>

                                                            <label class="label">Cheque Date<span style="color:red">*</span></label>

                                                            <label class="input">

                                                                <input type="text" class="bootstrap-datepicker-strdatecls" name="cheque_date" data-placeholder="01/01/2016" maxlength="100" />

                                                            </label>

                                                        </section>

                                                    </div>

                                                    <!--<div class="dd_details">

                                                        <section>

                                                            <label class="label">DD No<span style="color:red">*</span></label>

                                                            <label class="input">

                                                                <input type="text" class="form-control" name="dd_no"  data-placeholder="DD No" maxlength="100" />

                                                            </label>

                                                        </section>

                                                        <section class="dd_details">

                                                            <label class="label">DD Date<span style="color:red">*</span></label>

                                                            <label class="input">

                                                                <input type="text" class="bootstrap-datepicker-comncls" name="dd_date"  data-placeholder="DD Date" maxlength="100" />

                                                            </label>

                                                        </section>

                                                    </div>-->

                                                </section>

                                            </div>

                                        </fieldset>

                                        <footer>

                                            <button type="button" class="btn btn-default" onclick="show_sales_form();">Back</button>

											<?php if(in_array(10, $permission)) {?>

												<button type="submit" class="btn btn-success" id="pay_now" name="payment" value="pay_now">Pay Now</button>

											<?php }?>

                                            <button type="submit" class="btn btn-warning" id="skip_payment" name="payment" value="skip_payment" onclick="disable_payment();">Skip Payment</button>

                                        </footer>

                                    </div>

                                    </form>

                                    </div>  

                                    <div class="tab-pane" id="tab-2">

                                    <form class="exchangeorder_form" id="exchangeorder_form" action="" method="post" data-form_submit="0">

                                        <div class="smart-form" id="sales_form">

                                             <fieldset>

                                                <legend style=" font-size: 12px; color: red;">Select Exchange Order to Generate Invoice</legend>



                                                <div class="row">

                                                    <section class="col col-3"></section>

                                                    <section class="col col-6">

                                                        <label class="select">

                                                            <select name="exchangeorder_id[]" multiple id="exchangeorder_id" class="select2 exchangeorder_id" tabindex="1">

                                                                <!--<option disabled="" value="" > --- Select Exchange Order ---</option>-->

                                                                <?php foreach ($exchange_order_list as $key => $exchange_order) { ?> 

                                                                    <option value="<?php echo $exchange_order->id; ?>" ><?php echo $exchange_order->exr_code.' / '.$exchange_order->name; ?></option>

                                                                <?php } ?>

                                                            </select>

                                                        </label>    

                                                    </section>

                                                    <section class="col col-3"></section>

                                                </div>

                                             </fieldset>

                                            <footer>

                                                <button type="submit" name="submit" class="btn btn-primary" >Submit</button>

                                                <button type="reset" class="btn btn-default" >Cancel</button>

                                            </footer>

                                        </div>

                                    </form>

                                    </div>    

                                    <div class="tab-pane" id="tab-3">

                                    <form class="service_form" id="service_form" action="" method="post" data-form_submit="0">

                                        <div class="smart-form" id="sales_form">

                                             <fieldset>

                                                <legend style=" font-size: 12px; color: red;">Select Service to Generate Invoice</legend>



                                                <div class="row">

                                                    <section class="col col-3"></section>

                                                    <section class="col col-6">

                                                        <label class="select">

                                                            <select name="service_id" id="service_id" class="select2 service_id" tabindex="1">

                                                                <option value="" > --- Select Service ---</option>

                                                                <?php foreach ($service_list as $key => $service) { ?> 

                                                                    <option value="<?php echo $service->id; ?>" ><?php echo $service->jobsheet_number.' / '.$service->customer_name; ?></option>

                                                                <?php } ?>

                                                            </select>

                                                        </label>    

                                                    </section>

                                                    <section class="col col-3"></section>

                                                </div>

                                             </fieldset>

                                        </div>

                                    </form>

                                    </div>    

                                    <div class="tab-pane" id="tab-4">

                                    <form class="quotation_form" id="quotation_form" action="" method="post" data-form_submit="0">

                                        <div class="smart-form" id="sales_form">

                                             <fieldset>

                                                <legend style=" font-size: 12px; color: red;">Select Quotation to Generate Invoice</legend>



                                                <div class="row">

                                                    <section class="col col-3"></section>

                                                    <section class="col col-6">

                                                        <label class="select">

                                                            <select name="pst_quotation_id" id="pst_quotation_id" class="select2 pst_quotation_id" tabindex="1">

                                                                <option value="" > --- Select Quotation ---</option>

                                                                <?php foreach ($active_qtn_list as $key => $qtn_so) { ?> 

                                                                    <option value="<?php echo $qtn_so->id; ?>" ><?php echo $qtn_so->quo_no.' / '.$qtn_so->name; ?></option>

                                                                <?php } ?>

                                                            </select>

                                                        </label>    

                                                    </section>

                                                    <section class="col col-3"></section>

                                                </div>

                                             </fieldset>

                                        </div>

                                    </form>

                                    </div>    

                                    <div class="tab-pane" id="tab-5">

                                    <form class="so_hold_form" id="so_hold_form" action="" method="post" data-form_submit="0">

                                        <div class="smart-form" id="sales_form">

                                             <fieldset>

                                                <legend style=" font-size: 12px; color: red;">Select Hold Invoice to Generate Invoice</legend>

                                                <div class="row">

													<div class="table-responsive">

													<table class="table table-striped table-bordered table-hover" width="100%">

														<thead>

															<tr>

																<th data-class="expand" width="10%">S.No</th>

																<th data-hide="phone" width="15%">Date</th>

																<th width="35%">

																	<i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Customer Name

																</th>                                                

																<th data-hide="phone,tablet" width="20%">Total Amount</th>

																<th data-hide="phone,tablet" width="20%">Action</th>

															</tr>

														</thead>

														<tbody>

														<?php if (count($hold_so_list)) {?>

														<?php foreach ($hold_so_list as $key => $hold_so) { ?>

															<tr>

																<td><?php echo ++ $key;?></td>

																<td><?php echo $hold_so->so_date;?></td>

																<td><?php echo $hold_so->name;?></td>

																<td align="right"><?php echo $hold_so->total_amt;?></td>

																<td>

																	<span class="actions">

																		<button type="submit" class="label btn btn-primary" name="so_hold_id" value="<?php echo $hold_so->id; ?>" style="display: inline; padding: 3px; color: #fff;"><i class="fa fa-send" aria-hidden="true"></i> To Invoice</button>

																	

																		<a class="label btn btn-danger delete delete-confirm" href="#" data-confirm-content="You will not be able to recover hold invoice details!" data-redirect-url="<?php echo $hold_invoice_delete_url . $hold_so->hold_invoice_delete_url . $hold_so->id; ?>" data-bindtext="hold invoice" style="display: inline; padding: 3px; color: #fff;"><span>Delete</span></a>

																	</span>

																</td>

															</tr>

														<?php } ?>

														<?php }

														else {?>

															<tr>

																<td colspan="5" align="center">No Record Found!</td>

															</tr>

														<?php } ?>

														</tbody>

													</table>

													</div>

                                                </div>

                                             </fieldset>

                                        </div>

                                    </form>

                                    </div>    

                                    </div>    

                                </div>

                            </div>

                            <!-- /.col -->

                        </div>

                    </article>

                    <!-- /.row -->

                </div>

            </section>

        <!-- /.content -->

    </div>

    <!-- /.main -->

</div>

<script type="text/javascript">

    var payment_terms = <?php echo json_encode($payment_terms, JSON_UNESCAPED_SLASHES); ?>;

</script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/sales_order.js"></script>