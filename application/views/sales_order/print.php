<script type="text/javascript">
    $(document).ready(function () {
        $('#print').focus();
    });
</script>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
            </span> 
        </span>
        
        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>
    
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if(!empty($success)) {?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                            <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                            <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php }
                else if(!empty($error)) {?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error;?>
                    </div>
                <?php }?>&nbsp;
            </div>
        </div>
        
        <!--<form class="print_form" action="<?php echo $form_action;?>" method="post">-->
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <a href="<?php echo $list_link; ?>" class="large_icon_des" style=" color: white;">
                            <button type="button" class="btn btn-primary btn-circle btn-xl temp_color ">
                                <i class="glyphicon glyphicon-list"></i>
                            </button>
                        </a>
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Invoice Print </h2>
                            </header>

                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" id ="sales_form">
                                        <fieldset>
                                            <div class="row">
                                                <div class="col col-md-12" style="text-align: center; margin: 5% 0;"><p style="font-size: 30px;">Do you want to take a print ?</p></div>
                                                <div class="col col-md-12" style="text-align: center; margin: 0% 0 15%;">
                                                    <div class="col col-md-2" style="max-width: 14%; margin-top: 2%; min-width: 8%;">
                                                        <input type="hidden" class="form-control" name="so_id" value="<?php echo $so_id;?>" />
                                                        <button type="button" class="btn btn-lg btn-default" onclick="location.href='<?php echo $list_link; ?>'">Cancel</button>
                                                    </div>
                                                    <div class="col col-md-2" style="max-width: 22%; margin-top: 2%; min-width: 20%;">
                                                        <a href="<?php echo $redirect_link; ?>"><button type="button" name="print" id="print" class="btn btn-lg btn-primary" onclick="window.open('<?php echo $invoice_link; ?>', '_blank')">Print Invoice</button></a>
                                                    </div>
                                                    <?php if ($sales_order->invoice_from == 4) { ?>
                                                    <div class="col col-md-3" style="max-width: 32%;  margin-top: 2%; min-width: 28%;">
                                                        <a href="javascript:void(0);"><button type="button" class="btn btn-lg btn-primary" disabled="">Print Delivery Order</button></a>
                                                    </div>
                                                    <?php } else { ?>
                                                    <div class="col col-md-3" style="max-width: 32%;  margin-top: 2%; min-width: 28%;">
                                                        <a href="<?php echo $redirect_link; ?>"><button type="button" name="print" id="print" class="btn btn-lg btn-primary" onclick="window.open('<?php echo $do_link; ?>', '_blank')">Print Delivery Order</button></a>
                                                    </div>
                                                    <?php } ?>
                                                    <div class="col col-md-2" style="max-width: 18%;  margin-top: 2%; min-width: 21%;">
                                                        <a href="<?php echo $redirect_link; ?>" onclick="printResults('<?php echo $so_id; ?>','<?php echo $browser['name'];?>')"><button type="button" name="print" id="print" class="btn btn-lg btn-warning">Thermal Print</button></a>
                                                    </div>
                                                    <div class="col col-md-2" style="max-width: 18%;  margin-top: 2%;">
                                                        <a href="<?php echo $redirect_link; ?>"><button type="button" name="print" id="print" class="btn btn-lg btn-primary" onclick="window.open('<?php echo $receipt_link; ?>', '_blank')">Print Receipt</button></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>
        <!--</form>-->
    </div>
</div>

<iframe id="thermal_content" src="<?php echo $thermal_src;?>" style="display: none;"></iframe>
<script type="text/javascript">
    function printResults(id,browser_txt) {   
        var w = window.open();
		var content = '<html>';
		content += '<body>';
		content += $('#thermal_content').contents().find('body').html();
		content += '</body>';
		content += '</html>';
        w.document.write(content);  //only part of the page to print, using jquery
        w.document.close(); //this seems to be the thing doing the trick
        w.focus();
        w.print();
        w.close();
        $('#myModal').modal('hide');
    }    
</script>