<style type="text/css">

    .exc-add-row, .exc-remove-row {

       color: #A90329;

       padding-right: 3px;

    }

</style>

<!-- Content Wrapper. Contains page content -->

<div id="main" role="main">

    <div id="ribbon">

        <span class="ribbon-button-alignment"> 

            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">

                <i class="fa fa-refresh"></i>

            </span> 

        </span>



        <!-- breadcrumb -->

        <?php echo breadcrumb_links(); ?>

        <!-- end breadcrumb -->

    </div>



    <div id="content">

        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <?php if (!empty($success)) { ?>

                    <div class="alert alert-success fade in" >

                        <button class="close" data-dismiss="alert">×</button>

                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>

                        <strong>Success</strong> <?php echo $success; ?>

                    </div>

                <?php } else if (!empty($error)) {

                    ?>

                    <div class="alert alert-danger alert-dismissable" >

                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>

                    </div>

                <?php } ?>&nbsp;

            </div>

        </div>



        <form class="edit_form" id="sales_order" action="<?php echo $form_action; ?>" method="post" data-form_submit="0">

            <input type="hidden" class="form-control so_id_cls" name="so_id" value="<?php echo $sales_order->id; ?>" />

            <input type="hidden" class="form-control" name="invoice_from" value="<?php echo $sales_order->invoice_from; ?>" />

            <input type="hidden" class="form-control" name="exchange_order_id" value="<?php echo $sales_order->exchange_order_id; ?>" />

            <input type="hidden" class="form-control" name="payment_status" value="<?php echo $sales_order->payment_status; ?>" />



            <!-- Main content -->

            <section id="widget-grid" class="">

                <div class="row">

                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <a href="<?php echo $list_link; ?>" class="large_icon_des" style=" color: white;">

                            <button type="button" class="btn btn-primary btn-circle btn-xl temp_color ">

                                <i class="glyphicon glyphicon-list"></i>

                            </button>

                        </a>

                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">

                            <header>

                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>

                                <h2>Invoice Edit </h2>

                            </header>



                            <div style="padding: 0px 0px 0px 0px;">

                                <div class="jarviswidget-editbox">

                                    <!-- This area used as dropdown edit box -->

                                </div>

                                <div class="widget-body" style="padding-bottom: 0px;">

                                    <div class="smart-form" id ="sales_form">

                                        <fieldset>

                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>

                                            <div class="row">

                                                <section class="col col-6" >

                                                    <section>

                                                        <label class="label">Exchange Order</label>

                                                        <?php $exctx_data = explode(',', $sales_order->exchange_order_id); ?>

                                                        <label class="select">

                                                            <select name="exchange_order_id_dis[]" multiple id="exchange_order_id" class="select2 exchange_order_id" disabled="" tabindex="1">

                                                                <?php foreach ($exchange_order_list as $key => $exchange_order) { ?> 

                                                                    <option value="<?php echo $exchange_order->id; ?>" <?php echo ( in_array($exchange_order->id, $exctx_data) ? 'selected' : '' ); ?>><?php echo $exchange_order->exr_code . ' / ' . $exchange_order->name; ?></option>

                                                                <?php } ?>

                                                            </select>

                                                        </label> 

                                                    </section>

                                                    <section>

                                                        <label class="select">

                                                            <select name="customer_name" id="customer_name" class="select2" tabindex="1">

                                                                <!--<option value="">Select Customer name</option>-->

                                                                <option value="cash" <?php echo ($sales_order->so_type == 2) ? 'selected' : ''; ?>>Cash</option>

                                                                <?php foreach ($customers as $key => $value) { ?>     <option value="<?php echo $value->id; ?>" <?php echo ($value->id == $sales_order->customer_id) ? 'selected' : ''; ?>><?php echo $value->name; ?></option>

                                                                <?php } ?>

                                                            </select>

                                                        </label>

                                                    </section>



                                                    <div class="reg_customer_add" style="display: <?php echo ($sales_order->so_type == '1') ? 'block' : 'none'; ?>;">

                                                        <section class="cust_address">

                                                            <p class="alert alert-info not-dismissable"> Address: <?php echo (!empty($customer->address)) ? $customer->address : 'N/A'; ?></p>

                                                        </section>



                                                        <section>

                                                            <label class="label">Deliver To</label>

                                                            <label class="select">

                                                                <select name="ship_to" id="ship_to" class="select2">

                                                                    <option value="">Use different Location</option>

                                                                    <?php foreach ($addresses as $key => $value) { ?>     <option value="<?php echo $value->id; ?>" data-address="<?php echo $value->address; ?>" <?php echo ($value->id == $sales_order->shipping_address) ? 'selected' : ''; ?>><?php echo $value->location_name; ?></option>

                                                                    <?php } ?>

                                                                </select>

                                                            </label>

                                                        </section>



                                                        <section class="ship_to_address">

                                                            <p class="alert alert-info not-dismissable">Delivery Address: <?php echo (!empty($shipping_address->address)) ? nl2br($shipping_address->address) : 'N/A'; ?>

                                                            </p>

                                                        </section>

                                                        <div class="new_shipping_address">

                                                            <section>

                                                                <label class="label">Location Name</label>

                                                                <label class="input">

                                                                    <input type="text" name="location_name" class="form-control" data-placeholder="Mayo Street" />

                                                                </label>

                                                            </section>

                                                            <section>

                                                                <label class="label">Address</label>

                                                                <label class="input">

                                                                    <input type="text" name="address" class="form-control" data-placeholder="29 Mayo Street" />

                                                                </label>

                                                            </section>

                                                        </div>

                                                    </div>



                                                    <!-- For cash customer start -->

                                                    <div class="cash_customer_name" style="display: <?php echo ($sales_order->so_type == '2') ? 'block' : 'none'; ?>;">

                                                        <section class="cash_customer_name">

                                                            <label class="label">Customer Name</label>

                                                            <label class="input">

                                                                <input type="text" name="cash_customer_name" id="cash_customer_name" class="form-control" data-placeholder="Cash Customer" value="<?php echo $sales_order->name; ?>" />

                                                            </label>

                                                        </section>



                                                        <section class="current_address">

                                                            <p class="alert alert-info not-dismissable"> Address: CASH CUSTOMER<?php //echo $address->address;  ?></p>

                                                        </section>

                                                    </div>

                                                    <!-- For cash customer end -->



                                                    <section>

                                                        <label class="label">Customer Reference</label>

                                                        <label class="input">

                                                            <input type="text" name="your_reference" id="your_reference" class="form-control" value="<?php echo $sales_order->your_reference; ?>" data-placeholder="Customer Reference" />

                                                        </label>

                                                    </section>

                                                </section>



                                                <section class="col col-6">

                                                    <section>

                                                        <label class="label">Invoice No#</label>

                                                        <label class="input">

                                                            <input type="text" name="so_no" class="form-control" value="<?php echo $sales_order->so_no; ?>" tabindex="-1" data-placeholder="" readonly />

                                                        </label>

                                                    </section>



                                                    <section>

                                                        <label class="label">Date<span style="color: red;">*</span>

                                                            <!--<label class="checkbox pull-right"><input type="checkbox" name="leave_date" value="1" <?php echo (!empty($sales_order->leave_date)) ? 'checked' : ''; ?> /><i></i> Leave Blank</label>-->

                                                        </label>

                                                        <label class="input">

                                                            <i class="icon-append fa fa-calendar"></i>

                                                            <input type="text" name="so_date" id="so_date" class="form-control bootstrap-datepicker-comncls" tabindex="-1" data-placeholder="DD/MM/YYYY" value="<?php echo date('d/m/Y', strtotime($sales_order->so_date)); ?>" />

                                                        </label>

                                                    </section>



                                                    <section>

                                                        <label class="label">Remarks</label>

                                                        <label class="textarea ">

                                                            <textarea rows="3" class="show_txt_count" name="remarks" data-placeholder="Remarks" maxlength="250"><?php echo form_prep($sales_order->remarks); ?></textarea>

                                                        </label>

                                                    </section>

                                                    <section>

                                                        <label class="label">Delivered On</label>

                                                        <label class="input">

                                                            <i class="icon-append fa fa-calendar"></i>

                                                            <input type="text" name="do_date" id="do_date" class="form-control bootstrap-datepicker-comncls" tabindex="-1" value="<?php echo text_date($sales_order->do_date, '', 'd/m/Y'); ?>" />

                                                        </label>

                                                    </section>

                                                </section>

                                            </div>

                                        </fieldset>

                                        <fieldset>

                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Exchange Order Item</legend>

                                            

                                            <div class="row material-container exc-product_details">

                                               

                                                <div class="items_head col-xs-12 clr">

                                                    <section class="col col-2">

                                                        <label class="label" style="text-align: center;"><b>Item Code</b></label>

                                                    </section>

                                                    <section class="col col-4">

                                                        <label class="label" style="text-align: center;"><b>Brand - Description</b></label>

                                                    </section>

                                                    <section class="col col-1">

                                                        <label class="label" style="text-align: center;"><b>Qty</b></label>

                                                    </section>

                                                    <?php $unit_cost = (in_array(7, $permission)); ?>

                                                    <section class="col col-1">

                                                        <label class="label" style="text-align: center;"><b>Price</b></label>

                                                    </section>



                                                    <?php #if ($unit_cost) { ?>

                                                        <section class="col col-1">

                                                            <label class="label" style="text-align: center;"><b>Cost</b></label>

                                                        </section>

                                                    <?php #} ?>

													<section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                        <label class="label"><b>Discount</b></label>

                                                    </section>

                                                    <section class="col col-1">

                                                        <label class="label" style="text-align: center;"><b>Amount</b></label>

                                                    </section>

													<section class="col col-1"></section>

                                                </div>



                                                <?php $item_count = count($exc_items); ?>

                                                <?php foreach ($exc_items as $item_key => $item) { ?>

                                                    <div class="exc-items exc-divid-<?= $item->exr_id; ?> calculate_items col-xs-12 clr" data-excdivid="<?= $item->exr_id; ?>">

                                                        <input type="hidden" name="hidden_excsoitem_id[<?php echo $item_key; ?>]" value="<?php echo $item->id; ?>" />

                                                        <section class="col col-2" style="padding-right: 0px;">

                                                            <span class="exr_code_cls"><?= $item->exr_code; ?></span>

                                                            <label class="input" style="display: inline-block; width: 64%;">

                                                                <input type="hidden" name="exc_product_id[<?php echo $item_key; ?>]" class="exc_product_id" value="<?php echo $item->item_id; ?>" />

                                                                <input type="text" name="exc_product_code[<?php echo $item_key; ?>]" class="exc_product_code" value="<?php echo $item->pdt_code; ?>" data-value="<?php echo $item->pdt_code; ?>" />

                                                            </label>

                                                            <span class="product_code_label">Selected Item code already exist!</span>

															

															<label style="display: inline-block; width: 34%;">

																<span class="item-serialnum-popup btn btn-primary" style="padding:3px 4px 3px 4px;" title="Quantity should not be empty to enter Serial No.">

																	<i class="fa fa-plus-square-o" style="font-size: 14px;"> </i> Sl No

																</span>

																<input type="hidden" name="exc_serial_no[<?php echo $item_key; ?>]" class="form-control serial_no" data-value="<?php echo $item->serial_no; ?>" value="<?php echo $item->serial_no; ?>" />

															</label>

															<span class="available_qty_label">Available quantity : 0</span>

                                                        </section>



                                                        <section class="col col-4">

                                                            <span>&nbsp;</span>

                                                            <!--<label class="input">

                                                                <input type="text" name="exc_product_description[<?php echo $item_key; ?>]" class="exc_product_description" data-value="<?php echo form_prep($item->product_description); ?>" value="<?php echo form_prep($item->product_description); ?>" />

                                                            </label>-->
                                                            
                                                            <label class="textarea textarea-expandable"><textarea rows="3" name="product_description[<?php echo $item_key; ?>]" class="product_description" data-value="<?php echo form_prep($item->product_description); ?>"><?php echo form_prep($item->product_description); ?></textarea> </label> 

                                                            <span class="error_description">Selected Item description already exist!</span>

                                                        </section>



                                                        <section class="col col-1">

                                                            <span>&nbsp;</span>

                                                            <label class="input">

                                                                <input type="text" name="exc_quantity[<?php echo $item_key; ?>]" class="exc_quantity quantity float_value" value="<?php echo $item->quantity; ?>" style="padding-left: 2px; padding-right: 2px; text-align: right;" />

                                                            </label>

                                                            <span class="unit_label"></span>

                                                        </section>



                                                        <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                            <span>&nbsp;</span>

                                                            <label class="input">

                                                                <!--<i class="icon-prepend fa fa-dollar"></i>-->

                                                                <input type="text" name="exc_price[<?php echo $item_key; ?>]" class="exc_price price float_value" value="<?php echo $item->price; ?>" style="padding-left: 2px; padding-right: 2px;" />

                                                            </label>

                                                        </section>



                                                        <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                            <span>&nbsp;</span>

                                                            <label class="input" style="margin-top: 1px;">

                                                                <!--<i class="icon-prepend fa fa-dollar"></i>-->

                                                                <input type="text" name="exc_cost[<?php echo $item_key; ?>]" class="exc_cost cost float_value" value="<?php echo $item->unit_cost; ?>" style="padding-left: 2px; padding-right: 2px;" />

                                                            </label>

                                                        </section>

														

														<section class="col col-1" style="padding: 20px 5px 0px 5px;">

															<div class="input-group">

																<input type="text" name="exc_item_discount[<?php echo $item_key;?>]" class="form-control exc_item_discount item_discount float_value" style="height: 30px; padding-right: 5px;" value="<?php echo $item->discount; ?>" />

																<input type="hidden" name="exc_item_discount_amt[<?php echo $item_key;?>]" class="exc_item_discount_amt item_discount_amt" value="<?php echo $item->discount_amount; ?>" />

																<span style="display: block; width: 100%; text-align: right;">S$ <span class="exc_item_discount_amt item_discount_amt"><?php echo $item->discount_amount; ?></span></span>

																<span class="onoffswitch" style="float: right;">

																	<input type="checkbox" name="exc_item_discount_type[<?php echo $item_key;?>]" class="onoffswitch-checkbox exc_item_discount_type item_discount_type" id="exc_item_discount_type-<?php echo $item_key;?>" value="1" <?php echo ($item->discount_type == '1') ? 'checked' : ''; ?> />

																	

																	<label class="onoffswitch-label" for="exc_item_discount_type-<?php echo $item_key;?>"> 

																		<span class="onoffswitch-inner" data-swchon-text="%" data-swchoff-text="S$"></span> 

																		<span class="onoffswitch-switch"></span> 

																	</label> 

																</span>

															</div>

														</section>

														

                                                        <section class="col col-1">

                                                            <span>&nbsp;</span>

                                                            <label class="input">

                                                                <!--<i class="icon-prepend fa fa-dollar"></i>-->

                                                                <input type="text" name="exc_amount[<?php echo $item_key; ?>]" class="exc_amount amount float_value" value="<?php echo $item->total; ?>" style="padding-left: 2px; padding-right: 2px;" readonly="" />

                                                            </label>

                                                        </section>



                                                        <section class="col col-1">

                                                            <span>&nbsp;</span>

                                                            <label class="input">

                                                                <?php if ($item_count == ($item_key + 1)) { ?>

                                                                    <i class="fa fa-2x fa-plus-circle exc-add-row"></i>

                                                                <?php } ?>

                                                                <?php if ($item_count > 1) { ?>

                                                                    <i class="fa fa-2x fa-minus-circle exc-remove-row"></i>

                                                                <?php } ?>

                                                            </label>

                                                        </section>

                                                    </div>

                                                <?php } ?>

                                            </div>

                                            

                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Items From Inventory</legend>

                                            <div class="row product_search">

                                                <section class="col col-5">

                                                    <div class="input hidden-mobile">

                                                        <input name="search_product" class="form-control search_product lowercase" type="text" id="search_product" tabindex="-1" autofocus />

                                                    </div>

                                                </section>

                                            </div>

                                            <div class="row inv-material-container product_details">

                                                 <div class="col-xs-12">

                                                   &nbsp;

                                                </div>

                                                <div class="items_head col-xs-12 clr">

                                                    <section class="col col-2">

                                                        <label class="label" style="text-align: center;">

                                                            <b>Item Code</b>

                                                        </label>

                                                    </section>

                                                    <section class="col col-4">

                                                        <label class="label" style="text-align: center;"><b>Brand Name - Description</b></label>

                                                    </section>

                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                        <label class="label" style="text-align: center;"><b>Qty</b></label>

                                                    </section>

                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                        <label class="label" style="text-align: center;"><b>Price</b></label>

                                                    </section>

                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                        <label class="label" style="text-align: center;"><b>Cost</b></label>

                                                    </section>

													<section class="col col-1">

                                                        <label class="label" style="text-align: center;"><b>Discount</b></label>

                                                    </section>

                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                        <label class="label" style="padding-left: 60px;"><b>Amount</b></label>

                                                    </section>

													<section class="col col-1"></section>

                                                </div>

                                                <?php $invitems_count = count($inventory_items); ?>

                                                <?php if(count($inventory_items)>0) {

													foreach ($inventory_items as $key => $inventory) { ?>

                                                <div class="inv-items items calculate_items col-xs-12 clr">

                                                    <section class="col col-2" style="padding-right: 0px;">

                                                        <label class="input" style="display: inline-block; width: 64%;">

                                                            <input type="hidden" name="hidden_soitem_id[<?= $key; ?>]" class="hidden_soitem_id" value="<?= $inventory->id; ?>" />

                                                            <input type="hidden" name="category_id[<?= $key; ?>]" class="category_id" value="<?= $inventory->category_id; ?>" />

                                                            <input type="hidden" name="product_id[<?= $key; ?>]" class="product_id" value="<?= $inventory->item_id; ?>" />

                                                            <input type="text" name="product_code[<?= $key; ?>]" class="product_code product_code_valcls" value="<?= $inventory->pdt_code; ?>" data-value="<?= $inventory->pdt_code; ?>" />

                                                        </label>

                                                        <span class="product_code_label">Selected Item code already exist!</span>

														

														<label style="display: inline-block; width: 34%;">

															<span class="item-serialnum-popup btn btn-primary" style="padding:3px 4px 3px 4px;" title="Quantity should not be empty to enter Serial No.">

																<i class="fa fa-plus-square-o" style="font-size: 14px;"> </i> Sl No

															</span>

															<input type="hidden" name="serial_no[<?= $key; ?>]" class="form-control serial_no" data-value="<?= $inventory->serial_no; ?>" value="<?= $inventory->serial_no; ?>" />

														</label>

														<span class="available_qty_label">Selected Item code already exist!</span>

                                                    </section>



                                                    <section class="col col-4">

                                                        <label class="input">

                                                            <input type="text" name="product_description[<?= $key; ?>]" class="product_description" value="<?= $inventory->product_description; ?>" data-value="<?= $inventory->product_description; ?>" />

                                                        </label>

                                                        <span class="error_description">Selected Item description already exist!</span>

                                                    </section>



                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                        <label class="input">

                                                            <input type="text" name="quantity[<?= $key; ?>]" class="inv_quantity quantity float_value" value="<?= $inventory->quantity; ?>" />

                                                        </label>

                                                        <span class="unit_label"></span>

                                                    </section>



                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                        <label class="input">

                                                            <!--<i class="icon-prepend fa fa-dollar"></i>-->

                                                            <input type="text" name="price[<?= $key; ?>]" class="inv_price price float_value" value="<?= $inventory->price; ?>"/>

                                                        </label>

                                                    </section>

                                                    

                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                        <label class="input" style="margin-top: 1px;">

                                                            <!--<i class="icon-prepend fa fa-dollar"></i>-->

                                                            <input type="text" name="cost[<?= $key; ?>]" class="inv_cost cost float_value" value="<?= $inventory->unit_cost; ?>" />

                                                        </label>

                                                    </section>

													

													<section class="col col-1" style="padding: 0px 15px 0px 5px;">

														<div class="input-group">

															<input type="text" name="item_discount[<?php echo $key;?>]" class="form-control item_discount float_value" style="height: 30px; padding-right: 5px;" value="<?php echo $inventory->discount; ?>" />

															<input type="hidden" name="item_discount_amt[<?php echo $key;?>]" class="item_discount_amt" value="<?php echo $inventory->discount_amount; ?>" />

															<span style="display: block; width: 100%; text-align: right;">S$ <span class="item_discount_amt"><?php echo $inventory->discount_amount; ?></span></span>

															<span class="onoffswitch" style="float: right;">

																<input type="checkbox" name="item_discount_type[<?php echo $key;?>]" class="onoffswitch-checkbox item_discount_type" id="item_discount_type-<?php echo $key;?>" value="1" <?php echo ($inventory->discount_type == '1') ? 'checked' : ''; ?> />

																

																<label class="onoffswitch-label" for="item_discount_type-<?php echo $key;?>"> 

																	<span class="onoffswitch-inner" data-swchon-text="%" data-swchoff-text="S$"></span> 

																	<span class="onoffswitch-switch"></span> 

																</label> 

															</span>

														</div>

													</section>

													

                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                        <label class="input">

                                                            <!--<i class="icon-prepend fa fa-dollar"></i>-->

                                                            <input type="text" name="amount[<?= $key; ?>]" class="inv_amount amount float_value" value="<?= $inventory->total; ?>" readonly="" />

                                                        </label>

                                                    </section>

                                                    <section class="col col-1">

                                                        <label class="input">

                                                            <?php if ($invitems_count == ($key + 1)) { ?>

                                                                <i class="fa fa-2x fa-plus-circle inv-add-row"></i>

                                                            <?php } ?>

                                                            <?php if ($invitems_count > 1) { ?>

                                                                <i class="fa fa-2x fa-minus-circle inv-remove-row"></i>

                                                            <?php } ?>

                                                        </label>

                                                    </section>

                                                </div>

                                                <?php } } else { ?>

                                                <div class="inv-items items calculate_items col-xs-12 clr">

                                                    <section class="col col-2" style="padding-right: 0px;">

                                                        <label class="input" style="display: inline-block; width: 64%;">

                                                            <input type="hidden" name="new_category_id[0]" class="category_id" value="" />

                                                            <input type="hidden" name="new_product_id[0]" class="product_id" value="" />

                                                            <input type="text" name="new_product_code[0]" class="product_code product_code_valcls" data-value="" />

                                                        </label>

                                                        <span class="product_code_label">Selected Item code already exist!</span>

														

														<label style="display: inline-block; width: 34%;">

															<span class="item-serialnum-popup btn btn-primary" style="padding:3px 4px 3px 4px;" title="Quantity should not be empty to enter Serial No.">

																<i class="fa fa-plus-square-o" style="font-size: 14px;"> </i> Sl No

															</span>

															<input type="hidden" name="new_serial_no[0]" data-value="" class="form-control serial_no" />

														</label>

														<span class="available_qty_label">Selected Item code already exist!</span>

                                                    </section>



                                                    <section class="col col-4">

                                                        <label class="input">

                                                            <input type="text" name="new_product_description[0]" class="product_description" data-value="" />

                                                        </label>

                                                        <span class="error_description">Selected Item description already exist!</span>

                                                    </section>



                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                        <label class="input">

                                                            <input type="text" name="new_quantity[0]" class="inv_quantity quantity float_value" />

                                                        </label>

                                                        <span class="unit_label"></span>

                                                    </section>



                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                        <label class="input">

                                                            <!--<i class="icon-prepend fa fa-dollar"></i>-->

                                                            <input type="text" name="new_price[0]" class="inv_price price float_value" />

                                                        </label>

                                                    </section>

                                                    

                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                        <label class="input" style="margin-top: 1px;">

                                                            <!--<i class="icon-prepend fa fa-dollar"></i>-->

                                                            <input type="text" name="new_cost[0]" class="inv_cost cost float_value" />

                                                        </label>

                                                    </section>

													

													<section class="col col-1" style="padding: 0px 15px 0px 5px;">

														<div class="input-group">

															<input type="text" name="new_item_discount[0]" class="form-control item_discount float_value" style="height: 30px; padding-right: 5px;" value="0" />

															<input type="hidden" name="new_item_discount_amt[0]" class="item_discount_amt" value="0.00" />

															<span style="display: block; width: 100%; text-align: right;">S$ <span class="item_discount_amt">0.00</span></span>

															<span class="onoffswitch" style="float: right;">

																<input type="checkbox" name="item_discount_type[0]" class="onoffswitch-checkbox item_discount_type" id="item_discount_type-0" value="1" checked />

																

																<label class="onoffswitch-label" for="item_discount_type-0"> 

																	<span class="onoffswitch-inner" data-swchon-text="%" data-swchoff-text="S$"></span> 

																	<span class="onoffswitch-switch"></span> 

																</label> 

															</span>

														</div>

													</section>



                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px;">

                                                        <label class="input">

                                                            <!--<i class="icon-prepend fa fa-dollar"></i>-->

                                                            <input type="text" name="new_amount[0]" class="inv_amount amount float_value" readonly="" />

                                                        </label>

                                                    </section>

                                                    <section class="col col-1">

                                                        <label class="input">

                                                            <i class="fa fa-2x fa-plus-circle inv-add-row"></i>

                                                        </label>

                                                    </section>

                                                </div>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="row" style="margin-top: 15px;">

                                                <section class="col col-6">

                                                </section>

                                                <section class="col col-3">

                                                    <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Sub Total</b></label>

                                                </section>

                                                <section class="col col-2">

                                                    <label class="input">

                                                        <i class="icon-prepend fa fa-dollar"></i>

                                                        <input type="text" name="sub_total" class="sub_total" value="<?php echo $sales_order->sub_total; ?>" readonly="" />

                                                    </label>

                                                </section>

                                                <section class="col col-1">

                                                </section>

                                            </div>



                                            <div class="row">

                                                <section class="col col-6">

                                                </section>

                                                <section class="col col-3">

                                                    <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Discount</b></label>

                                                </section>

                                                <section class="col col-2">

                                                    <!--<label class="input">

                                                        <i class="icon-prepend fa fa-dollar"></i>

                                                        <input type="text" name="sub_total" class="sub_total" readonly="" data-placeholder="0.00" />

                                                    </label>-->



                                                    <div class="input-group">

                                                        <input type="text" name="discount" class="form-control discount float_value" value="<?php echo $sales_order->discount; ?>" data-placeholder="0.00" />

                                                        <span class="input-group-addon">

                                                            <span class="onoffswitch">

                                                                <input type="checkbox" name="discount_type" class="onoffswitch-checkbox discount_type" id="discount_type-0" value="1" <?php echo ($sales_order->discount_type) ? 'checked' : ''; ?> />

                                                                <label class="onoffswitch-label" for="discount_type-0"> 

                                                                    <span class="onoffswitch-inner" data-swchon-text="%" data-swchoff-text="S$"></span> 

                                                                    <span class="onoffswitch-switch"></span> 

                                                                </label> 

                                                            </span>

                                                        </span>

                                                    </div>

                                                    <input type="hidden" name="discount_amt" class="discount_amt" value="<?php echo $sales_order->discount_amount; ?>" />

                                                    <span class="discount_container">

                                                        <span class="currency">S$</span>

                                                        <span class="discount_amount">0.00</span>

                                                    </span>

                                                </section>

                                                <section class="col col-1">

                                                </section>

                                            </div>



                                            <div class="row gst_details" style="display:<?php echo (empty($company->comp_gst_no)) ? 'none' : 'block'; ?>">

                                                <section class="col col-4">

                                                </section>

                                                <section class="col col-2">&nbsp;</section>

                                                <section class="col col-3">

                                                    <section>

                                                        <label class="label">&nbsp;</label>

                                                        <?php if(!empty($company->comp_gst_no)){?>

                                                        <div class="inline-group">

                                                            <label class="radio">

                                                                <input type="radio" class="gst_type" name="gst_type" value="1" <?php echo ($sales_order->gst == 1) ? 'checked' : ''; ?> /><i></i>Include GST

                                                            </label>

                                                            <label class="radio">

                                                                <input type="radio" class="gst_type" name="gst_type" value="2" <?php echo ($sales_order->gst == 2) ? 'checked' : ''; ?> /><i></i>Exclude GST

                                                            </label>

                                                        </div>

                                                        <?php }else{?>

                                                        <div class="inline-group">

                                                            <label class="radio">

                                                                <input type="radio" class="gst_type" name="gst_type" value="1" <?php echo (empty($company->comp_gst_no)) ? 'checked' : ''; ?> /><i></i>Include GST

                                                            </label>

                                                            <label class="radio">

                                                                <input type="radio" class="gst_type" name="gst_type" value="2" <?php echo (!empty($company->comp_gst_no)) ? 'checked' : ''; ?> /><i></i>Exclude GST

                                                            </label>

                                                        </div>

                                                        <?php }?>

                                                        

                                                        

                                                    </section>

                                                </section>

                                                <section class="col col-2">

                                                    <label class="label">&nbsp;</label>

                                                    <label class="input change_perc_disamt"><i class="icon-prepend fa fa-dollar"></i>

                                                        <input type="text" name="gst_amount" class="gst_amount float_value" value="<?php echo ($sales_order->gst == 2) ? $sales_order->gst_amount : '0.00'; ?>" data-placeholder="0.00" readonly="" />

                                                        <input type="hidden" value="<?php echo $sales_order->gst_percentage; ?>" name="hidden_gst" id="hidden_gst" />

                                                    </label>

                                                    <span class="gst_val" style="display: <?php echo (!empty($sales_order->gst)) ? 'block' : 'none'; ?>">GST @ <span class="gst_percentage"><?php echo ($sales_order->gst == 2) ? $sales_order->gst_percentage : 0; ?></span> %</span>

                                                </section>

                                            </div>



                                            <div class="row">

                                                <section class="col col-6">

                                                </section>

                                                <section class="col col-3">

                                                    <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Total Amount</b></label>

                                                </section>

                                                <section class="col col-2">

                                                    <label class="input">

                                                        <i class="icon-prepend fa fa-dollar"></i>

                                                        <input type="text" name="total_amt" class="total_amt" value="<?php echo $sales_order->total_amt; ?>" readonly="" />

                                                    </label>

                                                </section>

                                                <section class="col col-1">

                                                </section>

                                            </div>

                                        </fieldset>



                                        <footer>

                                            <button type="submit" class="btn btn-primary" >Submit</button>

                                            <button type="reset" class="btn btn-default" >Cancel</button>

                                        </footer>

                                    </div>

                                    <div class="smart-form" id ="payment_form" style="display: none;">

                                        <input type="hidden" name="payment_id" value="<?php echo (!empty($payment)) ? $payment->id : ''; ?>" />

                                        <fieldset>

                                            <div class="row">

                                                <section class="col col-md-6">

                                                    <section>

                                                        <label class="label">Total Amount</label>

                                                    </section>

                                                </section>

                                                <section class="col col-md-6">

                                                    <section>

                                                        <label class="input">S$ <span class="payment_amount"><?php echo $sales_order->total_amt; ?></span></label>

                                                    </section>

                                                </section>

                                            </div>

                                            <div class="row">

                                                <section class="col col-md-6">

                                                    <section>

                                                        <label class="label">Payment Type<?php //print_r($payment);  ?></label>

                                                    </section>

                                                </section>

                                                <section class="col col-md-6">

                                                    <section>

                                                        <label class="select">

                                                            <select name="payment_type"  class="select2 payment_type" id="payment_type">

                                                                <option value="">Select Payment Type</option>

                                                                <?php foreach ($payment_types as $key => $value) { ?>

                                                                    <option value="<?php echo $value->id; ?>" data-is_cheque="<?php echo $value->is_cheque; ?>" <?php echo ((!empty($payment->pay_pay_type)) && ($payment->pay_pay_type == $value->id)) ? 'selected' : ''; ?> data-group="<?php echo $value->group_name;?>"><?php echo $value->type_name; ?></option>

                                                                <?php } ?>

                                                            </select>

                                                             <input type="hidden" name="data_group" id="data_group" value="<?php echo $sales_order->payment_group_type; ?>">

                                                        </label>

                                                    </section>



                                                    <div class="cheque_details" style="display: <?php echo ((!empty($payment->pay_pay_type)) && $payment->is_cheque == 1) ? 'block' : 'none'; ?>">

                                                        <section>

                                                            <label class="label">Bank Name<span style="color:red">*</span></label>

                                                            <label class="input">

                                                                <input type="text" class="form-control" name="cheque_bank_name" value="<?php echo (!empty($payment->pay_pay_bank)) ? $payment->pay_pay_bank : $bnk_acc_name; ?>" data-placeholder="SBI" maxlength="100" />

                                                            </label>

                                                        </section>

                                                        <section>

                                                            <label class="label">Cheque No<span style="color:red">*</span></label>

                                                            <label class="input">

                                                                <input type="text" class="form-control" name="cheque_acc_no" value="<?php echo (!empty($payment->pay_pay_no)) ? $payment->pay_pay_no : ''; ?>" data-placeholder="Cheque No" maxlength="100" />

                                                            </label>

                                                        </section>

                                                        <section>

                                                            <label class="label">Cheque Date<span style="color:red">*</span></label>

                                                            <label class="input">

                                                                <input type="text" class="bootstrap-datepicker-strdatecls" name="cheque_date" value="<?php echo (!empty($payment->pay_pay_date)) ? date('d/m/Y', strtotime($payment->pay_pay_date)) : ''; ?>" data-placeholder="01/01/2016" maxlength="100" />

                                                            </label>

                                                        </section>

                                                    </div>



                                                    <!--<div class="dd_details" style="display: none;">

                                                        <section>

                                                            <label class="label">DD No<span style="color:red">*</span></label>

                                                            <label class="input">

                                                                <input type="text" class="form-control" name="dd_no" value="<?php echo (!empty($payment->pay_dd_no)) ? $payment->pay_dd_no : ''; ?>" data-placeholder="DD No" maxlength="100" />

                                                            </label>

                                                        </section>

                                                        <section>

                                                            <label class="label">DD Date<span style="color:red">*</span></label>

                                                            <label class="input">

                                                                <input type="text" class="bootstrap-datepicker-comncls" name="dd_date" value="<?php echo (!empty($payment->pay_dd_date)) ? date('d/m/Y', strtotime($payment->pay_dd_date)) : ''; ?>" data-placeholder="DD Date" maxlength="100" />

                                                            </label>

                                                        </section>

                                                    </div>-->

                                                </section>

                                            </div>

                                        </fieldset>

                                        <footer>

                                            <button type="button" class="btn btn-default" onclick="show_sales_form();">Back</button>

											<?php if(in_array(10, $permission)) {?>

												<button type="submit" class="btn btn-success" id="pay_now" name="payment" value="pay_now" >Pay Now</button>

											<?php }?>

                                            <button type="submit" class="btn btn-warning" id="skip_payment" name="payment" value="skip_payment" onclick="disable_payment();">Skip Payment</button>

                                        </footer>

                                    </div>

                                </div>

                            </div>

                            <!-- /.col -->

                        </div>

                    </article>

                    <!-- /.row -->

                </div>

            </section>

        </form>

        <!-- /.content -->

    </div>

    <!-- /.main -->

</div>

<script type="text/javascript">

    var payment_terms = <?php echo json_encode($payment_terms, JSON_UNESCAPED_SLASHES); ?>;

</script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/exchange_order_invoice.js"></script>