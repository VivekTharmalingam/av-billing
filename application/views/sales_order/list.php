<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment">
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span>
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>

        <!-- Main content -->
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo $add_link; ?>" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-pencil"></i></button></a>
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false">

                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Invoice List</h2>
<!--                            <span style=" float: right; margin-top: 6px;"><a href="<?php echo base_url(); ?>purchase_orders/add" style=" color: white;"><i class="fa fa-lg fa-plus-circle" aria-hidden="true"></i></a></span>-->
                        </header>

                        <div>
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <div class="widget-body no-padding">
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;" data-hide="phone" data-class="expand">S.No</th>
                                            <th style="width: 14%;" ><i class="fa fa-fw fa-key txt-color-blue hidden-sm hidden-xs"></i>Inv No.</th>
                                            <th style="width: 10%;" data-hide="phone,tablet"><i class="fa fa-fw fa-calander txt-color-blue hidden-md hidden-sm hidden-xs"></i>Inv Date</th>
                                            <th style="width: 23%;" data-hide="phone"><i class="fa fa-fw fa-building txt-color-blue hidden-md hidden-sm hidden-xs"></i>Customer Name</th>
                                            <th style="width: 15%;" data-hide="phone,tablet"><i class="fa fa-fw fa-building txt-color-blue hidden-md hidden-sm hidden-xs"></i>Customer Ref.</th>
                                            <th style="width: 13%;" data-hide="phone,tablet"><i class="fa fa-fw fa-money txt-color-blue hidden-md hidden-sm hidden-xs"></i>Total Amt (S$)</th>
                                            <!--<th style="width: 15%;" data-hide="phone,tablet"><i class="fa fa-fw fa-exclamation-triangle txt-color-blue hidden-md hidden-sm hidden-xs"></i>Status</th>-->
                                            <th style="width: 20%;" style="text-align: center;"><i class="fa fa-fw fa-cog txt-color-blue hidden-xs"></i> Actions</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                </article>
                <!-- /.row -->
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/your_style.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/datatables.js"></script>
<script type="text/javascript">
    $(function () {
        $(document).on('click', '#smart-mod-eg2', function (e) {
            $('#sendEmail').modal();

            var $action = $(this).attr('data-href');
            var $id = $(this).attr('data-id');
            $('#sendEmail form').attr('action', $action);
            $('#sendEmail').find('p').text('Do you want to send email for the Invoice?');
            var ajaxURL = baseUrl + 'invoice/get_email_by_id';
            var ajaxData = {
                id: $id
            }
            var details = ajaxRequest(ajaxURL, ajaxData, '');
            if (details.email != '' && details.email != null) {
                $('#sendEmail input.mail_txt').val(details.email);
            }

        });

        $(document).on('click', '#sendEmail .ok', function (e) {
            var emailID = $('#sendEmail .mail_txt').val();
            var atpos = emailID.indexOf("@");
            var dotpos = emailID.lastIndexOf(".");
            if ($('#sendEmail .mail_txt').val() == '') {
                $('#sendEmail .error_msg').text('Please enter Email Address!');
                return false;
            } else if (atpos < 1 || (dotpos - atpos < 2)) {
                $('#sendEmail .error_msg').text('Invalid Email Address!');
                return false;
            } else {
                $('#sendEmail .error_msg').text('');
            }
        });
    });
</script>