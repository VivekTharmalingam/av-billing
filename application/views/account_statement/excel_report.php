<?php
$ti = time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Account_Statement_" . $ti . ".xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<?php if (!empty($custom)) { ?>
    <div class="header_div">
        STATEMENT OF ACCOUNT AS AT <?php echo date('d M Y', strtotime($from_date)); ?>
    </div><br>
    <table width="100%" border="1" style="border-collapse:collapse;width: 1000px">
            <tbody>
                <tr>
                    <td style="width:35%;font-weight: bold;">Customer Name</td>
                    <td><?php echo (!empty($cus_details->name)) ? $cus_details->name : 'N/A'; ?></td>
                </tr>
                <tr>
                    <td style="width:35%;font-weight: bold;">Address</td>
                    <td><?php echo (!empty($cus_details->address)) ? $cus_details->address : 'N/A'; ?></td>
                </tr>
                <tr>
                    <td style="width:35%;font-weight: bold;">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="width:35%;font-weight: bold;">Attention</td>
                    <td><?php echo (!empty($cus_details->contact_name)) ? $cus_details->contact_name : 'N/A'; ?></td>
                </tr>
                <tr>
                    <td style="width:35%;font-weight: bold;">Phone Number</td>
                    <td><?php echo (!empty($cus_details->phone_no)) ? $cus_details->phone_no : 'N/A'; ?></td>
                </tr>
                <tr>
                    <td style="width:35%;font-weight: bold;">Fax</td>
                    <td><?php echo (!empty($cus_details->fax)) ? $cus_details->fax : 'N/A'; ?></td>
                </tr>
            </tbody>
    </table><br>
    <?php } ?>
    <?php if ($list_data == '1') { ?>
        <table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; margin-left:10px" cellpadding="5">
            <tr style="font-family:Times new roman;">
                <th width="15%">Inv Date</th>
                <th width="15%">Inv No</th>
                <th width="20%">Description</th>
                <th width="15%">Debit Amount</th>
                <th width="15%">Credit Amount</th>
                <th width="15%">Balance</th>
            </tr>
            <?php if (count($purchase)) { ?>
                <?php
                $to_amt = $pay_amt = $balance = $credit_tot = $balance_tot = 0;
                foreach ($purchase as $key => $row) {
                    $to_amt += $row->total_amt;
                    $pay_amt = (!empty($row->pay_amt)) ? $row->pay_amt : 0;
					$balance = $row->total_amt - $pay_amt;
					$credit_tot += $pay_amt;
                    $balance_tot += $balance;
                    ?>                    
                    <tr>
                        <td><?php echo nl2br($row->so_date_str); ?></td>                            
                        <td><?php echo text($row->so_no); ?></td>
                        <td><?php echo 'INVOICE'; ?></td>
                        <td><?php echo number_format($row->total_amt, 2); ?></td>
                        <td><?php echo number_format($pay_amt,2);?></td>
                        <td align="right"><?php echo number_format($balance, 2); ?></td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td colspan="6" align="center">No Record Found</td>
                </tr>
            <?php } ?>
            <?php if (!empty($purchase)) { ?>
                <tfoot style="font-weight: bold;"><tr>
                    <td colspan="3" style="text-align: right;">Total (S$)</td>
                            <td align="right"><?php echo number_format($to_amt, 2); ?></td>
                            <td align="right"><?php echo number_format($credit_tot, 2); ?></td>
                            <td align="right"><?php echo number_format($balance_tot, 2); ?></td>

                        </tr></tfoot>
            <?php } ?>
        </table><br>
        <table width="100%" border="1" style="border-collapse:collapse; width: 250px;">
            <tbody>
                <tr>
                    <td style="width:35%;"><b>Credit Term</b></td>
                    <td style="width:65%;" align="right">Now Overdue</td>
                </tr>
                <?php foreach ($paT as $key => $value) { if(!empty($value['amt'])){ ?>
                    <tr>
                        <td style="width:35%;"><b><?php echo $value['title']; ?></b></td>
                        <td style="width:65%;" align="right">S$ <?php echo (!empty($value['amt'])) ? number_format($value['amt'], 2) : '0.00'; ?></td>
                    </tr>
                <?php } }?>
            </tbody>
        </table><br>
        <table width="100%" border="1" style="border-collapse:collapse;">
            <tbody>
                <tr>
                    <td style="width:20%;">&nbsp;</td>
                    <td style="font-weight: bold;">Current</td>
                    <td style="font-weight: bold;">> 30 days</td>
                    <td style="font-weight: bold;">> 60 days</td>
                    <td style="font-weight: bold;">> 90 days</td>
                    <td style="font-weight: bold;">> 120 days</td>
                </tr>
                <tr>
                    <td style="width:20%;">Aging Balance</td>
                    <td style="font-weight: bold;"><?php echo number_format($curent_amt, 2); ?></td>
                    <td style="font-weight: bold;"><?php echo number_format($two, 2); ?></td>
                    <td style="font-weight: bold;"><?php echo number_format($three, 2); ?></td>
                    <td style="font-weight: bold;"><?php echo number_format($four, 2); ?></td>
                    <td style="font-weight: bold;"><?php echo number_format($five, 2); ?></td>
                </tr>
            </tbody>
        </table>
    <?php } ?>  