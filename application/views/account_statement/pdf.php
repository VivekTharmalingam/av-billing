<table width="100%" border="0" style="font-family: calibri;"  cellpadding="5">
    <tr>
        <td width="100%" style="vertical-align:top; padding: 0; text-align: center; font-weight: bold;font-size: 20px;border-bottom: 2px solid #333;" colspan="6" >STATEMENT OF ACCOUNT <?php echo (!empty($to_date)) ? 'AS AT ' . date('d-M-Y', strtotime($to_date)) : ''; ?></td>
    </tr>          
    <tr>    
        <td style="text-align: left;font-weight: bold;">Customer</td>
        <td style="text-align: left;" colspan="2"><?php echo (!empty($cus_details->name)) ? $cus_details->name : 'N/A'; ?></td>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td style="text-align: left;font-weight: bold;" valign="top">Address</td>
        <td style="text-align: left;" colspan="2"><?php echo (!empty($cus_details->address)) ? $cus_details->address : 'N/A'; ?></td>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td width="20%">&nbsp;</td>
        <td width="20%">&nbsp;</td>
        <td width="20%">&nbsp;</td>
        <td width="20%">&nbsp;</td>
        <td width="10%">&nbsp;</td>
        <td width="10%">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: left;font-weight: bold;">Attention</td>
        <td style="text-align: left;" colspan="2"><?php echo (!empty($cus_details->contact_name)) ? $cus_details->contact_name : 'N/A'; ?></td>
        <td colspan="3"></td>
    </tr>  
    <tr>
        <td style="font-weight: bold;">TEL</td>
        <td><?php echo (!empty($cus_details->phone_no)) ? $cus_details->phone_no : 'N/A'; ?></td>
        <td style="font-weight: bold;">FAX</td>
        <td><?php echo (!empty($cus_details->fax)) ? $cus_details->fax : 'N/A'; ?></td>
        <td colspan="2"></td>
    </tr>
</table>

<table width="100%" border="1" style="border-collapse:collapse; font-family: calibri;" cellpadding="6">
    <thead style="font-size: 13px;">
        <tr>
            <th width="15%" style="background:#EBEBEB;">Inv Date</th>
            <th width="15%" style="background:#EBEBEB;">Inv No</th>
            <th width="20%" style="background:#EBEBEB;">Customer Ref.</th>
            <th width="15%" style="background:#EBEBEB;">Debit Amount</th>                    
            <th width="15%" style="background:#EBEBEB;">Credit Amount</th>
            <th width="15%" style="background:#EBEBEB;">Balance</th>
        </tr>
    </thead>

    <tbody style="font-size: 11px;">
        <?php
        if (!empty($invoices)) {
            $to_amt = $pay_amt = $balance = $credit_tot = $balance_tot = 0;
            foreach ($invoices as $key => $row) {
                $to_amt += $row->total_amt;
                $pay_amt = (!empty($row->pay_amt)) ? $row->pay_amt : 0;
				$balance = $row->total_amt - $pay_amt;
				$credit_tot += $pay_amt;
                $balance_tot += $balance;
                //print_r($row);
                ?>
                <tr style="vertical-align:top;">
                    <td style="border-top: 0; border-bottom: none;"><?php echo $row->so_date_str; ?></td>
                    <td style="border-top: 0;border-bottom: none;"><?php echo text($row->so_no); ?></td>
                    <td align="center" style="border-top: 0;border-bottom: none;"><?php echo text($row->your_reference); ?></td>
                    <td align="right" style="border-top: 0;border-bottom: none;">$<?php echo number_format($row->total_amt, 2); ?></td>
                    <td align="right" style="border-top: 0;border-bottom: none;">$<?php echo number_format($pay_amt, 2); ?></td>
                    <td align="right" style="border-top: 0;border-bottom: none;">$<?php echo number_format($balance, 2); ?></td>
                </tr>  
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="6" align="center">No Record Found</td>
            </tr>
        <?php } ?>
        <?php if (!empty($invoices)) { ?>
            <tr>
                <td colspan="3" style="border: 0px;"></td>
                <td colspan="2"  style="border: 0px; padding-left: 10px;font-weight: bold;"> BALANCE</td>
                <td colspan="1"  style="border: 0px; padding-right: 10px;font-weight: bold;text-align: right;">$<?php echo number_format($balance_tot, 2); ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<br />
<?php
$val = 1;
foreach ($paT as $key => $value) {
    if (!empty($value['amt'])) {
        $val = 2;
    }
} 
if ($val ==2) {
    ?>
    <table width="100%" style="border-collapse:collapse; font-family: calibri;">
        <tr>
            <td width="20%">&nbsp;</td>
            <td width="20%">&nbsp;</td>
            <td width="15%">&nbsp;</td>
            <td width="15%">&nbsp;</td>
            <td width="15%">&nbsp;</td>
            <td width="15%">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4"></td>
            <td style="padding: 5px; border:1px solid;"><b>Credit Term</b></td>
            <td style="padding: 5px; border:1px solid;" align="right">Now Overdue</td>
        </tr>
        <?php foreach ($paT as $key => $value) {
            if (!empty($value['amt'])) { ?>
                <tr>
                    <td colspan="4"></td>
                    <td style="padding: 5px; border:1px solid;"><b><?php echo $value['title']; ?></b></td>
                    <td style="padding: 5px; border:1px solid;" align="right">$<?php echo (!empty($value['amt'])) ? number_format($value['amt'], 2) : '0.00'; ?></td>
                </tr>
            <?php }
        } ?>
    </table><?php } ?>
