<?php //print_r($from_date);exit;  ?>
<table border="0" cellpadding="3" cellspacing="0" style="font-family: calibri; font-size:14px; width: 1000px;">
    <?php echo $head; ?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>

<table width="100%" border="1" style="font-family: calibri; border-collapse:collapse;" cellpadding="3">
    <thead>
        <tr style="font-size:16px;">
            <th width="12%">Inv No</th>
            <th width="11%">Inv Date</th>
            <th width="30%">Customer</th>
            <th width="19%">Customer Ref.</th>
            <th width="13%">Pay Status</th>
            <th width="15%">Amount</th>
        </tr>
    </thead>
    <?php if (count($purchase)) { ?>
        <tbody>
            <?php
            $to_amt = $profit = 0;
            foreach ($purchase as $key => $row) {
                $to_amt += $row->total_amt;
                $profit += $row->tot_profit;
                ?>                    
                <tr>
                    <td><?php echo text($row->so_no); ?></td>                            
                    <td><?php echo nl2br($row->so_date_str); ?></td>
                    <td><?php echo strtoupper(text($row->name)); ?></td>
                    <td><?php echo strtoupper(text($row->your_reference)); ?></td>
                    <td><?php echo text($row->pay_sts); ?></td>
                    <td align="right"><?php echo number_format($row->total_amt, 2); ?></td>
                </tr>
    <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="6" align="center">No Record Found</td>
            </tr>
<?php } ?>
    </tbody>
<?php if (!empty($purchase)) { ?>
                <!--<tfoot>
                        <tr>
                                <td colspan="5">&nbsp;</td>
                                <td align="right" style="height: 30px;"><?php echo number_format($to_amt, 2); ?></td>
                        </tr>
                </tfoot>-->
<?php } ?>
</table>