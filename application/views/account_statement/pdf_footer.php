<table width="100%" border="0" style="font-family: calibri; border-collapse:collapse;" cellpadding="6">
    <tr>
        <td  width="100%" colspan="6">
            <i>Note : Payment Received after <?php echo (!empty($to_date)) ? date('d-M-Y', strtotime($to_date)) : ''; ?> is not reflected in this statement.</i>
        </td>
    </tr>
    <tr>
        <th width="20%" style="background:#EBEBEB;">&nbsp;</th>
        <th width="15%" style="background:#EBEBEB;">Current</th>
        <th width="15%" style="background:#EBEBEB;">> 30 days</th>
        <th width="15%" style="background:#EBEBEB;">> 60 days</th>                    
        <th width="15%" style="background:#EBEBEB;">> 90 days</th>
        <th width="15%" style="background:#EBEBEB;">> 120 days</th>
    </tr>
    <tr style="vertical-align:top;">
        <td width="20%" align="left" style="font-weight: bold;border-top: 0;">Aging Balance</td>
        <td style="font-weight: bold;border-top: 0; text-align: center;">$ <?php echo number_format($curent_amt, 2); ?></td>
        <td style="font-weight: bold;border-top: 0; text-align: center;">$ <?php echo number_format($one, 2); ?></td>
        <td style="font-weight: bold;border-top: 0; text-align: center;">$ <?php echo number_format($two, 2); ?></td>
        <td style="font-weight: bold;border-top: 0; text-align: center;">$ <?php echo number_format($three, 2); ?></td>
        <td style="font-weight: bold;border-top: 0; text-align: center;">$ <?php echo number_format($four, 2); ?></td>
    </tr>
    <tr>
        <td style="border-bottom: 2px solid #999;">&nbsp;</td>
        <td colspan="4" align="center" style="border-bottom: 2px solid #999;">Page {PAGENO}/{nbpg}</td>
        <td align="right" style="border-bottom: 2px solid #999;">DATE : <?php echo date('d-m-Y'); ?></td>
    </tr>
    <tr>
        <td align="left"><b><?php echo $company->comp_name;?></b></td>
        <td colspan="4" align="center">E & E.O</td>
        <td align="right">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="6" align="left">Head Office : <?php echo $company->comp_address;?></td>
    </tr>
    <tr>
        <td colspan="6" align="left">TEL : <?php echo $company->comp_phone;?> , Website : <?php echo $company->comp_website;?> , Fax : <?php echo $company->comp_fax;?>, Email : <?php echo $company->comp_email;?></td>
    </tr>    
</table>
</body>
</html>
