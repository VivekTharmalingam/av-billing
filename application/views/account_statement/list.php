<script>window.localStorage.clear();</script>
<style>
    .header_div{
        padding: 20px;
        text-align: center;
        font-size: 18px;
        width: 100%;
        margin-top: 20px;
        font-weight: bolder;
        color: #2e3192;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">&nbsp;</div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="widget-body">
                        <!-- Widget ID (each widget will need unique ID)-->                                
                        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false"	>

                            <header><span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                <h2>Statement of Account</h2>
                            </header>
                            <div>
                                <!--<div class="date-range"></div>-->
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="box-body table-responsive">
                                        <form name="order_search" action="<?php echo $form_action; ?>" method="post" class="search_form validate_form">
                                            <div class="col-xs-12">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control search_date" id="search_date" data-placeholder="dd/mm/yyyy" style="padding: 6px 12px; margin-right:5%; width: 100%;" name="search_date" value="<?php echo (!empty($search_date)) ? $search_date : date('d/m/Y'); ?>" autocomplete="off" >
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-shopping-cart"></i>
                                                            </div>
                                                            <select name="cus_name" id="customer_name" class="select2 cus_name" style="width: 85%;">
                                                                <option value="">Select Customer</option>
                                                                <?php foreach ($customers as $key => $customer) { ?>  
                                                                    <option value="<?php echo $customer->id; ?>" <?php echo ($customer->id == $custom) ? 'selected' : ''; ?>><?php echo $customer->name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-dollar"></i>
                                                            </div>
                                                            <select name="payment_status" class="select2 payment_status">
                                                                <option value="">Payment status - All</option>
                                                                <option value="1" <?php echo ($pay_sts == 1) ? 'selected' : ''; ?>>Paid</option>
                                                                <option value="2" <?php echo ($pay_sts == 2) ? 'selected' : ''; ?>>Unpaid</option>
                                                                <option value="3" <?php echo ($pay_sts == 3) ? 'selected' : ''; ?>>Cancelled</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <input type="submit" name="reset_btn" value="Reset" id="reset" class="btn btn-success btn-warning">&nbsp;&nbsp;<input type="submit" name="submit_btn" value="Search" class="btn btn-success btn-primary">
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php  if ($list_data == '1') { ?>
                                                    <div class="col-md-1" style="float: right;">
                                                        <div class="form-group">
                                                            <label style="width:28%; padding-top: 5px;"></label>
                                                            <div class="input-group">
                                                                <input type="submit" name="submit" value="pdf" class="submit_pdf">
                                                            </div>
                                                        </div>
                                                    </div>
<?php } ?>                                            </div>
                                        </form>
                                    </div>                                    
                                    <?php if (!empty($custom)) { ?>
                                        <div class="header_div">
                                            STATEMENT OF ACCOUNT AS AT <?php echo date('d-M-Y', strtotime($to_date)); ?>
                                        </div>
                                        <table id="user" class="table table-bordered table-striped" style="clear: both">
                                            <tbody>
                                                <tr>
                                                    <td style="width:35%;font-weight: bold;">Customer Name</td>
                                                    <td><?php echo (!empty($cus_details->name)) ? $cus_details->name : 'N/A'; ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="width:35%;font-weight: bold;">Address</td>
                                                    <td><?php echo (!empty($cus_details->address)) ? $cus_details->address : 'N/A'; ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="width:35%;font-weight: bold;">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width:35%;font-weight: bold;">Attention</td>
                                                    <td><?php echo (!empty($cus_details->contact_name)) ? $cus_details->contact_name : 'N/A'; ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="width:35%;font-weight: bold;">Phone Number</td>
                                                    <td><?php echo (!empty($cus_details->phone_no)) ? $cus_details->phone_no : 'N/A'; ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="width:35%;font-weight: bold;">Fax</td>
                                                    <td><?php echo (!empty($cus_details->fax)) ? $cus_details->fax : 'N/A'; ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    <?php } ?>
                                    <?php if ($list_data == '1') { ?>
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>
                                                    <th width="5%" data-class="expand">S.No</th>

                                                    <th width="10%" data-hide="phone"><i class="fa fa-fw fa-calendar text-muted hidden-md hidden-sm hidden-xs"></i> Inv Date</th>
                                                    <th width="15%" ><i class="fa fa-fw fa-fax text-muted hidden-md hidden-sm hidden-xs"></i> Inv No.</th>
                                                    <th width="15%" data-hide="phone,tablet"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Customer Ref.</th>
                                                    <th width="15%" data-hide="phone,tablet"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Debit Amount</th>
                                                    <th width="15%" data-hide="phone,tablet"><i class="fa fa-fw fa-flag-o text-muted hidden-md hidden-sm hidden-xs"></i>Credit Amount</th>
                                                    <th width="15%" data-hide="phone"><i class="fa fa-fw fa-dollar text-muted hidden-md hidden-sm hidden-xs"></i> Balance</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (!empty($invoices)) {
                                                    $to_amt = $pay_amt = $balance = $credit_tot = $balance_tot = 0;
                                                    foreach ($invoices as $key => $row) {
                                                        $to_amt += $row->total_amt;
                                                        $pay_amt = (!empty($row->pay_amt)) ? $row->pay_amt : 0;
                                                        $balance = $row->total_amt - $pay_amt;
                                                        $credit_tot += $pay_amt;
                                                        $balance_tot += $balance;
                                                        //print_r($row);
                                                        ?>
                                                        <tr>
                                                            <td><?php echo ++$key; ?></td>                                                        
                                                            <td><?php echo $row->so_date_str; ?></td>
                                                            <td><?php echo text($row->so_no); ?></td>
                                                            <td><?php echo text($row->your_reference); ?></td>
                                                            <td align="right"><?php echo number_format($row->total_amt, 2); ?></td>
                                                            <td align="right"><?php echo number_format($pay_amt, 2); ?></td>
                                                            <td align="right"><?php echo number_format($balance, 2); ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <tr>
                                                        <td colspan="7" align="center">No Record Found</td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                            <?php if (!empty($invoices)) { ?>
                                                <tfoot style="font-weight: bold;"><tr>
                                                        <td colspan="4" style="text-align: right;">Total (S$)</td>
                                                        <td align="right"><?php echo number_format($to_amt, 2); ?></td>
                                                        <td align="right"><?php echo number_format($credit_tot, 2); ?></td>
                                                        <td align="right"><?php echo number_format($balance_tot, 2); ?></td>

                                                    </tr></tfoot>
                                            <?php } ?>
                                        </table>

                                        <br />
                                        <?php
                                        $val =1;
                                         foreach ($paT as $key => $value) { if(!empty($value['amt'])){                                             
                                             $val =2;
                                            }
                                         }                                         
                                        if($val ==2){?>
                                            <div class="col-md-4"></div>
                                            <div class="col-md-4"></div>
                                            <div class="col-md-4">
                                                <table id="user" class="table table-bordered table-striped" style="clear: both;">
                                                    <tbody>
                                                        <tr>
                                                            <td style="width:50%;"><b>Credit Term</b></td>
                                                            <td style="width:50%;" align="right">Now Overdue</td>
                                                        </tr>
                                                        <?php foreach ($paT as $key => $value) { if(!empty($value['amt'])){?>
                                                            <tr>
                                                                <td style="width:50%;"><b><?php echo $value['title']; ?></b></td>
                                                                <td style="width:50%;" align="right">S$ <?php echo (!empty($value['amt'])) ? number_format($value['amt'], 2) : '0.00'; ?></td>
                                                            </tr>
                                                        <?php }} ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php }?><br>
                                        <table id="user" class="table table-bordered table-striped" style="clear: both">
                                            <tbody>
                                                <tr>
                                                    <td style="width:20%;">&nbsp;</td>
                                                    <td style="font-weight: bold;" align="center">Current</td>
                                                    <td style="font-weight: bold;" align="center">> 30 days</td>
                                                    <td style="font-weight: bold;" align="center">> 60 days</td>
                                                    <td style="font-weight: bold;" align="center">> 90 days</td>
                                                    <td style="font-weight: bold;" align="center">> 120 days</td>
                                                </tr>
                                                <tr>
                                                    <td style="width:20%;">Aging Balance</td>
                                                    <td style="font-weight: bold;" align="center"><?php echo number_format($curent_amt, 2); ?></td>
                                                    <td style="font-weight: bold;" align="center"><?php echo number_format($one, 2); ?></td>
                                                    <td style="font-weight: bold;" align="center"><?php echo number_format($two, 2); ?></td>
                                                    <td style="font-weight: bold;"align="center"><?php echo number_format($three, 2); ?></td>
                                                    <td style="font-weight: bold;" align="center"><?php echo number_format($four, 2); ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    <?php } ?>                                    
                                </div>                              

                                <!-- end widget content -->
                            </div>
                        </div> 
                    </div>
                </article>
                <!-- /.row -->
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
	$(function () {
		$('#search_date').daterangepicker({format: 'DD/MM/YYYY'});
		
		setTimeout(function() {
			$('#s2id_customer_name').css('width', '90%');
		}, 2000);
	});
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/customer.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/custom.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datepicker/daterangepicker.js"></script>
