<html>
    <head>
        <style>
            @page {
                size: portrait;
                header: html_header;
                footer: html_footerPageNo;
                margin-footer: 0px;
                margin-bottom: 60mm;
            }                       

            .barcode {
                padding: 1.5mm;
                margin: 0;
                vertical-align: top;
                color: #424242;
                width:200px;
            }
        </style>
    </head>
    <body>
    <htmlpagefooter name="footerPageNo" style="display:none">
        <table width="100%" style="vertical-align: bottom; font-family: calibri; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
            <tr>
                <td width="100%" align="center">Page {PAGENO}/{nbpg}</td>
            </tr>
        </table>
    </htmlpagefooter>

    <htmlpageheader name="header" style="display:block">
        <table width="100%" border="0" style="font-family: calibri;">
            <tr>
                <?php
                $comp_logo = (!empty($company->comp_logo) && file_exists(UPLOADS . $company->comp_logo) ) ? base_url() . UPLOADS . $company->comp_logo : 'assets/images/logo.png';
                ?>
                <td width="50%" style="vertical-align:top; padding: 0;">
                    <table width="100%">
                        <tr>
                            <td><img src="<?php echo $comp_logo; ?>"></td>
                        </tr>                       
                    </table>
                </td>
                <td width="20%" style="vertical-align:top; padding: 0;"></td>
                <td width="30%" style="vertical-align:top; padding: 0;">
                    <table width="100%">                   
                        <tr>
                            <td style="text-align: center;">GST No: <?php echo $company->comp_gst_no; ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">Co. Reg No.: <?php echo $company->comp_reg_no; ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </htmlpageheader>