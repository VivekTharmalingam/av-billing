<?php //print_r($from_date);exit;?>
<table border="0" cellpadding="3" cellspacing="0" style="font-family: calibri; font-size:14px; width: 1000px;">
    <?php echo $head;?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>
<table width="100%" border="1" style="font-family: calibri; border-collapse:collapse;" cellpadding="3">
	<thead>
	<tr>
		<th width="8%">S.NO</th>
		<th width="15%">Customer Name</th>
		<th width="15%">Customer Group</th>
		<th width="15%">Contact Name</th>
		<th width="15%">Phone No</th>
		<th width="12%">Email</th>
		<th width="25%">Address</th>
	</tr>
	</thead>
	<tbody>
	<?php if (count($customer)) {?>
	<?php foreach($customer as $key => $row) {?>                    
		<tr>
			<td><?php echo ++ $key;?></td>
			<td><?php echo strtoupper(text($row->name));?></td>
			<td><?php echo strtoupper(text($row->cust_group));?></td>
			<td><?php echo strtoupper(text($row->contact_name));?></td>
			<td><?php echo text($row->phone_no);?></td>
			<td><?php echo text($row->email);?></td>
			<td><?php echo strtoupper(nl2br($row->address));?></td>
		</tr>
	<?php }?>
	<?php }
	else {?>
		<tr>
			<td colspan="6" align="center">No Record Found</td>
		</tr>
	<?php }?>
	</tbody>
</table>