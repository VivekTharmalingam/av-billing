<!DOCTYPE html>
<html class="bg-myclr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo $title;?></title>
		<!--<link rel="shortcut icon" href="<?php echo BASE_URL; ?>assets/images/favicon/favicon.ico" type="image/x-icon">-->
        <?php
		if ((!empty($company->comp_favicon)) && (file_exists(FCPATH . DIR_FAVICON . $company->comp_favicon))) {
                $favicon_url = base_url() . DIR_FAVICON . $company->comp_favicon;
            }
            else {
                $favicon_url = base_url() . 'assets/images/favicon/favicon.ico';
            
            }
		?>
			<link rel="shortcut icon" href="<?php echo $favicon_url;?>" type="image/x-icon">
        <style type="text/css">
                    * {padding:0; margin:0;}
                        body {font-family: "Open Sans",Arial,Helvetica,Sans-Serif; font-size:14px;}
                        .login_div {margin:7% auto; width:370px; height:310px; border:5px solid #f4f4f4; border-radius:10px; box-shadow:0px 0px 10px 0px rgba(0,0,0,0.2); padding:2% 4%;}
                        .login_div h1 {text-align:center; margin-bottom:5%;}
                        .selectbox_div {float:left;width:100%;}
                        .selectbox_div select {position: relative; z-index: 1000; width:100%; height:35px; padding:5px; border-radius:5px; box-shadow:0px 0px 6px 0px rgba(0,0,0,0.2); border:1px solid #F4F4F4; font-size:14px;}
                        .submit_div {float:right; width:100%; margin-top:20px;}
                        .submit_div button {float:right; width:137px;  /*width:100px;*/ height:50px; font-size:14px;background: #E3E3E3; /* For browsers that do not support gradients */
                    background: -webkit-linear-gradient(#E3E3E3, #F7F7F7); /* For Safari 5.1 to 6.0 */
                    background: -o-linear-gradient(#E3E3E3, #F7F7F7); /* For Opera 11.1 to 12.0 */
                    background: -moz-linear-gradient(#E3E3E3, #F7F7F7); /* For Firefox 3.6 to 15 */
                    background: linear-gradient(#E3E3E3, #F7F7F7); /* Standard syntax */
                        border:1px solid #f4f4f4; border-radius:5px; position: relative; z-index: 1000; cursor:pointer;}
                        .submit_div button:hover {background: #E3E3E3; /* For browsers that do not support gradients */
                    background: -webkit-linear-gradient(#F7F7F7, #E3E3E3); /* For Safari 5.1 to 6.0 */
                    background: -o-linear-gradient(#F7F7F7, #E3E3E3); /* For Opera 11.1 to 12.0 */
                    background: -moz-linear-gradient(#F7F7F7, #E3E3E3); /* For Firefox 3.6 to 15 */
                    background: linear-gradient(#F7F7F7, #E3E3E3);}
                        .footer {width:100%; /*position:absolute;*/ bottom:0;}
                        .footer_img {float:left; width:100%; opacity:.5; margin-top:-130px;}
                        .footer_img img {background-size:contain; width:100%;}
                /*	.poweredby {float:left; width:100%; text-align:center;}*/
                        @media only screen and (max-width:900px){
                                .login_div {width:80%; margin:12% auto;}
                                .footer_img {margin-top:0px;}
                        }
                        @media only screen and (max-width:340px){  
                            .login_div h1 img {width: 220px;}
                        }
            
		    .error_container {
				color: #F00;
			}
		</style>
		
        <script src="<?php echo BASE_URL;?>assets/js/jquery.min.js"></script>
        <script type='text/javascript' src="<?php echo BASE_URL; ?>assets/js/libs/jquery.validate.min.js"></script>
        <script type="text/javascript">
		    $(document).ready(function(e) {
                $('form[name="company"]').validate({
					errorElement: "p",
					errorLabelContainer: '.error_container',
					rules: {
						branch_id: {
							required: true
						},
					},
					messages: {
						branch_id: {
							required: 'Please select Branch Name!'
						}
					}
				});
            });
		</script>
        
    </head>
    <body>
        <form action="<?php echo $form_action;?>" method="post" name="company">
        <div class="login_div">
            <h1>
			<?php
			if ((!empty($company->login_logo)) && (file_exists(FCPATH . DIR_COMPANY_LOGO . $company->login_logo))) {
                $image_url = base_url() . DIR_COMPANY_LOGO . $company->login_logo;
            }
            else {
                $image_url = base_url() . 'assets/images/logo.png';
               
            }
            ?>
			<img src="<?php echo $image_url;?>" style="width: 110px;" />
			<!--<img style="height:100px;" src="<?php echo BASE_URL ?>assets/images/login/logo.png" />-->
			</h1>
            <div class="selectbox_div">
                    <select name="branch_id" >
                        <option value="">Select Branch</option>
                        <?php foreach($branches as $key => $branch) {?>
                            <option value="<?php echo $branch->id;?>"><?php echo $branch->branch_name;?></option>
                        <?php }?>
                    </select>
            </div>
            <div class="error_container"></div>
            <div><a  style="color:#00A65A;width:100%; float: left; text-align: center; margin-top: 15px; text-decoration: none;" href="<?php echo base_url();?>login">Click here to login with different user?</a></div> 
            <div class="submit_div">
                <button type="submit" value="Login">Proceed&nbsp;to&nbsp;Sign&nbsp;in</button>
                <button type="reset" value="Login" style="margin-right:10px;" onClick="form_reset();">Reset</button>
            </div>
        </div>
        <!--<div class="footer">
            <div class="footer_img"><img src="<?php echo BASE_URL ?>assets/images/login/m_footer.png" /></div>
        </div>-->
        </form>     
    </body>
</html>