<html>
<head>
<link href="<?php echo base_url();?>/assets/css/plugin/barcode.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url();?>/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/assets/js/plugin/barcode/jquery-barcode.min.js"></script>
<script type="text/javascript">
      function generateBarcode(){
        var value = $('input[name="so_no"]').val();
        var btype = 'code39';
        var renderer = 'css';
        var settings = {
          output:renderer,
          bgColor: '#FFFFFF',
          color: '#424242',
          barWidth: '1',
          barHeight: '30',
          moduleSize: '5',
          posX: 10,
          posY: 20,
          addQuietZone: 1
        };
        //if ($("#rectangular").is(':checked') || $("#rectangular").attr('checked')){
          value = {code:value, rect: true};
        //}
        /*if (renderer == 'canvas'){
          clearCanvas();
          $("#barcodeTarget").hide();
          $("#canvasTarget").show().barcode(value, btype, settings);
        } else {*/
          $("#canvasTarget").hide();
          $("#barcodeTarget").html("").show().barcode(value, btype, settings);
		  $('#barcodeTarget').find('div:last').hide();
        //}
      }
          
      /*function showConfig1D(){
        $('.config .barcode1D').show();
        $('.config .barcode2D').hide();
      }
      
      function showConfig2D(){
        $('.config .barcode1D').hide();
        $('.config .barcode2D').show();
      }
      
      function clearCanvas(){
        var canvas = $('#canvasTarget').get(0);
        var ctx = canvas.getContext('2d');
        ctx.lineWidth = 1;
        ctx.lineCap = 'butt';
        ctx.fillStyle = '#FFFFFF';
        ctx.strokeStyle  = '#000000';
        ctx.clearRect (0, 0, canvas.width, canvas.height);
        ctx.strokeRect (0, 0, canvas.width, canvas.height);
      }*/
      
      $(function(){
        /*$('input[name=btype]').click(function(){
          if ($(this).attr('id') == 'datamatrix') showConfig2D(); else showConfig1D();
        });
        $('input[name=renderer]').click(function(){
          if ($(this).attr('id') == 'canvas') $('#miscCanvas').show(); else $('#miscCanvas').hide();
        });*/
        generateBarcode();
      });
  
    </script>
</head>
<body>
	
        <div style="width: 302px; border: 1px solid #bbb; padding: 10px; margin: auto; font-family: calibri; font-size: 18px; color: #000;">
            <div style="text-align:center;">
                <img src="<?php echo base_url(); ?>assets/images/thermal/acs1.png" style="max-width: 100%;" />
            </div>
			<?php
			$address = $branch->address;
			$tel_pos = strripos($address, 'FAX');
			$addr = trim(substr($address, 0, $tel_pos), ' ');
			$addr = trim($addr, '|');
			$telephone = substr($address, $tel_pos);
			$branch->address = $addr . "\n" . $telephone;
			?>
			
			<p style="text-align: center; margin: 20px 0px 10px 0px;"><?php echo strtoupper(nl2br($branch->address));?></p>
			<p style="margin: 10px 0px 0px 0px; text-align: center;">GST REG NO.: <?php echo strtoupper($company->comp_gst_no);?></p>
			<p style="margin: 0px 0px 10px 0px; text-align: center;">BUSINESS REG NO.: <?php echo strtoupper($company->comp_reg_no);?></p>
			
			<p style="margin: 0px 0px 10px 0px; border-bottom: 1px dashed #333; height: 20px;"></p>
            <h3 style="text-align: center; font-size: 18px; margin:0;">TAX INVOICE</h3>
			<p style="margin: 10px 0px 15px 0px; border-bottom: 1px dashed #333; height: 1px;"></p>
			<input type="hidden" name="so_no" value="<?php echo strtoupper($sales_order->so_no); ?>" />
			<table style="width: 100%; border-collapse: collapse;" cellpadding="0">
                <tr>
                    <td width="50%">INVOICE NUMBER</td>
                    <td width="5%"> : </td>
                    <td width="45%"><b><?php echo strtoupper($sales_order->so_no); ?></b></td>
                </tr>
				<tr>
                    <td>INVOICE DATE</td>
                    <td> : </td>
                    <td><?php echo (empty($sales_order->leave_date)) ? text_date($sales_order->so_date, '', 'd-m-Y') : ''; ?></td>
                </tr>
                <!--<tr>
                    <td>ISSUED BY</td>
                    <td> : </td>
                    <td><?php echo strtoupper(text($sales_order->uname, '--')); ?></td>
                </tr>-->
                <tr>
                    <td valign="top">SOLD TO</td>
                    <td valign="top"> : </td>
                    <td  style="vertical-align: top;"><?php echo strtoupper(text($sales_order->name, '--')); ?></td>
                </tr>
            </table>
			
            <table style="width: 100%; border-collapse: collapse;" cellpadding="0">
                <tr>
					<td colspan="4"><p style="margin: 0px 0px 10px 0px; border-bottom: 1px dashed #333; height: 25px;"></p></td>
				</tr>
                <tr class="head">
                    <td width="55%" style="padding-right: 5px;">CODE/DESC</th>
                    <td width="10%" align="right" style="padding: 0 5px;">QTY</td>
                    <td width="15%" align="right" style="padding: 0 5px;">PRICE</td>
                    <td width="20%" align="right" style="padding: 0 5px;">TOTAL</td>
                </tr>
				
				<tr>
					<td colspan="4"><p style="margin: 0px 0px 10px 0px; border-bottom: 1px dashed #333; height: 15px;"></p></td>
				</tr>
				
                <?php foreach ($so_items as $key => $item) {?>
					<?php $pc = ($item->quantity > 1) ? ' PCS' : ' PC'; ?>
                    <tr>
                        <td colspan="4" style="padding-right: 5px;">
							<?php echo strtoupper($item->pdt_code); ?>
							<?php echo (!empty($item->pdt_code) && (!empty($item->product_description))) ? ' / ' : ' ';?>
							<?php echo strtoupper(nl2br($item->product_description)); ?>
						</td>
					</tr>
                    <tr>
                        <td colspan="2" style="text-align: right; padding: 0 5px;"><?php echo $item->quantity . $pc; ?></td>
                        <td align="right" style="padding: 0 5px;"><?php echo text_amount($item->price); ?></td>
                        <td align="right" style="padding: 0 5px;"><?php echo text_amount($item->total); ?></td>
                    </tr>
					<tr>
						<td colspan="4" height="10"></td>
					</tr>
                <?php } ?>
				<tr>
					<td colspan="4">
						<p style="margin: 20px 0 20px 0px; border-bottom: 1px dashed #333; height: 2px;"></p>
					</td>
				</tr>
			</table>
			<table style="width: 100%; border-collapse: collapse;" cellpadding="0">
                <?php 
                if($sales_order->gst == 2 || $sales_order->discount > 0){?>
                    <tr>
                        <td align="left" width="50%">SUB-TOTAL</td>
						<td width="5%"> : </td>
                        <td align="right" width="45%">$ <?php echo text_amount($sales_order->sub_total); ?></td>
                    </tr>
                    <?php if ($sales_order->discount > 0) {?>
                        <tr>
							<td align="left">DISCOUNT <?php echo ($sales_order->discount_type == 1 && $sales_order->discount > 0) ? ' @ (' . $sales_order->discount . ' %'. ' )'  : '';?></td>
							<td> : </td>
                            <td align="right">$ <?php echo text_amount($sales_order->discount_amount);?></td>
                        </tr>
                    <?php }?>
                    
                    <?php if ($sales_order->gst == 2) { ?>
                        <tr>
							<td align="left">GST (<?php echo $sales_order->gst_percentage; ?> %)</td>
							<td> : </td>
                            <td align="right">$ <?php echo text_amount($sales_order->gst_amount); ?></td>
                        </tr>
                    <?php }?>
                    <tr>
						<td align="left" width="50%">TOTAL</td>
						<td width="5%"> : </td>
                        <td align="right" width="45%"><b>$ <?php echo text_amount($sales_order->total_amt); ?></b></td>
                    </tr>
                <?php }
                else {?>
                    <tr>
						<td align="left" width="50%">TOTAL</td>
						<td width="5%"> : </td>
                        <td align="right" width="45%"><b>$ <?php echo text_amount($sales_order->total_amt); ?></b></td>
                    </tr>
                <?php }?>
				<?php if ($sales_order->gst == 1) { ?>
                    <tr>
                        <td align="right" colspan="3">[<?php echo 'Inclusive of GST ' . $sales_order->gst_percentage . ' % : $ ' . text_amount($sales_order->gst_amount);?>]
                        </td>
                    </tr>
                <?php }?>
				<?php if (!empty($sales_order->payment_type)) {?>
					<tr>
						<td colspan="3" height="5"></td>
					</tr>
                    <tr>
                        <td style="vertical-align: top;">PAYMENT MODE</td>
						<td style="vertical-align: top;"> : </td>
                        <td align="right" style="vertical-align: top;"><?php echo strtoupper($sales_order->payment_type); ?></td>
                    </tr>
                <?php }?>
				<tr>
					<td colspan="3"><p style="margin-bottom: 10px; border-bottom: 1px dashed #333; height: 20px;"></p></td>
				</tr>
				<tr>
					<th colspan="3" style="font-size: 16px;">THANK YOU FOR SHOPPING AT REFULGENCE</th>
				</tr>
				<tr>
					<td colspan="3">
						<img src="<?php echo base_url(); ?>assets/images/thermal/bottom_logo.jpg" style="max-width: 100%;" />
					</td>
				</tr>
				<tr>
					<th colspan="3">www.refulgenceinc.com</th>
				</tr>
				<tr>
					<td colspan="3" height="15"></td>
				</tr>
            </table>
			
			<div id="barcodeTarget" class="barcodeTarget" style="width: 100%;margin: auto;" height="150"></div>
<canvas id="canvasTarget" style="width: 100%;margin: auto;" height="150"></canvas>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
        </div>
</body>
</html>