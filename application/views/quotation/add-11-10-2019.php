<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) {
                    ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>

        <form class="add_form" id="sales_order" action="<?php echo $form_action; ?>" method="post" data-form_submit="0">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <a href="<?php echo $list_link; ?>" class="large_icon_des" style=" color: white;">
                            <button type="button" class="btn btn-primary btn-circle btn-xl temp_color ">
                                <i class="glyphicon glyphicon-list"></i>
                            </button>
                        </a>
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Quotation Add </h2>
                            </header>

                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" id="sales_form">
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                            <!--<section>
                                                <label class="label">Customer Type?</label>
                                                <div class="inline-group">
                                                    <label class="radio">
                                                        <input type="radio" class="cus_type" name="cus_type" value="1" checked="" /><i></i>Regular Customer
                                                    </label>
                                                    <label class="radio">
                                                        <input type="radio" class="cus_type" name="cus_type" value="2" /><i></i>Cash Customer
                                                    </label>
                                                </div>
                                            </section>-->
                                            <div class="row">
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label" style="display: inline-block; width: 100%;">Quotation To<span style="color: red;">*</span>
															<span class="new-window-popup btn btn-primary pull-right" style="margin: 0 0 2px 0;" data-cmd="customer" data-title="Customer Add" data-iframe-src="<?php echo $add_customer_link;?>">
															<i class="fa fa-plus-square-o" style="font-size: 16px;"> </i> Add Customer
															</span>
                                                            <span class="new-window-popup btn btn-primary pull-right" style="margin: 0 10px 2px 0;" data-cmd="customer" data-title="Customer Add" data-iframe-src="<?php echo $list_customer_link;?>">
															<i class="fa fa-indent" style="font-size: 16px;"> </i> Customers
															</span>
														</label>
                                                        <label class="select">
                                                            <select name="customer_name" id="customer_name" class="select2" tabindex="1" autofocus>
                                                                <!--<option value="">Select Customer name</option>-->
                                                                <option value="cash">Cash</option>
                                                                <?php foreach ($customers as $key => $customer) { ?> 
																	<option value="<?php echo $customer->id; ?>"><?php echo $customer->name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </label>
                                                    </section>

                                                    <div class="reg_customer_add" style="display: none;">
                                                        <section class="cust_address"></section>

                                                        <section>
                                                            <label class="label">Deliver To</label>
                                                            <label class="select">
                                                                <select name="ship_to" id="ship_to" class="select2">
                                                                    <option value="">Use different Location</option>
                                                                </select>
                                                            </label>
                                                        </section>

                                                        <section class="ship_to_address"></section>

                                                        <div class="new_shipping_address">
                                                            <section>
                                                                <label class="label">Location Name</label>
                                                                <label class="input">
                                                                    <input type="text" name="location_name" class="form-control" data-placeholder="Mayo Street" />
                                                                </label>
                                                            </section>
                                                            <section>
                                                                <label class="label">Address</label>
                                                                <label class="input">
                                                                    <input type="text" name="address" class="form-control" data-placeholder="29 Mayo Street" />
                                                                </label>
                                                            </section>
                                                        </div>
                                                    </div>

                                                    <!-- For cash customer start -->
                                                    <div class="cash_customer_name">
                                                        <section>
                                                            <label class="label">Customer Name</label>
                                                            <label class="input">
                                                                <input type="text" name="cash_customer_name" id="cash_customer_name" class="form-control" data-placeholder="Cash" value="Cash" />
                                                            </label>
                                                        </section>
                                                        <section>
                                                            <p class="alert alert-info not-dismissable"> Address: CASH CUSTOMER<?php //echo $address->address; ?></p>
                                                        </section>
                                                    </div>
                                                    <!-- For cash customer end -->

                                                    <section>
                                                        <label class="label">Vendor Name</label>
                                                        <label class="input">
                                                            <input type="text" name="your_reference" id="your_reference" class="form-control" data-placeholder="Customer Reference" data-focused=false; />
                                                        </label>
                                                    </section>
                                                </section>

                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Quotation No #</label>
                                                        <label class="input">
                                                            <input type="text" name="so_no" class="form-control" tabindex="-1" data-placeholder="" value="<?php echo $so_no;?>" readonly />
                                                        </label>
                                                    </section>

                                                    <section>
                                                        <label class="label">Date<span style="color: red;">*</span>
															<!--<label class="checkbox pull-right"><input type="checkbox" name="leave_date" value="1" /><i></i> Leave Blank</label>-->
														</label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
                                                            <input type="text" name="so_date" id="so_date" class="form-control bootstrap-datepicker-comncls" tabindex="-1" data-placeholder="DD/MM/YYYY" value="<?php echo date('d/m/Y'); ?>" />
                                                        </label>
                                                    </section>

                                                    <!--<section>
                                                        <label class="label">Payment Terms</label>
                                                        <label class="select">
															<select name="payment_terms" id="payment_terms" class="select2">
																<option value="">Select Payment Term</option>
																<?php //foreach($payment_terms as $key => $value) {?>
																	<option value="<?php echo $value;?>"><?php echo $value;?></option>
																<?php //}?>
															</select>
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Remarks</label>
                                                        <label class="textarea">
                                                            <textarea rows="3" class="show_txt_count" name="remarks" id="remarks" data-placeholder="Remarks" maxlength="250"></textarea>
                                                        </label>
                                                    </section>-->
                                                </section>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <legend style="font-weight: bold; font-size: 15px; color: #A90329;">Items Details</legend>
                                            <div class="row product_search">
                                                <section class="col col-5">
                                                    <div class="input hidden-mobile">
                                                        <input name="search_product" class="form-control search_product lowercase" type="text" data-placeholder="Scan / Search Item by code or description" id="search_product" tabindex="-1" />
                                                    </div>
                                                </section>
                                            </div>
                                            <div class="row material-container product_details">
												<div class="col-xs-12">
													<section class="col">
                                                        <span class="new-window-popup btn btn-primary" data-iframe-src="<?php echo $list_item_link;?>" data-cmd="item" data-title="Item Add">
															<i class="fa fa-indent" style="font-size: 16px;"> </i> Items
														</span>
														<span class="new-window-popup btn btn-primary" data-iframe-src="<?php echo $add_item_link;?>" data-cmd="item" data-title="Item Add">
															<i class="fa fa-plus-square-o" style="font-size: 16px;"> </i> Add Item
														</span>
													</section>
												</div>
												
                                                <div class="items_head col-xs-12 clr">
                                                    <section class="col col-2">
                                                        <label class="label" style="text-align: center;">
															<b>Item Code</b>
														</label>
                                                    </section>
                                                    <section class="col col-4">
                                                        <label class="label" style="text-align: center;"><b>Brand - Description</b></label>
                                                    </section>
                                                    <section class="col col-1">
                                                        <label class="label" style="text-align: center;"><b>Qty</b></label>
                                                    </section>
                                                    <?php $unit_cost = (in_array(7, $permission)); ?>
                                                    <section class="col col-<?php echo ($unit_cost) ? '1' : '2'; ?>">
                                                        <label class="label" style="text-align: center;"><b>Price</b></label>
                                                    </section>

                                                    <?php if ($unit_cost) { ?>
                                                        <section class="col col-1">
                                                            <label class="label" style="text-align: center;"><b>Cost</b></label>
                                                        </section>
                                                    <?php } ?>

                                                    <section class="col col-2">
                                                        <label class="label" style="text-align: center;"><b>Amount</b></label>
                                                    </section>
                                                </div>

                                                <div class="items col-xs-12 clr">
                                                    <section class="col col-2">
                                                        <label class="input">
                                                            <input type="hidden" name="category_id[0]" class="category_id" value="" />
                                                            <input type="hidden" name="product_id[0]" class="product_id" value="" />
                                                            <input type="text" name="product_code[0]" class="product_code" data-value="" data-placeholder="ITM_TV001" />
                                                        </label>
                                                        <span class="product_code_label">Selected Item code already exist!</span>
                                                    </section>

                                                    <section class="col col-4">
                                                        <label class="input">
                                                            <input type="text" name="product_description[0]" class="product_description" data-value="" data-placeholder="Sony Sony Braviya TV" />
                                                        </label>
                                                        <span class="error_description">Selected Item description already exist!</span>
                                                    </section>

                                                    <section class="col col-1">
                                                        <label class="input">
                                                            <input type="text" name="quantity[0]" class="quantity float_value" style="padding-left: 2px; padding-right: 2px; text-align: right;" />
                                                        </label>
                                                        <span class="unit_label"></span>
                                                    </section>

                                                    <section class="col col-<?php echo ($unit_cost) ? '1' : '2'; ?>" style="padding-left: 5px; padding-right: 5px;">
                                                        <label class="input">
                                                            <!--<i class="icon-prepend fa fa-dollar"></i>-->
                                                            <input type="text" name="price[0]" class="price float_value" style="padding-left: 2px; padding-right: 2px;" data-placeholder="0.00" />
                                                        </label>
                                                    </section>

                                                    <section class="col col-1" style="padding-left: 5px; padding-right: 5px; display: <?php echo ($unit_cost) ? 'block' : 'none'; ?>;">
                                                        <label class="input" style="margin-top: 1px;">
                                                            <!--<i class="icon-prepend fa fa-dollar"></i>-->
                                                            <input type="text" name="cost[0]" class="cost float_value" style="padding-left: 2px; padding-right: 2px;" data-placeholder="0.00" />
                                                        </label>
                                                    </section>

                                                    <section class="col col-2">
                                                        <label class="input">
                                                            <i class="icon-prepend fa fa-dollar"></i>
                                                            <input type="text" name="amount[0]" class="amount float_value" style="padding-left: 2px; padding-right: 2px;" data-placeholder="0.00" readonly="" />
                                                        </label>
                                                    </section>

                                                    <section class="col col-1">
                                                        <label class="input">
                                                            <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                        </label>
                                                    </section>
                                                </div>
                                            </div>

                                            <div class="row" style="margin-top: 15px;">
                                                <section class="col col-6">
                                                </section>
                                                <section class="col col-3">
                                                    <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Sub Total</b></label>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="sub_total" class="sub_total" readonly="" data-placeholder="0.00" />
                                                    </label>
                                                </section>
                                                <section class="col col-1">
                                                </section>
                                            </div>

                                            <div class="row">
                                                <section class="col col-6">
                                                </section>
                                                <section class="col col-3">
                                                    <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Discount</b></label>
                                                </section>
                                                <section class="col col-2">
                                                    <!--<label class="input">
                                                        <i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="sub_total" class="sub_total" readonly="" data-placeholder="0.00" />
                                                    </label>-->

                                                    <div class="input-group">
                                                        <input type="text" name="discount" class="form-control discount float_value" value="0.00" data-placeholder="0.00" />
                                                        <span class="input-group-addon">
                                                            <span class="onoffswitch">
                                                                <input type="checkbox" name="discount_type" class="onoffswitch-checkbox discount_type" id="discount_type-0" value="1" checked />
                                                                <label class="onoffswitch-label" for="discount_type-0"> 
                                                                    <span class="onoffswitch-inner" data-swchon-text="%" data-swchoff-text="S$"></span> 
                                                                    <span class="onoffswitch-switch"></span> 
                                                                </label> 
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <input type="hidden" name="discount_amt" class="discount_amt" />
                                                    <span class="discount_container">
                                                        <span class="currency">S$</span>
                                                        <span class="discount_amount">0.00</span>
                                                    </span>
                                                </section>
                                                <section class="col col-1">
                                                </section>
                                            </div>

                                            <!--<div class="row gst_details">
                                                <section class="col col-4">
                                                </section>
                                                <section class="col col-2">&nbsp;</section>
                                                <section class="col col-3">
                                                    <section>
                                                        <label class="label">&nbsp;</label>
                                                        <div class="inline-group">
                                                            <label class="radio">
                                                                <input type="radio" class="gst_type" name="gst_type" value="1" <?php echo (empty($company->gst_percent)) ? 'checked' : ''; ?> /><i></i>Include GST
                                                            </label>
                                                            <label class="radio">
                                                                <input type="radio" class="gst_type" name="gst_type" value="2" <?php echo (!empty($company->gst_percent)) ? 'checked' : ''; ?> /><i></i>Exclude GST
                                                            </label>
                                                        </div>
                                                    </section>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="label">&nbsp;</label>
                                                    <label class="input change_perc_disamt"><i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="gst_amount" class="gst_amount float_value" data-placeholder="0.00" readonly="" />
                                                        <input type="hidden" value="<?php echo $company->gst_percent; ?>" name="hidden_gst" id="hidden_gst" />
                                                    </label>
                                                    <span class="gst_val" style="display: <?php echo (!empty($company->gst_percent)) ? 'block' : 'none'; ?>">GST @ <span class="gst_percentage"><?php echo $company->gst_percent; ?></span> %</span>
                                                </section>
                                            </div>-->

                                            <div class="row">
                                                <section class="col col-6">
                                                </section>
                                                <section class="col col-3">
                                                    <label class="label" style="color: #A90329; padding-top: 5px; text-align: right;"><b>Total Amount</b></label>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-dollar"></i>
                                                        <input type="text" name="total_amt" class="total_amt" readonly="" data-placeholder="0.00" />
                                                    </label>
                                                </section>
                                                <section class="col col-1">
                                                </section>
                                            </div>
                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary" >Submit</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>
                                    <div class="smart-form" id="payment_form" style="display: none;">
                                        <fieldset>
                                            <div class="row">
                                                <section class="col col-md-6">
                                                    <section>
                                                        <label class="label">Total Amount</label>
                                                    </section>
                                                </section>
                                                <section class="col col-md-6">
                                                    <section>
                                                        <label class="input">S$ <span class="payment_amount">0.00</span></label>
                                                    </section>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-md-6">
                                                    <section>
                                                        <label class="label">Payment Type</label>
                                                    </section>
                                                </section>
                                                <section class="col col-md-6">
                                                    <section>
                                                        <label class="select">
                                                            <select name="payment_type" class="payment_type select2" id="payment_type">
                                                                <option value="">Select Payment Type</option>
                                                                <?php
                                                                foreach ($payment_types as $key => $value) {
                                                                    echo '<option value="' . $value->id . '" data-is_cheque="' . $value->is_cheque . '" >' . $value->type_name . '</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                    <div class="cheque_details">
                                                        <section>
                                                            <label class="label">Bank Name<span style="color:red">*</span></label>
                                                            <label class="input">
                                                                <input type="text" class="form-control" name="cheque_bank_name" data-placeholder="SBI" maxlength="100" />
                                                            </label>
                                                        </section>
                                                        <section>
                                                            <label class="label">Cheque No<span style="color:red">*</span></label>
                                                            <label class="input">
                                                                <input type="text" class="form-control" name="cheque_acc_no" data-placeholder="Cheque No" maxlength="100" />
                                                            </label>
                                                        </section>
                                                        <section>
                                                            <label class="label">Cheque Date<span style="color:red">*</span></label>
                                                            <label class="input">
                                                                <input type="text" class="bootstrap-datepicker-comncls" name="cheque_date" data-placeholder="01/01/2016" maxlength="100" />
                                                            </label>
                                                        </section>
                                                    </div>
                                                    <!--<div class="dd_details">
                                                        <section>
                                                            <label class="label">DD No<span style="color:red">*</span></label>
                                                            <label class="input">
                                                                <input type="text" class="form-control" name="dd_no"  data-placeholder="DD No" maxlength="100" />
                                                            </label>
                                                        </section>
                                                        <section class="dd_details">
                                                            <label class="label">DD Date<span style="color:red">*</span></label>
                                                            <label class="input">
                                                                <input type="text" class="bootstrap-datepicker-comncls" name="dd_date"  data-placeholder="DD Date" maxlength="100" />
                                                            </label>
                                                        </section>
                                                    </div>-->
                                                </section>
                                            </div>
                                        </fieldset>
                                        <footer>
                                            <button type="button" class="btn btn-default" onclick="show_sales_form();">Back</button>
                                            <button type="submit" class="btn btn-success" id="pay_now" name="payment" value="pay_now" >Pay Now</button>
                                            <button type="submit" class="btn btn-warning" id="skip_payment" name="payment" value="skip_payment" onclick="disable_payment();">Skip Payment</button>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                    <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript">
	var payment_terms = <?php echo json_encode($payment_terms, JSON_UNESCAPED_SLASHES);?>;
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/quotation.js"></script>