<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>invoice/" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="List"><i class="glyphicon glyphicon-list"></i></button></a>
                    <a href="<?php echo $invoice_link; ?>" class="large_icon_des" style=" color: white;margin-right: 80px;" ><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "  title="Download"><i class="glyphicon glyphicon-save"></i></button></a>
                    <!-- <a href="<?php echo $redirect_link; ?>" class="large_icon_des" style=" color: white;margin-right: 80px;" ><button type="button" name="submit" value="pdf" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-save"></i></button></a> -->     
<!--<a target="_blank" href="<?php echo base_url(); ?>sales_orders/pdf/<?php echo $sales_order->id; ?>" class="large_icon_des" style=" color: white;padding-right: 80px;" ><button type="button" class="btn btn-primary btn-circle btn-xl temp_color " title="Download"><i class="glyphicon glyphicon-save"></i></button></a>-->

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-file-text-o"></i> </span>
                            <h2>Quotation View</h2>
                        </header>

                        <!-- widget div-->
                        <div role="content">
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body">
                                <!--<span id="watermark">Malar ERP.</span>-->
                                <table id="user" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Quotation Number</td>
                                            <td ><?php echo $sales_order->quo_no; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Quotation Date</td>
                                            <td ><?php echo text_date($sales_order->so_date); ?></td>
                                        </tr>
										<tr>
                                            <td style="width:35%;font-weight: bold;">Leave Blank</td>
                                            <td ><?php echo (empty($sales_order->leave_date)) ? 'No' : 'Yes'; ?></td>
                                        </tr>
                                        <!--<tr>
                                            <td style="width:35%;font-weight: bold;">Customer Type</td>
                                            <td ><?php echo ($sales_order->so_type == '1') ? 'Regular Customer' : 'Cash Customer'; ?></td>
                                        </tr>-->
                                        <tr>
                                            <td style="font-weight: bold;">Quotation To</td>
                                            <td ><?php echo $sales_order->name; ?></td>
                                        </tr>
                                        <?php if ($sales_order->so_type == '1') { ?>
                                            <tr>
                                                <td style="font-weight: bold;">Deliver To</td>
                                                <td >
                                                    <!--Location Name:
                                                    <?php echo $shipping_address->location_name; ?>
                                                    <br />-->
                                                    Address: <?php echo text(nl2br($shipping_address->address)); ?>
                                                </td>
                                            </tr>
                                        <?php } else { ?>
                                            <tr>
                                                <td style="font-weight: bold;">Deliver To</td>
                                                <td ><?php echo (!empty($address->address))?$address->address:'N/A'; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Customer Reference</td>
                                            <td ><?php echo nl2br(text($sales_order->your_reference)); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Payment Terms</td>
                                            <td ><?php echo nl2br(text($sales_order->payment_terms)); ?></td>
                                        </tr>
										
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Remarks</td>
                                            <td ><?php echo nl2br(text($sales_order->remarks)); ?></td>
                                        </tr>
                                        <!--<tr>
                                            <td style="width:35%;font-weight: bold;">Status</td>
                                            <td ><?php echo $sales_order->status_str; ?></td>
                                        </tr>-->
                                        
                                    </tbody>
                                </table>

                                <div class="text-left">
                                    <span class="onoffswitch-title">
                                        <h3><b style="color: #8e233b;">Items Details </b></h3>
                                    </span>
                                </div>
                                <div id="widget-tab-1" class="table-responsive">
                                    <table id="dt_basic_2" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
                                        <thead>
                                            <tr>
                                                <th data-class="expand" style="font-weight: bold; text-align:left;">S.No</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:left;">Item Code</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:left;">Description</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Quantity</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:right;">Price (S$)</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:right;" style="border-right: 1px solid #ddd;">Amount (S$)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($sales_order_items as $key => $item) { ?>
                                                <tr>
                                                    <td><?php echo ++$key; ?></td>
                                                    <td><?php echo $item->pdt_code; ?></td>
                                                    <td><?php echo nl2br($item->product_description); ?></td>
                                                    <td align="center"><?php echo $item->quantity; ?></td>
                                                    <td align="right"><?php echo number_format($item->price, 2); ?></td>
                                                    <td align="right" style="border-right: 1px solid #ddd;"><?php echo number_format($item->total, 2); ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="8">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <section class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                            <span style="float: right;">
                                                                <label style="color: #A90329;"><b>Sub Total</b></label>
                                                            </span>
                                                        </section>
                                                        <section class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                            <span style="float: right; margin-right: -25px;"><b>S$&nbsp;<?php echo number_format($sales_order->sub_total, 2); ?></b></span>
                                                        </section> 
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="8">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <section class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                            <span style="float: right;">
                                                                <label><b>Discount</b></label>
                                                            </span>
                                                        </section>
                                                        <section class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                            <span style="float: right; margin-right: -25px;"><b>S$&nbsp;<?php echo ($sales_order->discount_type == 1 && $sales_order->discount > 0) ? number_format($sales_order->discount_amount, 2) . ' (' . $sales_order->discount . ' %' . ' )' : number_format($sales_order->discount_amount, 2); ?></b></span>
                                                        </section>
                                                    </div>
                                                </td>
                                            </tr>
											<?php if ($sales_order->gst == 2) {?>
                                            <tr>
                                                <td colspan="8">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <section class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                            <span style="float: right;">
                                                                <label>
																	<b>ADD GST <?php echo $sales_order->gst_percentage . ' %';?></b>
																</label>
                                                            </span>
                                                        </section>
                                                        <section class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                            <span style="float: right; margin-right: -25px;">
																<b>S$&nbsp; <?php echo number_format($sales_order->gst_amount, 2); ?></b>
															</span>
                                                        </section>
                                                    </div>
                                                </td>
                                            </tr>
											<?php }?>
                                            <tr>
                                                <td colspan="8">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <section class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                            <span style="float: right;">
                                                                <label style="color: #A90329;"><b>Total Amount</b></label>
                                                            </span>
                                                        </section>
                                                        <section class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                            <span style="float: right; margin-right: -25px;"><b>S$&nbsp;<?php echo number_format($sales_order->total_amt, 2); ?></b></span>
                                                        </section>
                                                    </div>
                                                </td>
                                            </tr>
											<?php if ($sales_order->gst == 1) {?>
                                            <tr>
                                                <td colspan="8" align="right">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        [<?php echo '<b style="font-size: 12px;">Inclusive of GST '.$sales_order->gst_percentage.' %</b> : $ ' . number_format($sales_order->gst_amount, 2);?>]
                                                    </div>
                                                </td>
                                            </tr>
											<?php }?>
											
                                        </tfoot>
                                    </table>
                                    <br />
                                    <br />
                                </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->

<iframe id="thermal_content" src="<?php echo $thermal_src;?>" style="display: none;"></iframe>
<script type="text/javascript">
    function printResults(id,browser_txt) {   
        var w = window.open();
		var content = '<html>';
		content += '<body>';
		content += $('#thermal_content').contents().find('body').html();
		content += '</body>';
		content += '</html>';
        w.document.write(content);  //only part of the page to print, using jquery
        w.document.close(); //this seems to be the thing doing the trick
        w.focus();
        w.print();
        w.close();
        $('#myModal').modal('hide');
    }    
</script>
