    <table width="100%" border="1" style="border-collapse:collapse; font-family: calibri; border: 1px solid #777;" cellpadding="5">
        <thead>
            <tr>
                <th width="16%" style="background:#EBEBEB; font-size: 15px;">Item Code</th>
                <th width="49%" style="background:#EBEBEB; font-size: 15px;">Description</th>
                <th width="7%" style="background:#EBEBEB; font-size: 15px;">Qty</th>                    
                <th width="14%" style="background:#EBEBEB; font-size: 15px;">Unit Price ($)</th>
                <th width="14%" style="background:#EBEBEB; font-size: 15px;">Amount ($)</th>
            </tr>
        </thead>
		<tbody>
		<?php $height = 410; ?>
        <?php foreach ($so_items as $key => $item) {?>
            <tr style="vertical-align:top;">
                <td style="border-top: 0; border-bottom: none; font-size: 14px;"><?php echo strtoupper($item->pdt_code); ?></td>
                <td style="border-top: 0;border-bottom: none; font-size: 14px;"><?php echo nl2br(strtoupper($item->product_description)); ?></td>
                <td align="center" style="border-top: 0;border-bottom: none; font-size: 14px;"><?php echo $item->quantity; ?></td>
                <td align="right" style="border-top: 0;border-bottom: none; font-size: 14px;"><?php echo number_format($item->price, 2); ?></td>
                <td align="right" style="border-top: 0;border-bottom: none; font-size: 14px;"><?php echo number_format($item->total, 2); ?></td>
            </tr>
            <?php $height -= 55; ?>
        <?php } ?>
        <?php $height -= ($sales_order->discount > 0) ? 30 : 0; ?>
		<?php $height -= ($sales_order->gst_amount > 0) ? 30 : 0; ?>
        <tr>
            <td height="<?php echo $height; ?>" style="border-top: 0;"></td>
            <td style="border-top: 0;"></td>
            <td style="border-top: 0;"></td>
            <td style="border-top: 0;"></td>
            <td style="border-top: 0;"></td>
        </tr>
    <!--</table>-->

    <?php
    //$row_span = ($sales_order->gst_amount > 0) ? 3 : 2;
    $row_span = 2;
    if ($sales_order->gst == 2 && $sales_order->discount > 0)
        $row_span = 4;
    else if ($sales_order->gst == 2 || $sales_order->discount > 0)
        $row_span = 3;
    ?>
    <!--<table width="100%" style="border-collapse:collapse; border-top:none;" cellpadding="5">-->
        <tr>
            <td align="left" style="vertical-align: top; border-left: 1px solid #777; border-bottom: 1px solid #777; border-right: 1px solid #777; padding-right: 10px; font-size: 14px;" colspan="3">
				
<?php if (!empty($sales_order->remarks)) {?>
	<b style="font-size: 15px;">Remarks</b>
	<div>
		<?php echo strtoupper(nl2br($sales_order->remarks));?>
	</div>
<?php }?>
				<!--<br />
				<table style="font-size: 11px;" cellpadding="0" cellspacing="0">
					<tr>
						<td border="0" >1.</td>
						<td border="0"  style="padding-left: 2px;">Goods sold are not returnable.</td>
					</tr>
					<tr>
						<td border="0"  style="vertical-align: top;">2.</td>
						<td border="0"  style="padding-left: 2px;">Items without <b>PSB</b> approval and Telecommunications Equipment without <b>IDA</b> approval are sold for export only. Not approved for local use.</td>
					</tr>
					<tr>
						<td border="0" >3.</td>
						<td border="0"  style="padding-left: 2px;">All products come with 1 year local warranty unless otherwise stated.</td>
					</tr>
					<tr>
						<td border="0" >4.</td>
						<td border="0"  style="padding-left: 2px;">All cheques to be made payable to "Lucky Store"</td>
					</tr>
				</table>-->
            </td>
			<td colspan="2" style="vertical-align: top; padding: 0;">
				<table style="font-size: 12px; width: 100%;" cellpadding="0" cellspacing="0">
					<tr>
						<th style="text-align: right; height: 30px; width: 109px; border-left: none; border-bottom: 1px solid #777; border-right: 1px solid #777; padding-right: 10px; font-size: 15px;">Sub Total</th>
						<td align="right" style="width: 108px; border-right: none; border-bottom: 1px solid #777; padding-right: 5px; font-size: 14px;"><?php echo number_format($sales_order->sub_total, 2); ?></td>
					</tr>
					<?php if ($sales_order->discount > 0) { ?>
						<tr>
							<th style="height: 30px; border-left: none; border-bottom: 1px solid #777; border-right: 1px solid #777; padding-right: 0px; padding-left: 0px; font-size: 15px;">Discount <span style="font-size: 12px;"><?php echo ($sales_order->discount_type == 1 && $sales_order->discount > 0) ? $sales_order->discount . ' %' : ''; ?></span></th>
							<td align="right" style="border-bottom: 1px solid #777;border-right: none; padding-right: 5px; font-size: 14px;"><?php echo number_format($sales_order->discount_amount, 2); ?></td>
						</tr>
					<?php } ?>
					
					<?php if ($sales_order->gst == 2) { ?>
						<tr>
							<th align="right" style="height: 30px; border-left: none; border-bottom: 1px solid #777; border-right: 1px solid #777; padding-right: 10px; font-size: 15px;">ADD GST <?php echo $sales_order->gst_percentage; ?> %</th>
							<td align="right" style="border-right: none; border-bottom: 1px solid #777; padding-right: 5px; font-size: 14px;"><?php echo number_format($sales_order->gst_amount, 2); ?></td>
						</tr>
					<?php } ?>
					<tr>
						<th align="right" style="height: 30px; border-left: none; border-bottom: none; border-right: 1px solid #777; padding-right: 10px; font-size: 15px;">Total</th>
						<td align="right" style="font-weight:normal; border-right: none; border-bottom: none; padding-right: 5px; font-size: 14px;"><?php echo number_format($sales_order->total_amt, 2); ?></td>
					</tr>
				</table>
			</td>
        </tr>
		<?php if ($sales_order->gst == 1) {?>
		<?php #$gst_amount = $sales_order->total_amt * ($sales_order->gst_percentage / 100);?>
		<tr>
			<td colspan="3"></td>
			<td colspan="2" style="padding-left: 10px;">[<?php echo '<b style="font-size: 12px;">Inclusive of GST '.$sales_order->gst_percentage.' %</b> : $ ' . number_format($sales_order->gst_amount, 2);?>]</td>
		</tr>
		<?php }?>
		</tbody>
    </table>