<style>
    #dt_basic_wrapper{
        float: left;
        display: block;
        margin-top: -18px;
    }
    .dataTables_length{position: relative;
                       float: right;}
    .pagination>li{
        display: inline-block;
        padding:0px;
    }
</style><!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>

        <!-- Main content -->
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                    <a href="<?php echo $add_link; ?>" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-pencil"></i></button></a>
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false">

                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Invoice List</h2> 
<!--                            <span style=" float: right; margin-top: 6px;"><a href="<?php echo base_url(); ?>purchase_orders/add" style=" color: white;"><i class="fa fa-lg fa-plus-circle" aria-hidden="true"></i></a></span>-->
                        </header>

                        <div>
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <div class="widget-body no-padding">
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;" data-class="expand">S.No</th>
                                            <th style="width: 10%;" data-hide="phone"><i class="fa fa-fw fa-key txt-color-blue hidden-sm hidden-xs"></i>Inv No.</th>
                                            <th style="width: 10%;" data-hide="phone,tablet"><i class="fa fa-fw fa-calander txt-color-blue hidden-md hidden-sm hidden-xs"></i>Inv Date</th>
                                            <th style="width: 30%;" data-hide="phone,tablet"><i class="fa fa-fw fa-building txt-color-blue hidden-md hidden-sm hidden-xs"></i>Customer Name</th>
                                            <th style="width: 15%;" data-hide="phone,tablet"><i class="fa fa-fw fa-building txt-color-blue hidden-md hidden-sm hidden-xs"></i>Customer Ref.</th>
                                            <th style="width: 13%;" data-hide="phone,tablet"><i class="fa fa-fw fa-money txt-color-blue hidden-md hidden-sm hidden-xs"></i>Total Amt (S$)</th>
                                            <!--<th style="width: 15%;" data-hide="phone,tablet"><i class="fa fa-fw fa-exclamation-triangle txt-color-blue hidden-md hidden-sm hidden-xs"></i>Status</th>-->
                                            <th style="width: 17%;" data-hide="phone" style="text-align: center;"><i class="fa fa-fw fa-cog txt-color-blue hidden-xs"></i> Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php echo $this->table->generate(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                </article>
                <!-- /.row -->
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var responsiveHelper_dt_basic = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        var oTable = $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            'iDisplayLength': 100,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            },
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url(); ?>index.php/invoice/datatable',
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            //"order": [[0, "desc"]],
            "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
            "columns": [
                {"data": "iterator", "name": "iterator"},
                {"data": "invoice_no", "name": "invoice_no"},
                {"data": "invoice_date", "name": "invoice_date"},
                {"data": "name", "name": "name"},
                {"data": "invoice_reference", "name": "invoice_reference"},
                {"data": "invoice_amount", "name": "invoice_amount", "sClass": "text-right"},
                {"data": "Actions", "name": ""},
            ],
            "oLanguage": {
                "sProcessing": "<img src='<?php echo base_url(); ?>assets/images/st-13.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        });
        $('select[name="dt_basic_length"]').addClass('form-control');
        $('input[type="search"]').addClass('form-control');
        $('.fg-button').removeClass('ui-button');
    });
</script>