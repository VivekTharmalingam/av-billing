<?php $colspan = 7;?>
<table border="0" cellpadding="3" cellspacing="0" style="font-family:calibri; font-size:13px; width: 1000px;">
    <?php echo $head; ?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            Date : <?php echo text_date($from_date, '--'); ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
    <tr>
        <td colspan="2" width="100%">
            <table width="100%" border="1" style="border-collapse: collapse; font-family: calibri;">
                <tr style="font-size:14px; font-weight:bold;">
                    <th width="5%" style="padding:4px;">S.No</th>
                    <th width="32%" style="padding:4px;">Branch Name</th>
                    <th width="8%" style="padding:4px;">Inv No</th>                
                    <th width="32%" style="padding:4px;">Customer</th>
                    <th width="10%" style="padding:4px;">Sub Total</th>
                    <th width="10%" style="padding:4px;">Discount</th>
					<?php if (!empty($setting->comp_gst_no)) {?>
						<?php $colspan ++;?>
						<th width="10%" style="padding:4px;">GST Amt</th>
					<?php }?>
                    <th width="15%" style="padding:4px;">Total Amt</th>
					<?php if(in_array(7, $permission)) {?>
						<?php $colspan ++;?>
						<th width="8%" style="padding:4px;">Profit</th>
					<?php }?>
                </tr>
                <?php if (count($purchase)) { ?>
                    <?php  $to_sub = $to_dis = $to_gst = $to_amt = $profit =0; 
                    foreach ($purchase as $key => $row) { 
                        $to_sub += $row->sub_total;
                        $to_dis += $row->discount_amount;
                        $to_gst += $row->gst_amount;
                        $to_amt += $row->total_amt;
                        $profit += $row->tot_profit;
                        ?>                    
                        <tr>
                            <td style="padding:8px;"><?php echo ++$key; ?></td>
                            <td style="padding:8px;"><?php echo strtoupper(text($row->branch_name)); ?></td>
                            <td style="padding:8px;"><?php echo strtoupper(text($row->so_no)); ?></td>
                            <td style="padding:8px;"><?php echo strtoupper(text($row->name)); ?></td>
                            <td style="padding:8px;" align="right"><?php echo number_format($row->sub_total,2); ?></td>
                            <td style="padding:8px;" align="right"><?php echo number_format($row->discount_amount,2); ?></td>
							
							<?php if (!empty($setting->comp_gst_no)) {?>
								<td style="padding:8px;" align="right"><?php echo number_format($row->gst_amount,2); ?></td>
							<?php }?>
							
                            <td style="padding:8px;" align="right"><?php echo number_format(($row->total_amt),2); ?></td>
							<?php if(in_array(7, $permission)) {?>
								<td style="padding:8px;" align="right"><?php echo number_format(($row->tot_profit),2); ?></td>
							<?php }?>
                        </tr>
                    <?php } ?>
                        <tfoot><tr>
                            <td colspan="4" style="text-align: right; padding-right: 10px;"><b>Total</b></td>
                            <td style="padding:8px;" align="right"><?php echo number_format($to_sub,2);?></td>
                            <td style="padding:8px;" align="right"><?php echo number_format($to_dis,2);?></td>
							
							<?php if (!empty($setting->comp_gst_no)) {?>
								<td style="padding:8px;" align="right"><?php echo number_format($to_gst,2);?></td>
							<?php }?>
							
                            <td style="padding:8px;" align="right"><?php echo number_format($to_amt,2);?></td>
							<?php if(in_array(7, $permission)) {?>
								<td style="padding:8px;" align="right"><?php echo number_format($profit,2);?></td>
							<?php }?>
                    </tr></tfoot>
                <?php } else { ?>
                    <tr>
                        <td colspan="<?php echo $colspan;?>" align="center">No Record Found</td>
                    </tr>
                <?php } ?>
            </table>
        </td>
    </tr>
    <tr width="100%">
    <td>&nbsp;</td>
    <td align="right" style="vertical-align: top;">
		<br />
		<br />
        <table border="1" cellpadding="5" style="border-collapse:collapse; width: 250px; font-family: calibri;">
        <tbody>
            <tr>
                <td style="width:40%;"><b>Total Sales</b></td>
                <td style="width:60%;" align="right">S$ <?php echo number_format($btotal, 2); ?></td>
            </tr>
			<?php foreach ($bpaid as $key => $value) { ?>
					<tr>
						<td style="width:40%;"><b><?php echo $value->type_name; ?></b></td>
						<td style="width:60%;" align="right">S$ <?php echo (!empty($value->pay_amt)) ? number_format($value->pay_amt, 2) : '0.00'; ?></td>
					</tr>
			<?php } ?>
			<?php //foreach ($paT as $key => $value) {?>
			<!--<tr>
				<td style="width:35%;"><b><?php #echo $value['title']; ?></b></td>
				<td style="width:65%;" align="right">S$ <?php #echo (!empty($value['amt'])) ? number_format($value['amt'], 2) : '0.00'; ?></td>
			</tr>-->
        <?php //} ?>
        </tbody>
        </table>
    </td>
</tr>
</table>