<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo base_url(); ?>bank/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                            <h2><?php echo 'Bank Account Add'; ?></h2>
                            </span>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="" action="<?php echo $form_action; ?>" method="post" id="bank_form" name="bank_form" enctype="multipart/form-data">
                                <input type="hidden" class="form-control opb_update_status" name="opb_update_status" value="" />
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">                                   
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>

                                            <div class="row">      
                                                <div class="col col-6">
                                                <section>
                                                    <label class="label">Bank Name <span style="color: red">*</span></label>
                                                    <label class="input"> <i class="icon-append fa fa-bank"></i>
                                                        <input type="text" data-placeholder="XYZ"  name="bank_name" id="bank_name" maxlength="150">
                                                    </label>
                                                </section>
                                                <section>
                                                    <label class="label">Branch Code <span style="color: red"></span></label>
                                                    <label class="input"> 
                                                        <input type="text" data-placeholder="B001"  name="branch_code" id="branch_code" maxlength="150">
                                                    </label>
                                                </section>
                                                <section>
                                                    <label class="label">Branch Name <span style="color: red"></span></label>
                                                    <label class="input"> 
                                                        <input type="text" data-placeholder="ABCD"  name="branch_name" id="branch_name" maxlength="150">
                                                    </label>
                                                </section>
                                                <section>
                                                    <label class="label">Shift Code <span style="color: red"></span></label>
                                                    <label class="input"> 
                                                        <input type="text" data-placeholder="SHFT001"  name="shift_code" id="shift_code" maxlength="150">
                                                    </label>
                                                </section>    
                                                <section>
                                                    <label class="label">Status <span style="color: red">*</span></label>
                                                    <label class="select">
                                                        <select name="status" id="status" class="select2">
                                                            <option value="1">Active</option>
                                                            <option value="2">Inactive</option>
                                                        </select></label>
                                                </section>  
                                                </div>
                                                <div class="col col-6">
                                                <section>
                                                    <label class="label">Account No. <span style="color: red">*</span></label>
                                                    <label class="input"> <i class="icon-append fa fa-key"></i>
                                                        <input type="text" data-placeholder="AD02942"  name="account_no" id="account_no" maxlength="150">
                                                    </label>
                                                </section>    
                                                <section>
                                                    <label class="label">Open Balance</label>
                                                    <label class="input"> <i class="icon-append fa fa-money"></i>
                                                        <input type="text" data-placeholder="200" class="open_balance float_value" name="open_balance" id="open_balance" >
                                                    </label>
                                                </section>    
                                                <section>
                                                    <label class="label">Address </label>
                                                    <label class="textarea textarea-expandable"><textarea rows="3" name="bank_address" id="bank_address" maxlength="500"></textarea> </label> <div class="note">
                                                        <strong>Note:</strong> expands on focus.
                                                    </div>
                                                </section> 
                                                </div>
                                            </div>
                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/bank.js"></script>
