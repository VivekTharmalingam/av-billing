<script>
    window.localStorage.clear();
</script>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">&nbsp;</div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="widget-body">
                        <!-- Widget ID (each widget will need unique ID)-->                                
                        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false"	>

                            <header><span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                <h2><?php echo $page_title;?></h2>
                            </header>                                  
                            <div>
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="box-body table-responsive">
                                        <div class="date-range"></div>
                                        <form name="order_search" action="<?php echo $form_action; ?>" method="post" class="search_form validate_form">
                                            <div class="col-xs-12">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label style="width:28%; padding-top: 5px;"></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="search_date" data-placeholder="dd/mm/yyyy" id="date_alloc" style="padding: 6px 12px;margin-right:5%; width: 100%;" name="search_date" value="<?php echo $search_date; ?>" autocomplete="off" >
                                                        </div>
                                                    </div>
                                                </div>
												<div class="col-md-3">
													<div class="form-group">
														<label style="width:28%; padding-top: 5px;"></label>
														<div class="input-group">
															<div class="input-group-addon">
																<i class="fa fa-building"></i>
															</div>
															<select name="branch_id" class="select2">
																<option value="">Branch - ALL</option>
																<?php foreach($branches as $key => $branch) {?>
																	<option value="<?php echo $branch->id;?>" <?php echo ($branch->id == $branch_id)? 'selected' : '';?>><?php echo $branch->branch_name;?></option>
																<?php }?>
															</select>
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label style="width:28%; padding-top: 5px;"></label>
														<div class="input-group">
															<div class="input-group-addon">
																<i class="fa fa-user-circle-o"></i>
															</div>
															<select name="user_id" class="select2">
																<option value="">User - All</option>
																<?php foreach($users as $key => $user) {?>
																	<option value="<?php echo $user->id;?>" <?php echo ($user->id == $user_id)? 'selected' : '';?>><?php echo $user->name;?></option>
																<?php }?>
															</select>
														</div>
													</div>
												</div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <div class="input-group" style="padding-top: 17px;">
                                                            <input type="submit" name="reset_btn" value="Reset" id="reset" class="btn btn-success btn-warning" />&nbsp;&nbsp;<input type="submit" value="Search" class="btn btn-success btn-primary" />
															<input type="submit" name="submit" value="pdf" class="submit_pdf" />&nbsp;&nbsp;<input type="submit" name="submit" value="excel" class="submit_excel" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>			                
                                            <tr>
                                                <th width="5%" data-class="expand">S.No</th>
                                                <th width="10%" data-hide="phone,tablet"><i class="fa fa-fw fa-calendar text-muted hidden-md hidden-sm hidden-xs"></i>Date Time</th>
                                                <th width="10%" data-hide="phone"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> User Name</th>
                                                <th width="20%" ><i class="fa fa-fw fa-building text-muted hidden-md hidden-sm hidden-xs"></i> Module Name</th>
                                                <th width="15%" data-hide="phone"><i class="fa fa-fw fa-instagram text-muted hidden-md hidden-sm hidden-xs"></i> Operation</th>
                                                <th width="10%" data-hide="phone" style="text-align: center;"><i class="fa fa-fw fa-cog txt-color-blue hidden-md hidden-sm hidden-xs"></i> Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($log_details as $key => $row) { ?>
                                            <?php $value_arr = explode('_', $row->controller_name);
                                            $value_arr = array_map(function($word) {
                                                return ucfirst($word);
                                            }, $value_arr
                                            );

                                            $value = implode(' ', $value_arr);
                                            
                                            $value = (!empty($row->ref_number_txt)) ? $value.' (&nbsp;'.$row->ref_number_txt .'&nbsp;) ' : $value;
                                            
                                            ?>
                                                <tr>
                                                    <td><?php echo ++$key; ?></td>
                                                    <td><?php echo $row->created_on_str; ?></td>
                                                    <td><?php echo text($row->name); ?></td>
                                                    <td><?php echo text($value); ?></td>
                                                    <td><?php echo text($row->method_name); ?></td>
                                                    <td align="center">
                                                        <?php if (!empty($row->view_id) && $row->is_deleted!=1) { ?>
                                                            <a class="label btn btn-info view" href="<?php echo base_url() . $row->controller_name.'/'.$row->view_method_name.'/'.$row->view_id; ?>"><span>Details</span></a>
                                                        <?php } else { ?>
                                                            <a class="label btn btn-default view" style="color: red;" href="javascript:void(0);"><span>Details</span></a>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- end widget content -->
                            </div>
                        </div> 
                    </div>
                </article>
                <!-- /.row -->
            </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function () {
        /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            'iDisplayLength': 25,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            }
        });

        /* END BASIC */
        $('#date_alloc').daterangepicker({
            autoUpdateInput: false,
            opens: 'center',
            locale: {
                format: 'MM/DD/YYYY h:mm A',
                cancelLabel: 'Cancel'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            //alert(start + '|' + end + '|' + label);
            // getResults();
        });

        $(function () {
            $('#search_date').daterangepicker({format: 'DD/MM/YYYY'});
        });
    });
</script>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/custom.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datepicker/daterangepicker.js"></script>