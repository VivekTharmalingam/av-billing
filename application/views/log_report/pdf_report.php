<table border="0" cellpadding="3" cellspacing="0" style="font-family: calibri; font-size:14px; width: 1000px;">
    <?php echo $head;?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>
<table width="100%" border="1" style="font-family: calibri; border-collapse:collapse;" cellpadding="3">
	<thead>
	<tr>
		<th width="10%">S.NO.</th>
		<th width="20%">Date Time</th>
		<th width="25%">User Name</th>
		<th width="25%">Module Name</th>
		<th width="20%">Operation</th>
	</tr>
	</thead>
	<tbody>
	<?php if (count($log_details)) {?>
	<?php foreach($log_details as $key => $row) {?>  
        <?php $value_arr = explode('_', $row->controller_name);
        $value_arr = array_map(function($word) {
			return ucfirst($word);
		}, $value_arr
		);
		$value = implode(' ', $value_arr); ?>
		<tr>
			<td><?php echo ++ $key;?></td>
			<td><?php echo $row->created_on_str; ?></td>
			<td><?php echo strtoupper(text($row->name));?></td>
			<td><?php echo strtoupper(text($value));?></td>
			<td><?php echo strtoupper(text($row->method_name));?></td>
		</tr>
	<?php }?>
	<?php }
	else {?>
		<tr>
			<td colspan="6" align="center">No Record Found</td>
		</tr>
	<?php }?>
	</tbody>
</table>