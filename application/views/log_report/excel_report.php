<?php
$ti=time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Log_report".$ti.".xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table width="100%" border="1" style="border-collapse:collapse; margin-left:10px" cellpadding="5" cellspacing="0">
	<tr style="font-family:Times new roman;font-weight:bold;">
		<th width="6%" style="padding:10px">S.NO</th>
		<th width="20%">Date Time</th>
		<th width="20%">User Name</th>
		<th width="20%">Module Name</th>
		<th width="20%">Operation</th>
	</tr>
	<?php if (count($log_details)) {?>
	<?php foreach($log_details as $key => $row) {?> 
        <?php $value_arr = explode('_', $row->controller_name);
                $value_arr = array_map(function($word) {
                    return ucfirst($word);
                }, $value_arr
                );

                $value = implode(' ', $value_arr); ?>
		<tr>
			<td style="padding:8px;" ><?php echo ++ $key;?></td>
			<td style="padding:8px;"><?php echo $row->created_on_str;?></td>
			<td style="padding:8px;"><?php echo strtoupper(text($row->name));?></td>
			<td style="padding:8px;"><?php echo strtoupper(text($value));?></td>
			<td style="padding:8px;"><?php echo strtoupper(text($row->method_name));?></td>
		</tr>
	<?php }?>
	<?php } else {?>
		<tr>
			<td colspan="6" align="center">No Record Found</td>
		</tr>
	<?php }?>
</table>