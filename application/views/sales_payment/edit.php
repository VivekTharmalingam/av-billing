<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">

    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if(!empty($success)) {?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                            <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                            <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php }
                else if(!empty($error)) {?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error;?>
                    </div>
                <?php }?>&nbsp;
            </div>
        </div>

        <form class="" action="<?php echo $form_action;?>" method="post" name="sales_payment_form" id="sales_payment_form" enctype="multipart/form-data">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                        <a href="<?php echo $list_link; ?>" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">

                            <header>
                                <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                <h2>Invoice Payment Edit </h2>		
                                <!--<span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Purchase Payment List"><a href="<?php echo $list_link; ?>" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a>-->
                                </span>
                            </header>

                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">
                                    <div class="smart-form" >
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">Fields marked with * are mandatory</legend>
                                            <div class="row">
                                                 <input type="hidden" class="form-control pay_id" name="pay_id" value="" />
                                                 <?php #echo '<pre>';		print_r($sp_details);	echo '</pre>';?>
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Invoice No<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" value="<?php echo $sp_details->so_no;?>" readonly />
                                                            <input type="hidden" class="sales_order" value="<?php echo $sp_details->so_id?>" name="hidden_so">
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Customer Name</label>
                                                        <label class="input"><i class="icon-append fa fa-black-tie"></i>
                                                            <input type="text" class="form-control cus_name" name="cus_name" readonly="" data-placeholder="Customer" maxlength="100" value="" />
                                                        </label>
                                                    </section>
                                                    
                                                    <section>
                                                         <label class="label">Customer Address</label>
                                                         <label class="textarea">
                                                             <textarea class="form-control cus_address" readonly="" rows="3" name="cus_address" maxlength="500"></textarea>   
                                                         </label>
                                                    </section>

                                                    <section>
                                                        <label class="label">Total Amount</label>
                                                        <label class="input"><i class="icon-append fa fa-money"></i>
                                                            <input type="text" class="form-control total_amnt" readonly="" name="total_amnt" data-placeholder="Total amnt" value="" maxlength="50" />
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Payment Status <span style="color:red"></span></label>
                                                        <label class="select">
                                                            <select name="payment_status"  class="select2 payment_status" id="payment_status">
                                                                <option value="1" <?php echo ($sp_details->payment_status=='1')?'selected':'';?>>Paid</option>
                                                                <option value="2" <?php echo ($sp_details->payment_status=='2')?'selected':'';?>>Unpaid</option>
                                                                <!--<option value="3" <?php echo ($sp_details->payment_status=='3')?'selected':'';?>>Cancelled</option>-->
                                                            </select>
                                                        </label>
                                                    </section>
                                                    
<!--                                                    <section>
                                                        <label class="label">Paid Amount</label>
                                                        <label class="input"><i class="icon-append fa fa-money"></i>
                                                            <input type="text" class="form-control paid_amnt" readonly="" name="paid_amnt" data-placeholder="Paid amnt" maxlength="50" />
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">Amount To Pay<span style="color: red;">*</span><span style="float:right; color: #A90327; display: none;" class="bal_amount">Balance Amount : <b style="color: #3276B1;" class="amt"></b></span></label>
                                                        <label class="input"><i class="icon-append fa fa-money"></i>
                                                            <input type="text" class="form-control float_value  amnt-to_pay" name="amnt_to_pay" data-placeholder="Amount Pay" maxlength="50" />
                                                        </label>
                                                    </section>-->
                                                </section>
                                                 <section class="col col-6 payment_div">
                                                    <section>
                                                         <label class="label">Pay Date<span style="color:red">*</span></label>
                                                         <label class="input">
                                                             <div class="input-prepend input-group">
                                                                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                                <input type="text" name="pay_date" id="pay_date" class="bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php echo date('d/m/Y', strtotime($sp_details->pay_date)); ?>" />
                                                             </div>
                                                         </label>
                                                     </section>
                                                    <section>
                                                        <label class="label">Payment Type <span style="color:red">*</span></label>
                                                        <label class="select">
                                                            <select name="payment_type" class="select2 payment_type" id="payment_type">
                                                                <?php foreach($payment_types as $key => $value) {?>
                                                                    <option value="<?php echo $value->id;?>" data-is_cheque="<?php echo $value->is_cheque;?>" <?php echo ($sp_details->pay_pay_type == $value->id) ? 'selected' : '';?>><?php echo $value->type_name;?></option>
                                                                <?php }?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                     <section class="cheque_details" style="display: <?php echo ($sp_details->is_cheque == 1) ? 'block' : 'none';?>;">
                                                        <label class="label">Bank Name<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="form-control" name="cheque_bank_name" data-placeholder="SBI" maxlength="100" value="<?php echo trim($sp_details->pay_pay_bank);?>" />
                                                        </label>
                                                    </section>
                                                     <section class="cheque_details" style="display: <?php echo ($sp_details->is_cheque == 1) ? 'block' : 'none';?>;">
                                                        <label class="label">Cheque No<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="form-control" name="cheque_acc_no" data-placeholder="Cheque No" maxlength="100" value="<?php echo trim($sp_details->pay_pay_no);?>" />
                                                        </label>
                                                    </section>
                                                     <section class="cheque_details" style="display: <?php echo ($sp_details->is_cheque == 1) ? 'block' : 'none';?>;">
                                                        <label class="label">Cheque Date<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <?php $pay_date = (!empty($sp_details->pay_pay_date)) ? date('d/m/Y', strtotime($sp_details->pay_pay_date)) : '';?>
                                                            <input type="text" class="bootstrap-datepicker-comncls" name="cheque_date" data-placeholder="01/01/2016" maxlength="100"  value="<?php echo $pay_date; ?>"  />
                                                        </label>
                                                    </section>
                                                    <!--<section class="dd_details">
                                                        <label class="label">DD No<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="form-control" name="dd_no"  data-placeholder="DD No" maxlength="100" value="<?php echo $sp_details->pay_dd_no;?> " />
                                                        </label>
                                                    </section>
                                                     <section class="dd_details">
                                                        <label class="label">DD Date<span style="color:red">*</span></label>
                                                        <label class="input">
                                                            <input type="text" class="bootstrap-datepicker-comncls" name="dd_date"  data-placeholder="DD Date" maxlength="100"  value="<?php echo date('d/m/Y', strtotime($sp_details->pay_dd_date)); ?>" />
                                                        </label>
                                                    </section>--> 
                                                </section> 
                                                 
                                            </div>
                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn btn-primary" >Submit</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                <!-- /.row -->
                </div>
            </section>
        </form>
        <!-- /.content -->
    </div>
    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/app/sales_payment.js"></script>

