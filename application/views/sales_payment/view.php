<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo base_url(); ?>invoice_payment/" class="large_icon_des" style=" color: white;"><button type="button" class="btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-file-text-o"></i> </span>
                            <h2>Invoice Payment View </h2>	
                             <!--<span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Purchase Payment List"><a href="<?php echo base_url(); ?>purchase_payments/" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a>-->
                             
                             </span>
                            
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body">
                                <?php //print_r($sp_details);?>
                                 <table id="user" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                         <tr>
                                            <td style="width:35%;"><b>Payment No</b></td>
                                            <td style="width:65%"><?php echo text($sp_details->pay_no);?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;"><b>Invoice No</b></td>
                                            <td style="width:65%"><?php echo text($sp_details->so_no);?></td>
                                        </tr>
                                       
                                        <tr>
                                            <td><b>Customer Name</b></td>
                                            <td><?php echo text($sp_details->name);?></td>
                                        </tr>
                                        
                                        <tr>
                                            <td><b>Customer Address</b></td>
                                            <td><?php echo text($sp_details->address);?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Total Amount</b></td>
                                            <td><?php echo number_format($sp_details->pay_amt, 2);?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Payment Status</b></td>
                                            <td><?php echo ($sp_details->status_str);?></td>
                                        </tr>
                                        <?php if($sp_details->payment_status=='1'){?>
                                        <tr>
                                            <td><b>Payment Type</b></td>
                                            <td>
                                                <?php echo $sp_details->type_name;?>
                                                <?php if($sp_details->is_cheque == '1') { ?>
                                                    <div class="btn-group">
                                                        <a href="javascript:void(0);" style="text-decoration: none;" class="dropdown-toggle" data-toggle="dropdown" rel="popover" data-placement="top"  data-content="<?php //echo $toolboxtxt; ?>" data-html="true"><i class="fa fa-info-circle" ></i></a>
                                                        <ul class="dropdown-menu" role="menu" style="width: 200px;">
                                                            <li><label>&nbsp;Bank Name&nbsp;:&nbsp;</label><?php echo text($sp_details->pay_pay_bank);?></li>
                                                            <li><label>&nbsp;Cheque No&nbsp;:&nbsp;</label><?php echo text($sp_details->pay_pay_no);?></li>
                                                            <li><label>&nbsp;Cheque Date&nbsp;:&nbsp;</label><?php echo date('d-m-y',strtotime($sp_details->pay_pay_date));?></li> 
                                                        </ul>
                                                    </div>
                                                 <?php }?>
                                             </td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                            </div> 
                        </div>
                        <!-- end widget div -->

                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script>
    $(document).ready(function() {
                    
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
	
			/* END BASIC */
                $(".smart-mod-delete").click(function(e) {
                                var $bindId = $(this).data('bindid');
                                var $bindText = $(this).data('bindtext');
                                $.SmartMessageBox({
                                        title : "Are you sure?",
                                        content : "You will not be able to recover this Purchase Payment Details!",
                                        buttons : '[No][Yes]'
                                }, function(ButtonPressed) {
                                        if (ButtonPressed === "Yes") {
                                                window.location.replace(baseUrl + "purchase_payments/delete/" + $bindId);
                                        }
                                        if (ButtonPressed === "No") {
                                                $.smallBox({
                                                        title : "Sorry ! ...",
                                                        content : "<i class='fa fa-info-circle'></i> <i>You just selected NO...<br>So The action of DELETE is disallowed for the "+$bindText+" details.. </i>",
                                                        color : "#C46A69",
                                                        iconSmall : "fa fa-times fa-2x fadeInRight animated",
                                                        timeout : 4000
                                                });
                                        }

                                });
                                e.preventDefault();
                        })
                    });
    </script>