<table border="0" cellpadding="3" cellspacing="0" style="font-family: calibri; font-size:14px; width: 1000px;">
    <?php echo $head; ?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>
<?php $colspan = 7;?>
<table width="100%" border="1" style="font-family: calibri; border-collapse:collapse;" cellpadding="3">
	<thead>
		<tr style="font-size:16px;">
			<th width="12%">Inv No</th>
			<th width="10%">Inv Date</th>
			<th width="10%">Issued By</th>
			<th width="25%">Customer</th>
			<th width="13%">Customer Ref.</th>
			<th width="10%">Pay Status</th>
			<th width="10%">Sales Amt</th>
			<?php if(in_array(7, $permission)) {?>
				<?php $colspan = 8;?>
				<th width="10%">Profit Amt</th>
			<?php }?>
		</tr>
	</thead>
	<?php if (count($purchase)) { ?>
		<tbody>
		<?php $to_amt = $profit = 0;
		foreach ($purchase as $key => $row) {
			$to_amt += $row->total_amt;
			$profit += $row->tot_profit;?>                    
			<tr>
				<td><?php echo text($row->so_no); ?></td>                            
				<td><?php echo nl2br($row->so_date_str); ?></td>
				<td><?php echo strtoupper(text($row->issued_by)); ?></td>
				<td><?php echo strtoupper(text($row->name)); ?></td>
				<td><?php echo strtoupper(text($row->your_reference)); ?></td>
				<td><?php echo text($row->pay_sts); ?></td>
				<td align="right"><?php echo number_format($row->total_amt,2); ?></td>
				<?php if(in_array(7, $permission)) {?>
					<td align="right"><?php echo number_format($row->tot_profit,2); ?></td>
				<?php }?>
			</tr>
		<?php } ?>
		<?php if(in_array(7, $permission)) {?>
			<tr>
				<td align="right" colspan="<?php echo $colspan - 2;?>">Total</td>
				<td align="right" style="height: 30px;"><?php echo number_format($to_amt, 2); ?></td>
				<td align="right"><?php echo number_format($profit, 2); ?></td>
			</tr>
		<?php }?>
		</tbody>
	<?php }
	else { ?>
		<tbody>
			<tr>
				<td colspan="<?php echo $colspan;?>" align="center">No Record Found</td>
			</tr>
		</tbody>
	<?php } ?>
</table>