
<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                     <a href="<?php echo $list_link; ?>" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>User - View</h2>	
<!--                             <span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Users List"><a href="<?php echo $list_link; ?>" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a></span>-->
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body"> 
                                <span id="watermark"><?php echo WATER_MARK; ?></span>
                                 <table id="user" class="table table-bordered table-striped" style="clear: both;">
                                    <tbody>
                                        <tr>
                                            <td style="width:35%;"><b>Branch Name</b></td>
                                            <td style="width:65%">
                                                <?php 
                                                    $brn_names = '';
                                                    $brn_id = explode(',', $user->branch_id );
                                                    $tot_count = count($brn_id);
                                                    $int_val = 1;
                                                    foreach( $branches as $key => $value ) : 
                                                        if( in_array( $value->id, $brn_id  ) ) :
                                                            if($int_val == $tot_count) :
                                                                $brn_names .= $value->branch_name ;
                                                            else :
                                                                $brn_names .= $value->branch_name .',<br />';
                                                            endif;
                                                            $int_val++;
                                                        endif;
                                                    endforeach;
                                                    echo $brn_names;
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><b>Name</b></td>
                                            <td><?php echo $user->name;?></td>
                                        </tr>
                                        <tr>
                                            <td><b>User Name</b></td>
                                            <td><?php echo $user->user_name;?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Email</b></td>
                                            <td><?php echo $user->email;?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Is Admin</b></td>
                                            <td><?php echo ($user->is_admin == 1) ? 'Yes' : 'No';?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Push Notification</b></td>
                                            <td><?php echo ($user->push_notify == 1) ? 'Yes' : 'No';?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Status</b></td>
                                            <td><?php echo ( $user->status === '1' ) ? 'Active' : 'Inactive'; ?></td>
                                        </tr>        
                                        <?php if(!empty($user->image)){ ?>
                                        <tr>
                                            <td><b>Profile Photo</b></td>
                                            <td><img src="<?php echo ( !empty( $user->image  ) ? BASE_URL . UPLOADS . $user->image : BASE_URL . DEFAULT_AVATAR ); ?>" width="35" height="35"/></td>
                                        </tr>        
                                        <?php } ?>
                                    </tbody>
                                </table>
                                
                                <div class="smart-form" >      
                                    <fieldset>
                                        <section >
                                            <label class="label" style=" font-weight: bold;">Permissions</label>
                                            <label class="checkbox state-error"><input name="permissions[check_all]" type="checkbox" class="check-all" disabled="disabled" ><i></i>Full Permissions</label>
                                        </section>
                                    </fieldset>

                                    <fieldset id="permissions">


                                        <?php
                                            $per_arr    = array(
                                                '1'     => 'View',
                                                '2'     => 'Add',
                                                '3'     => 'Edit',
                                                '4'     => 'Delete',
                                                '5'     => 'Export Excel',
                                                '6'     => 'Print PDF',
                                                '10'    => 'Approve'    

                                            );

                                            $split = 0;//floor( 12 / count( $per_arr ) );
                                            $lvl1 = 0;
                                            foreach( $permissions_struct as $mk => $m ) :
                                                $i = $lvl2 = 0;
                                                $me = explode( '`', $m['label'] );    
                                                $mslug = get_var_name( $me[0], '_' );
                                                ?>
                                                <div class="row">
                                                    <section class="col col-xs-12">
                                                        <div>
                                                            <header style="font-size: 14px;">
                                                                <label class="checkbox state-error"><input type="checkbox" name="permissions[<?php echo $mslug; ?>]" class="master super_master lvl lvl-1" data-lvl="1" data-val="<?php echo ++$lvl1; ?>"  data-master="user,company" <?php echo ( !empty( $permissions[$mslug] ) ) ? 'checked' : ''; ?>  disabled="disabled"><i></i><b style="color: #1baadd;"><?php echo $me[0]; ?></b></label> 
                                                            </header>

                                                            <fieldset>
                                                                <div class="row">
                                                                <?php
                                                                    unset( $m['label'] );
                                                                    unset( $m['cls_menu'] );

                                                                    if( count( $m ) > 0 ) :
                                                                        foreach( $m as $prefix => $item ) :
                                                                            
                                                                            $ie     = explode( '`', $item['label'] );
                                                                            $islug   = get_var_name( $ie[0], '_' );   
                                                                            $cntrlr = !empty( $item['controller'] ) ? $item['controller'] : $islug;
                                                                            $lvl3 = 0;
                                                                        ?>
                                                                            <section class="col col-6">
                                                                                <label class="label checkbox state-error"><input type="checkbox" name="permissions[<?php echo $cntrlr; ?>]" class="master lvl lvl-2" data-lvl="2" data-val="<?php echo ++$lvl2; ?>" data-master="user" <?php echo ( !empty( $permissions[$cntrlr] ) ) ? 'checked' : ''; ?>  disabled="disabled"><i></i><span style="font-weight: bold;"><?php echo $ie[0]; ?></span></label> 
                                                                                <div class="row per-blck" style="margin: 10px; margin-right: 0; padding: 3px;">
                                                                                    <?php
                                                                                        unset( $item['label'] );

                                                                                        $per_arrs =  $per_arr;
                                                                                        if( !empty( $item['extra_per'] ) ) :

                                                                                            foreach( $item['extra_per'] as $ek => $eka ) :
                                                                                                if( empty( $eka['label'] ) ) :
                                                                                                    unset( $item['extra_per'][$ek] );
                                                                                                endif;
                                                                                                unset($per_arrs[$ek]);
                                                                                            endforeach;

                                                                                            $per_arrs = ( $per_arrs + $item['extra_per'] );
                                                                                        endif;
                                                                                        
                                                                                        //$per_arrs = !empty( $item['extra_per'] ) ? ( $per_arr + $item['extra_per'] ) : $per_arr;
                                                                                        foreach( $per_arrs as $pk => $per ) :

                                                                                            if( ( $mk == 'masters' && $pk == 10 ) || ( $mk == 'reports' && $pk != 1 && $pk != 5 && $pk != 6 && $pk != 7 ) ) :
                                                                                                continue;
                                                                                            endif;
                                                                                            ?>
                                                                                                <div class="col col-<?php echo $split; ?>">
                                                                                                    <label class="checkbox state-error"><input name="permissions[<?php echo $cntrlr; ?>][<?php echo $pk; ?>]" class="permission acts lvl lvl-3" data-lvl="3" data-val="<?php echo ++$lvl3; ?>" value="<?php echo $pk; ?>" <?php echo ( !empty( $permissions[$cntrlr][$pk] ) ) ? 'checked' : ''; ?> type="checkbox"  disabled="disabled"><i></i><?php echo !empty( $per['label'] ) ? $per['label'] : $per; ?></label>
                                                                                                </div>
                                                                                            <?php
                                                                                        endforeach;
                                                                                    ?>

                                                                                </div>
                                                                            </section>
                                                                            <?php
                                                                            if( ++$i % 2 == 0 ) :
                                                                                // Add new row
                                                                                echo '</div></fieldset><fieldset><div class="row">';
                                                                                //echo '</div><div class="row">';
                                                                            endif;

                                                                        endforeach;

                                                                    endif;
                                                                ?>
                                                            </fieldset>    


                                                        </div>   
                                                    </section>
                                                </div>
                                                <?php

                                            endforeach;

                                        ?>
                                    </fieldset>
                                </div> <!-- smart-form -->
                                
                            </div>                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
