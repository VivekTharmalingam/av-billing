<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/user.js"></script>
<aside class="right-side">
	<section class="content-header">
        <h1>Change Password</h1>
    </section>
	<section class="content">
        <div class="row">
		 <!-- .box-tools -->
                    <div class="pull-right box-tools">
                        <div class="back_btn" style="padding-right: 18px;">
                            <a href="javascript:void(0);" onclick="goBack()">
                                <i class="fa fa-chevron-circle-left"></i>&nbsp;&nbsp;<b>Back</b>
                            </a>
                        </div>
                    </div>
                    <!-- /.box-tools -->
            <div class="col-xs-12">
                <!-- general form elements -->
                <div class="box box-danger">
					<span style="color:#FF0000; padding-left:15px;">All fields are mandatory</span>
                    
					<!-- form start -->
					<div class="box-body table-responsive">
                        <?php if(!empty($success)) {?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><?php echo $success;?></h4>
                            </div>
                        <?php }
						else if(!empty($error)) {?>
							<div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><?php echo $error;?></h4>
                            </div>
                        <?php }?>
                        
                        <form action="<?php echo $form_action;?>" method="post" name="change_password">
                        	<div class="error_wrapper">
							    <span><ul class="error_container"></ul></span>
						    </div>
							<div class="row">
								<div class="left-col">
									<div class="form-group">
										<label>New Password</label>
										<input type="password" class="form-control" name="new_password" value="" maxlength="20" autofocus />
									</div>
									
									<div class="form-group">
										<label>Confirm Password</label>
										<input type="password" class="form-control" name="confirm_password" value="" maxlength="20" />
									</div>
								</div>
                            </div>
                            
                            <div class="box-footer" style="text-align: center; width: 50%;">
								<input class="btn reset" type="reset" value="Reset" />
                                <input type="submit" name="submit" value="Change Password" class="btn btn-success btn-primary" style="margin-left: 50px;" />
							</div>
                        </form>
					</div>
                 </div><!-- /.box -->
            </div><!-- /.col-xs-12 -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</aside>