<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <?php
        /*
         * Breadcrumbs
         */
        echo breadcrumb_links();
        ?>
    </div>

    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) : ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php elseif (!empty($error)) : ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>

        <form class="" action="<?php echo $form_action; ?>" method="post" name="user_form" id="user_form" enctype="multipart/form-data">
            <!-- Main content -->
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-sm-12">	
                        <a href="<?php echo $list_link; ?>" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">

                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>User - Add </h2>
<!--                                <span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Users List"><a href="<?php echo $list_link; ?>" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a></span>-->
                            </header>

                            <div style="padding: 0px 0px 0px 0px;">
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body" style="padding-bottom: 0px;">

                                    <div class="smart-form" >

                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">
                                                All fields are mandatory
                                            </legend>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <input type="hidden" class="form-control user_id" name="user_id" value="" />
                                                    <section>
                                                        <label class="label">Branch Name</label>
                                                        <label class="select"><span style="float: right;margin: 8px 10px -22px 0px;z-index: 1;" class="glyphicon glyphicon-chevron-down"></span>
                                                            <select name="branch_ids[]" multiple class="select2 company_ids">
                                                                <?php
                                                                foreach ($branches as $key => $value) :
                                                                    ?>
                                                                    <option value="<?php echo $value->id ?>"><?php echo $value->branch_name; ?></option>
                                                                    <?php
                                                                endforeach;
                                                                ?>
                                                                    
                                                            </select>
                                                            
                                                        </label>

                                                    </section>
                                                    <section>
                                                        <label class="label">Name</label>
                                                        <label class="input">
                                                            <input type="text" class="form-control" name="name" data-placeholder="Paul Jacob" maxlength="50" value="" />
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="label">User Name</label>
                                                        <label class="input">
                                                            <input type="text" class="form-control user_name lowercase" name="user_name" maxlength="50" value="" />
                                                        </label>
                                                    </section>

                                                    <section>
                                                        <label class="label">Password</label>
                                                        <label class="input">
                                                            <input type="password" class="form-control lowercase" name="password" maxlength="50" value="" />
                                                        </label>
                                                    </section>
                                                    
                                                    <section>
                                                        <label class="label">Push Notification</label>
                                                        <label class="">
                                                            <label class="checkbox state-success">
                                                                <input type="checkbox" class="checkbox" name="push_notify" value="1"><i></i>

                                                            </label>
                                                        </label> 
                                                    </section>

                                                </section>

                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Email</label>
                                                        <label class="input">
                                                            <input type="text" class="form-control email lowercase" name="email" maxlength="50" value=""/>
                                                        </label>
                                                    </section>

                                                    <section>
                                                        <label class="label">Profile Photo</label>
                                                        <label class="input">

                                                            <div class="parallel thumb preview" style="width: 9%;">
                                                                <img src="<?php echo BASE_URL . DEFAULT_AVATAR; ?>" />
                                                            </div>
                                                            <input type="file" class="form-control btn btn-default parallel" name="file" value="" data-default="<?php echo BASE_URL . DEFAULT_AVATAR; ?>" style="width: 90%; height: 45px;"/>
                                                        </label>
                                                    </section>

                                                    <section>
                                                        <label class="label">Status</label>
                                                        <label class="select">
                                                            <select class="select2" name="status">
                                                                <option value="1" selected>Active</option>
                                                                <option value="2">Inactive</option>
                                                            </select> 
                                                        </label>
                                                    </section>

                                                    <section>
                                                        <label class="label">Is Admin</label>
                                                        <label class="">
                                                            <label class="checkbox state-success">
                                                                <input type="checkbox" class="checkbox" name="is_admin" value="1"><i></i>

                                                            </label>
                                                        </label> 
                                                    </section> 
                                                </section>    
                                            </div>
                                        </fieldset>

                                        <fieldset>
                                            <section >
                                                <label class="label" style=" font-weight: bold;">Permissions</label>
                                                <label class="checkbox state-error"><input name="permissions[check_all]" type="checkbox" class="check-all"><i></i>Full Permissions</label>
                                            </section>
                                        </fieldset>

                                        <fieldset id="permissions">


                                            <?php
                                            $per_arr = array(
                                                '1' => 'View',
                                                '2' => 'Add',
                                                '3' => 'Edit',
                                                '4' => 'Delete',
                                                /* '5' => 'Export Excel', */
                                                '6' => 'Print PDF'
                                            );

                                            $split = 0; //floor( 12 / count( $per_arr ) );
                                            $lvl1 = 0;
                                            foreach ($permissions_struct as $mk => $m) :
                                                $i = $lvl2 = 0;
                                                $me = explode('`', $m['label']);
                                                ?>
                                                <div class="row">
                                                    <section class="col col-xs-12">
                                                        <div>
                                                            <header style="font-size: 14px;">
                                                                <label class="checkbox state-error"><input type="checkbox" name="permissions[<?php echo get_var_name($me[0], '_'); ?>]" class="master super_master lvl lvl-1" data-lvl="1" data-val="<?php echo ++$lvl1; ?>"  data-master="user,company" ><i></i><b style="color: #1baadd;"><?php echo $me[0]; ?></b></label> 
                                                            </header>

                                                            <fieldset>
                                                                <div class="row">
                                                                    <?php
                                                                    unset($m['label']);
                                                                    unset($m['cls_menu']);

                                                                    if (count($m) > 0) :
                                                                        foreach ($m as $prefix => $item) :

                                                                            $ie = explode('`', $item['label']);
                                                                            $islug = get_var_name($ie[0], '_');
                                                                            $cntrlr = !empty($item['controller']) ? $item['controller'] : $islug;
                                                                            $lvl3 = 0;
                                                                            ?>
                                                                            <section class="col col-6">
                                                                                <label class="label checkbox state-error"><input type="checkbox" name="permissions[<?php echo $cntrlr; ?>]" class="master lvl lvl-2" data-lvl="2" data-val="<?php echo ++$lvl2; ?>" data-master="user" ><i></i><span style="font-weight: bold;"><?php echo $ie[0]; ?></span></label> 
                                                                                <div class="row per-blck" style="margin: 10px; margin-right: 0; padding: 3px;">
                                                                                    <?php
                                                                                    unset($item['label']);

                                                                                    $per_arrs = $per_arr;
                                                                                    if (!empty($item['extra_per'])) :

                                                                                        foreach ($item['extra_per'] as $ek => $eka) :
                                                                                            if (empty($eka['label'])) :
                                                                                                unset($item['extra_per'][$ek]);
                                                                                            endif;

                                                                                            unset($per_arrs[$ek]);
                                                                                        endforeach;

                                                                                        $per_arrs = ( $per_arrs + $item['extra_per'] );
                                                                                    endif;

                                                                                    // $per_arrs = !empty( $item['extra_per'] ) ? ( $per_arr + $item['extra_per'] ) : $per_arr;
                                                                                    foreach ($per_arrs as $pk => $per) :

                                                                                        if (( $mk == 'masters' && $pk == 6 ) || ( $mk == 'reports' && $pk != 1 && $pk != 5 && $pk != 6  && $pk != 7 )) :
                                                                                            continue;
                                                                                        endif;
                                                                                        ?>
                                                                                        <div class="col col-<?php echo $split; ?>">
                                                                                            <label class="checkbox state-error"><input name="permissions[<?php echo $cntrlr; ?>][<?php echo $pk; ?>]" class="permission acts lvl lvl-3" data-lvl="3" data-val="<?php echo ++$lvl3; ?>" value="<?php echo $pk; ?>" type="checkbox"><i></i><?php echo!empty($per['label']) ? $per['label'] : $per; ?></label>
                                                                                        </div>
                                                                                        <?php
                                                                                    endforeach;
                                                                                    ?>

                                                                                </div>
                                                                            </section>
                                                                            <?php
                                                                            if (++$i % 2 == 0) :
                                                                                // Add new row
                                                                                echo '</div></fieldset><fieldset><div class="row">';
                                                                            //echo '</div><div class="row">';
                                                                            endif;

                                                                        endforeach;

                                                                    endif;
                                                                    ?>
                                                            </fieldset>    


                                                        </div>   
                                                    </section>
                                                </div>
                                                <?php
                                            endforeach;
                                            ?>



                                            <!--
                                            
                                            <div class="row">
                                                <section class="col col-6">
                                                    <div >
                                                        <header style="font-size: 14px;">
                                                            <label class="checkbox state-error"><input type="checkbox" name="permissions[masters]" class="master super_master" data-master="user,company" value="1" ><i></i><b>Masters</b></label> 
                                                        </header>

                                                        <fieldset>
                                                            <section>
                                                                <label class="label checkbox state-error"><input type="checkbox" name="permissions[user]" class="master" data-master="user" value="All" ><i></i><span style="font-weight: bold;">User Master</span></label> 
                                                                <div class="row">
                                                                    <div class="col col-3">
                                                                        <label class="checkbox state-error"><input name="permissions[user][]" class="permission" value="1" type="checkbox"><i></i>View</label>
                                                                    </div>
                                                                    <div class="col col-3">
                                                                        <label class="checkbox state-error"><input name="permissions[user][]" class="permission" value="2" type="checkbox"><i></i>Add</label>
                                                                    </div>
                                                                    <div class="col col-3">
                                                                        <label class="checkbox state-error"><input name="permissions[user][]" class="permission" value="3" type="checkbox"><i></i>Edit</label>
                                                                    </div>
                                                                    <div class="col col-3">
                                                                        <label class="checkbox state-error"><input name="permissions[user][]" class="permission" value="4" type="checkbox"><i></i>Delete</label>
                                                                    </div>
                                                                </div>
                                                            </section>
                                                            <section>
                                                                <label class="label checkbox state-error"><input type="checkbox" name="permissions[company]" class="master" data-master="company" value="All" ><i></i><span style="font-weight: bold;">Company Master</span></label> 
                                                                <div class="row">
                                                                    <div class="col col-3">
                                                                        <label class="checkbox state-error"><input name="permissions[company][]" class="permission" value="1" type="checkbox"><i></i>View</label>
                                                                    </div>
                                                                    <div class="col col-3">
                                                                        <label class="checkbox state-error"><input name="permissions[company][]" class="permission" value="2" type="checkbox"><i></i>Add</label>
                                                                    </div>
                                                                    <div class="col col-3">
                                                                        <label class="checkbox state-error"><input name="permissions[company][]" class="permission" value="3" type="checkbox"><i></i>Edit</label>
                                                                    </div>
                                                                    <div class="col col-3">
                                                                        <label class="checkbox state-error"><input name="permissions[company][]" class="permission" value="4" type="checkbox"><i></i>Delete</label>
                                                                    </div>
                                                                </div>
                                                            </section>
                                                        </fieldset>
                                                    </div>
                                                </section>
                                                <section class="col col-6">
                                                    <div >
                                                        <header style="font-size: 14px;">
                                                            <label class="checkbox state-error"><input type="checkbox" name="permissions[boq]" class="master" data-master="boq" value="All" ><i></i><b>Operations</b></label> 
                                                        </header>

                                                        <fieldset>
                                                            <section>
                                                                <label class="label checkbox state-error"><input type="checkbox" name="permissions[boq]" class="master" data-master="user" value="All" ><i></i><span style="font-weight: bold;">BOQ</span></label> 
                                                                <div class="row">
                                                                    <div class="col col-3">
                                                                        <label class="checkbox state-error"><input name="permissions[boq][]" class="permission" value="1" type="checkbox"><i></i>View</label>
                                                                    </div>
                                                                    <div class="col col-3">
                                                                        <label class="checkbox state-error"><input name="permissions[boq][]" class="permission" value="2" type="checkbox"><i></i>Add</label>
                                                                    </div>
                                                                    <div class="col col-3">
                                                                        <label class="checkbox state-error"><input name="permissions[boq][]" class="permission" value="3" type="checkbox"><i></i>Edit</label>
                                                                    </div>
                                                                    <div class="col col-3">
                                                                        <label class="checkbox state-error"><input name="permissions[boq][]" class="permission" value="4" type="checkbox"><i></i>Delete</label>
                                                                    </div>
                                                                </div>	
                                                            </section>
                                                        </fieldset>
                                                    </div>
                                                </section>
                                            </div>
                                            
                                            -->
                                            <div class="note note-error">You must select at least one option.</div>
                                        </fieldset>
                                        <footer>
                                            <button type="submit" name="submit" class="btn btn-primary" >Submit</button>
                                            <button type="reset" class="btn btn-default" >Cancel</button>
                                        </footer>
                                    </div>

                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </article>
                    <!-- /.row -->
                </div>
            </section>
        </form>

        <!-- /.content -->
    </div>

    <!-- /.main -->
</div>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/app/users.js"></script>