<table border="0" cellpadding="3" cellspacing="0" style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:14px; width: 1000px;">
    <?php echo $head;?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
	
	<tr>
		<td><b>Customer Name</b></td>
		<td>:  <?php echo text($customer->name);?></td>
	</tr>	
	<tr>
		<td><b>Contact Name</b></td>		
		<td>:  <?php echo text($customer->contact_name);?></td>
	</tr>                                        
	<tr>
		<td><b>Phone No</b></td>
		<td>:  <?php echo text($customer->phone_no);?></td>
	</tr>
	<tr>
		<td><b>Email</b></td>
		<td>: <?php echo text($customer->email);?></td>
	</tr>
	<tr>
		<td><b>Address</b></td>
		<td>:  <?php echo text($customer->address);?></td>
	</tr>
	<tr>
		<td><b>Total Invoice Amount</b></td>
		<td>:  <?php echo text_amount($customer->so_amt);?></td>
	</tr>
	<tr>
		<td><b>Total Paid Amount</b></td>
		<td>:  <?php echo text_amount($customer->paid_amt);?></td>
	</tr>
	
	<tr>
        <td colspan="2" height="20"></td>
    </tr>
	
    <tr>
        <td colspan="2" width="100%">
            <table width="100%" border="1" style="border-collapse:collapse; margin-left:10px">
                <tr style="font-family:Times new roman;font-size:16px;font-weight:bold;">
                    <th width="6%" style="padding:10px">S.NO</th>
					<th width="20%">Invoice No</th>
                    <th width="25%">Invoice Date</th>
                    <th width="25%">Total Amount</th>
                    <th width="20%">Payment Status</th>
                </tr>
                <?php if (count($sales)) {?>
                <?php foreach($sales as $key => $val) {?>                    
                    <tr>
                        <td style="padding:8px;" ><?php echo ++ $key;?></td>
                        <td style="padding:8px;"><?php echo text($val->so_no);?></td>
                        <td style="padding:8px;"><?php echo text_date($val->so_date);?></td>
                        <td style="padding:8px;"><?php echo text($val->total_amt);?></td>
                        <td style="padding:8px;"><?php echo text($val->status_str);?></td>
                    </tr>
                <?php }?>
                <?php } else {?>
                    <tr>
                        <td colspan="5" align="center">No Record Found</td>
                    </tr>
                <?php }?>
            </table>
        </td>
    </tr>
</table>