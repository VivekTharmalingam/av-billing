<?php
$ti=time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Customer_Payment_Summary_Report".$ti.".xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table width="100%" border="1" cellpadding="3" cellspacing="0" style="border-collapse:collapse; margin-left:10px">
	<tr style="font-family: calibri; font-weight:bold;">
		<th width="5%">S.NO</th>
		<th width="25%">Customer</th>
		<th width="15%">Customer Ref.</th>
		<th width="23%">Address</th>
		<th width="11%">Total Amount</th>
		<th width="11%">Paid Amount</th>
		<th width="10%">Balance Amount</th>
	</tr>
	<?php if (count($sales)) {?>
	<?php foreach($sales as $key => $row) {?>                    
		<tr>
			<td><?php echo ++ $key;?></td>
			<td><?php echo strtoupper(text($row->name));?></td>
			<td><?php echo strtoupper(text($row->your_reference));?></td>
			<td><?php echo strtoupper(text($row->address));?></td>
			<td align="right"><?php echo number_format($row->so_amt, 2); ?></td>
			<td align="right"><?php echo number_format($row->paid_amt, 2); ?></td>
			<td align="right"><?php echo number_format($row->so_amt - $row->paid_amt, 2); ?></td>
		</tr>
	<?php }?>
	<?php } else {?>
		<tr>
			<td colspan="7" align="center">No Record Found</td>
		</tr>
	<?php }?>
</table>