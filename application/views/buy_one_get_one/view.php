<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                     <a href="<?php echo base_url(); ?>buy_one_get_one/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>Buy One Get One - View</h2>	
<!--                             <span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Users List"><a href="<?php echo $list_link; ?>" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a></span>-->
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body"> 
                                <span id="watermark"><?php echo WATER_MARK; ?></span>
                                 <table id="user" class="table table-bordered table-striped" style="clear: both;">
                                    <tbody>
                                        <tr>
                                            <td style="width:35%;"><b>Item Name</b></td>
                                            <td style="width:65%;"><?php echo text($bogo->pdt_name);?></td>
                                        </tr>
                                        <tr>
                                            <td><b>From Date</b></td>
                                            <td><?php echo text_date($bogo->from_date);?></td>
                                        </tr>
                                        <tr>
                                            <td><b>To Date</b></td>
                                            <td><?php echo text_date($bogo->to_date);?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Remarks</b></td>
                                            <td><?php echo text($bogo->remarks);?></td>
                                        </tr>
										<tr>
                                            <td><b>Status</b></td>
                                            <td><?php echo ( $bogo->status_str === 'Active' ) ? 'Active' : 'Inactive'; ?></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="text-left">
                                        <span class="onoffswitch-title">
                                          <h3><b style="color: #8e233b;">Offered Items</b></h3>
                                        </span>
                                </div>
								
                                    <div id="widget-tab-1" class="table-responsive">
                                        <table id="dt_basic_2" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
                                           <thead>
                                              <tr>
                                                   <th data-class="expand" style="font-weight: bold; text-align:center;">S.No</th>
                                                   <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Offered Item Name</th>
                                                   <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Selling Price</th>
<!--												   <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Offered Price</th>-->
                                              </tr>
                                           <thead>
                                           <tbody>
												<?php $total_price = 0;?>
												<?php foreach($bogo_items as $key=>$item){?>
													<?php $total_price += $item->selling_price;?>
                                                    <tr>
                                                        <td align="center" style="width:10%;"><?php echo ++$key;?></td>
                                                        <td align="left" style="width:40%;"><?php echo text($item->pdt_name);?></td>
                                                        <td align="right" style="width:40%;"><?php echo text_amount($item->selling_price);?></td>
                                                    </tr>
                                               <?php } ?>
                                            </tbody>
											<tfoot>
												<tr>
													<th colspan="2" style="text-align: right;">Total</th>
													<td align="right"><?php echo text_amount($total_price);?></td>
												</tr>
											</tfoot>
                                        </table>
                            </div>
                        </div>
                        <!-- end widget div -->

                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
