<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo base_url(); ?>buy_one_get_one/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                            </span>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="" action="<?php echo $form_action; ?>" method="post" id="bogo_form" name="bogo_form" enctype="multipart/form-data">

                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">                                   
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>
											
											<div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Buy Item <span style="color: red">*</span></label>
                                                    <label class="select">
                                                        <select name="purchase_item" id="purchase_item" class="select2">
                                                            <option value="">Item Code / Item name</option>
                                                            <?php foreach ($items as $key => $item) { ?>
                                                                <option value="<?php echo $item->id; ?>"><?php echo $item->pdt_code.' / '.$item->pdt_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </label>
                                                </section>
                                            </div>
											
                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">From Date <span style="color: red"></span></label>
                                                    <label class="input"> <i class="icon-append fa fa-calendar-check-o"></i>
                                                        <input type="text" name="from_date" id="from_date" class="form-control bootstrap-datepicker-comncls" tabindex="-1" value="<?php echo date('d/m/Y'); ?>"/>
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">To Date <span style="color: red"></span></label>
                                                    <label class="input"> <i class="icon-append fa fa-calendar-check-o"></i>
                                                        <input type="text" name="to_date" id="to_date" class="form-control bootstrap-datepicker-comncls" tabindex="-1" />
                                                    </label>
                                                </section>
                                            </div>
                                        </fieldset>
										
                                        <fieldset >
                                            <legend style="font-weight: bold; padding-bottom: 10px; font-size: 15px; color: #A90329; border-bottom: 1px dashed rgba(0,0,0,.2);">Offered Items</legend>
                                            <div class="row material-container product_details" style="border-radius:5px; border: 1px outset #eee; padding: 10px; margin: 0px 10px 20px 10px; box-shadow: 3px 3px 3px #eee; background: #fcfcfc; ">
                                                <div class="clr items" >
                                                    <section class="col col-5">
                                                        <label class="label">Item Code / Item name <span style="color: red">*</span></label>
                                                        <label class="select">
															<select name="items[0]" id="items" class="select2 item_name">
																<option value="">Item Code / Item name</option>
																<?php foreach ($items as $key => $item) { ?>
																	<option value="<?php echo $item->id; ?>" data-item-price="<?php echo $item->selling_price; ?>"><?php echo $item->pdt_code.' / '.$item->pdt_name; ?></option>
																<?php } ?>
															</select>
														</label>
                                                    </section>

                                                    <section class="col col-4">
                                                        <label class="label">Selling Price</label>
                                                        <label class="input"> <i class="icon-append fa fa-usd  "></i>
                                                            <input type="text" name="oprice[0]" class="oprice" maxlength="150" tabindex="10" readonly="" />
                                                        </label>
                                                    </section>
													
                                                    <section class="col col-3">
                                                        <label class="label">&nbsp;</label>
                                                        <label class="input">
                                                            <i class="fa fa-2x fa-plus-circle add-row"></i>
                                                        </label>
                                                    </section>
                                                </div>
                                            </div>   
                                        </fieldset>	

                                        <fieldset style="border:0; padding-top: 0;">		
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Remarks </label>
                                                    <label class="textarea"><textarea rows="3" name="remarks" id="remarks" maxlength="500"></textarea> </label>
                                                    <!--<div class="note">
    <strong>Note:</strong> expands on focus.
</div>-->
                                                </section> 

                                                <section class="col col-6">
                                                    <label class="label">Status</label>
                                                    <label class="select">
                                                        <select name="status" class="select2" id="status">
                                                            <option value="1">Active</option>
                                                            <option value="2">Inactive</option>
                                                        </select>
                                                    </label>
                                                </section>  
                                            </div>

                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/buy_one_get_one.js"></script>
