<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>service_partner/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="edit_form" action="<?php echo $form_action; ?>" method="post" id="service_partner_form" name="service_partner_form" enctype="multipart/form-data">
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">

                                        <fieldset>                                      
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>
                                            <div class="row">  
                                                <input type="hidden" class="form-control service_partner_id" name="service_partner_id" value="<?php echo $service_partner->id; ?>" />

                                                <section class="col col-6">
                                                    <label class="label">Service Partner Name <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-user-md"></i>
                                                        <input type="text" data-placeholder="XYZ"  name="service_partner_name" id="service_partner_name" maxlength="50" value="<?php echo $service_partner->name; ?>">
                                                    </label>
                                                </section>

                                                <section class="col col-6">
                                                    <label class="label">Address <span style="color: red"></span></label>
                                                    <label class="textarea textarea-expandable"><textarea rows="3" name="address" id="address" maxlength="500"><?php echo $service_partner->address; ?></textarea> </label> <div class="note">
                                                        <strong>Note:</strong> expands on focus.
                                                    </div>
                                                </section> 

                                            </div>
                                            <div class="row">                                       
                                                <section class="col col-6">
                                                    <label class="label">Phone No <span style="color: red"></span></label>
                                                    <label class="input"><i class="icon-append fa fa-phone"></i>
                                                        <input type="text" data-placeholder="9898989898"  name="phone_no" id="phone_no" maxlength="15" value="<?php echo $service_partner->phone_no; ?>">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Fax</label>
                                                    <label class="input"><i class="icon-append fa fa-fax"></i>
                                                        <input type="text" data-placeholder="99999999"  name="fax" id="fax" maxlength="15" value="<?php echo $service_partner->fax; ?>">
                                                    </label>
                                                </section>

                                            </div>
                                            <div class="row"> 
                                                <section class="col col-6">
                                                    <label class="label">Contact Person <span style="color: red"></span></label>
                                                    <label class="input"><i class="icon-append fa fa-user-md"></i>
                                                        <input type="text" data-placeholder="XYZ"  name="name" id="name" maxlength="50" value="<?php echo $service_partner->contact_name; ?>">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Email <span style="color: red"></span></label>
                                                    <label class="input"><i class="icon-append fa fa-envelope-o"></i>
                                                        <input type="text" data-placeholder="abc@info.com"  name="email" id="email" maxlength="70" value="<?php echo $service_partner->email_id; ?>">
                                                    </label>
                                                </section>     
                                            </div>

                                            <div class="row">  
                                              <!--<section class="col col-6">
                                              <label class="label">Website </label>
                                              <label class="input"><i class="icon-append fa fa-globe"></i>
                                                  <input type="text" data-placeholder="http://www.example.com"  name="website" id="website" maxlength="150" value="<?php echo $service_partner->website; ?>">
                                              </label>
                                          </section>-->
                                                <section class="col col-6">
                                                    <label class="label">Payment Term <span style="color: red"></span></label>
                                                    <label class="select">
                                                        <select name="payment_term" id="payment_term" class="select2" >
                                                            <option value="">Select Payment Term</option>
                                                            <option value="1" <?php echo ($service_partner->payment_terms == 1) ? 'selected' : ''; ?>>COD</option>
                                                            <option value="2" <?php echo ($service_partner->payment_terms == 2) ? 'selected' : ''; ?>>14-Days</option>
                                                            <option value="3" <?php echo ($service_partner->payment_terms == 3) ? 'selected' : ''; ?>>30-Days</option>
                                                            <option value="4" <?php echo ($service_partner->payment_terms == 4) ? 'selected' : ''; ?>>45-Days</option>
                                                            <option value="5" <?php echo ($service_partner->payment_terms == 5) ? 'selected' : ''; ?>>Others</option>             
                                                        </select>  </label>
                                                </section>										 
                                                <section class="col col-6">
                                                    <label class="label">Status<span style="color: red"></span></label>
                                                    <label class="select">
                                                        <select name="status" id="status" class="select2" >
                                                            <option value="1" <?php echo ($service_partner->status_str == 'Active') ? 'selected' : ''; ?>>Active</option>
                                                            <option value="2"<?php echo ($service_partner->status_str == 'Inactive') ? 'selected' : ''; ?>>Inactive</option>
                                                        </select>  </label>
                                                </section> 
                                            </div>
                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/service_partner.js"></script>
