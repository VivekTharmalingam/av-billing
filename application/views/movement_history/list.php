<script type="text/javascript">
    window.localStorage.clear();
</script>
<!-- Content Wrapper. Contains page content -->
<div id="main" role="main">
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->
        <?php echo breadcrumb_links(); ?>
        <!-- end breadcrumb -->
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">&nbsp;</div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8"></div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="widget-body">
                        <!-- Widget ID (each widget will need unique ID)-->                                
                        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false"	>

                            <header><span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                <h2>Movement History</h2></header>

                            <div>
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <div class="widget-body no-padding">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                            <tr>
                                                <th data-class="expand">S.No</th>
                                                <th data-hide="phone">Item Code</th>
                                                <th data-hide="phone" class="datatable-column-hidden">Bar Code</th>
                                                <th data-hide="phone" class="datatable-column-hidden">Srl No.</th>
                                                <th >
                                                    <i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Item Name
                                                </th>                                                
                                                <th data-hide="phone">Transaction Type</th>
                                                <th data-hide="phone"> Date</th>
                                                <th data-hide="phone"> Branch Name</th>
                                                <th data-hide="phone,tablet">Order No</th>
                                                <th data-hide="phone,tablet">Qty Before</th>
                                                <th data-hide="phone,tablet">Qty</th>
                                                <th data-hide="phone,tablet">Qty After</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (!empty($transactions)) {
                                                foreach ($transactions as $key => $row) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo ++$key; ?></td>
                                                        <td><?php echo $row->pdt_code; ?></td>
                                                        <td class="datatable-column-hidden"><?php echo $row->bar_code; ?></td>
                                                        <td class="datatable-column-hidden"><?php echo $row->s_no; ?></td>
                                                        <td><?php echo $row->pdt_name; ?></td>
                                                        <td><?php echo $row->transaction_type_str; ?></td>
                                                        <td><?php echo text_date($row->created_on, "N/A", 'd-m-Y h:i A'); ?></td>
                                                        <td><?php echo $row->branch_name; ?> </td>
                                                        <td><?php echo text($row->order_no); ?></td>
                                                        <td><?php echo $row->before_qty; ?></td>
                                                        <td><?php echo $row->quantity; ?></td>
                                                        <td><?php echo $row->after_qty; ?></td>
                                                    </tr>
                                                <?php }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- end widget content -->
                            </div>
                        </div> 
                    </div>
                </article>
                <!-- /.row -->
            </div>
        </section>
    </div>
</div>

<script>
    $(document).ready(function () {
        /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            'iDisplayLength': 25,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            }
        });
        /* END BASIC */
    });
</script>

