<table border="0" cellpadding="3" cellspacing="0" style="font-family:calibri; font-size:14px; width: 1000px;">
    <?php echo $head;?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>
<table width="100%" border="1" style="font-family:calibri; border-collapse:collapse;" cellpadding="3">
	<thead>
		<tr style="font-family:calibri; font-size:16px; font-weight:bold;">
			<th width="10%">S.NO.</th>
			<th width="15%">Received Date</th>
			<th width="32%">Supplier Name</th>
			<th width="15%">Invoice No.</th>
			<th width="14%">Gst Amount</th>
			<th width="14%">Total Amount</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="6" style="border-top: 1px;"></td>
		</tr>
	</tfoot>
	<tbody>
	<?php if (count($goods_receives)) {?>
	<?php $total_gst = 0; $total_amt = 0;?>
	<?php foreach ($goods_receives as $key => $row) {?>
		<?php $total_gst += $row->pr_gst_amount;?>
		<?php $total_amt += $row->pr_net_amount;?>
		<tr>
			<td><?php echo ++ $key; ?></td>
			<td><?php echo text($row->pr_date); ?></td>
			<td><?php echo text($row->supplier_name); ?></td>
			<td><?php echo text($row->supplier_invoice_no); ?></td>
			<td align="right"><?php echo number_format($row->pr_gst_amount, 2); ?></td>
			<td align="right"><?php echo number_format($row->pr_net_amount, 2); ?></td>
		</tr>
	<?php } ?>
	<tr>
		<td colspan="4" align="right" style="border-bottom: 0;">Total</td>
		<td align="right" style="border-bottom: 0;"><?php echo number_format($total_gst, 2);?></td>
		<td align="right" style="border-bottom: 0;"><?php echo number_format($total_amt, 2);?></td>
	</tr>
	<?php } else {?>
		<tr>
			<td colspan="6" align="center">No Record Found</td>
		</tr>
	<?php }?>
</tbody>
</table>