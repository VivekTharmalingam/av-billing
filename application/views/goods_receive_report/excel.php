<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$file_name.xls");
header("Pragma: no-cache");
header("Expires: 0");
#echo "<pre>";print_r($item);echo "</pre>";
#echo "</br> : ".count($item);


?>
<table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; margin-left:10px">
	<tr style="font-family:calibri; font-size:16px; font-weight:bold;">
		<th width="10%">S.NO.</th>
		<th width="15%">Received Date</th>
		<th width="32%">Supplier Name</th>
		<th width="15%">Invoice No.</th>
		<th width="14%">Gst Amount</th>
		<th width="14%">Total Amount</th>
	</tr>
	<tbody>
	<?php if (count($goods_receives)) {?>
	<?php $total_gst = 0; $total_amt = 0;?>
	<?php foreach ($goods_receives as $key => $row) {?>
		<?php $total_gst += $row->pr_gst_amount;?>
		<?php $total_amt += $row->pr_net_amount;?>
		<tr>
			<td><?php echo ++ $key; ?></td>
			<td><?php echo text($row->pr_date); ?></td>
			<td><?php echo text($row->supplier_name); ?></td>
			<td><?php echo text($row->supplier_invoice_no); ?></td>
			<td align="right"><?php echo number_format($row->pr_gst_amount, 2); ?></td>
			<td align="right"><?php echo number_format($row->pr_net_amount, 2); ?></td>
		</tr>
	<?php } ?>
	<tr>
		<td colspan="4" align="right" style="border-bottom: 0;">Total</td>
		<td align="right" style="border-bottom: 0;"><?php echo number_format($total_gst, 2);?></td>
		<td align="right" style="border-bottom: 0;"><?php echo number_format($total_amt, 2);?></td>
	</tr>
	<?php } else {?>
		<tr>
			<td colspan="6" align="center">No Record Found</td>
		</tr>
	<?php }?>
	</tbody>
</table>