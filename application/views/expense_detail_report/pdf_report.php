<?php //print_r($from_date);exit;?>
<table border="0" cellpadding="3" cellspacing="0" style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:14px; width: 1000px;">
    <?php echo $head;?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>
        <td align="right" style="vertical-align: top;">
            <table border="1" style="border-collapse:collapse; width: 250px;">
                <tr>
                    <td align="right" width="50%">From Date</td>
                    <td align="right" width="50%"><?php echo text_date($from_date, '--'); ?></td>
                </tr>
                <tr style="boder-top:0px;">
                    <td align="right" width="50%">To Date</td>
                    <td align="right" width="50%"><?php echo text_date($to_date, '--'); ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>

<table width="100%" border="0" style="border-collapse:collapse; margin-left:10px">
    <tr>
        <td align="left" width="20%">EXPENSE TYPE</td>
        <td align="left" width="5%">:</td>
        <td align="left" width="80%"> <?php 
        foreach($exp_name as $key => $name) {
        $expenses_name = implode (',', $name);  ?><?php echo text($expenses_name.',', '--'); ?><?php } ?></td> 
    </tr>
</table><br>
<table width="100%" border="1" style="border-collapse:collapse; margin-left:10px">
	<tr style="font-family:Times new roman; font-weight:bold;">
		<th width="20%">Expense Date</th>
		<th width="30%">Pay To</th>
		<th width="25%">Amount</th>
		<th width="25%">Expense Type</th>
	</tr>
	<?php if (count($results)) {?>
	<?php foreach($results as $key => $row) {?>                    
		<tr>
			<td style="padding:8px;"><?php echo date('d-m-Y',strtotime($row->expense_date));?></td>
			<td style="padding:8px;"><?php echo text($row->pay_to);?></td>
			<td style="padding:8px; text-align: right;"><?php echo number_format($row->amount, 2)?></td>
			<td style="padding:8px;"><?php foreach($exp_type as $key => $val){ ?>
					<?php if($val->id == $row->exp_type){ echo $val->exp_type_name; } else { echo '';} ?>
				<?php } ?></td>
		</tr>
	<?php }?>
	<?php } else {?>
		<tr>
			<td colspan="4" align="center">No Record Found</td>
		</tr>
	<?php }?>
</table>