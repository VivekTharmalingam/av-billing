<?php
$ti=time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Expense_detail_report".$ti.".xls");
header("Pragma: no-cache");
header("Expires: 0");
#echo "<pre>";print_r($item);echo "</pre>";
#echo "</br> : ".count($item);


?>
<table width="100%" border="1" cellpadding="5" style="border-collapse:collapse;">
	<tr style="background:#3b4e87;font-family:Arial, Helvetica, sans-serif; font-weight:bold;">
		<th width="20%">Expense Date</th>
		<th width="30%">Pay To</th>
		<th width="25%">Amount</th>
		<th width="25%">Expense Type</th>
	</tr>
	<?php if (count($results)) {?>
	<?php foreach($results as $key => $row) {?>
		<tr>
			<td style="padding:8px;"><?php echo date('d-m-Y',strtotime($row->expense_date));?></td>
			<td style="padding:8px;"><?php echo text($row->pay_to);?></td>
			<td style="padding:8px; text-align: right;"><?php echo number_format($row->amount, 2)?></td>
			<td style="padding:8px;"><?php foreach($exp_type as $key => $val){ ?>
					<?php if($val->id == $row->exp_type){ echo $val->exp_type_name; } else { echo '';} ?>
				<?php } ?></td>
		</tr>
	<?php }?>
	<?php }
	else {?>
		<tr>
			<td colspan="4" align="center">No Record Found</td>
		</tr>
	<?php }?>
</table>