<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo base_url(); ?>expense/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                            </span>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="" action="<?php echo $form_action; ?>" method="post" id="expense_form" name="expense_form" enctype="multipart/form-data">

                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">                                   
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>

                                            <div class="row"> 
                                                <section class="col col-6">
                                                    <label class="label">Expense Date <span style="color: red">*</span></label>
                                                    <label class="input">
                                                        <div class="input-prepend input-group">
                                                            <input type="text" name="expense_date" id="expense_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php echo date('d/m/Y'); ?>" />
                                                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        </div>
                                                    </label>
                                                </section>

                                                <section class="col col-6">
                                                    <label class="label">Expense Type<span style="color: red">*</span></label>
                                                    <label class="select">
                                                        <select class="form-control account select2" name="exp_type" id="exp_type">
                                                            <option value="">Select</option>
                                                            <?php foreach ($exp_type as $exp) { ?>
                                                                <option value="<?php echo $exp->id; ?>"><?php echo $exp->exp_type_name; ?></option>
                                                            <?php } ?>
                                                        </select></label>
                                                </section>                                             
                                            </div>

                                            <div class="row"> 
                                                <section class="col col-6">
                                                    <label class="label">Pay to<span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-user"></i>
                                                        <input type="text" data-placeholder=" Daniel Craige" name="pay_to" id="pay_to"  autocomplete="off" ></label>
                                                </section> 

                                                <section class="col col-6 ">
                                                    <label class="label">Amount<span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-money"></i>
                                                        <input type="text" data-placeholder=" 200.00" name="amt" id="amt" class="float_value" autocomplete="off" >
                                                    </label>
                                                </section>
                                            </div>

                                            <div class="row">	
                                                <section class="col col-6">
                                                    <label class="label">Description</label>
                                                    <label class="textarea textarea-expandable"><textarea rows="3" name="desc" id="desc" maxlength="500"></textarea> </label> <div class="note">
                                                        <strong>Note:</strong> expands on focus.
                                                    </div>
                                                </section>

                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Status<span style="color: red">*</span></label>
                                                        <label class="select">
                                                            <select  name="status" class="select2">
                                                                <option value="1">Active</option>
                                                                <option value="2">Inactive</option>
                                                            </select>
                                                        </label>
                                                    </section>
                                                </section>
                                            </div>

                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script>
    var base_url = '<?php echo base_url(); ?>';
    function getExpenseCode() {
        var payment_mode = $('#payment_mode').val();

        if (payment_mode != '') {
            $.ajax({
                type: "POST",
                url: base_url + "expense/getExpenseCode",
                data: {payment_mode: payment_mode},
                dataType: "text",
                success: function (responseData) {
                    $("#unique_expense").val(responseData);

                }
            });
        }
    }
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/expense.js"></script>
