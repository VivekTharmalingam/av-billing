<!-- MAIN PANEL --><?php //error_reporting(0);    ?>
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>payslip/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form class="" action="<?php echo $form_action; ?>" method="post" id="payslip_form" name="payslip_form" enctype="multipart/form-data">

                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">                                   
                                        <fieldset style="margin-bottom: 20px;">
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>

                                            <div class="row">   
                                                <section class="col col-6">
                                                    <label class="label">Employee Name <span style="color: red">*</span></label>
                                                    <label class="select">
                                                        <select name="employee_name" id="employee_name" class="form-control select2" onChange="getEmployeeDetails();">
                                                            <option value="">Select</option>
                                                            <?php foreach ($employee as $key => $val) { ?>
                                                                <option value="<?php echo $val->id; ?>" <?php
                                                                if ($val->id == $payslip->employee_id) {
                                                                    echo 'selected';
                                                                }
                                                                ?> > <?php echo $val->employee_name; ?></option>
                                                                    <?php } ?>
                                                        </select>
                                                    </label>
                                                </section>
												<section class="col col-6">
                                                    <label class="label">Basic Pay<span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa  fa-money"></i>
                                                        <input type="text"  data-placeholder=" 200.00" name="basic_pay" id="basic_pay" autocomplete="off" value="<?php echo $payslip->basic_pay; ?>" />
                                                    </label>
                                                </section>
                                                
                                            </div>

                                            <div class="row">
											    <input type="hidden" value="<?php echo $payslip->id ?>" name="payslip_id" class="payslip_id">
                                                <section class="col col-6">
                                                    <label class="label">Month <span style="color: red">*</span></label>
                                                    <label class="select">
                                                        <select name="month" id="month" onchange="return validate(this);" class="select2">
                                                            <option value="" >Month</option>
                                                            <?php foreach ($month_list as $key => $month) { ?>
                                                                <option value="<?php echo $key; ?>" <?php echo ($payslip->month == $key) ? 'selected' : ''; ?>><?php echo $month; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </label>
                                                </section> 
												<section class="col col-6">
                                                    <label class="label">Over Time Hours <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa  fa-times-circle"></i>
                                                        <input type="text" data-placeholder=" 23:59" name="ot_hour" id="ot_hour" autocomplete="off" onkeyup="getOTamount(); netAmount();" onchange="getOTamount(); netAmount();" value="<?php echo $payslip->overtime_hours; ?>" />
                                                    </label>
                                                </section> 
                                                
                                            </div>

                                            <div class="row">
											    <section class="col col-6">
                                                    <label class="label">Year <span style="color: red">*</span></label>
                                                    <label class="select">
                                                        <select name="year" id="year" class="form-control select2">
                                                            <option value="" >Year</option>
                                                            <option value="" >Year</option>
                                                            <?php for ($i = 2010; $i <= 2030; $i++) { ?>
                                                                <option <?php if ($i == $payslip->year) { ?> selected="selected" <?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </label>
                                                </section>
												<section class="col col-6">
                                                    <label class="label">Over Time Amount per Hour</label>
                                                    <label class="input"><i class="icon-append fa fa-money"></i>
                                                        <input type="text" data-placeholder=" 100.00" name="ot_per_hour" id="ot_per_hour" autocomplete="off" class="float_value" value="<?php echo $payslip->overtime_amount_per_hrs; ?>"/ onkeyup="getOTamount();netAmount();" onchange="getOTamount();netAmount();">
                                                    </label>
                                                </section> 
												
                                                                                           

                                                
                                            </div>

                                            <div class="row"> 
											    <section class="col col-6">
                                                    <label class="label">Total No. Of Working Days <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                        <input type="text" data-placeholder=" 26" name="no_of_workingdays" id="no_of_workingdays" autocomplete="off" maxlength="4" onkeypress="return isNumberKey(event)"  onchange="netAmount();" onkeyup="netAmount();" value="<?php echo $payslip->no_of_workingdays; ?>"/>
                                                    </label>
                                                </section> 
												 <section class="col col-6">
                                                        <label class="label">Over Time Amount<span style="color: red;">*</span></label>
                                                        <label class="input"><i class="icon-append fa  fa-money"></i>
                                                            <input type="text"  name="over_time_amt" id="over_time_amt"   data-placeholder=" 100.00" autocomplete="off" readonly value="<?php echo $payslip->overtime_amount; ?>" class="float_value" onkeyup="getOTamount();
                                                                    netAmount();" onchange="getOTamount();
                                                                            netAmount();"/>
                                                        </label>
                                                    </section>
												
                                                

                                                
                                            </div>

                                            <div class="row">
											    <section class="col col-6">
                                                   <section>
                                                        <label class="label">Employee Worked Days<span style="color: red;">*</span></label>
                                                        <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                            <input type="text"  name="emp_worked_days" id="emp_worked_days"   data-placeholder=" 26" autocomplete="off" onkeypress="return isNumberKey(event)" onchange="netAmount();" onkeyup="netAmount();" value="<?php echo $payslip->emp_worked_days; ?>"/>
                                                        </label>
                                                    </section>
                                                </section>
												<section class="col col-6">
                                                    <label class="label">CPF from employer <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-money"></i>
                                                        <input type="text" name="cpf_employer" id="cpf_employer"   data-placeholder=" 10.00" autocomplete="off" class="float_value" value="<?php echo $payslip->cpf_from_employer; ?>">
                                                    </label>
                                                </section> 
												 <section class="col col-6">
                                                    <label class="label">CPF from employee <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-money"></i>
                                                        <input type="text" data-placeholder=" 10.00" name="cpf_employee" id="cpf_employee" autocomplete="off" class="float_value" value="<?php echo $payslip->cpf_from_employee; ?>" >
                                                    </label>
                                                </section> 
                                                  
                                            </div>

                                            
                                        </fieldset>


                                        <fieldset style="background-color: #d6c49f">                                            
                                            <div class="row">
                                                <section class="col col-4">
                                                    <section class="form">
                                                        <label class="label">Commission</label>
                                                        <label class="input">
                                                            <input type="text" name="commission" id="commission"   data-placeholder=" 100.00" autocomplete="off" onchange="netAmount();" onkeyup="netAmount();" value="<?php echo $payslip->commission; ?>" class="float_value"/>
                                                        </label>
                                                    </section>

<!--                                                    <div class="div_payment" style="display:none">
                                                        <div class="row form">
                                                            <section class="col col-6">
                                                                <label class="label">Payment Mode</label>
                                                                <label class="select">
                                                                    <select class="commission_payment_mode" name="commission_payment_mode" id="commission_payment_mode">
                                                                        <option value="">Select</option>
                                                                        <option value="Cash" <?php echo ($payslip->commission_payment == 'Cash') ? 'selected' : ''; ?>>Cash</option>
                                                                        <option value="Cheque" <?php echo ($payslip->commission_payment == 'Cheque') ? 'selected' : ''; ?>>Cheque</option> 
                                                                    </select>
                                                                </label>
                                                            </section>
                                                        </div>
                                                        <div class="payment_type" style="display:none">
                                                            <div class="row">
                                                                <section class="col col-12">
                                                                    <label class="label">Cheque Number</label>
                                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                                        <input type="text" name="commission_cheque_no" id="commission_cheque_no"  data-placeholder=" 657833435" autocomplete="off" value="<?php echo $payslip->commission_cheque; ?>"/>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-12 cheque_date">
                                                                    <label class="label">Cheque Date</label>
                                                                    <label class="input">
                                                                        <div class="input-prepend input-group">
                                                                            <input type="text" name="commission_cheque_date" id="commission_cheque_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php
                                                                            if ($payslip->commission_cheque_date != '0000-00-00 00:00:00') {
                                                                                echo date('d/m/Y', strtotime($payslip->commission_cheque_date));
                                                                            }
                                                                            ?>" />
                                                                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                                        </div>
                                                                    </label>
                                                                </section>
                                                            </div>	
                                                        </div>	
                                                    </div>-->
                                                </section>

                                                <section class="col col-4">
                                                    <section class="form">
                                                        <label class="label">Allowance</label>
                                                        <label class="input">
                                                            <input  type="text" data-placeholder=" Allowance title"  name="allow_title" id="allow_title" class="allow_title" value="<?php echo $payslip->allowance_title; ?>"style="width: 48%;display: inline-block; float: left;" class="float_value"/>
                                                            <input  type="text" data-placeholder=" Allowance Amount"  name="allow_amt" id="allow_amt" maxlength="30" class=" allow_amt" onchange="allowanceamt();
                                                                    netAmount();" onkeyup="allowanceamt();
                                                                            netAmount();" value="<?php echo $payslip->total_allowance; ?>" onkeypress="return isNumberKey(event)" style="width: 48%;margin-left: 142px;" />
                                                            <input  type="hidden" data-placeholder=" Allowance Amount" id="allowance" name="allowance" value="<?php echo $payslip->total_allowance; ?>"/>
                                                        </label>
                                                    </section>	

<!--                                                    <div class="div_payment" style="display:none">
                                                        <div class="row form">
                                                            <section class="col col-6">
                                                                <label class="label">Payment Mode</label>
                                                                <label class="select">
                                                                    <select class="allowance_payment_mode" name="allowance_payment_mode" id="allowance_payment_mode">
                                                                        <option value="">Select</option>	
                                                                        <option value="Cash" <?php echo ($payslip->allowance_payment == 'Cash') ? 'selected' : ''; ?>>Cash</option>
                                                                        <option value="Cheque" <?php echo ($payslip->allowance_payment == 'Cheque') ? 'selected' : ''; ?>>Cheque</option>
                                                                    </select>
                                                                </label>
                                                            </section>
                                                        </div>	
                                                        <div class="payment_type" style="display:none">
                                                            <div class="row">
                                                                <section class="col col-12">
                                                                    <label class="label">Cheque Number</label>
                                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                                        <input type="text" name="allowance_cheque_no" id="allowance_cheque_no"  data-placeholder=" 657833435" autocomplete="off" value="<?php echo $payslip->allowance_cheque; ?>"/>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-12 cheque_date">
                                                                    <label class="label">Cheque Date</label>
                                                                    <label class="input">
                                                                        <div class="input-prepend input-group">
                                                                            <input type="text" name="allowance_cheque_date" id="allowance_cheque_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php
                                                                            if ($payslip->allowance_cheque_date != '0000-00-00 00:00:00') {
                                                                                echo date('d/m/Y', strtotime($payslip->allowance_cheque_date));
                                                                            }
                                                                            ?>" />
                                                                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                                        </div>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>	-->
                                                </section>

                                                <section class="col col-4">
                                                    <section class="form">
                                                        <label class="label">MBF</label>
                                                        <label class="input">
                                                            <input type="text" name="mbf" id="mbf"  data-placeholder=" 100.00" autocomplete="off" onchange="netAmount();" onkeyup="netAmount();" onkeypress="return isNumberKey(event)" value="<?php echo $payslip->mbf; ?>" class="float_value"/>
                                                        </label>
                                                    </section>	

<!--                                                    <div class="div_payment" style="display:none">
                                                        <div class="row form">
                                                            <section class="col col-6">
                                                                <label class="label">Payment Mode</label>
                                                                <label class="select">
                                                                    <select class="mbf_payment_mode" name="mbf_payment_mode" id="mbf_payment_mode">
                                                                        <option value="">Select</option>
                                                                        <option value="Cash" <?php echo ($payslip->mbf_payment == 'Cash') ? 'selected' : ''; ?>>Cash</option>
                                                                        <option value="Cheque" <?php echo ($payslip->mbf_payment == 'Cheque') ? 'selected' : ''; ?>>Cheque</option>
                                                                    </select>
                                                                </label>
                                                            </section>
                                                        </div>
                                                        <div class="payment_type" style="display:none">
                                                            <div class="row">
                                                                <section class="col col-12">
                                                                    <label class="label">Cheque Number</label>
                                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                                        <input type="text" name="mbf_cheque_no" id="mbf_cheque_no"  data-placeholder=" 657833435" autocomplete="off" value="<?php echo $payslip->mbf_cheque; ?>"/>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-12 cheque_date">
                                                                    <label class="label">Cheque Date </label>
                                                                    <label class="input">
                                                                        <div class="input-prepend input-group">
                                                                            <input type="text" name="mbf_cheque_date" id="mbf_cheque_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php
                                                                            if ($payslip->mbf_cheque_date != '0000-00-00 00:00:00') {
                                                                                echo date('d/m/Y', strtotime($payslip->mbf_cheque_date));
                                                                            }
                                                                            ?>" />
                                                                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                                        </div>
                                                                    </label>
                                                                </section>
                                                            </div>	
                                                        </div>
                                                    </div>-->
                                                </section> 
                                            </div>

                                            <div class="row">
                                                <section class="col col-4">
                                                    <section class="form">	
                                                        <label class="label">Advance / Loan</label>
                                                        <label class="input">
                                                            <input type="text" name="loan" id="loan" class="form-control"  data-placeholder=" 100.00" autocomplete="off" onchange="netAmount();" onkeyup="netAmount();" onkeypress="return isNumberKey(event)" value="<?php echo $payslip->loan; ?>" class="float_value"/>
                                                        </label>
                                                    </section>	

<!--                                                    <div class="div_payment" style="display:none">
                                                        <div class="row form">
                                                            <section class="col col-6">
                                                                <label class="label">Payment Mode</label>
                                                                <label class="select">
                                                                    <select class="loan_payment_mode" name="loan_payment_mode" id="loan_payment_mode">
                                                                        <option value="">Select</option>
                                                                        <option value="Cash" <?php echo ($payslip->advance_payment == 'Cash') ? 'selected' : ''; ?>>Cash</option>
                                                                        <option value="Cheque" <?php echo ($payslip->advance_payment == 'Cheque') ? 'selected' : ''; ?>>Cheque</option>
                                                                    </select>
                                                                </label>
                                                            </section>
                                                        </div>
                                                        <div class="payment_type" style="display:none">
                                                            <div class="row">
                                                                <section class="col col-12">
                                                                    <label class="label">Cheque Number</label>
                                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                                        <input type="text" name="loan_cheque_no" id="loan_cheque_no"  data-placeholder=" 657833435" autocomplete="off" value="<?php echo $payslip->advance_cheque; ?>"/>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-12 cheque_date">
                                                                    <label class="label">Cheque Date</label>
                                                                    <label class="input">
                                                                        <div class="input-prepend input-group">
                                                                            <input type="text" name="loan_cheque_date" id="loan_cheque_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php
                                                                            if ($payslip->advance_cheque_date != '0000-00-00 00:00:00') {
                                                                                echo date('d/m/Y', strtotime($payslip->advance_cheque_date));
                                                                            }
                                                                            ?>" />
                                                                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                                        </div>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>-->
                                                </section>  

                                                <section class="col col-4">
                                                    <section class="form">
                                                        <label class="label">Income Tax</label>
                                                        <label class="input">
                                                            <input type="text" name="income_tax" id="income_tax"   data-placeholder=" 100.00" autocomplete="off" onchange="netAmount();" onkeyup="netAmount();" onkeypress="return isNumberKey(event)" value="<?php echo $payslip->income_tax; ?>" class="float_value"/>
                                                        </label>
                                                    </section>
<!--                                                    <div class="div_payment" style="display:none">
                                                        <div class="row form">
                                                            <section class="col col-6">
                                                                <label class="label">Payment Mode</label>
                                                                <label class="select">
                                                                    <select class="income_payment_mode" name="income_payment_mode" id="income_payment_mode">
                                                                        <option value="">Select</option>	
                                                                        <option value="Cash" <?php echo ($payslip->income_payment == 'Cash') ? 'selected' : ''; ?>>Cash</option>
                                                                        <option value="Cheque" <?php echo ($payslip->income_payment == 'Cheque') ? 'selected' : ''; ?>>Cheque</option>
                                                                    </select>
                                                                </label>
                                                            </section>
                                                        </div>
                                                        <div class="payment_type" style="display:none">
                                                            <div class="row">
                                                                <section class="col col-12">
                                                                    <label class="label">Cheque Number</label>
                                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                                        <input type="text" name="income_cheque_no" id="income_cheque_no"  data-placeholder=" 657833435" autocomplete="off" value="<?php echo $payslip->income_cheque; ?>"/>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-12 cheque_date">
                                                                    <label class="label">Cheque Date </label>
                                                                    <label class="input">
                                                                        <div class="input-prepend input-group">
                                                                            <input type="text" name="income_cheque_date" id="income_cheque_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php
                                                                            if ($payslip->income_cheque_date != '0000-00-00 00:00:00') {
                                                                                echo date('d/m/Y', strtotime($payslip->income_cheque_date));
                                                                            }
                                                                            ?>" />
                                                                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                                        </div>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>-->
                                                </section> 

                                                <section class="col col-4">
                                                    <section class="form">
                                                        <label class="label">Reimbursement</label>
                                                        <label class="input">
                                                            <input type="text" name="reimbursement" id="reimbursement"   data-placeholder=" 100.00" autocomplete="off" value="<?php echo $payslip->reimbursement; ?>" onchange="netAmount();" onkeyup="netAmount();" class="float_value"/>
                                                        </label>
                                                    </section>

<!--                                                    <div class="div_payment" style="display:none">
                                                        <div class="row form">
                                                            <section class="col col-6">
                                                                <label class="label">Payment Mode</label>
                                                                <label class="select">
                                                                    <select class="reimbursement_payment_mode" name="reimbursement_payment_mode" id="reimbursement_payment_mode">
                                                                        <option value="">Select</option>
                                                                        <option value="Cash" <?php echo ($payslip->reimbursement_payment == 'Cash') ? 'selected' : ''; ?>>Cash</option>
                                                                        <option value="Cheque" <?php echo ($payslip->reimbursement_payment == 'Cheque') ? 'selected' : ''; ?>>Cheque</option> 
                                                                    </select>
                                                                </label>
                                                            </section>
                                                        </div>
                                                        <div class="payment_type" style="display:none">
                                                            <div class="row">
                                                                <section class="col col-12">
                                                                    <label class="label">Cheque Number</label>
                                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                                        <input type="text" name="reimbursement_cheque_no" id="reimbursement_cheque_no"  data-placeholder=" 657833435" autocomplete="off" value="<?php echo $payslip->reimbursement_cheque; ?>"/>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-12 cheque_date">
                                                                    <label class="label">Cheque Date </label>
                                                                    <label class="input">
                                                                        <div class="input-prepend input-group">
                                                                            <input type="text" name="reimbursement_cheque_date" id="reimbursement_cheque_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php
                                                                            if ($payslip->reimbursement_cheque_date != '0000-00-00 00:00:00') {
                                                                                echo date('d/m/Y', strtotime($payslip->reimbursement_cheque_date));
                                                                            }
                                                                            ?>" />
                                                                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                                        </div>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>-->
                                                </section>  
                                            </div>			
                                        </fieldset>

                                        <fieldset style="margin-top: 20px;border-top:none;">
                                            <div class="row">
                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Pay Date1<span style="color: red;">*</span></label>
                                                        <label class="input">
                                                            <div class="input-prepend input-group">
                                                                <input type="text" name="pay_date" id="pay_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php
                                                                if ($payslip->pay_date != '0000-00-00') {
                                                                    echo date('d/m/Y', strtotime($payslip->pay_date));
                                                                }
                                                                ?>" />
                                                                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                            </div>
                                                        </label>
                                                    </section>
                                                </section>

                                                <section class="col col-6">
                                                    <label class="label">Payment Mode</label>
                                                    <label class="select">
                                                        <select class="form-control payment_mode select2" name="payment_mode" id="payment_mode">
                                                            <option value="">Select</option>
                                                            <option value="Cash" <?php echo ($payslip->payment_mode == 'Cash') ? 'selected' : ''; ?>>Cash</option>
                                                            <option value="Cheque" <?php echo ($payslip->payment_mode == 'Cheque') ? 'selected' : ''; ?>>Cheque</option>
                                                        </select>
                                                    </label>
                                                    <input type="hidden" name="emp_salary_type" id="emp_salary_type" value="<?= $employee_data_byid->salary_type; ?>" autocomplete="off" readonly/>
                                                </section>										
                                            </div>

                                            <div class="row">
                                                <section class="col col-6 emp_acc_no">
                                                    <label class="label">Employee Account Number <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                        <input type="text" name="emp_acc_no" id="emp_acc_no" data-placeholder=" 657833435" autocomplete="off" readonly value="<?php echo $payslip->employee_acc_no; ?>"/>
                                                    </label>
                                                </section> 

                                                <section class="col col-6 emp_acc_bank">
                                                    <label class="label">Employee Account Bank<span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-bank"></i>
                                                        <input type="text" name="emp_acc_bank" id="emp_acc_bank" data-placeholder=" Bank of America" autocomplete="off" readonly value="<?php echo $payslip->employee_acc_bank; ?>"/>
                                                    </label>
                                                </section>  
                                            </div>

                                            <div class="row">
                                                <section class="col col-6 comp_acc_bank">
                                                    <label class="label">Company Account Bank <span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-bank"></i>
                                                        <input type="text" name="comp_acc_bank" id="comp_acc_bank"  data-placeholder=" Bank of America" autocomplete="off" value="<?php echo $payslip->company_acc_bank; ?>" />
                                                    </label>
                                                </section> 

                                                <section class="col col-6 cheque_no">
                                                    <label class="label">Cheque Number<span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-sort-numeric-asc"></i>
                                                        <input type="text" name="cheque_no" id="cheque_no"  data-placeholder=" 657833435" autocomplete="off" value="<?php echo $payslip->cheque_no; ?>"/>
                                                    </label>
                                                </section>  
                                            </div>

                                            <div class="row">
                                                <section class="col col-6 cheque_date">
                                                    <label class="label">Cheque Date <span style="color: red">*</span></label>
                                                    <label class="input">
                                                        <div class="input-prepend input-group">
                                                            <input type="text" name="cheque_date" id="cheque_date" class="form-control bootstrap-datepicker-comncls" data-placeholder="DD/MM/YYYY" value="<?php
                                                            if ($payslip->cheque_date != '0000-00-00 00:00:00') {
                                                                echo date('d/m/Y', strtotime($payslip->cheque_date));
                                                            }
                                                            ?>" />
                                                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        </div>
                                                    </label>
                                                </section> 

                                                <section class="col col-6">
                                                    <label class="label">Total CPF<span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-money"></i>
                                                        <input type="text" name="total_cpf" id="total_cpf" data-placeholder=" 100.00" autocomplete="off" readonly value="<?php echo $payslip->total_cpf; ?>"/>
                                                    </label>
                                                </section>  
                                            </div>

                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Net Amount<span style="color: red">*</span></label>
                                                    <label class="input"><i class="icon-append fa fa-money"></i>
                                                        <input type="text" name="net_amount" id="net_amount" data-placeholder=" 100.00" autocomplete="off" readonly value="<?php echo $payslip->net_amount; ?>"/>
                                                    </label>
                                                </section>

                                                <section class="col col-6">
                                                    <section>
                                                        <label class="label">Status<span style="color: red">*</span></label>
                                                        <label class="select">
                                                            <select  name="status" class="select2">
                                                                <option value="1" <?php echo ($payslip->status == 1) ? 'selected' : ''; ?>>Paid</option>
                                                                <option value="2"<?php echo ($payslip->status == 2) ? 'selected' : ''; ?>>Unpaid</option>
                                                            </select>
                                                        </label>
                                                    </section>
                                                </section>
                                            </div>                                      
                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/payslip.js"></script>
