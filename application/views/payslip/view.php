<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>payslip/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-file-text-o"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                            <span class="widget-toolbar" rel="tooltip" data-placement="bottom" data-original-title="Print Pdf">
                                <a href="<?php echo base_url(); ?>payslip/payslip_pdf/<?php echo $payslip->id; ?>" style="color:#fff;">
                                    <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                </a>
                            </span>
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body">  
                                <span id="watermark"><?php echo WATER_MARK; ?></span>
                                <table id="user" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                        <tr>
                                            <td style="width:35%;font-weight: bold;">Employee Name</td>
                                            <td style="width:65%"><?php foreach ($employee as $key => $val) { ?>
                                                    <?php if ($val->id == $payslip->employee_id) {
                                                        echo $val->employee_name;
                                                    } else {
                                                        'N/A';
                                                    } ?>
<?php } ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Month</td>
                                            <td><?php echo ($months != '') ? $months : 'N/A'; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Year</td>
                                            <td><?php echo (!empty($payslip->year)) ? $payslip->year : 'N/A'; ?></td>
                                        </tr>
										<tr>
                                            <td style="font-weight: bold;">Total No. Of Working Days</td>
                                            <td><?php echo (!empty($payslip->no_of_workingdays)) ? $payslip->no_of_workingdays : 'N/A'; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Employee Worked Days</td>
                                            <td><?php echo (!empty($payslip->emp_worked_days)) ? $payslip->emp_worked_days : 'N/A'; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Basic Pay</td>
                                            <td><?php echo (!empty($payslip->basic_pay)) ? $payslip->basic_pay : '0.00'; ?></td>
                                        </tr>
										<tr>
                                            <td style="font-weight: bold;">Over Time Hours</td>
                                            <td><?php echo (!empty($payslip->overtime_hours)) ? $payslip->overtime_hours : 'N/A'; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Over Time Amount per Hour</td>
                                            <td><?php echo (!empty($payslip->overtime_amount_per_hrs)) ? number_format($payslip->overtime_amount_per_hrs, 2) : '0.00'; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Over Time Amount</td>
                                            <td><?php echo (!empty($payslip->overtime_amount)) ? number_format($payslip->overtime_amount, 2) : '0.00'; ?></td>
                                        </tr>
                                        
                                        <tr>
                                            <td style="font-weight: bold;">CPF From Employer</td>
                                            <td><?php echo (!empty($payslip->cpf_from_employer)) ? number_format($payslip->cpf_from_employer, 2) : 'N/A'; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">CPF From Employee</td>
                                            <td><?php echo (!empty($payslip->cpf_from_employee)) ? number_format($payslip->cpf_from_employee, 2) : 'N/A'; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Pay Date</td>
                                            <td><?php echo ($payslip->pay_date != '0000-00-00') ? date('d/m/Y', strtotime($payslip->pay_date)) : 'N/A'; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Payment Mode</td>
                                            <td><?php echo $payslip->payment_mode; ?>   </td>
                                        </tr>

<?php if ($payslip->payment_mode == 'Cheque') { ?>
                                            <tr>
                                                <td style="font-weight: bold;">Employee Account Number</td>
                                                <td><?php echo (!empty($payslip->employee_acc_no)) ? $payslip->employee_acc_no : 'N/A'; ?></td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold;">Employee Account Bank</td>
                                                <td><?php echo (!empty($payslip->employee_acc_bank)) ? $payslip->employee_acc_bank : 'N/A'; ?></td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold;">Company Account Bank</td>
                                                <td><?php echo (!empty($payslip->company_acc_bank)) ? $payslip->company_acc_bank : 'N/A'; ?></td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold;">Cheque Number</td>
                                                <td><?php echo (!empty($payslip->cheque_no)) ? $payslip->cheque_no : 'N/A'; ?></td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold;">Cheque Date</td>
                                                <td><?php echo ($payslip->cheque_date != '0000-00-00 00:00:00') ? date('d/m/Y', strtotime($payslip->cheque_date)) : 'N/A'; ?></td>
                                            </tr>
<?php } ?>

                                        <tr>
                                            <td style="font-weight: bold;">Total CPF</td>
                                            <td><?php echo (!empty($payslip->total_cpf)) ? number_format($payslip->total_cpf, 2) : 'N/A'; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Net Amount</td>
                                            <td><?php echo (!empty($payslip->net_amount)) ? number_format($payslip->net_amount, 2) : 'N/A'; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Status</td>
                                            <td><?php echo text($payslip->status_str); ?></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="text-left">
                                    <span class="onoffswitch-title">
                                        <h3><b style="color: #8e233b;"></b></h3>
                                    </span>
                                </div>

                                <div id="widget-tab-1" class="table-responsive">
                                    <table id="dt_basic_2" class="table table-striped table-bordered table-hover" width="100%" style="margin-bottom: 0px !important; border-collapse: collapse!important;">
                                        <thead>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th data-class="expand" style="font-weight: bold; text-align:center;">Commission</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Title/Allowance</th>                                                   
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">MBF</th> 
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Advance / Loan </th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Income Tax</th>
                                                <th data-hide="phone,tablet" style="font-weight: bold;text-align:center;">Reimbursement</th>
                                            </tr>
                                        <thead>
                                        <tbody>
                                            <tr>
                                                <td align="center">Amount</td>
                                                <td align="right"><?php echo (!empty($payslip->commission)) ? $payslip->commission : 'N/A'; ?></td>
                                                <td><?php echo (!empty($payslip->allowance_title)) ? $payslip->allowance_title : 'N/A'; ?> /
<?php echo (!empty($payslip->total_allowance)) ? $payslip->total_allowance : 'N/A'; ?></td>
                                                <td align="right"><?php echo (!empty($payslip->mbf)) ? $payslip->mbf : 'N/A'; ?></td>
                                                <td align="right"><?php echo (!empty($payslip->loan)) ? $payslip->loan : 'N/A'; ?></td>
                                                <td align="right"><?php echo (!empty($payslip->income_tax)) ? $payslip->income_tax : 'N/A'; ?></td>
                                                <td align="right"><?php echo (!empty($payslip->reimbursement)) ? $payslip->reimbursement : 'N/A'; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
								<br><br><br>
                            </div>                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->