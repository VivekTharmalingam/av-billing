<table border="0" cellpadding="5" cellspacing="0" style="font-family:calibri; font-size:14px; width: 1000px;">
    <?php echo $head; ?>
    <tr>
        <td>
            <?php echo $header; ?>
        </td>        
    </tr>
    <?php if (!empty($product->pdt_code)) {?>
    	<tr>
        	<td width="100%" style="vertical-align:top; padding: 0; text-align: center; font-weight: bold;font-size: 20px;" colspan="2" >SUPPLIERS OF <?php echo $product->pdt_code.' / '.$product->pdt_name; ?></td>
    	</tr>
    <?php }?>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
</table>
<table width="100%" border="1" style="border-collapse:collapse; margin-left:10px; font-family: calibri;" cellpadding="5">
    <thead>
        <tr style="font-size:16px;">
            <th width="6%">S.NO</th>
            <th width="15%">Supplier Name</th>
            <th width="15%">Contact Name</th>                    
            <th width="15%">Phone</th>
            <th width="25%">Email</th>
            <th width="25%">Address</th>
        </tr>
    </thead>
    <tbody>
        <?php if (count($supplier)) { ?>
        <?php  foreach ($supplier as $key => $row) { ?>                   
            <tr>
                <td style="text-align:center"><?php echo ++$key; ?></td>
                <td><?php echo text($row->name); ?></td>
                <td><?php echo text($row->contact_name); ?></td>
                <td><?php echo text($row->phone_no); ?></td>                            
                <td><?php echo text($row->email_id); ?></td>
                <td><?php echo text(nl2br($row->address)); ?></td>               
            </tr>
        <?php } ?>
    <?php } else { ?>
        <tr>
            <td colspan="6" align="center">No Record Found</td>
        </tr>
    <?php } ?>
    </tbody>
</table>