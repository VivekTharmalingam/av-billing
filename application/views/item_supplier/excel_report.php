<?php
$ti = time();
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Item_Wise_Supplier_Report" . $ti . ".xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; margin-left:10px">
    <tr>
        <td width="100%" style="vertical-align:top; padding: 0; text-align: center; font-weight: bold;font-size: 20px;" colspan="6" >SUPPLIERS OF <?php echo $product->pdt_code.' / '.$product->pdt_name; ?></td>
    </tr>
    <tr style="font-family:Times new roman; font-weight:bold;">
        <th width="6%" style="padding:10px">S.NO</th>
        <th width="15%">Supplier Name</th>
        <th width="15%">Contact Name</th>                    
        <th width="15%">Phone</th>
        <th width="25%">Email</th>
        <th width="25%">Address</th>
    </tr>
    <?php if (count($supplier)) { ?>
        <?php foreach ($supplier as $key => $row) { ?>                   
            <tr>
                <td style="text-align:center"><?php echo ++$key; ?></td>
                <td><?php echo text($row->name); ?></td>
                <td><?php echo text($row->contact_name); ?></td>
                <td><?php echo text($row->phone_no); ?></td>                            
                <td><?php echo text($row->email_id); ?></td>
                <td><?php echo text(nl2br($row->address)); ?></td>                
            </tr>
        <?php } ?>
    <?php } else { ?>
        <tr>
            <td colspan="6" align="center">No Record Found</td>
        </tr>
    <?php } ?>
</table>