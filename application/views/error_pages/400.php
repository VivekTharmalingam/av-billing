<div id="main" role="main">
    <div id="content">
        <div id="ribbon">
            <span class="ribbon-button-alignment"> 
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span> 
            </span>

            <!-- breadcrumb -->
            <?php echo breadcrumb_links(); ?>
            <!-- end breadcrumb -->
        </div>
        
        <section class="content-header">
            <h1>Bad Request - 400</h1>
        </section>
        
        <section class="content" style="padding: 60px;">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-danger">
                        <div class="box-body">
                            <p><?php echo (!empty($error_message)) ? $error_message : '';?></p>
                            <h4 style="color: #C63434;"><i class="fa fa-3x fa-exclamation-triangle"></i> Bad Request: Please check the request url.</h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>