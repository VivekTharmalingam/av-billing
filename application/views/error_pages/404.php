<div id="main" role="main">
    <div id="content">
        <div id="ribbon">
            <span class="ribbon-button-alignment"> 
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span> 
            </span>

            <!-- breadcrumb -->
            <?php echo breadcrumb_links(); ?>
            <!-- end breadcrumb -->
        </div>
        
        <section class="content-header">
            <h1>Page Not Found - 404</h1>
        </section>
        
        <section class="content" style="padding: 60px;">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-danger">
                        <div class="box-body">
                            <p><?php echo (!empty($error_message)) ? $error_message : '';?></p>
                            <h4 style="color: #C63434;"><i class="fa fa-3x fa-exclamation-triangle"></i> The page you requested cannot be found. The page you are looking for might have been removed, had its name changed or is temporarily unavailable.</h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>