<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo base_url(); ?>customer_group/" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>Customer Group - View</h2>	
<!--                             <span style=" float: right;padding:7px;" rel="tooltip" data-placement="bottom" data-original-title="Users List"><a href="<?php echo $list_link; ?>" style="color:#333333;"><i class="fa fa-list" aria-hidden="true"></i></a></span>-->
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body"> 
                                <span id="watermark"><?php echo WATER_MARK; ?></span>
                                <table id="user" class="table table-bordered table-striped" style="clear: both;">
                                    <tbody>
                                        <tr>
                                            <td style="width:35%;"><b>Customer Group Code</b></td>
                                            <td style="width:65%;"><?php echo $customer_group->customer_group_code; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;"><b>Customer Group</b></td>
                                            <td style="width:65%;"><?php echo $customer_group->customer_group; ?></td>
                                        </tr> 
                                        <tr>
                                            <td style="width:35%;"><b>Notes</b></td>
                                            <td style="width:65%;"><?php echo text($customer_group->notes); ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Status</b></td>
                                            <td><?php echo ( $customer_group->status_str === 'Active' ) ? 'Active' : 'Inactive'; ?></td>
                                        </tr>
                                    </tbody>
                                </table>                                
                            </div>
                            <!-- end widget div -->

                        </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
