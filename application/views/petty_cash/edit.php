<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo $list_link; ?>" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                            </span>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form id="petty_cash_date" action="<?php echo $form_action; ?>" method="post">
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">                                   
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>

                                            <div class="row">
                                                <section class="col col-6">
													<section>
														<label class="label">Date <span style="color: red">*</span></label>
														<label class="input">
															<i class="icon-append fa fa-calendar"></i>
															<input type="text" name="petty_cash_date" class="form-control bootstrap-datepicker-comncls" value="<?php echo text_date($petty_cash->date); ?>" />
														</label>
													</section>
													<section>
														<label class="label">Notes </label>
														<label class="textarea">
															<textarea rows="3" name="remarks" maxlength="500"><?php echo $petty_cash->remarks; ?></textarea>
														</label>
														<div class="note">
															<strong>Note:</strong> expands on focus.
														</div>
													</section>
                                                </section>
												
                                                <section class="col col-6">
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6">Opening Balance </label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="opening_balance" class="form-control float_value amount" value="<?php echo text_amount($petty_cash->opening_balance); ?>" />
														</label>
													</div>
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6"><i id ="total-sales" class="fa fa-minus-square-o"></i> Total Sales Amount</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="total_sales" class="form-control float_value amount" value="<?php echo text_amount($petty_cash->total_sales); ?>" />
														</label>
													</div>
													<div class="total-sales-content">
														<div class="div-amt">
															<label class="label col-sm-6 col-md-6">Unpaid Amount</label>
															<label class="input col-sm-6 col-md-6">
																<i class="icon-prepend fa fa-dollar"></i>
																<input type="text" name="unpaid_amount" class="form-control float_value amount" value="<?php echo text_amount($petty_cash->unpaid_sale_amt); ?>" />
															</label>
														</div>
														<?php if(!empty($petty_cash_items)) {?>
															<?php foreach($petty_cash_items as $key => $sales_amount) {?>
																<div class="div-amt sales-amt">
																	<input type="hidden" name="item_id[<?php echo $key;?>]" value="<?php echo $sales_amount->id;?>" />
																	<input type="hidden" name="type_name[<?php echo $key;?>]" value="<?php echo $sales_amount->payment_type;?>" class="type_name" />
																	<input type="hidden" class="group_name" name="group_name[<?php echo $key;?>]" value="<?php echo $sales_amount->group_name;?>" />
																	<label class="label col-sm-6 col-md-6"><?php echo $sales_amount->payment_type;?></label>
																	<label class="input col-sm-6 col-md-6">
																		<i class="icon-prepend fa fa-dollar"></i>
																		<input type="text" name="sale_amount[<?php echo $key;?>]" class="form-control float_value amount sale_amount" value="<?php echo text_amount($sales_amount->amount);?>" />
																	</label>
																</div>
															<?php }?>
														<?php }?>
													</div>
													
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6"><i id="prev-sales-amt" class="fa fa-minus-square-o"></i> Previous Day Collection</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="prev_day_inv_amount" class="form-control float_value amount" value="<?php echo text_amount($petty_cash->prev_day_collection); ?>" />
														</label>
													</div>
													<div class="prev-day-sales-content">
														<?php if(!empty($petty_cash_prev_items)) {?>
															<?php foreach($petty_cash_prev_items as $key => $sales_amount) {?>
																<div class="div-amt sales-amt">
																	<input type="hidden" name="prev_item_id[<?php echo $key;?>]" value="<?php echo $sales_amount->id;?>" />
																	<input type="hidden" name="prev_type_name[<?php echo $key;?>]" value="<?php echo $sales_amount->payment_type;?>" class="prev_type_name" />
																	<input type="hidden" class="prev_group_name" name="prev_group_name[<?php echo $key;?>]" value="<?php echo $sales_amount->group_name;?>" />
																	<label class="label col-sm-6 col-md-6"><?php echo $sales_amount->payment_type;?></label>
																	<label class="input col-sm-6 col-md-6">
																		<i class="icon-prepend fa fa-dollar"></i>
																		<input type="text" name="prev_sale_amount[<?php echo $key;?>]" class="form-control float_value amount prev_sale_amount" value="<?php echo text_amount($sales_amount->amount);?>" />
																	</label>
																</div>
															<?php }?>
														<?php }?>
													</div>
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6">Expense Amount</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="expense_amount" class="form-control float_value amount" value="<?php echo text_amount($petty_cash->expense_amt); ?>" />
														</label>
													</div>
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6">Bill Payment</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="bill_payment" class="form-control float_value amount" value="<?php echo text_amount($petty_cash->bill_payment); ?>" />
														</label>
													</div>
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6">Refund Amount</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="refund_amount" class="form-control float_value amount" value="<?php echo text_amount($petty_cash->refund_amt); ?>" />
														</label>
													</div>
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6">Purchase Amount</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="purchase_amount" class="form-control float_value amount" value="<?php echo text_amount($petty_cash->purchase_amt); ?>" />
														</label>
													</div>
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6">Ledger Balance</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="ledger_amount" class="form-control float_value amount" value="<?php echo text_amount($petty_cash->ledger_amt); ?>" />
														</label>
													</div>
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6">Banking</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="banking_amount" class="form-control float_value amount" value="<?php echo text_amount($petty_cash->banking); ?>" />
														</label>
													</div>
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6">Closing Balance</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="closing_balance" class="form-control float_value amount" value="<?php echo text_amount($petty_cash->closing_balance); ?>" />
														</label>
													</div>
												</section>
                                            </div>
                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/petty_cash.js"></script>