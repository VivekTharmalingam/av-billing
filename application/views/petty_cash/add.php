<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if (!empty($success)) { ?>
                    <div class="alert alert-success fade in" >
                        <button class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-thumbs-o-up" style="font-size: 16px;"></i>
                        <strong>Success</strong> <?php echo $success; ?>
                    </div>
                <?php } else if (!empty($error)) { ?>
                    <div class="alert alert-danger alert-dismissable" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-thumbs-o-down" style="font-size: 16px;"></i> <strong>Failed</strong> <?php echo $error; ?>
                    </div>
                <?php } ?>&nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <a href="<?php echo $list_link; ?>" class="large_icon_des"><button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color "><i class="glyphicon glyphicon-list"></i></button></a>

                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">						
                        <header>
                            <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                            <h2><?php echo $page_title; ?></h2>
                            </span>
                        </header>				
                        <!-- widget div-->
                        <div>			
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->				
                            </div>
                            <!-- end widget edit box -->
                            <form id="petty_cash_form" action="<?php echo $form_action; ?>" method="post">
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div class="smart-form">                                   
                                        <fieldset>
                                            <legend style=" font-size: 12px; color: red;">
                                                <span style="color: red">*</span>Fields are mandatory
                                            </legend>

                                            <div class="row">
                                                <section class="col col-6">
													<section>
														<label class="label">Date <span style="color: red">*</span></label>
														<label class="input">
															<i class="icon-append fa fa-calendar"></i>
															<input type="text" name="petty_cash_date" class="form-control bootstrap-datepicker-comncls" value="<?php echo date('d/m/Y'); ?>" />
														</label>
													</section>
													<section>
														<label class="label">Notes </label>
														<label class="textarea">
															<textarea rows="3" name="remarks" maxlength="500"></textarea>
														</label>
														<div class="note">
															<strong>Note:</strong> expands on focus.
														</div>
													</section>
                                                </section>
												
                                                <section class="col col-6">
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6">Opening Balance </label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="opening_balance" class="form-control float_value amount" value="<?php echo text_amount($petty_cash_amount['opening_balance']);?>" />
														</label>
													</div>
													<div class="div-amt">
														<label id ="total-sales" class="label col-sm-6 col-md-6"><i class="fa fa-minus-square-o"></i> Total Sales Amount</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="total_sales" class="form-control float_value amount" value="<?php echo text_amount($petty_cash_amount['total_sales_amount']);?>" />
														</label>
													</div>
													
													<div class="total-sales-content">
														<div class="div-amt unpaid">
															<label class="label col-sm-6 col-md-6">Unpaid Amount</label>
															<label class="input col-sm-6 col-md-6">
																<i class="icon-prepend fa fa-dollar"></i>
																<input type="text" name="unpaid_amount" class="form-control float_value amount" value="<?php echo text_amount($petty_cash_amount['unpaid_invoice_amount']);?>" />
															</label>
														</div>
														<?php if(!empty($petty_cash_amount['sales_payment'])) {?>
															<?php foreach($petty_cash_amount['sales_payment'] as $key => $sales_amount) {?>
																<div class="div-amt sales-amt">
																	<input type="hidden" name="type_name[<?php echo $key;?>]" value="<?php echo $sales_amount->type_name;?>" class="type_name" />
																	<input type="hidden" class="group_name" name="group_name[<?php echo $key;?>]" value="<?php echo $sales_amount->group_name;?>" />
																	<label class="label col-sm-6 col-md-6"><?php echo $sales_amount->type_name;?></label>
																	<label class="input col-sm-6 col-md-6">
																		<i class="icon-prepend fa fa-dollar"></i>
																		<input type="text" name="sale_amount[<?php echo $key;?>]" class="form-control float_value amount sale_amount" value="<?php echo text_amount($sales_amount->total_amt);?>" />
																	</label>
																</div>
															<?php }?>
														<?php }?>
													</div>
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6" style="color: #9c340b;" id="prev-sales-amt"><i class="fa fa-minus-square-o"></i> Previous Day Collection</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="prev_day_inv_amount" class="form-control float_value amount" value="<?php echo text_amount($petty_cash_amount['prev_sales_amount']);?>" />
														</label>
													</div>
													<div class="prev-day-sales-content">
														<!--<div class="div-amt unpaid">
															<label class="label col-sm-6 col-md-6">Unpaid Amount</label>
															<label class="input col-sm-6 col-md-6">
																<i class="icon-prepend fa fa-dollar"></i>
																<input type="text" name="unpaid_amount" class="form-control float_value amount" value="<?php echo text_amount($petty_cash_amount['prev_unpaid_invoice_amount']);?>" />
															</label>
														</div>-->
														<?php if(!empty($petty_cash_amount['prev_day_sales_payment'])) {?>
															<?php foreach($petty_cash_amount['prev_day_sales_payment'] as $key => $sales_amount) {?>
																<div class="div-amt sales-amt">
																	<input type="hidden" name="prev_type_name[<?php echo $key;?>]" value="<?php echo $sales_amount->type_name;?>" class="prev_type_name" />
																	<input type="hidden" class="prev_group_name" name="prev_group_name[<?php echo $key;?>]" value="<?php echo $sales_amount->group_name;?>" />
																	<label class="label col-sm-6 col-md-6" style="color: #9c340b;"><?php echo $sales_amount->type_name;?></label>
																	<label class="input col-sm-6 col-md-6">
																		<i class="icon-prepend fa fa-dollar"></i>
																		<input type="text" name="prev_sale_amount[<?php echo $key;?>]" class="form-control float_value amount prev_sale_amount" value="<?php echo text_amount($sales_amount->total_amt);?>" />
																	</label>
																</div>
															<?php }?>
														<?php }?>
													</div>
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6">Expense Amount</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="expense_amount" class="form-control float_value amount" value="<?php echo text_amount($petty_cash_amount['expense_amount']);?>" />
														</label>
													</div>
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6">Bill Payment</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="bill_payment" class="form-control float_value amount" value="<?php echo text_amount($petty_cash_amount['bill_amount']);?>" />
														</label>
													</div>
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6">Refund Amount</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="refund_amount" class="form-control float_value amount" value="<?php echo text_amount($petty_cash_amount['refund_amount']);?>" />
														</label>
													</div>
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6">Purchase Amount</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="purchase_amount" class="form-control float_value amount" value="<?php echo text_amount($petty_cash_amount['purchase_amount']);?>" />
														</label>
													</div>
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6">Ledger Balance</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="ledger_amount" class="form-control float_value amount" value="0.00" />
														</label>
													</div>
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6">Banking</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="banking_amount" class="form-control float_value amount" value="<?php echo text_amount($petty_cash_amount['banking_amount']);?>" />
														</label>
													</div>
													<div class="div-amt">
														<label class="label col-sm-6 col-md-6">Closing Balance</label>
														<label class="input col-sm-6 col-md-6">
															<i class="icon-prepend fa fa-dollar"></i>
															<input type="text" name="closing_balance" class="form-control float_value amount" value="0.00" />
														</label>
													</div>
												</section>
                                            </div>
                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </footer>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </form> 

                        </div><!-- end widget div -->
                    </div><!-- end widget -->
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/petty_cash.js"></script>