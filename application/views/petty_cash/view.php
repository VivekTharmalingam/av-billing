<!-- MAIN PANEL -->
<div id="main" role="main">    
    <!-- RIBBON -->
    <div id="ribbon">
        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>
        <!-- breadcrumb -->				
        <?php echo breadcrumb_links(); ?>
    </div><!-- END RIBBON -->    
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo $list_link; ?>" class="large_icon_des">
						<button type="button" class="label  btn btn-primary btn-circle btn-xl temp_color ">
							<i class="glyphicon glyphicon-list"></i>
						</button>
					</a>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>Petty Cash View</h2>
                        </header>

                        <!-- widget div-->
                        <div role="content">
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body"> 
                                <span id="watermark"><?php echo WATER_MARK; ?></span>
                                <table id="user" class="table table-bordered table-striped" style="clear: both;">
                                    <tbody>
                                        <tr>
                                            <td style="width:35%;"><b>Date</b></td>
                                            <td style="width:65%;"><?php echo text_date($petty_cash->date); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;"><b>Opening Balance</b></td>
                                            <td style="width:65%;"><?php echo text_amount($petty_cash->opening_balance); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;"><b>Total Sales Amount</b></td>
                                            <td style="width:65%;"><?php echo text_amount($petty_cash->total_sales); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;"><b>Unpaid Amount</b></td>
                                            <td style="width:65%;"><?php echo text_amount($petty_cash->unpaid_sale_amt); ?></td>
                                        </tr>
										<?php if(!empty($petty_cash_items)) {?>
											<?php foreach($petty_cash_items as $key => $sales_amount) {?>
												<tr>
													<td style="width:35%;"><b><?php echo $sales_amount->payment_type;?></b></td>
													<td style="width:65%;"><?php echo text_amount($sales_amount->amount); ?></td>
												</tr>
											<?php }?>
										<?php }?>
                                        <tr>
                                            <td style="width:35%;"><b>Previous Day Collection</b></td>
                                            <td style="width:65%;"><?php echo text_amount($petty_cash->prev_day_collection); ?></td>
                                        </tr>
										<?php if(!empty($petty_cash_prev_items)) {?>
											<?php foreach($petty_cash_prev_items as $key => $sales_amount) {?>
												<tr>
													<td style="width:35%;"><b><?php echo $sales_amount->payment_type;?></b></td>
													<td style="width:65%;"><?php echo text_amount($sales_amount->amount); ?></td>
												</tr>
											<?php }?>
										<?php }?>
                                        <tr>
                                            <td style="width:35%;"><b>Expense Amount</b></td>
                                            <td style="width:65%;"><?php echo text_amount($petty_cash->expense_amt); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;"><b>Bill Payment</b></td>
                                            <td style="width:65%;"><?php echo text_amount($petty_cash->bill_payment); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;"><b>Refund Amount</b></td>
                                            <td style="width:65%;"><?php echo text_amount($petty_cash->refund_amt); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;"><b>Purchase Amount</b></td>
                                            <td style="width:65%;"><?php echo text_amount($petty_cash->purchase_amt); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;"><b>Ledger Balance</b></td>
                                            <td style="width:65%;"><?php echo text_amount($petty_cash->ledger_amt); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;"><b>Banking</b></td>
                                            <td style="width:65%;"><?php echo text_amount($petty_cash->banking); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;"><b>Closing Balance</b></td>
                                            <td style="width:65%;"><?php echo text_amount($petty_cash->closing_balance); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="width:35%;"><b>Notes</b></td>
                                            <td style="width:65%;"><?php echo text($petty_cash->remarks); ?></td>
                                        </tr>
                                    </tbody>
                                </table>                                
                            </div>
                            <!-- end widget div -->
                        </div>
                </article><!-- END COL -->
            </div><!-- END ROW -->				
        </section><!-- end widget grid -->
    </div><!-- END MAIN CONTENT -->
</div><!-- END MAIN PANEL -->
