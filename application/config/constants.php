<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.	 The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */

define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

define('SITE_NAME', 'AV');
define('WATER_MARK', 'AV.');
define('PAGE_TITLE_PREFIX', 'Welcome to AV!'); // Only for login page.
define('NO_IMG', 'assets/images/no-img.jpeg');
define('IMG_DIR', 'assets/images/');
define('DEFAULT_AVATAR', 'assets/images/avatars/male.png');
define('DEFAULT_FAVICON', 'assets/images/favicon.ico');
define('TBL_PREFIX', 'uc_');
define('TIME_ZONE', 'Asia/Singapore'); #'Asia/Singapore'

define('COMP_PH', '+65 62948277'); // Default company phone number
define('COMP_EMAIL', 'info@refulgenceinc.com'); // Default company email
define('COMP_WWW', 'http://refulgenceinc.com'); // Default company website

/* For frent end */
define('PAGINATION_LIMIT', 8);
if (defined('TIME_ZONE')) {
    date_default_timezone_set(TIME_ZONE);
}

/* Upload directory constant */
define('UPLOADS', 'uploads/');
define('DIR_COMPANY_LOGO', UPLOADS);
define('DIR_FAVICON', UPLOADS);
define('PO_EMAIL_PATH', str_replace('\\', '/', FCPATH) . 'tmp/purchase_email/');
define('QUOT_EMAIL_PATH', str_replace('\\', '/', FCPATH) . 'tmp/quotation_email/');

/* SMS */
define('SMS_USER',               'APIXQE9MPSZV7'); // 'API48Z28JBWS3'
define('SMS_PWD',                'APIXQE9MPSZV7XQE9M'); // 'API48Z28JBWS3XMH9Z'
define('SMS_FROM',               'AVS'); // 'TIRU PRO'
define('MOB_PREFIX',             '65');
define('TO_SEND',                FALSE);

/* Notification */
define('NOT_VIEW',               TRUE);

/* Upload Code Prefix constant */
define('CODE_BND', 			'BND_');
define('CODE_COMP', 			'COMP_');
define('CODE_SUP', 			'SUP_');
define('CODE_PDT', 			'ITM_');
define('CODE_EMP', 			'EMP_');
define('CODE_EXP_TYPE', 		'EXPT_');
define('CODE_CUST_GROUP', 		'AVCG_');
define('CODE_LOC', 			'LOC_');
define('CODE_ITEM_CAT', 		'AVIC');

define('CODE_PR', 			'GRN_');
define('CODE_PO', 			'PO_');
define('CODE_PINV', 			'PINV_');
define('CODE_PORET', 			'PORET_');
define('CODE_PAY', 			'PO_PAY_');

define('CODE_SPAY', 			'INV_PAY_');
define('CODE_EXP', 			'EXP_');
define('CODE_SO', 			'INV_');
define('CODE_SORET', 			'INV_RET_');
define('CODE_DO', 			'DO_');
define('CODE_EXR', 			'AVEXO_');
define('CODE_SJS', 			'AVSJS');
define('CODE_EXTSER', 			'AVESR');
define('CODE_ITEM_CODE', 		'AV');

/* Table names */
define('TBL_COMP', 				TBL_PREFIX . 'companies');
define('TBL_DAILY_REPORT', 		TBL_PREFIX . 'daily_report');
define('TBL_USER', 				TBL_PREFIX . 'users');
define('TBL_USER_TEMP', 		TBL_PREFIX . 'user_temp');
define('TBL_BRN',	 			TBL_PREFIX . 'branches');
define('TBL_CUS', 				TBL_PREFIX . 'customer');
define('TBL_SUP', 				TBL_PREFIX . 'supplier');
define('TBL_SERPTN', 			TBL_PREFIX . 'service_partner');
define('TBL_ITM', 				TBL_PREFIX . 'product');
define('TBL_CAT', 				TBL_PREFIX . 'category');
define('TBL_SERSTS', 			TBL_PREFIX . 'service_status');
define('TBL_SRSTLG', 			TBL_PREFIX . 'service_status_log');
define('TBL_WRSTLG', 			TBL_PREFIX . 'warranty_status_log');
define('TBL_PDT', 				TBL_PREFIX . 'product');
define('TBL_EMP', 				TBL_PREFIX . 'employee');
define('TBL_EMP_DOC', 			TBL_PREFIX . 'employee_docs');
define('TBL_EXP_TYPE', 			TBL_PREFIX . 'expense_type');
define('TBL_UNIT', 				TBL_PREFIX . 'unit');

define('TBL_SHIP_ADR', 			TBL_PREFIX . 'ship_address');
define('TBL_PO', 				TBL_PREFIX . 'purchase_orders');
define('TBL_POIT', 				TBL_PREFIX . 'purchase_order_items');
define('TBL_PO_EMAIL', 			TBL_PREFIX . 'purchase_email_sent');
define('TBL_PR', 				TBL_PREFIX . 'purchase_receive');
define('TBL_PRIT', 				TBL_PREFIX . 'purchase_receive_items');
//define('TBL_INV_PDT', TBL_PREFIX . 'inventory_products');
define('TBL_BRN_PDT', 			TBL_PREFIX . 'branch_products');
define('TBL_PCOL', 				TBL_PREFIX . 'product_cost_logs');
define('TBL_SCN_PDT', 			TBL_PREFIX . 'scan_product');
define('TBL_ICT', 				TBL_PREFIX . 'item_category');
define('TBL_SUB_CAT', 			TBL_PREFIX . 'sub_category');

define('TBL_PPM', 				TBL_PREFIX . 'purchase_payments');
define('TBL_SO', 				TBL_PREFIX . 'sales_order');
define('TBL_SPM', 				TBL_PREFIX . 'sales_payment');
define('TBL_SO_ITM', 			TBL_PREFIX . 'sales_order_item');
define('TBL_QUO', 				TBL_PREFIX . 'quotation');
define('TBL_QUO_ITM', 			TBL_PREFIX . 'quotation_item');
define('TBL_QUO_EMAIL', 		TBL_PREFIX . 'quotation_email_sent');
define('TBL_SO_RET_ITM', 		TBL_PREFIX . 'sales_return_items');
define('TBL_SO_RET', 			TBL_PREFIX . 'sales_return');
define('TBL_DO', 				TBL_PREFIX . 'delivery_order');
define('TBL_DO_ITM', 			TBL_PREFIX . 'delivery_order_item');

define('TBL_INVENTORY_ADJUST', 	TBL_PREFIX . 'inventory_adjustment');
define('TBL_PORET', 			TBL_PREFIX . 'purchase_return');
define('TBL_PORETI', 			TBL_PREFIX . 'purchase_return_items');
define('TBL_TRANS_STOCK', 		TBL_PREFIX . 'transfer_stock');
define('TBL_TRANS_STOCK_GRP', 	TBL_PREFIX . 'transfer_stock_group');

define('TBL_BANK',  			TBL_PREFIX . 'bank');
define('TBL_DEPS',  			TBL_PREFIX . 'deposit');
define('TBL_DEDC',  			TBL_PREFIX . 'deduction');
define('TBL_PAYM',  			TBL_PREFIX . 'payment');
define('TBL_CUST_GROUP', 		TBL_PREFIX . 'customer_group');

define('TBL_EXR',   			TBL_PREFIX . 'exchange_order');
define('TBL_EXRIT', 			TBL_PREFIX . 'exchange_order_items');
define('TBL_EXRPM', 			TBL_PREFIX . 'exchange_order_payments');

define('TBL_SCAT',      		TBL_PREFIX . 'service_category');
define('TBL_SITM',      		TBL_PREFIX . 'service_item');
define('TBL_SJST',      		TBL_PREFIX . 'service_jobsheet');
define('TBL_SJSTITM',   		TBL_PREFIX . 'service_jobsheet_items');
define('TBL_SEXINITM',  		TBL_PREFIX . 'service_exc_inventory_item');
define('TBL_SEREXTITM', 		TBL_PREFIX . 'service_external_item');
define('TBL_EXTSERPAY', 		TBL_PREFIX . 'external_service_payments');

define('TBL_SO_HLD',        	TBL_PREFIX . 'sales_order_hold');
define('TBL_SO_ITM_HLD',    	TBL_PREFIX . 'sales_order_item_hold');
define('TBL_EXTSER',        	TBL_PREFIX . 'external_service');
define('TBL_NOTIFICATION',  	TBL_PREFIX . 'notifications');

/* PROMOTIONS TABLES */
define('TBL_PWP', 				TBL_PREFIX . 'purchase_with_purchase');
define('TBL_PWP_ITEMS', 		TBL_PREFIX . 'purchase_with_purchase_items');
define('TBL_BOGO', 				TBL_PREFIX . 'buy_one_get_one');
define('TBL_BOGO_ITEMS', 		TBL_PREFIX . 'buy_one_get_one_items');
define('TBL_GROUP_BUY', 		TBL_PREFIX . 'group_buy');
define('TBL_GROUP_BUY_ITEMS', 	TBL_PREFIX . 'group_buy_items');
define('TBL_DISC_SALE', 		TBL_PREFIX . 'discount_sale');

define('TBL_PAY', 				TBL_PREFIX . 'payslip');
define('TBL_EXP', 				TBL_PREFIX . 'expense');
define('TBL_PETTY_CASH', 		TBL_PREFIX . 'petty_cash');
define('TBL_PETTY_CASH_ITEMS', 	TBL_PREFIX . 'petty_cash_items');
define('TBL_ENQ_LOG_FRM', 		TBL_PREFIX . 'enquiry_log_form');
define('TBL_LOG_FRM', 			TBL_PREFIX . 'log_form');

define('TBL_PAYMENT_TYPE', 		TBL_PREFIX . 'payment_type');
define('TBL_PAYMENT_MODE', 		TBL_PREFIX . 'payment_mode');
define('TBL_BANK_TRANSFER', 	TBL_PREFIX . 'bank_transfer');
define('TBL_ACT_LOG', 			TBL_PREFIX . 'activity_log');
define('PER_PAGE', 200);
define('MAX_EXPORT_CLM', 7);  // Disable: -1

/* BACKUP FILE PATH */
define('BACKUP_FILE_PATH', 		FCPATH . 'backup/');
/* End of file constants.php */
/* Location: ./application/config/constants.php */