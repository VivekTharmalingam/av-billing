<?php
class REF_Controller extends CI_Controller {
    public function __construct() {
        // To Check session already started or not
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }

        parent::__construct();
        /* This user_data and ip_address will be accessed in all controllers*/
        $this->user_data = get_user_data();
        $this->company_data = get_company_data();
        
        $this->ip_address = $_SERVER['REMOTE_ADDR'];
        
        /* Data initialization for render function */
        $this->data = array();
        $this->data['success'] = '';
        $this->data['error'] = '';
        if (!empty($_SESSION['success'])) {
            $this->data['success'] = $_SESSION['success'];
            unset($_SESSION['success']);
        }
        else if (!empty($_SESSION['error'])) {
            $this->data['error'] = $_SESSION['error'];
            unset($_SESSION['error']);
        }
        //$router =& 
        //
        //load_class('Router', 'core');
        //$router->fetch_class();
        // $controler_name = $router->fetch_class();
        //$method_name = $router->fetch_method();
        $current_url =& get_instance(); //  get a reference to CodeIgniter
        $this->log_controler_name = $controler_name = $current_url->router->fetch_class(); // for Class name or controller
        $this->log_method_name = $method_name = $current_url->router->fetch_method(); // for method name

//        $this->insert_log($controler_name, $method_name);
        
        $actions = $this->actions();
        $this->data['list_link']    = $actions['index'];
        $this->data['add_link']     = $actions['add'] . '/';
        $this->data['edit_link']    = $actions['edit'] . '/';
        $this->data['view_link']    = $actions['view'] . '/';
        $this->data['delete_link']  = $actions['delete'] . '/';
        $this->data['add_action']   = $actions['insert'] . '/';
        $this->data['edit_action']  = $actions['update'] . '/';
        
        $this->config->set_item('css_url', BASE_URL . 'assets/css/');
        $this->config->set_item('js_url', BASE_URL . 'assets/js/');
        $this->config->set_item('images_url', BASE_URL . 'assets/images/');
        $this->config->set_item('upload_url', BASE_URL . UPLOADS);
        
        $user_data  = get_user_data();
        #$this->user_data = $user_data;
        $this->permissions = $user_data['permissions'];
        $this->permission = (!empty($user_data['permission'])) ? $user_data['permission'] : array();
        check_permission();
    }
    
    public function insert_log($controller, $method, $view_method, $view_id, $ref_number=NULL, $is_deleted=NULL) {        
            $insert = array(
                'controller_name' => $controller,
                'method_name' => $method,
                'view_method_name' => $view_method,
                'view_id' => $view_id,
                'ref_number_txt' => $ref_number,
                'is_deleted' => $is_deleted,
                'branch_id' => $this->user_data['branch_id'],
                'created_by' => $this->user_data['user_id'],
                'created_on' => date('Y-m-d H:i:s')
            );
         $this->user_model->insert_log($insert);
         
         if($is_deleted == 1) {
             $updatetxt = array(
                'is_deleted' => $is_deleted
             );
             $this->user_model->update_logtxt($updatetxt,$controller,$view_id);
         }
    }
    
    public function render($data, $view_file = '') {
        $data = (!empty($data)) ? $data : array();
        $header     = get_header_data();
        $header = (!empty($header)) ? $header : array();
        #echo '<pre>';
        $header['permissions'] = $this->permissions;
        $header['permission'] = $this->permission;
        //$header['page_title'] = $user_data['page_title'];
        $header['view_file'] = (!empty($view_file)) ? $view_file : 'home';
        $header['user_data'] = $this->user_data;
        $header['company_data'] = $this->company_data;
        $header['left_menu'] = 1;
        #print_r($header);
        // To set custom header datas from controllers
        if (!empty($data['header'])) {
            foreach($data['header'] as $key => $value) {
                $header[$key] = $value;
            }
        }
        
        $head = array();
        $data['permissiona'] = array();
        $data['permission']     = $header['permission'];
        $data['user_data']      = $head['user_data'] = $this->user_data;
        $head['company_data']  = $data['company_data'] = $this->company_data;
        $head['page_title']     = $header['page_title'];
        if (!empty($header['permissions'][$header['current_controller']])) {
            $data['permission'] = $header['permissions'][$header['current_controller']];
        }
        
        $footer = array();
        $footer['branches'] = $this->company_model->get_branch_name_by_ids($this->user_data['branch_ids']);
        $footer['user_data'] = $this->user_data;
        $footer['switch_branch'] = FALSE;
        if (!empty($_SESSION['switch_branch'])) {
            $footer['switch_branch'] = $_SESSION['switch_branch'];
            unset($_SESSION['switch_branch']);
        }
        
        // Load Views
        $this->load->view('templates/head', $head);
        $this->load->view('templates/header', $header);
        $this->load->view($view_file, $data);
        $this->load->view('templates/footer', $footer);
    }
    
    protected function actions($controller = '') {
        $action = array();
        $controller = (empty($controller)) ? $this->router->fetch_class() : $controller;
        $action['index']  = base_url() . $controller . '/';
        $action['view']   = base_url() . $controller . '/view';
        $action['add']    = base_url() . $controller . '/add';
        $action['insert'] = base_url() . $controller . '/insert';
        $action['edit']   = base_url() . $controller . '/edit';
        $action['update'] = base_url() . $controller . '/update';
        $action['delete'] = base_url() . $controller . '/delete';
        
        $action['res_insert'] = base_url() . $controller . '/res_insert';
        $action['res_view'] = base_url() . $controller . '/res_view';
        $action['res_edit'] = base_url() . $controller . '/res_edit';
        $action['res_update'] = base_url() . $controller . '/res_update';
        $action['res_delete'] = base_url() . $controller . '/res_delete';
        
        return $action;
    }

    protected function num_digit_generation($id, $no_of_digit = 3) {  
        $no_of_digit = ($no_of_digit > 3)? $no_of_digit : 3;
        $nine_txt ='';
        for($i = 0; $i < $no_of_digit; $i++) {
            $nine_txt .= '9';
        }
        return ($id > (int)$nine_txt) ? $id : sprintf('%0'.$no_of_digit.'d', $id);
    }
    
    protected function auto_generation_code($tbl, $prefix='', $suffix='', $no_of_digit = 3, $edit_id='', $branch_id = 0) {
		$code = '';
        if($tbl != '') {
            $nxt_aut_id  = $this->user_model->next_auto_id($tbl, $branch_id, $prefix, $suffix);
            if($edit_id != '') 
                $nxt_aut_id = $edit_id;    
            
            $nxt_autid_digit  = $this->num_digit_generation($nxt_aut_id, $no_of_digit);
            $code = $prefix . $nxt_autid_digit . $suffix;
        }
        return $code;
    }
    
    protected function auto_generation_code_item($tbl, $prefix='', $suffix='', $no_of_digit = 3, $edit_id='') {
		$code = '';
        if($tbl != '') {
            $nxt_aut_id  = $this->user_model->next_auto_item_id($tbl, $prefix, $suffix);
            
            if($edit_id != '') 
                $nxt_aut_id = $edit_id;    
            
            $nxt_autid_digit  = $this->num_digit_generation($nxt_aut_id, $no_of_digit);
            $code = $prefix.$nxt_autid_digit.$suffix;
        }
        return $code;
    }
	
    public function calculate_inclusive_gst($amount) {
        return ($amount / 1.07) * 0.07;
    }
	
    protected function ajax_response($data) {
        echo json_encode($data, JSON_UNESCAPED_SLASHES);
        exit(0);
    }
}
?>