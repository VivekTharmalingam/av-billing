<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Invoice_Payment extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('sales_payment_model', '', TRUE);
        $this->load->model('sales_order_model', '', TRUE);
        $this->load->model('payment_type_model', '', TRUE);
        $this->load->model('bank_model', '', TRUE);
        $this->load->model('customer_model', '', TRUE);
        $this->load->library('Datatables');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        $data['sales_payment'] = $this->sales_payment_model->list_all();
        $this->render($data, 'sales_payment/list');
    }
    
    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('pay.id AS pId,so.so_no AS inv,pay.pay_no AS pay,DATE_FORMAT(pay.pay_date, "%d/%m/%Y") as date,so.your_reference AS ref,SUM(pay.pay_amt) as paid_amount,IF(payt.is_cheque = 1 AND pay.pay_pay_no != "", CONCAT(payt.type_name, "&nbsp;/ ", pay.pay_pay_bank, "&nbsp;- ", pay.pay_pay_no), payt.type_name) as type_name,CASE pay.payment_status WHEN 1 THEN "Paid" WHEN 2 THEN "Not Paid" WHEN 3 THEN "Cancelled" ELSE "" END as status_str, CASE so.so_type WHEN 1 THEN c.name ELSE so.customer_name END as name', FALSE)
                ->where('pay.status != 10 AND pay.brn_id IN ("' . $this->user_data['branch_id'] . '")')
                ->join(TBL_SO . ' as so', 'so.id = pay.so_id')
                ->join(TBL_PAYMENT_TYPE . ' as payt', 'payt.id = pay.pay_pay_type', 'left')
                ->join(TBL_CUS . ' as c', 'c.id = so.customer_id', 'left')
                ->group_by('pay.id')
                ->add_column('Actions', $this->get_buttons('$1'), 'pId')
                ->from(TBL_SPM . ' AS pay');
        echo $this->datatables->generate();
    }

    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
//        if (in_array(3, $this->permission)) {
//            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
//        }
        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Invoice Payment!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Invoice Payment"><span>Delete</span></a>';
        }
        $html.='</span>';
        return $html;
    }

    public function add($id = '') {
        $data = array();
        $data['payment_id'] = '';
        if ($id != '') {
            $data['payment_id'] = $id;
        }
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];

        $actions = $this->actions();
        $data['form_action'] = $actions['insert'];
        $data['list_link'] = $actions['index'];
        $bnk_acc_name = $this->user_model->get_active_acc($this->user_data['bank_account_id']);
        $data['bnk_acc_name'] = (count($bnk_acc_name)>0) ? $bnk_acc_name[0]['bank_name'] : '';
        $data['so'] = $this->sales_payment_model->list_sales_order_no();
	$data['customers'] = $this->customer_model->list_active();	
        $data['payment_types'] = $this->payment_type_model->list_active();
        $data['payment_id'] = $id;
        $this->render($data, 'sales_payment/add');
    }
    
    public function unpaid_invice() {
        $customer_id = $this->input->post('customer_id');
        $data = array();
        $data['invoices'] = (!empty($customer_id)) ? $this->sales_payment_model->get_unpaid_invice_by_customer($customer_id) : array();
        $this->ajax_response($data);
    }

    public function insert() {
        $user_data = get_user_data();
//        $short_name = strtoupper(substr($this->input->post(''), 0, 3));
        $prefix_wt_sub = CODE_SPAY;
        
        $sales_payment = array(
            'pay_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('pay_date')))),
            'pay_pay_type' => $this->input->post('payment_type'),
            'pay_pay_no' => '',
            'pay_pay_bank' => '',
            'pay_pay_date' => NULL,
            'pay_dd_no' => '',
            'pay_dd_date' => NULL,
            'payment_status' => 1,
            'status' => 1,
            'bank_acc_id' => $this->user_data['bank_account_id'],
            'brn_id' => $user_data['branch_id'],
            'created_by' => $user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s'),
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        
        $payment_type = $this->payment_type_model->get_by_id($this->input->post('payment_type'));
        if ($payment_type->group_name == 3) {
                $_SESSION['error'] = 'Invoice Payment not added due to the Payment type CREDIT!..';
                redirect('invoice_payment/add', 'refresh');
        }
		
        if ($payment_type->is_cheque == 1) {
            $sales_payment['pay_pay_no'] = $this->input->post('cheque_acc_no');
            $sales_payment['pay_pay_bank'] = $this->input->post('cheque_bank_name');
            $sales_payment['pay_pay_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('cheque_date'))));
        }
        /*else {
            $sales_payment['pay_pay_no'] = '';
            $sales_payment['pay_pay_bank'] = '';
            $sales_payment['pay_pay_date'] = NULL;
        }
        if ($this->input->post('payment_type') == '3') {
            $sales_payment['pay_dd_no'] = $this->input->post('dd_no');
            $sales_payment['pay_dd_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('dd_date'))));
        } else {
            $sales_payment['pay_dd_no'] = '';
            $sales_payment['pay_dd_date'] = NULL;
        }*/
        
        $grn_no = $this->input->post('grn_no');
        $credit_notes = $this->input->post('credit_notes');
        $amount = $this->input->post('amount');
        $paid_amount = $this->input->post('paid_amount');
        $already_paid = $this->input->post('already_paid');
        $affected_rows = 0;
        foreach($grn_no as $key => $value) {
                $sales_payment_code = $this->auto_generation_code(TBL_SPM, $prefix_wt_sub, '', '', '');
                $sales_payment['pay_no'] = $sales_payment_code;
                $sales_payment['so_id'] = $value;
                $sales_payment['pay_amt'] = $paid_amount[$key];
                $salesorderdata = $this->sales_order_model->get_by_id($value);
                $last_id=$this->sales_payment_model->insert($sales_payment);
                if ($last_id > 0) {
                    $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, $salesorderdata->so_no, '');
			// Paid and Cash group payment will be added in petty cash
                    if (!in_array($payment_type->group_name, array(1, 3))) {
                        $update_data = array(
                            'updated_by' => $this->user_data['user_id'],
                            'updated_on' => date('Y-m-d H:i:s')
                        );
                        $this->bank_model->update_bank_amount($this->user_data['bank_account_id'],$update_data,$paid_amount[$key], '+');
                    }

                    $status = array();
                    $status['payment_status'] = 1;
                    $this->sales_payment_model->update_status($value, $status);
                    $affected_rows ++;
                }
        }
        

        if ($affected_rows > 0) {
            $_SESSION['success'] = 'Invoice Payment has been added!..';
        }
        else {
            $_SESSION['error'] = 'Invoice Payment not added!..';
        }
        redirect('invoice_payment/add', 'refresh');
    }

    public function edit($id = '') {
        if (empty($id)) {
            show_400_error();
        }

        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];        
        $data['sp_details'] = $this->sales_payment_model->get_by_id($id);
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' .$id;
        $data['list_link'] = $actions['index'];
        $data['payment_types'] = $this->payment_type_model->list_active();
        $this->render($data, 'sales_payment/edit');
    }

    public function update($id = '') {
        //echo '<pre>';print_r($_REQUEST);die();
        if (empty($id)) {
            show_400_error();
        }
        $user_data = get_user_data();
//        $short_name = strtoupper(substr($this->input->post(''), 0, 3));
        $prefix_wt_sub = CODE_SPAY;
        $sales_payment_code = $this->auto_generation_code(TBL_SPM, $prefix_wt_sub, '', 3, $id);
        $so = $this->input->post('hidden_so');
        $sales_payment = array();
        #$sales_payment['pay_no'] = $sales_payment_code;
        #$sales_payment['so_id'] = $so;
        $sales_payment['pay_amt'] = ($this->input->post('payment_status') == 1) ? $this->input->post('total_amnt') : 0;
        $sales_payment['pay_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('pay_date'))));
        $sales_payment['pay_pay_type'] = $this->input->post('payment_type');
        $sales_payment['pay_pay_no'] = '';
        $sales_payment['pay_pay_bank'] = '';
        $sales_payment['pay_pay_date'] = NULL;
        $sales_payment['pay_dd_no'] = '';
        $sales_payment['pay_dd_date'] = NULL;
        $sales_payment['status'] = 1;
        $sales_payment['payment_status'] = $this->input->post('payment_status');
        $sales_payment['bank_acc_id'] = $this->user_data['bank_account_id'];
        $sales_payment['brn_id'] = $user_data['branch_id'];
        $sales_payment['updated_by'] = $user_data['user_id'];
        $sales_payment['updated_on'] = date('Y-m-d H:i:s');
        $sales_payment['ip_address'] = $_SERVER['REMOTE_ADDR'];
            
        $payment_type = $this->payment_type_model->get_by_id($this->input->post('payment_type'));
        if ($payment_type->is_cheque == 1) {
            $sales_payment['pay_pay_no'] = $this->input->post('cheque_acc_no');
            $sales_payment['pay_pay_bank'] = $this->input->post('cheque_bank_name');
            $sales_payment['pay_pay_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('cheque_date'))));
        }/* else {
            $sales_payment['pay_pay_no'] = '';
            $sales_payment['pay_pay_bank'] = '';
            $sales_payment['pay_pay_date'] = NULL;
        }
        if ($this->input->post('payment_type') == '3'  && $this->input->post('payment_status')=='1') {
            $sales_payment['pay_dd_no'] = $this->input->post('dd_no');
            $sales_payment['pay_dd_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('dd_date'))));
        } else {
            $sales_payment['pay_dd_no'] = '';
            $sales_payment['pay_dd_date'] = NULL;
        }*/

        $sales_payment_details = $this->sales_payment_model->get_sales_payment_details($id);
        
        if ($this->sales_payment_model->update($id,$sales_payment) > 0) {
            $total_amt_txt = ($this->input->post('payment_status') == 1) ? $this->input->post('total_amnt') : 0;
            $update_data = array(
                'updated_by' => $this->user_data['user_id'],
                'updated_on' => date('Y-m-d H:i:s')
            );

            $this->bank_model->update_bank_amount($sales_payment_details[0]['bank_acc_id'], $update_data, $sales_payment_details[0]['pay_amt'], '-');
            $this->bank_model->update_bank_amount($this->user_data['bank_account_id'], $update_data, $total_amt_txt, '+');
            
            $status = array();
            $status['payment_status'] = $this->input->post('payment_status');
            $this->sales_payment_model->update_status($so, $status);

            $_SESSION['success'] = 'Invoice Payment has been Updated Successfully!..';
        } else {
            $_SESSION['error'] = 'Invoice Payment not Updated!..';
        }
        redirect('invoice_payment/edit/' . $id, 'refresh');
    }

    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $actions = $this->actions();
        $data = array();
        $data['delete_link'] = $actions['delete'];
        $data['sp_details'] = $this->sales_payment_model->get_by_id($id);      
        $this->render($data, 'sales_payment/view');
    }

    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }
        $user_data = get_user_data();
        $sales_orderdata = $this->sales_payment_model->get_by_id($id);
        $purchase_request = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $user_data['user_id']
        );
          
        if ($this->sales_payment_model->update($id, $purchase_request) > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $sales_orderdata->so_no, 1);
            $sp_details = $this->sales_payment_model->get_by_id($id);
            
            $update_data = array(
                'updated_by' => $this->user_data['user_id'],
                'updated_on' => date('Y-m-d H:i:s')
            );
            $this->bank_model->update_bank_amount($sp_details->bank_acc_id, $update_data, $sp_details->pay_amt, '-');
            
            $status = array();
            $status['payment_status'] = 2;
            $this->sales_payment_model->update_status($sp_details->so_id, $status);
            $_SESSION['success'] = 'Invoice Payment has been deleted';
        } else {
            $_SESSION['error'] = 'Invoice Payment not deleted!';
        }
        redirect('invoice_payment', 'refresh');
    }

    public function get_customer_details() {
        $sno = $_POST['soNo'];
        $data = array();
        if (!empty($sno)) {
            $data['s_order'] = $this->sales_payment_model->get_sales_order_details($sno);
            //error_log($this->db->last_query());
        }
        echo json_encode($data['s_order'][0]);
    }

    public function get_paid_amount() {
        $pi_no = $_POST['pi_no'];
        $data = array();
        if (!empty($pi_no)) {
            $data['paid_amount'] = $this->sales_payment_model->get_paid_amount($pi_no);
        }
        echo json_encode($data['paid_amount'], 1);
    }

}

?>