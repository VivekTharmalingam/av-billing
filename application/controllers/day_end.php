<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Day_End extends REF_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('sales_order_model', '', TRUE);
    }
    
    public function index() {
        $data = array();
        $data['header'] = array();
        $data['breakdowns'] = $this->sales_order_model->day_end_report();
        $data['total_sales'] = $this->sales_order_model->day_end_total();
        $data['report_time'] = date('d-m-Y h:i a');
        $data['print_thermal'] = base_url() . 'day_end/thermal';
        $data['thermal_content'] = $this->thermal();
        $this->render($data, 'day_end/view');
    }
    
    public function thermal() {
        $data = array();
        $data['header'] = array();
        $data['breakdowns'] = $this->sales_order_model->day_end_report();
        $data['total_sales'] = $this->sales_order_model->day_end_total();
        $data['report_time'] = date('d-m-Y h:i a');
        return $this->load->view('day_end/thermal', $data, TRUE);
    }
}

?>