<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Exchange_Order extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('exchange_order_model', '', TRUE);
        $this->load->model('exchange_order_payment_model', '', TRUE);
        $this->load->model('bank_model', '', TRUE);
        $this->load->model('purchase_order_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->model('supplier_model', '', TRUE);
        $this->load->model('company_model', '', TRUE);
        $this->load->library('Datatables');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        $data['print_link'] = base_url() . 'exchange_order/pdf/';
        $data['exchange_order'] = $this->exchange_order_model->list_all();
        $this->render($data, 'exchange_order/list');
    }
    
    function datatable() {
        $this->datatables->select('exr.id AS exId, brn.branch_name AS branch, exr.exr_code, DATE_FORMAT(exr.exr_date, "%d/%m/%Y") as exr_date_str, exr.exr_net_amount AS net, sup.name AS supplier, CASE exr.payment_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as status_str', FALSE)             
                ->where('exr.exr_status != 10 AND exr.branch_id IN ("'.$this->user_data['branch_id'].'")')
                ->join(TBL_SUP . ' as sup', 'sup.id = exr.sup_id', 'left')
                ->join(TBL_BRN . ' as brn', 'brn.id = exr.branch_id', 'left')
                ->add_column('Actions', $this->get_buttons('$1'), 'exId')
                ->from(TBL_EXR.' AS exr');
        echo $this->datatables->generate();
    }
    
    function get_buttons($id) {
        $actions = $this->actions();
        $to_invoice = base_url() . 'invoice/add/';
        $html = '<span class="actions">';
        if (in_array(6, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . base_url() . 'exchange_order/pdf/' . $id . '" title="Print Invoice Details"><span><i class="fa fa-file-pdf-o" aria-hidden="true"></i></span></a>&nbsp';
        }
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(3, $this->permission)) {
            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
        }
        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Exchange Order!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Exchange Order"><span>Delete</span></a>';        
        }
        if (in_array(7, $this->permission)) {
            $html .='</br><a class="label btn btn-success formpost-confirm" href="#" title="Convert to Invoice" data-confirm-content="Do you want to convert the Exchange Order to invoice!" data-redirect-url="' . base_url() . 'invoice/add/" data-bindinputfield="exchangeorder_id[]" data-bindpostid="' . $id . '" data-bindtext="Exchange Order" data-action="INVOICE CONVERSION" style="display: inline-block; margin-top: 5px;"><span style="line-height: 14px;">To Invoice</span></a>';
        }
        
        $html.='</span>';
        return $html;
    }
   
    public function add() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['form_action'] = $actions['insert'];
        $data['items'] = $this->purchase_order_model->get_all_items();
        $data['suppliers'] = $this->purchase_order_model->get_all_suppliers();
        $data['company'] = $this->company_model->get_company();
        $data['add_item_link'] = base_url() . 'item/add/';
        $data['add_po_link'] = base_url() . 'exchange_order/add/';
        $this->render($data, 'exchange_order/add');
    }
    
    public function search_supplier() {
        $data = array();
        $data = $this->supplier_model->search_supplier();
        foreach ($data as $key => $value) {
                $value->label = $value->name;
                $value->value = $value->label;
        }

        $this->ajax_response($data);
    }
    
    public function insert() {
        $prefix_wt_sub = CODE_EXR;
        $grn_no = $this->auto_generation_code(TBL_EXR, $prefix_wt_sub, '', 3, '');
        $user_data = get_user_data();
        
        if($this->input->post('supplier_id') != '' && $this->input->post('supplier_id')!='0') {
            $supplier_id = $this->input->post('supplier_id');
        } else {
            $sub_insert = array(
                'name' => $this->input->post('supplier_name'),
                'supplier_from_exc' => 'yes',
                'status' => 1,            
                'branch_id' => $user_data['branch_id'],
                'created_by' => $user_data['user_id'],
                'created_on' => date('Y-m-d H:i:s')
            );
            $supplier_id =$this->supplier_model->insert($sub_insert);
        }
        
        
        $gst_amount = $this->input->post('gst_amount');
        
        $insert = array(
            'exr_code' => $grn_no,
            'exr_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('exr_date')))),
            'sup_id' => $supplier_id,
            'exr_your_info' => $this->input->post('your_info'),
            'exr_sub_total' => $this->input->post('sub_total'),
            'exr_discount' => $this->input->post('discount'),
            'exr_discount_type' => $this->input->post('discount_type'),
            'exr_total_discount' => $this->input->post('discount_amt'),
            'exr_gst_type' => $this->input->post('gst_type'),
            'exr_gst_val' => $this->input->post('hidden_gst'),
            'exr_gst_amount' => $gst_amount,
            'exr_net_amount' => $this->input->post('total_amt'),
            'exr_status' => 1,
            'received_status' => 1,
            'created_by' => $user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s'),
            'branch_id' => $user_data['branch_id'],
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
	
        $last_id = $this->exchange_order_model->insert($insert);

        if ($last_id > 0) {
            	$this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, $grn_no, '');
                $product_code = $this->input->post('product_code');
                $product_description = $this->input->post('product_description');
                $sl_no = $this->input->post('sl_no');
                $sl_itm_code = $this->input->post('sl_itm_code');
                $quantity = $this->input->post('quantity');
                $price = $this->input->post('price');
                $unit_cost = $this->input->post('cost');
                $item_discount = $this->input->post('item_discount');
                $item_discount_amt = $this->input->post('item_discount_amt');
                $item_discount_type = $this->input->post('item_discount_type');
                $amount = $this->input->post('amount');
				
                if (!empty($product_code)) {
                    foreach ($product_code as $key => $item_id) {
                        if (!empty($item_discount_type[$key])) {
                                $discount_type = 1;
                                $discount_percentage = $item_discount[$key];
                        }
                        else {
                                $discount_type = 0;
                                $discount_percentage = 0;
                        }
						
                        $item = array(
                            'exr_id' => $last_id,
                            'product_text' => $product_code[$key],
                            'product_description' => $product_description[$key],
                            'sl_no' => $sl_no[$key],
                            'sl_itm_code' => $sl_itm_code[$key],
                            'exr_price' => $price[$key],
                            'exr_unit_cost' => $unit_cost[$key],
                            'exr_quantity' => $quantity[$key],
                            'discount' => $item_discount_amt[$key],
                            'discount_percentage' => $discount_percentage,
                            'discount_type' => $discount_type,
                            'total' => $amount[$key],
                            'status' => 1,
                            'branch_id' => $user_data['branch_id'],
                            'created_by' => $user_data['user_id'],
                            'created_on' => date('Y-m-d H:i:s'),
                        );
                        
                        $lst_item_id = $this->exchange_order_model->insert_item($item);
                        
                    }
                }
		$_SESSION['success'] = 'Exchange Order added successfully';
                
        } else {
            $_SESSION['error'] = 'Exchange Order not added!';
        }

        redirect('exchange_order/add', 'refresh');
    }
    
    public function edit($id = '') {

        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['exc_order'] = $this->exchange_order_model->get_by_id($id);
        $data['exc_order_item'] = $this->exchange_order_model->get_item_by_id($id);
        $data['company'] = $this->company_model->get_company();
        $data['add_item_link'] = base_url() . 'item/add/';
        
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' . $data['exc_order']->id;
        
        $this->render($data, 'exchange_order/edit');
    }
    
     public function update($id = '') {
        
        if (empty($id)) {
            show_400_error();
        }
        
        $user_data = get_user_data();
        
        if($this->input->post('supplier_id') != '' && $this->input->post('supplier_id')!='0') {
            $supplier_id = $this->input->post('supplier_id');
        } else {
            $sub_insert = array(
                'name' => $this->input->post('supplier_name'),
                'supplier_from_exc' => 'yes',
                'status' => 1,            
                'branch_id' => $user_data['branch_id'],
                'created_by' => $user_data['user_id'],
                'created_on' => date('Y-m-d H:i:s')
            );
            $supplier_id =$this->supplier_model->insert($sub_insert);
        }
        
        $gst_amount = $this->input->post('gst_amount');
        
        $grns = array(            
            'exr_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('exr_date')))),
            'sup_id' => $supplier_id,
            'exr_sub_total' => $this->input->post('sub_total'),
            'exr_total_discount' => $this->input->post('discount_amt'),
            'exr_discount' => $this->input->post('discount'),
            'exr_discount_type' => $this->input->post('discount_type'),
            'exr_total_discount' => $this->input->post('discount_amt'),
            'exr_gst_type' => $this->input->post('gst_type'),
            'exr_gst_val' => $this->input->post('hidden_gst'),
            'exr_gst_amount' => $gst_amount,
            'exr_net_amount' => $this->input->post('total_amt'),
            'exr_your_info' => $this->input->post('your_info'),
            'exr_status' => 1,
            'branch_id' => $user_data['branch_id'],
            'updated_by' => $user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s'),
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );

        $affected_rows = $this->exchange_order_model->update($id, $grns);
        
        if ($affected_rows > 0) {
            $exc_order_arr = $this->exchange_order_model->get_by_id($id);
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $exc_order_arr->exr_code, '');
            $inactive_rows = $this->exchange_order_model->inactive_exc_items($id);
            
            /*** Update Old Item ***/
            $exc_item_id = $this->input->post('exc_item_id');
            $product_code = $this->input->post('product_code');
            $product_description = $this->input->post('product_description');
            $sl_no = $this->input->post('sl_no');
            $sl_itm_code = $this->input->post('sl_itm_code');
            $quantity = $this->input->post('quantity');
            $price = $this->input->post('price');
            $unit_cost = $this->input->post('cost');
            $item_discount = $this->input->post('item_discount');
            $item_discount_amt = $this->input->post('item_discount_amt');
            $item_discount_type = $this->input->post('item_discount_type');
            $amount = $this->input->post('amount');
            
            if (!empty($exc_item_id)) {
                foreach ($exc_item_id as $key => $exr_item) {
                    if (!empty($item_discount_type[$key])) {
                        $discount_type = 1;
                        $discount_percentage = $item_discount[$key];
                    } else {
                        $discount_type = 0;
                        $discount_percentage = 0;
                    }
                    
                    $item = array(
                        'product_text' => $product_code[$key],
                        'product_description' => $product_description[$key],
                        'sl_no' => $sl_no[$key],
                        'sl_itm_code' => $sl_itm_code[$key],
                        'exr_price' => $price[$key],
                        'exr_unit_cost' => $unit_cost[$key],
                        'discount' => $item_discount_amt[$key],
                        'discount_percentage' => $discount_percentage,
                        'discount_type' => $discount_type,
                        'exr_quantity' => $quantity[$key],
                        'total' => $amount[$key],
                        'status' => 1,
                        'branch_id' => $user_data['branch_id'],
                        'updated_by' => $user_data['user_id'],
                        'updated_on' => date('Y-m-d H:i:s')
                    );
                    
                    $this->exchange_order_model->update_item($item, $exr_item);
                }
            }
            
            /** Insert New Item ** */
            $new_product_code = $this->input->post('new_product_code');
            $new_product_description = $this->input->post('new_product_description');
            $new_sl_no = $this->input->post('new_sl_no');
            $new_sl_itm_code = $this->input->post('new_sl_itm_code');
            $new_quantity = $this->input->post('new_quantity');
            $new_price = $this->input->post('new_price');
            $new_unit_cost = $this->input->post('new_cost');
            $new_item_discount = $this->input->post('new_item_discount');
            $new_item_discount_amt = $this->input->post('new_item_discount_amt');
            $new_item_discount_type = $this->input->post('new_item_discount_type');
            $new_amount = $this->input->post('new_amount');
            
            if (!empty($new_product_code)) {
                foreach ($new_product_code as $key => $product_txt) {
                    if (!empty($new_item_discount_type[$key])) {
                        $discount_type = 1;
                        $discount_percentage = $new_item_discount[$key];
                    } else {
                        $discount_type = 0;
                        $discount_percentage = 0;
                    }

                    $item = array(
                        'exr_id' => $id,
                        'product_text' => $new_product_code[$key],
                        'product_description' => $new_product_description[$key],
                        'sl_no' => $new_sl_no[$key],
                        'sl_itm_code' => $new_sl_itm_code[$key],
                        'exr_price' => $new_price[$key],
                        'exr_unit_cost' => $new_unit_cost[$key],
                        'discount' => $new_item_discount_amt[$key],
                        'discount_percentage' => $discount_percentage,
                        'discount_type' => $discount_type,
                        'exr_quantity' => $new_quantity[$key],
                        'total' => $new_amount[$key],
                        'status' => 1,
                        'branch_id' => $user_data['branch_id'],
                        'created_by' => $user_data['user_id'],
                        'created_on' => date('Y-m-d H:i:s'),
                    );
                    
                    $this->exchange_order_model->insert_item($item);
                    
                }
            }

            $_SESSION['success'] = 'Exchange Order Updated Successfully';
           
        } else {
            $_SESSION['error'] = 'Exchange Order not updated!';
        }

        redirect('exchange_order/edit/' . $id, 'refresh');
    }
    
    public function view($exr_id = '') {
        if (empty($exr_id)) {
            show_400_error();
        }
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['exc_order_pdf'] = base_url() . 'exchange_order/pdf/' . $exr_id;
        $data['exc_order_details'] = $this->exchange_order_model->get_by_id($exr_id);
	$data['exc_order_item'] = $this->exchange_order_model->get_item_by_id($exr_id);
		
        $this->render($data, 'exchange_order/view');
    }
    
    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }
        $user_data = get_user_data();
                
        $exc_order = array(
            'exr_status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $user_data['user_id']
        );
        
        if ($this->exchange_order_model->update($id, $exc_order) > 0){
            $exc_order_arr = $this->exchange_order_model->get_by_id($id);
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $exc_order_arr->exr_code, 1);
            $_SESSION['success'] = 'Exchange Order deleted successfully';
            $this->exchange_order_model->delete_exc_items($id);
            
            $exchange_order_payment = $this->exchange_order_payment_model->get_payment_by_excord_id($id);
            if(count($exchange_order_payment)>0) {
            foreach ($exchange_order_payment as $key => $row) {
                
                $update_payment = array(
                    'status' => 10,
                    'updated_on' => date('Y-m-d H:i:s'),
                    'updated_by' => $this->user_data['user_id']
                );
                if ($this->exchange_order_payment_model->update($row->id, $update_payment) > 0) {
                $update_data = array(
                    'updated_by' => $this->user_data['user_id'],
                    'updated_on' => date('Y-m-d H:i:s')
                );
                $this->bank_model->update_bank_amount($row->bank_acc_id, $update_data, $row->pay_amt, '+');  
                }
            }
            }
        }else{
            $_SESSION['error'] = 'Exchange Order not deleted!';
        }
        redirect('exchange_order', 'refresh');
    }
    
    public function pdf($id) {
        $data = array();
        $user_data = get_user_data();
        $data['exc_order'] = $this->exchange_order_model->get_by_id($id);
        $data['exc_order_item'] = $this->exchange_order_model->get_item_by_id($id);
        //$data['address'] = $this->purchase_order_model->get_current_address($user_data['branch_id']);
        $data['amt_word'] = convert_amount_to_word_rupees($data['exc_order']->exr_net_amount);
        $data['supplier'] = $this->purchase_order_model->supplier_address_by_id($data['exc_order']->sup_id);
        $data['user_data'] = get_user_data();
        $data['branch'] = $this->company_model->branch_by_id($data['exc_order']->branch_id);
		$data['branches'] = $this->company_model->branch_list_active();
        $data['company'] = $this->company_model->get_company();
        $filename = 'PDF_' . $data['exc_order']->exr_code;
        $filename = strtoupper($filename);
		
        // Load Views
        $header = $this->load->view('exchange_order/pdf_header', $data, TRUE);;
        $content = $this->load->view('exchange_order/pdf', $data, TRUE);
        $footer = $this->load->view('exchange_order/pdf_footer', $data, TRUE);
		
        $this->load->helper(array('My_Pdf'));
        invoice_pdf($header, $content, $footer, $filename);
        exit(0);
    }
    

}

?>