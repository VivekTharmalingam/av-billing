<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Expense_Detail_Reports extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('expense_model', '', TRUE);
        $this->load->model('company_model', '', TRUE);
        $this->load->library('upload');
    }

    public function index() {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = base_url() . 'expense/view' . '/';
        $data['form_action'] = base_url() . 'expense_detail_reports';
        $from = '';
        $to = '';
        $data['search_date'] = '';

        if ($this->input->post('search_date') != '') {
            $data['search_date'] = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : '';
            $search_date = explode('-', $this->input->post('search_date'));
            $from = trim($search_date[0]);
            $to = trim($search_date[1]);

            $from = explode('/', $from);
            $from = $from[2] . '-' . $from[1] . '-' . $from[0];

            $to = explode('/', $to);
            $to = $to[2] . '-' . $to[1] . '-' . $to[0];
        }
        $expense = (!empty($this->input->post('exp_type'))) ? $this->input->post('exp_type') : '';
        $data['from_date'] = $from;
        $data['to_date'] = $to;
        $data['expense_type'] = $expense;
        $data['exp_name'] = $this->expense_model->list_exp_name($expense);
        $data['results'] = $this->expense_model->list_search_all($from, $to, $expense);
        $data['exp_type'] = $this->expense_model->list_exp_type();
        
        if ($this->input->post('reset_btn')) {
            $data['search_date'] = '';
            $data['expense_type'] = array();
            $data['results'] = $this->expense_model->list_search_all();
        }
        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Expense Detail Reports';
            $head['setting'] = $this->company_model->get_company();
            $filename = 'Expense_detail_report_' . current_date();
            //echo '<pre>'; print_r($data['exp_name']); exit;
            // Load Views
            $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
            $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
            $header = '<div></div>';
            $content = $this->load->view('expense_detail_report/pdf_report', $data, TRUE);
            $footer = '<div></div>';
            $this->load->helper(array('My_Pdf'));

            create_pdf($header, $content, $footer, $filename);
            exit(0);
        } elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $this->load->view('expense_detail_report/excel_report', $data);
        } else {
            $this->render($data, 'expense_detail_report/list');
        }
    }

    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['expense'] = $this->expense_model->get_by_id($id);
        $data['exp_type'] = $this->expense_model->list_exp_type();

        $this->render($data, 'expense/view');
    }

}

?>