<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Services extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('services_model', '', TRUE);
        $this->load->model('exchange_order_model', '', TRUE);
        $this->load->model('bank_model', '', TRUE);
        $this->load->model('purchase_order_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->model('supplier_model', '', TRUE);
        $this->load->model('company_model', '', TRUE);
        $this->load->model('sales_order_model', '', TRUE);
        $this->load->library('Datatables');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        $data['print_link'] = base_url() . 'services/pdf/';
        $data['services'] = $this->services_model->list_all();
        $this->render($data, 'services/list');
    }
    
    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('serv.id AS srId, serv.customer_name AS cust_name, serv.jobsheet_number as jobsheet_number, DATE_FORMAT(serv.received_date, "%d/%m/%Y") as received_date_str, user.name AS eng_name, CASE serv.service_status WHEN 1 THEN "Processing" WHEN 2 THEN "Completed" WHEN 3 THEN "Delivered" ELSE "" END as service_status', FALSE)             
                ->where('serv.status != 10 AND serv.branch_id IN ("'.$this->user_data['branch_id'].'")')
                ->join(TBL_BRN . ' as brn', 'brn.id = serv.branch_id', 'left')
                ->join(TBL_USER . ' as user', 'user.id = serv.engineer_id', 'left')
                ->add_column('Updates', $this->set_updates('$1'), 'srId')
                ->add_column('Actions', $this->get_buttons('$1'), 'srId')
                ->from(TBL_SJST.' AS serv');
        echo $this->datatables->generate();
    }
    
    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
//        $html .='<a class="label btn btn-primary view" style="background: #009688; border-color: #009688;" data-href="' . base_url() . 'services/update_rack_num/' . $id . '" id="smart-mod-eg2" title="Rack No." data-id="' . $id . '"><span><i class="fa fa-shopping-basket" aria-hidden="true"></i> Rack No.</span></a>&nbsp';
        if (in_array(6, $this->permission)) {
        $html .='<a class="label btn btn-warning view" href="' . base_url() . 'services/pdf/' . $id . '" title="Print Invoice Details"><span><i class="fa fa-file-pdf-o" aria-hidden="true"></i></span></a>&nbsp';
        }
        if (in_array(1, $this->permission)) {
        $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(3, $this->permission)) {
        $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
        }
        if (in_array(4, $this->permission)) {
        $html .='<a class="label btn btn-danger delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Exchange Order!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Exchange Order"><span>Delete</span></a>';        
        }
        $html .='</br><a class="label btn btn-success formpost-confirm" href="#" title="Convert to Invoice" data-confirm-content="Do you want to convert the Service to invoice!" data-redirect-url="' . base_url() . 'invoice/add/" data-bindinputfield="service_id" data-bindpostid="' . $id . '" data-bindtext="Service" data-action="INVOICE CONVERSION" style="display: inline-block; margin-top: 5px;"><span style="line-height: 14px;">To Invoice</span></a>';
        $html.='</span>';
        return $html;
    }
    
    function set_updates($id) {
        $html = '<span class="actions">';
        $html .='<a class="label btn btn-primary view" style="background: #00AEED; border-color: #00AEED;" href="' . base_url() . 'services/item_updation/' . $id . '" title="Add Item and Service Cost"><span><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Item and Service Cost</span></a>';
        $html .='</br><a class="label btn btn-primary view" style="background: #009688; border-color: #009688;display: inline-block; margin-top: 5px;" data-href="' . base_url() . 'services/update_rack_num/' . $id . '" id="smart-mod-eg2" title="Rack No." data-id="' . $id . '"><span style="line-height: 14px;"><i class="fa fa-shopping-basket" aria-hidden="true"></i> Rack No.</span></a>&nbsp<a class="label btn btn-warning view" href="' . base_url() . 'services/status_log/' . $id . '"><span><i class="fa fa-pencil"></i> Status Log</span></a>&nbsp';
        $html.='</span>';
        return $html;
    }
    
    public function get_rack_no() {
        $data = $this->services_model->get_by_id($_POST['id']);
        echo json_encode($data);
    }
    
    public function add() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['form_action'] = $actions['insert'];
        $data['customer_list'] = $this->services_model->customer_list();
        $data['user_list'] = $this->services_model->user_list();
        $data['category_list'] = $this->services_model->service_category();
        $data['category'] = $this->product_model->category();
        $data['company'] = $this->company_model->get_company();
        $data['add_category_link'] = base_url() . 'brand/add/';
        $prefix_wt_sub = CODE_SJS;
        $data['jobsheet_number'] = $this->auto_generation_code(TBL_SJST, $prefix_wt_sub, '', 3, '');
        $this->render($data, 'services/add');
    }
    
    public function getcustomer_data() {
        $cust_id = $this->input->post('cust_id');
        $response = array();
        $response['cust'] = $this->services_model->getcustomer_data($cust_id);
        $this->ajax_response($response);
    }
    
    public function insert() {
        $prefix_wt_sub = CODE_SJS;
        $jobsheet_number = $this->auto_generation_code(TBL_SJST, $prefix_wt_sub, '', 3, '');
        $user_data = get_user_data();
        
        $cust_id = $this->input->post('customer_id');
        $customer_name = ($cust_id != '' && $cust_id=='0') ? $this->input->post('customer_name') : $this->input->post('customer_name_txt');
        $estimated_delivery_date = (!empty($this->input->post('estimated_delivery_date'))) ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('estimated_delivery_date')))) : '';
        $uploaded_data = '';
        if (!empty($_FILES['upload_photo']['name'])) {
            $uploaded_data = uploads(array('upload_path' => 'service', 'field' => 'upload_photo'));
        }
        
        $category_idtxt = ($this->input->post('cat_name') != '') ? $this->input->post('cat_name') : null;
        
        $insert = array(
            'jobsheet_number' => $jobsheet_number,
            'customer_id' => $cust_id,
            'customer_name' => $customer_name,
            'contact_person' => $this->input->post('contact_person'),
            'contact_number' => $this->input->post('contact_number'),
            'email_address' => $this->input->post('email_address'),
            'engineer_id' => $this->input->post('engineer_id'),
            'guessing_problem' => $this->input->post('guessing_problem'),
            'estimated_price_quoted' => $this->input->post('estimated_price_quoted'),
            'cat_id' => $category_idtxt,
            'material_name' => $this->input->post('material_name'),
            'material_type' => $this->input->post('material_type'),
            'problem_desc' => $this->input->post('problem_desc'),
            'received_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('received_date')))),
            'physical_condition' => $this->input->post('physical_condition'),
            'remarks' => $this->input->post('remarks'),
            'created_by' => $user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s'),
            'branch_id' => $user_data['branch_id'],
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        
        if ($estimated_delivery_date != "") :
            $insert['estimated_delivery_date'] = $estimated_delivery_date;
        endif;
        
        if (!empty($uploaded_data['upload_photo'])) :
            $insert['upload_photo'] = $uploaded_data['upload_photo']['file_path_str'];
        endif;
	
        $last_id = $this->services_model->insert($insert);

        if ($last_id > 0) {
            	$this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, $jobsheet_number, '');
                $cat_ids = $this->input->post('cat_ids');
		$cat_other = $this->input->post('cat_other');
                $item_other = $this->input->post('item_other');
                $ser_item = $this->input->post('ser_item');
                $item_others_txt = $this->input->post('item_others_txt');
                $cat_others_txt = $this->input->post('cat_others_txt');
				
                if (!empty($cat_ids)) {
                    foreach ($cat_ids as $key => $catid) {
                        $item_id = 0;
                        if($cat_other[$key]==1 && $cat_others_txt[$key] != "") {
                            $item = array(
                                'service_id' => $last_id,
                                'cat_id' => $catid,
                                'others_txt' => $cat_others_txt[$key],
                                'branch_id' => $user_data['branch_id'],
                                'created_by' => $user_data['user_id'],
                                'created_on' => date('Y-m-d H:i:s'),
                            );
                            $lst_item_id = $this->services_model->insert_item($item);

                        } else if(count($ser_item[$key]) && $ser_item[$key][0] != '') {
                                foreach ($ser_item[$key] as $itemkey => $itm_id) {
                                    $item_othertext = ($item_other[$key][$itm_id]==1) ? $item_others_txt[$key][$itm_id] : '';
                                    $item = array(
                                        'service_id' => $last_id,
                                        'cat_id' => $catid,
                                        'item_id' => $itm_id,
                                        'others_txt' => $item_othertext,
                                        'branch_id' => $user_data['branch_id'],
                                        'created_by' => $user_data['user_id'],
                                        'created_on' => date('Y-m-d H:i:s'),
                                    );
                                    $lst_item_id = $this->services_model->insert_item($item);
                                }
                            
                        } 
                        
                    }
                }
		$_SESSION['success'] = 'Service added successfully';
                
        } else {
            $_SESSION['error'] = 'Service not added!';
        }

        redirect('services/add', 'refresh');
    }
    
    public function edit($id = '') {

        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['service'] = $this->services_model->get_by_id($id);
	$data['service_item'] = $this->services_model->get_item_by_id($id);
        $data['customer_list'] = $this->services_model->customer_list();
        $data['user_list'] = $this->services_model->user_list();
        $data['category_list'] = $this->services_model->service_category();
        $data['company'] = $this->company_model->get_company();
        $data['category'] = $this->product_model->category();
        
        $data['add_category_link'] = base_url() . 'brand/add/';
        
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' . $data['service']->id;
        
        $this->render($data, 'services/edit');
    }
    
     public function update($id = '') {
        
        if (empty($id)) {
            show_400_error();
        }
        
        $user_data = get_user_data();
        
        $cust_id = $this->input->post('customer_id');
        $customer_name = ($cust_id != '' && $cust_id=='0') ? $this->input->post('customer_name') : $this->input->post('customer_name_txt');
        $estimated_delivery_date = (!empty($this->input->post('estimated_delivery_date'))) ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('estimated_delivery_date')))) : '';
        $delivered_date = (!empty($this->input->post('delivered_date'))) ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('delivered_date')))) : '';
        $uploaded_data = '';
        if (!empty($_FILES['upload_photo']['name'])) {
            $uploaded_data = uploads(array('upload_path' => 'service', 'field' => 'upload_photo'));
        }
        
        $category_idtxt = ($this->input->post('cat_name') != '') ? $this->input->post('cat_name') : null;
        
        $servicedata = array(            
            'customer_id' => $cust_id,
            'customer_name' => $customer_name,
            'contact_person' => $this->input->post('contact_person'),
            'contact_number' => $this->input->post('contact_number'),
            'email_address' => $this->input->post('email_address'),
            'engineer_id' => $this->input->post('engineer_id'),
            'guessing_problem' => $this->input->post('guessing_problem'),
            'estimated_price_quoted' => $this->input->post('estimated_price_quoted'),
            'cat_id' => $category_idtxt,
            'material_name' => $this->input->post('material_name'),
            'material_type' => $this->input->post('material_type'),
            'problem_desc' => $this->input->post('problem_desc'),
            'received_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('received_date')))),
            'physical_condition' => $this->input->post('physical_condition'),
            'remarks' => $this->input->post('remarks'),
            'service_status' => $this->input->post('service_status'),
            'branch_id' => $user_data['branch_id'],
            'updated_by' => $user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s'),
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        
         if ($estimated_delivery_date != "") :
            $servicedata['estimated_delivery_date'] = $estimated_delivery_date;
        endif;
        
         if ($delivered_date != "") :
            $servicedata['delivered_date'] = $delivered_date;
        endif;
        
        if (!empty($uploaded_data['upload_photo'])) :
            $servicedata['upload_photo'] = $uploaded_data['upload_photo']['file_path_str'];
        endif;

        $affected_rows = $this->services_model->update($id, $servicedata);
        
        if ($affected_rows > 0) {
            $data_service = $this->services_model->get_by_id($id);
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $data_service->jobsheet_number, '');
            $inactive_rows = $this->services_model->inactive_exc_items($id);
            
            /*** Service Item ***/
            $cat_ids = $this->input->post('cat_ids');
            $cat_other = $this->input->post('cat_other');
            $item_other = $this->input->post('item_other');
            $ser_item = $this->input->post('ser_item');
            $item_others_txt = $this->input->post('item_others_txt');
            $cat_others_txt = $this->input->post('cat_others_txt');

            if (!empty($cat_ids)) {
                foreach ($cat_ids as $key => $catid) {
                    $item_id = 0;
                    if($cat_other[$key]==1 && $cat_others_txt[$key] != "") {
                        $ser_itm_data = $this->services_model->service_item_check($id,$catid,'');
                        if(count($ser_itm_data)>0) {
                            $item = array(
                                'status' => 1,
                                'others_txt' => $cat_others_txt[$key],
                                'branch_id' => $user_data['branch_id'],
                                'updated_by' => $user_data['user_id'],
                                'updated_on' => date('Y-m-d H:i:s'),
                            );
                            $lst_item_id = $this->services_model->update_item($item, $ser_itm_data[0]['id']);
                        } else {
                            $item = array(
                                'service_id' => $id,
                                'cat_id' => $catid,
                                'others_txt' => $cat_others_txt[$key],
                                'branch_id' => $user_data['branch_id'],
                                'created_by' => $user_data['user_id'],
                                'created_on' => date('Y-m-d H:i:s'),
                            );
                            $lst_item_id = $this->services_model->insert_item($item);
                        }

                    } else if(count($ser_item[$key]) && $ser_item[$key][0] != '') {
                            foreach ($ser_item[$key] as $itemkey => $itm_id) {
                                $item_othertext = ($item_other[$key][$itm_id]==1) ? $item_others_txt[$key][$itm_id] : '';
                                
                                $ser_itm_data = $this->services_model->service_item_check($id,$catid,$itm_id);
                                if(count($ser_itm_data)>0) {
                                    $item = array(
                                        'status' => 1,
                                        'others_txt' => $item_othertext,
                                        'branch_id' => $user_data['branch_id'],
                                        'updated_by' => $user_data['user_id'],
                                        'updated_on' => date('Y-m-d H:i:s'),
                                    );
                                    $lst_item_id = $this->services_model->update_item($item, $ser_itm_data[0]['id']);
                                } else {
                                    $item = array(
                                        'service_id' => $id,
                                        'cat_id' => $catid,
                                        'item_id' => $itm_id,
                                        'others_txt' => $item_othertext,
                                        'branch_id' => $user_data['branch_id'],
                                        'created_by' => $user_data['user_id'],
                                        'created_on' => date('Y-m-d H:i:s'),
                                    );
                                    $lst_item_id = $this->services_model->insert_item($item);
                                }
                                
                            }

                    } 

                }
            }

            $_SESSION['success'] = 'Service Updated Successfully';
            
            /*** SMS ***/
            $sms_type = (!empty($this->input->post('sms_type'))) ? strtolower($this->input->post('sms_type')) : '';
            if(($this->input->post('service_status')=='2') || ($this->input->post('estimated_date_change')=='yes' && $sms_type=='yes')) {
                if($this->input->post('contact_number') != "") {
                    $receiver_number = trim(str_replace(' ','',$this->input->post('contact_number')));
                    $num_cunt = strlen($receiver_number);$mbl = substr($receiver_number, 0, 3);$mbl1 = substr($receiver_number, 0, 2);
                    $contact_no = '';
                    if($mbl=='+65') {
                    $contact_no = substr($receiver_number, 3);
                    } else if($mbl1=='65' && $num_cunt>8) {
                    $contact_no = substr($receiver_number, 2);
                    } else {
                    $contact_no = $receiver_number;	
                    }

                    $contact_no = substr($contact_no, -8);
                    if($contact_no!='') {
                        if($this->input->post('estimated_date_change')=='yes' && $sms_type=='yes') {
                            $msg = 'Your estimated delivery date for the job sheet number '.$this->input->post('jobsheet_number').' has been changed '.$this->input->post('estimated_delivery_date_temp').' to '.$this->input->post('estimated_delivery_date').'.';
                            $servicedata = array(            
                                'service_id' => $id,
                                'log_description' => $msg,
                                'status' => 1,
                                'branch_id' => $user_data['branch_id'],
                                'created_by' => $user_data['user_id'],
                                'created_on' => date('Y-m-d H:i:s')
                            );
                            $inrst_id = $this->services_model->insert_status_log($servicedata);
                        } else {
                            $msg = 'Your service for the job sheet number '.$this->input->post('jobsheet_number').' has been completed.';
                        }
                        
                        if(!empty($msg)){
                            $sms = send_sms($contact_no, $msg);
                        }
                    }
                }
            }
            
           
        } else {
            $_SESSION['error'] = 'Service not updated!';
        }

        redirect('services/edit/' . $id, 'refresh');
    }
    
    public function update_rack_num($id) {
        $user_data = get_user_data();
        $delivered_date = (!empty($this->input->post('delivered_date'))) ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('delivered_date')))) : '';
        $servicedata = array(            
            'rack_number' => $this->input->post('rack_number_txt'),
            'service_status' => $this->input->post('service_status'),
            'branch_id' => $user_data['branch_id'],
            'updated_by' => $user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s'),
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        if ($delivered_date != "") :
            $servicedata['delivered_date'] = $delivered_date;
        endif;
        
        $affected_rows = $this->services_model->update($id, $servicedata);
        
        if ($affected_rows > 0) {
            $data_service = $this->services_model->get_by_id($id);
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $data_service->jobsheet_number, '');
            /*** SMS ***/
            if($this->input->post('service_status')=='2') {
                $service_data = $this->services_model->get_by_id($id);
                if($service_data->contact_number != "") {
                $receiver_number = trim(str_replace(' ','',$service_data->contact_number));
                $num_cunt = strlen($receiver_number);$mbl = substr($receiver_number, 0, 3);$mbl1 = substr($receiver_number, 0, 2);
                $contact_no = '';
                if($mbl=='+65') {
                $contact_no = substr($receiver_number, 3);
                } else if($mbl1=='65' && $num_cunt>8) {
                $contact_no = substr($receiver_number, 2);
                } else {
                $contact_no = $receiver_number;	
                }
                
                $contact_no = substr($contact_no, -8);
                if($contact_no!='') {
                    $msg = 'Your service for the job sheet number '.$service_data->jobsheet_number.' has been completed.';
                    $sms = send_sms($contact_no, $msg);
                }
                }
            }
            $_SESSION['success'] = 'Rack Number Updated Successfully';
        } else {
            $_SESSION['error'] = 'Rack Number not updated!';
        }
        
        redirect('services', 'refresh');
    }

    public function view($serv_id = '') {
        if (empty($serv_id)) {
            show_400_error();
        }
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['service_pdf'] = base_url() . 'services/pdf/' . $serv_id;
        $data['service'] = $this->services_model->get_by_id($serv_id);
	$data['service_item'] = $this->services_model->get_item_by_id($serv_id);
        $data['company'] = $this->company_model->get_company();
        
        $data['service_cost'] = $this->services_model->service_costitems_byid($serv_id);
        $data['exc_items_group'] = $this->services_model->service_excitems_byid($serv_id);
        $data['inventory_items'] = $this->services_model->service_inventoryitems_byid($serv_id);
        
        $data['service_external_items'] = $this->services_model->get_services_exter_item_view($serv_id);
        $edt_page = (!empty($data['exc_items_group'])) ? 'service' : '';
        $edt_page_id = (!empty($data['exc_items_group'])) ? $serv_id : '';
        $data['exchange_order_list'] = $this->sales_order_model->get_filtered_exc($edt_page,$edt_page_id);
		
        $this->render($data, 'services/view');
    }
    
    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }
        $user_data = get_user_data();
        $data_service = $this->services_model->get_by_id($id);
        
        $service = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $user_data['user_id']
        );
        
        if ($this->services_model->update($id, $service) > 0){
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $data_service->jobsheet_number, 1);
            $_SESSION['success'] = 'Service deleted successfully';
            $this->services_model->delete_exc_items($id);
        }else{
            $_SESSION['error'] = 'Service not deleted!';
        }
        redirect('services', 'refresh');
    }
    
    public function item_updation($serv_id) {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['external_service'] = $this->services_model->get_external_service($serv_id);
        $data['service_external_items'] = $this->services_model->get_services_exter_item($serv_id);
        $data['external_form_action'] = base_url() . 'services/update_external_item/'.$serv_id;
        $data['service'] = $this->services_model->get_by_id($serv_id);
        if($data['service']->exc_inventory_itemadded==1) {
            $data['form_action'] = base_url() . 'services/ser_item_update/'.$serv_id;
            $data['service_cost'] = $this->services_model->service_costitems_byid($serv_id);
            $data['exc_items_group'] = $this->services_model->service_excitems_byid($serv_id);
            $data['inventory_items'] = $this->services_model->service_inventoryitems_byid($serv_id);
            $edt_page = (!empty($data['exc_items_group'])) ? 'service' : '';
            $edt_page_id = (!empty($data['exc_items_group'])) ? $serv_id : '';
            $data['exchange_order_list'] = $this->sales_order_model->get_filtered_exc($edt_page,$edt_page_id);
            $this->render($data, 'services/edit_items');
        } else {
            $data['form_action'] = base_url() . 'services/ser_item_add/'.$serv_id;
            $data['exchange_order_list'] = $this->sales_order_model->get_filtered_exc();
            $this->render($data, 'services/add_items');
        }
    }
    
    public function ser_item_add($serv_id) {
        
        $user_data = get_user_data();
        $updat_txt = 0;
        
        $servicedata = array(            
            'exc_inventory_amount' => $this->input->post('total_amt'),
            'exc_inventory_itemadded' => 1,
            'branch_id' => $user_data['branch_id'],
            'updated_by' => $user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s'),
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        $affected_rows = $this->services_model->update($serv_id, $servicedata);
        
        
        /***  Service Cost  ***/
        $serv_product_code = $this->input->post('serv_product_code');
        $serv_amount = $this->input->post('serv_amount');     
        foreach ($serv_product_code as $key => $product) {
            if (!empty($serv_product_code[$key])) {
                $item = array(
                    'service_id' => $serv_id,
                    'product_description' => $serv_product_code[$key],
                    'total' => $serv_amount[$key],
                    'form_type' => 3,
                    'branch_id' => $user_data['branch_id'],
                    'created_by' => $user_data['user_id'],
                    'created_on' => date('Y-m-d H:i:s')
                );
                $this->services_model->insert_exc_inventory_item($item);
                $updat_txt = 1;
            }
        }
        
        /***  Exchange Order Items  ***/
        $exchange_order_id = $this->input->post('exchange_order_id');
        $exc_product_id = $this->input->post('exc_product_id');
        $exc_product_description = $this->input->post('exc_product_description');
        $exc_quantity = $this->input->post('exc_quantity');
        $exc_price = $this->input->post('exc_price');
        $exc_unit_cost = $this->input->post('exc_cost');
        $exc_amount = $this->input->post('exc_amount');        
        foreach ($exchange_order_id as $key => $excid) {
            if (!empty($exchange_order_id[$key])) {    
                foreach ($exc_product_id[$key] as $exckey => $excproduct) {
                    if (!empty($exc_product_id[$key][$exckey])) {
                        $item = array(
                            'service_id' => $serv_id,
                            'exchange_order_id' => $exchange_order_id[$key],
                            'item_id' => $exc_product_id[$key][$exckey],
                            'product_description' => $exc_product_description[$key][$exckey],
                            'price' => $exc_price[$key][$exckey],
                            'unit_cost' => $exc_unit_cost[$key][$exckey],
                            'quantity' => $exc_quantity[$key][$exckey],
                            'total' => $exc_amount[$key][$exckey],
                            'form_type' => 1,
                            'branch_id' => $user_data['branch_id'],
                            'created_by' => $user_data['user_id'],
                            'created_on' => date('Y-m-d H:i:s')
                        );
                        $this->services_model->insert_exc_inventory_item($item);
                        $updat_txt = 1;
                    }
                }
            }
        }
        
        
        /***  Inventory Items  ***/
        $category_id = $this->input->post('category_id');
        $product_id = $this->input->post('product_id');
        $product_description = $this->input->post('product_description');
        $quantity = $this->input->post('quantity');
        $price = $this->input->post('price');
        $unit_cost = $this->input->post('cost');
        $amount = $this->input->post('amount');        
        foreach ($product_id as $key => $product) {
            if (!empty($product_id[$key])) {
                $item = array(
                    'service_id' => $serv_id,
                    'category_id' => $category_id[$key],
                    'item_id' => $product_id[$key],
                    'product_description' => $product_description[$key],
                    'price' => $price[$key],
                    'unit_cost' => $unit_cost[$key],
                    'quantity' => $quantity[$key],
                    'total' => $amount[$key],
                    'form_type' => 2,
                    'branch_id' => $user_data['branch_id'],
                    'created_by' => $user_data['user_id'],
                    'created_on' => date('Y-m-d H:i:s')
                );
                $this->services_model->insert_exc_inventory_item($item);
                $updat_txt = 1;
            }
        }
        
        if ($updat_txt == 1){
            $data_service = $this->services_model->get_by_id($serv_id);
            $this->insert_log($this->log_controler_name, 'Service Items Add', 'view', $serv_id, $data_service->jobsheet_number, '');
            $_SESSION['success'] = 'Service Items added successfully';
        }else{
            $_SESSION['error'] = 'Service Items not added!';
        }
        
        redirect('services', 'refresh');
    }
    
    public function ser_item_update($serv_id) {
        
        $user_data = get_user_data();
        $updat_txt = 0;
        
        $servicedata = array(            
            'exc_inventory_amount' => $this->input->post('total_amt'),
            'exc_inventory_itemadded' => 1,
            'branch_id' => $user_data['branch_id'],
            'updated_by' => $user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s'),
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        $affected_rows = $this->services_model->update($serv_id, $servicedata);
        
        $inactive_rows = $this->services_model->inactive_ser_excinventory_items($serv_id);
        
        
        /***  Service Cost  ***/
        $service_cost_id = $this->input->post('service_cost_id');
        $serv_product_code = $this->input->post('serv_product_code');
        $serv_amount = $this->input->post('serv_amount');     
        
        $new_serv_product_code = $this->input->post('new_serv_product_code');
        $new_serv_amount = $this->input->post('new_serv_amount');
        
        if(count($service_cost_id)>0) {
            foreach ($service_cost_id as $key => $cost_id) {
                if (!empty($serv_product_code[$key])) {
                    $item = array(
                        'product_description' => $serv_product_code[$key],
                        'total' => $serv_amount[$key],
                        'form_type' => 3,
                        'status' => 1,
                        'branch_id' => $user_data['branch_id'],
                        'updated_by' => $user_data['user_id'],
                        'updated_on' => date('Y-m-d H:i:s')
                    );
                    $this->services_model->update_excinventory_item($item, $service_cost_id[$key]);
                    $updat_txt = 1;
                }
            }
        }
        
        if(count($new_serv_product_code)>0) {
            foreach ($new_serv_product_code as $key => $serv_product) {
                if (!empty($new_serv_product_code[$key])) {
                    $item = array(
                        'service_id' => $serv_id,
                        'product_description' => $new_serv_product_code[$key],
                        'total' => $new_serv_amount[$key],
                        'form_type' => 3,
                        'branch_id' => $user_data['branch_id'],
                        'created_by' => $user_data['user_id'],
                        'created_on' => date('Y-m-d H:i:s')
                    );
                    $this->services_model->insert_exc_inventory_item($item);
                    $updat_txt = 1;
                }
            }
        }
        
        /***  Exchange Order Items  ***/
        $ser_exc_itemid = $this->input->post('ser_exc_itemid');
        $exchange_order_id = $this->input->post('exchange_order_id');
        $exc_product_id = $this->input->post('exc_product_id');
        $exc_product_description = $this->input->post('exc_product_description');
        $exc_quantity = $this->input->post('exc_quantity');
        $exc_price = $this->input->post('exc_price');
        $exc_unit_cost = $this->input->post('exc_cost');
        $exc_amount = $this->input->post('exc_amount');  
        
        $new_exchange_order_id = $this->input->post('new_exchange_order_id');
        $new_exc_product_id = $this->input->post('new_exc_product_id');
        $new_exc_product_description = $this->input->post('new_exc_product_description');
        $new_exc_quantity = $this->input->post('new_exc_quantity');
        $new_exc_price = $this->input->post('new_exc_price');
        $new_exc_unit_cost = $this->input->post('new_exc_cost');
        $new_exc_amount = $this->input->post('new_exc_amount');  
        
        foreach ($exchange_order_id as $key => $excid) {
            if (!empty($exchange_order_id[$key]) && count($exc_product_id[$key])>0) {    
                foreach ($exc_product_id[$key] as $exckey => $excproduct) {
                    if (!empty($exc_product_id[$key][$exckey])) {
                        $item = array(
                            'exchange_order_id' => $exchange_order_id[$key],
                            'item_id' => $exc_product_id[$key][$exckey],
                            'product_description' => $exc_product_description[$key][$exckey],
                            'price' => $exc_price[$key][$exckey],
                            'unit_cost' => $exc_unit_cost[$key][$exckey],
                            'quantity' => $exc_quantity[$key][$exckey],
                            'total' => $exc_amount[$key][$exckey],
                            'form_type' => 1,
                            'status' => 1,
                            'branch_id' => $user_data['branch_id'],
                            'updated_by' => $user_data['user_id'],
                            'updated_on' => date('Y-m-d H:i:s')
                        );
                        $this->services_model->update_excinventory_item($item, $ser_exc_itemid[$key][$exckey]);
                        $updat_txt = 1;
                    }
                }
            }
            
            if (!empty($exchange_order_id[$key]) && count($new_exc_product_id[$key])>0) {    
                foreach ($new_exc_product_id[$key] as $exckey => $excproduct) {
                    if (!empty($new_exc_product_id[$key][$exckey])) {
                        $item = array(
                            'service_id' => $serv_id,
                            'exchange_order_id' => $exchange_order_id[$key],
                            'item_id' => $new_exc_product_id[$key][$exckey],
                            'product_description' => $new_exc_product_description[$key][$exckey],
                            'price' => $new_exc_price[$key][$exckey],
                            'unit_cost' => $new_exc_unit_cost[$key][$exckey],
                            'quantity' => $new_exc_quantity[$key][$exckey],
                            'total' => $new_exc_amount[$key][$exckey],
                            'form_type' => 1,
                            'branch_id' => $user_data['branch_id'],
                            'created_by' => $user_data['user_id'],
                            'created_on' => date('Y-m-d H:i:s')
                        );
                        $this->services_model->insert_exc_inventory_item($item);
                        $updat_txt = 1;
                    }
                }
            }
            
        }
        
        foreach ($new_exchange_order_id as $key => $excid) {
            if (!empty($new_exchange_order_id[$key]) && count($new_exc_product_id[$key])>0) {    
                foreach ($new_exc_product_id[$key] as $exckey => $excproduct) {
                    if (!empty($new_exc_product_id[$key][$exckey])) {
                        $item = array(
                            'service_id' => $serv_id,
                            'exchange_order_id' => $new_exchange_order_id[$key],
                            'item_id' => $new_exc_product_id[$key][$exckey],
                            'product_description' => $new_exc_product_description[$key][$exckey],
                            'price' => $new_exc_price[$key][$exckey],
                            'unit_cost' => $new_exc_unit_cost[$key][$exckey],
                            'quantity' => $new_exc_quantity[$key][$exckey],
                            'total' => $new_exc_amount[$key][$exckey],
                            'form_type' => 1,
                            'branch_id' => $user_data['branch_id'],
                            'created_by' => $user_data['user_id'],
                            'created_on' => date('Y-m-d H:i:s')
                        );
                        $this->services_model->insert_exc_inventory_item($item);
                        $updat_txt = 1;
                    }
                }
            }
        }
        
        /***  Inventory Items  ***/
        $ser_invventory_id = $this->input->post('ser_invventory_id');
        $category_id = $this->input->post('category_id');
        $product_id = $this->input->post('product_id');
        $product_description = $this->input->post('product_description');
        $quantity = $this->input->post('quantity');
        $price = $this->input->post('price');
        $unit_cost = $this->input->post('cost');
        $amount = $this->input->post('amount');  
        
        $new_category_id = $this->input->post('new_category_id');
        $new_product_id = $this->input->post('new_product_id');
        $new_product_description = $this->input->post('new_product_description');
        $new_quantity = $this->input->post('new_quantity');
        $new_price = $this->input->post('new_price');
        $new_unit_cost = $this->input->post('new_cost');
        $new_amount = $this->input->post('new_amount');  
        
        if(count($ser_invventory_id)>0) {
            foreach ($product_id as $key => $product) {
                if (!empty($product_id[$key])) {
                    $item = array(
                        'category_id' => $category_id[$key],
                        'item_id' => $product_id[$key],
                        'product_description' => $product_description[$key],
                        'price' => $price[$key],
                        'unit_cost' => $unit_cost[$key],
                        'quantity' => $quantity[$key],
                        'total' => $amount[$key],
                        'form_type' => 2,
                        'status' => 1,
                        'branch_id' => $user_data['branch_id'],
                        'updated_by' => $user_data['user_id'],
                        'updated_on' => date('Y-m-d H:i:s')
                    );
                    $this->services_model->update_excinventory_item($item, $ser_invventory_id[$key]);
                    $updat_txt = 1;
                }
            }
        }
        
        if(count($new_product_id)>0) {
            foreach ($new_product_id as $key => $product) {
                if (!empty($new_product_id[$key])) {
                    $item = array(
                        'service_id' => $serv_id,
                        'category_id' => $new_category_id[$key],
                        'item_id' => $new_product_id[$key],
                        'product_description' => $new_product_description[$key],
                        'price' => $new_price[$key],
                        'unit_cost' => $new_unit_cost[$key],
                        'quantity' => $new_quantity[$key],
                        'total' => $new_amount[$key],
                        'form_type' => 2,
                        'branch_id' => $user_data['branch_id'],
                        'created_by' => $user_data['user_id'],
                        'created_on' => date('Y-m-d H:i:s')
                    );
                    $this->services_model->insert_exc_inventory_item($item);
                    $updat_txt = 1;
                }
            }
        }
        
        if ($updat_txt == 1){
            $data_service = $this->services_model->get_by_id($serv_id);
            $this->insert_log($this->log_controler_name, 'Service Items Update', 'view', $serv_id, $data_service->jobsheet_number, '');
            $_SESSION['success'] = 'Service Items Updated successfully';
        }else{
            $_SESSION['error'] = 'Service Items not Updated!';
        }
        
        redirect('services', 'refresh');
    }
    
    public function update_external_item($serv_id) {
        
        $user_data = get_user_data();
        $updat_txt = 0;
        
        $servicedata = array(            
            'external_service_amount' => $this->input->post('total_exter_amt'),
            'external_service_itemadded' => 1,
            'branch_id' => $user_data['branch_id'],
            'updated_by' => $user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s'),
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        $affected_rows = $this->services_model->update($serv_id, $servicedata);
        
        $inactive_rows = $this->services_model->inactive_ser_external_items($serv_id);
        
        
        /***  Service External Items  ***/
        $ser_external_id = $this->input->post('ser_external_id');
        $external_service_id = $this->input->post('external_service_id');
        $exter_service_desc = $this->input->post('exter_service_desc');
        $external_cost = $this->input->post('external_cost');     
        $our_cost = $this->input->post('our_cost');     
        $external_sub_amount = $this->input->post('external_sub_amount');     
        
        $new_external_service_id = $this->input->post('new_external_service_id');
        $new_exter_service_desc = $this->input->post('new_exter_service_desc');
        $new_external_cost = $this->input->post('new_external_cost');     
        $new_our_cost = $this->input->post('new_our_cost');     
        $new_external_sub_amount = $this->input->post('new_external_sub_amount');     
        
        if(count($ser_external_id)>0) {
            foreach ($ser_external_id as $key => $enternal_id) {
                if (!empty($ser_external_id[$key])) {
                    $item = array(
                        'external_service_id' => $external_service_id[$key],
                        'exter_service_desc' => $exter_service_desc[$key],
                        'external_cost' => $external_cost[$key],
                        'our_cost' => $our_cost[$key],
                        'external_sub_amount' => $external_sub_amount[$key],
                        'status' => 1,
                        'branch_id' => $user_data['branch_id'],
                        'updated_by' => $user_data['user_id'],
                        'updated_on' => date('Y-m-d H:i:s')
                    );
                    $this->services_model->update_external_item($item, $ser_external_id[$key]);
                    $updat_txt = 1;
                }
            }
        }
        
        if(count($new_external_service_id)>0) {
            foreach ($new_external_service_id as $key => $external_service) {
                if (!empty($new_external_service_id[$key])) {
                    $item = array(
                        'service_id' => $serv_id,
                        'external_service_id' => $new_external_service_id[$key],
                        'exter_service_desc' => $new_exter_service_desc[$key],
                        'external_cost' => $new_external_cost[$key],
                        'our_cost' => $new_our_cost[$key],
                        'external_sub_amount' => $new_external_sub_amount[$key],
                        'status' => 1,
                        'branch_id' => $user_data['branch_id'],
                        'created_by' => $user_data['user_id'],
                        'created_on' => date('Y-m-d H:i:s')
                    );
                    $this->services_model->insert_external_item($item);
                    $updat_txt = 1;
                }
            }
        }
        
        if ($updat_txt == 1){
            $data_service = $this->services_model->get_by_id($serv_id);
            $this->insert_log($this->log_controler_name, 'External Service Items', 'view', $serv_id, $data_service->jobsheet_number, '');
            $_SESSION['success'] = 'External Service Items Updated successfully';
        }else{
            $_SESSION['error'] = 'External Service Items not Updated!';
        }
        
        redirect('services', 'refresh');
    }

        public function pdf($id) {
        $data = array();
        $user_data = get_user_data();
        $data['service'] = $this->services_model->get_by_id($id);
	$data['service_item'] = $this->services_model->get_item_by_id($id);
        //$data['address'] = $this->purchase_order_model->get_current_address($user_data['branch_id']);
        $data['user_data'] = get_user_data();
        $data['branch'] = $this->company_model->branch_by_id($data['service']->branch_id);
	$data['branches'] = $this->company_model->branch_list_active();
        $data['company'] = $company = $this->company_model->get_company();
        $filename = 'PDF_' . $data['service']->jobsheet_number;
        $filename = strtoupper($filename);
        
        $header = $this->load->view('services/pdf_header', $data, TRUE);//'<div></div>';
        $content = $this->load->view('services/pdf', $data, TRUE);
        $footer = $this->load->view('services/pdf_footer', $data, TRUE);

        #echo $header.$content.$footer;exit;
        $this->load->helper(array('My_Pdf'));
        delivery_order_pdf($header, $content, $footer, $filename);        
        exit(0);
    }
    
    public function get_exter_service_data() {
        $data = array();
        if($this->input->post('ext_serid') != '') {
        $data = $this->services_model->get_exter_service_data($this->input->post('ext_serid'));
        }
        $this->ajax_response($data);
    }
    
    public function status_log($serv_id) {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['service_status_log'] = $this->services_model->service_status_log_details($serv_id);
        $data['service'] = $this->services_model->get_by_id($serv_id);
        $data['status_list'] = $this->services_model->service_status_list();
        $data['form_action'] = base_url() . 'services/update_status_log/'.$serv_id;
        
        $this->render($data, 'services/status_log');
        
    }
    
    public function update_status_log($serv_id) {
        
        $user_data = get_user_data();
        $updat_txt = 0;
        
        $servicedata = array(            
            'service_id' => $serv_id,
            'service_status_id' => $this->input->post('service_status'),
            'log_description' => $this->input->post('log_description'),
            'status' => 1,
            'branch_id' => $user_data['branch_id'],
            'created_by' => $user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s')
        );
        $inrst_id = $this->services_model->insert_status_log($servicedata);
        
        if (!empty($inrst_id)){
            $_SESSION['success'] = 'Service Status Updated successfully';
        }else{
            $_SESSION['error'] = 'Service Status not Updated!';
        }
        
        redirect('services', 'refresh');
    }

}

?>