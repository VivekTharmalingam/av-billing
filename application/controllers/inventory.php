<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inventory extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('inventory_model', '', TRUE);
        $this->load->library('upload');
        $this->load->model('product_model', '', TRUE);
        $this->load->model('company_model', '', TRUE);
        $this->load->library('Datatables');
    }
	
    public function index() {
        $data = array();
        $actions = $this->actions();
        $data['add_btn'] = 'Create Inventory';
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        $data['inventory'] = $this->inventory_model->list_all();
        $data['branch'] = $this->inventory_model->list_branch();
        $data['product'] = $this->inventory_model->list_product();
        $this->render($data, 'inventory/list');
    }
	
    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('bp.id AS bId,bp.product_id AS pId, CASE bp.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str, p.pdt_code AS code, p.bar_code as bar_code,p.s_no as s_no, p.pdt_name AS product, c.cat_name AS cat,b.branch_name AS branch,bp.available_qty AS quantity', FALSE)
                ->where('bp.status != 10 AND bp.status != 2 AND bp.brn_id =' . $this->user_data['branch_id'])
                ->join(TBL_PDT . ' as p', 'p.id = bp.product_id AND p.status != 10')
                ->join(TBL_CAT . ' as c', 'c.id = p.cat_id', 'left')
                ->join(TBL_BRN . ' as b', 'b.id = bp.brn_id', 'left')
                ->add_column('Actions', $this->get_buttons('$1'), 'pId')
                ->from(TBL_BRN_PDT . ' AS bp');
		
        echo $this->datatables->generate();
    }
	
    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" target="_blank" href="' . base_url() . 'item/view/' . $id . '"><span>View</span></a>&nbsp';
        }
        $html.='</span>';
        return $html;
    }
	
}
?>