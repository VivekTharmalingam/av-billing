<?php

/*
 * Created By : Vivek T
 * Purpose : Promotion Module - Purchase with Purchase items Controller
 * Created On : 2k17-02-18 11:35:00
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Purchase_With_Purchase extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('purchase_with_purchase_model', '', TRUE);
        $this->load->model('category_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->library('Datatables');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        $data['pwp'] = $this->purchase_with_purchase_model->list_all();
        $this->render($data, 'purchase_with_purchase/list');
    }

    public function add() {
        $data = array();
        $actions = $this->actions();
        $user_data = get_user_data();
        $data['list_link'] = $actions['index'] . '/';
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['form_action'] = $actions['insert'];
        $data['categories'] = $this->category_model->list_active();
        $data['items'] = $this->product_model->list_active();
        $data['company'] = $this->company_model->get_company();        
        $this->render($data, 'purchase_with_purchase/add');
    }
    
    public function insert() {
        $user_data = get_user_data();
        $insert = array(
            //'from_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('from_date')))),
            //'to_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('to_date')))),
            'item_id' => $this->input->post('purchase_item'),
            'remarks' => $this->input->post('remarks'),
            'status' => $this->input->post('status'),
            'created_by' => $user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s')
        );
        if($this->input->post('from_date') != '') {
           $insert['from_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('from_date'))));
        }
        if($this->input->post('to_date') != '') {
           $insert['to_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('to_date'))));
        }
        $last_id = $this->purchase_with_purchase_model->insert($insert);
        
        if ($last_id > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, '', '');
            $_SESSION['success'] = 'Purchase With Purchase added successfully';
            $items = $this->input->post('items');
            $selling_price = $this->input->post('oprice');
            $offered_price = $this->input->post('price');
            foreach ($items as $key => $item_id) {
                    $item = array(
                        'pwp_id' => $last_id,
                        'offer_item_id' => $item_id,
                        'selling_price' => $selling_price[$key],
                        'offer_price' => $offered_price[$key],
                        'status' => 1,
                        'created_by' => $user_data['user_id'],
                        'created_on' => date('Y-m-d H:i:s')
                    );
            $this->purchase_with_purchase_model->insert_item($item);
            }
            redirect('purchase_with_purchase', 'refresh');
        } else {
            $_SESSION['error'] = 'Purchase With Purchase not added!';
            redirect('purchase_with_purchase/add', 'refresh');
        }
    }
    
    public function edit($id = '') {

        if (empty($id)) {
            show_400_error();
        }
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['items'] = $this->product_model->list_active();
        $data['pwp'] = $this->purchase_with_purchase_model->get_by_id($id);
        $data['pwp_items'] = $this->purchase_with_purchase_model->get_by_items($id);
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' . $data['pwp']->id;

        $this->render($data, 'purchase_with_purchase/edit');
    }

    public function update($id = '') { 
        if (empty($id)) {
            show_400_error();
        }
        $user_data = get_user_data();

        $update = array(
            //'from_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('from_date')))),
            //'to_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('to_date')))),
            'item_id' => $this->input->post('purchase_item'),
            'remarks' => $this->input->post('remarks'),
            'status' => $this->input->post('status'),
            'updated_by' => $user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s')
        ); 
        if($this->input->post('from_date') != '') {
           $update['from_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('from_date'))));
        } else {
           $update['from_date'] = NULL; 
        }
        if($this->input->post('to_date') != '') {
           $update['to_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('to_date'))));
        } else {
           $update['to_date'] = NULL; 
        }
        //echo '<pre>'; print_r($update); exit;
		$edit_id = $this->input->post('item_id');
		$old_id = $this->input->post('edit_pwp_item_id');

		$del_id = array_diff($old_id,$edit_id);
                    foreach($del_id as $key => $addrId) {
                        $update_addr = array(
                            'status' => 10,
                            'updated_on' => date('Y-m-d H:i:s'),
                            'updated_by' => $this->user_data['user_id']
                        );
                        $this->purchase_with_purchase_model->update_item($addrId,$update_addr);
                    }

		//update old Item 	

		if(!empty($edit_id)) {
			$items = $this->input->post('items');
			$selling_price = $this->input->post('oprice');
			$offered_price = $this->input->post('price'); 
                        
                        $val = array();
			foreach($edit_id as $k => $update_id){
				$val['pwp_id'] = $id;
				$val['offer_item_id'] = $items[$k];
                                $val['selling_price'] = $selling_price[$k];
                                $val['offer_price'] = $offered_price[$k];
				$val['status'] = 1;
				$val['updated_on'] = date('Y-m-d H:i:s');
				$val['updated_by'] = $this->user_data['user_id']; 
				$this->purchase_with_purchase_model->update_item($update_id,$val);
			} 
		}
		//Insert new Item

		if (!empty($this->input->post('new_items'))) {
                    $new_item = $this->input->post('new_items');
                    $new_selling_price = $this->input->post('new_oprice');
                    $new_offered_price = $this->input->post('new_price');

                $val = array();

                    foreach($new_item as $key => $value){
                        $val['pwp_id'] = $id;
                        $val['offer_item_id'] = $value;
                        $val['selling_price'] = $new_selling_price[$key];
                        $val['offer_price'] = $new_offered_price[$key];
                        $val['status'] = 1;
                        $val['created_on'] = date('Y-m-d H:i:s');
                        $val['created_by'] = $this->user_data['user_id'];	
                        $last_item_id = $this->purchase_with_purchase_model->insert_item($val); 
                    }
		}

	if ($this->purchase_with_purchase_model->update($id, $update) > 0 || $last_item_id > 0 || $del_id > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', '');
            $_SESSION['success'] = 'Purchase With Purchase updated successfully';
        } else {
            $_SESSION['error'] = 'Purchase With Purchase not updated!';
        }
        redirect('purchase_with_purchase/edit/' . $id, 'refresh');
    }

    public function view($id = '') {

        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['pwp'] = $this->purchase_with_purchase_model->get_by_id($id);
        $data['pwp_items'] = $this->purchase_with_purchase_model->get_by_items($id);
        $this->render($data, 'purchase_with_purchase/view');
    }
    
    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }

        $user_data = get_user_data();
            $pwp = array(
                'status' => 10,
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $user_data['user_id']
            );

        if ($this->purchase_with_purchase_model->update($id, $pwp) > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', 1);
            $pwp_items = array(
                'status' => 10,
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $user_data['user_id']
            );
            $this->purchase_with_purchase_model->delete_item($id, $pwp_items);
            $_SESSION['success'] = 'Purchase With Purchase deleted successfully';
        } else
            $_SESSION['error'] = 'Purchase With Purchase not deleted!';

        redirect('purchase_with_purchase', 'refresh');
    }
    
    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('pwp.id AS bId,p.pdt_name AS item,DATE_FORMAT(pwp.from_date,"%d/%m/%Y") AS f_date,DATE_FORMAT(pwp.to_date,"%d/%m/%Y") AS t_date,CASE pwp.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE)
                ->where('pwp.status != 10')
                ->join(TBL_PDT . ' as p', 'p.id = pwp.item_id', 'left')
                ->add_column('Actions', $this->get_buttons('$1'), 'bId')
                ->from(TBL_PWP . ' AS pwp');
        echo $this->datatables->generate();
    }



    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(3, $this->permission)) {
            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
        }
        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger pop_up_confirm delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Purchase With Purchase!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="PWP"><span>Delete</span></a>';
        }
        $html.='</span>';
        return $html;
    }

}
