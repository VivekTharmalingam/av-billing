<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Count_Sheet extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('company_model', '', TRUE);
        $this->load->model('inventory_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->library('upload');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $user_data = get_user_data();
        $data['company'] = $this->company_model->get_company();
        $actions = $this->actions();
        //$data['form_action'] = $actions['update'] . '/excelimport';
        $this->render($data, 'count_sheet/edit');
    }
    
    public function excelimport() {  
		if($this->input->post('hideval') != ""
			&& ($_FILES["xlsimport"]["type"] == "application/vnd.ms-excel")
			|| ($_FILES["xlsimport"]["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
			|| ($_FILES["xlsimport"]["type"] == "application/octet-stream")) {
			if ($_FILES["xlsimport"]["error"] > 0) {
				echo "Error: " . $_FILES["xlsimport"]["error"] ;
            }
            else {
				/**
				 * PHPExcel_IOFactory
				 * TMC Ulu Tiriam-Membership Soft ware members
				 */

				$this->load->library('ExcelReader/PHPExcel/PHPExcel_IOFactory');
				$inputFileName = $_FILES['xlsimport']['tmp_name'];  // File to read
				
				try {
					$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
				}
				catch(Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
				}
				
				$sheetData = $objPHPExcel->getActiveSheet()->toArray(NULL, true, true, true);
				$num_of_rows = count($sheetData);
				$x = 2;
				while($x <= $num_of_rows) {
					if (count(array_filter($sheetData[$x])) == 0) {
						unset($sheetData[$x]);
					}
					$x++;
				}
				$sheetData = array_filter($sheetData);
				$num_of_rows = count($sheetData);
				$x = 2;

				while($x <= $num_of_rows) {
					$branch_name = ($sheetData[$x]['A']) ? $sheetData[$x]['A'] : '';
					$item_code = ($sheetData[$x]['B']) ? $sheetData[$x]['B'] : '';
					$brand_name = ($sheetData[$x]['C']) ? $sheetData[$x]['C'] : '';
					$item_description = ($sheetData[$x]['D']) ? $sheetData[$x]['D'] : '';
					$quantity = ($sheetData[$x]['E']) ? $sheetData[$x]['E'] : '';
					$price = ($sheetData[$x]['F']) ? $sheetData[$x]['F'] : '';
					$total_price = ($sheetData[$x]['G']) ? $sheetData[$x]['G'] : '';
					$shortage_qty = ($sheetData[$x]['H']) ? $sheetData[$x]['H'] : '';
					$sign = ($sheetData[$x]['I']) ? $sheetData[$x]['I'] : '';
					$reason = ($sheetData[$x]['J']) ? $sheetData[$x]['J'] : '';
					$reason_cpunt_sheet = 'Count Sheet ' . $reason;
					if (($shortage_qty > 0) && (($sign == '+') || ($sign == '-'))) {
						$branch = $this->company_model->get_branch_by_name($branch_name);
						$product_id = $this->product_model->product_id($item_code);
						$inventory_details = array(
							'brn_id' => $branch->id,
							'product_id' => $product_id->id,
							'quantity' => $shortage_qty,
							'is_from_count_sheet' => 1,
							//'adjust_type' => $sign,
							'remarks' => $reason_cpunt_sheet,
							'status' => 1,
							'branch_id' => $this->user_data['branch_id'],
							'created_by' => $this->user_data['user_id'],
							'created_on' => date('Y-m-d H:i:s')
						);
						if($sign == '+') {
							$inventory_details['adjust_type'] = '1';
						} else if($sign == '-') {
							$inventory_details['adjust_type'] = '2';
						}
						
						$id = $this->inventory_model->insert_adjustment($inventory_details);
						$this->product_model->update_branch_stock($branch->id,$product_id->id, $shortage_qty, $sign);
					}
					$x ++;
				}
			}
		}
		
		redirect('count_sheet', 'refresh');
	}
}
?>