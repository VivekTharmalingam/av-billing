<?php

/*
 * Created By : Vivek T
 * Purpose : Promotion Module - Buy 1 Get 1 Controller
 * Created On : 2k17-02-18 05:35:00
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Buy_One_Get_One extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('buy_one_get_one_model', '', TRUE);
        $this->load->model('category_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->library('Datatables');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        $data['pwp'] = $this->buy_one_get_one_model->list_all();
        $this->render($data, 'buy_one_get_one/list');
    }

    public function add() {
        $data = array();
        $actions = $this->actions();
        $user_data = get_user_data();
        $data['list_link'] = $actions['index'] . '/';
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['form_action'] = $actions['insert'];
        $data['categories'] = $this->category_model->list_active();
        $data['items'] = $this->product_model->list_active();
        $data['company'] = $this->company_model->get_company();        
        $this->render($data, 'buy_one_get_one/add');
    }
    
    public function insert() {
        $user_data = get_user_data();
        $insert = array(
            //'from_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('from_date')))),
            //'to_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('to_date')))),
            'item_id' => $this->input->post('purchase_item'),
            'remarks' => $this->input->post('remarks'),
            'status' => $this->input->post('status'),
            'created_by' => $user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s')
        );
        if($this->input->post('from_date') != '') {
           $insert['from_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('from_date'))));
        }
        if($this->input->post('to_date') != '') {
           $insert['to_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('to_date'))));
        }
        $last_id = $this->buy_one_get_one_model->insert($insert);
        
        if ($last_id > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, '', '');
            $_SESSION['success'] = 'Buy One Get One added successfully';
            $items = $this->input->post('items');
            $selling_price = $this->input->post('oprice');
            $offered_price = $this->input->post('price');
            foreach ($items as $key => $item_id) {
                    $item = array(
                        'bogo_id' => $last_id,
                        'offer_item_id' => $item_id,
                        'selling_price' => $selling_price[$key],
                        //'offer_price' => $offered_price[$key],
                        'status' => 1,
                        'created_by' => $user_data['user_id'],
                        'created_on' => date('Y-m-d H:i:s')
                    );
            $this->buy_one_get_one_model->insert_item($item);
            }
            redirect('buy_one_get_one', 'refresh');
        } else {
            $_SESSION['error'] = 'Buy One Get One not added!';
            redirect('buy_one_get_one/add', 'refresh');
        }
    }
    
    public function edit($id = '') {

        if (empty($id)) {
            show_400_error();
        }
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['items'] = $this->product_model->list_active();
        $data['bogo'] = $this->buy_one_get_one_model->get_by_id($id);
        $data['bogo_items'] = $this->buy_one_get_one_model->get_by_items($id);
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' . $data['bogo']->id;

        $this->render($data, 'buy_one_get_one/edit');
    }

    public function update($id = '') { 
        if (empty($id)) {
            show_400_error();
        }
        $user_data = get_user_data();

        $update = array(
            //'from_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('from_date')))),
            //'to_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('to_date')))),
            'item_id' => $this->input->post('purchase_item'),
            'remarks' => $this->input->post('remarks'),
            'status' => $this->input->post('status'),
            'updated_by' => $user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s')
        );  
        if($this->input->post('from_date') != '') {
           $update['from_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('from_date'))));
        } else {
           $update['from_date'] = NULL;
        }
        if($this->input->post('to_date') != '') {
           $update['to_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('to_date'))));
        } else {
           $update['to_date'] = NULL; 
        }
        
		$edit_id = $this->input->post('item_id');
		$old_id = $this->input->post('edit_bogo_item_id');

		$del_id = array_diff($old_id,$edit_id);
                    foreach($del_id as $key => $addrId) {
                        $update_addr = array(
                            'status' => 10,
                            'updated_on' => date('Y-m-d H:i:s'),
                            'updated_by' => $this->user_data['user_id']
                        );
                        $this->buy_one_get_one_model->update_item($addrId,$update_addr);
                    }

		//update old Item 	

		if(!empty($edit_id)) {
			$items = $this->input->post('items');
			$selling_price = $this->input->post('oprice');
			$offered_price = $this->input->post('price'); 
                        
                        $val = array();
			foreach($edit_id as $k => $update_id){
				$val['bogo_id'] = $id;
				$val['offer_item_id'] = $items[$k];
                                $val['selling_price'] = $selling_price[$k];
                                //$val['offer_price'] = $offered_price[$k];
				$val['status'] = 1;
				$val['updated_on'] = date('Y-m-d H:i:s');
				$val['updated_by'] = $this->user_data['user_id']; 
                                //print_r($update_id);
                                //echo '<pre>'; print_r($val); exit;
				$this->buy_one_get_one_model->update_item($update_id,$val);
			} 
		}
		//Insert new Item

		if (!empty($this->input->post('new_items'))) {
                    $new_item = $this->input->post('new_items');
                    $new_selling_price = $this->input->post('new_oprice');
                    $new_offered_price = $this->input->post('new_price');

                $val = array();

                    foreach($new_item as $key => $value){
                        $val['bogo_id'] = $id;
                        $val['offer_item_id'] = $value;
                        $val['selling_price'] = $new_selling_price[$key];
                        //$val['offer_price'] = $new_offered_price[$key];
                        $val['status'] = 1;
                        $val['created_on'] = date('Y-m-d H:i:s');
                        $val['created_by'] = $this->user_data['user_id'];	
                        $last_item_id = $this->buy_one_get_one_model->insert_item($val); 
                    }
		}

	if ($this->buy_one_get_one_model->update($id, $update) > 0 || $last_item_id > 0 || $del_id > 0) {  
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', '');
            $_SESSION['success'] = 'Buy One Get One updated successfully';
        } else {
            $_SESSION['error'] = 'Buy One Get One not updated!';
        }
        redirect('buy_one_get_one/edit/' . $id, 'refresh');
    }

    public function view($id = '') {

        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['bogo'] = $this->buy_one_get_one_model->get_by_id($id);
        $data['bogo_items'] = $this->buy_one_get_one_model->get_by_items($id);
        $this->render($data, 'buy_one_get_one/view');
    }
    
    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }

        $user_data = get_user_data();
            $bogo = array(
                'status' => 10,
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $user_data['user_id']
            );

        if ($this->buy_one_get_one_model->update($id, $bogo) > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', 1);
            $bogo_item = array(
                'status' => 10,
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $user_data['user_id']
            );
            $this->buy_one_get_one_model->delete_item($id, $bogo_item);
                    
            $_SESSION['success'] = 'Buy One Get One deleted successfully';
        } else
            $_SESSION['error'] = 'Buy One Get One not deleted!';

        redirect('buy_one_get_one', 'refresh');
    }
    
    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('b1g1.id AS bId,p.pdt_name AS item,DATE_FORMAT(b1g1.from_date,"%d/%m/%Y") AS f_date,DATE_FORMAT(b1g1.to_date,"%d/%m/%Y") AS t_date,CASE b1g1.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE)
                ->where('b1g1.status != 10')
                ->join(TBL_PDT . ' as p', 'p.id = b1g1.item_id', 'left')
                ->add_column('Actions', $this->get_buttons('$1'), 'bId')
                ->from(TBL_BOGO . ' AS b1g1');
        echo $this->datatables->generate();
    }



    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(3, $this->permission)) {
            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
        }
        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger pop_up_confirm delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Buy One Get One!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="BUY 1 GET 1"><span>Delete</span></a>';
        }
        $html.='</span>';
        return $html;
    }

}
