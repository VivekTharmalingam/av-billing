<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('company_model', '', TRUE);
        $this->load->library('Datatables');
    }

    public function index() {
        $user_data = get_user_data();
        $actions = $this->actions();
        $data = array(
            'add_btn' => 'Create User',
            'view_link' => $actions['view'] . '/',
            'add_link' => $actions['add'],
            'edit_link' => $actions['edit'] . '/',
            'delete_link' => $actions['delete'] . '/',
            'user_data' => $user_data,
            'users' => $this->user_model->list_all(),
            'branches' => $this->company_model->list_active(),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
        );

        $this->render($data, 'user/list');
    }

    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('@i:=@i+1 AS iterator,user.id AS uId,user.branch_id AS branch,user.name AS uname,user.user_name AS username,user.email AS useremail, CASE user.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE)
                ->unset_column('iterator')
                ->unset_column('uId')
                ->where('user.status != 10')
                ->add_column('Actions', $this->get_buttons('$1'), 'uId')
                ->from(TBL_USER . ' AS user,(SELECT @i:=0) AS foo');
        echo $this->datatables->generate();
    }

    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(3, $this->permission)) {
            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
        }

        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger pop_up_confirm delete delete-confirm" href="#" data-confirm-content="You will not be able to recover user details!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="User"><span>Delete</span></a>';
        }
        $html.='</span>';
        return $html;
    }

    public function add() {
        $actions = $this->actions();
        $data = array(
            'form_action' => $actions['insert'],
            'list_link' => $actions['index'],
            'branches' => $this->company_model->list_active(),
            'permissions_struct' => get_permissions_struct(),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
        );

        $this->render($data, 'user/add');
    }
	
    public function insert() {
        $user_data = get_user_data();
        $uploaded_data = uploads(array('upload_path' => 'users', 'field' => 'file'));
        $user = array(
            'branch_id' => implode(',', $this->input->post('branch_ids')),
            'name' => $this->input->post('name'),
            'user_name' => $this->input->post('user_name'),
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password'),
            'user_opts' => '{"fixRibbon":"true","themeId":"smart-style-0","class":" fixed-header fixed-navigation fixed-ribbon"}',
            'is_admin' => ((!empty($this->input->post('is_admin'))) ? 1 : 2),
            'push_notify' => ((!empty($this->input->post('push_notify'))) ? 1 : 0),
            'status' => $this->input->post('status'),
            'permissions' => serialize($this->input->post('permissions')),
            'brn_id' => $user_data['branch_id'],
            'created_on' => date('Y-m-d H:i:s'),
            'created_by' => $user_data['user_id'],
        );

        if (!empty($uploaded_data['file'])) :
            $user['image'] = $uploaded_data['file']['file_path_str'];
        endif;

        $lst_id = $this->user_model->insert($user);
        
        if ($lst_id > 0) :
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $lst_id, '', '');
            $this->session->set_flashdata('success', 'User has been added successfully');
        else :
            $this->session->set_flashdata('error', 'User has been not added');
        endif;

        redirect('user/add', 'refresh');
    }

    public function edit($id = '') {
        if (empty($id)) {
            show_400_error();
        }

        $actions = $this->actions();
        $user = $this->user_model->get_by_id($id);
        $data = array(
            'user' => $user,
            'permissions' => !empty($user->permissions) ? unserialize($user->permissions) : array(),
            'branches' => $this->company_model->list_active(),
            'permissions_struct' => get_permissions_struct(),
            'form_action' => $actions['update'] . '/' . $id,
            'list_link' => $actions['index'],
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'change_password_action' => ''
        );

        $this->render($data, 'user/edit');
    }

    public function update($id = '') {
        if (empty($id)) {
            show_400_error();
        }

        $user_data = get_user_data();
        $user = $this->user_model->get_by_id($id);

        $image = $this->input->post('image');
        if (empty($image)) :
            $image = $this->input->post('hidden-file');
            if (empty($image) && !empty($user->image)) :
                // Remove prev. file
                unlink(FCPATH . UPLOADS . $user->image);
            endif;
        endif;
        
        $uploaded_data = uploads(array('upload_path' => 'users', 'field' => 'file'));

        $user = array(
            'branch_id' => implode(',', $this->input->post('branch_ids')),
            'name' => $this->input->post('name'),
            'user_name' => $this->input->post('user_name'),
            'password' => $this->input->post('password'),
            'email' => $this->input->post('email'),
            'is_admin' => ((!empty($this->input->post('is_admin'))) ? 1 : 2),
            'push_notify' => ((!empty($this->input->post('push_notify'))) ? 1 : 0),
            'status' => $this->input->post('status'),
            'image' => $image,
            'permissions' => serialize($this->input->post('permissions')), // a:1:{s:4:"user";a:2:{i:0;s:1:"1";i:1;s:1:"2";}}
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $user_data['user_id'],
            'brn_id' => $user_data['branch_id'],
        );

        if (!empty($uploaded_data['file']['file_path_str'])) :
            $user['image'] = $uploaded_data['file']['file_path_str'];
        endif;

        if ($this->user_model->update($id, $user) > 0) :
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', '');
            $this->session->set_flashdata('success', 'User has been updated successfully');
        else :
            $this->session->set_flashdata('error', 'User has been not updated');
        endif;
        /*
         * Re-assign user data session
         */
        if( $user_data['user_id'] === $id  ) :
            set_user_data( $id );
        endif;
		redirect('user/edit/' . $id, 'refresh');
    }

    public function view($id = '') {
        if (empty($id)) {
            show_400_error();

        }

        $actions = $this->actions();
        $user = $this->user_model->get_by_id($id);

        $data = array(
            'user' => $user,
            'permissions' => !empty($user->permissions) ? unserialize($user->permissions) : array(),
            'branches' => $this->company_model->list_active(),
            'list_link' => $actions['index'],
            'permissions_struct' => get_permissions_struct(),
        );

        $this->render($data, 'user/view');
    }

    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }

        $user = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $this->user_data['user_id']
        );
		
        if ($this->user_model->update($id, $user) > 0) :
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', 1);
            $this->session->set_flashdata('success', 'User has been deleted successfully');
        else :
            $this->session->set_flashdata('error', 'User has been not deleted');
        endif;
		
        redirect('user', 'refresh');
    }

    function remove($clm = 'image', $id) {
        $user = $this->user_model->get_by_id($id);
        $args = array(
            $clm => ''
        );
        $this->user_model->update($id, $args);
        if ($clm == 'image') :
            // Unlink file
            unlink(FCPATH . UPLOADS . $user->image);
        endif;
    }
	
    function update_db($clm, $id) {
        $data = $this->input->post('data');
        if (empty($data)) :
            return '';
        endif;

        $args = array(
            'user_opts' => json_encode($data)
        );
        $this->user_model->update($id, $args);
    }

    public function check_user_name_exists() {
        $user_name = $_POST['user_name'];
        echo $this->user_model->check_user_name_exists($user_name, $_POST['user_id']);
    }

    public function check_email_id_exists() {
        $email_id = $_POST['email_id'];
        echo $this->user_model->check_email_id_exists($email_id, $_POST['user_id']);
    }
	
    public function email_exists() {
        $data = array();
        $data['email_exists'] = $this->user_model->email_exists($this->input->post('email'), $this->input->post('user_id'));

        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }
	
    public function switch_branch($branch_id = '') {
        $data = array();
        $data['success'] = '';
        $data['error'] = '';
        if ($branch_id) {
            $user_data = $this->session->userdata( TBL_PREFIX . 'user_logged_in');
            $user_data['branch_id'] = $branch_id;
            $this->session->set_userdata( TBL_PREFIX . 'user_logged_in', $user_data);
            $_SESSION['switch_branch'] = 'S';
        }
        else {
            $_SESSION['switch_branch'] = 'F';
        }
        
        redirect($this->input->server('HTTP_REFERER'));
        #$this->ajax_response($data);
    }
}
?>