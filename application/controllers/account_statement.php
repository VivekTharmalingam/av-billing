<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class account_statement extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('account_statement_model', '', TRUE);
        $this->load->model('customer_model', '', TRUE);
        $this->load->model('company_model', '', TRUE);
    }

    public function index() {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = base_url() . 'invoice/view' . '/';
        $data['form_action'] = base_url() . 'account_statement';
        $from = '';
        $to = '';
        $data['search_date'] = '';
        $data['list_data'] = '0';
        
        if ($this->input->post('search_date') != '') {
            $data['search_date'] = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : '';
            $search_date = explode('-', $this->input->post('search_date'));
            $from = convert_text_date(trim($search_date[0]));
			if (!empty($search_date[1])) {
				$to = convert_text_date(trim($search_date[1]));
			}
			else {
				$from = '';
				$to = convert_text_date(trim($search_date[0]));
			}
            $data['list_data'] = '1';
        }
		#echo $from . '<br />';		echo $to;
		
        $data['from_date'] = $from;
        $data['to_date'] = $to;
        $data['custom'] = $this->input->post('cus_name');
        $data['pay_sts'] = $this->input->post('payment_status');
        $data['pay_term'] = $this->input->post('payment_terms');
        $data['payment_terms'] = payment_terms();
        $data['company'] = $this->company_model->get_company();
        $data['cus_details'] = '';
        if (!empty($this->input->post('cus_name'))) {
            $data['cus_details'] = $this->customer_model->get_by_id($this->input->post('cus_name'));
        }
        $data['invoices'] = $this->account_statement_model->statement_summary($from, $to, $this->input->post('cus_name'), $this->input->post('payment_status'));
		//echo $this->db->last_query();exit;
		$data['paT'] = $this->account_statement_model->get_static_pay_type_by_customer($from, $to, $this->input->post('cus_name'));
		
		$current = $this->minusDayswithdate($to, 30, $from);
        $data['curent_amt'] = $this->account_statement_model->pending_account_summary($current, $to, $this->input->post('cus_name'));
        
		$one = $this->minusDayswithdate($current, 30, $from);
        $data['one'] = $this->account_statement_model->pending_account_summary($one, $current, $this->input->post('cus_name'));
        
		$two = $this->minusDayswithdate($one, 30, $from);
        $data['two'] = $this->account_statement_model->pending_account_summary($two, $one, $this->input->post('cus_name'));
        
		$three = $this->minusDayswithdate($two, 30, $from);
        $data['three'] = $this->account_statement_model->pending_account_summary($three, $two, $this->input->post('cus_name'));
        
		$four = $this->minusDayswithdate($three, 30, $from);
        $data['four'] = $this->account_statement_model->pending_account_summary($from, $three, $this->input->post('cus_name'));
		#exit;
        #$five = $this->minusDayswithdate($four, 30);
        #$data['five'] = $this->account_statement_model->pending_account_summary('', $four, $this->input->post('cus_name'));      
        if ($this->input->post('reset_btn')) {
            $data['cus_details'] = '';
            $data['list_data'] = '0';
            $data['search_date'] = '';
            $data['custom'] = '';
            $data['pay_sts'] = '';
            $data['pay_term'] = '';
            $data['invoices'] = $this->account_statement_model->statement_summary();
        }
        $data['customers'] = $this->customer_model->list_active();

        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Statement of Account';
            $head['setting'] = $this->company_model->get_company();
            $filename = 'Account_Statement_' . current_date();            
            $header = $this->load->view('account_statement/pdf_header', $data, TRUE);//'<div></div>';
            $content = $this->load->view('account_statement/pdf', $data, TRUE);
            $footer = $this->load->view('account_statement/pdf_footer', $data, TRUE);
            $this->load->helper(array('My_Pdf'));
            
            invoice_pdf($header, $content, $footer, $filename);            
            exit(0);
        }
		elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $this->load->view('account_statement/excel_report', $data);
        }
		else {
            $this->render($data, 'account_statement/list');
        }
    }

    public function minusDayswithdate($date, $days, $from_date = '') {
        $date = strtotime("-" . $days . " days", strtotime($date));
		if (!empty($from_date)) {
			$date = ($date >= strtotime($from_date)) ? $date : strtotime("-1 day", strtotime($from_date));
		}
		
        return date("Y-m-d", $date);
    }

}

?>
