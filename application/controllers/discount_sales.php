<?php

/*
 * Created By : Vivek T
 * Purpose : Promotion Module - Discount Sales Controller
 * Created On : 2k17-02-20 02:35:00
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Discount_sales extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('discount_sales_model', '', TRUE);
        $this->load->model('category_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->library('Datatables');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        $data['discount_sales'] = $this->discount_sales_model->list_all();
        $this->render($data, 'discount_sales/list');
    }

    public function add() {
        $data = array();
        $actions = $this->actions();
        $user_data = get_user_data();
        $data['list_link'] = $actions['index'] . '/';
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['form_action'] = $actions['insert'];
        $data['categories'] = $this->category_model->list_active();
        $data['items'] = $this->product_model->list_active();
        $data['company'] = $this->company_model->get_company();        
        $this->render($data, 'discount_sales/add');
    }
    
    public function insert() {
        $user_data = get_user_data();
        $insert = array(
            //'from_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('from_date')))),
            //'to_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('to_date')))),
            'offer_item_id' => $this->input->post('item'),
            'selling_price' => $this->input->post('selling_price'),
            'offer_percentage' => $this->input->post('offer_percentage'),
            'offer_price' => $this->input->post('offer_price'),
            'remarks' => $this->input->post('remarks'),
            'status' => $this->input->post('status'),
            'created_by' => $user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s')
        );
        if($this->input->post('from_date') != '') {
           $insert['from_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('from_date'))));
        }
        if($this->input->post('to_date') != '') {
           $insert['to_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('to_date'))));
        }
        $last_id = $this->discount_sales_model->insert($insert);
        
        if ($last_id > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, '', '');
            $_SESSION['success'] = 'Discount Sales added successfully';
            redirect('discount_sales', 'refresh');
        } else {
            $_SESSION['error'] = 'Discount Sales not added!';
            redirect('discount_sales/add', 'refresh');
        }
    }
    
    public function edit($id = '') {

        if (empty($id)) {
            show_400_error();
        }
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['items'] = $this->product_model->list_active();
        $data['ds'] = $this->discount_sales_model->get_by_id($id);
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' . $data['ds']->id;

        $this->render($data, 'discount_sales/edit');
    }

    public function update($id = '') { 
        if (empty($id)) {
            show_400_error();
        }
        $user_data = get_user_data();

        $update = array(
            //'from_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('from_date')))),
            //'to_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('to_date')))),
            'offer_item_id' => $this->input->post('item'),
            'selling_price' => $this->input->post('selling_price'),
            'offer_percentage' => $this->input->post('offer_percentage'),
            'offer_price' => $this->input->post('offer_price'),
            'remarks' => $this->input->post('remarks'),
            'status' => $this->input->post('status'),
            'updated_by' => $user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s')
        ); 
        if($this->input->post('from_date') != '') {
           $update['from_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('from_date'))));
        } else {
           $update['from_date'] = NULL;
        }
        if($this->input->post('to_date') != '') {
           $update['to_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('to_date'))));
        } else {
           $update['to_date'] = NULL; 
        }
	if ($this->discount_sales_model->update($id, $update) > 0) { 
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', '');
            $_SESSION['success'] = 'Discount Sales updated successfully';
        } else { 
            $_SESSION['error'] = 'Discount Sales not updated!';
        }
        redirect('discount_sales/edit/' . $id, 'refresh');
    }

    public function view($id = '') {

        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['ds'] = $this->discount_sales_model->get_by_id($id);
        $this->render($data, 'discount_sales/view');
    }
    
    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }

        $user_data = get_user_data();
            $ds = array(
                'status' => 10,
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $user_data['user_id']
            );

        if ($this->discount_sales_model->update($id, $ds) > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', 1);
            $_SESSION['success'] = 'Discount Sales deleted successfully';
        } else
            $_SESSION['error'] = 'Discount Sales not deleted!';

        redirect('discount_sales', 'refresh');
    }
    
    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('ds.id AS bId, p.pdt_name AS name, ds.offer_price as offer_price, DATE_FORMAT(ds.from_date,"%d/%m/%Y") AS f_date, DATE_FORMAT(ds.to_date,"%d/%m/%Y") AS t_date, CASE ds.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE)
                ->where('ds.status != 10')
                ->join(TBL_PDT . ' as p', 'p.id = ds.offer_item_id', 'left')
                ->add_column('Actions', $this->get_buttons('$1'), 'bId')
                ->from(TBL_DISC_SALE . ' AS ds');
        echo $this->datatables->generate();
    }



    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(3, $this->permission)) {
            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
        }
        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger pop_up_confirm delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Discount Sales!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Discount Sales"><span>Delete</span></a>';
        }
        $html.='</span>';
        return $html;
    }

}
