<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inventory_By_Location extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('inventory_model', '', TRUE);
        $this->load->library('upload');
    }

    public function index() {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = base_url() . 'inventory_by_location/view' . '/';
        $data['form_action'] = base_url() . 'inventory_by_location';

        $data['branch_id'] = $this->input->post('branch_id');
        $data['brand_id'] = $this->input->post('brand_id');
        $data['inventory'] = $this->inventory_model->list_search_all($data['branch_id'], $data['brand_id']);
        $data['branch'] = $this->inventory_model->list_branch();
        $data['brand'] = $this->inventory_model->list_brand();

        if ($this->input->post('reset_btn')) {
            $data['search_date'] = '';
            $data['branch_id'] = '';
            $data['brand_id'] = '';
            $data['inventory'] = $this->inventory_model->list_search_all();
        }

        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Inventory By Location Reports';
            $head['setting'] = $this->company_model->get_company();
            $filename = 'Inventory_By_Location_Report_' . current_date();

            // Load Views
            $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
            $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
            $header = '<div></div>';
            $content = $this->load->view('inventory_by_location/pdf_report', $data, TRUE);
            $footer = $this->load->view('templates/pdf/pdf_footer', '', TRUE);
            $this->load->helper(array('My_Pdf'));

            create_pdf($header, $content, $footer, $filename);
            exit(0);
        } elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $this->load->view('inventory_by_location/excel_report', $data);
        } else {
            $this->render($data, 'inventory_by_location/list');
        }
    }

    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['product'] = $this->inventory_model->get_product_by_id($id);
        $this->render($data, 'inventory_by_location/view');
    }

}

?>