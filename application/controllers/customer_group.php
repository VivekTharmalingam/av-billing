<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer_group extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('customer_group_model', '', TRUE);
        $this->load->library('upload');
        $this->load->library('Datatables');
        $this->load->library('table');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['add_btn'] = 'Create Customer Group';
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        $data['customer_group'] = $this->customer_group_model->list_all();
        $this->render($data, 'customer_group/list');
    }

    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('grp.id AS tId,grp.customer_group AS name,DATE_FORMAT(grp.created_on,"%d/%m/%Y") AS crated_date,CASE grp.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE)
//                ->unset_column('iterator')
                ->where('grp.status != 10')
                ->add_column('Actions', $this->get_buttons('$1'), 'tId')
                ->from(TBL_CUST_GROUP . ' AS grp');
        echo $this->datatables->generate();
    }

    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(3, $this->permission)) {
            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
        }
        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger pop_up_confirm delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Customer Group details!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Customer Group"><span>Delete</span></a>';
        }
        $html.='</span>';
        return $html;
    }

    public function add() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['form_action'] = $actions['insert'];
        $data['list_link'] = $actions['index'];
        $this->render($data, 'customer_group/add');
    }

    public function insert() {
        $user_data = get_user_data();
        $prefix_wt_sub = CODE_CUST_GROUP;
        $code = $this->auto_generation_code(TBL_CUST_GROUP, $prefix_wt_sub, '', 3, '');
        $insert = array(
            'customer_group_code' => $code,
            'customer_group' => $this->input->post('customer_group'),
            'notes' => $this->input->post('desc'),
            'status' => $this->input->post('status'),
            'branch_id' => $this->user_data['branch_id'],
            'created_by' => $this->user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s')
        );
        $last_id = $this->customer_group_model->insert($insert);
        if ($last_id > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, '', '');
            $_SESSION['success'] = 'Customer Group added successfully';
        } else {
            $_SESSION['error'] = 'Customer Group not added!';
        }
        redirect('customer_group/add', 'refresh');
    }

    public function edit($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['customer_group'] = $this->customer_group_model->get_by_id($id);
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' . $data['customer_group']->id;
        $this->render($data, 'customer_group/edit');
    }

    public function update($id = '') {
        if (empty($id)) {
            show_400_error();
        }
		
        $prefix_wt_sub = CODE_CUST_GROUP;        
        $code = $this->auto_generation_code(TBL_CUST_GROUP, $prefix_wt_sub, '', 3, $id);
        $update = array(
            'customer_group_code' => $code,
            'customer_group' => $this->input->post('customer_group'),
            'notes' => $this->input->post('desc'),
            'status' => $this->input->post('status'),
            'branch_id' => $this->user_data['branch_id'],
            'updated_by' => $this->user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s')
        );
        if ($this->customer_group_model->update($id, $update) > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', '');
            $_SESSION['success'] = 'Customer Group updated successfully';
        } else {
            $_SESSION['error'] = 'Customer Group not updated!';
        }
        redirect('customer_group/edit/' . $id, 'refresh');
    }

    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['customer_group'] = $this->customer_group_model->get_by_id($id);
        $this->render($data, 'customer_group/view');
    }

    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }
        $exp = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $this->user_data['user_id']
        );
        if ($this->customer_group_model->update($id, $exp) > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', 1);
            $_SESSION['success'] = 'Customer Group deleted successfully';
        } else {
            $_SESSION['error'] = 'Customer Group not deleted!';
        }
        redirect('customer_group', 'refresh');
    }

    public function check_exp_exists() {
        $exp = $_REQUEST['exp_name'];
        echo $this->customer_group_model->check_exp_exists($exp, $_REQUEST['group_id']);
    }
}
?>