<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Price_List extends REF_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model('product_model', '', TRUE);
		$this->load->model('inventory_model', '', TRUE);
        $this->load->library('upload');
    }
	
    public function index() {
        $data = array();
		$data1 = '';
		$head = array();
        $data['success']=$this->data['success'];  
        $data['error']=$this->data['error'];       
        $actions = $this->actions();
        $data['view_link']  = base_url() . 'item/view' . '/';
		$data['form_action'] = base_url() . 'price_list';
		
		$brand_id = $this->input->post('brand_id');
		$category_id = $this->input->post('category_id');
		$sub_category_id = $this->input->post('sub_category_id');
		$data['brand']    = $this->inventory_model->list_brand();
		$data['item_category'] = $this->product_model->item_category();
		if($this->input->post('reset_btn')) {
			$brand_id ='';
			$category_id = '';
			$sub_category_id = '';
        }
		
		$data['product'] = $this->product_model->get_active_items_price_list($brand_id, $category_id, $sub_category_id);
		
		$data['brand_id'] = $brand_id;
		$data['category_id'] = $category_id;
		$data['sub_category_id'] = $sub_category_id;
		
		if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Price List Reports';
		    $head['setting'] = $this->company_model->get_company();
			$filename = 'Price_list_' . current_date();
			
			// Load Views
			$data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
			$data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
			$header = '<div></div>';
			$content = $this->load->view('price_list/pdf_report', $data, TRUE);			
			$footer = '<div></div>';
			$this->load->helper(array('My_Pdf'));
			create_pdf($header, $content, $footer, $filename);
			exit(0);
			
		} elseif(((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel')) ){
			$this->load->view('price_list/excel_report', $data);
			
		}else {
			$this->render($data, 'price_list/list');
		}
		
    }
	
    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }        
        $data = array();
        $data['customer']   = $this->customer_model->get_by_id($id);
		$data['ship_address']   = $this->customer_model->get_address_by_id($id);
			
        $this->render($data, 'customer/view');
    }
}
?>