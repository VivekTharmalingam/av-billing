<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payslip_Generation_Reports extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('payslip_model', '', TRUE);
        $this->load->library('upload');
    }

    public function month_list() {
        $month = array(
            '1' => 'Janaury',
            '2' => 'February',
            '3' => 'March',
            '4' => 'April',
            '5' => 'May',
            '6' => 'June',
            '7' => 'July',
            '8' => 'August',
            '9' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December'
        );

        return $month;
    }

    public function index() {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = base_url() . 'payslip/view' . '/';
        $data['form_action'] = base_url() . 'payslip_generation_reports';

        $from = '';
        $to = '';
        $data['search_date'] = '';

        if ($this->input->post('search_date') != '') {
            $data['search_date'] = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : '';
            $search_date = explode('-', $this->input->post('search_date'));
            $from = trim($search_date[0]);
            $to = trim($search_date[1]);

            $from = explode('/', $from);
            $from = $from[2] . '-' . $from[1] . '-' . $from[0];

            $to = explode('/', $to);
            $to = $to[2] . '-' . $to[1] . '-' . $to[0];
        }
        $data['status_id'] = $this->input->post('status_id');

        $data['from_date'] = $from;
        $data['to_date'] = $to;
        $data['payslip'] = $this->payslip_model->list_search_all($from, $to, $data['status_id']);
        $data['employee'] = $this->payslip_model->list_employee();
        $data['month'] = $this->month_list();

        if ($this->input->post('reset_btn')) {
            $data['search_date'] = '';
            $data['status_id'] = '';
            $data['payslip'] = $this->payslip_model->list_search_all();
        }

        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Payslip Generation Reports';
            $head['setting'] = $this->company_model->get_company();
            $filename = 'Payslip_Generaton_Report_' . current_date();

            // Load Views
            $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
            $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
            $header = '<div></div>';
            $content = $this->load->view('payslip_generation_report/pdf_report', $data, TRUE);
            $footer = '<div></div>';
            $this->load->helper(array('My_Pdf'));

            create_pdf($header, $content, $footer, $filename);
            exit(0);
        } elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $this->load->view('payslip_generation_report/excel_report', $data);
        } else {
            $this->render($data, 'payslip_generation_report/list');
        }
    }

    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['customer'] = $this->customer_model->get_by_id($id);
        $data['ship_address'] = $this->customer_model->get_address_by_id($id);

        $this->render($data, 'customer/view');
    }

}

?>