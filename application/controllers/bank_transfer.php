<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bank_Transfer extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('bank_transfer_model', '', TRUE);
        $this->load->model('payment_mode_model', '', TRUE);
        $this->load->model('bank_model', '', TRUE);
        $this->load->library('upload');
        $this->load->library('Datatables');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['add_btn'] = 'Add Transfer';
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        $data['deduction'] = $this->bank_transfer_model->list_all();
        $this->render($data, 'bank_transfer/list');
    }

    public function add() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['active_bank'] = $this->bank_transfer_model->get_active_bank();
        $data['payment_types'] = $this->payment_mode_model->list_active_payment_mode();
        $data['form_action'] = $actions['insert'];
        $data['list_link'] = $actions['index'];
        $this->render($data, 'bank_transfer/add');
    }
    
    public function insert() {
        
        $user_data = get_user_data();
        $insert_data = array(
            'from_bank_id' => $this->input->post('frm_account_id'),
            'to_bank_id' => $this->input->post('to_account_id'),
            'amount' => $this->input->post('trans_amount'),
            'reason' => $this->input->post('reason'),
            'payment_type' => $this->input->post('payment_type'),
            #'status' => $this->input->post('status'),
            'branch_id' => $this->user_data['branch_id'],
            'created_by' => $this->user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s')
        );
        
        $payment_type = $this->payment_mode_model->get_by_id($this->input->post('payment_type'));
        if ($payment_type->is_cheque == 1) {
            $insert_data['cheque_acc_no'] = $this->input->post('cheque_acc_no');
            $insert_data['cheque_bank_name'] = $this->input->post('cheque_bank_name');
            $insert_data['cheque_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('cheque_date'))));
        }
        $last_id = $this->bank_transfer_model->insert($insert_data);

        if ($last_id > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, '', '');
            $update_data = array(
                'updated_by' => $this->user_data['user_id'],
                'updated_on' => date('Y-m-d H:i:s')
            );
            $this->bank_model->update_bank_amount($this->input->post('frm_account_id'), $update_data, $this->input->post('trans_amount'), '-');
            
            $this->bank_model->update_bank_amount($this->input->post('to_account_id'), $update_data, $this->input->post('trans_amount'), '+');
            $_SESSION['success'] = 'Bank Transfer Record added successfully';
        } else {
            $_SESSION['error'] = 'Bank Transfer Record not added!';
        }
        
        redirect('bank_transfer/add', 'refresh');
    }
    
    public function edit($id = '') {

        if (empty($id)) {
            show_400_error();
        }
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['transfer'] = $this->bank_transfer_model->get_by_id($id);
        $data['active_bank'] = $this->bank_transfer_model->get_active_bank();
        $data['payment_types'] = $this->payment_mode_model->list_active_payment_mode();
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' . $data['transfer']->id;

        $this->render($data, 'bank_transfer/edit');
    }
    
    public function update($id = '') {

        if (empty($id)) {
            show_400_error();
        }
        
        $update_data = array(
            'from_bank_id' => $this->input->post('frm_account_id'),
            'to_bank_id' => $this->input->post('to_account_id'),
            'amount' => $this->input->post('trans_amount'),
            'reason' => $this->input->post('reason'),
            'payment_type' => $this->input->post('payment_type'),
            #'status' => $this->input->post('status'),
            'branch_id' => $this->user_data['branch_id'],
            'updated_by' => $this->user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s')
        );
        
        $payment_type = $this->payment_mode_model->get_by_id($this->input->post('payment_type'));
        if ($payment_type->is_cheque == 1) {
            $update_data['cheque_acc_no'] = $this->input->post('cheque_acc_no');
            $update_data['cheque_bank_name'] = $this->input->post('cheque_bank_name');
            $update_data['cheque_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('cheque_date'))));
        }
        
        
        if ($this->bank_transfer_model->update($id, $update_data) > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', '');
            $_SESSION['success'] = 'Bank Transfer Record updated successfully';
        } else {
            $_SESSION['error'] = 'Bank Transfer Record not updated!';
        }
        
        redirect('bank_transfer/edit/' . $id, 'refresh');
    }
    
    public function view($id = '') {

        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['transfer'] = $this->bank_transfer_model->get_by_id($id);
        $this->render($data, 'bank_transfer/view');
    }
    
    public function delete($id) {

        if (empty($id)) {
            show_400_error();
        }
        
        $transfer = $this->bank_transfer_model->get_by_id($id);
        
        $credit_bank_details = $this->bank_model->get_by_id($transfer->to_bank_id);
        if($transfer->amount <= $credit_bank_details->current_balance) {
            $exp = array(
                'status' => 10,
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $this->user_data['user_id']
            );

            if ($this->bank_transfer_model->update($id, $exp) > 0) {
                $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', 1);
                $update_data = array(
                    'updated_by' => $this->user_data['user_id'],
                    'updated_on' => date('Y-m-d H:i:s')
                );
                $this->bank_model->update_bank_amount($transfer->from_bank_id, $update_data, $transfer->amount, '+');
                $this->bank_model->update_bank_amount($transfer->to_bank_id, $update_data, $transfer->amount, '-');
                $_SESSION['success'] = 'Bank Transfer Record deleted successfully';
            } else {
                $_SESSION['error'] = 'Bank Transfer Record not deleted!';
            }
        } else {
            $_SESSION['error'] = 'Insufficient Amount in <b>"' . $credit_bank_details->account_no . '/'. $credit_bank_details->bank_name . '"</b>. So, this Record is couldn\'t be deleted!';
        }

        redirect('bank_transfer', 'refresh');
    }
    
    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('bt.id AS btId, IF(payt.is_cheque = 1 AND bt.cheque_acc_no != "", CONCAT(payt.type_name, "&nbsp;/ ", bt.cheque_bank_name, "&nbsp;- ", bt.cheque_acc_no), payt.type_name) as type_name, CONCAT(bnk1.account_no," / ",bnk1.bank_name) AS debitbank, CONCAT( bnk2.account_no," / ",bnk2.bank_name) AS creditbank, bt.amount AS btAmount', FALSE)
                ->where('bt.status != 10')
                ->join(TBL_BANK . ' as bnk1', 'bnk1.id = bt.from_bank_id', 'left')
                ->join(TBL_BANK . ' as bnk2', 'bnk2.id = bt.to_bank_id', 'left')
                ->join(TBL_PAYMENT_MODE . ' as payt', 'payt.id = bt.payment_type', 'left')
                ->add_column('Actions', $this->get_buttons('$1'), 'btId')
                ->from(TBL_BANK_TRANSFER . ' AS bt');
        echo $this->datatables->generate();
    }


    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(3, $this->permission)) {
            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
        }
        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger pop_up_confirm delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Bank Transfer Record!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Deduction Record"><span>Delete</span></a>';
        }
        $html.='</span>';
        return $html;

    }
}

?>