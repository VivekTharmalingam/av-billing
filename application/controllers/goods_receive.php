<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Goods_Receive extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('purchase_receive_model', '', TRUE);
        $this->load->model('purchase_order_model', '', TRUE);
        $this->load->model('supplier_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->model('company_model', '', TRUE);
        $this->load->library('Datatables');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        $data['print_link'] = base_url() . 'goods_receive/pdf/';
        $data['purchases'] = $this->purchase_receive_model->list_all();
        $this->render($data, 'purchase_receive/list');
    }

    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('pr.id AS pId, brn.branch_name AS branch, DATE_FORMAT(pr.invoice_date, "%d/%m/%Y") as pr_invoice_date, pr.pr_code, DATE_FORMAT(pr.pr_date, "%d/%m/%Y") as pr_current_date, pr.supplier_invoice_no AS inv_no, pr.pr_net_amount AS net, sup.name AS supplier, CASE pr.payment_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as status_str, pr.credit_notes AS notes', FALSE)
                ->where('pr.pr_status != 10 AND pr.branch_id IN ("' . $this->user_data['branch_id'] . '")')
                ->join(TBL_SUP . ' as sup', 'sup.id = pr.sup_id', 'left')
                ->join(TBL_BRN . ' as brn', 'brn.id = pr.branch_id', 'left')
                ->add_column('Actions', $this->get_buttons('$1'), 'pId')
                ->from(TBL_PR . ' AS pr');
        echo $this->datatables->generate();
    }

    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(6, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . base_url() . 'goods_receive/pdf/' . $id . '" title="Print Invoice Details"><span><i class="fa fa-file-pdf-o" aria-hidden="true"></i></span></a>&nbsp';
        }
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(3, $this->permission)) {
            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
        }
        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Goods Receives!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Goods Receives"><span>Delete</span></a>';
        }
        $html.='</span>';
        return $html;
    }

    public function add($po_id = '') {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['form_action'] = $actions['insert'];
        //$data['purchases'] = $this->purchase_receive_model->get_purchase_list();
        //$data['items'] = $this->purchase_order_model->get_all_items();
        $data['suppliers'] = $this->purchase_order_model->get_all_suppliers();
        $data['company'] = $this->company_model->get_company();
        $data['add_link'] = $actions['add'] . '/';
        $data['add_item_link'] = base_url() . 'item/add/';
        $data['add_supplier_link'] = base_url() . 'supplier/add/';
        $data['add_po_link'] = base_url() . 'purchase_orders/add/';
		$data['po_id'] = '';
		$data['purchase_order'] = new stdClass();
		$data['po_items'] = array();
		if (!empty($po_id)) {
			$data['po_id'] = $po_id;
			$data['purchase_order'] = $this->purchase_order_model->get_by_id($data['po_id']);
			$data['po_items'] = $this->purchase_receive_model->get_all_po_items($data['po_id']);
		}
        $this->render($data, 'purchase_receive/add');
    }

    public function get_branch_by_po() {
        $po_id = $_REQUEST['po_id'];
        $data = array();
        if (!empty($po_id)) {
            $data['brn'] = $this->purchase_receive_model->get_branch_by_po($po_id);
            $data['supplier'] = $this->purchase_receive_model->get_supplier_by_po($po_id);
        }
        #$user_data = get_user_data();
        #$data['branches'] = $this->purchase_order_model->get_all_branches($user_data['branch_id']);
        $this->ajax_response($data);
    }

    public function get_products_by_po() {
        $po_id = $_REQUEST['po_id'];
        $data = array();
        if (!empty($po_id)) {
            $data['products'] = $this->purchase_receive_model->get_products_by_po($po_id);
            $data['po_detail'] = $this->purchase_receive_model->get_supplier_by_po($po_id);
        }
        $this->ajax_response($data);
    }

    public function get_all_po_items() {
        $po_id = $_REQUEST['po_id'];
        $data = array();
        $data = $this->purchase_receive_model->get_all_po_items($po_id);
        foreach ($data as $key => $value) {
            //$value->value = $value->id;
            if (!empty($product_code))
                $value->label = $value->pdt_code;
            else
                $value->label = $value->pdt_code . ' / ' . $value->pdt_name;

            $value->value = $value->label;
        }
        $this->ajax_response($data);
    }

    public function get_item_details() {
        $mat_id = $_REQUEST['mat_id'];
        $po_id = $_REQUEST['po_id'];
        $pr_id = $_REQUEST['pr_id'];
        $data = array();
        if ((!empty($po_id)) && (!empty($mat_id))) {
            $data['items'] = $this->purchase_receive_model->get_item_details($mat_id, $po_id);
            //error_log($this->db->last_query());
            $data['bal_qty'] = $this->purchase_receive_model->check_mat_already_exists($mat_id, $po_id, $pr_id);
            //error_log($this->db->last_query());
        }
        echo json_encode($data);
    }

    public function insert() {
		$prefix_wt_sub = CODE_PR;
        $grn_no = $this->auto_generation_code(TBL_PR, $prefix_wt_sub, '', 3, '');
        $user_data = get_user_data();
		
		$sub_total = $this->input->post('sub_total');
		$discount_amt = $this->input->post('discount_amt');
        $gst_amount = $this->input->post('gst_amount');
		$total_amt = $this->input->post('total_amt');
		$usd_net_amount = 0;
		if ($this->input->post('currency') == 2) {
			$sub_total *= $this->input->post('exchange_rate');
			$discount_amt *= $this->input->post('exchange_rate');
			$gst_amount *= $this->input->post('exchange_rate');
			$usd_net_amount = $total_amt;
			$total_amt *= $this->input->post('exchange_rate');
		}
        $insert = array(
            'pr_code' => $grn_no,
            //'brn_id' => $user_data['branch_id'],
            'pr_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('pr_date')))),
            'invoice_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('inv_date')))),
            'sup_id' => $this->input->post('sup_name'),
            'supplier_invoice_no' => $this->input->post('sup_invoice'),
            'pr_your_info' => $this->input->post('your_info'),
            'pr_sub_total' => $sub_total,
            'pr_discount' => $this->input->post('discount'),
            'pr_discount_type' => $this->input->post('discount_type'),
            'pr_total_discount' => $discount_amt,
            'pr_gst_type' => $this->input->post('gst_type'),
            'pr_gst_val' => $this->input->post('hidden_gst'),
            'pr_gst_amount' => $gst_amount,
            'pr_net_amount' => $total_amt,
			'currency' => $this->input->post('currency'),
			'exchange_rate' => $this->input->post('exchange_rate'),
            'usd_net_amount' => $usd_net_amount,
            'credit_notes' => (!empty($this->input->post('credit_notes'))) ? 1 : 0,
            'credit_amount' => 0,
            'pr_status' => 1,
            'received_status' => 1,
            'created_by' => $user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s'),
            'branch_id' => $user_data['branch_id'],
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
		
        $last_id = $this->purchase_receive_model->insert($insert);
		if ($last_id > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, $grn_no, '');
            $category_id = $this->input->post('category_id');
            $product_id = $this->input->post('product_id');
            $product_description = $this->input->post('product_description');
            $sl_no = $this->input->post('sl_no');
            $sl_itm_code = $this->input->post('sl_itm_code');
            $quantity = $this->input->post('quantity');
            $price = $this->input->post('price');
            $price_after_gst = $this->input->post('price_after_gst');
            $unit_cost = $this->input->post('cost');
            $item_discount = $this->input->post('item_discount');
            $item_discount_amt = $this->input->post('item_discount_amt');
            $item_discount_type = $this->input->post('item_discount_type');
            $amount = $this->input->post('amount');

            if (!empty($product_id)) {
                foreach ($product_id as $key => $item_id) {
                    if (!empty($item_discount_type[$key])) {
                        $discount_type = 1;
                        $discount_percentage = $item_discount[$key];
                    } else {
                        $discount_type = 0;
                        $discount_percentage = 0;
                    }
					
					if ($this->input->post('currency') == 2) {
						$price[$key] *= $this->input->post('exchange_rate');
						$price_after_gst[$key] *= $this->input->post('exchange_rate');
						$unit_cost[$key] *= $this->input->post('exchange_rate');
						$item_discount_amt[$key] *= $this->input->post('exchange_rate');
						$amount[$key] *= $this->input->post('exchange_rate');
					}
					
                    $item = array(
                        'pr_id' => $last_id,
                        'category_id' => $category_id[$key],
                        'product_id' => $product_id[$key],
                        'product_description' => $product_description[$key],
                        'sl_no' => $sl_no[$key],
                        'sl_itm_code' => $sl_itm_code[$key],
                        'pr_price' => $price[$key],
                        'pr_price_after_gst' => $price_after_gst[$key],
                        'pr_unit_cost' => $unit_cost[$key],
                        //'po_quantity' => $quantity[$key],
                        'pr_quantity' => $quantity[$key],
                        'discount' => $item_discount_amt[$key],
                        'discount_percentage' => $discount_percentage,
                        'discount_type' => $discount_type,
                        'total' => $amount[$key],
                        'status' => 1,
                        'branch_id' => $user_data['branch_id'],
                        'created_by' => $user_data['user_id'],
                        'created_on' => date('Y-m-d H:i:s'),
                    );
                    if ($this->purchase_receive_model->insert_item($item)) {
                        $this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $quantity[$key], '+');
                        if(!empty($sl_no[$key])) {
                            $this->product_model->update_serial_no($product_id[$key], $sl_no[$key]);
                        }
                    }
                    if (!empty($product_id[$key])) {//Insert Item Cost Log
                        $this->update_price_log($product_id[$key], $price_after_gst[$key], $price[$key], 'From Goods Receive');
                        $pdt = array(
                            'selling_price' => $price[$key],
                            'bfr_gst_buying_price' => $unit_cost[$key],
                            'buying_price' => $price_after_gst[$key],
                            'updated_by' => $user_data['user_id'],
                            'updated_on' => date('Y-m-d H:i:s')
                        );
                        $this->purchase_order_model->update_product($product_id[$key], $pdt);
                    }
                }
            }
            
            $_SESSION['success'] = 'Goods Receive added successfully';
        } else {
            $_SESSION['error'] = 'Purchase Receive not added!';
        }
		
        redirect('goods_receive/add', 'refresh');
    }

    public function edit($id = '') {

        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        //$data['purchases'] = $this->purchase_receive_model->get_purchase_list_edit();
        $data['grn'] = $this->purchase_receive_model->get_by_id($id);
        $data['grn_item'] = $this->purchase_receive_model->get_item_by_id($id);
        // $data['grn_charges'] = $this->good_received_note_model->get_charges_by_id($data['grn']->id);
        // $data['grn_item_vat'] = $this->purchase_receive_model->get_item_vat_by_id($id);
        //$data['grn_item_vat_amt'] = $this->purchase_receive_model->get_item_vat_amt_by_id($id);
        //$data['grn_tax'] = $this->good_received_note_model->get_tax_by_id($data['grn']->id);
        //$data['material'] = $this->purchase_receive_model->get_material_by_po($data['grn']->po_id);
        //echo $this->db->last_query();
        $data['company'] = $this->company_model->get_company();
        $data['add_item_link'] = base_url() . 'item/add/';
        $data['add_supplier_link'] = base_url() . 'supplier/add/';
        //$data['add_po_link'] = base_url() . 'purchase_orders/add/';
        //$user_data = get_user_data();
        #echo '<pre>';print_r($data['grn']);print_r($data['grn_item']);exit;
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' . $data['grn']->id;
        $this->render($data, 'purchase_receive/edit');
    }

    public function view($pr_id = '') {
        if (empty($pr_id)) {
            show_400_error();
        }
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        //$data['supplier'] = $this->purchase_receive_model->get_supplier_by_po($pr_id);
        $data['grn_details'] = $this->purchase_receive_model->get_by_id($pr_id);
        $data['grn_item'] = $this->purchase_receive_model->get_item_by_id($pr_id);
        #echo '<pre>';		print_r($data);exit;
        $this->render($data, 'purchase_receive/view');
    }

//    public function delete($grn_id) {
//        if (empty($grn_id)) {
//            show_400_error();
//        }
//        $user_data = get_user_data();
//        $grn = $this->purchase_receive_model->get_by_id($grn_id);
//        $grn_item = $this->purchase_receive_model->get_item_by_id($grn_id);
//        $return_val = 1;
//        foreach ($grn_item as $key => $item) {
//            $delete_quantity = $this->purchase_receive_model->get_old_mat_quantity($item->product_id, $item->id);
//            $aval_qty = $this->purchase_receive_model->branch_materials($grn->brn_id, $item->product_id)->row();
//            if ($aval_qty->available_qty < $delete_quantity) {
//                $return_val = 0;
//                break;
//            } else {
//                $this->purchase_receive_model->update_branch_material_qty($grn->brn_id, $item->product_id, $delete_quantity, '-');
//            }
//        }
//        if ($return_val == 0) {
//            $_SESSION['error'] = 'Purchase receive not deleted!';
//        } else {
//            $grns = array(
//                'pr_status' => 10,
//                'updated_on' => date('Y-m-d H:i:s'),
//                'updated_by' => $user_data['user_id']
//            );
//            if ($this->purchase_receive_model->update($grn_id, $grns) > 0) {
//                $grns_items = array(
//                    'status' => 10,
//                    'updated_on' => date('Y-m-d H:i:s'),
//                    'updated_by' => $user_data['user_id']
//                );
//                $this->purchase_receive_model->update_item_by_pr($grns_items, $grn_id);
//                $_SESSION['success'] = 'Purchase receive deleted successfully';
//                // Update GRN Status in Purchase Order
//                $po_total_quantity = $this->purchase_receive_model->get_po_total_quantity($grn->po_id);
//                $grn_total_qty = $this->purchase_receive_model->get_grn_total_quantity($grn->po_id);
//                if ($po_total_quantity->po_quantity > $grn_total_qty->pri_quantity) {
//                    $grn_status = 2;
//                } elseif ($po_total_quantity->po_quantity == $grn_total_qty->pri_quantity) {
//                    $grn_status = 3;
//                } else {
//                    $grn_status = 1;
//                }
//                $update = array(
//                    'pr_status' => $grn_status
//                );
//                $this->purchase_order_model->update($grn->po_id, $update);
//            }
//        }
//        //exit;
//        redirect('goods_receive', 'refresh');
//    }

    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }
        $user_data = get_user_data();
        $grn = $this->purchase_receive_model->get_by_id($id);
        $delete_items = $this->purchase_receive_model->get_item_by_id($id);
        foreach ($delete_items as $key => $remove_item) {
            $item = array(
                'status' => 10,
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $user_data['user_id']
            );
            if ($this->purchase_receive_model->update_prit($item, $remove_item->id)) {
                $this->product_model->update_branch_stock($user_data['branch_id'], $remove_item->product_id, $remove_item->pr_quantity, '-');
            }
        }
        $grns = array(
            'pr_status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $user_data['user_id']
        );
        if ($this->purchase_receive_model->update($id, $grns) > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $grn->pr_code, 1); 
            $_SESSION['success'] = 'Goods receive deleted successfully';
            // Update GRN Status in Purchase Order
            $po_total_quantity = $this->purchase_receive_model->get_po_total_quantity($grn->po_id);
            $grn_total_qty = $this->purchase_receive_model->get_grn_total_quantity($grn->po_id);
            if ($po_total_quantity->quantity > $grn_total_qty->pr_quantity) {
                $grn_status = 2;
            } elseif ($po_total_quantity->quantity == $grn_total_qty->pr_quantity) {
                $grn_status = 3;
            } else {
                $grn_status = 1;
            }
            $update = array(
                'pr_status' => $grn_status
            );
            $this->purchase_order_model->update($grn->po_id, $update);
        } else {
            $_SESSION['error'] = 'Goods receive not deleted!';
        }
        redirect('goods_receive', 'refresh');
    }
	
    public function update($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        //$prefix_wt_sub = CODE_PR;
        //$grn_no = $this->auto_generation_code(TBL_PR, $prefix_wt_sub, '', 3, $id);
        $user_data = get_user_data();
        $grn = $this->purchase_receive_model->get_by_id($id);
        
        $gst_amount = $this->input->post('gst_amount');
//        if ($this->input->post('gst_type') == 1) {
//            $gst_amount = $this->calculate_inclusive_gst($this->input->post('sub_total') - $this->input->post('discount_amt'));
//        } else {
//            $gst_amount = ($this->input->post('sub_total') - $this->input->post('discount_amt')) * ($this->input->post('hidden_gst') / 100);
//        }

        $grns = array(
            //'pr_code' => $grn_no,
            //'po_id' => $this->input->post('hidden_po_id'),
            //'brn_id' => $user_data['branch_id'],
            'pr_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('pr_date')))),
            'invoice_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('inv_date')))),
            'pr_sub_total' => $this->input->post('sub_total'),
            'pr_total_discount' => $this->input->post('discount_amt'),
            'pr_discount' => $this->input->post('discount'),
            'pr_discount_type' => $this->input->post('discount_type'),
            'pr_total_discount' => $this->input->post('discount_amt'),
            'pr_gst_type' => $this->input->post('gst_type'),
            'pr_gst_val' => $this->input->post('hidden_gst'),
            'pr_gst_amount' => $gst_amount,
            'pr_net_amount' => $this->input->post('total_amt'),
            //'pr_remarks' => '',
            'credit_notes' => (!empty($this->input->post('credit_notes'))) ? 1 : 0,
            'credit_amount' => 0,
            'supplier_invoice_no' => $this->input->post('sup_invoice'),
            'pr_your_info' => $this->input->post('your_info'),
            'pr_status' => 1,
            /* 'received_status' => ((!empty($this->input->post('last_grn'))) ? $this->input->post('last_grn') : ''), */
            //'pr_last_grn' => ((!empty($this->input->post('last_grn'))) ? $this->input->post('last_grn') : ''),
            //'reason' => ((!empty($this->input->post('last_grn'))) ? '1' : '0'),
            'branch_id' => $user_data['branch_id'],
            'updated_by' => $user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s'),
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );

        $affected_rows = $this->purchase_receive_model->update($id, $grns);
        //$return_val = 0;
        if ($affected_rows > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $grn->pr_code, ''); 
            //$return_val = 1;
            /**             * Update Old Item ** */
            $grni_id = $this->input->post('grni_id');
            $category_id = $this->input->post('category_id');
            $product_id = $this->input->post('product_id');
            $po_quantity = $this->input->post('po_quantity');
            $quantity = $this->input->post('quantity');
            $price = $this->input->post('price');
            $price_after_gst = $this->input->post('price_after_gst');
            $unit_cost = $this->input->post('cost');
            $item_discount = $this->input->post('item_discount');
            $item_discount_amt = $this->input->post('item_discount_amt');
            $item_discount_type = $this->input->post('item_discount_type');
            $amount = $this->input->post('amount');
            $product_code = $this->input->post('product_code');
            $product_description = $this->input->post('product_description');
            $sl_no = $this->input->post('sl_no');
            $sl_itm_code = $this->input->post('sl_itm_code');

            if (!empty($grni_id)) {
                foreach ($grni_id as $key => $pr_item) {
                    if (!empty($item_discount_type[$key])) {
                        $discount_type = 1;
                        $discount_percentage = $item_discount[$key];
                    } else {
                        $discount_type = 0;
                        $discount_percentage = 0;
                    }
                    // Old sales order item 
                    $old_pr_item = $this->purchase_receive_model->get_pr_item($pr_item);
                    if ($old_pr_item->product_id == $product_id[$key]) {
                        $item = array(
                            'category_id' => $category_id[$key],
                            'product_id' => $product_id[$key],
                            'product_description' => $product_description[$key],
                            'sl_no' => $sl_no[$key],
                            'sl_itm_code' => $sl_itm_code[$key],
                            'pr_price' => $price[$key],
                            'pr_price_after_gst' => $price_after_gst[$key],
                            'pr_unit_cost' => $unit_cost[$key],
                            'discount' => $item_discount_amt[$key],
                            'discount_percentage' => $discount_percentage,
                            'discount_type' => $discount_type,
                            //'po_quantity' => $po_quantity[$key],
                            'pr_quantity' => $quantity[$key],
                            'total' => $amount[$key],
                            'status' => 1,
                            'branch_id' => $user_data['branch_id'],
                            'updated_by' => $user_data['user_id'],
                            'updated_on' => date('Y-m-d H:i:s')
                        );

                        if ($this->purchase_receive_model->update_item($item, $pr_item)) {
                            $pdt = array(
                                'selling_price' => $price[$key],
                                'bfr_gst_buying_price' => $unit_cost[$key],
                                'buying_price' => $price_after_gst[$key],
                                'updated_by' => $user_data['user_id'],
                                'updated_on' => date('Y-m-d H:i:s')
                            );
                            $this->purchase_order_model->update_product($product_id[$key], $pdt);
                            if(!empty($old_pr_item->sl_no)) {
                                $this->product_model->remove_serial_no($old_pr_item->product_id, $old_pr_item->sl_no);
                            }
                            if(!empty($sl_no[$key])) {
                                $this->product_model->update_serial_no($product_id[$key], $sl_no[$key]);
                            }
                            $update_quantity = $quantity[$key] - $old_pr_item->pr_quantity;
                            $this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $update_quantity, '+');
                        }
                    } else {
                        $item = array(
                            'pr_id' => $id,
                            'category_id' => $category_id[$key],
                            'product_id' => $product_id[$key],
                            'product_description' => $product_description[$key],
                            'sl_no' => $sl_no[$key],
                            'sl_itm_code' => $sl_itm_code[$key],
                            'pr_price' => $price[$key],
                            'pr_price_after_gst' => $price_after_gst[$key],
                            'pr_unit_cost' => $unit_cost[$key],
                            'discount' => $item_discount_amt[$key],
                            'discount_percentage' => $discount_percentage,
                            'discount_type' => $discount_type,
                            //'po_quantity' => $po_quantity[$key],
                            'pr_quantity' => $quantity[$key],
                            'total' => $amount[$key],
                            'status' => 1,
                            'branch_id' => $user_data['branch_id'],
                            'updated_by' => $user_data['user_id'],
                            'updated_on' => date('Y-m-d H:i:s')
                        );

                        if ($this->purchase_receive_model->insert_item($item)) {
                            $pdt = array(
                                'selling_price' => $price[$key],
                                'bfr_gst_buying_price' => $unit_cost[$key],
                                'buying_price' => $price_after_gst[$key],
                                'updated_by' => $user_data['user_id'],
                                'updated_on' => date('Y-m-d H:i:s')
                            );
                            $this->purchase_order_model->update_product($product_id[$key], $pdt);

                            if(!empty($old_pr_item->sl_no)) {
                                $this->product_model->remove_serial_no($old_pr_item->product_id, $old_pr_item->sl_no);
                            }
                            if(!empty($sl_no[$key])) {
                                $this->product_model->update_serial_no($product_id[$key], $sl_no[$key]);
                            }
                            
                            /* To update old item stock also delete from sales order items start */
                            $this->product_model->update_branch_stock($user_data['branch_id'], $old_pr_item->product_id, $old_pr_item->pr_quantity, '-');
                            $this->purchase_receive_model->delete_pr_item($pr_item);
                            /* To update old item stock also delete from sales order items end */
                            // To update new item quantity in stock
                            $this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $quantity[$key], '+');
                        }
                        //Insert Item Cost Log
                        $this->update_price_log($product_id[$key], $price_after_gst[$key], $price[$key], 'From Goods receive');
                    }
                }
            }
            // Update existing product end
            // Delete existing row start
            $delete_items = $this->purchase_receive_model->get_removed_pr_items($id, $product_id);
            foreach ($delete_items as $key => $remove_item) {
                if ($this->purchase_receive_model->delete_pr_item($remove_item->id)) {
                    $this->product_model->update_branch_stock($user_data['branch_id'], $remove_item->product_id, $remove_item->pr_quantity, '-');
                    if(!empty($remove_item->sl_no)) {
                        $this->product_model->remove_serial_no($remove_item->product_id, $remove_item->sl_no);
                    }
                }
            }
            // Delete existing row end
            // Add newly added product start
            /** Insert New Item ** */
            $new_category_id = $this->input->post('new_category_id');
            $new_product_id = $this->input->post('new_product_id');
            $new_act_quantity = $this->input->post('new_po_quantity');
            $new_quantity = $this->input->post('new_quantity');
            $new_price = $this->input->post('new_price');
            $new_price_after_gst = $this->input->post('new_price_after_gst');
            $new_unit_cost = $this->input->post('new_cost');
            $new_item_discount = $this->input->post('new_item_discount');
            $new_item_discount_amt = $this->input->post('new_item_discount_amt');
            $new_item_discount_type = $this->input->post('new_item_discount_type');
            $new_amount = $this->input->post('new_amount');
            $new_product_code = $this->input->post('new_product_code');
            $new_product_description = $this->input->post('new_product_description');
            $new_sl_no = $this->input->post('new_sl_no');
            $new_sl_itm_code = $this->input->post('new_sl_itm_code');
            if (!empty($new_product_id)) {
                foreach ($new_product_id as $key => $product_id) {
                    if (!empty($new_item_discount_type[$key])) {
                        $discount_type = 1;
                        $discount_percentage = $new_item_discount[$key];
                    } else {
                        $discount_type = 0;
                        $discount_percentage = 0;
                    }

                    $item = array(
                        'pr_id' => $id,
                        'category_id' => $new_category_id[$key],
                        'product_id' => $new_product_id[$key],
                        'product_description' => $new_product_description[$key],
                        'sl_no' => $new_sl_no[$key],
                        'sl_itm_code' => $new_sl_itm_code[$key],
                        'pr_price' => $new_price[$key],
                        'pr_price_after_gst' => $new_price_after_gst[$key],
                        'pr_unit_cost' => $new_unit_cost[$key],
                        'discount' => $new_item_discount_amt[$key],
                        'discount_percentage' => $discount_percentage,
                        'discount_type' => $discount_type,
                        //'po_quantity' => $new_act_quantity[$key],
                        'pr_quantity' => $new_quantity[$key],
                        'total' => $new_amount[$key],
                        'status' => 1,
                        'branch_id' => $user_data['branch_id'],
                        'created_by' => $user_data['user_id'],
                        'created_on' => date('Y-m-d H:i:s'),
                    );
                    if ($this->purchase_receive_model->insert_item($item)) {
                        if(!empty($new_sl_no[$key])) {
                            $this->product_model->update_serial_no($new_product_id[$key], $new_sl_no[$key]);
                        }
                        $this->product_model->update_branch_stock($user_data['branch_id'], $new_product_id[$key], $new_quantity[$key], '+');
                    }
                    if (!empty($product_id[$key])) { //Insert Item Cost Log
                        $this->update_price_log($new_product_id[$key], $new_price_after_gst[$key], $new_price[$key], 'From Goods Receive');
                        $pdt = array(
                            'selling_price' => $new_price[$key],
                            'bfr_gst_buying_price' => $new_unit_cost[$key],
                            'buying_price' => $new_price_after_gst[$key],
                            'updated_by' => $user_data['user_id'],
                            'updated_on' => date('Y-m-d H:i:s')
                        );
                        $this->purchase_order_model->update_product($new_product_id[$key], $pdt);
                    }
                }
            }

            // Update GRN Status in Purchase Order
            /* $po_total_quantity = $this->purchase_receive_model->get_po_total_quantity($this->input->post('hidden_po_id'));
              $grn_total_qty = $this->purchase_receive_model->get_grn_total_quantity($this->input->post('hidden_po_id'));
              if ($grn_total_qty->pr_quantity >= $po_total_quantity->quantity) {
              $pr_status = 3;
              } elseif (!empty($grn_total_qty->pr_quantity)) {
              $pr_status = 2;
              } else {
              $pr_status = 1;
              }
              if ($this->input->post('last_grn') == '1') {
              $pr_status = 3;
              }
              $update = array(
              'pr_status' => $pr_status
              );
              $this->purchase_order_model->update($this->input->post('hidden_po_id'), $update);
              //echo $this->db->last_query();

              if ($grn_total_qty->pr_quantity >= $po_total_quantity->quantity) {
              $received_status = 1;
              }
              else {
              $received_status = 0;
              }

              $grns = array(
              'received_status' => $received_status
              );
              $affected_rows = $this->purchase_receive_model->update($id, $grns); */

            //if ($return_val == 1) {
            $_SESSION['success'] = 'Goods Receive Updated Successfully';
            //}
        } else
            $_SESSION['error'] = 'Purchase Receive not updated!';

        redirect('goods_receive/edit/' . $id, 'refresh');
    }

    public function update_price_log($pid, $bprice, $sprice, $tit) {
        $last = $this->product_model->get_product_last_price_log($pid);
        $last_id = '';
        if (($last->buying_price != $bprice) || ($last->selling_price != $sprice)) {
            $insert = array(
                'product_id' => $pid,
                'buying_price' => $bprice,
                'selling_price' => $sprice,
                'updated_from' => $tit,
                'brn_id' => $this->user_data['branch_id'],
                'created_by' => $this->user_data['user_id'],
                'created_on' => date('Y-m-d H:i:s'),
                'ip_address' => $_SERVER['REMOTE_ADDR']
            );
            $last_id = $this->product_model->insert_cost_log($insert);
        }
        return $last_id;
    }

    public function pdf($id) {
        $data = array();
        $user_data = get_user_data();
        $data['grn'] = $this->purchase_receive_model->get_by_id($id);
        //$data['por'] = $this->purchase_order_model->get_by_id($data['grn']->po_id);
        $data['grn_item'] = $this->purchase_receive_model->get_item_by_id($id);
        //$data['address'] = $this->purchase_order_model->get_current_address($user_data['branch_id']);
        $data['amt_word'] = convert_amount_to_word_rupees($data['grn']->pr_net_amount);
        $data['supplier'] = $this->purchase_order_model->supplier_address_by_id($data['grn']->sup_id);
        $data['user_data'] = get_user_data();
        $data['branch'] = $this->company_model->branch_by_id($data['grn']->branch_id);
        $data['branches'] = $this->company_model->branch_list_active();
        $data['company'] = $this->company_model->get_company();
        $filename = 'PDF_' . $data['grn']->pr_code;
        $filename = strtoupper($filename);
        // Load Views
        $header = $footer = $this->load->view('purchase_receive/pdf_header', $data, TRUE);;
        $content = $this->load->view('purchase_receive/pdf', $data, TRUE);
        $footer = $this->load->view('purchase_receive/pdf_footer', $data, TRUE);

        $this->load->helper(array('My_Pdf'));
        invoice_pdf($header, $content, $footer, $filename);
        exit(0);
    }
	
    public function search_item_description1() {
        $po = $this->input->post('po');
        $grn = $this->input->post('grn');
        $data = array();
        if (!empty($grn)) {
            $result1 = $this->purchase_receive_model->get_current_grn_items_desc($po, $grn);
            $data = $result1;
        } else {
            $data = $this->purchase_receive_model->search_item_by_brand($po);
        }
        //error_log($this->db->last_query());
        foreach ($data as $key => $value) {
            #$value->label = (!empty($value->cat_name)) ? $value->cat_name . ' - ' : '';
            $value->label = (!empty($value->cat_name)) ? $value->cat_name . ' ' : '';
            $value->label .= $value->pdt_name;
            $value->value = $value->label;
        }
        $this->ajax_response($data);
    }

    public function search_item1() {
        $po = $this->input->post('po');
        $product_code = $this->input->post('product_code');
        $grn = $this->input->post('grn');
        $data = array();
        if (!empty($grn)) {
            $data = $this->purchase_receive_model->get_current_grn_items($po, $grn);
        } else {
            $data = $this->purchase_receive_model->search_item($po);
        }
        foreach ($data as $key => $value) {
            //$value->value = $value->id;
            if (!empty($product_code))
                $value->label = $value->pdt_code;
            else
                $value->label = $value->pdt_code . ' / ' . $value->pdt_name;
            $value->value = $value->label;
        }
        $this->ajax_response($data);
    }

    public function edit_purchase_receive_items() {
        $po = $this->input->post('po');
        $product_code = $this->input->post('product_code');
        $grn = $this->input->post('grn');
        $data = array();
        $data = $this->purchase_receive_model->edit_purchase_receive_items($po, $grn);
        //error_log($this->db->last_query());
        foreach ($data as $key => $value) {
            //$value->value = $value->id;
            if (!empty($product_code))
                $value->label = $value->pdt_code;
            else
                $value->label = $value->pdt_code . ' / ' . $value->pdt_name;
            $value->value = $value->label;
        }
        $this->ajax_response($data);
    }

    public function edit_purchase_receive_desc() {
        $po = $this->input->post('po');
        $grn = $this->input->post('grn');
        $data = array();
        $data = $this->purchase_receive_model->edit_purchase_receive_desc($po, $grn);
        //error_log($this->db->last_query());
        foreach ($data as $key => $value) {
            #$value->label = (!empty($value->cat_name)) ? $value->cat_name . ' - ' : '';
            $value->label = (!empty($value->cat_name)) ? $value->cat_name . ' ' : '';
            $value->label .= $value->pdt_name;
            $value->value = $value->label;
        }
        $this->ajax_response($data);
    }
	
    public function unreceived_purchase_orders() {
		$data = array();
		$data['purchase_orders'] = $this->purchase_receive_model->unreceived_purchase_orders();
		$this->ajax_response($data);
    }
	
    public function get_supplier_data() {
		$data = array();
		$data['supplier'] = $this->supplier_model->get_by_id($this->input->post('supp_id'));
		$this->ajax_response($data);
    }
}
?>