<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Petty_Cash extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('bank_model', '', TRUE);
        $this->load->model('petty_cash_model', '', TRUE);
        $this->load->library('Datatables');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['add_btn'] = 'Add Petty Cash';
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        //$data['petty_cash'] = $this->petty_cash_model->list_all();
        $this->render($data, 'petty_cash/list');
    }

    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('pc.id AS id, DATE_FORMAT(pc.date, "%d/%m/%Y") AS date, pc.opening_balance AS opening_balance, pc.closing_balance AS closing_balance', FALSE)
                ->where('pc.status != 10')
                ->where('branch_id', $this->user_data['branch_id'])
                ->add_column('Actions', $this->get_buttons('$1'), 'id')
                ->from(TBL_PETTY_CASH . ' AS pc');
        echo $this->datatables->generate();
    }

    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(1, $this->permission)) {
            $html .= '<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '">';
			$html .= '<span>View</span>';
			$html .= '</a> ';
        }
        if (in_array(3, $this->permission)) {
            $html .= '<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '">';
			$html .= '<span>Edit</span>';
			$html .= '</a> ';
        }
        if (in_array(4, $this->permission)) {
            $html .= '<a class="label btn btn-danger pop_up_confirm delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Petty Cash details!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Petty Cash">';
			$html .= '<span>Delete</span>';
			$html .= '</a>';
        }
        $html .= '</span>';
        return $html;
    }

    public function add() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['form_action'] = $actions['insert'];
        $data['list_link'] = $actions['index'];
		$data['petty_cash_amount'] = $this->get_amounts();
        $this->render($data, 'petty_cash/add');
    }
	
	public function get_amounts($date = '') {
		if (empty($date)) {
			$date = date('d/m/Y');
		}
		
		$data = array();  
		$data['opening_balance'] = $this->petty_cash_model->opening_balance($date);
		$data['unpaid_invoice_amount'] = $this->petty_cash_model->unpaid_invoice_amount($date);
		$data['prev_unpaid_invoice_amount'] = $this->petty_cash_model->previous_unpaid_invoice_amount($date);
		$data['prev_sales_amount'] = $this->petty_cash_model->prev_sales_amount($date);
		$data['total_sales_amount'] = $this->petty_cash_model->total_sales_amount($date);
		$data['sales_payment'] = $this->petty_cash_model->sales_payment($date);
		$data['prev_day_sales_payment'] = $this->petty_cash_model->previous_day_sales_payment($date);
		$data['expense_amount'] = $this->petty_cash_model->expense_amount($date);
		$payment_amount = $this->petty_cash_model->payment_amount($date);
		$deduction_amount = $this->petty_cash_model->deduction_amount($date);
		$data['bill_amount'] = $deduction_amount + $payment_amount;
		$purchase_amount = $this->petty_cash_model->purchase_payments($date);
		$eo_payments = $this->petty_cash_model->eo_payments($date);
		$external_service_payment = $this->petty_cash_model->external_service_payment($date);
		$data['purchase_amount'] = $purchase_amount + $eo_payments + $external_service_payment;
		$data['banking_amount'] = $this->petty_cash_model->deposite_amount($date);
		$data['refund_amount'] = $this->petty_cash_model->refund_amount($date);
		
		return $data;
	}
	
	public function get_petty_cash_amount() {
		$date = $this->input->post('petty_cash_date');
		$data = $this->get_amounts($date);
		$this->ajax_response($data);
	}
	
    public function insert() {
        $insert = array(
            'date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('petty_cash_date')))),
            'bank_acc_id' => $this->user_data['bank_account_id'],
            'opening_balance' => $this->input->post('opening_balance'),
            'total_sales' => $this->input->post('total_sales'),
            'unpaid_sale_amt' => $this->input->post('unpaid_amount'),
            'expense_amt' => $this->input->post('expense_amount'),
            'bill_payment' => $this->input->post('bill_payment'),
            'prev_day_collection' => $this->input->post('prev_day_inv_amount'),
            'refund_amt' => $this->input->post('refund_amount'),
            'purchase_amt' => $this->input->post('purchase_amount'),
            'ledger_amt' => $this->input->post('ledger_amount'),
            'banking' => $this->input->post('banking_amount'),
            'closing_balance' => $this->input->post('closing_balance'),
            'remarks' => $this->input->post('remarks'),
            'branch_id' => $this->user_data['branch_id'],
            'created_by' => $this->user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s')
        );
		
        $last_id = $this->petty_cash_model->insert($insert);
        if ($last_id > 0) {
                        $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, '', '');
			$update_data = array(
				'updated_by' => $this->user_data['user_id'],
				'updated_on' => date('Y-m-d H:i:s')
			);
			$this->bank_model->update_bank_amount($this->user_data['bank_account_id'], $update_data, $this->input->post('banking_amount'), '+');
			
			if (!empty($this->input->post('type_name'))) {
				$type_name = $this->input->post('type_name');
				$group_name = $this->input->post('group_name');
				$sale_amount = $this->input->post('sale_amount');
				foreach($type_name as $key => $value) {
					$insert = array(
						'petty_cash_id' => $last_id,
						'type' => 1,
						'payment_type' => $value,
						'group_name' => $group_name[$key],
						'amount' => $sale_amount[$key],
						'branch_id' => $this->user_data['branch_id'],
						'created_by' => $this->user_data['user_id'],
						'created_on' => date('Y-m-d H:i:s')
					);
					
					$this->petty_cash_model->insert_item($insert);
				}
			}
			
			if (!empty($this->input->post('prev_type_name'))) {
				$type_name = $this->input->post('prev_type_name');
				$group_name = $this->input->post('prev_group_name');
				$sale_amount = $this->input->post('prev_sale_amount');
				foreach($type_name as $key => $value) {
					$insert = array(
						'petty_cash_id' => $last_id,
						'type' => 2,
						'payment_type' => $value,
						'group_name' => $group_name[$key],
						'amount' => $sale_amount[$key],
						'branch_id' => $this->user_data['branch_id'],
						'created_by' => $this->user_data['user_id'],
						'created_on' => date('Y-m-d H:i:s')
					);
					
					$this->petty_cash_model->insert_item($insert);
				}
			}
			
			$_SESSION['success'] = 'Petty Cash added successfully';
		}
        else {
			$_SESSION['error'] = 'Petty Cash not added!';
		}
		
        redirect('petty_cash/add', 'refresh');
    }

    public function edit($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['petty_cash'] = $this->petty_cash_model->get_by_id($id);
        $data['petty_cash_items'] = $this->petty_cash_model->get_items_by_id($id);
        $data['petty_cash_prev_items'] = $this->petty_cash_model->get_prev_day_items_by_id($id);
		
        $actions = $this->actions();
		$data['list_link'] = $actions['index'];
        $data['form_action'] = $actions['update'] . '/' . $data['petty_cash']->id;
        $this->render($data, 'petty_cash/edit');
    }

    public function update($id = '') {
        if (empty($id)) {
            show_400_error();
        }
		
		$update = array(
            'date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('petty_cash_date')))),
            'opening_balance' => $this->input->post('opening_balance'),
            'total_sales' => $this->input->post('total_sales'),
            'unpaid_sale_amt' => $this->input->post('unpaid_amount'),
            'expense_amt' => $this->input->post('expense_amount'),
            'bill_payment' => $this->input->post('bill_payment'),
            'prev_day_collection' => $this->input->post('prev_day_inv_amount'),
            'refund_amt' => $this->input->post('refund_amount'),
            'purchase_amt' => $this->input->post('purchase_amount'),
            'ledger_amt' => $this->input->post('ledger_amount'),
            'banking' => $this->input->post('banking_amount'),
            'closing_balance' => $this->input->post('closing_balance'),
            'remarks' => $this->input->post('remarks'),
            'branch_id' => $this->user_data['branch_id'],
            'updated_by' => $this->user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s')
        );
		
		if ($this->petty_cash_model->update($id, $update) > 0) {
                    $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', '');
			if (!empty($this->input->post('type_name'))) {
				$item_id = $this->input->post('item_id');
				$type_name = $this->input->post('type_name');
				$group_name = $this->input->post('group_name');
				$sale_amount = $this->input->post('sale_amount');
				foreach($type_name as $key => $value) {
					$update = array(
						/*'payment_type' => $value,
						'group_name' => $group_name[$key],*/
						'amount' => $sale_amount[$key],
						'branch_id' => $this->user_data['branch_id'],
						'updated_by' => $this->user_data['user_id'],
						'updated_on' => date('Y-m-d H:i:s')
					);
					
					$this->petty_cash_model->update_item($item_id[$key], $update);
				}
			}
			
			if (!empty($this->input->post('prev_type_name'))) {
				$prev_item_id = $this->input->post('prev_item_id');
				$type_name = $this->input->post('prev_type_name');
				$group_name = $this->input->post('prev_group_name');
				$sale_amount = $this->input->post('prev_sale_amount');
				foreach($type_name as $key => $value) {
					$update = array(
						/*'payment_type' => $value,
						'group_name' => $group_name[$key],*/
						'amount' => $sale_amount[$key],
						'branch_id' => $this->user_data['branch_id'],
						'created_by' => $this->user_data['user_id'],
						'created_on' => date('Y-m-d H:i:s')
					);
					
					$this->petty_cash_model->update_item($prev_item_id[$key], $update);
				}
			}
			
			$_SESSION['success'] = 'Petty Cash updated successfully';
		}
        else {
			$_SESSION['error'] = 'Petty Cash not updated!';
		}
		
        redirect('petty_cash/edit/' . $id, 'refresh');
    }

    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $data = array();
		$actions = $this->actions();
		$data['list_link'] = $actions['index'];
        $data['petty_cash'] = $this->petty_cash_model->get_by_id($id);
		$data['petty_cash_items'] = $this->petty_cash_model->get_items_by_id($id);
        $data['petty_cash_prev_items'] = $this->petty_cash_model->get_prev_day_items_by_id($id);
        $this->render($data, 'petty_cash/view');
    }

    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }
		
        $update = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $this->user_data['user_id']
        );
		
		$petty_cash = $this->petty_cash_model->get_by_id($id);
        if ($this->petty_cash_model->update($id, $update) > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', 1);
            $_SESSION['success'] = 'Petty Cash deleted successfully';
			
			$update_data = array(
				'updated_by' => $this->user_data['user_id'],
				'updated_on' => date('Y-m-d H:i:s')
			);
			$this->bank_model->update_bank_amount($petty_cash->bank_acc_id, $update_data, $petty_cash->banking, '-');
		}
        else
            $_SESSION['error'] = 'Petty Cash not deleted!';
		
        redirect('petty_cash', 'refresh');
    }

    public function date_already_exists() {
		$data = array();
		$petty_cash_id = '';
		$petty_cash_date = $this->input->post('petty_cash_date');
		$data['exists'] = $this->petty_cash_model->date_already_exists($petty_cash_date, $petty_cash_id);
		$this->ajax_response($data);
    }
}
?>