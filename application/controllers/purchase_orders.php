<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Purchase_Orders extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('purchase_order_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->model('sales_order_model', '', TRUE);
        $this->load->library('Datatables');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        $data['send_mail_link'] = base_url() . 'purchase_orders/send_mail/';
        //print_r(get_user_data());
        //$data['purchases'] = $this->purchase_order_model->list_all();
        $this->render($data, 'purchase_order/list');
    }    
    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('po.id as poId, DATE_FORMAT(po.po_date, "%d/%m/%Y") as poDate, po.po_no, sup.name as supplier, po.po_net_amount', FALSE)
                ->where('po.po_status != 10 AND po.brn_id IN ("'.$this->user_data['branch_id'].'")')
                ->join(TBL_SUP . ' as sup', 'sup.id = po.sup_id','left')
                ->join(TBL_BRN . ' as brn', 'brn.id = po.brn_id','left')
                ->add_column('Actions', $this->get_buttons('$1'), 'poId')
                ->from(TBL_PO.' AS po');
        echo $this->datatables->generate();
    }
    
    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(6, $this->permission)) {
			$html .='<a class="label btn btn-warning view" href="' . base_url() . 'purchase_orders/pdf/' . $id .'" title="Print Purchase Order Details"><span><i class="fa fa-file-pdf-o" aria-hidden="true"></i></span></a>&nbsp';
        }

        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(3, $this->permission)) {
            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
        }
        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Purchase Order!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Goods Receives"><span>Delete</span></a>&nbsp';        
        }
		
		if (in_array(8, $this->permission)) {
			$html .='<a class="label btn btn-success delete-confirm" href="#" title="Convert to Goods Receive" data-confirm-content="Do you want to convert the purchase order to goods receive!" data-redirect-url="' . base_url() . 'goods_receive/add/' . $id . '" data-bindtext="quotation" data-action="GOODS RECEIVE CONVERSION" style="display: inline-block; margin-top: 5px;"><span style="line-height: 14px;">To Goods Rec</span></a>&nbsp';
        }
        if (in_array(9, $this->permission)) {
            $html .='<a class="label btn btn-primary view" style="background: #009688; border-color: #009688;" data-href="' . base_url() . 'purchase_orders/send_mail/' . $id . '" id="smart-mod-eg2" title="Send Email" data-id="' . $id . '"><span><i class="fa fa-send" aria-hidden="true"></i> Send Email</span></a>&nbsp';
        }
		
        $html.='</span>';
        return $html;
    }

    public function add() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['form_action'] = $actions['insert'];
        $data['list_link'] = $actions['index'];
        $data['suppliers'] = $this->purchase_order_model->get_all_suppliers();
        $data['company'] = $this->company_model->get_company();
        $data['items'] = $this->purchase_order_model->get_all_items();
        $data['add_item_link'] = base_url() . 'item/add/';
        $data['add_supplier_link'] = base_url() . 'supplier/add/';
        $user_data = get_user_data();
        $this->render($data, 'purchase_order/add');
    }

    public function get_supplier_details() {
        $sup_id = $_REQUEST['sup_id'];
        $data = array();
        if (!empty($sup_id)) {
            $data['sup'] = $this->purchase_order_model->get_supplier_details($sup_id);
        }
        echo json_encode($data);
    }

    public function get_item_details() {
        $mat_id = $_REQUEST['mat_id'];
        $data = array();
        if ((!empty($mat_id))) {
            $data['items'] = $this->purchase_order_model->get_item_details($mat_id);
        }
        echo json_encode($data);
    }

    public function insert() {
        #echo '<pre>';print_r($_REQUEST);exit;
        $prefix_wt_sub = CODE_PO;
        $po_no = $this->auto_generation_code(TBL_PO, $prefix_wt_sub, '', 3, '');
        $user_data = get_user_data();
        $dis = $this->input->post('sub_total') - $this->input->post('discount_amt');
        $purchase = array(
            'po_no' => $po_no,
            'sup_id' => $this->input->post('sup_name'),
            'contact_name' => $this->input->post('contact_person'),
			'contact_no' => $this->input->post('contact_no'),
			'supplier_address' => $this->input->post('supplier_address'),
            'brn_id' => $user_data['branch_id'],
            'po_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('po_date')))),
            'po_sub_total' => $this->input->post('sub_total'),
            'po_discount' => $this->input->post('discount'),
            'po_discount_type' => $this->input->post('discount_type'),
            'po_total_discount' => $this->input->post('discount_amt'),
            'po_total_with_discount' => $dis,
            'gst_type' => $this->input->post('gst_type'),
            'gst_val' => $this->input->post('hidden_gst'),
            'gst_total' => $this->input->post('gst_amount'),
            'po_net_amount' => $this->input->post('total_amt'),
            'po_remarks' => $this->input->post('po_remarks'),
            'po_status' => $this->input->post('status'),
            'branch_id' => $user_data['branch_id'],
            'created_by' => $user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s'),
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        $last_id = $this->purchase_order_model->insert($purchase);
        if ($last_id > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, $po_no, '');
            $_SESSION['success'] = 'Purchase Order added successfully';
            $category_id = $this->input->post('category_id');
            $product_id = $this->input->post('product_id');
            $sl_itm_code = $this->input->post('sl_itm_code');
            $quantity = $this->input->post('quantity');
            $price = $this->input->post('price');
            $unit_cost = $this->input->post('cost');
            $discount = $this->input->post('discount');
            $discount_type = $this->input->post('discount_type');
            $discount_amt = $this->input->post('discount_amt');
            $amount = $this->input->post('amount');
            $product_code = $this->input->post('product_code');
            $product_description = $this->input->post('product_description');
            foreach ($product_id as $key => $product) {
                if (!empty($product) && !empty($product)) {                    
                    $profit = ($quantity[$key] * $price[$key]) - ($quantity[$key] * $unit_cost[$key]);
                    $item = array(
                        'po_id' => $last_id,
                        'category_id' => $category[$key],
                        'product_id' => $product,
                        'product_description' => $product_description[$key],
                        'sl_itm_code' => $sl_itm_code[$key],
                        'price' => $price[$key],
                        'unit_cost' => $unit_cost[$key],
                        'quantity' => $quantity[$key],
                        'total' => $amount[$key],
                        'status' => 1,
                        'branch_id' => $user_data['branch_id'],
                        'created_by' => $user_data['user_id'],
                        'created_on' => date('Y-m-d H:i:s'),
                    );
                    
                    if ($this->purchase_order_model->insert_item($item)) {
                        //$this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $quantity[$key],'+');
                    }
                    
                    if(!empty($product)){
                         //Insert Item Cost Log
                        /*$this->update_price_log($product, $unit_cost[$key], $price[$key], 'From Purchase');
                        $pdt= array(
                            'selling_price' => $price[$key],
                            'buying_price' => $unit_cost[$key],      
                            'updated_by' => $user_data['user_id'],
                            'updated_on' => date('Y-m-d H:i:s')
                        );
                        $this->purchase_order_model->update_product($product,$pdt); */
                    }
                   
                }
            }
        } else{
            $_SESSION['error'] = 'Purchase Order not added!';
        }        
        redirect('purchase_orders/add', 'refresh');
    }

    public function edit($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['purchase'] = $this->purchase_order_model->get_by_id($id);
        $data['po_item'] = $this->purchase_order_model->get_item_by_id($id);
        //echo $this->db->last_query();
        $data['suppliers'] = $this->purchase_order_model->get_all_suppliers();
        //$data['branches'] = $this->purchase_order_model->get_all_branches();        
        $data['products'] = $this->purchase_order_model->get_all_items();
        $data['add_item_link'] = base_url() . 'item/add/';
        $data['add_supplier_link'] = base_url() . 'supplier/add/';

        $data['form_action'] = $actions['update'] . '/' . $data['purchase']->id;
        $this->render($data, 'purchase_order/edit');
    }

    public function update($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $prefix_wt_sub = CODE_PO;
        $po_no = $this->auto_generation_code(TBL_PO, $prefix_wt_sub, '', 3, $id);
        $user_data = get_user_data();
        $dis = $this->input->post('sub_total') - $this->input->post('discount_amt');
        $purchase = array(
            'po_no' => $po_no,
            'sup_id' => $this->input->post('sup_name'),
			'contact_name' => $this->input->post('contact_person'),
			'contact_no' => $this->input->post('contact_no'),
			'supplier_address' => $this->input->post('supplier_address'),
            'brn_id' => $user_data['branch_id'],
            'po_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('po_date')))),
            'po_sub_total' => $this->input->post('sub_total'),
            'po_discount' => $this->input->post('discount'),
            'po_discount_type' => $this->input->post('discount_type'),
            'po_total_discount' => $this->input->post('discount_amt'),
            'po_total_with_discount' => $dis,
            'gst_type' => $this->input->post('gst_type'),
            'gst_val' => $this->input->post('hidden_gst'),
            'gst_total' => $this->input->post('gst_amount'),
            'po_net_amount' => $this->input->post('total_amt'),
            'po_remarks' => $this->input->post('po_remarks'),
            'po_status' => $this->input->post('status'),
            'branch_id' => $user_data['branch_id'],
            'created_by' => $user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s'),
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        $affected_rows = $this->purchase_order_model->update($id, $purchase);               
        if ($affected_rows > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $po_no, ''); 
            $_SESSION['success'] = 'Purchase Order updated successfully';
            // Update existing product start
            $hidden_poitem_id = $this->input->post('hidden_soitem_id');
            $category_id = $this->input->post('category_id');
            $product_id = $this->input->post('product_id');
            $product_code = $this->input->post('product_code');
            $product_description = $this->input->post('product_description');
            $quantity = $this->input->post('quantity');
            $sl_itm_code = $this->input->post('sl_itm_code');
            $old_quantity = $this->input->post('old_quantity');
            $price = $this->input->post('price');
            $unit_cost = $this->input->post('cost');
            $amount = $this->input->post('amount');
            foreach ($hidden_poitem_id as $key => $po_item_id) {
                // Old purchase order item 
                $old_po_item = $this->purchase_order_model->get_po_item($po_item_id);
                if ($old_po_item->product_id == $product_id[$key]) {
                    //$profit = ($quantity[$key] * $price[$key]) - ($quantity[$key] * $unit_cost[$key]);
                    $item = array(                        
                        'category_id' => $category_id[$key],
                        'product_id' => $product_id[$key],
                        'product_description' => $product_description[$key],
                        'sl_itm_code' => $sl_itm_code[$key],
                        'price' => $price[$key],
                        'unit_cost' => $unit_cost[$key],
                        'quantity' => $quantity[$key],
                        'total' => $amount[$key],
                        'status' => 1,
                        'branch_id' => $user_data['branch_id'],
                        'created_by' => $user_data['user_id'],
                        'created_on' => date('Y-m-d H:i:s'),
                    );

                    if ($this->purchase_order_model->update_item($item, $po_item_id)) {                        
                        $update_quantity = $quantity[$key] - $old_po_item->quantity;
                        //$this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $update_quantity, '+');
                    }
                } else {
                    //$profit = ($quantity[$key] * $price[$key]) - ($quantity[$key] * $unit_cost[$key]);
                    $item = array(
                        'po_id' => $id,
                        'category_id' => $category_id[$key],
                        'product_id' => $product_id[$key],
                        'product_description' => $product_description[$key],
                        'sl_itm_code' => $sl_itm_code[$key],
                        'price' => $price[$key],
                        'unit_cost' => $unit_cost[$key],
                        'quantity' => $quantity[$key],
                        'total' => $amount[$key],
                        //'item_profit' => $profit,
                        'status' => 1,
                        'branch_id' => $user_data['branch_id'],
                        'updated_by' => $user_data['user_id'],
                        'updated_on' => date('Y-m-d H:i:s')
                    );

                    if ($this->purchase_order_model->insert_item($item)) {
                        /* To update old item stock also delete from purchase order items start */
                        //$this->product_model->update_branch_stock($user_data['branch_id'], $old_po_item->product_id, $old_po_item->quantity, '-');
                        $this->purchase_order_model->delete_po_item($po_item_id);
                        /* To update old item stock also delete from purchase order items end */

                        // To update new item quantity in stock
                        //$this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $quantity[$key], '+');
                    }                    
                }
                
                /*if(!empty($product_id[$key])){
                    //Insert Item Cost Log
                    $this->update_price_log($product_id[$key], $unit_cost[$key], $price[$key], 'From Purchase');
                    $pdt= array(
                        'selling_price' => $price[$key],
                        'buying_price' => $unit_cost[$key],
                        'updated_by' => $user_data['user_id'],
                        'updated_on' => date('Y-m-d H:i:s')
                    );
                    $this->purchase_order_model->update_product($product_id[$key],$pdt); 
                }*/
            }
            // Update existing product end
             
            // Delete existing row start
            $delete_items = $this->purchase_order_model->get_removed_po_items($id, $product_id);
            foreach ($delete_items as $key => $remove_item) {
                if ($this->purchase_order_model->delete_po_item($remove_item->id)){}
                    //$this->product_model->update_branch_stock($user_data['branch_id'], $remove_item->product_id, $remove_item->quantity, '-');
            }            
            // Delete existing row end
            // Add newly added product start
			
            if (!empty($this->input->post('new_product_id'))) {
                $category_ids = $this->input->post('new_category_id');
                $product_ids = $this->input->post('new_product_id');
                $product_description = $this->input->post('new_product_description');
                $sl_itm_code = $this->input->post('new_sl_itm_code');
                $product_code = $this->input->post('new_product_code');
                $quantity = $this->input->post('new_quantity');
                $price = $this->input->post('new_price');
                $unit_cost = $this->input->post('new_cost');               
                $amount = $this->input->post('new_amount');

                foreach ($product_ids as $key => $product_id) {
                    if (!empty($product_id)) {
                        //$profit = ($quantity[$key] * $price[$key]) - ($quantity[$key] * $unit_cost[$key]);
                        $item = array(
                            'po_id' => $id,
                            'category_id' => $category_ids[$key],
                            'product_id' => $product_id,
                            'product_description' => $product_description[$key],
                            'sl_itm_code' => $sl_itm_code[$key],
                            'price' => $price[$key],
                            'unit_cost' => $unit_cost[$key],
                            'quantity' => $quantity[$key],
                            'total' => $amount[$key],
                            'status' => 1,
                            'branch_id' => $user_data['branch_id'],
                            'created_by' => $user_data['user_id'],
                            'created_on' => date('Y-m-d H:i:s')
                        );
                        if ($this->purchase_order_model->insert_item($item)) {
                            //$this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $quantity[$key],'+');
                        }
                        
                    }
                    /*if(!empty($product_id[$key])){
                        //Insert Item Cost Log
                        $this->update_price_log($product_id[$key], $unit_cost[$key], $price[$key], 'From Purchase');
                        $pdt= array(
                            'selling_price' => $price[$key],
                            'buying_price' => $unit_cost[$key],
                            'updated_by' => $user_data['user_id'],
                            'updated_on' => date('Y-m-d H:i:s')
                        );
                        $this->purchase_order_model->update_product($product_id[$key],$pdt); 
                    }*/
                }
            }
            redirect('purchase_orders/edit/' . $id, 'refresh');
        } else {
            $_SESSION['error'] = 'Purchase Order not updated!';
            
        }           
    }

    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }

        $data = array();
        $data['purchase'] = $this->purchase_order_model->get_by_id($id);
        $data['po_item'] = $this->purchase_order_model->get_item_by_id($id);
        //$data['suppliers'] = $this->purchase_order_model->get_all_suppliers();
        //$data['branches'] = $this->purchase_order_model->get_all_branches();        
        $data['products'] = $this->purchase_order_model->get_all_items();
        $user_data = get_user_data();
        $this->render($data, 'purchase_order/view');
    }

    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }
        $user_data = get_user_data();
        $data_purchase = $this->purchase_order_model->get_by_id($id);
        $data = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $user_data['user_id']
        );
        $this->purchase_order_model->update_po_item($id, $data);
        $purchase = array(
            'po_status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $user_data['user_id']
        );
        if ($this->purchase_order_model->update($id, $purchase) > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $data_purchase->po_no, 1); 
            $_SESSION['success'] = 'Purchase Order deleted successfully';
        } else {
            $_SESSION['error'] = 'Purchase Order not deleted!';
        }

        redirect('purchase_orders', 'refresh');
    }

    public function get_warehouse_by_pr() {
        $pr_id = $_REQUEST['pr_id'];
        $po_id = $_REQUEST['po_id'];
        $data = array();
        if (!empty($pr_id)) {
            $data['ware'] = $this->purchase_order_model->get_warehouse_by_pr($pr_id);
            $data['suppliers'] = $this->purchase_order_model->get_supplier_by_pr($pr_id, $po_id);
        }
        $data['wares'] = $this->purchase_order_model->get_warehouse_list();
        echo json_encode($data);
    }

    public function get_material_by_sup() {
        $sup_id = $_REQUEST['sup_id'];
        $pr_id = $_REQUEST['pr_id'];
        $data = array();
        if ((!empty($sup_id)) && (!empty($pr_id) )) {
            $data['mats'] = $this->purchase_order_model->get_material_by_sup($sup_id, $pr_id);
        }
        echo json_encode($data);
    }
    
    public function get_supplier_itemcode() {
            $data = array();
            $data['itemcode'] = $this->purchase_order_model->get_supplier_itemcode($this->input->post('supp_id'),$this->input->post('item_id'));
            $this->ajax_response($data);
    }
    
    public function get_supplier_exc_itemcode() {
            $data = array();
            $data['itemcode'] = $this->purchase_order_model->get_supplier_exc_itemcode($this->input->post('supp_id'),$this->input->post('item_id'));
            $this->ajax_response($data);
    }
    
    public function pdf($id) {
        $data = array();
        $user_data = get_user_data();
        $data['po'] = $this->purchase_order_model->get_by_id($id);
        $data['po_items'] = $this->purchase_order_model->get_item_by_id($id);
        //$data['address'] = $this->purchase_order_model->get_current_address($user_data['branch_id']);
        $data['amt_word'] = convert_amount_to_word_rupees($data['po']->po_net_amount);
        $data['supplier'] = $this->purchase_order_model->supplier_address_by_id($data['po']->sup_id);
        $data['user_data'] = get_user_data();
        $data['branch'] = $this->company_model->branch_by_id($data['po']->branch_id);
        #$data['branch'] = $this->company_model->branch_by_id($user_data['branch_id']);
        $data['company'] = $this->company_model->get_company();
        $filename = 'Purchase_' . $data['po']->po_no;
        $filename = strtoupper($filename);
        // Load Views
        $header = $this->load->view('purchase_order/pdf_header', $data, TRUE); 
        $content = $this->load->view('purchase_order/pdf', $data, TRUE);
        $footer = $this->load->view('purchase_order/pdf_footer', $data, TRUE);
		
        #echo $header.$content.$footer.$filename;exit;
        $this->load->helper(array('My_Pdf'));
        invoice_pdf($header, $content, $footer, $filename);
        exit(0);
    }

    public function purchase_pdf($id) {
        $purchase = $this->purchase_order_model->get_by_id($id);
        $po_items = $this->purchase_order_model->get_item_by_id($id);
        $amt_word = convert_amount_to_word_rupees($purchase->po_net_amount);
        
        $current_branch = $this->company_model->branch_by_id($user_data['branch_id']);
        $company = $this->company_model->get_company();
        
        #echo '<pre>';print_r($current_branch); print_r($company);exit;
        $user_data = get_user_data();
        $this->load->helper(array('My_Pdf'));   //  Load 
        $header = '';
        $data = '';
        $data .= '<table style="width: 100%; font-size: 13px; border-collapse:collapse; margin:20px 0; margin-bottom:4px;">';
        $data .= '<tr>';
        $data .= '<td style="width:10%; border: 1px solid; border-right: 0;"><img src="' . base_url() . DIR_COMPANY_LOGO . $company->comp_logo . '" width="200" style="margin-left:15px;"/></td>';
        $data .= '<td colspan="3" style="width:88%; border: 1px solid; height:100px; text-align:center; padding:10px; border-left: 0; border-right: 0;">';
        $data .= '<table style="width: 100%; font-size: 13px; border-collapse:collapse;">';

        $data .= '<tr><th style="padding:4px; padding-top:0px; font-size:20px; color:#0000ff;">' . $company->comp_name . '</th></tr>';
        $data .= '<tr><td style="padding:4px; text-align:center;">' . nl2br($current_branch->address) . '</td></tr>';
        #$data .= '<tr><td style="padding:4px; text-align:center;">Singapore 208315</td></tr>';
        $data .= '<tr><td style="padding:4px; text-align:center;">Phone : ' . $company->comp_phone . '</td></tr>';
        $data .= '<tr><td style="padding:4px; text-align:center;">E-mail : ' . $company->comp_email . '</td></tr>';
        $data .= '<tr><td style="padding-top:10px; text-align:center;"><strong>Purchase Order</strong></td></tr>';
        $data .= '</table>';
        $data .= '</td>';

        $data .= '<td colspan="1" style="height:100px; border: 1px solid; text-align:center; padding:10px; border-left: 0;"></td>';

        $data .= '</tr>';
        $data .= '<tr style="border: 1px solid #000; border-top: 0px;">';
        $data .= '<td style="width:10%; vertical-align: top; padding-left:6px; border-right-color: #fff;">';
        $data .= '<table style="width: 100%; font-size: 13px; border-collapse:collapse;">';
        $data .= '<tr>';
        $data .= '<td style=" padding:4px;">M/S.</td>';
        $data .= '</tr>';
        $data .= '<tr>';
        $data .= '<td style=" padding:4px;">&nbsp;</td>';
        $data .= '</tr>';
        $data .= '<tr>';
        $data .= '<td style=" padding:4px;">&nbsp;</td>';
        $data .= '</tr>';
        $data .= '</table>';
        $data .= '</td>';
        $data .= '<td style="width:50%; padding-left:6px; border-right-color: #fff;">';
        $data .= '<table style="width: 100%; font-size: 13px; border-collapse:collapse;">';
        $data .= '<tr>';
        $data .= '<td style=" padding:4px;">' . $purchase->name . '</td>';
        $data .= '</tr>';
        $data .= '<tr>';
        $data .= '<td style=" padding:4px;">' . nl2br($purchase->address) . '</td>';
        $data .= '</tr>';
        $data .= '<tr>';
        $data .= '<td style=" padding:4px;">&nbsp;</td>';
        $data .= '</tr>';
        $data .= '</table>';
        $data .= '</td>';
        $data .= '<td style="width:16%; vertical-align: top; font-weight:bold; padding-left:6px; border-right-color: #fff;">';
        $data .= '<table style="width: 100%; font-size: 13px; border-collapse:collapse;">';
        $data .= '<tr>';
        $data .= '<td style=" padding:4px; valign="top";"><strong>PO Number</strong></td>';
        $data .= '</tr>';
        $data .= '<tr>';
        $data .= '<td style=" padding:4px;"><strong>PO Date</strong></td>';
        $data .= '</tr>';
        $data .= '<tr>';
        $data .= '<td style=" padding:4px;"><strong>PO Status</strong></td>';
        $data .= '</tr>';
        $data .= '</table>';
        $data .= '</td>';
        $data .= '<td style="width:8%; vertical-align: top; font-weight:bold; padding-left:6px; border-right-color: #fff;">';
        $data .= '<table style="width: 100%; font-size: 13px; border-collapse:collapse;">';
        $data .= '<tr>';
        $data .= '<td style=" padding:4px; valign="top";">:</td>';
        $data .= '</tr>';
        $data .= '<tr>';
        $data .= '<td style=" padding:4px;">:</td>';
        $data .= '</tr>';
        $data .= '<tr>';
        $data .= '<td style=" padding:4px;">:</td>';
        $data .= '</tr>';
        $data .= '</table>';
        $data .= '</td>';
        $data .= '<td style="width:16%; padding-left:6px; vertical-align: top;">';
        $data .= '<table style="width: 100%; font-size: 13px; border-collapse:collapse;">';
        $data .= '<tr>';
        $data .= '<td style=" padding:4px;">' . $purchase->po_no . '</td>';
        $data .= '</tr>';
        $data .= '<tr>';
        $data .= '<td style=" padding:4px;">' . date('d/m/Y', strtotime($purchase->po_date)) . '</td>';
        $data .= '</tr>';
        $data .= '<tr>';
        if ($purchase->po_status == 1) {
            $status = 'New';
        } elseif ($purchase->po_status == 2) {
            $status = 'Pending';
        } elseif ($purchase->po_status == 3) {
            $status = 'Closed';
        }
        $data .= '<td style=" padding:4px;">' . $status . '</td>';
        $data .= '</tr>';
        $data .= '</table>';
        $data .= '</td>';
        $data .= '</tr>';
        $data .= '</table>';
        $data .= '<table style="width: 100%; font-size: 13px; border-collapse:collapse; margin:7px 0;" border="0">';
        $data .= '<tr>';
        $data .= '<td>We are pleased to release our Purchase Order towards the supply of the following items:</td>';
        $data .= '</tr>';
        $data .= '</table>';
        $data .= '<table cellspacing="0" cellpadding="5" style="width: 100%; font-size: 13px; border-collapse:collapse; margin:7px 0;">';
        $data .= '<tr>';
        $data .= '<th style=" width: 5%; height: 50px; padding:0 4px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;">S.No.</th>';
        $data .= '<th style=" width: 22%; height: 50px; padding:0 4px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">Item Code</th>';
        $data .= '<th style=" width: 32%; height: 50px; padding:0 4px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">Item Description</th>';
//       $data .= '<th style=" width: 10%; height: 50px; padding:0 4px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">UOM</th>';
        $data .= '<th style=" width: 8%; height: 50px; padding:0 4px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">Quantity</th>';
        $data .= '<th style=" width: 10%; height: 50px; padding:0 4px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">Rate</th>';
        $data .= '<th style=" width: 13%; height: 50px; padding:0 4px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">Amount</th>';
        $data .= '</tr>';

        foreach ($po_items as $key => $item) {
            $data .= '<tr>';
            $data .= '<td style="padding-bottom:15px; border-left: 1px solid #000; border-right: 1px solid #000;">' . ++$key . '</td>';
            $data .= '<td style="padding-bottom:15px; border-right: 1px solid #000;">' . $item->pdt_code . '</td>';
            $data .= '<td style="padding-bottom:15px; border-right: 1px solid #000;">' . $item->product_description . '</td>';
//           $data .= '<td style="text-align:center; padding-bottom:15px; border-right: 1px solid #000;">' . $item->unit_name . '</td>';
            $data .= '<td style="text-align:center; padding-bottom:15px; border-right: 1px solid #000;">' . $item->quantity . '</td>';
            $data .= '<td style=" padding-bottom:15px; text-align: right; padding-right: 3px; border-right: 1px solid #000;">' . number_format($item->price, 2) . '</td>';
            $data .= '<td style=" padding-bottom:15px; text-align: right; padding-right: 3px; border-right: 1px solid #000;">' . number_format($item->total, 2) . '</td>';
            $data .= '</tr>';
        }

        $data .= '<tr>';
        $data .= '<td style="padding-bottom:10px; border-left: 1px solid #000; border-right: 1px solid #000;"></td>';
        $data .= '<td style="padding-bottom:10px;border-right: 1px solid #000;"></td>';
        $data .= '<td style="padding-bottom:10px; border-right: 1px solid #000;"><strong>Total Item Amount S$</strong></td>';
//       $data .= '<td style="text-align:center; padding-bottom:10px; border-right: 1px solid #000;"></td>';
        $data .= '<td style="text-align:center; padding-bottom:10px; border-right: 1px solid #000;"></td>';
        $data .= '<td style=" padding-bottom:10px; text-align: right; padding-right: 3px; border-right: 1px solid #000;"></td>';
        $data .= '<td style=" padding-bottom:10px; text-align: right; padding-right: 3px; border-right: 1px solid #000;"><strong>' . number_format($purchase->po_sub_total, 2) . '</strong></td>';
        $data .= '</tr>';
        $val = ($purchase->po_discount_type == 1) ? ' (' . $purchase->po_discount . ' %' . ' )' : '';
        $data .= '<tr>';
        $data .= '<td style="padding-bottom:10px; border-left: 1px solid #000; border-right: 1px solid #000;"></td>';
        $data .= '<td style="padding-bottom:10px;border-right: 1px solid #000;"></td>';
        $data .= '<td style="padding-bottom:10px; border-right: 1px solid #000;"><strong>Total Discount Amount ' . $val . ' S$</strong></td>';
//       $data .= '<td style="text-align:center; padding-bottom:10px; border-right: 1px solid #000;"></td>';
        $data .= '<td style="text-align:center; padding-bottom:10px; border-right: 1px solid #000;"></td>';
        $data .= '<td style=" padding-bottom:10px; text-align: right; padding-right: 3px; border-right: 1px solid #000;"></td>';
        $data .= '<td style=" padding-bottom:10px; text-align: right; padding-right: 3px; border-right: 1px solid #000;"><strong>' . number_format($purchase->po_total_discount, 2) . '</strong></td>';
        $data .= '</tr>';

        $data .= '<tr>';
        $data .= '<td style="padding-bottom:10px; border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;"></td>';
        $data .= '<td style="padding-bottom:10px; border-bottom: 1px solid #000; border-right: 1px solid #000;"></td>';
        $data .= '<td style="padding-bottom:10px; border-bottom: 1px solid #000; border-right: 1px solid #000;"></td>';
//       $data .= '<td style="text-align:center; padding-bottom:10px; border-bottom: 1px solid #000; border-right: 1px solid #000;"></td>';
        $data .= '<td style="text-align:center; padding-bottom:10px; border-bottom: 1px solid #000; border-right: 1px solid #000;"></td>';
        $data .= '<td style=" padding-bottom:10px; text-align: right; padding-right: 3px; border-bottom: 1px solid #000; border-right: 1px solid #000;"></td>';
        $data .= '<td style=" padding-bottom:10px; text-align: right; padding-right: 3px; border-bottom: 1px solid #000; border-right: 1px solid #000;"></td>';
        $data .= '</tr>';
        $data .= '</table>';

        $data .= '<table style="width: 100%; font-size: 13px; border-collapse:collapse; margin-bottom:100px;" border="0">';
        $data .= '</tr>';
        $data .= '<tr>';

        $data .= '<td width="44%" style="padding: 5px 0;"><strong>Total Amount (with Discount) S$</strong></td>';
        $data .= '<td width="6%" style="padding: 5px 0; text-align:right;"><strong>' . number_format($purchase->po_total_with_discount, 2) . '</strong></td>';
        $data .= '</tr>';

        $data .= '<tr>';
        $data .= '<td width="44%" style="padding: 5px 0;"><strong>Total GST (' . $purchase->gst_val . '%) S$</strong></td>';
        $data .= '<td width="6%" style="padding: 5px 0; text-align:right;"><strong>' . number_format($purchase->gst_total, 2) . '</strong></td>';
        $data .= '</tr>';

        $data .= '<tr>';
        $data .= '<td width="44%" style="padding: 5px 0;"><strong>Net Amount S$</strong></td>';
        $data .= '<td width="6%" style="padding: 5px 0; text-align:right;"><strong>' . number_format($purchase->po_net_amount, 2) . '</strong></td>';
        $data .= '</tr>';

        $data .= '</table>';

        $data .= '<table style="width: 100%; font-size: 13px; border-collapse:collapse;" border="1">';
        $data .= '<tr>';
        $data .= '<td width="100%" style="padding:5px;"><strong>PO Value :</strong>' . $amt_word . '</td>';
        $data .= '</tr>';
        $data .= '</table>';
        $data .= '<table style=" float:right; font-size: 13px; width: 100%; border-collapse:collapse; margin-top:6px; margin-bottom:70px;" border="0">';
        $data .= '<tr>';
        $data .= '<td width="50%" style="padding:4px;">Contact : </td>';
        $data .= '<td width="50%" style="padding-left:8px; text-align: right;">For ' . $company->comp_name . '</td>';
        $data .= '</tr>';
        $data .= '<tr>';
        $data .= '<td width="50%" style="padding:4px;">Mobile : </td>';
        $data .= '<td width="50%" style="padding-left:8px;">&nbsp;</td>';
        $data .= '</tr>';
        $data .= '</table>';
        $data .= '<table style=" float:right; font-size: 13px; width: 100%; border-collapse:collapse; margin:20px 0;" border="0">';
        $data .= '<tr>';
        $data .= '<td width="50%" style="padding:4px;">&nbsp;</td>';
        $data .= '<td width="50%" style="padding:0px; font-size: 13px; text-align: right;">Authorized Signatory</td>';
        $data .= '</tr>';
        $data .= '</table>';
        $footer = '';
        $title = 'Purchase Order';
//       echo $header . $data . $footer;exit;
        create_pdf($header, $data, $footer, $title); //Create pdf
    }
    
    public function update_price_log($pid, $bprice, $sprice, $tit) {
        $last = $this->product_model->get_product_last_price_log($pid);
        $last_id = '';
        if (($last->buying_price != $bprice) || ($last->selling_price != $sprice)) {
            $insert = array(
                'product_id' => $pid,
                'buying_price' => $bprice,
                'selling_price' => $sprice,
                'updated_from' => $tit,
                'brn_id' => $this->user_data['branch_id'],
                'created_by' => $this->user_data['user_id'],
                'created_on' => date('Y-m-d H:i:s'),
                'ip_address' => $_SERVER['REMOTE_ADDR']
            );
            $last_id = $this->product_model->insert_cost_log($insert);
        }
        return $last_id;
    }
    public function active_suppliers() {
        $data = array();
        $data = $this->purchase_order_model->get_all_suppliers();
        $this->ajax_response($data);
    }
    
    
    public function send_mail($id = '') {
        if (empty($id)) {
            show_400_error();
        }
		
		$data = array();
        $user_data = get_user_data();
		$this->user_data;
		
		#$to = $data['sales_order']->email;
        $to = $this->input->post('to_mail');
		if (empty($to)) {
			$_SESSION['error'] = 'Customer email is empty. Please add customer email and try again!';
			redirect('purchase_orders', 'refresh');
		}
        
		$data['po'] = $this->purchase_order_model->get_by_id($id);
        $data['po_items'] = $this->purchase_order_model->get_item_by_id($id);
        $data['address'] = $this->purchase_order_model->get_current_address($user_data['branch_id']);
        $data['amt_word'] = convert_amount_to_word_rupees($data['po']->po_net_amount);
        $data['supplier'] = $this->purchase_order_model->supplier_address_by_id($data['po']->sup_id);
        //$data['user_data'] = get_user_data();
        $data['branch'] = $this->company_model->branch_by_id($data['po']->branch_id);
        //$data['branch'] = $this->company_model->branch_by_id($user_data['branch_id']);
        $data['company'] = $this->company_model->get_company();
        $filename = 'Purchase_' . $data['po']->po_no;
        $filename = strtoupper($filename) . '.pdf';
		
        // Load Views
        $header = $this->load->view('purchase_order/pdf_header', $data, TRUE); 
        $content = $this->load->view('purchase_order/pdf', $data, TRUE);
        $footer = $this->load->view('purchase_order/pdf_footer', $data, TRUE);
		
		require(APPPATH . '/helpers/mpdf/mpdf.php');
		$mpdf = new mPDF('utf-8', // mode - default ''
			'A4', // format - A4, for example, default ''
			0, // font size - default 0
			'', // default font family
			'15', // 15 margin_left
			'5', // 15 margin right
			'0', // 16 margin top 84
			'0', // margin bottom 75 150 140
			'8', // 9 margin header
			'0', // 9 margin footer
			'P'
		);
		$mpdf->setAutoTopMargin = 'stretch';
		$mpdf->setAutoBottomMargin = 'stretch';
		$mpdf->WriteHTML($header);
        $mpdf->WriteHTML($content);
		$mpdf->SetHTMLFooter($footer);
		
		$file_path = PO_EMAIL_PATH;
		if (!is_dir($file_path)) {
			if (!mkdir($file_path, 0777, true)) 
				error_log('Failed to create folder for ' . $branch_name);
		}
		
		$file_path = $file_path . $filename;
		$mpdf->Output($file_path, 'F');
		
		/* From Email */
		$from = (!empty($data['company']->comp_email)) ? $data['company']->comp_email : 'info@refulgenceinc.com';
		$subject = 'UCS - Purchase Order(' . $data['po']->po_no . ')';
		
		$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "From: " . $from . "\r\n";
		$headers .= "Reply-To: " . $from . "\r\n";
		$headers .= "X-Mailer: PHP/" . phpversion();
		
		// boundary
		$semi_rand = md5(time());
		$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
		
		// headers for attachment 
		$headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"";
		
		// multipart boundary 
		$mail_html = "This is a multi-part message in MIME format.\n\n" . "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"iso-8859-1\"\n" . "Content-Transfer-Encoding: 7bit\n\n" . $mail_html . "\n\n"; 
		// preparing attachments
		
		$file_ptr = fopen($file_path, "rb");
		$data = fread($file_ptr, filesize($file_path));
		fclose($file_ptr);
		$data = chunk_split(base64_encode($data));
		
		$mail_html .= "--{$mime_boundary}\n";
		$mail_html .= "Content-Type: {\"application/octet-stream\"};\n" . " name=\"$file_name\"\n";
		$mail_html .= "Content-Disposition: attachment;\n" . " filename=\"$file_name\"\n";
		$mail_html .= "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
		$mail_html .= "--{$mime_boundary}--\n";
		#echo $mail_html;exit;
		
		$ok = @mail($to, $subject, $mail_html, $headers);
		if ($ok) {
			$_SESSION['success'] = 'Purchase Order sent successfully to the customer email <span class="lowercase">' . $to . '</span>.';
		}
		else {
			$_SESSION['error'] = 'Purchase Order not sent to the customer email <span class="lowercase">' . $to . '</span>. Try again!';
		}
		
		$insert = array(
			'po_id' => $id,
			'created_on' => date('Y-m-d H:i:s'),
			'created_by' => $this->user_data['user_id']
		);
		
		$this->purchase_order_model->insert_purchase_email_sent($insert);
		unlink($file_path);
		redirect('purchase_orders', 'refresh');
    }
    
    public function get_email_by_id() {
        $data = $this->purchase_order_model->get_by_id($_POST['id']);
        echo json_encode($data);
    }

}

?>