<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Purchase_Returns extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('purchase_return_model', '', TRUE);
        $this->load->model('purchase_order_model', '', TRUE);
        $this->load->model('purchase_receive_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->library('Datatables');
    }
    
    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link']  = $actions['view'] . '/';
        $data['add_link']   = $actions['add'];
        $data['edit_link']  = $actions['edit'] . '/';
        $data['delete_link']= $actions['delete'] . '/';
        $data['returns'] = $this->purchase_return_model->list_all();
        $this->render($data, 'purchase_return/list');
    }

    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('por.id AS prId, DATE_FORMAT(por.return_date, "%d/%m/%Y") as pr_return_date, pr.supplier_invoice_no AS inv, DATE_FORMAT(pr.invoice_date, "%d/%m/%Y") as inv_date, SUM(pori.returned_quantity) as return_qty', FALSE)
                ->where('por.status != 10 AND por.branch_id IN ("' . $this->user_data['branch_id'] . '")')
                ->join(TBL_PORETI . ' as pori', 'pori.po_ret_id = por.id AND pori.status != 10')
                ->join(TBL_PR . ' as pr', 'pr.id = por.precv_id')
                ->join(TBL_BRN . ' as brn', 'brn.id = por.brn_id')
                ->group_by('por.id')
                ->add_column('Actions', $this->get_buttons('$1'), 'prId')
                ->from(TBL_PORET . ' AS por');
        echo $this->datatables->generate();
    }

    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(3, $this->permission)) {
            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
        }
        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Purchase Return!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Purchase Return"><span>Delete</span></a>';
        }
        $html.='</span>';
        return $html;
    }

    public function add() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();        
        $data['list_link']   = $actions['index']; 
        $data['form_action'] = $actions['insert'];
        $data['purchases'] = $this->purchase_return_model->get_purchase_receive_list();
        $this->render($data, 'purchase_return/add');
    }
    
    public function insert() {
        $user_data = get_user_data();        
        $prefix_wt_sub = CODE_PORET;
        $por_no = $this->auto_generation_code(TBL_PORET, $prefix_wt_sub, '', 3, '');        
        $item = array(
            'por_no' => $por_no,
            //'po_id' => $this->input->post('po_id'),
			'precv_id' => $this->input->post('po_id'),
            'branch_id' => $user_data['branch_id'],
            'return_date' => date('Y-m-d', strtotime(str_replace('/','-',$this->input->post('por_date')))),
            'po_ret_remarks' => $this->input->post('por_remarks'),
            'status' => $this->input->post('status'),
            'brn_id' => $user_data['branch_id'],
            'created_by' => $user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s'),
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );        
        $last_id = $this->purchase_return_model->insert($item);
        
        if (  $last_id > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, $por_no, '');
            $_SESSION['success'] = 'Purchase Return added successfully';            
            $category_id = $this->input->post('category_id');
            $product_id = $this->input->post('product_id');
            $product_code = $this->input->post('product_code');
            $product_description = $this->input->post('product_description');
            $grni_quantity = $this->input->post('por_qty');
            $received_quantity = $this->input->post('rec_quantity');
            $returned_quantity = $this->input->post('retn_quantity');
            
            foreach($category_id as $key => $category_id){
				$item = array(
                    'po_ret_id' => $last_id,
                    'product_id' =>$product_id[$key],
                    'category_id' =>$category_id,
                    'product_description' =>$product_description[$key],
                    'por_quantity' =>$grni_quantity[$key],
                    'received_quantity' =>$received_quantity[$key],
                    'returned_quantity' =>$returned_quantity[$key],
                    'brn_id' => $user_data['branch_id'],
                    'created_by' => $user_data['user_id'],
                    'created_on' => date('Y-m-d H:i:s'),
                    'ip_address' => $_SERVER['REMOTE_ADDR']
                );                
                $this->purchase_return_model->insert_item($item);
				
				//if ($returned_quantity[$key] > 0) {
					// Update Return Quantity in Warehouse
					$this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $returned_quantity[$key], '-');
				//}
            }            
        }
        else 
            $_SESSION['error'] = 'Purchase Return not added!';
        
        redirect('purchase_returns/add', 'refresh');
    }
    
    public function edit($id = '') {
        if (empty($id)) {
            show_400_error();
        }        
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['purchases'] = $this->purchase_return_model->get_purchase_receive_list();
        $data['return'] = $this->purchase_return_model->get_by_id($id);
        $data['materials'] = $this->purchase_return_model->get_material_by_po($data['return']->po_id);
        $data['return_items'] = $this->purchase_return_model->get_items_by_por($id);
		#echo '<pre>';print_r($data);exit;
        $actions = $this->actions();
        $data['list_link']   = $actions['index']; 
        $data['form_action'] = $actions['update'] . '/' . $id;
        $this->render($data, 'purchase_return/edit');
    }
    
    public function update($id = '') {
        //echo '<pre>';print_r($_REQUEST);die();
        if (empty($id) && empty($user_data['branch_id'])) {
            show_400_error();
        }        
        $user_data = get_user_data();        
        $prefix_wt_sub = CODE_PORET;
        $por_no = $this->auto_generation_code(TBL_PORET, $prefix_wt_sub, '', 3, $id);        
        $items = array(
            'por_no' => $por_no,
            //'po_id' => $this->input->post('hidden_po_id'),
            'branch_id' => $user_data['branch_id'],
            'return_date' => date('Y-m-d', strtotime(str_replace('/','-',$this->input->post('por_date')))),
			'po_ret_remarks' => $this->input->post('por_remarks'),
            'status' => $this->input->post('status'),
            'brn_id' => $user_data['branch_id'],
            'updated_by' => $user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s'),
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        $affected_rows = $this->purchase_return_model->update($id, $items);        
        
        /*** Update Old Item ***/
//        $por_id = $this->input->post('por_id');
//        
//        if(!empty($this->input->post('edit_por_id'))) {
//            $old_mat_id = $this->input->post('mat_id');
//            $old_unit_id = $this->input->post('unit_id');
//            $old_grni_quantity = $this->input->post('grni_quantity');
//            $old_received_quantity = $this->input->post('received_quantity');
//            $old_returned_quantity = $this->input->post('returned_quantity');
//            $edit_por_id = $this->input->post('edit_por_id');
//            
//            foreach($por_id as $key => $por_edit_id) {
//                $item = array(
//                    'po_ret_id' => $id,
//                    'product_id' =>$old_mat_id[$key],
//                    //'unit_id' =>$old_unit_id[$key],
//                    'por_quantity' =>$old_grni_quantity[$key],
//                    'received_quantity' =>$old_received_quantity[$key],
//                    'returned_quantity' =>$old_returned_quantity[$key],
//                    'brn_id' => $user_data['branch_id'],
//                    'updated_by' => $user_data['user_id'],
//                    'updated_on' => date('Y-m-d H:i:s'),
//                    'ip_address' => $_SERVER['REMOTE_ADDR']
//                );
//                
//                /*** Update Warehouse Material ***/
//                $old_quantity = $this->purchase_return_model->get_old_mat_quantity($old_mat_id[$key],$por_edit_id);
//                $ava_qty = $old_returned_quantity[$key] - $old_quantity;
//                if( $ava_qty > 0){
//                   $this->purchase_receive_model->update_branch_material_qty($user_data['branch_id'],$old_mat_id[$key],$ava_qty, '-');
//               }
//               else{
//                 $quantity = $this->purchase_receive_model->branch_materials($user_data['branch_id'],$old_mat_id[$key])->row();
//                 if($quantity->available_qty > $ava_qty ) {
//                   $this->purchase_receive_model->update_branch_material_qty($user_data['branch_id'],$old_mat_id[$key],$ava_qty, '-');
//                 }
//              }
//                  $this->purchase_return_model->update_item($por_edit_id,$item);
//                if (in_array($por_id[$key], $edit_por_id)) {
//                    $item_id_index = array_search($por_id[$key], $edit_por_id);
//                    unset($edit_por_id[$item_id_index]);
//                 }
//            }
//        }
//        
//        foreach($edit_por_id as $key => $item_id) {
//            $this->purchase_return_model->row_delete_item($id,$item_id);
//        }        
        
        $hidden_pri_id = $this->input->post('pori_id');
        $category_id = $this->input->post('category_id');
        $product_id = $this->input->post('product_id');
        $product_code = $this->input->post('product_code');
        $product_description = $this->input->post('product_description');
        $grni_quantity = $this->input->post('por_qty');
        $received_quantity = $this->input->post('rec_quantity');
        $returned_quantity = $this->input->post('retn_quantity');
        
        foreach ($hidden_pri_id as $key => $pri_id) {
            // Old sales order item 
            $old_pr_item = $this->purchase_return_model->get_pr_item($pri_id);

            if ($old_pr_item->product_id == $product_id[$key]) {
                $item = array(
                    'po_ret_id' => $id,
                    'product_id' =>$product_id[$key],
                    'category_id' =>$category_id[$key],
                    'product_description' =>$product_description[$key],
                    'por_quantity' =>$grni_quantity[$key],
                    'received_quantity' =>$received_quantity[$key],
                    'returned_quantity' =>$returned_quantity[$key],
                    'brn_id' => $user_data['branch_id'],
                    'updated_by' => $user_data['user_id'],
                    'updated_on' => date('Y-m-d H:i:s')
                );
                
                if ($this->purchase_return_model->update_item($pri_id,$item)) {
                    $update_quantity = $returned_quantity[$key] - $old_pr_item->returned_quantity;
                    $this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $update_quantity, '-');
                }
                
            }
			else {
                $item = array(
                    'po_ret_id' => $id,
                    'product_id' =>$product_id[$key],
                    'category_id' =>$category_id[$key],
                    'product_description' =>$product_description[$key],
                    'por_quantity' =>$grni_quantity[$key],
                    'received_quantity' =>$received_quantity[$key],
                    'returned_quantity' =>$returned_quantity[$key],
                    'brn_id' => $user_data['branch_id'],
                    'updated_by' => $user_data['user_id'],
                    'updated_on' => date('Y-m-d H:i:s')
                );

                if ($this->purchase_return_model->insert_item($item)) {
                    /* To update old item stock also delete from sales order items start */
                    $this->product_model->update_branch_stock($user_data['branch_id'], $old_pr_item->product_id, $old_pr_item->returned_quantity, '+');
                    $this->purchase_return_model->delete_pr_item($pri_id);
                    /* To update old item stock also delete from sales order items end */

                    // To update new item quantity in stock
                    $this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $returned_quantity[$key], '-');
                }                
            }
        }
        // Update existing product end
        // Delete existing row start
        $delete_items = $this->purchase_return_model->get_removed_pr_items($id, $product_id);
        foreach ($delete_items as $key => $remove_item) {
            if ($this->purchase_return_model->delete_pr_item($remove_item->id))
                $this->product_model->update_branch_stock($user_data['branch_id'], $remove_item->product_id, $remove_item->returned_quantity, '+');
        }      
        
        /*** Insert New Item ***/
        $category_id = $this->input->post('new_category_id');
        $product_id = $this->input->post('new_product_id');
        $product_code = $this->input->post('new_product_code');
        $product_description = $this->input->post('new_product_description');
        $grni_quantity = $this->input->post('new_por_qty');
        $received_quantity = $this->input->post('new_rec_quantity');
        $returned_quantity = $this->input->post('new_retn_quantity');
        if(!empty($product_id)){
            foreach($product_id as $key => $product_id){
                $item = array(
                    'po_ret_id' => $id,
                    'product_id' =>$product_id[$key],
                    'category_id' =>$category_id[$key],
                    'product_description' =>$product_description[$key],
                    'por_quantity' =>$grni_quantity[$key],
                    'received_quantity' =>$received_quantity[$key],
                    'returned_quantity' =>$returned_quantity[$key],
                    'brn_id' => $user_data['branch_id'],
                    'created_by' => $user_data['user_id'],
                    'created_on' => date('Y-m-d H:i:s'),
                    'ip_address' => $_SERVER['REMOTE_ADDR']
                );                
                $this->purchase_return_model->insert_item($item);                
                // Update Return Quantity in Warehouse
                $this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $returned_quantity[$key], '-');               
            }
        }
        if ( $affected_rows > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $por_no, '');
            $_SESSION['success'] = 'Purchase Return updated successfully';
        } else {
            $_SESSION['error'] = 'Purchase Return not updated!';
        }
		
        redirect('purchase_returns/edit/' . $id, 'refresh');
    }
    
    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();        
        $data['list_link']   = $actions['index'];
        $data['return'] = $this->purchase_return_model->get_by_id($id);
        #$data['supplier'] = $this->purchase_receive_model->get_supplier_by_po($data['return']->po_id);
        $data['return_items'] = $this->purchase_return_model->get_items_by_por($id);
        $this->render($data, 'purchase_return/view');
    }
    
    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }       
        $user_data = get_user_data();
        $return = $this->purchase_return_model->get_by_id($id);
        $return_items = $this->purchase_return_model->get_items_by_por($id);
        $affected_rows = 1;
        foreach($return_items as $key => $items) {
            // Update Return Quantity in Warehouse
            $this->purchase_receive_model->update_branch_material_qty($return->branch_id, $items->product_id, $items->returned_quantity, '+');
        }
		
        if($affected_rows) {
           $pors = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $user_data['user_id']
          ); 
          
          $this->purchase_return_model->update($id,$pors);
          $this->purchase_return_model->update_item($id,$pors);
          $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $return->por_no, 1);
          $_SESSION['success'] = 'Purchase Returns deleted successfully!';
        }
        else{
           $_SESSION['error'] = 'Purchase Returns not deleted!';
        }
		
        redirect('purchase_returns', 'refresh');
    }
	
    public function get_branch_by_po(){
        $po_id = $_REQUEST['po_id'];
        $data = array();
        if(!empty($po_id)){
            $data['brn'] = $this->purchase_receive_model->get_branch_by_po($po_id);
            $data['supplier'] = $this->purchase_receive_model->get_supplier_by_po($po_id);
        }
        echo json_encode($data);
    }
    
    public function get_material_by_po(){
        $po_id = $_REQUEST['po_id'];
        //$por_id = $_REQUEST['por_id'];
        $data = array();
        if(!empty($po_id)){
            $data['mats'] = $this->purchase_return_model->get_material_by_po($po_id);
        }
        //error_log($this->db->last_query());
        echo json_encode($data);
    }
    
    public function get_item_qty_by_mat_id() {
        $mat_id = $_REQUEST['mat_id'];
        $ware_id = $_REQUEST['ware_id'];
        $data = array();
        if(!empty($mat_id)){
            $data['quantity'] = $this->purchase_return_model->get_item_qty_by_mat_id($mat_id,$ware_id);
        }
        echo json_encode($data);
    }
    
    public function search_item_description() {
        $po = $this->input->post('po');
        $data = array();        
        $data = $this->purchase_return_model->get_product_for_autocomplete($po);
        
        foreach ($data as $key => $value) {
            $value->label = (!empty($value->cat_name)) ? $value->cat_name . ' ' : '';
            $value->label .= $value->pdt_name;
            $value->value = $value->label;
        }
        $this->ajax_response($data);
    }

    public function search_item() {
        $po = $this->input->post('po');
        $product_code = $this->input->post('product_code');
        $data = array();       
        $data = $this->purchase_return_model->get_product_for_autocomplete($po);    
        
        foreach ($data as $key => $value) {
            if (!empty($product_code))
                $value->label = $value->pdt_code;
            else
                $value->label = $value->pdt_code . ' / ' . $value->pdt_name;
            $value->value = $value->label;
        }
        $this->ajax_response($data);
    }
}
?>