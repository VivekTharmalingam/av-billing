<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Warranty_Report extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('sales_return_model', '', TRUE);
    }

    public function index() {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['form_action'] = base_url() . 'warranty_report';
        $data['view_link']  = base_url() . 'invoice_return_warrantyitem/view/';

        $warranty_cleared = $this->input->post('warranty_cleared');
        $search_date = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : date('01/m/Y') . ' - ' . date('t/m/Y');

        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'Reset')) {
            $warranty_cleared = '';
            $search_date = date('01/m/Y') . ' - ' . date('t/m/Y');
        }

        if ((!empty($this->input->post('submit'))) && (empty($this->input->post('search_date')))) {
            $search_date = '';
            $from = '';
            $to = '';
        } else {
            $search_date_arr = explode('-', $search_date);
            $from = convert_text_date(trim($search_date_arr[0]));
            $to = convert_text_date(trim($search_date_arr[1]));
        }

        $data['search_date'] = $search_date;
        $data['from_date'] = $from;
        $data['to_date'] = $to;
        $data['warranty_cleared'] = $warranty_cleared;
        
        $data['warranty_rept'] = $this->sales_return_model->warranty_report($from, $to, $warranty_cleared);
        
        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Warranty Reports';
            $head['setting'] = $this->company_model->get_company();
            $filename = 'Warranty_Report_' . current_date();
            // Load Views
            $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
            $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
            $header = '<div></div>';
            $content = $this->load->view('warranty_report/pdf_report', $data, TRUE);
            $footer = '<div></div>';
            $this->load->helper(array('My_Pdf'));
            create_pdf($header, $content, $footer, $filename);
            exit(0);
        } elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $this->load->view('warranty_report/excel_report', $data);
        } else {
            $this->render($data, 'warranty_report/list');
        }
    }

}

?>