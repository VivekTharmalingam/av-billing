<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Movement_History extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('movement_history_model', '', TRUE);
    }

    private function sort_transctions_by_date($transactions) {
        uasort($transactions, function($a, $b) {
            return strtotime($a->created_on) - strtotime($b->created_on);
        });

        return $transactions;
    }

    private function calculate($operand_1, $operand_2, $op) {
        $result = 0;
        switch ($op) {
            case '+':
                $result = $operand_1 + $operand_2;
                break;

            case '-':
                $result = $operand_1 - $operand_2;
                break;

            default:
                break;
        }

        return $result;
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        $po_items = $this->movement_history_model->po_receive_items();
        //echo $this->db->last_query();
        $po_return_items = $this->movement_history_model->po_return_items();
        $so_items = $this->movement_history_model->so_items();
        $so_ser_items = $this->movement_history_model->so_service_items();
        $so_return_items = $this->movement_history_model->so_return_items();
        $so_return_ser_items = $this->movement_history_model->so_return_service_items();
        $scan_items = $this->movement_history_model->scan_items();
        $inv_adjustment = $this->movement_history_model->inventory_adjustment();
        //echo $this->db->last_query();
        $frm_trn_stk = $this->movement_history_model->from_transfer_stock();
        //echo $this->db->last_query();
        $to_trn_stk = $this->movement_history_model->to_transfer_stock();
        // echo $this->db->last_query();

        $transactions = array_merge($po_items, $po_return_items, $so_items, $so_return_items, $so_return_ser_items, $so_ser_items, $scan_items, $inv_adjustment, $frm_trn_stk, $to_trn_stk);
        $data['transactions'] = $this->sort_transctions_by_date($transactions);
        //echo '<pre>';print_r($data['transactions']);
        $item_stock = array();
        foreach ($transactions as $key => $item) {
            if (array_key_exists($item->item_id, $item_stock)) {
                $item->before_qty = $item_stock[$item->item_id];
                $curent_qty = $this->calculate($item_stock[$item->item_id], $item->quantity, $item->op);
                $item_stock[$item->item_id] = $curent_qty;
                $item->after_qty = $item_stock[$item->item_id];
            } else {
                $item->before_qty = 0;
                $curent_qty = $this->calculate(0, $item->quantity, $item->op);
                $item->after_qty = $curent_qty;
                $item_stock[$item->item_id] = $curent_qty;
            }
        }
        //exit;
        $data['transactions'] = $transactions;
        #echo '<pre>'; print_r($data['transactions']); echo '</pre>'; exit;
        $this->render($data, 'movement_history/list');
    }

}

?>