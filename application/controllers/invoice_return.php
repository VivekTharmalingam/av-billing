<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Invoice_Return extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('sales_order_model', '', TRUE);
        $this->load->model('sales_return_model', '', TRUE);
        $this->load->model('category_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->model('customer_model', '', TRUE);
        $this->load->model('company_model', '', TRUE);
        $this->load->model('services_model', '', TRUE);
        $this->load->library('Datatables');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        $data['returns'] = $this->sales_return_model->list_all();
        $this->render($data, 'sales_return/list');
    }
    
    function datatable() {
        $this->datatables->select('sor.id AS srId, DATE_FORMAT(sor.return_date, "%d/%m/%Y") as ret_date, sor.so_id AS soId, so.so_no AS inv, DATE_FORMAT(so.so_date, "%d/%m/%Y") as inv_date, brn.branch_name AS branch, SUM(sori.returned_qty) as return_qty', FALSE)
            ->where('sor.status != 10 AND sor.branch_id IN ("' . $this->user_data['branch_id'] . '")')
            ->join(TBL_SO_RET_ITM . ' as sori', 'sori.so_ret_id = sor.id AND sori.status != 10', 'left')
            ->join(TBL_SO . ' as so', 'so.id = sor.so_id', 'left')
            ->join(TBL_BRN . ' as brn', 'brn.id = sor.branch_id', 'left')
            ->group_by('sor.id')
            ->add_column('Actions', $this->get_buttons('$1'), 'srId')
            ->from(TBL_SO_RET . ' AS sor');
        echo $this->datatables->generate();
    }

    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';        
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        /*if (in_array(3, $this->permission)) {
            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
        }*/
        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Invoice Return!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Invoice Return"><span>Delete</span></a>';
        }
        $html.='</span>';
        return $html;
    }

    public function add() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['list_link'] = $actions['index'];
        $data['form_action'] = $actions['insert'];
        $data['so'] = $this->sales_return_model->get_so_list();
        $data['categories'] = $this->category_model->list_active();
        $data['items'] = $this->product_model->list_active();
        $data['customers'] = $this->customer_model->list_active();
        $data['company'] = $this->company_model->get_company();
        $this->render($data, 'sales_return/add');
    }

    public function customer_details() {
        $data = array();
        $user_data = get_user_data();
        if ($this->input->post('so_id') != '') {
            $so_id = $this->input->post('so_id');
            $data['so'] = $this->sales_order_model->get_by_id($so_id);
            //$data['addresses'] = $this->customer_model->get_address_by_id($data['so']->customer_id);
            $data['customer'] = $this->customer_model->get_by_id($data['so']->customer_id);
            //$data['items'] = $this->sales_return_model->get_category_by_so($so_id);
//            $data['so_items'] = $this->sales_return_model->get_product_by_category($so_id, '');
            $data['so_items'] = $this->sales_return_model->get_product_by_category_all($so_id, '');
			foreach ($data['so_items'] as $key => $value) {
				$value->label = $value->pdt_code;
				$value->value = $value->label;
			}
            //$data['shipping_address'] = $this->sales_return_model->shipping_address_by_id($data['so']->shipping_address);
            //$data['address'] = $this->sales_order_model->get_current_address($user_data['branch_id']);
        }
		//error_log(print_r($data, 1));
        $this->ajax_response($data);
    }

    public function get_product_by_category() {
        $data = array();
        if ($this->input->post('so') != '' && $this->input->post('cid') != '') {
            $data['so'] = $this->sales_return_model->get_product_by_category($this->input->post('so'), $this->input->post('cid'));
        }
        $this->ajax_response($data);
    }

    public function insert() {
        $prefix_wt_sub = CODE_SORET;
        $sor = $this->auto_generation_code(TBL_SO_RET, $prefix_wt_sub, '', 6, '');
        $sale = array(
            'sor_no' => $sor,
            'so_id' => $this->input->post('so_no'),
            'branch_id' => $this->user_data['branch_id'],
            'return_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('so_date')))),
            'so_return_remarks' => $this->input->post('re_remarks'),
            'refund_amount' => $this->input->post('refund_amount'),
            'brn_id' => $this->user_data['branch_id'],
            'created_by' => $this->user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s'),
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        $last_id = $this->sales_return_model->insert($sale);
		
        if ($last_id > 0) {
            $data_sales_order = $this->sales_order_model->get_by_id($this->input->post('so_no'));
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, $data_sales_order->so_no, '');
            $_SESSION['success'] = 'Invoice Returned successfully';
            $product_id = $this->input->post('product_id');
            $so_item_autoid = $this->input->post('so_item_autoid');
            $product_code = $this->input->post('product_code');
			$product_description = $this->input->post('product_description');
            $return_quantity = $this->input->post('return_quantity');
            $serial_nos_txt = $this->input->post('serial_nos_txt');
            $return_item_type = $this->input->post('return_item_type');
            $restock_quantity = $this->input->post('restock_quantity');
			
            foreach ($product_id as $key => $value) {
                $serial_nos = (count($serial_nos_txt[$key])>0) ? implode(',', $serial_nos_txt[$key]) : '';
                $item = array(
                    'so_ret_id' => $last_id,
                    'so_item_autoid' => $so_item_autoid[$key],
                    'pdt_desc' => $product_description[$key],
                    'item_id' => $product_id[$key],
                    //'so_quantity' => $grni_quantity[$key],
					'return_item_type' => $return_item_type[$key],
                    'returned_qty' => $return_quantity[$key],
                    'restock_quantity' => $restock_quantity[$key],
                    'serial_nos_txt' => $serial_nos,
                    'brn_id' => $this->user_data['branch_id'],
                    'created_by' => $this->user_data['user_id'],
                    'created_on' => date('Y-m-d H:i:s'),
                    'ip_address' => $_SERVER['REMOTE_ADDR']
                );
                $this->sales_return_model->insert_item($item);
		
                $sales_order_item = $this->sales_return_model->get_sales_order_item_by_id($so_item_autoid[$key]);
                if(count($sales_order_item)>0 && $return_quantity[$key]>0) {
                    if($sales_order_item[0]['quantity']<=$return_quantity[$key]) {
                        $itemdata = array(
                            'is_returned' => 1,
                            'status' => 10,
                            'updated_by' => $this->user_data['user_id'],
                            'updated_on' => date('Y-m-d H:i:s')
                        ); 
                    } else {
                        $inv_update_qty = $sales_order_item[0]['quantity'] - $return_quantity[$key];
                        $qty_prft = $inv_update_qty * ($sales_order_item[0]['price'] - $sales_order_item[0]['unit_cost']);
                        $total_amount = $inv_update_qty * $sales_order_item[0]['price'];
                        $itemdata = array(
                            'quantity' => $inv_update_qty,
                            'total' => $total_amount,
                            'item_profit' => $qty_prft,
                            'is_returned' => 2,
                            'updated_by' => $user_data['user_id'],
                            'updated_on' => date('Y-m-d H:i:s')
                        ); 
                    }
                    $this->sales_order_model->update_item($itemdata, $so_item_autoid[$key]);
                }
                
                if (!empty($restock_quantity[$key])) {

                        $get_so_item = $this->sales_order_model->get_so_item($so_item_autoid[$key]);
                        $product_id_txt = '';
                        if($get_so_item->service_form_type == 2) {
                            $get_pro_itemid = $this->services_model->get_service_item_byid($get_so_item->item_id);
                            $product_id_txt = $get_pro_itemid->item_id;
                        } else if($get_so_item->exc_form_type == 2) {
                            $product_id_txt = $product_id[$key];
                        } else if($get_so_item->service_form_type == 0 && $get_so_item->exc_form_type == 0) {
                            $product_id_txt = $product_id[$key];
                        }
                        // Update Return Quantity in Warehouse
                        if($product_id_txt != "") {
                            $this->product_model->update_branch_stock($this->user_data['branch_id'], $product_id_txt, $restock_quantity[$key], '+');
                        }
                }
            }
            
            $all_sales_order_item = $this->sales_return_model->get_product_soiditem_sum($this->input->post('so_no'));
            $sales_order_data = $this->sales_order_model->get_by_id($this->input->post('so_no'));
            if(count($all_sales_order_item)>0) {
                $sub_total = (!empty($all_sales_order_item[0]['so_total'])) ? $all_sales_order_item[0]['so_total'] : 0;
                $discount_per = 0;$discount_amount = 0;$gst_amount = 0;$tot_amount=0;
                $discount = ($sales_order_data->discount_type==1) ? $sales_order_data->discount : 0;
                if($sub_total > 0) {
                    $discount_per = ($sales_order_data->discount_type==1) ? $sales_order_data->discount : (($sales_order_data->discount / $sales_order_data->sub_total) * 100);
                    $discount = ($sales_order_data->discount_type==1) ? $sales_order_data->discount : (($discount_per / 100) * $sub_total);
                    $discount_amount = (($discount_per / 100) * $sub_total);
                    $with_dis_total = $sub_total - $discount_amount;
                    $gst_amount = (($sales_order_data->gst_percentage / 100) * $with_dis_total);
                    $tot_amount = ($sales_order_data->gst_percentage==2) ? ($with_dis_total + $gst_amount) : $with_dis_total;
                }
                $update_inv = array(
                        'sub_total' => $sub_total,
                        'discount' => $discount,
                        'discount_amount' => $discount_amount,
                        'gst_amount' => $gst_amount,
                        'total_amt' => $tot_amount,
                        'updated_by' => $this->user_data['user_id'],
                        'updated_on' => date('Y-m-d H:i:s')
                );
                $affected_rows = $this->sales_order_model->update($this->input->post('so_no'), $update_inv);
            }
            
        }
		else
            $_SESSION['error'] = 'Invoice not Returned!';
        
        redirect('invoice_return/add', 'refresh');
    }

    /*public function edit($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['so'] = $this->sales_return_model->get_so_list($id);
        #echo $this->db->last_query();exit;
        $data['categories'] = $this->category_model->list_active();
        $data['items'] = $this->product_model->list_active();
        $data['customers'] = $this->customer_model->list_active();
        $data['company'] = $this->company_model->get_company();
        $data['return'] = $this->sales_return_model->get_by_id($id);
        $data['return_items'] = $this->sales_return_model->get_items_by_por($id);
        //echo "<pre>"; print_r($data['return_items']);die;
        $actions = $this->actions();
        $data['list_link'] = $actions['index'];
        $data['form_action'] = $actions['update'] . '/' . $id;
        $this->render($data, 'sales_return/edit');
    }

    public function update($id = '') {        
        
        if (empty($id) && empty($user_data['branch_id'])) {
            show_400_error();
        }
        $user_data = get_user_data();
        $prefix_wt_sub = CODE_SORET;
        $sor_no = $this->auto_generation_code(TBL_SO_RET, $prefix_wt_sub, '', 3, $id);

        $sales = array(
            'sor_no' => $sor_no,
            'so_id' => $this->input->post('hidden_so_id'),
            'branch_id' => $user_data['branch_id'],
            'return_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('so_date')))),
            'status' => $this->input->post('status'),
            'brn_id' => $user_data['branch_id'],
            'updated_by' => $user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s'),
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        $affected_rows = $this->sales_return_model->update($id, $sales);        

        // Update Old Item
        $sor_id = $this->input->post('hidden_sri_id');
        if (!empty($sor_id)) {
            $old_cat_id = $this->input->post('category_id');
            $old_item_id = $this->input->post('product_id');
            $old_grni_quantity = $this->input->post('grni_quantity');
            $old_received_quantity = $this->input->post('received_quantity');
            $old_returned_quantity = $this->input->post('returned_quantity');
            $edit_sor_id = $this->input->post('hidden_soitem_id');

            foreach ($sor_id as $key => $sor_edit_id) {
                $item = array(
                    'so_ret_id' => $id,
                    'category_id' => $old_cat_id[$key],
                    'item_id' => $old_item_id[$key],
                    'so_quantity' => $old_grni_quantity[$key],
                    'available_qty' => $old_received_quantity[$key],
                    'returned_qty' => $old_returned_quantity[$key],                    
                    'brn_id' => $user_data['branch_id'],
                    'updated_by' => $user_data['user_id'],
                    'updated_on' => date('Y-m-d H:i:s'),
                    'ip_address' => $_SERVER['REMOTE_ADDR']
                );
                // Update Warehouse Material
                $old_quantity = $this->sales_return_model->get_old_mat_quantity($old_cat_id[$key],$old_item_id[$key], $sor_edit_id);
                $ava_qty = $old_returned_quantity[$key] - $old_quantity;
                if ($ava_qty > 0) {
                     $this->product_model->update_branch_stock($user_data['branch_id'], $old_item_id[$key],$ava_qty, '-');                  
                } else {
                    $quantity = $this->sales_return_model->branch_materials($user_data['branch_id'], $old_item_id[$key])->row();
                    if ($quantity->available_qty > $ava_qty) {
                        $this->product_model->update_branch_stock($user_data['branch_id'], $old_item_id[$key],$ava_qty, '-');                
                    }
                }
                
                $this->sales_return_model->update_item($sor_edit_id, $item);
                
                 if (in_array($sor_id[$key], $edit_sor_id)) {
                    $item_id_index = array_search($sor_id[$key], $edit_sor_id);
                    unset($edit_sor_id[$item_id_index]);
                 }
            }
        }
        foreach ($edit_sor_id as $key => $item_id) {
            $this->sales_return_model->row_delete_item($id, $item_id);
        }
        // Insert New Item
        $cid = $this->input->post('new_category_id');
        $pid = $this->input->post('new_product_id');
        $grni_quantity = $this->input->post('new_grni_quantity');
        $received_quantity = $this->input->post('new_received_quantity');
        $returned_quantity = $this->input->post('new_returned_quantity');
        if (!empty($pid)) {
            foreach ($cid as $key => $cat) {
                $item = array(
                    'so_ret_id' => $id,
                    'category_id' => $cat,
                    'item_id' => $pid[$key],
                    'so_quantity' => $grni_quantity[$key],
                    'available_qty' => $received_quantity[$key],
                    'returned_qty' => $returned_quantity[$key],
                    'brn_id' => $user_data['branch_id'],
                    'created_by' => $user_data['user_id'],
                    'created_on' => date('Y-m-d H:i:s'),
                    'ip_address' => $_SERVER['REMOTE_ADDR']
                );
                $this->sales_return_model->insert_item($item);
                // Update Return Quantity in Warehouse                
                $this->product_model->update_branch_stock($user_data['branch_id'], $pid[$key], $returned_quantity[$key], '+');
            }
        }        
        
        if ($affected_rows > 0)
            $_SESSION['success'] = 'Purchase Return details updated successfully';
        else
            $_SESSION['error'] = 'Purchase Return details not updated!';
        redirect('invoice_return/edit/' . $id, 'refresh');
    }*/

    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }

        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['list_link'] = $actions['index'];
        $data['return'] = $this->sales_return_model->get_by_id($id);
        $data['return_items'] = $this->sales_return_model->get_items_by_sor_all($id);
        $this->render($data, 'sales_return/view');
    }

    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }
        $user_data = get_user_data();
        $return = $this->sales_return_model->get_by_id($id);
        $return_items = $this->sales_return_model->get_items_by_sor_all($id);
        $affected_rows = 1;
        
        foreach ($return_items as $key => $items) {
            // Update Return Quantity in Warehouse         
            $get_so_item = $this->sales_order_model->get_so_item($items->so_item_autoid);
            $product_id_txt = '';
            if($get_so_item->service_form_type == 2) {
                $get_pro_itemid = $this->services_model->get_service_item_byid($get_so_item->item_id);
                $product_id_txt = $get_pro_itemid->item_id;
            } else if($get_so_item->exc_form_type == 2) {
                $product_id_txt = $items->item_id;
            } else if($get_so_item->service_form_type == 0 && $get_so_item->exc_form_type == 0) {
                $product_id_txt = $items->item_id;
            }
            // Update Return Quantity in Warehouse
            if($product_id_txt != "") {
            $this->product_model->update_branch_stock($return->branch_id, $product_id_txt, $items->returned_qty, '-');
            }
            
            $sales_order_item = $this->sales_return_model->get_sales_order_item_by_id($items->so_item_autoid);
            if(count($sales_order_item)>0 && $items->returned_qty > 0) {
				$inv_update_qty = $sales_order_item[0]['quantity'];
                $inv_update_qty += ($sales_order_item[0]['is_returned'] != 1) ?  $items->returned_qty : 0;
                $qty_prft = $inv_update_qty * ($sales_order_item[0]['price'] - $sales_order_item[0]['unit_cost']);
                $total_amount = $inv_update_qty * $sales_order_item[0]['price'];
                $itemdata = array(
                    'quantity' => $inv_update_qty,
                    'total' => $total_amount,
                    'item_profit' => $qty_prft,
                    'status' => 1,
                    'updated_by' => $user_data['user_id'],
                    'updated_on' => date('Y-m-d H:i:s')
                );
				$this->sales_order_model->update_item($itemdata, $items->so_item_autoid);
            }
        }
		
        $all_sales_order_item = $this->sales_return_model->get_product_soiditem_sum($return->so_id);
		$sales_order_data = $this->sales_order_model->get_by_id($return->so_id);
        if(count($all_sales_order_item)>0) {
            $sub_total = (!empty($all_sales_order_item[0]['so_total'])) ? $all_sales_order_item[0]['so_total'] : 0;
            $discount_per = 0;$discount_amount = 0;$gst_amount = 0;$tot_amount=0;
            $discount = ($sales_order_data->discount_type==1) ? $sales_order_data->discount : 0;
            if($sub_total > 0) {
                $discount_per = ($sales_order_data->discount_type==1) ? $sales_order_data->discount : (($sales_order_data->discount / $sales_order_data->sub_total) * 100);
                $discount = ($sales_order_data->discount_type==1) ? $sales_order_data->discount : (($discount_per / 100) * $sub_total);
                $discount_amount = (($discount_per / 100) * $sub_total);
                $with_dis_total = $sub_total - $discount_amount;
                $gst_amount = (($sales_order_data->gst_percentage / 100) * $with_dis_total);
                $tot_amount = ($sales_order_data->gst_percentage==2) ? ($with_dis_total + $gst_amount) : $with_dis_total;
            }
            $update_inv = array(
                    'sub_total' => $sub_total,
                    'discount' => $discount,
                    'discount_amount' => $discount_amount,
                    'gst_amount' => $gst_amount,
                    'total_amt' => $tot_amount,
                    'updated_by' => $this->user_data['user_id'],
                    'updated_on' => date('Y-m-d H:i:s')
            );
            $affected_rows1 = $this->sales_order_model->update($return->so_id, $update_inv);
        }
        
        if ($affected_rows) {
            $pors = array(
                'status' => 10,
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $user_data['user_id']
            );

            $this->sales_return_model->update($id, $pors);
            $this->sales_return_model->update_item_by_so($id, $pors);
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $return->so_no, 1);
            $_SESSION['success'] = 'Invoice Returns deleted successfully!';
        } else {
            $_SESSION['error'] = 'Invoice Returns not deleted!';
        }
        redirect('invoice_return', 'refresh');
    }
}

?>