<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Location extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('location_model', '', TRUE);
        $this->load->library('upload');
    }
    
    public function index() {
        $data = array();
        $data['success']=$this->data['success'];  
        $data['error']=$this->data['error'];       
        $actions = $this->actions();
        $data['add_btn']    = 'Create Location';
        $data['view_link']  = $actions['view'] . '/';
        $data['add_link']   = $actions['add'];
        $data['edit_link']  = $actions['edit'] . '/';
        $data['delete_link']= $actions['delete'] . '/';		
        $data['location']      = $this->location_model->list_all();   			
        $this->render($data, 'location/list');
    }
    
    public function add() {
        $data = array();
        $data['success']=$this->data['success'];  
        $data['error']=$this->data['error'];        
        $actions = $this->actions();
        $data['form_action'] = $actions['insert'];
        $data['list_link']   =  $actions['index'];    
        $this->render($data, 'location/add');
    }
    
    public function insert() {
        //echo '<pre>';print_r($_REQUEST);print_r($_FILES);
        //exit;
	
        $insert = array(
            'location_name' => $this->input->post('location_name'),
            'city' => $this->input->post('city'),
            'address' => $this->input->post('address'),
            'phone_no' => $this->input->post('phone_no'),           
            'pincode' => $this->input->post('pincode'),
            'state' => $this->input->post('state'),
            'country' => $this->input->post('country'),
			'phone_no' => $this->input->post('phone_no'),
            'status' => $this->input->post('status'),            
            'branch_id' => $this->user_data['branch_id'],
            'created_by' => $this->user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s'),
        );	
     
        $last_id =$this->location_model->insert($insert);
		
        if ( $last_id > 0)            
            $_SESSION['success'] = 'Location added successfully';			
        else 
            $_SESSION['error'] = 'Location not added!';        
        redirect('location/add', 'refresh');
    }
    
    public function edit($id = ''){
        if (empty($id)) {
            show_400_error();
        }
        $data['success']=$this->data['success'];  
        $data['error']=$this->data['error'];        
        $data['location'] = $this->location_model->get_by_id($id);							
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' . $data['location']->id;    
			//echo "<pre>"; print_r($data['location']);die;
        $this->render($data, 'location/edit');
    }
    
    public function update($id = '') {    
			
        if (empty($id)) {
            show_400_error();
        }
        
        $update = array(
            'location_name' => $this->input->post('location_name'),
            'city' => $this->input->post('city'),
            'address' => $this->input->post('address'),
            'phone_no' => $this->input->post('phone_no'),           
            'pincode' => $this->input->post('pincode'),
            'state' => $this->input->post('state'),
            'country' => $this->input->post('country'),
			'phone_no' => $this->input->post('phone_no'),
            'status' => $this->input->post('status'),            
            'branch_id' => $this->user_data['branch_id'],
            'updated_by' => $this->user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s')
        );  
      
        if ($this->location_model->update($id, $update) > 0)                
            $_SESSION['success'] = 'Location updated successfully';
        else 
            $_SESSION['error'] = 'Location not updated!';
        redirect('location/edit/' . $id, 'refresh');
    }
    
    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }        
        $data = array();
        $data['location'] = $this->location_model->get_by_id($id);	
        $this->render($data, 'location/view');
    }
    
    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }        
        $location = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $this->user_data['user_id']
        );        
        if ($this->location_model->update($id, $location) > 0) 
            $_SESSION['success'] = 'Location deleted successfully';
        else 
            $_SESSION['error'] = 'Location not deleted!';        
        redirect('location', 'refresh');
    }
    
    public function check_location_exists(){
       $location = $_REQUEST['location_name'];
       echo $this->location_model->check_location_exists($location,$_REQUEST['location_id']);
    }
        

}
?>