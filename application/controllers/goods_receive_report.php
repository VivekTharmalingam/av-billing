<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Goods_Receive_Report extends REF_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model('purchase_receive_model', '', TRUE);
		$this->load->model('supplier_model', '', TRUE);
    }
	
    public function index() {
        $data = array();
		$data1 = '';
		$head = array();
        $data['success']=$this->data['success'];  
        $data['error']=$this->data['error'];       
        $actions = $this->actions();
        $data['view_link']  = base_url() . 'goods_receive/view' . '/';
		//$data['form_action'] = base_url() . 'supplier_payment_summary';
		
        $from = '';
        $to = '';
        $data['search_date'] = '';
			
        if($this->input->post('search_date') != ''){				
            $data['search_date'] = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : '';
            $search_date = explode('-', $this->input->post('search_date'));
            $from = trim($search_date[0]);
            $to = trim($search_date[1]);

            $from = explode('/',$from);           
            $from = $from[2].'-'.$from[1].'-'.$from[0];

            $to = explode('/',$to);
            $to = $to[2].'-'.$to[1].'-'.$to[0];
        }
		
        $data['from_date'] = $from;
        $data['to_date'] = $to;
		$data['branch_id'] = ($this->input->post('branch_id') != '') ? $this->input->post('branch_id') : $this->user_data['branch_id'];
		$data['gst_type'] = ($this->input->post('gst_type') != '') ? $this->input->post('gst_type') : '';
		$data['currency'] = ($this->input->post('currency') != '') ? $this->input->post('currency') : '';
		$data['branches'] = $this->company_model->active_user_branches();
		
		if($this->input->post('reset_btn')){
			$from = '';
			$to = '';
			$data['search_date']='';
			$data['branch_id'] = $this->user_data['branch_id'];
			$data['gst'] ='';
			$data['currency'] ='';
        }		
		$data['goods_receives'] = $this->purchase_receive_model->list_report($from, $to, $data['branch_id'], $data['gst_type'], $data['currency']);
		#echo '<pre>';print_r($data['goods_receives']);exit;
		if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Good Receive Report';
		    $head['setting'] = $this->company_model->get_company();
			$filename = 'Goods_Receive_Report_' . current_date();
				
			// Load Views
			$data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
			$data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
			$header = '<div></div>';
			$content = $this->load->view('goods_receive_report/pdf', $data, TRUE);			
			$footer = '<div></div>';
			$this->load->helper(array('My_Pdf'));
			
			create_pdf($header, $content, $footer, $filename);
			exit(0);
			
		} elseif(((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel')) ){
			$data['file_name'] = 'Goods_Receive_Report_' . current_date();
			$this->load->view('goods_receive_report/excel', $data);
			
		}else {
			$this->render($data, 'goods_receive_report/list');
		}
		
    }
	
    public function view($id = '',$from_date='',$to_date='') {
        if (empty($id)) {
            show_400_error();
        }        
        $data = array();
		$data['id'] = $id;
		$data['from_date'] = $from_date;
		$data['to_date'] = $to_date;
		$data['supplier']   = $this->purchase_order_model->get_supplier_by_id($id, $from_date, $to_date);
        $data['purchase']   = $this->purchase_order_model->get_purchase_by_id($id, $from_date, $to_date);		
		#echo "<pre>"; print_r($data);die;
        $this->render($data, 'supplier_payment_summary/view');
    }
	
	public function pdf($id = '',$from_date='',$to_date='') {
        if (empty($id)) {
            show_400_error();
        }        
        $data = array();
		$data['id'] = $id;
		$data['from_date'] = $from_date;
		$data['to_date'] = $to_date;
		$data['supplier']   = $this->purchase_order_model->get_supplier_by_id($id, $from_date, $to_date);
                $data['purchase']   = $this->purchase_order_model->get_purchase_by_id($id, $from_date, $to_date);	
			
		$head['title'] = 'Supplier Payment Report';
		$head['setting'] = $this->company_model->get_company();
		$filename = 'Supplier_Payment_Report_' . current_date();
		
		// Load Views
		$data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
		$data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
		$header = '<div></div>';
		$content = $this->load->view('supplier_payment_summary/view_pdf_report', $data, TRUE);			
		$footer = '<div></div>';//$this->load->view('templates/pdf/pdf_footer', '', TRUE);
		
		$this->load->helper(array('My_Pdf'));
		create_pdf($header, $content, $footer, $filename);
		exit(0);		
    }
	
}
?>