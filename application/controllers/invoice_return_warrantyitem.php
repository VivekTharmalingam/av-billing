<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Invoice_Return_Warrantyitem extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('sales_order_model', '', TRUE);
        $this->load->model('sales_return_model', '', TRUE);
        $this->load->model('category_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->model('customer_model', '', TRUE);
        $this->load->model('company_model', '', TRUE);
        $this->load->model('services_model', '', TRUE);
        $this->load->model('purchase_receive_model', '', TRUE);
        $this->load->model('purchase_order_model', '', TRUE);
        $this->load->library('Datatables');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        $data['returns'] = $this->sales_return_model->list_all();
        $this->render($data, 'warranty_item/list');
    }
    
    function datatable() {
        $this->datatables->select('sori.id AS srId, DATE_FORMAT(sor.return_date, "%d/%m/%Y") as ret_date, so.so_no AS inv, sor.sor_no as sor_no, soi.so_item_codetxt as pdt_code, sori.pdt_desc as pdt_desc, sor.so_id AS soId, sori.returned_qty as return_qty, sori.warranty_cleared as war_cleared, CASE sori.warranty_cleared WHEN 2 THEN "Cleared" WHEN 1 THEN "Processing" WHEN 0 THEN "Not Cleared" ELSE "" END as warranty_cleared_str', FALSE)
            ->where('sori.status != 10 AND sori.return_item_type=3 AND sor.status != 10 AND sor.branch_id IN ("' . $this->user_data['branch_id'] . '")')
            ->join(TBL_PDT . ' as mat', 'mat.id = sori.item_id', 'left')
            ->join(TBL_SO_ITM . ' as soi', 'soi.id = sori.so_item_autoid', 'left')
            ->join(TBL_SO_RET . ' as sor', 'sori.so_ret_id = sor.id AND sori.status != 10')
            ->join(TBL_SO . ' as so', 'so.id = sor.so_id', 'left')    
            ->join(TBL_BRN . ' as brn', 'brn.id = sor.branch_id', 'left')
            ->add_column('Actions', $this->get_buttons('$1','$2'), 'srId,war_cleared')
            ->from(TBL_SO_RET_ITM . ' AS sori');
        echo $this->datatables->generate();
    }

    function get_buttons($id, $wrnt) {
        $actions = $this->actions();
        $html = '<span class="actions">';        
        $html .='<a class="label btn btn-primary view" href="' . base_url() . 'invoice_return_warrantyitem/update_status/' . $id . '"><span><i class="fa fa-pencil"></i> Update Status</span></a>&nbsp';
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Warranty Clear!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Return Warranty Clear"><span>Delete</span></a>';
        }
        $html .='</br><a class="label btn btn-warning view" style="background: #009688;border-color: #009688; padding: 5px; display: inline-block;margin-top: 8px;" href="' . base_url() . 'invoice_return_warrantyitem/send_to_supplier/' . $id . '"><span><i class="fa fa-file-pdf-o"></i> Send To Supplier</span></a>&nbsp';
        $html.='</span>';
        return $html;
    }

    public function clear_warranty($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $war_clear_date = (!empty($this->input->post('war_clear_date'))) ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('war_clear_date')))) : '';
        $pors = array(
                'warranty_cleared' => 1,
                'warranty_cleared_date' => $war_clear_date,
                'warranty_cleared_remark' => $this->input->post('warrantyItem_txt'),
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $this->user_data['user_id']
        );
        if ($this->sales_return_model->update_item($id, $pors)) {
            $_SESSION['success'] = 'Warranty Cleared successfully!';
        } else {
            $_SESSION['error'] = 'Warranty not Cleared!';
        }
        
        redirect('invoice_return_warrantyitem', 'refresh');
    }
    
    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }

        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['list_link'] = $actions['index'];
        $data['warranty_items'] = $this->sales_return_model->get_warranty_return_item_byid($id);
        $this->render($data, 'warranty_item/view');
    }

    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }
        $temp = '';
        $pors = array(
                'warranty_cleared' => 0,
                'warranty_cleared_date' => $temp,
                'warranty_cleared_remark' => $temp,
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $this->user_data['user_id']
        );
        if ($this->sales_return_model->update_item($id, $pors)) {
            $this->sales_return_model->delete_status_log($id);
            $_SESSION['success'] = 'Warranty Cleared Data deleted successfully!';
        } else {
            $_SESSION['error'] = 'Warranty Cleared not deleted!';
        }
        redirect('invoice_return_warrantyitem', 'refresh');
    }
    
    public function send_to_supplier($id) {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['suppliers'] = $this->purchase_order_model->get_all_suppliers();
        $data['warranty_items'] = $this->sales_return_model->get_warranty_return_item_byid($id);
        $data['form_action'] = base_url() . 'invoice_return_warrantyitem/update_supplier_warranty/'.$id;
        
        $this->render($data, 'warranty_item/supplier_warranty');
        
    }
    
    public function update_supplier_warranty($id) {
        
        $user_data = get_user_data();
        
        $warranty_supplier_date = (!empty($this->input->post('warranty_supplier_date'))) ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('warranty_supplier_date')))) : '';
        $pors = array(
                'warranty_supplier_id' => $this->input->post('warranty_supplier_id'),
                'warranty_supplier_date' => $warranty_supplier_date,
                'warranty_item_sl_no' => $this->input->post('warranty_item_sl_no'),
                'warranty_limitations' => $this->input->post('warranty_limitations')
        );
        $this->sales_return_model->update_item($id, $pors);
        
        redirect('invoice_return_warrantyitem/print_pdf/' . $id, 'refresh');
    }
    
    public function print_pdf($id) {
        $data = array();
        $actions = $this->actions();
        $data['browser'] = browser_info();
        $data['list_link'] = $actions['index'];
        $data['redirect_link'] = $actions['index'];

        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['warranty_link'] = base_url() . 'invoice_return_warrantyitem/pdf/' . $id;
        $data['war_itm_id'] = $id;
        
        $this->render($data, 'warranty_item/print');
    }
    
    public function update_status($id) {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['warranty_status_log'] = $this->sales_return_model->warranty_status_log_details($id);
        $data['warranty_items'] = $this->sales_return_model->get_warranty_return_item_byid($id);
        $data['form_action'] = base_url() . 'invoice_return_warrantyitem/update_status_log/'.$id;
        
        $this->render($data, 'warranty_item/status_log');
        
    }
    
    public function update_status_log($id) {
        
        $user_data = get_user_data();
        $updat_txt = 0;
        
        $warrantydata = array(            
            'warranty_id' => $id,
            'warranty_status_txt' => $this->input->post('warranty_status_txt'),
            'log_description' => $this->input->post('log_description'),
            'status' => 1,
            'branch_id' => $user_data['branch_id'],
            'created_by' => $user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s')
        );
        $inrst_id = $this->sales_return_model->insert_status_log($warrantydata);
        
        $war_clear_date = ($this->input->post('warranty_status')=='2') ? date('Y-m-d') : '';
        $warrantyItem_txt = ($this->input->post('warranty_status')=='2') ? $this->input->post('log_description') : '';
        $pors = array(
                'warranty_cleared' => $this->input->post('warranty_status'),
                'warranty_cleared_date' => $war_clear_date,
                'warranty_cleared_remark' => $warrantyItem_txt
        );
        $this->sales_return_model->update_item($id, $pors);
        
        if (!empty($inrst_id)){
            $_SESSION['success'] = 'Warranty Status Updated successfully';
        }else{
            $_SESSION['error'] = 'Warranty Status not Updated!';
        }
        
        redirect('invoice_return_warrantyitem', 'refresh');
    }
    
    public function pdf($id) {
        $data = array();
        $user_data = get_user_data();
        
        $data['warranty_items'] = $this->sales_return_model->get_warranty_return_item_byid($id);
        $data['supplier'] = $this->purchase_order_model->supplier_address_by_id($data['warranty_items'][0]['warranty_supplier_id']);
//        print_r($data['supplier']);
//        exit();
        $data['user_data'] = get_user_data();
        $data['branch'] = $this->company_model->branch_by_id($data['warranty_items'][0]['brn_id']);
        $data['branches'] = $this->company_model->branch_list_active();
        $data['company'] = $this->company_model->get_company();
        $filename = 'PDF_WRN' . $id;
        $filename = strtoupper($filename);
        // Load Views
        $header = $footer = $this->load->view('warranty_item/pdf_header', $data, TRUE);;
        $content = $this->load->view('warranty_item/pdf', $data, TRUE);
        $footer = $this->load->view('warranty_item/pdf_footer', $data, TRUE);
//echo $header.$content.$footer;
        $this->load->helper(array('My_Pdf'));
        delivery_order_pdf($header, $content, $footer, $filename);
        exit(0);
    }

}

?>