<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Service_Reports extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('services_model', '', TRUE);
        $this->load->library('upload');
    }

    public function index($unpaid_sts = '') {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = base_url() . 'services/view' . '/';
        $data['form_action'] = base_url() . 'service_reports';
		$service_from = (!empty($this->input->post('service_from'))) ? $this->input->post('service_from') : '';
		$branch = (!empty($this->input->post('branch'))) ? $this->input->post('branch') : '';
		$status = (!empty($this->input->post('status'))) ? $this->input->post('status') : '';
		$customer = (!empty($this->input->post('customer'))) ? $this->input->post('customer') : '';
		$engineer = (!empty($this->input->post('engineer'))) ? $this->input->post('engineer') : '';
		$search_date = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : date('01/m/Y') . ' - ' . date('t/m/Y');
		if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'Reset')) {
			$branch = '';
			$status = '';
			$customer = '';
			$engineer = '';
			$search_date = date('01/m/Y') . ' - ' . date('t/m/Y');
		}
		
		if((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'Search')) {
			$search_date = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : '';
		}
		
		if($service_from == 'home') {
			$search_date = '';
		}
		
		$search_date_arr = explode('-', $search_date);
		$from = convert_text_date(trim($search_date_arr[0]));
		$to = convert_text_date(trim($search_date_arr[1]));
        $data['search_date'] = $search_date;
        $data['from_date'] = $from;
        $data['to_date'] = $to;
        $data['branch_id'] = $branch;
        $data['type'] = $status;
        $data['customer_id'] = $customer;
        $data['engineer_id'] = $engineer;
		
        $data['users'] = $this->user_model->active_user_list();
        $data['customer_list'] = $this->services_model->customer_list();
        $data['branches'] = $this->services_model->list_branche();
        $data['service_report'] = $this->services_model->get_services_report($from, $to, $customer, $engineer, $branch, $status, $service_from);
		
        $data['permission'] = $this->permission;
        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Service Reports';
            $head['setting'] = $this->company_model->get_company();
            $filename = 'Service_Report_' . current_date();
            // Load Views
            $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
            $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
            $header = '<div></div>';
            $content = $this->load->view('service_report/pdf_report', $data, TRUE);
            $footer = '<div></div>';//$this->load->view('templates/pdf/pdf_footer', '', TRUE);
            $this->load->helper(array('My_Pdf'));
            create_pdf($header, $content, $footer, $filename);
            exit(0);
        }
		elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $this->load->view('service_report/excel_report', $data);
        }
		else {
            $this->render($data, 'service_report/list');
        }
    }


}

?>