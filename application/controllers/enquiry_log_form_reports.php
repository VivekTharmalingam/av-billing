<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Enquiry_log_form_Reports extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('enquiry_log_form_model', '', TRUE);
        //$this->load->library('upload');
    }

    public function index($unpaid_sts = '') {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = base_url() . 'enquiry_log_form/view' . '/';
        $data['pdf_link'] = base_url() . 'enquiry_log_form/log_pdf' . '/';
        $data['form_action'] = base_url() . 'enquiry_log_form_reports';
		
                $branch = (!empty($this->input->post('branch'))) ? $this->input->post('branch') : '';
		$assigned_to = (!empty($this->input->post('assigned_to'))) ? $this->input->post('assigned_to') : '';
                $enq_status = (!empty($this->input->post('enq_status'))) ? $this->input->post('enq_status') : '';
		$type_sts = (!empty($this->input->post('type'))) ? $this->input->post('type') : '';
		$search_date = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : date('01/m/Y') . ' - ' . date('t/m/Y');
		if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'Reset')) {
                        $branch = '';
			$assigned_to = '';
                        $enq_status = '';
			$type_sts = '';
			$search_date = date('01/m/Y') . ' - ' . date('t/m/Y');
		}
                if((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'Search')) {
                    $search_date = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : '';
                }
		
		$search_date_arr = explode('-', $search_date);
		$from = convert_text_date(trim($search_date_arr[0]));
		$to = convert_text_date(trim($search_date_arr[1]));
        $data['search_date'] = $search_date;
        $data['from_date'] = $from;
        $data['to_date'] = $to;
        $data['branch_id'] = $branch;
        $data['assign_to'] = $assigned_to;
        $data['enq_sts'] = $enq_status;
        if($enq_status == 1) {
            $data['enq_sts_str'] = 'Success';
        } else if($enq_status == 2) {
            $data['enq_sts_str'] = 'Process';
        } else {
            $data['enq_sts_str'] = 'Cancel';
        }
        $data['type'] = $type_sts; 
        if($type_sts == 1) {
            $data['type_str'] = 'Sales';
        } else if($type_sts == 2) {
            $data['type_str'] = 'Purchase';
        } else if($type_sts == 3) { 
            $data['type_str'] = 'Accounts';
        } else {
            $data['type_str'] = 'Promotions';
        }

        $data['users'] = $this->user_model->active_user_list();
        $data['branches'] = $this->enquiry_log_form_model->list_branche();
        $data['enq_log_report'] = $this->enquiry_log_form_model->list_enq_log_summary($from, $to, $branch, $assigned_to, $enq_status, $type_sts);
        $data['permission'] = $this->permission;
        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Enquiry Log Form Reports';
            $head['setting'] = $this->company_model->get_company();
            $filename = 'Enquiry_log_form_Report_' . current_date();
            // Load Views
            $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
            $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
            $header = '<div></div>';
            $content = $this->load->view('enquiry_log_form_report/pdf_report', $data, TRUE);
            $footer = '<div></div>';//$this->load->view('templates/pdf/pdf_footer', '', TRUE);
            $this->load->helper(array('My_Pdf'));
            create_pdf($header, $content, $footer, $filename);
            exit(0);
        }
		elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $this->load->view('enquiry_log_form_report/excel_report', $data);
        }
		else {
            $this->render($data, 'enquiry_log_form_report/list');
        }
    }


}

?>