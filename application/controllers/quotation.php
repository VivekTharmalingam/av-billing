<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quotation extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('quotation_model', '', TRUE);
        $this->load->model('company_model', '', TRUE);
        $this->load->model('category_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->model('customer_model', '', TRUE);
        $this->load->model('payment_type_model', '', TRUE);
        $this->load->model('sales_payment_model', '', TRUE);
        $this->load->model('sales_order_model', '', TRUE);
        $this->load->model('delivery_order_model', '', TRUE);
        $this->load->library('Datatables');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        $data['print_link'] = base_url() . 'quotation/pdf/';
        $data['to_invoice'] = base_url() . 'invoice/add/';
        $data['send_mail_link'] = base_url() . 'quotation/send_mail/';
        $data['quotation'] = $this->quotation_model->list_all();
        //print_r($data['quotation']); exit;
        $this->render($data, 'quotation/list');
    }

    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('quo.id as quoId, quo.quo_no, DATE_FORMAT(quo.so_date, "%d/%m/%Y") as soDate, CASE quo.quo_type WHEN 1 THEN c.name ELSE quo.customer_name END as name, quo.your_reference, quo.total_amt', FALSE)
                ->where('quo.status != 10 AND quo.branch_id IN ("' . $this->user_data['branch_id'] . '")')
                ->order_by('quo.id', 'desc')
                ->join(TBL_CUS . ' as c', 'c.id = quo.customer_id', 'left')
                ->add_column('Actions', $this->get_buttons('$1'), 'quoId')
                ->from(TBL_QUO . ' AS quo');
        echo $this->datatables->generate();
    }

    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(6, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . base_url() . 'quotation/pdf/' . $id . '" title="Print Invoice Details"><span><i class="fa fa-file-pdf-o" aria-hidden="true"></i></span></a>&nbsp';
        }

        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(3, $this->permission)) {
            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
        }
        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger delete delete-confirm" href="#" data-confirm-content="You will not be able to recover invoice details!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="invoice "><span>Delete</span></a>&nbsp';
        }
        if (in_array(8, $this->permission)) {
            $html .='<a class="label btn btn-success delete-confirm" href="#" title="Convert to Invoice" data-confirm-content="Do you want to convert the quotation to invoice!" data-redirect-url="' . base_url() . 'invoice/add/' . $id . '" data-bindtext="quotation" data-action="INVOICE CONVERSION" style="display: inline-block; margin-top: 5px;"><span style="line-height: 14px;">To Invoice</span></a>&nbsp';
        }
        if (in_array(9, $this->permission)) {
            $html .='<a class="label btn btn-primary view" style="background: #009688; border-color: #009688;" data-href="' . base_url() . 'quotation/send_mail/' . $id . '" id="smart-mod-eg2" title="Send Email" data-id="' . $id . '"><span><i class="fa fa-send" aria-hidden="true"></i> Send Email</span></a>&nbsp';
        }
        $html.='</span>';
        return $html;
    }

    public function add() {
        $data = array();
        $actions = $this->actions();
        $user_data = get_user_data();
        $data['list_link'] = $actions['index'] . '/';
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['form_action'] = $actions['insert'];
        $data['categories'] = $this->category_model->list_active();
        $data['items'] = $this->product_model->list_active();
        $data['customers'] = $this->customer_model->list_active();
        $data['address'] = $this->quotation_model->get_current_address($user_data['branch_id']);
        $data['company'] = $this->company_model->get_company();
        $data['payment_types'] = $this->payment_type_model->list_active();
        $brn_slug = $this->company_model->list_active_by_company_id($this->user_data['branch_id']);

        $inv = strtoupper(get_var_name($brn_slug[0]->branch_name . '_Q'));
        //echo'tr'.TBL_QUO;
        $data['so_no'] = $this->auto_generation_code(TBL_QUO, $inv . ' ', '', 5, '', $this->user_data['branch_id']);
        $data['list_item_link'] = base_url() . 'item/';
        $data['add_item_link'] = base_url() . 'item/add/';
        $data['list_customer_link'] = base_url() . 'customer/';
        $data['add_customer_link'] = base_url() . 'customer/add/';
        $data['payment_terms'] = payment_terms();

        $this->render($data, 'quotation/add');
    }

    public function shipping_address_by_customer() {
        $data = array();
        $data['addresses'] = $this->customer_model->get_address_by_id($this->input->post('customer_id'));
        $data['customer'] = $this->customer_model->get_by_id($this->input->post('customer_id'));
        $this->ajax_response($data);
    }

    public function get_product_by_category() {
        $data = array();
        $data['products'] = $this->product_model->get_product_by_category($this->input->post('category_id'));
        $this->ajax_response($data);
    }

    public function get_products_by_so() {
        $so_id = $_REQUEST['so_id'];
        $data = array();
        if (!empty($so_id)) {
            $data['products'] = $this->purchase_receive_model->get_products_by_so($so_id);
            $data['so_detail'] = $this->purchase_receive_model->get_supplier_by_so($so_id);
        }
        echo json_encode($data);
    }

    public function search_item() {
        $product_code = $this->input->post('product_code');
        $data = array();
        $data = $this->product_model->search_item();
        foreach ($data as $key => $value) {
            //$value->value = $value->id;
            if (!empty($product_code))
                $value->label = $value->pdt_code;
            else
                $value->label = $value->pdt_code . ' / ' . $value->pdt_name;

            $value->value = $value->label;
        }
        $this->ajax_response($data);
    }

    public function search_item_description() {
        $data = array();
        $data = $this->product_model->search_item_by_brand();
        foreach ($data as $key => $value) {
            #$value->label = (!empty($value->cat_name)) ? $value->cat_name . ' - ' : '';
            $value->label = (!empty($value->cat_name)) ? $value->cat_name . ' ' : '';
            $value->label .= $value->pdt_name;
            $value->value = $value->label;
        }
        $this->ajax_response($data);
    }

    public function active_customers() {
        $data = array();
        $data = $this->customer_model->list_active();
        /* foreach ($data as $key => $value) {
          $value->id = $value->id;
          $value->text = $value->name;
          } */
        $this->ajax_response($data);
    }

    public function convert_to_invoice($quotation_id = '') {
        if (empty($quotation_id)) {
            show_400_error();
        }

        $quotation = $this->quotation_model->get_by_id($quotation_id);
        if ($quotation->inv_created) {
            $_SESSION['error'] = 'Invoice already created for the quotation(' . $quotation->quo_no . '). Please edit quotation details and try again!';
            redirect('quotation', 'refresh');
        }

        $quotation_items = $this->quotation_model->get_products_by_so($quotation_id);
        $brn_slug = $this->company_model->list_active_by_company_id($this->user_data['branch_id']);
        $inv = trim(strtoupper(get_var_name($brn_slug[0]->branch_name)));
        $so_no = $this->auto_generation_code(TBL_QUO, $inv . ' ', '', 6, '', $this->user_data['branch_id']);

        if ($quotation->gst == '1') {
            $gst_amount = $this->calculate_inclusive_gst($quotation->total_amt);
        } else {
            $gst_amount = $quotation->gst_amount;
        }

        $customer_name = '';
        $customer = $this->customer_model->get_by_id($quotation->customer_id);
        if (!empty($customer)) {
            $customer_name = $customer->name;
        }

        $insert = array(
            'so_no' => $so_no,
            'quotation_id' => $quotation_id,
            'so_date' => date('Y-m-d', strtotime(str_replace('/', '-', $quotation->so_date))),
            'leave_date' => (!empty($quotation->leave_date)) ? 1 : 0,
            'so_type' => $quotation->quo_type,
            'customer_id' => $quotation->customer_id,
            'customer_name' => $customer_name,
            'shipping_address' => $quotation->shipping_address,
            'remarks' => $quotation->remarks,
            'status' => 1,
            'sub_total' => $quotation->sub_total,
            'discount' => $quotation->discount,
            'discount_type' => $quotation->discount_type,
            'discount_amount' => $quotation->discount_amount,
            'gst' => $quotation->gst,
            'gst_amount' => $quotation->gst_amount,
            'gst_percentage' => $quotation->gst_percentage,
            'total_amt' => $quotation->total_amt,
            'payment_terms' => $quotation->payment_terms,
            'your_reference' => $quotation->your_reference,
            'branch_id' => $this->user_data['branch_id'],
            'created_by' => $this->user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s')
        );

        $last_id = $this->sales_order_model->insert($insert);
        if ($last_id > 0) {
            $_SESSION['success'] = 'Invoice added successfully';
            foreach ($quotation_items as $key => $item) {
                $profit = ($item->quantity * $item->price) - ($item->quantity * $item->unit_cost);
                $insert = array(
                    'so_id' => $last_id,
                    'category_id' => $item->category_id,
                    'item_id' => $item->item_id,
                    'product_description' => $item->product_description,
                    'price' => $item->price,
                    'unit_cost' => $item->unit_cost,
                    'quantity' => $item->quantity,
                    'total' => $item->total,
                    'item_profit' => $profit,
                    'status' => 1,
                    'branch_id' => $this->user_data['branch_id'],
                    'created_by' => $this->user_data['user_id'],
                    'created_on' => date('Y-m-d H:i:s')
                );
                if ($this->sales_order_model->insert_item($insert)) {
                    $this->product_model->update_branch_stock($this->user_data['branch_id'], $item->item_id, $item->quantity);
                }
                //Insert Item Cost Log
                $this->update_price_log($item->item_id, $item->unit_cost, $item->price, 'From Quotation to Invoice');
            }

            $update = array(
                'inv_created' => 1
            );
            $this->quotation_model->update($quotation_id, $update);
            redirect('invoice/edit/' . $last_id, 'refresh');
        } else {
            $_SESSION['error'] = 'Invoice not added!';
            redirect('quotation', 'refresh');
        }
    }

    public function update_price_log($pid, $bprice, $sprice, $tit) {
        $last = $this->product_model->get_product_last_price_log($pid);
        $last_id = '';
        if (($last->buying_price != $bprice) || ($last->selling_price != $sprice)) {
            $insert = array(
                'product_id' => $pid,
                'buying_price' => $bprice,
                'selling_price' => $sprice,
                'updated_from' => $tit,
                'brn_id' => $this->user_data['branch_id'],
                'created_by' => $this->user_data['user_id'],
                'created_on' => date('Y-m-d H:i:s'),
                'ip_address' => $_SERVER['REMOTE_ADDR']
            );
            $last_id = $this->product_model->insert_cost_log($insert);
        }
        return $last_id;
    }

    public function insert() {
        $user_data = get_user_data();
        $brn_slug = $this->company_model->list_active_by_company_id($user_data['branch_id']);
        $inv = strtoupper(get_var_name($brn_slug[0]->branch_name . '_Q'));
        $so_no = $this->auto_generation_code(TBL_QUO, $inv . ' ', '', 5, '', $this->user_data['branch_id']);

        //$so_no = $this->auto_generation_code(TBL_SO, CODE_SO, '', 3, '');
        if ($this->input->post('gst_type') == '1') {
            $gst_amount = $this->calculate_inclusive_gst($this->input->post('total_amt'));
        } else {
            $gst_amount = $this->input->post('gst_amount');
        }

        $customer_id = $this->input->post('customer_name');

        $customer_name = '';
        if (($customer_id == 'cash') && ($this->input->post('cash_customer_name') != ''))
            $customer_name = $this->input->post('cash_customer_name');

        else if ($customer_id == 'cash')
            $customer_name = 'Cash Customer';

        $customer_name = ($customer_id == 'cash') ? $this->input->post('cash_customer_name') : '';

        $insert = array(
            'quo_no' => $so_no,
            'so_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('so_date')))),
            //'leave_date' => (!empty($this->input->post('leave_date'))) ? 1 : 0,
            'quo_type' => ($this->input->post('customer_name') == 'cash') ? 2 : 1,
            'customer_name' => $customer_name,
            //'remarks' => $this->input->post('remarks'),
            'status' => 1,
            'sub_total' => $this->input->post('sub_total'),
            'discount' => $this->input->post('discount'),
            'discount_type' => $this->input->post('discount_type'),
            'discount_amount' => $this->input->post('discount_amt'),
            //'gst' => $this->input->post('gst_type'),
            //'gst_amount' => $gst_amount,
            //'gst_percentage' => $this->input->post('hidden_gst'),
            'total_amt' => $this->input->post('total_amt'),
            //'payment_terms' => $this->input->post('payment_terms'),
            'your_reference' => $this->input->post('your_reference'),
            'branch_id' => $user_data['branch_id'],
            'created_by' => $user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s')
        );


        if ($customer_id != 'cash') {
            $insert['customer_id'] = $customer_id;
            $ship_to = $this->input->post('ship_to');
            $location_name = $this->input->post('location_name');
            $address = $this->input->post('address');

            if ($ship_to != "") {
                $insert['shipping_address'] = $this->input->post('ship_to');
            } else if (($ship_to == "") && ($location_name != '')) {
                $shipping_address = array(
                    'customer_id' => $customer_id,
                    'location_name' => $location_name,
                    'address' => $address,
                    'is_default' => 1,
                    'status' => 1,
                    'branch_id' => $user_data['branch_id'],
                    'created_by' => $user_data['user_id'],
                    'created_on' => date('Y-m-d H:i:s')
                );

                $insert['shipping_address'] = $this->customer_model->insert_ship_address($shipping_address);
                if ($insert['shipping_address'] > 0) {
                    $update_ship = array(
                        'is_default' => 0,
                        'branch_id' => $user_data['branch_id'],
                        'updated_by' => $user_data['user_id'],
                        'updated_on' => date('Y-m-d H:i:s')
                    );

                    $this->customer_model->reset_customer_default_address($customer_id, $insert['shipping_address'], $update_ship);
                }
            } else {
                //$insert['shipping_address'] = NULL;
            }
        }

        $last_id = $this->quotation_model->insert($insert);

        if ($last_id > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, $so_no, '');
            $_SESSION['success'] = 'Quotation added successfully';
            $category_id = $this->input->post('category_id');
            $product_id = $this->input->post('product_id');
            $quantity = $this->input->post('quantity');
            $price = $this->input->post('price');
            $unit_cost = $this->input->post('cost');
            $discount = $this->input->post('discount');
            $discount_type = $this->input->post('discount_type');
            $discount_amt = $this->input->post('discount_amt');
            $amount = $this->input->post('amount');
            $product_code = $this->input->post('product_code');
            $product_description = $this->input->post('product_description');

            foreach ($category_id as $key => $category) {
                if (!empty($product_id[$key])) {
                    //$cur_item_cost = $this->product_model->get_product_last_price_log($product_id[$key]);
                    $profit = ($quantity[$key] * $price[$key]) - ($quantity[$key] * $unit_cost[$key]);
                    $item = array(
                        'quo_id' => $last_id,
                        'category_id' => $category,
                        'item_id' => $product_id[$key],
                        'product_description' => $product_description[$key],
                        'price' => $price[$key],
                        'unit_cost' => $unit_cost[$key],
                        'quantity' => $quantity[$key],
                        'total' => $amount[$key],
                        'item_profit' => $profit,
                        'status' => 1,
                        'branch_id' => $user_data['branch_id'],
                        'created_by' => $user_data['user_id'],
                        'created_on' => date('Y-m-d H:i:s')
                    );

                    if ($this->quotation_model->insert_item($item)) {
                        //$this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $quantity[$key]);
                    }

                    //Insert Item Cost Log
                    // $this->update_price_log($product_id[$key], $unit_cost[$key], $price[$key], 'From Invoice');
                }
            }

            redirect('quotation/print_pdf/' . $last_id, 'refresh');
        } else {
            $_SESSION['error'] = 'Quotation not added!';
            redirect('quotation/add', 'refresh');
        }
    }

    public function print_pdf($id, $from_list = '') {
        $data = array();
        $actions = $this->actions();
        $data['browser'] = browser_info();
        $data['list_link'] = $actions['index'];
        if (!empty($from_list))
            $data['redirect_link'] = $actions['index'];
        else
            $data['redirect_link'] = $actions['add'];

        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['invoice_link'] = base_url() . 'quotation/pdf/' . $id;
        $data['do_link'] = base_url() . 'quotation/invoice_to_delivery_order/' . $id;
        $data['so_id'] = $id;
        //$data['thermal'] = $this->thermal($id);
        //$data['thermal_src'] = base_url() . 'quotation/thermal/' . $id;
        $this->render($data, 'quotation/print');
    }

    public function pdf($id) {
        $data = array();
        $user_data = get_user_data();
        $data['sales_order'] = $this->quotation_model->get_by_id($id);
        $data['so_items'] = $this->quotation_model->get_products_by_so($id);
        $data['address'] = $this->quotation_model->get_current_address($user_data['branch_id']);
        $data['amt_word'] = convert_amount_to_word_rupees($data['sales_order']->total_amt);
        $data['shipping_address'] = $this->quotation_model->shipping_address_by_id($data['sales_order']->shipping_address);
        $data['user_data'] = get_user_data();
        $data['branch'] = $this->company_model->branch_by_id($data['sales_order']->branch_id);
        $data['branches'] = $this->company_model->active_branch_first();
        $data['company'] = $this->company_model->get_company();
        $filename = 'Quotation_' . $data['sales_order']->quo_no;
        $filename = strtoupper($filename);

        // Load Views
        $header = $this->load->view('quotation/pdf_header', $data, TRUE); //'<div></div>';
        $content = $this->load->view('quotation/pdf', $data, TRUE);
        $footer = ''; //$this->load->view('quotation/pdf_footer', $data, TRUE);
        #echo $header.$content.$footer;exit;
        $this->load->helper(array('My_Pdf'));
        invoice_pdf($header, $content, $footer, $filename);
        exit(0);
    }

    public function edit($id = '') {
        if (empty($id)) {
            show_400_error();
        }

        $data = array();
        $actions = $this->actions();
        $user_data = get_user_data();
        $data['form_action'] = $actions['update'];
        $data['list_link'] = $actions['index'];
        $data['sales_order'] = $this->quotation_model->get_by_id($id);
        $data['sales_order_items'] = $this->quotation_model->get_products_by_so($id);
        $data['payment'] = $this->sales_payment_model->get_by_so_id($id);
        $data['shipping_address'] = $this->quotation_model->shipping_address_by_id($data['sales_order']->shipping_address);
        $data['addresses'] = $this->customer_model->get_address_by_id($data['sales_order']->customer_id);
        $data['customer'] = $this->customer_model->get_by_id($data['sales_order']->customer_id);
        $data['address'] = $this->quotation_model->get_current_address($user_data['branch_id']);
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['categories'] = $this->category_model->list_active();
        $data['items'] = $this->product_model->list_active();
        $data['customers'] = $this->customer_model->list_active();
        $data['company'] = $this->company_model->get_company();
        $data['payment_types'] = $this->payment_type_model->list_active();
        #$data['payment'] = $this->payment_type_model->get_by_id($data['payment']->pay_pay_type);
        $data['payment_terms'] = payment_terms();

        $data['list_item_link'] = base_url() . 'item/';
        $data['add_item_link'] = base_url() . 'item/add/';
        $data['list_customer_link'] = base_url() . 'customer/';
        $data['add_customer_link'] = base_url() . 'customer/add/';
        $this->render($data, 'quotation/edit');
    }

    public function update() {
        $id = $this->input->post('so_id');
        $user_data = get_user_data();
        $gst_type = (!empty($this->input->post('gst_type'))) ? 1 : 0;
        $customer_id = $this->input->post('customer_name');
        $customer_name = '';
        if (($customer_id == 'cash') && ($this->input->post('cash_customer_name') != ''))
            $customer_name = $this->input->post('cash_customer_name');
        else if ($customer_id == 'cash')
            $customer_name = 'Cash Customer';

        if ($this->input->post('gst_type') == '1') {
            $gst_amount = $this->calculate_inclusive_gst($this->input->post('total_amt'));
        } else {
            $gst_amount = $this->input->post('gst_amount');
        }

        $update = array(
            'so_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('so_date')))),
            //'leave_date' => (!empty($this->input->post('leave_date'))) ? 1 : 0,
            'quo_type' => ($this->input->post('customer_name') == 'cash') ? 2 : 1,
            'customer_id' => ($customer_id != 'cash') ? $customer_id : NULL,
            'customer_name' => $customer_name,
            //'remarks' => $this->input->post('remarks'),
            /* 'status' => 1, */
            'inv_created' => 0,
            'sub_total' => $this->input->post('sub_total'),
            'discount' => $this->input->post('discount'),
            'discount_type' => $this->input->post('discount_type'),
            'discount_amount' => $this->input->post('discount_amt'),
            //'gst' => $this->input->post('gst_type'),
            //'gst_amount' => $gst_amount,
            //'gst_percentage' => $this->input->post('hidden_gst'),
            'total_amt' => $this->input->post('total_amt'),
            //'payment_terms' => $this->input->post('payment_terms'),
            'your_reference' => $this->input->post('your_reference'),
            'branch_id' => $user_data['branch_id'],
            'updated_by' => $user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s')
        );

        if (!empty($this->input->post('do_date'))) {
            $update['do_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('do_date'))));
        }

        /* To update shipping address start */
        if ($customer_id != 'cash') {
            $ship_to = $this->input->post('ship_to');
            $location_name = $this->input->post('location_name');
            $address = $this->input->post('address');
            if ($ship_to != "") {
                $update['shipping_address'] = $this->input->post('ship_to');
            } else if (($ship_to == "") && ($location_name != '')) {
                $shipping_address = array(
                    'customer_id' => $customer_id,
                    'location_name' => $location_name,
                    'address' => $address,
                    'is_default' => 1,
                    'status' => 1,
                    'branch_id' => $user_data['branch_id'],
                    'created_by' => $user_data['user_id'],
                    'created_on' => date('Y-m-d H:i:s')
                );

                $update['shipping_address'] = $this->customer_model->insert_ship_address($shipping_address);
                if ($update['shipping_address'] > 0) {
                    $update_ship = array(
                        'is_default' => 0,
                        'branch_id' => $user_data['branch_id'],
                        'updated_by' => $user_data['user_id'],
                        'updated_on' => date('Y-m-d H:i:s')
                    );

                    $this->customer_model->reset_customer_default_address($customer_id, $update['shipping_address'], $update_ship);
                }
            } else {
                $update['shipping_address'] = NULL;
            }
        }
        /* To update shipping address end */

        $affected_rows = $this->quotation_model->update($id, $update);
        if ($affected_rows > 0) {
            $data_sales_order = $this->quotation_model->get_by_id($id);
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $data_sales_order->quo_no, '');
            $_SESSION['success'] = 'Quotation updated successfully';

            // Update existing product start
            $hidden_soitem_id = $this->input->post('hidden_soitem_id');
            $category_id = $this->input->post('category_id');
            $product_id = $this->input->post('product_id');
            $product_code = $this->input->post('product_code');
            $product_description = $this->input->post('product_description');
            $quantity = $this->input->post('quantity');
            $old_quantity = $this->input->post('old_quantity');
            $price = $this->input->post('price');
            $unit_cost = $this->input->post('cost');
            $discount = $this->input->post('discount');
            $discount_type = $this->input->post('discount_type');
            $discount_amt = $this->input->post('discount_amt');
            $amount = $this->input->post('amount');

            foreach ($hidden_soitem_id as $key => $so_item_id) {
                // Old sales order item
                $old_so_item = $this->quotation_model->get_so_item($so_item_id);

                if ($old_so_item->item_id == $product_id[$key]) {
                    $profit = ($quantity[$key] * $price[$key]) - ($quantity[$key] * $unit_cost[$key]);
                    $item = array(
                        'category_id' => $category_id[$key],
                        'product_description' => $product_description[$key],
                        'price' => $price[$key],
                        'unit_cost' => $unit_cost[$key],
                        'quantity' => $quantity[$key],
                        'total' => $amount[$key],
                        'item_profit' => $profit,
                        'status' => 1,
                        'branch_id' => $user_data['branch_id'],
                        'updated_by' => $user_data['user_id'],
                        'updated_on' => date('Y-m-d H:i:s')
                    );

                    if ($this->quotation_model->update_item($item, $so_item_id)) {
                        $update_quantity = $quantity[$key] - $old_so_item->quantity;
                        //$this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $update_quantity, '-');
                    }
                } else {
                    $profit = ($quantity[$key] * $price[$key]) - ($quantity[$key] * $unit_cost[$key]);
                    $item = array(
                        'quo_id' => $id,
                        'category_id' => $category_id[$key],
                        'item_id' => $product_id[$key],
                        'product_description' => $product_description[$key],
                        'price' => $price[$key],
                        'unit_cost' => $unit_cost[$key],
                        'quantity' => $quantity[$key],
                        'total' => $amount[$key],
                        'item_profit' => $profit,
                        'status' => 1,
                        'branch_id' => $user_data['branch_id'],
                        'updated_by' => $user_data['user_id'],
                        'updated_on' => date('Y-m-d H:i:s')
                    );

                    if ($this->quotation_model->insert_item($item)) {
                        /* To update old item stock also delete from sales order items start */
                        //$this->product_model->update_branch_stock($user_data['branch_id'], $old_so_item->item_id, $old_so_item->quantity, '+');
                        $this->quotation_model->delete_so_item($so_item_id);
                        /* To update old item stock also delete from sales order items end */

                        // To update new item quantity in stock
                        //$this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $quantity[$key], '-');
                    }
                    //Insert Item Cost Log
                    //$this->update_price_log($product_id[$key], $unit_cost[$key], $price[$key], 'From Invoice');
                }
            }

            // Update existing product end
            // Delete existing row start
            $delete_items = $this->quotation_model->get_removed_so_items($id, $product_id);
            foreach ($delete_items as $key => $remove_item) {
                if ($this->quotation_model->delete_so_item($remove_item->id)) {
                    //$this->product_model->update_branch_stock($user_data['branch_id'], $remove_item->item_id, $remove_item->quantity, '+');
                }
            }
            // Delete existing row end
            // Add newly added product start
            if (!empty($this->input->post('new_quantity'))) {
                $category_id = $this->input->post('new_category_id');
                $product_id = $this->input->post('new_product_id');
                $product_description = $this->input->post('new_product_description');
                $product_code = $this->input->post('new_product_code');
                $quantity = $this->input->post('new_quantity');
                $price = $this->input->post('new_price');
                $unit_cost = $this->input->post('new_cost');
                $discount = $this->input->post('new_discount');
                $discount_type = $this->input->post('new_discount_type');
                $discount_amt = $this->input->post('new_discount_amt');
                $amount = $this->input->post('new_amount');

                foreach ($category_id as $key => $category) {
                    if (!empty($product_id[$key])) {
                        $profit = ($quantity[$key] * $price[$key]) - ($quantity[$key] * $unit_cost[$key]);
                        $item = array(
                            'quo_id' => $id,
                            'category_id' => $category,
                            'item_id' => $product_id[$key],
                            'product_description' => $product_description[$key],
                            'price' => $price[$key],
                            'unit_cost' => $unit_cost[$key],
                            'quantity' => $quantity[$key],
                            'total' => $amount[$key],
                            'item_profit' => $profit,
                            'status' => 1,
                            'branch_id' => $user_data['branch_id'],
                            'created_by' => $user_data['user_id'],
                            'created_on' => date('Y-m-d H:i:s')
                        );
                        if ($this->quotation_model->insert_item($item)) {
                            //$this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $quantity[$key]);
                        }
                        //Insert Item Cost Log
                        //$this->update_price_log($product_id[$key], $unit_cost[$key], $price[$key], 'From Invoice');
                    }
                }
            }

            // Add newly added product end
            redirect('quotation/print_pdf/' . $id, 'refresh');
        } else {
            $_SESSION['error'] = 'Quotation not updated!';
            redirect('quotation/edit/' . $id, 'refresh');
        }
    }

    public function view($id) {
        $data = array();
        $user_data = get_user_data();
        $data['browser'] = browser_info();
        $data['sales_order'] = $this->quotation_model->get_by_id($id);
        $data['sales_order_items'] = $this->quotation_model->get_products_by_so($id);
        $data['shipping_address'] = $this->quotation_model->shipping_address_by_id($data['sales_order']->shipping_address);
        $data['address'] = $this->quotation_model->get_current_address($user_data['branch_id']);

        //$data['thermal_src'] = base_url() . 'invoice/thermal/' . $id;
        //$data['thermal'] = $this->load->view('sales_order/thermal', $data, TRUE);;
        $data['invoice_link'] = base_url() . 'quotation/pdf/' . $id;
        //$data['thermal'] = $this->thermal($id);
        $data['do_link'] = base_url() . 'invoice/invoice_to_delivery_order/' . $id;
        $this->render($data, 'quotation/view');
    }

    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }

        $user_data = get_user_data();
        $data_sales_order = $this->quotation_model->get_by_id($id);
        $delete_items = $this->quotation_model->get_removed_so_items($id);
        foreach ($delete_items as $key => $remove_item) {
            $item = array(
                'status' => 10,
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $user_data['user_id']
            );
            if ($this->quotation_model->update_item($item, $remove_item->id)) {
                $this->product_model->update_branch_stock($user_data['branch_id'], $remove_item->item_id, $remove_item->quantity, '+');
            }
        }

        $sales_order = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $user_data['user_id']
        );

        if ($this->quotation_model->update($id, $sales_order) > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $data_sales_order->quo_no, 1);
            $_SESSION['success'] = 'Quotation deleted successfully';
        } else {
            $_SESSION['error'] = 'Quotation Order not deleted!';
        }

        redirect('quotation', 'refresh');
    }

    public function send_mail($id = '') {
        if (empty($id)) {
            show_400_error();
        }

        $data = array();
        $user_data = get_user_data();
        $this->user_data;

        $data['sales_order'] = $this->quotation_model->get_by_id($id);
        #$to = $data['sales_order']->email;
        $to = $this->input->post('to_mail');
        if (empty($to)) {
            $_SESSION['error'] = 'Customer email is empty. Please add customer email and try again!';
            redirect('quotation', 'refresh');
        }

        $data['so_items'] = $this->quotation_model->get_products_by_so($id);
        $data['address'] = $this->quotation_model->get_current_address($this->user_data['branch_id']);
        $data['amt_word'] = convert_amount_to_word_rupees($data['sales_order']->total_amt);
        $data['shipping_address'] = $this->quotation_model->shipping_address_by_id($data['sales_order']->shipping_address);
        $data['user_data'] = $this->user_data;
        $data['branch'] = $this->company_model->branch_by_id($data['sales_order']->branch_id);
        $data['branches'] = $this->company_model->active_branch_first();
        $data['company'] = $this->company_model->get_company();
        $filename = 'Quotation_' . $data['sales_order']->quo_no;
        $filename = strtoupper($filename) . '.pdf';

        // Load Views
        $header = $this->load->view('quotation/pdf_header', $data, TRUE); //'<div></div>';
        $content = $this->load->view('quotation/pdf', $data, TRUE);
        $footer = ''; //$this->load->view('quotation/pdf_footer', $data, TRUE);
        $mail_html = $header . $content . $footer;
        //echo $mail_html;

        require(APPPATH . '/helpers/mpdf/mpdf.php');
        $mpdf = new mPDF('utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                0, // font size - default 0
                '', // default font family
                '15', // 15 margin_left
                '5', // 15 margin right
                '0', // 16 margin top 84
                '0', // margin bottom 75 150 140
                '8', // 9 margin header
                '0', // 9 margin footer
                'P'
        );
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->setAutoBottomMargin = 'stretch';
        $mpdf->WriteHTML($header);
        $mpdf->WriteHTML($content);
        $mpdf->SetHTMLFooter($footer);

        $file_path = QUOT_EMAIL_PATH;
        if (!is_dir($file_path)) {
            if (!mkdir($file_path, 0777, true))
                error_log('Failed to create folder for ' . $branch_name);
        }

        $file_path = $file_path . $filename;
        $mpdf->Output($file_path, 'F');

        /* From Email */
        $from = (!empty($data['company']->comp_email)) ? $data['company']->comp_email : 'info@refulgenceinc.com';
        $subject = 'UCS - Quotation(' . $data['sales_order']->quo_no . ')';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "From: " . $from . "\r\n";
        $headers .= "Reply-To: " . $from . "\r\n";
        $headers .= "X-Mailer: PHP/" . phpversion();

        // boundary
        $semi_rand = md5(time());
        $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";

        // headers for attachment
        $headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"";

        // multipart boundary
        $mail_html = "This is a multi-part message in MIME format.\n\n" . "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"iso-8859-1\"\n" . "Content-Transfer-Encoding: 7bit\n\n" . $mail_html . "\n\n";
        // preparing attachments

        $file_ptr = fopen($file_path, "rb");
        $data = fread($file_ptr, filesize($file_path));
        fclose($file_ptr);
        $data = chunk_split(base64_encode($data));

        $mail_html .= "--{$mime_boundary}\n";
        $mail_html .= "Content-Type: {\"application/octet-stream\"};\n" . " name=\"$filename\"\n";
        $mail_html .= "Content-Disposition: attachment;\n" . " filename=\"$filename\"\n";
        $mail_html .= "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
        $mail_html .= "--{$mime_boundary}--\n";
        #echo $mail_html;exit;

        $ok = @mail($to, $subject, $mail_html, $headers);
        if ($ok) {
            $_SESSION['success'] = 'Quotation sent successfully to the customer email <span class="lowercase">' . $to . '</span>.';
        } else {
            $_SESSION['error'] = 'Quotation not sent to the customer email <span class="lowercase">' . $to . '</span>. Try again!';
        }

        $insert = array(
            'quotation_id' => $id,
            'created_on' => date('Y-m-d H:i:s'),
            'created_by' => $this->user_data['user_id']
        );

        $this->quotation_model->insert_quotation_mail($insert);
        unlink($file_path);
        redirect('quotation', 'refresh');
    }

    public function get_email_by_id() {
        $data = $this->quotation_model->get_by_id($_POST['id']);
        echo json_encode($data);
    }

}

?>