<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class External_services extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('external_services_model', '', TRUE);
        $this->load->model('services_model', '', TRUE);
        $this->load->model('exchange_order_model', '', TRUE);
        $this->load->model('bank_model', '', TRUE);
        $this->load->model('purchase_order_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->model('supplier_model', '', TRUE);
        $this->load->model('company_model', '', TRUE);
        $this->load->model('sales_order_model', '', TRUE);
        $this->load->library('Datatables');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        $data['print_link'] = base_url() . 'external_services/pdf/';
        $data['external_services'] = $this->external_services_model->list_all();
        $this->render($data, 'external_services/list');
    }
    
    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('extser.id AS srId, extser.exter_service_no as exter_service_no, extser.service_partner_name as service_partner_name, extser.exter_amount as exter_amount, sjst.jobsheet_number as jobsheet_number, DATE_FORMAT(extser.issued_date, "%d/%m/%Y") as issued_date_str, DATE_FORMAT(extser.received_date, "%d/%m/%Y") as received_date_str, CASE extser.exter_pay_status WHEN 1 THEN "Unpaid" WHEN 2 THEN "Paid" ELSE "" END as exter_pay_status_str, CASE extser.status WHEN 1 THEN "Issued" WHEN 2 THEN "Received" ELSE "" END as status_str', FALSE)             
                ->where('extser.status != 10 AND extser.branch_id IN ("'.$this->user_data['branch_id'].'")')
                ->join(TBL_BRN . ' as brn', 'brn.id = extser.branch_id', 'left')
                ->join(TBL_SERPTN . ' as supl', 'supl.id = extser.service_partner_id', 'left')
                ->join(TBL_SEREXTITM . ' as extl', 'extl.external_service_id = extser.id AND extl.status != 2 AND extl.status != 10', 'left')
                ->join(TBL_SJST . ' as sjst', 'sjst.id = extl.service_id', 'left')
                ->add_column('Actions', $this->get_buttons('$1'), 'srId')
                ->from(TBL_EXTSER.' AS extser');
        echo $this->datatables->generate();
    }
    
    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(1, $this->permission)) {
        $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(3, $this->permission)) {
        $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
        }
        if (in_array(4, $this->permission)) {
        $html .='<a class="label btn btn-danger delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Exchange Order!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Exchange Order"><span>Delete</span></a>';        
        }
        $html.='</span>';
        return $html;
    }
    
    public function add() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['form_action'] = $actions['insert'];
        $data['service_partners'] = $this->external_services_model->get_all_service_partner();
        $data['company'] = $this->company_model->get_company();
        $prefix_wt_sub = CODE_EXTSER;
        $data['exter_service_number'] = $this->auto_generation_code(TBL_EXTSER, $prefix_wt_sub, '', 3, '');
        $this->render($data, 'external_services/add');
    }
    
    public function insert() {
        $prefix_wt_sub = CODE_EXTSER;
        $exter_ser_number = $this->auto_generation_code(TBL_EXTSER, $prefix_wt_sub, '', 3, '');
        $user_data = get_user_data();
        
        $sup_id = $this->input->post('service_partner_id');
        $service_partner_name = ($sup_id != '') ? $this->input->post('service_partner_name') : $this->input->post('service_partner_name_txt');
        
        $received_date = (!empty($this->input->post('received_date'))) ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('received_date')))) : '';
        
        $insert = array(
            'exter_service_no' => $exter_ser_number,
            'service_partner_id' => $sup_id,
            'service_partner_name' => $service_partner_name,
            'exter_amount' => $this->input->post('exter_amount'),
            'exter_desc' => $this->input->post('exter_desc'),
            'issued_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('issued_date')))),
            'status' => $this->input->post('status'),
            'created_by' => $user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s'),
            'branch_id' => $user_data['branch_id']
        );
        
        if ($received_date != "") :
            $insert['received_date'] = $received_date;
        endif;
	
        $last_id = $this->external_services_model->insert($insert);

        if ($last_id > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, $exter_ser_number, '');
            $_SESSION['success'] = 'Service added successfully';
        } else {
            $_SESSION['error'] = 'Service not added!';
        }

        redirect('external_services/add', 'refresh');
    }
    
    public function edit($id = '') {

        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['external_service'] = $this->external_services_model->get_by_id($id);
	$data['service_partners'] = $this->external_services_model->get_all_service_partner();
        $data['company'] = $this->company_model->get_company();
        
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' . $data['external_service']->id;
        
        $this->render($data, 'external_services/edit');
    }
    
     public function update($id = '') {
        
        if (empty($id)) {
            show_400_error();
        }
        
        $user_data = get_user_data();
        
        $sup_id = $this->input->post('service_partner_id');
        $service_partner_name = ($sup_id != '') ? $this->input->post('service_partner_name') : $this->input->post('service_partner_name_txt');
        
        $received_date = (!empty($this->input->post('received_date'))) ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('received_date')))) : '';
        
        $servicedata = array(            
            'service_partner_id' => $sup_id,
            'service_partner_name' => $service_partner_name,
            'exter_amount' => $this->input->post('exter_amount'),
            'exter_desc' => $this->input->post('exter_desc'),
            'issued_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('issued_date')))),
            'status' => $this->input->post('status'),
            'branch_id' => $user_data['branch_id'],
            'updated_by' => $user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s')
        );
        
        if ($received_date != "") :
            $servicedata['received_date'] = $received_date;
        endif;
        
        $affected_rows = $this->external_services_model->update($id, $servicedata);
        
        if ($affected_rows > 0) {
            $external_service = $this->external_services_model->get_by_id($id);
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $external_service->exter_service_no, ''); 
            $_SESSION['success'] = 'Service Updated Successfully';
           
        } else {
            $_SESSION['error'] = 'Service not updated!';
        }

        redirect('external_services/edit/' . $id, 'refresh');
    }
    
    public function view($serv_id = '') {
        if (empty($serv_id)) {
            show_400_error();
        }
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['service_pdf'] = base_url() . 'external_services/pdf/' . $serv_id;
        $data['external_service'] = $this->external_services_model->get_by_id($serv_id);
        $data['company'] = $this->company_model->get_company();
		
        $this->render($data, 'external_services/view');
    }
    
    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }
        $user_data = get_user_data();
        $external_service = $this->external_services_model->get_by_id($id);        
        $service = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $user_data['user_id']
        );
        
        if ($this->external_services_model->update($id, $service) > 0){
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $external_service->exter_service_no, 1); 
            $_SESSION['success'] = 'Service deleted successfully';
        }else{
            $_SESSION['error'] = 'Service not deleted!';
        }
        redirect('external_services', 'refresh');
    }
    
}

?>