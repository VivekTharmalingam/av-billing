<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class External_Service_Payments extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('external_service_payment_model', '', TRUE);
        $this->load->model('external_services_model', '', TRUE);
        $this->load->model('purchase_receive_model', '', TRUE);
        $this->load->model('supplier_model', '', TRUE);
        $this->load->model('payment_type_model', '', TRUE);
        $this->load->model('bank_model', '', TRUE);
        $this->load->library('Datatables');
    }
    
    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['add_btn']    = 'Create Exc-Order Payments';
        $data['view_link']  = $actions['view'] . '/';
        $data['add_link']   = $actions['add'];
        $data['edit_link']  = $actions['edit'] . '/';
        $data['external_service_payment'] = $this->external_service_payment_model->list_all();
        $this->render($data, 'external_service_payment/list');
    }

    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('exr.id AS exrId, exr.exter_service_no AS exter_service_no, sjst.jobsheet_number as jobsheet_number, DATE_FORMAT(exr.issued_date, "%d/%m/%Y") as issued_date_str, IF(payt.is_cheque = 1 AND pay.pay_pay_no != "", CONCAT(payt.type_name, "&nbsp;/ ", pay.pay_pay_bank, "&nbsp;- ", pay.pay_pay_no), payt.type_name) as type_name, exr.exter_amount AS net, COALESCE(SUM(pay.pay_amt), 0) as paid_amount, CASE exr.exter_pay_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as status_str, exr.service_partner_name AS supplier', FALSE)
                ->where('pay.status != 10 AND pay.brn_id IN ("' . $this->user_data['branch_id'] . '")')
                ->join(TBL_EXTSER . ' as exr', 'exr.id = pay.external_service_id AND exr.status != 10', 'left')
                ->join(TBL_PAYMENT_TYPE . ' as payt', 'payt.id = pay.pay_pay_type', 'left')
                ->join(TBL_SERPTN . ' as sup', 'sup.id = exr.service_partner_id', 'left')
                ->join(TBL_SEREXTITM . ' as extl', 'extl.external_service_id = exr.id AND extl.status != 2 AND extl.status != 10', 'left')
                ->join(TBL_SJST . ' as sjst', 'sjst.id = extl.service_id', 'left')
                ->group_by('pay.external_service_id')
                ->add_column('Actions', $this->get_buttons('$1'), 'exrId')
                ->from(TBL_EXTSERPAY . ' AS pay');
        echo $this->datatables->generate();
    }

    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        $html.='</span>';
        return $html;
    }

    public function add($id = '') {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        
        $actions = $this->actions();
        $data['form_action'] = $actions['insert'];
        $data['list_link'] = $actions['index'];
        $data['service_partners']  = $this->external_services_model->get_all_service_partner();
        $data['other_service_partners'] = $this->external_service_payment_model->other_service_partners_list();
        $data['payment_types'] = $this->payment_type_model->list_active();
        $bnk_acc_name = $this->user_model->get_active_acc($this->user_data['bank_account_id']);
        $data['bnk_acc_name'] = (count($bnk_acc_name)>0) ? $bnk_acc_name[0]['bank_name'] : '';
        $this->render($data, 'external_service_payment/add');
    }
    
    public function unpaid_purchases() {
        $service_partner_id = $this->input->post('service_partner_id');
        $service_partner_txt = $this->input->post('service_partner_txt');
        $data = array();
        $data['exc_order'] = $this->external_service_payment_model->get_unpaid_purchases_by_service_partner($service_partner_id,$service_partner_txt);
        //error_log($this->db->last_query());
        $this->ajax_response($data);
    }

    public function insert() {
		$external_service_payment = array(
			'pay_date' => date('Y-m-d', strtotime(str_replace('/','-',$this->input->post('pay_date')))),
			'pay_pay_type' => $this->input->post('payment_type'),
			'pay_pay_no' => '',
			'pay_pay_bank' => '',
			'pay_pay_date' => NULL,
			'pay_dd_no' => '',
			'pay_dd_date' => NULL,
			'status' => 1,
			'brn_id' => $this->user_data['branch_id'],
			'created_by' => $this->user_data['user_id'],
			'created_on' => date('Y-m-d H:i:s'),
			'ip_address' => $_SERVER['REMOTE_ADDR']
		);
		
		$payment_type = $this->payment_type_model->get_by_id($this->input->post('payment_type'));
		if ($payment_type->is_cheque == 1) {
			$external_service_payment['pay_pay_no'] = $this->input->post('cheque_acc_no');
			$external_service_payment['pay_pay_bank'] = $this->input->post('cheque_bank_name');
			$external_service_payment['pay_pay_date'] = date('Y-m-d', strtotime(str_replace('/','-',$this->input->post('cheque_date'))));
		}
		
		$exr_noId = $this->input->post('exr_noId');
		$amount = $this->input->post('amount');
		$paid_amount = $this->input->post('paid_amount');
		$already_paid = $this->input->post('already_paid');
		$affected_rows = 0;
		foreach($exr_noId as $key => $value) {
			$external_service_payment['external_service_id'] = $value;
			$external_service_payment['pay_amt'] = $paid_amount[$key];
			$external_service_payment['bank_acc_id'] = $this->user_data['bank_account_id'];
			if ($this->external_service_payment_model->insert($external_service_payment)) {
                                $external_service = $this->external_service_payment_model->get_by_id($value);
                                $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $value, $external_service->exter_service_no, '');
                                if (!in_array($payment_type->group_name, array(1, 3))) {
                                                        $update_data = array(
                                                                'updated_by' => $this->user_data['user_id'],
                                                                'updated_on' => date('Y-m-d H:i:s')
                                                        );

                                                        $this->bank_model->update_bank_amount($this->user_data['bank_account_id'], $update_data, $paid_amount[$key], '-');
                                }
				
				$payment = array();
				if ((($amount[$key] - ($paid_amount[$key] + $already_paid[$key])) > 0)) 
					$payment['exter_pay_status'] = 2;
				else 
					$payment['exter_pay_status'] = 3;
				
				$payment['branch_id'] = $this->user_data['branch_id'];
				$payment['updated_by'] = $this->user_data['user_id'];
				$payment['updated_on'] = date('Y-m-d H:i:s');
				$this->external_service_payment_model->update_exc_order_payment_status($value, $payment);
				
				$affected_rows ++;
			}
		}
		
		if ($affected_rows) {
			$_SESSION['success'] = 'External Service Payment has been added successfully.';
		}
		else {
			$_SESSION['error'] = 'External Service Payment not added!';
        }
        
        redirect('external_service_payments/add', 'refresh');
    }
    
    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $actions = $this->actions();
        $data = array();
        $data['delete_link']= $actions['delete'];
        $external_service = $this->external_service_payment_model->get_by_id($id);
        $external_service_payment = $this->external_service_payment_model->get_payment_by_excord_id($id);
        $data['external_service'] = $external_service;
        $data['external_service_payment'] = $external_service_payment;
        $this->render($data, 'external_service_payment/view');
    }
    
    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }
		
		$external_service = $this->external_service_payment_model->get_payment_by_pay_id($id);
		$payment = $this->external_service_payment_model->get_excord_pay_by_id($id);
        $update_payment = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $this->user_data['user_id']
        );
			
        if ($this->external_service_payment_model->update($id, $update_payment) > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $external_service->external_service_id, $external_service->exter_service_no, 1);
            if (!in_array($payment->group_name, array(1, 3))) {
				$update_data = array(
					'updated_by' => $this->user_data['user_id'],
					'updated_on' => date('Y-m-d H:i:s')
				);
				
				$this->bank_model->update_bank_amount($payment->bank_acc_id, $update_data, $payment->pay_amt, '+');
			}
			
            $update_external_service = array();
            $update_external_service['exter_pay_status'] = 1;
            $update_external_service['branch_id'] = $this->user_data['branch_id'];
            $update_external_service['updated_by'] = $this->user_data['user_id'];
            $update_external_service['updated_on'] = date('Y-m-d H:i:s');
            $this->external_service_payment_model->update_exc_order_payment_status($payment->external_service_id, $update_external_service);
			
            $_SESSION['success'] = 'External Service Payment has been deleted';
        }
		else {
            $_SESSION['error'] = 'External Service Payment not deleted!';
        }
		
        redirect('external_service_payments', 'refresh');
    }
}
?>