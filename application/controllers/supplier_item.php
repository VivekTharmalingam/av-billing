<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Supplier_item extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('supplier_item_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->model('company_model', '', TRUE);
        $this->load->library('upload');
    }
    public function index() {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = base_url() . 'item/view' . '/';
        $data['form_action'] = base_url() . 'supplier_item';
        $data['item'] = array();
        $data['supp_id'] = '';
        if(!empty($this->input->post('supp_id'))){
            $data['supp_id'] = $this->input->post('supp_id');
            $data['item'] = $this->supplier_item_model->get_unique_supplier_lists($data['supp_id']);
            //echo '<pre>'; print_r($data['item']); exit;
        }
//      echo $this->db->last_query();
        $data['supplier_list'] = $this->supplier_item_model->list_supplier();
        if ($this->input->post('reset_btn')) {
            $data['search_date'] = '';
            $data['branch_id'] = '';
            $data['supp_id'] = '';
            $data['item'] = array();
        }

        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Supplier Wise Item Reports';
            $head['setting'] = $this->company_model->get_company();
            $filename = 'Supplier_Wise_Item_' . current_date();
            // Load Views
            $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
            $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
            $header = '<div></div>';
            $content = $this->load->view('supplier_item/pdf_report', $data, TRUE);
            $footer = '<div></div>';
            $this->load->helper(array('My_Pdf'));
            create_pdf($header, $content, $footer, $filename);
            exit(0);
        } elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $this->load->view('supplier_item/excel_report', $data);
        } else {
            $this->render($data, 'supplier_item/list');
        }
    }
}

?>