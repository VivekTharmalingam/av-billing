<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Service_status extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('service_status_model', '', TRUE);
        $this->load->library('upload');
        $this->load->library('Datatables');
    }
    
    public function index() {
        $data = array();
        $data['success']=$this->data['success'];  
        $data['error']=$this->data['error'];       
        $actions = $this->actions();
        $data['add_btn']    = 'Create Service Status';
        $data['view_link']  = $actions['view'] . '/';
        $data['add_link']   = $actions['add'];
        $data['edit_link']  = $actions['edit'] . '/';
        $data['delete_link']= $actions['delete'] . '/';
        $this->render($data, 'service_status/list');
    }

    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('serst.id AS bId,serst.status_name AS name', FALSE)
                ->where('serst.status != 10 AND serst.status != 2')
                ->add_column('Actions', $this->get_buttons('$1'), 'bId')
                ->from(TBL_SERSTS . ' AS serst');
        echo $this->datatables->generate();
    }

    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(3, $this->permission)) {
            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
        }
        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger pop_up_confirm delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Service Status details!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Service Status"><span>Delete</span></a>';
        }
        $html.='</span>';
        return $html;
    }
        
    public function add() {
        $data = array();
        $data['success']=$this->data['success'];  
        $data['error']=$this->data['error'];        
        $actions = $this->actions();
        $data['form_action'] = $actions['insert'];
        $data['list_link']   =  $actions['index'];
		
        $this->render($data, 'service_status/add');
    }
    
    public function insert() {
        $user_data = get_user_data();  
       
        $insert = array(
            'status_name' => $this->input->post('status_name'),            
            'status_desc' => $this->input->post('status_desc'),            
            'status' => 1,            
            'branch_id' => $this->user_data['branch_id'],
            'created_by' => $this->user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s')
        );
		
        $last_id =$this->service_status_model->insert($insert);
       
        if ( $last_id > 0)  {          
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, '', '');
            $_SESSION['success'] = 'Service Status added successfully';			
        } else { 
            $_SESSION['error'] = 'Service Status not added!';   
        }
        redirect('service_status/add', 'refresh');
    }
    
    public function edit($id = ''){
        if (empty($id)) {
            show_400_error();
        }
        $data['success']=$this->data['success'];  
        $data['error']=$this->data['error'];
        
        $data['service_status'] = $this->service_status_model->get_by_id($id);				
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' . $data['service_status']->id;        
        $this->render($data, 'service_status/edit');
    }
    
    public function update($id = '') {    
			
        if (empty($id)) {
            show_400_error();
        }
        $user_data = get_user_data();
        
        $update = array(
            'status_name' => $this->input->post('status_name'),            
            'status_desc' => $this->input->post('status_desc'),            
            'branch_id' => $this->user_data['branch_id'],              
            'updated_by' => $this->user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s')
        );
       
        if ($this->service_status_model->update($id, $update) > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', '');    
            $_SESSION['success'] = 'Service Status updated successfully';
        } else {
            $_SESSION['error'] = 'Service Status not updated!';
        }
        redirect('service_status/edit/' . $id, 'refresh');
    }
    
    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }        
        $data = array();
        $data['service_status'] = $this->service_status_model->get_by_id($id);						
        $this->render($data, 'service_status/view');
    }
    
    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }        
        $service_status = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $this->user_data['user_id']
        );        
        if ($this->service_status_model->update($id, $service_status) > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', 1);
            $_SESSION['success'] = 'Service Status deleted successfully';
        } else {
            $_SESSION['error'] = 'Service Status not deleted!';    
        }
        redirect('service_status', 'refresh');
    }
    
    public function check_service_status_exists(){
       $service_status_id = (!empty($_REQUEST['service_status_id'])) ? $_REQUEST['service_status_id'] : '';
       $service_status = $_REQUEST['service_status_name'];
       echo $this->service_status_model->check_service_status_exists($service_status, $service_status_id);
    }
}
?>