<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Delivery_Orders extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('delivery_order_model', '', TRUE);
        $this->load->model('sales_order_model', '', TRUE);
        $this->load->model('category_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->model('customer_model', '', TRUE);
        #$this->load->model('company_model', '', TRUE);
        $this->load->library('Datatables');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['print_link'] = base_url() . 'delivery_orders/pdf/';
        $data['delete_link'] = base_url() . 'delivery_orders/delete_so/';
        $data['delivery_orders'] = $this->delivery_order_model->list_all();
        #echo $this->db->last_query();exit;
        $this->render($data, 'delivery_order/list');
    }

    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('do.id AS dId, do.so_id AS sId, so.so_no AS inv, DATE_FORMAT(so.so_date, "%d/%m/%Y") as date, CASE so.status WHEN 1 THEN "New" WHEN 2 THEN "Partially Delivered" WHEN 3 THEN "Delivered" ELSE "" END as status_str, CASE so.so_type WHEN 1 THEN c.name ELSE so.customer_name END as name, (SELECT SUM(quantity) FROM ' . TBL_DO_ITM . ' as doi WHERE doi.do_id = do.id AND doi.status != 10) as do_quantity, (SELECT SUM(quantity) FROM ' . TBL_SO_ITM . ' as soi WHERE soi.so_id = do.so_id AND soi.status != 10) as so_quantity', FALSE)
                ->where('do.status != 10 AND do.branch_id IN ("' . $this->user_data['branch_id'] . '")')
                ->join(TBL_SO . ' as so', 'so.id = do.so_id', 'left')
                ->join(TBL_CUS . ' as c', 'c.id = so.customer_id', 'left')
                ->group_by('do.so_id')
                ->add_column('Actions', $this->get_buttons('$1'), 'sId')
                ->from(TBL_DO . ' AS do');
        echo $this->datatables->generate();
    }

    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(6, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . base_url() . 'delivery_orders/pdf/' . $id . '" title="Print Invoice Details"><span><i class="fa fa-file-pdf-o" aria-hidden="true"></i></span></a>&nbsp';
        }
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Delivery Order!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Delivery Order"><span>Delete</span></a>';
        }
        $html.='</span>';
        return $html;
    }

    public function add() {
        $data = array();
        $actions = $this->actions();
        $data['list_link'] = $actions['index'] . '/';
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['form_action'] = $actions['insert'];
        /* $data['categories'] = $this->category_model->list_active();
          $data['items'] = $this->product_model->list_active();
          $data['customers'] = $this->customer_model->list_active(); */
        $data['sales_orders'] = $this->delivery_order_model->undelivered_sales_orders();

        #$data['item'] = json_encode($data['items']);

        $this->render($data, 'delivery_order/add');
    }

    public function so_details() {
        $data = array();
        $so_id = $this->input->post('so_id');
        $user_data = get_user_data();
        $data['sales_order'] = $this->sales_order_model->get_by_id($so_id);
        $data['shipping_address'] = $this->sales_order_model->shipping_address_by_id($data['sales_order']->shipping_address);
        $data['customer'] = $this->customer_model->get_by_id($data['sales_order']->customer_id);
        $data['so_category'] = $this->delivery_order_model->item_category_by_so_id($so_id);
//        $data['so_items'] = $this->delivery_order_model->items_by_so_id($so_id);
        $data['so_items'] = $this->delivery_order_model->items_by_allso_id($so_id);
        $data['query'] = $this->db->last_query();
        $data['address'] = $this->sales_order_model->get_current_address($user_data['branch_id']);
        $this->ajax_response($data);
    }

    public function so_item_details() {
        $so_id = $this->input->post('so_id');
        $category_id = $this->input->post('category_id');
//        $data['so_items'] = $this->delivery_order_model->items_by_so_id($so_id, $category_id);
        $data['so_items'] = $this->delivery_order_model->items_by_allso_id($so_id, $category_id);
        $this->ajax_response($data);
    }

    public function shipping_address_by_customer() {
        $data = array();
        $data['addresses'] = $this->customer_model->get_address_by_id($this->input->post('customer_id'));
        $data['customer'] = $this->customer_model->get_by_id($this->input->post('customer_id'));
        $this->ajax_response($data);
    }

    public function get_product_by_category() {
        $data = array();
        $data['products'] = $this->product_model->get_product_by_category($this->input->post('category_id'));
        $this->ajax_response($data);
    }

    public function get_products_by_so() {
        $so_id = $_REQUEST['so_id'];
        $data = array();
        if (!empty($so_id)) {
            $data['products'] = $this->purchase_receive_model->get_products_by_so($so_id);
            $data['so_detail'] = $this->purchase_receive_model->get_supplier_by_so($so_id);
        }
        echo json_encode($data);
    }

    public function search_item($item = '') {
        $data = array();
        $data = $this->product_model->search_item($item);
        foreach ($data as $key => $value) {
            //$value->value = $value->id;
            $value->label = $value->pdt_code . ' / ' . $value->pdt_name;
            $value->value = $value->label;
        }
        $this->ajax_response($data);
    }

    /* public function search_item($item = '') {
      $data = array();
      $items = $this->product_model->search_item($item);
      foreach($items as $key => $value) {
      $item = array(
      'value' => $value->id,
      'label' => $value->pdt_code . ' / ' . $value->pdt_name
      );
      array_push($data, $item);
      }
      $this->ajax_response($data);
      } */

    public function insert() {
        $sales_order = $this->sales_order_model->get_by_id($this->input->post('so_no'));
        $branch = $this->company_model->branch_by_id($sales_order->branch_id);

        $do_no = str_replace($branch->branch_name, 'DO', $sales_order->so_no);
        //$do_no = $this->auto_generation_code(TBL_DO, CODE_DO, '', 6, '');
        /* $brn_slug = $this->company_model->list_active_by_company_id($this->user_data['branch_id']);
          $inv = strtoupper(get_var_name($brn_slug[0]->branch_name));
          $do_no = $this->auto_generation_code(TBL_SO, $inv . ' ', '', 5, '', $this->user_data['branch_id']); */
        $user_data = get_user_data();
        $insert = array(
            'so_id' => $this->input->post('so_no'),
            'do_no' => $do_no,
            'do_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('do_date')))),
            'status' => 1,
            'branch_id' => $user_data['branch_id'],
            'created_by' => $user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s')
        );
        $last_id = $this->delivery_order_model->insert($insert);

        if ($last_id > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $this->input->post('so_no'), $do_no, '');
            $_SESSION['success'] = 'Delivery Order added successfully';
            $category_id = $this->input->post('category_id');
            $product_id = $this->input->post('pdt_id');
            //$product_desc = $this->input->post('product_description');
            $quantity = $this->input->post('quantity');
            $so_quantity = $this->input->post('so_quantity');

            foreach ($category_id as $key => $category) {
                $item = array(
                    'do_id' => $last_id,
                    'category_id' => $category_id[$key],
                    'item_id' => $product_id[$key],
                    'quantity' => $quantity[$key],
                    'status' => 1,
                    'branch_id' => $user_data['branch_id'],
                    'created_by' => $user_data['user_id'],
                    'created_on' => date('Y-m-d H:i:s')
                );
                $this->delivery_order_model->insert_item($item);
            }

            $data = array();
            $so_id = $this->input->post('so_no');
            $qty = $this->delivery_order_model->get_soqty_by_id($so_id)->so_quantity;
            $delivered_qty = $this->delivery_order_model->get_doqty_by_id($so_id)->qty;

            if ($qty <= $delivered_qty) {
                $data['status'] = 3;
            } else {
                $data['status'] = 2;
            }
            $this->sales_order_model->update($so_id, $data);
        } else {
            $_SESSION['error'] = 'Delivery Order not added!';
        }

        redirect('delivery_orders/add', 'refresh');
    }

    public function print_pdf($id) {
        $data = array();
        $actions = $this->actions();
        $data['list_link'] = $actions['index'];
        $data['add_link'] = $actions['add'];
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['pdf_link'] = base_url() . 'sales_orders/pdf/' . $id;
        $data['so_id'] = $id;
        $this->render($data, 'sales_order/print');
    }

    /* public function edit($id) {
      $data = array();
      $actions = $this->actions();
      $data['form_action'] = $actions['update'];
      $data['list_link'] = $actions['index'];
      $data['sales_order'] = $this->sales_order_model->get_by_id($id);
      $data['sales_order_items'] = $this->sales_order_model->get_products_by_so($id);
      $data['payment'] = $this->sales_payment_model->get_by_id($id);
      //$data['shipping_address'] = $this->sales_order_model->shipping_address_by_id($data['sales_order']->shipping_address);
      $data['addresses'] = $this->customer_model->get_address_by_id($data['sales_order']->customer_id);
      $data['customer'] = $this->customer_model->get_by_id($data['sales_order']->customer_id);

      $data['success'] = $this->data['success'];
      $data['error'] = $this->data['error'];
      $data['categories'] = $this->category_model->list_active();
      $data['items'] = $this->product_model->list_active();
      $data['customers'] = $this->customer_model->list_active();
      $data['company'] = $this->company_model->get_company();

      $this->render($data, 'sales_order/edit');
      }

      public function update() {
      $id = $this->input->post('so_id');
      $payment_status = $this->input->post('payment_status');
      if ($this->input->post('payment') == 'pay_now') {
      $payment_status = 3;
      }
      $user_data = get_user_data();
      $gst_type = (!empty($this->input->post('gst_type'))) ? 1 : 0;
      $update = array(
      'customer_id' => $this->input->post('customer_name'),
      'shipping_address' => $this->input->post('ship_to'),
      'so_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('so_date')))),
      'payment_status' => $payment_status,
      'sub_total' => $this->input->post('sub_total'),
      'discount' => $this->input->post('discount'),
      'discount_type' => $this->input->post('discount_type'),
      'discount_amount' => $this->input->post('discount_amt'),
      'gst' => $gst_type,
      'gst_amount' => $this->input->post('gst_amount'),
      'gst_percentage' => $this->input->post('hidden_gst'),
      'total_amt' => $this->input->post('total_amt'),
      'payment_terms' => $this->input->post('payment_terms'),
      'your_reference' => $this->input->post('remarks'),
      'branch_id' => $user_data['branch_id'],
      'updated_by' => $user_data['user_id'],
      'updated_on' => date('Y-m-d H:i:s')
      );
      $affected_rows = $this->sales_order_model->update($id, $update);

      if ($affected_rows > 0) {
      $_SESSION['success'] = 'Delivery Order updated successfully';

      // Update existing product start
      $hidden_soitem_id = $this->input->post('hidden_soitem_id');
      $category_id = $this->input->post('category_id');
      $product_id = $this->input->post('product_id');
      $quantity = $this->input->post('quantity');
      $old_quantity = $this->input->post('old_quantity');
      $price = $this->input->post('price');
      $unit_cost = $this->input->post('cost');
      $discount = $this->input->post('discount');
      $discount_type = $this->input->post('discount_type');
      $discount_amt = $this->input->post('discount_amt');
      $amount = $this->input->post('amount');

      foreach ($hidden_soitem_id as $key => $so_item_id) {
      // Old sales order item
      $old_so_item = $this->sales_order_model->get_so_item($so_item_id);

      if ($old_so_item->item_id == $product_id[$key]) {
      $item = array(
      'category_id' => $category_id[$key],
      'price' => $price[$key],
      'unit_cost' => $unit_cost[$key],
      'quantity' => $quantity[$key],
      'total' => $amount[$key],
      'status' => 1,
      'branch_id' => $user_data['branch_id'],
      'updated_by' => $user_data['user_id'],
      'updated_on' => date('Y-m-d H:i:s')
      );

      if ($this->sales_order_model->update_item($item, $so_item_id)) {
      $update_quantity = $quantity[$key] - $old_so_item->quantity;
      #$op = ($update_quantity < 0) ? '-' : '+';
      #echo $op;echo '===='.$update_quantity;
      #exit;
      $this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $update_quantity, '-');
      }
      } else {
      $this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $old_so_item->quantity, '+');
      $this->sales_order_model->delete_so_item($so_item_id);

      $item = array(
      'category_id' => $category_id[$key],
      'item_id' => $product_id[$key],
      'price' => $price[$key],
      'unit_cost' => $unit_cost[$key],
      'quantity' => $quantity[$key],
      'total' => $amount[$key],
      'status' => 1,
      'branch_id' => $user_data['branch_id'],
      'updated_by' => $user_data['user_id'],
      'updated_on' => date('Y-m-d H:i:s')
      );

      if ($this->sales_order_model->insert_item($item)) {
      $this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $quantity[$key], '-');
      }
      }
      }
      // Update existing product end
      // Delete existing row start
      $delete_items = $this->sales_order_model->get_removed_so_items($id, $product_id);
      foreach ($delete_items as $key => $remove_item) {
      if ($this->sales_order_model->delete_so_item($remove_item->id))
      $this->product_model->update_branch_stock($user_data['branch_id'], $remove_item->item_id, $remove_item->quantity, '+');
      }
      // Delete existing row end
      // Add newly added product start
      if (!empty($this->input->post('new_product_id'))) {
      $category_id = $this->input->post('new_category_id');
      $product_id = $this->input->post('new_product_id');
      $quantity = $this->input->post('new_quantity');
      $price = $this->input->post('new_price');
      $unit_cost = $this->input->post('new_cost');
      $discount = $this->input->post('new_discount');
      $discount_type = $this->input->post('new_discount_type');
      $discount_amt = $this->input->post('new_discount_amt');
      $amount = $this->input->post('new_amount');

      foreach ($category_id as $key => $category) {
      $item = array(
      'so_id' => $id,
      'category_id' => $category,
      'item_id' => $product_id[$key],
      'price' => $price[$key],
      'unit_cost' => $unit_cost[$key],
      'quantity' => $quantity[$key],
      'total' => $amount[$key],
      'status' => 1,
      'branch_id' => $user_data['branch_id'],
      'created_by' => $user_data['user_id'],
      'created_on' => date('Y-m-d H:i:s')
      );

      if ($this->sales_order_model->insert_item($item)) {
      $this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $quantity[$key]);
      }
      }
      }
      // Add newly added product end

      if ($this->input->post('payment') == 'pay_now') {
      $sales_payment = array();
      $sales_payment['so_id'] = $id;
      $sales_payment['pay_amt'] = $this->input->post('total_amt');
      $sales_payment['pay_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('so_date'))));
      $sales_payment['pay_pay_type'] = $this->input->post('payment_type');
      if ($this->input->post('payment_type') == '2') {
      $sales_payment['pay_pay_no'] = $this->input->post('cheque_acc_no');
      $sales_payment['pay_pay_bank'] = $this->input->post('cheque_bank_name');
      $sales_payment['pay_pay_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('cheque_date'))));
      } else {
      $sales_payment['pay_pay_no'] = '';
      $sales_payment['pay_pay_bank'] = '';
      $sales_payment['pay_pay_date'] = NULL;
      }

      if ($this->input->post('payment_type') == '3') {
      $sales_payment['pay_dd_no'] = $this->input->post('dd_no');
      $sales_payment['pay_dd_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('dd_date'))));
      } else {
      $sales_payment['pay_dd_no'] = '';
      $sales_payment['pay_dd_date'] = NULL;
      }

      $sales_payment['payment_status'] = 1;
      $sales_payment['status'] = 1;
      $sales_payment['brn_id'] = $user_data['branch_id'];
      $sales_payment['ip_address'] = $_SERVER['REMOTE_ADDR'];

      $payment = $this->sales_payment_model->get_payment($id);
      if (!empty($payment->id)) {
      $sales_payment['updated_by'] = $user_data['user_id'];
      $sales_payment['updated_on'] = date('Y-m-d H:i:s');

      $this->sales_payment_model->update($payment->id, $sales_payment);
      } else {
      $prefix_wt_sub = CODE_SPAY;
      $sales_payment_code = $this->auto_generation_code(TBL_SPM, $prefix_wt_sub, '', '', '');
      $sales_payment['pay_no'] = $sales_payment_code;
      $sales_payment['created_by'] = $user_data['user_id'];
      $sales_payment['created_on'] = date('Y-m-d H:i:s');
      $this->sales_payment_model->insert($sales_payment);
      }
      }

      redirect('sales_orders/print_pdf/' . $id, 'refresh');
      } else {
      $_SESSION['error'] = 'Delivery Order not updated!';
      redirect('sales_orders/edit/' . $id, 'refresh');
      }
      } */

    public function view($id) {
        $data = array();
        $user_data = get_user_data();
        /* $data['delivery_order'] = $this->delivery_order_model->get_by_id($id);
          $data['sales_order'] = $this->sales_order_model->get_by_id($id);
          $data['delivery_order_items'] = $this->delivery_order_model->get_products_by_po($id);
          $data['delivery_order_item_details'] = $this->delivery_order_model->get_products_details($id);
          $data['shipping_address'] = $this->sales_order_model->shipping_address_by_id($data['sales_order']->shipping_address);
          $data['address'] = $this->sales_order_model->get_current_address($user_data['branch_id']); */

        $data['sales_order'] = $this->sales_order_model->get_by_id($id);
        $data['delivery_order'] = $this->delivery_order_model->get_by_invoice_id($id);
        $data['do_items'] = $this->delivery_order_model->delivered_items_by_inv_allid($id);

        $data['delivery_orders'] = array();
        $delivery_order_ids = explode(',', $data['delivery_order']->do_id);
        foreach ($delivery_order_ids as $key => $do_id) {
            $delivery_order = $this->delivery_order_model->get_by_id($do_id);
            $delivery_order->items = $this->delivery_order_model->items_by_alldo_id($do_id);
            array_push($data['delivery_orders'], $delivery_order);
        }

        $this->render($data, 'delivery_order/view');
    }

    public function delete_so($id) {
        $delivery_orders = $this->delivery_order_model->delivery_orders_by_so($id);
        $affected_rows = 0;
        $user_data = get_user_data();
        foreach ($delivery_orders as $key => $do) {
            $update = array(
                'status' => 10,
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $user_data['user_id']
            );

            $this->delivery_order_model->update_items($update, $do->id);
            $affected_rows += $this->delivery_order_model->update($update, $do->id);
        }

        if ($affected_rows > 0) {
            $data = array();
            $data['status'] = 1;
            $this->sales_order_model->update($id, $data);
            $_SESSION['success'] = 'Delivery Order deleted successfully';
        } else {
            $_SESSION['error'] = 'Delivery Order not deleted!';
        }

        redirect('delivery_orders', 'refresh');
    }

    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }

        $user_data = get_user_data();

        $update = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $user_data['user_id']
        );
        $getso = $this->delivery_order_model->delivery_orders_by_so($id);
        foreach ($getso as $key => $value) {
            $this->delivery_order_model->update_items($update, $value->id);
        }
        if ($this->delivery_order_model->update_soid($update, $id) > 0) {
            $delivery_order_arr = $this->delivery_order_model->get_by_invoice_id($id);
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $delivery_order_arr->do_no, 1);
            $_SESSION['success'] = 'Delivery Order deleted successfully';
        } else {
            $_SESSION['error'] = 'Delivery Order not deleted!';
        }

        redirect('delivery_orders', 'refresh');
    }

    public function pdf($invoice_id) {
        $data = array();
        $data['sales_order'] = $this->sales_order_model->get_by_id($invoice_id);
        $data['delivery_order'] = $this->delivery_order_model->get_by_invoice_id($invoice_id);
        $data['do_items'] = $this->delivery_order_model->delivered_items_by_inv_allid($invoice_id);

        $data['shipping_address'] = $this->sales_order_model->shipping_address_by_id($data['sales_order']->shipping_address);
        $data['user_data'] = get_user_data();
        $data['branch'] = $this->company_model->branch_by_id($data['sales_order']->branch_id);
        $data['branches'] = $this->company_model->branch_list_active();
        $data['company'] = $this->company_model->get_company();
        $filename = 'DELIVERY_' . $data['sales_order']->so_no;
        $filename = strtoupper($filename);

        // Load Views
        $header = $this->load->view('delivery_order/pdf_header', $data, TRUE); //'<div></div>';
        $content = $this->load->view('delivery_order/pdf', $data, TRUE);
        $footer = $this->load->view('delivery_order/pdf_footer', $data, TRUE);

        #echo $header.$content.$footer;exit;
        $this->load->helper(array('My_Pdf'));
        delivery_order_pdf($header, $content, $footer, $filename);
        exit(0);
    }

}

?>