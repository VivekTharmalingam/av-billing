<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Log_Reports extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('log_model', '', TRUE);
    }

    public function index() {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['form_action'] = base_url() . 'log_reports';

                $from = '';
                $to = '';
                $brn_name = $this->user_data['branch_id'];
                $search_date_txt = date('01/m/Y') . ' - ' . date('t/m/Y');

                if ($this->input->post('search_date') != '') {
                    $search_date_txt = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : '';
                    $brn_name = ($this->input->post('branch_id') != '') ? $this->input->post('branch_id') : '';
                }    
                
                if ($this->input->post('reset_btn')) {
                    $search_date = date('01/m/Y') . ' - ' . date('t/m/Y');
                    $data['user_id'] = '';
                    $data['branch_id'] = $brn_name;
                }
                
                if($search_date_txt != '') {
                    $search_date = explode('-', $search_date_txt);
                    $from = trim($search_date[0]);
                    $to = trim($search_date[1]);

                    $from = explode('/', $from);
                    $from = $from[2] . '-' . $from[1] . '-' . $from[0];

                    $to = explode('/', $to);
                    $to = $to[2] . '-' . $to[1] . '-' . $to[0];
                }
                
                $data['from_date'] = $from;
                $data['to_date'] = $to;
                $data['search_date'] = $search_date_txt;
		$data['user_id'] = ($this->input->post('user_id') != '') ? $this->input->post('user_id') : '';
		$data['branch_id'] = $brn_name;
        
		$data['users'] = $this->user_model->active_user_list();
                $data['branches'] = $this->company_model->active_user_branches();
                
		$data['log_details'] = $this->log_model->list_search_all($from, $to, $data['branch_id'], $data['user_id']);
		
        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Log Report';
            $head['setting'] = $this->company_model->get_company();
            $filename = 'Log_Report_' . current_date();
            // Load Views
            $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
            $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
            $header = '<div></div>';
            $content = $this->load->view('log_report/pdf_report', $data, TRUE);
            $footer = '<div></div>';//$this->load->view('templates/pdf/pdf_footer', '', TRUE);
            $this->load->helper(array('My_Pdf'));

            create_pdf($header, $content, $footer, $filename);
            exit(0);
        } elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $this->load->view('log_report/excel_report', $data);
        } else {
            $this->render($data, 'log_report/list');
        }
    }
}
?>