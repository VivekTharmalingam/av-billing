<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Scan_service extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('services_model', '', TRUE);
        $this->load->model('bank_model', '', TRUE);
        $this->load->model('purchase_order_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->model('supplier_model', '', TRUE);
        $this->load->model('company_model', '', TRUE);
        $this->load->library('Datatables');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $this->render($data, 'services/search_sheet');
    }
    
    
    public function getsheet_data() {
        $sheet_number = $this->input->post('sheet_number');
        $data = array();
        $response = array();
        $sheetdata = $this->services_model->getsheet_data($sheet_number);
        if(count($sheetdata)>0) {
            $data['service'] = $this->services_model->get_by_id($sheetdata->id);
            $data['service_item'] = $this->services_model->get_item_by_id($sheetdata->id);
            $data['company'] = $this->company_model->get_company();
            $response['jobsheet'] = $this->load->view('services/search_view', $data, TRUE);
        } else {
            $response['jobsheet'] = "<div>No Record!..</div>";
        }
        $this->ajax_response($response);
    }
    
    
    public function insert() {
        $prefix_wt_sub = CODE_SJS;
        $jobsheet_number = $this->auto_generation_code(TBL_SJST, $prefix_wt_sub, '', 3, '');
        $user_data = get_user_data();
        
        $cust_id = $this->input->post('customer_id');
        $customer_name = ($cust_id != '' && $cust_id=='0') ? $this->input->post('customer_name') : $this->input->post('customer_name_txt');
        $estimated_delivery_date = (!empty($this->input->post('estimated_delivery_date'))) ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('estimated_delivery_date')))) : '';
        $uploaded_data = '';
        if (!empty($_FILES['upload_photo']['name'])) {
            $uploaded_data = uploads(array('upload_path' => 'service', 'field' => 'upload_photo'));
        }
        
        $insert = array(
            'jobsheet_number' => $jobsheet_number,
            'customer_id' => $cust_id,
            'customer_name' => $customer_name,
            'contact_person' => $this->input->post('contact_person'),
            'contact_number' => $this->input->post('contact_number'),
            'email_address' => $this->input->post('email_address'),
            'engineer_id' => $this->input->post('engineer_id'),
            'guessing_problem' => $this->input->post('guessing_problem'),
            'estimated_price_quoted' => $this->input->post('estimated_price_quoted'),
            'material_name' => $this->input->post('material_name'),
            'material_type' => $this->input->post('material_type'),
            'problem_desc' => $this->input->post('problem_desc'),
            'received_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('received_date')))),
            'physical_condition' => $this->input->post('physical_condition'),
            'remarks' => $this->input->post('remarks'),
            'created_by' => $user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s'),
            'branch_id' => $user_data['branch_id'],
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        
        if ($estimated_delivery_date != "") :
            $insert['estimated_delivery_date'] = $estimated_delivery_date;
        endif;
        
        if (!empty($uploaded_data['upload_photo'])) :
            $insert['upload_photo'] = $uploaded_data['upload_photo']['file_path_str'];
        endif;
	
        $last_id = $this->services_model->insert($insert);

        if ($last_id > 0) {
            	
                $cat_ids = $this->input->post('cat_ids');
		$cat_other = $this->input->post('cat_other');
                $item_other = $this->input->post('item_other');
                $ser_item = $this->input->post('ser_item');
                $item_others_txt = $this->input->post('item_others_txt');
                $cat_others_txt = $this->input->post('cat_others_txt');
				
                if (!empty($cat_ids)) {
                    foreach ($cat_ids as $key => $catid) {
                        $item_id = 0;
                        if($cat_other[$key]==1 && $cat_others_txt[$key] != "") {
                            $item = array(
                                'service_id' => $last_id,
                                'cat_id' => $catid,
                                'others_txt' => $cat_others_txt[$key],
                                'branch_id' => $user_data['branch_id'],
                                'created_by' => $user_data['user_id'],
                                'created_on' => date('Y-m-d H:i:s'),
                            );
                            $lst_item_id = $this->services_model->insert_item($item);

                        } else if(count($ser_item[$key]) && $ser_item[$key][0] != '') {
                                foreach ($ser_item[$key] as $itemkey => $itm_id) {
                                    $item_othertext = ($item_other[$key][$itm_id]==1) ? $item_others_txt[$key][0] : '';
                                    $item = array(
                                        'service_id' => $last_id,
                                        'cat_id' => $catid,
                                        'item_id' => $itm_id,
                                        'others_txt' => $item_othertext,
                                        'branch_id' => $user_data['branch_id'],
                                        'created_by' => $user_data['user_id'],
                                        'created_on' => date('Y-m-d H:i:s'),
                                    );
                                    $lst_item_id = $this->services_model->insert_item($item);
                                }
                            
                        } 
                        
                    }
                }
		$_SESSION['success'] = 'Service added successfully';
                
        } else {
            $_SESSION['error'] = 'Service not added!';
        }

        redirect('services/add', 'refresh');
    }
    
    public function edit($id = '') {

        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['service'] = $this->services_model->get_by_id($id);
	$data['service_item'] = $this->services_model->get_item_by_id($id);
        $data['customer_list'] = $this->services_model->customer_list();
        $data['user_list'] = $this->services_model->user_list();
        $data['category_list'] = $this->services_model->service_category();
        $data['company'] = $this->company_model->get_company();
        
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' . $data['service']->id;
        
        $this->render($data, 'services/edit');
    }
    
     public function update($id = '') {
        
        if (empty($id)) {
            show_400_error();
        }
        
        $user_data = get_user_data();
        
        $cust_id = $this->input->post('customer_id');
        $customer_name = ($cust_id != '' && $cust_id=='0') ? $this->input->post('customer_name') : $this->input->post('customer_name_txt');
        $estimated_delivery_date = (!empty($this->input->post('estimated_delivery_date'))) ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('estimated_delivery_date')))) : '';
        $uploaded_data = '';
        if (!empty($_FILES['upload_photo']['name'])) {
            $uploaded_data = uploads(array('upload_path' => 'service', 'field' => 'upload_photo'));
        }
        
        $servicedata = array(            
            'customer_id' => $cust_id,
            'customer_name' => $customer_name,
            'contact_person' => $this->input->post('contact_person'),
            'contact_number' => $this->input->post('contact_number'),
            'email_address' => $this->input->post('email_address'),
            'engineer_id' => $this->input->post('engineer_id'),
            'guessing_problem' => $this->input->post('guessing_problem'),
            'estimated_price_quoted' => $this->input->post('estimated_price_quoted'),
            'material_name' => $this->input->post('material_name'),
            'material_type' => $this->input->post('material_type'),
            'problem_desc' => $this->input->post('problem_desc'),
            'received_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('received_date')))),
            'physical_condition' => $this->input->post('physical_condition'),
            'remarks' => $this->input->post('remarks'),
            'service_status' => $this->input->post('service_status'),
            'branch_id' => $user_data['branch_id'],
            'updated_by' => $user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s'),
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        
         if ($estimated_delivery_date != "") :
            $servicedata['estimated_delivery_date'] = $estimated_delivery_date;
        endif;
        
        if (!empty($uploaded_data['upload_photo'])) :
            $servicedata['upload_photo'] = $uploaded_data['upload_photo']['file_path_str'];
        endif;

        $affected_rows = $this->services_model->update($id, $servicedata);
        
        if ($affected_rows > 0) {
           
            $inactive_rows = $this->services_model->inactive_exc_items($id);
            
            /*** Service Item ***/
            $cat_ids = $this->input->post('cat_ids');
            $cat_other = $this->input->post('cat_other');
            $item_other = $this->input->post('item_other');
            $ser_item = $this->input->post('ser_item');
            $item_others_txt = $this->input->post('item_others_txt');
            $cat_others_txt = $this->input->post('cat_others_txt');

            if (!empty($cat_ids)) {
                foreach ($cat_ids as $key => $catid) {
                    $item_id = 0;
                    if($cat_other[$key]==1 && $cat_others_txt[$key] != "") {
                        $ser_itm_data = $this->services_model->service_item_check($id,$catid,'');
                        if(count($ser_itm_data)>0) {
                            $item = array(
                                'status' => 1,
                                'others_txt' => $cat_others_txt[$key],
                                'branch_id' => $user_data['branch_id'],
                                'updated_by' => $user_data['user_id'],
                                'updated_on' => date('Y-m-d H:i:s'),
                            );
                            $lst_item_id = $this->services_model->update_item($item, $ser_itm_data[0]['id']);
                        } else {
                            $item = array(
                                'service_id' => $id,
                                'cat_id' => $catid,
                                'others_txt' => $cat_others_txt[$key],
                                'branch_id' => $user_data['branch_id'],
                                'created_by' => $user_data['user_id'],
                                'created_on' => date('Y-m-d H:i:s'),
                            );
                            $lst_item_id = $this->services_model->insert_item($item);
                        }

                    } else if(count($ser_item[$key]) && $ser_item[$key][0] != '') {
                            foreach ($ser_item[$key] as $itemkey => $itm_id) {
                                $item_othertext = ($item_other[$key][$itm_id]==1) ? $item_others_txt[$key][0] : '';
                                
                                $ser_itm_data = $this->services_model->service_item_check($id,$catid,$itm_id);
                                if(count($ser_itm_data)>0) {
                                    $item = array(
                                        'status' => 1,
                                        'others_txt' => $item_othertext,
                                        'branch_id' => $user_data['branch_id'],
                                        'updated_by' => $user_data['user_id'],
                                        'updated_on' => date('Y-m-d H:i:s'),
                                    );
                                    $lst_item_id = $this->services_model->update_item($item, $ser_itm_data[0]['id']);
                                } else {
                                    $item = array(
                                        'service_id' => $id,
                                        'cat_id' => $catid,
                                        'item_id' => $itm_id,
                                        'others_txt' => $item_othertext,
                                        'branch_id' => $user_data['branch_id'],
                                        'created_by' => $user_data['user_id'],
                                        'created_on' => date('Y-m-d H:i:s'),
                                    );
                                    $lst_item_id = $this->services_model->insert_item($item);
                                }
                                
                            }

                    } 

                }
            }

            $_SESSION['success'] = 'Service Updated Successfully';
           
        } else {
            $_SESSION['error'] = 'Service not updated!';
        }

        redirect('services/edit/' . $id, 'refresh');
    }
    
    public function update_rack_num($id) {
        $user_data = get_user_data();
        $servicedata = array(            
            'rack_number' => $this->input->post('rack_number_txt'),
            'service_status' => $this->input->post('service_status'),
            'branch_id' => $user_data['branch_id'],
            'updated_by' => $user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s'),
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        $affected_rows = $this->services_model->update($id, $servicedata);
        
        if ($affected_rows > 0) {
            $_SESSION['success'] = 'Rack Number Updated Successfully';
        } else {
            $_SESSION['error'] = 'Rack Number not updated!';
        }
        
        redirect('services', 'refresh');
    }

    public function view($serv_id = '') {
        if (empty($serv_id)) {
            show_400_error();
        }
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['service_pdf'] = base_url() . 'services/pdf/' . $serv_id;
        $data['service'] = $this->services_model->get_by_id($serv_id);
	$data['service_item'] = $this->services_model->get_item_by_id($serv_id);
        $data['company'] = $this->company_model->get_company();
		
        $this->render($data, 'services/view');
    }
    
    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }
        $user_data = get_user_data();
                
        $service = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $user_data['user_id']
        );
        
        if ($this->services_model->update($id, $service) > 0){
            $_SESSION['success'] = 'Service deleted successfully';
            $this->services_model->delete_exc_items($id);
        }else{
            $_SESSION['error'] = 'Service not deleted!';
        }
        redirect('services', 'refresh');
    }
    
    public function pdf($id) {
        $data = array();
        $user_data = get_user_data();
        $data['service'] = $this->services_model->get_by_id($id);
	$data['service_item'] = $this->services_model->get_item_by_id($id);
        //$data['address'] = $this->purchase_order_model->get_current_address($user_data['branch_id']);
        $data['user_data'] = get_user_data();
        $data['branch'] = $this->company_model->branch_by_id($data['service']->branch_id);
	$data['branches'] = $this->company_model->branch_list_active();
        $data['company'] = $company = $this->company_model->get_company();
        $filename = 'PDF_' . $data['service']->jobsheet_number;
        $filename = strtoupper($filename);
        
        $header = $this->load->view('services/pdf_header', $data, TRUE);//'<div></div>';
        $content = $this->load->view('services/pdf', $data, TRUE);
        $footer = $this->load->view('services/pdf_footer', $data, TRUE);

        #echo $header.$content.$footer;exit;
        $this->load->helper(array('My_Pdf'));
        delivery_order_pdf($header, $content, $footer, $filename);        
        exit(0);
    }
    

}

?>