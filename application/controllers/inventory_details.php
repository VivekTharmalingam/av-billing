<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inventory_Details extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('inventory_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->library('upload');
    }

    public function index() {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = base_url() . 'inventory_details/view' . '/';
        $data['form_action'] = base_url() . 'inventory_details';
		
        $branch_id = $this->user_data['branch_id'];
        $brand_id = $this->input->post('brand_id');
		$category_id = $this->input->post('category_id');
		$sub_category_id = $this->input->post('sub_category_id');
        
		if ($this->input->post('reset_btn')) {
            $brand_id = '';
			$category_id = '';
			$sub_category_id = '';
        }
		else if (!empty($this->input->post('submit'))) {
			$branch_id = $this->input->post('branch_id');
		}
		
        $data['inventory'] = $this->inventory_model->list_search_all($branch_id, $brand_id, $category_id, $sub_category_id);
		$data['branch_id'] = $branch_id;
		$data['brand_id'] = $brand_id;
		$data['category_id'] = $category_id;
		$data['sub_category_id'] = $sub_category_id;
		$data['branch'] = $this->inventory_model->list_branch();
        $data['brand'] = $this->inventory_model->list_brand();
		$data['item_category'] = $this->product_model->item_category();
        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Inventory Details Reports';
            $head['setting'] = $this->company_model->get_company();
            $filename = 'Inventory_Details_Report_' . current_date();
            // Load Views
            $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
            $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
            $header = '<div></div>';
            $content = $this->load->view('inventory_details/pdf_report', $data, TRUE);
            $footer = '<div></div>';
            $this->load->helper(array('My_Pdf'));
			#echo $header . $content . $footer;exit
            create_pdf($header, $content, $footer, $filename);
            exit(0);
        } elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $thead = array(0 => array('field_name' => 'branch_name','thead' => 'Branch Name'), 
                    1 => array('field_name' => 'pdt_code','thead' => 'Item Code') , 
                    2 => array('field_name' => 'cat_name','thead' => 'Brand Name') , 
                    3 => array('field_name' => 'pdt_name','thead' => 'Item Description'),
                    4 => array('field_name' => 'available_qty','thead' => 'Quantity'),
                    5 => array('field_name' => 'selling_price','thead' => 'Price'),
                    6 => array('field_name' => '','thead' => 'Total', 'op' => '*', 'field_names' => 'available_qty,selling_price'),
             );
            $tbody = $data['inventory']; 
            $filename = 'Inventory_Report';
            $this->load->helper(array('my_excel_helper'));
            create_excel($thead, $tbody, $filename);
            //exit(0);
            //$this->load->view('inventory_details/excel_report', $data);
        } else {
            $this->render($data, 'inventory_details/list');
        }
    }

    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['product'] = $this->inventory_model->get_product_by_id($id);
        $this->render($data, 'inventory_details/view');
    }

}

?>