<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Update_Item extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('company_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->model('category_model', '', TRUE);
        $this->load->library('upload');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['company'] = $this->company_model->get_company();
        $data['item_category'] = $this->product_model->item_category();
        $data['sub_category'] = $this->product_model->active_sub_category_list();
        $data['category'] = $this->product_model->active_category();
        /* echo '<pre>';
          print_r($data['category']);
          print_r($data['item_category']);
          print_r($data['sub_category']);
          echo '</pre>';
          exit; */
        $actions = $this->actions();
        $data['form_action'] = $actions['update'];
        $this->render($data, 'update_item/edit');
    }

    public function update() {
        $sub_category = $this->input->post('sub_category');
        $item_id = $this->input->post('item_id');
        $buying_price = $this->input->post('item_cost');
        $selling_price = $this->input->post('item_selling_price');
        $item_cat = $this->input->post('item_cat');
        $search_brand = $this->input->post('search_brand');
        $brand = $this->input->post('brand');
        $category = $this->input->post('category');
        $_SESSION['updated_page_no'] = $this->input->post('page_no');
        $_SESSION['itemcategory'] = $item_cat;
        $_SESSION['brandvalues'] = $search_brand;

        $_SESSION['pagecountnunbers'] = $this->input->post('page_count_numbers');
        $_SESSION['all_files'] = $this->input->post('check_all');

        if (!empty($item_id)) {
            foreach ($item_id as $key => $item) {
                $update = array(
                    'sub_category' => $sub_category[$key],
                    'selling_price' => $selling_price[$key],
                    'buying_price' => $buying_price[$key],
                    'bfr_gst_buying_price' => $buying_price[$key],
                    'item_category' => $category[$key],
                    'cat_id' => $brand[$key],
                    'updated_by' => $this->user_data['user_id'],
                    'updated_on' => date('Y-m-d H:i:s')
                );
                $upd = $this->product_model->update($item, $update);
                if ($upd > 0) {
                    if (!empty($item)) {
                        $this->update_price_log($item, $buying_price[$key], $selling_price[$key], 'From Item Price Update');
                    }
                }
            }
        }
        redirect('update_item', 'refresh');
    }

    public function get_record_page_count() {
        $cmd = $this->input->post('cmd');
        if ($cmd == 'all_pages') {
            $countval = $this->product_model->get_product_page_count();
            $count_by_page = ceil($countval->pro_id / 100);
            $page_count = array();
            $rows = array();
//            for ($i = 1; $i <= $count_by_page; $i++) {
//                $page_count[] = $i;
//            }
            $rows['page_count'] = $count_by_page;
        } elseif ($cmd == 'category_pages') {
            $item_cat = $this->input->post('item_cat');
            $brand = $this->input->post('brand');
            $countval = $this->product_model->get_product_page_count_by_category($item_cat, $brand);
            if ($countval->pro_id < 100) {
                $count_by_page = ceil($countval->pro_id / 100);
                $rows['counts'] = "yes";
                $rows['page_count'] = $count_by_page;
            } else {
                $rows['counts'] = "no";
                $rows['page_count'] = $count_by_page;
            }
        }
        echo json_encode($rows);
    }

    public function update_price_log($pid, $bprice, $sprice, $tit) {
        $last = $this->product_model->get_product_last_price_log($pid);
        $last_id = '';
        if (($last->buying_price != $bprice) || ($last->selling_price != $sprice)) {
            $insert = array(
                'product_id' => $pid,
                'buying_price' => $bprice,
                'selling_price' => $sprice,
                'updated_from' => $tit,
                'brn_id' => $this->user_data['branch_id'],
                'created_by' => $this->user_data['user_id'],
                'created_on' => date('Y-m-d H:i:s'),
                'ip_address' => $_SERVER['REMOTE_ADDR']
            );
            $last_id = $this->product_model->insert_cost_log($insert);
        }
        return $last_id;
    }

    public function get_category_items() {
        $limit = 100;
        $brand = $this->input->post('brand');
        $page_no = $this->input->post('page_no');
        $cat = $this->input->post('cat_id');
        $all_items = $this->input->post('all_items');
        $data['items'] = $this->product_model->get_category_items($cat, $all_items, $brand, $page_no, $limit);
//        ($this->db->last_query());

        $this->ajax_response($data);
    }

}

?>