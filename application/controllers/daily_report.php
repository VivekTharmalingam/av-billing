<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class daily_report extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('sales_order_model', '', TRUE);
        $this->load->model('customer_model', '', TRUE);
    }
	
    public function index() {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = base_url() . 'invoice/view' . '/';
        $data['form_action'] = base_url() . 'daily_report';
        
		$branch = (!empty($this->input->post('brn_name'))) ? $this->input->post('brn_name') : '';
		$search_date = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : date('d/m/Y');
		
		if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'Reset')) {
			$branch = '';
			$search_date = date('d/m/Y');
		}
		
		$data['branch'] = $branch;
		$data['search_date'] = $search_date;
		$from = convert_text_date(trim($search_date));
		$to = $from;
        $data['from_date'] = $from;
        $data['to_date'] = $to;
        $data['invoice'] = $this->sales_order_model->list_daily_report_summary($from, $to, $branch);
        $data['btotal'] = $this->sales_order_model->daily_report_breakdown($from, $to, $branch);
        $data['bpaid'] = $this->sales_order_model->daily_report_breakdown_paid($from, $to, $branch);
        $data['paT'] = $this->sales_order_model->get_static_pay_type($from, $to, $branch);  
        
        $data['customers'] = $this->customer_model->list_active();
        $data['branches'] = $this->company_model->active_user_branches();
		$data['permission'] = $this->permission;
		$data['setting'] = $head['setting'] = $this->company_model->get_company();
        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Daily Report';
			$head['user_data'] = $this->user_data;
            $filename = 'Daily_Report_' . current_date();
            // Load Views
            $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
            $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
            $header = '<div></div>';
            $content = $this->load->view('daily_report/pdf_report', $data, TRUE);
            $footer = '';
			
            $this->load->helper(array('My_Pdf'));
            create_pdf($header, $content, $footer, $filename);
            exit(0);
        }
		elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $this->load->view('daily_report/excel_report', $data);
        }
		else {
            $this->render($data, 'daily_report/list');
        }
    }

}

?>