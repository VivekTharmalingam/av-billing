<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile extends REF_Controller {
    
    function __contruct() {        
        parent::__construct();     
    }
    
    function index() {
        $user_data = get_user_data();     
        
        $data = array(
            'form_action'=> base_url() . $user_data['controller'] . '/update/'.$user_data['user_id'],
            'success'   => $this->session->flashdata( 'success'),
            'error'     => $this->session->flashdata( 'error'),
        );
        
        $this->render( $data, 'profile/index' );
    }
    
    public function update($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        
        $user_data = get_user_data();
        $user = $this->user_model->get_by_id( $id );
        
        $image = $this->input->post('image');
        if( empty( $image ) ) :
            $image = $this->input->post('hidden-file');
            if( empty( $image ) && !empty( $user->image ) ) :
                // Remove prev. file
                unlink( FCPATH . UPLOADS . $user->image );
            
                
            endif;
            
            $uploaded_data = uploads( array( 'upload_path' => 'users', 'field' => 'file' ) );
            
        endif;
        
        
        //$uploaded_data = uploads();
        //echo "<pre>Uploaded:: "; print_r( $uploaded_data ); die; 
        
        $user = array(
            'name' => $this->input->post('name'),
            'user_name' => $this->input->post('user_name'),
            'email' => $this->input->post('email'),
            'image' => $image,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by'    => $user_data['user_id'],
        );
        
        if( !empty( $uploaded_data['file'] ) ) :
            $user['image'] = $uploaded_data['file']['file_path_str'];
        endif;
        
        if( $this->user_model->update( $id, $user ) > 0 ) :
            $this->session->set_flashdata( 'success', 'Profile is updated successfully' );
        else : 
            $this->session->set_flashdata( 'error', 'Profile is not updated' );
        endif;
        
        
        redirect('profile', 'refresh');
    }
    
}

