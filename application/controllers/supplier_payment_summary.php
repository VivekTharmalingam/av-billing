<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Supplier_Payment_Summary extends REF_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model('purchase_order_model', '', TRUE);
		$this->load->model('supplier_model', '', TRUE);
    }
	
    public function index() {
        $data = array();
		$data1 = '';
		$head = array();
        $data['success']=$this->data['success'];  
        $data['error']=$this->data['error'];       
        $actions = $this->actions();
        $data['view_link']  = base_url() . 'supplier_payment_summary/view' . '/';
		$data['form_action'] = base_url() . 'supplier_payment_summary';
		
        $from = '';
        $to = '';
        $data['search_date'] = '';
			
        if($this->input->post('search_date') != ''){				
            $data['search_date'] = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : '';
            $search_date = explode('-', $this->input->post('search_date'));
            $from = trim($search_date[0]);
            $to = trim($search_date[1]);

            $from = explode('/',$from);           
            $from = $from[2].'-'.$from[1].'-'.$from[0];

            $to = explode('/',$to);
            $to = $to[2].'-'.$to[1].'-'.$to[0];
        }
				
        $data['from_date'] = $from;
        $data['to_date'] = $to;
		$data['supplier_id'] = $this->input->post('supplier_id');
                $data['gst'] = $this->input->post('gst');
		$data['supplier'] = $this->supplier_model->list_active();
		$data['purchase'] = $this->purchase_order_model->list_search_all_result($from, $to,$data['supplier_id'],$data['gst']);
		#echo $this->db->last_query();exit;
		if($this->input->post('reset_btn')){
			$data['search_date']='';
			$data['supplier_id'] ='';
                        $data['gst'] ='';
			$data['purchase'] = $this->purchase_order_model->list_search_all_result();
        }		
				
		if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Supplier Payment Report';
		    $head['setting'] = $this->company_model->get_company();
			$filename = 'Supplier_Payment_Report_' . current_date();
				
			// Load Views
			$data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
			$data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
			$header = '<div></div>';
			$content = $this->load->view('supplier_payment_summary/pdf_report', $data, TRUE);			
			$footer = '<div></div>';
			$this->load->helper(array('My_Pdf'));
					
			create_pdf($header, $content, $footer, $filename);
			exit(0);
			
		} elseif(((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel')) ){
			$this->load->view('supplier_payment_summary/excel_report', $data);
			
		}else {
			$this->render($data, 'supplier_payment_summary/list');
		}
		
    }
	
    public function view($id = '',$from_date='',$to_date='') {
        if (empty($id)) {
            show_400_error();
        }        
        $data = array();
		$data['id'] = $id;
		$data['from_date'] = $from_date;
		$data['to_date'] = $to_date;
		$data['supplier']   = $this->purchase_order_model->get_supplier_by_id($id, $from_date, $to_date);
        $data['purchase']   = $this->purchase_order_model->get_purchase_by_id($id, $from_date, $to_date);
        
        $this->render($data, 'supplier_payment_summary/view');
    }
	
	public function pdf($id = '',$from_date='',$to_date='') {
        if (empty($id)) {
            show_400_error();
        }        
        $data = array();
		$data['id'] = $id;
		$data['from_date'] = $from_date;
		$data['to_date'] = $to_date;
		$data['supplier']   = $this->purchase_order_model->get_supplier_by_id($id, $from_date, $to_date);
                $data['purchase']   = $this->purchase_order_model->get_purchase_by_id($id, $from_date, $to_date);	
			
		$head['title'] = 'Supplier Payment Report';
		$head['setting'] = $this->company_model->get_company();
		$filename = 'Supplier_Payment_Report_' . current_date();
		
		// Load Views
		$data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
		$data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
		$header = '<div></div>';
		$content = $this->load->view('supplier_payment_summary/view_pdf_report', $data, TRUE);			
		$footer = '<div></div>';//$this->load->view('templates/pdf/pdf_footer', '', TRUE);
		
		$this->load->helper(array('My_Pdf'));
		create_pdf($header, $content, $footer, $filename);
		exit(0);		
    }
	
}
?>