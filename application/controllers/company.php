<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Company extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('company_model', '', TRUE);
        $this->load->library('upload');
    }

    public function index($id=NULL) {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $user_data = get_user_data();
        //error_log(print_r($user_data,1));
        $data['add_btn'] = 'Edit Company';
        $data['company'] = $this->company_model->get_company();
        $data['active_bank'] = $this->company_model->get_active_bank();
        $data['logo'] = $this->company_model->get_company()->comp_logo;
        $data['branches'] = $this->company_model->get_branch_by_id($data['company']->id);
        //echo $this->db->last_query();
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' . $data['company']->id;
        $this->render($data, 'company/edit');
    }

    public function update($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $user_data = get_user_data();
        $uploaded_data = '';
        if (!empty($_FILES['logo']['name'])) {
            $uploaded_data = uploads(array('upload_path' => 'logo', 'field' => 'logo'));
        }
        $uploaded_details = '';
        if (!empty($_FILES['favicon']['name'])) {
            $uploaded_details = uploads(array('upload_path' => 'favicon', 'field' => 'favicon'));
        }
        $uploaded_login = '';
        if (!empty($_FILES['login_logo']['name'])) {
            $uploaded_login = uploads(array('upload_path' => 'login_logo', 'field' => 'login_logo'));
        }
        $uploaded_inner = '';
        if (!empty($_FILES['inner_logo']['name'])) {
            $uploaded_inner = uploads(array('upload_path' => 'inner_logo', 'field' => 'inner_logo'));
        }
        $short_name = strtoupper(substr($this->input->post('company_name'), 0, 3));
        $prefix_wt_sub = CODE_COMP . $short_name;
        $code = $this->auto_generation_code(TBL_COMP, $prefix_wt_sub, '', 3, $id);
        $update = array(
            'comp_code' => $code,
            'comp_name' => $this->input->post('company_name'),
            'comp_reg_no' => $this->input->post('reg_no'),
            'comp_gst_no' => $this->input->post('gst_no'),
            'comp_address' => $this->input->post('address'),
            'comp_phone' => $this->input->post('phone_no'),
            'comp_pin_code' => $this->input->post('pin_code'),
            'comp_fax' => $this->input->post('fax'),
            'comp_email' => $this->input->post('email'),
            'comp_website' => $this->input->post('website'),
            'comp_vat' => $this->input->post('tax'),
            'status' => 1,
            'bank_acc_id' => $this->input->post('bank_acc_id'),
            'updated_by' => $this->user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s'),
            'branch_id' => $this->user_data['branch_id'],
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        if (!empty($uploaded_data['logo'])) :
            $update['comp_logo'] = $uploaded_data['logo']['file_path_str'];
        endif;
        
        if (!empty($uploaded_details['favicon'])) :
            $update['comp_favicon'] = $uploaded_details['favicon']['file_path_str'];
        endif;
        
        if (!empty($uploaded_login['login_logo'])) :
            $update['login_logo'] = $uploaded_login['login_logo']['file_path_str'];
        endif;
        
        if (!empty($uploaded_inner['inner_logo'])) :
            $update['inner_logo'] = $uploaded_inner['inner_logo']['file_path_str'];
        endif;
        
        if ($this->company_model->update($id, $update)) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'index', $id, '', '');
            $_SESSION['success'] = 'Company updated successfully';
            $edit_id = $this->input->post('brn_id');
            $old_id = $this->input->post('edit_brn_id');
            $del_id = array_diff($old_id, $edit_id);
            //update old items 	
            if (!empty(array_filter($edit_id))) {
                $brn_name = $this->input->post('brn_name');
                $address = $this->input->post('brn_address');
                $status = $this->input->post('brn_status');
                $val = array();
                foreach ($edit_id as $key => $update_id) {
                    $val['company_id'] = $id;
                    $val['branch_name'] = $brn_name[$key];
                    $val['address'] = $address[$key];
                    $val['status'] = $status[$key];
                    $val['bank_acc_id'] = $this->input->post('bank_acc_id');
                    $val['created_on'] = date('Y-m-d H:i:s');
                    $val['branch_id'] = $this->user_data['branch_id'];
                    $val['created_by'] = $this->user_data['user_id'];
                    $this->company_model->update_branches($update_id, $val);
                }
            }
            foreach ($del_id as $key => $brnId) {
				$update = array(
					'status' => 10,
					'updated_by' => $this->user_data['user_id'],
					'updated_on' => date('Y-m-d H:i:s')
				);
				
				$this->company_model->update_branches($brnId, $update);
                //$this->company_model->row_delete($brnId);
            }
			
            //Insert new Items
            if (!empty($this->input->post('new_brn_name'))) {
                $brn_name = $this->input->post('new_brn_name');
                $address = $this->input->post('new_brn_address');
                $status = $this->input->post('new_brn_status');
                $val = array();
                foreach ($brn_name as $key => $value) {
                    $val['company_id'] = $id;
                    $val['branch_name'] = $brn_name[$key];
                    $val['address'] = $address[$key];
                    $val['status'] = $status[$key];
                    $val['bank_acc_id'] = $this->input->post('bank_acc_id');
                    $val['created_on'] = date('Y-m-d H:i:s');
                    $val['branch_id'] = $this->user_data['branch_id'];
                    $val['created_by'] = $this->user_data['user_id'];
                    $last_brn_id = $this->company_model->insert_branches($val);
                }
            }
        } else
            $_SESSION['error'] = 'Company not updated!';

        redirect('company', 'refresh');
    }

}

?>