<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');
class invoice_reports extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('sales_order_model', '', TRUE);
        $this->load->model('customer_model', '', TRUE);
    }

    public function index($unpaid_sts = '') {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = base_url() . 'invoice/view' . '/';
        $data['form_action'] = base_url() . 'invoice_reports';
        
		$customer = (!empty($this->input->post('cus_name'))) ? $this->input->post('cus_name') : '';
		$issued_by = (!empty($this->input->post('issued_by'))) ? $this->input->post('issued_by') : '';
		$payment_status = (!empty($this->input->post('payment_status'))) ? $this->input->post('payment_status') : '';
		$search_date = date('01/m/Y') . ' - ' . date('t/m/Y');
		$branch_id = $this->user_data['branch_id'];
		$generated_from = (!empty($this->input->post('generated_from'))) ? $this->input->post('generated_from') : '';
		
		if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'Reset')) {
			$customer = '';
			$issued_by = '';
			$payment_status = '';
			$search_date = date('01/m/Y') . ' - ' . date('t/m/Y');
			$branch_id = '';
			$generated_from = '';
		}
		else if(!empty($this->input->post('submit'))) {
			$search_date = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : '';
			$branch_id = $this->input->post('branch_id');
		}
		
		$search_date_arr = explode('-', $search_date);
		$from = convert_text_date(trim($search_date_arr[0]));
		$to = convert_text_date(trim($search_date_arr[1]));
		$data['branch_id'] = $branch_id;
		$data['generated_from'] = $generated_from;
        $data['search_date'] = $search_date;
        $data['from_date'] = $from;
        $data['to_date'] = $to;
        $data['custom'] = $customer;
        $data['issued_by'] = $issued_by;
        if($unpaid_sts == '') {
			$data['pay_sts'] = $payment_status; 
        }
		else {
			$data['pay_sts'] = $unpaid_sts;    
        }
		
        $data['users'] = $this->user_model->active_user_list();
        $data['invoices'] = $this->sales_order_model->list_invoice_summary($from, $to, $customer, $issued_by, $data['pay_sts'], $branch_id, $generated_from);
		/*echo '<pre>';
		print_r($data['invoices']);
		echo '</pre>';
		echo $this->input->post('submit');echo $this->db->last_query();exit;*/
        $data['customers'] = $this->customer_model->list_active();
		$data['branches'] = $this->company_model->active_user_branches();
		$data['permission'] = $this->permission;
        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Invoice Summary Reports';
            $head['setting'] = $this->company_model->get_company();
            $filename = 'Invoice_Report_' . current_date();
            // Load Views
            $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
            $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
            $header = '<div></div>';
            $content = $this->load->view('invoice_report/pdf_report', $data, TRUE);
            $footer = '<div></div>';//$this->load->view('templates/pdf/pdf_footer', '', TRUE);
            $this->load->helper(array('My_Pdf'));
            create_pdf($header, $content, $footer, $filename);
            exit(0);
        }
		elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $this->load->view('invoice_report/excel_report', $data);
        }
		else {
            $this->render($data, 'invoice_report/list');
        }
    }
}
?>