<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Exchange_Order_Report extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('exchange_order_model', '', TRUE);
        $this->load->model('purchase_order_model', '', TRUE);
    }

    public function index() {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['form_action'] = base_url() . 'exchange_order_report';
        $data['view_link']  = base_url() . 'exchange_order/view/';
        
        $data['all_suppliers'] = $this->purchase_order_model->get_all_suppliers();
        $supplier_id = (!empty($this->input->post('supplier_id'))) ? $this->input->post('supplier_id') : '';
        $search_date = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : date('01/m/Y') . ' - ' . date('t/m/Y');

        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'Reset')) {
            $supplier_id = '';
            $search_date = date('01/m/Y') . ' - ' . date('t/m/Y');
        }

        if ((!empty($this->input->post('submit'))) && (empty($this->input->post('search_date')))) {
            $search_date = '';
            $from = '';
            $to = '';
        } else {
            $search_date_arr = explode('-', $search_date);
            $from = convert_text_date(trim($search_date_arr[0]));
            $to = convert_text_date(trim($search_date_arr[1]));
        }

        $data['search_date'] = $search_date;
        $data['from_date'] = $from;
        $data['to_date'] = $to;
        $data['supplier_id'] = $supplier_id;
        
        $data['exchange_order_rept'] = $this->exchange_order_model->get_exchange_order_report($from, $to, $supplier_id);
        
        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Exchange Order Reports';
            $head['setting'] = $this->company_model->get_company();
            $filename = 'Exchange_Order_Report_' . current_date();
            // Load Views
            $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
            $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
            $header = '<div></div>';
            $content = $this->load->view('exchange_order_report/pdf_report', $data, TRUE);
            $footer = '<div></div>';
            $this->load->helper(array('My_Pdf'));
            create_pdf($header, $content, $footer, $filename);
            exit(0);
        } elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $this->load->view('exchange_order_report/excel_report', $data);
        } else {
            $this->render($data, 'exchange_order_report/list');
        }
    }

}

?>