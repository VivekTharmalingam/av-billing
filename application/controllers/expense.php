<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Expense extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('expense_model', '', TRUE);
        $this->load->library('upload');
        $this->load->library('Datatables');
    }
    
    public function index() {
        $data = array();
        $data['success']=$this->data['success'];  
        $data['error']=$this->data['error'];       
        $actions = $this->actions();
        $data['add_btn']    = 'Create Expense';
        $data['view_link']  = $actions['view'] . '/';
        $data['add_link']   = $actions['add'];
        $data['edit_link']  = $actions['edit'] . '/';
        $data['delete_link']= $actions['delete'] . '/';
        $data['expense']      = $this->expense_model->list_all();
		$data['exp_type']      = $this->expense_model->list_exp_type();
	    $this->render($data, 'expense/list');
    }
	
	function datatable() {
		$user_data = get_user_data();
		$this->datatables->select('e.id AS eId,e.pay_to AS emp,e.amount AS net,DATE_FORMAT(e.expense_date, "%d/%m/%Y") as date,CASE e.status WHEN 1 THEN "Paid" WHEN 2 THEN "Unpaid" ELSE "" END as status_str,t.exp_type_name AS type', FALSE)
			->where('e.status != 10 AND e.branch_id IN ("' . $this->user_data['branch_id'] . '")')
			->join(TBL_EXP_TYPE . ' as t', 't.id = e.exp_type', 'left')
			->add_column('Actions', $this->get_buttons('$1'), 'eId')
			->from(TBL_EXP . ' AS e');
		
		echo $this->datatables->generate();
    }
	
	function get_buttons($id) {

            $actions = $this->actions();

            $html = '<span class="actions">';            

            if (in_array(1, $this->permission)) {

                $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';

            }

            if (in_array(3, $this->permission)) {

                $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';

            }

            if (in_array(4, $this->permission)) {

                $html .='<a class="label btn btn-danger delete delete-confirm" href="#" data-confirm-content="You will not be able to recover expense!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Expense"><span>Delete</span></a>';

            }

            $html.='</span>';

            return $html;

        }



        public function add() {

        $data = array();

        $data['success']=$this->data['success'];  

        $data['error']=$this->data['error'];        

        $actions = $this->actions();

        $data['form_action'] = $actions['insert'];

        $data['list_link']   =  $actions['index']; 

	    $data['exp_type']      = $this->expense_model->list_exp_type();

        $this->render($data, 'expense/add');

    }

    

    public function insert() {

        //echo '<pre>';print_r($_REQUEST);print_r($_FILES);

        //exit;

		

        $user_data = get_user_data(); 

       

		$issue_date = (!empty($this->input->post('expense_date'))) ? str_replace('/','-',$this->input->post('expense_date')) : '';		

		$exp_date = date('Y-m-d',strtotime($issue_date));	

	

        $prefix_wt_sub=CODE_EXP;

        $code = $this->auto_generation_code(TBL_EXP, $prefix_wt_sub, '', 3, '');

		

        $insert = array(

            'expense_code' => $code, 

            'expense_date'=>$exp_date,			

			'pay_to'=>$this->input->post('pay_to'), 

			'exp_type'=>$this->input->post('exp_type'), 

			'amount' => $this->input->post('amt'), 

			'desc' => $this->input->post('desc'), 					

			'status'=>$this->input->post('status'),             

            'branch_id' => $this->user_data['branch_id'],   

            'created_by' => $this->user_data['user_id'],

            'created_on' => date('Y-m-d H:i:s')

        ); 

	

        $last_id =$this->expense_model->insert($insert);

       	

        if ( $last_id > 0) {            
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, $code, '');
            $_SESSION['success'] = 'Expense added successfully';			

        } else { 

            $_SESSION['error'] = 'Expense not added!';        
        }    
        redirect('expense/add', 'refresh');

    }

    

    public function edit($id = ''){

        if (empty($id)) {

            show_400_error();

        }

        $data['success']=$this->data['success'];  

        $data['error']=$this->data['error'];

        

        $data['expense'] = $this->expense_model->get_by_id($id);		

		$data['exp_type'] = $this->expense_model->list_exp_type();

        $actions = $this->actions();

        $data['form_action'] = $actions['update'] . '/' . $data['expense']->id;        

        $this->render($data, 'expense/edit');

    }

    

    public function update($id = '') {    

			

        if (empty($id)) {

            show_400_error();

        }

        $user_data = get_user_data();  

       

		$issue_date = (!empty($this->input->post('expense_date'))) ? str_replace('/','-',$this->input->post('expense_date')) : '';		

		$exp_date = date('Y-m-d',strtotime($issue_date));	

		

        $update = array(

            'expense_date'=>$exp_date,			

			'pay_to'=>$this->input->post('pay_to'), 

			'exp_type'=>$this->input->post('exp_type'), 

			'amount' => $this->input->post('amt'), 

			'desc' => $this->input->post('desc'), 					

			'status'=>$this->input->post('status'),        

            'branch_id' => $this->user_data['branch_id'],   

            'updated_by' => $this->user_data['user_id'],

            'updated_on' => date('Y-m-d H:i:s')

        ); 

		

        if ($this->expense_model->update($id, $update) > 0) {
            $data_expense = $this->expense_model->get_by_id($id);
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $data_expense->expense_code, '');    

            $_SESSION['success'] = 'Expense updated successfully';

        } else {

            $_SESSION['error'] = 'Expense not updated!';
        }    
        redirect('expense/edit/' . $id, 'refresh');

    }

    

    public function view($id = '') {

        if (empty($id)) {

            show_400_error();

        }        

        $data = array();

        $data['expense'] = $this->expense_model->get_by_id($id);

		$data['exp_type'] = $this->expense_model->list_exp_type();

			

        $this->render($data, 'expense/view');

    }

    

    public function delete($id) {

        if (empty($id)) {

            show_400_error();

        }        

        $expense = array(

            'status' => 10,

            'updated_on' => date('Y-m-d H:i:s'),

            'updated_by' => $this->user_data['user_id']

        );        

        if ($this->expense_model->update($id, $expense) > 0) {
            $data_expense = $this->expense_model->get_by_id($id);
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $data_expense->expense_code, 1);    
            $_SESSION['success'] = 'Expense deleted successfully';

        } else {

            $_SESSION['error'] = 'Expense not deleted!';  
        }

        redirect('expense', 'refresh');

    }

	

}

?>