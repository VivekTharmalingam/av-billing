<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer_Payment_Summary extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('sales_order_model', '', TRUE);
        $this->load->model('customer_model', '', TRUE);
        $this->load->library('upload');
    }

    public function index() {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = base_url() . 'customer_payment_summary/view' . '/';
        $data['form_action'] = base_url() . 'customer_payment_summary';

        $from = '';
        $to = '';
        $data['search_date'] = '';

        if ($this->input->post('search_date') != '') {
            $data['search_date'] = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : '';
            $search_date = explode('-', $this->input->post('search_date'));
            $from = trim($search_date[0]);
            $to = trim($search_date[1]);

            $from = explode('/', $from);
            $from = $from[2] . '-' . $from[1] . '-' . $from[0];

            $to = explode('/', $to);
            $to = $to[2] . '-' . $to[1] . '-' . $to[0];
        }

        $data['from_date'] = $from;
        $data['to_date'] = $to;
        $data['customer_id'] = $this->input->post('cust_id');
        $data['customer'] = $this->customer_model->list_all();
        $data['sales'] = $this->sales_order_model->list_search_all($from, $to, $data['customer_id']);
		
//        echo $this->db->last_query();

        if ($this->input->post('reset_btn')) {
            $data['search_date'] = '';
            $data['customer_id'] = '';
            $data['sales'] = $this->sales_order_model->list_search_all();
        }
        //error_log($this->db->last_query());
        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Customer Payment Report';
            $head['setting'] = $this->company_model->get_company();
            $filename = 'Customer_Payment_Report_' . current_date();

            // Load Views
            $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
            $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
            $header = '<div></div>';
            $content = $this->load->view('customer_payment_summary/pdf_report', $data, TRUE);
            $footer = '<div></div>';//$this->load->view('templates/pdf/pdf_footer', '', TRUE);
            $this->load->helper(array('My_Pdf'));

            create_pdf($header, $content, $footer, $filename);
            exit(0);
        } elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $this->load->view('customer_payment_summary/excel_report', $data);
        } else {
            $this->render($data, 'customer_payment_summary/list');
        }
    }

    public function view($id = '', $from_date = '', $to_date = '') {
        $data = array();
        $data['id'] = $id;
        $data['from_date'] = $from_date;
        $data['to_date'] = $to_date;
        $data['customer'] = $this->sales_order_model->get_customer_by_id($id, $from_date, $to_date);
        $data['sales'] = $this->sales_order_model->get_sales_by_id($id, $from_date, $to_date);
        $data['inv_view'] = base_url() . 'invoice/view/' . $data['sales'][0]->id;
        $this->render($data, 'customer_payment_summary/view');
    }

    public function pdf($id = '', $from_date = '', $to_date = '') {
        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['id'] = $id;
        $data['from_date'] = $from_date;
        $data['to_date'] = $to_date;
        $data['customer'] = $this->sales_order_model->get_customer_by_id($id, $from_date, $to_date);
        $data['sales'] = $this->sales_order_model->get_sales_by_id($id, $from_date, $to_date);

        $head['title'] = 'Customer Payment Report';
        $head['setting'] = $this->company_model->get_company();
        $filename = 'Customer_Payment_Report_' . current_date();

        // Load Views
        $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
        $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
        $header = '<div></div>';
        $content = $this->load->view('customer_payment_summary/view_pdf_report', $data, TRUE);
        $footer = '<div></div>';//$this->load->view('templates/pdf/pdf_footer', '', TRUE);
        $this->load->helper(array('My_Pdf'));
        create_pdf($header, $content, $footer, $filename);
        exit(0);
    }

}

?>