<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Scan_Product extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('product_model', '', TRUE);
        $this->load->library('upload');
    }

//    public function index() {
//        $data = array();
//        $data['success'] = $this->data['success'];
//        $data['error'] = $this->data['error'];
//        $actions = $this->actions();
//        $data['add_btn'] = 'Create Product';
//        $data['view_link'] = $actions['view'] . '/';
//        $data['add_link'] = $actions['add'];
//        $data['edit_link'] = $actions['edit'] . '/';
//        $data['delete_link'] = $actions['delete'] . '/';
//        $data['product'] = $this->product_model->list_all();
//        $data['category'] = $this->product_model->category();
//        $this->render($data, 'product/list');
//    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['form_action'] = $actions['insert'];
        $data['list_link'] = $actions['index'];
        $data['category'] = $this->product_model->category();
        $this->render($data, 'product/scan_add');
    }

    public function insert() {
//        echo '<pre>';print_r($_REQUEST);die();            
        $user_data = get_user_data();   
        if($this->input->post('method')!='1'){
            $short_name = strtoupper(substr($this->input->post('pdt_desc'), 0, 3));
            $prefix_wt_sub = CODE_PDT . $short_name;
            $code = $this->auto_generation_code(TBL_PDT, $prefix_wt_sub, '', 3, '');
            $no = $this->input->post('pdt_code');
            if ($no == '') {
                $pdt_code = $code;
            } else {
                $pdt_code = $no;
            }
            $insert = array(
                'pdt_code' => $pdt_code,
                'pdt_name' => $this->input->post('pdt_desc'),
                'cat_id' => $this->input->post('cat_name'),
                'selling_price' => $this->input->post('sel_price'),
                'buying_price' => $this->input->post('buy_price'),
                'status' =>1,
                'bar_code' => $this->input->post('bar_code'),
                's_no' => $this->input->post('s_no'),
                'branch_id' => $this->user_data['branch_id'],
                'created_by' => $this->user_data['user_id'],
                'created_on' => date('Y-m-d H:i:s')
            );
            $last_id = $this->product_model->insert($insert);

            if ($last_id > 0) {                
                $brn_item = array(
                    'brn_id' => $user_data['branch_id'],
                    'product_id' => $last_id,
                    'available_qty' => $this->input->post('initial_quantity'),
                    'branch_id' => $user_data['branch_id'],
                    'created_by' => $user_data['user_id'],
                    'created_on' => date('Y-m-d H:i:s'),
                    'ip_address' => $_SERVER['REMOTE_ADDR']
                );
                $this->product_model->insert_brn_material($brn_item);
                
                $sc_item = array(
                    'branch_id' => $user_data['branch_id'],
                    'product_id' => $last_id,
                    'quantity' => $this->input->post('initial_quantity'),                    
                    'created_by' => $user_data['user_id'],
                    'created_on' => date('Y-m-d H:i:s'),
                    'ip_address' => $_SERVER['REMOTE_ADDR']
                );
                $this->product_model->insert_scan_product($sc_item);             
                
                $insert = array(
                    'product_id' => $last_id,
                    'buying_price' => $this->input->post('buy_price'),
                    'selling_price' => $this->input->post('sel_price'),
                    'updated_from' => 'From Product Master',
                    'brn_id' => $this->user_data['branch_id'],
                    'created_by' => $this->user_data['user_id'],
                    'created_on' => date('Y-m-d H:i:s'),
                    'ip_address' => $_SERVER['REMOTE_ADDR']
                );
                $this->product_model->insert_cost_log($insert);
            }
            if ($last_id > 0)
                $_SESSION['success'] = 'Scan Product added successfully';
            else
                $_SESSION['error'] = 'Scan Product not added!';
        }else{
        $qty = 0;
        if(!empty($this->input->post('new_quantity'))){
           $qty = $this->input->post('new_quantity'); 
        }
        $sc_item = array(
            'branch_id' => $user_data['branch_id'],
            'product_id' => $this->input->post('hidden_pdt_id'),
            'quantity' => $qty,                    
            'created_by' => $user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s'),
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        $this->product_model->insert_scan_product($sc_item);     
        
        $af_id = $this->product_model->update_branch_stock($user_data['branch_id'], $this->input->post('hidden_pdt_id'), $qty, '+');
         if ($af_id > 0)
                $_SESSION['success'] = 'Scan Item quantity updated successfully';
            else
                $_SESSION['error'] = 'Scan Item quantity not added!';
        }
        
        redirect('scan_product', 'refresh');
    }

}

?>