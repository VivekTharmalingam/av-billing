<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Purchase_Payments extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('purchase_payment_model', '', TRUE);
        $this->load->model('purchase_receive_model', '', TRUE);
        $this->load->model('supplier_model', '', TRUE);
        $this->load->model('payment_type_model', '', TRUE);
        $this->load->model('bank_model', '', TRUE);
        $this->load->library('Datatables');
    }
    
    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];        
        $actions = $this->actions();
        $data['add_btn']    = 'Create Qualifier';
        $data['view_link']  = $actions['view'] . '/';
        $data['add_link']   = $actions['add'];
        $data['edit_link']  = $actions['edit'] . '/';
        $data['purchase_payment'] = $this->purchase_payment_model->list_all();
        $this->render($data, 'purchase_payment/list');
    }

    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('pr.id AS prId, IF(pr.supplier_invoice_no IS NULL or pr.supplier_invoice_no = "", "N/A", pr.supplier_invoice_no) as supplier_invoice_no, DATE_FORMAT(pr.invoice_date, "%d/%m/%Y") as pr_invoice_date, pr.pr_net_amount AS net, COALESCE(SUM(pay.pay_amt), 0) as paid_amount, IF(payt.is_cheque = 1 AND pay.pay_pay_no != "", CONCAT(payt.type_name, "&nbsp;/ ", pay.pay_pay_bank, "&nbsp;- ", pay.pay_pay_no), payt.type_name) as type_name, CASE pr.payment_status WHEN 1 THEN "Not Paid" WHEN 2 THEN "Partially Paid" WHEN 3 THEN "Paid" ELSE "" END as status_str, sup.name AS supplier', FALSE)
                ->where('pay.status != 10 AND pay.brn_id IN ("' . $this->user_data['branch_id'] . '")')
                ->join(TBL_PR . ' as pr', 'pr.id = pay.precv_id AND pr.pr_status != 10', 'left')
                ->join(TBL_PAYMENT_TYPE . ' as payt', 'payt.id = pay.pay_pay_type', 'left')
                ->join(TBL_SUP . ' as sup', 'sup.id = pr.sup_id', 'left')
                ->group_by('pay.precv_id')
                ->order_by('pay.id', 'DESC')
                ->add_column('Actions', $this->get_buttons('$1'), 'prId')
                ->from(TBL_PPM . ' AS pay');
        echo $this->datatables->generate();
    }

    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        $html.='</span>';
        return $html;
    }

    public function unpaid_purchases() {
        $supplier_id = $this->input->post('supplier_id');
        $data = array();
        $data['invoices'] = $this->purchase_payment_model->get_unpaid_purchases_by_supplier($supplier_id);
        $this->ajax_response($data);
    }

    public function add($id = '') {
        $data = array();
        $data['grn_id'] = $id;
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        
        $actions = $this->actions();
        $data['form_action'] = $actions['insert'];
        $data['list_link'] = $actions['index'];
        $data['suppliers'] = $this->supplier_model->list_active();
        $data['payment_types'] = $this->payment_type_model->list_active();
        $bnk_acc_name = $this->user_model->get_active_acc($this->user_data['bank_account_id']);
        $data['bnk_acc_name'] = (count($bnk_acc_name)>0) ? $bnk_acc_name[0]['bank_name'] : '';
        $this->render($data, 'purchase_payment/add');
    }
    
    public function insert() {
		$purchase_payment = array(
			'pay_date' => date('Y-m-d', strtotime(str_replace('/','-',$this->input->post('pay_date')))),
			'pay_pay_type' => $this->input->post('payment_type'),
			'pay_pay_no' => '',
			'pay_pay_bank' => '',
			'pay_pay_date' => NULL,
			'pay_dd_no' => '',
			'pay_dd_date' => NULL,
			'status' => 1,
			'brn_id' => $this->user_data['branch_id'],
			'created_by' => $this->user_data['user_id'],
			'created_on' => date('Y-m-d H:i:s'),
			'ip_address' => $_SERVER['REMOTE_ADDR']
		);
		
		$payment_type = $this->payment_type_model->get_by_id($this->input->post('payment_type'));
		if ($payment_type->is_cheque == 1) {
			$purchase_payment['pay_pay_no'] = $this->input->post('cheque_acc_no');
			$purchase_payment['pay_pay_bank'] = $this->input->post('cheque_bank_name');
			$purchase_payment['pay_pay_date'] = date('Y-m-d', strtotime(str_replace('/','-',$this->input->post('cheque_date'))));
		}
		
		$grn_no = $this->input->post('grn_no');
		$credit_notes = $this->input->post('credit_notes');
		$amount = $this->input->post('amount');
		$paid_amount = $this->input->post('paid_amount');
		$already_paid = $this->input->post('already_paid');
		$affected_rows = 0;
		foreach($grn_no as $key => $value) {
			$purchase_payment['precv_id'] = $value;
			$purchase_payment['pay_amt'] = $paid_amount[$key];
			$purchase_payment['bank_acc_id'] = $this->user_data['bank_account_id'];
			if ($this->purchase_payment_model->insert($purchase_payment)) {
                                $purchase_receive = $this->purchase_receive_model->get_by_id($value);
                                $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $value, $purchase_receive->pr_code, '');
				if (!in_array($payment_type->group_name, array(1, 3))) {
					$update_data = array(
						'updated_by' => $this->user_data['user_id'],
						'updated_on' => date('Y-m-d H:i:s')
					);
					$this->bank_model->update_bank_amount($this->user_data['bank_account_id'], $update_data, $paid_amount[$key], '-');
				}
				
				$payment = array();
				if ((empty($credit_notes[$key])) && (($amount[$key] - ($paid_amount[$key] + $already_paid[$key])) > 0)) 
					$payment['payment_status'] = 2;
				else 
					$payment['payment_status'] = 3;
				
				$payment['brn_id'] = $this->user_data['branch_id'];
				$payment['updated_by'] = $this->user_data['user_id'];
				$payment['updated_on'] = date('Y-m-d H:i:s');
				$payment['ip_address'] = $_SERVER['REMOTE_ADDR'];
				$this->purchase_payment_model->update_purchase_receive_payment_status($value, $payment);
                $affected_rows ++;
			}
		}
		
		if ($affected_rows) {
			$_SESSION['success'] = 'Purchase Payment has been added successfully.';
		}
		else {
			$_SESSION['error'] = 'Purchase Payment not added!';
        }
        
        redirect('purchase_payments/add', 'refresh');
    }
    
    public function edit($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['invoice_no'] = $this->purchase_payment_model->list_invoice_no();
        $data['purchase_payment'] = $this->purchase_payment_model->get_by_id($id);
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' . $data['purchase_payment']->id;
        $data['list_link'] = $actions['index'];
        
        $data['payment_types'] = $this->payment_type_model->list_active();
        $this->render($data, 'purchase_payment/edit');
    }
    
    public function update($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        
        $user_data = get_user_data();
       
        $purchase_payment = array();
		$purchase_payment['pay_no'] = $purchase_payment_code;
		$purchase_payment['pi_id'] = $this->input->post('purchase_inv_no');
		$purchase_payment['pay_amt'] = $this->input->post('amnt_to_pay');
		$purchase_payment['pay_date'] = date('Y-m-d', strtotime(str_replace('/','-',$this->input->post('pay_date'))));
		$purchase_payment['pay_pay_type'] = $this->input->post('payment_type');
		$purchase_payment['pay_pay_no'] = '';
		$purchase_payment['pay_pay_bank'] = '';
		$purchase_payment['pay_pay_date'] = NULL;
		$purchase_payment['pay_dd_no'] = '';
		$purchase_payment['pay_dd_date'] = NULL;
		$purchase_payment['status'] = $this->input->post('status');
		$purchase_payment['branch_id'] = $user_data['branch_id'];
		$purchase_payment['created_by'] = $user_data['user_id'];
		$purchase_payment['created_on'] = date('Y-m-d H:i:s');
		$purchase_payment['ip_address'] = $_SERVER['REMOTE_ADDR'];
		
		$payment_type = $this->payment_type_model->get_by_id($this->input->post('payment_type'));
		if ($payment_type->is_cheque == 1) {
			$purchase_payment['pay_pay_no'] = $this->input->post('cheque_acc_no');
			$purchase_payment['pay_pay_bank'] = $this->input->post('cheque_bank_name');
			$purchase_payment['pay_pay_date'] = date('Y-m-d', strtotime(str_replace('/','-',$this->input->post('cheque_date'))));
		}
		
		$affected_rows = $this->purchase_payment_model->update($id, $purchase_payment);
        if ($affected_rows > 0) {
            $_SESSION['success'] = 'Purchase Payment has been Updated Successfully!..';
        }
		else {
            $_SESSION['error'] = 'Purchase Payment not Updated!..';
        }
		
        redirect('purchase_payments/edit/' . $id , 'refresh');
    }
    
    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $actions = $this->actions();
        $data = array();
        $data['delete_link']= $actions['delete'];
        $purchase_receive = $this->purchase_receive_model->get_by_id($id);
		$purchase_payment = array();
		if ($purchase_receive->credit_notes) {
			$purchase_payment[] = (object) array(
				'type_name' => 'Credit Notes',
				'payment_date' => str_replace('/', '-', $purchase_receive->pr_date_str),
				'pay_amt' => $purchase_receive->credit_amount,
				'is_cheque' => 0,
				'credit_notes' => 1
			);
		}
		
		$payments = $this->purchase_payment_model->get_payment_by_precv_id($id);
		#$purchase_payment = array_merge($purchase_payment, $payments);
		$purchase_payment = $payments;
		$data['purchase_receive'] = $purchase_receive;
        $data['purchase_payment'] = $purchase_payment;
        $this->render($data, 'purchase_payment/view');
    }
    
    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }
		
		$get_payment = $this->purchase_payment_model->get_payment_by_pay_id($id);
		$payment = $this->purchase_payment_model->get_by_id($id);
        $update_payment = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $this->user_data['user_id']
        );
			
        if ($this->purchase_payment_model->update($id, $update_payment) > 0) {
                        $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $get_payment->precv_id, $get_payment->pr_code, 1);
			if (!in_array($payment->group_name, array(1, 3))) {
				$update_data = array(
					'updated_by' => $this->user_data['user_id'],
					'updated_on' => date('Y-m-d H:i:s')
				);
				$this->bank_model->update_bank_amount($payment->bank_acc_id, $update_data, $payment->pay_amt, '+');    
                        }
			
			$purchase_receive = $this->purchase_payment_model->total_paid_by_purchase_recv_id($payment->precv_id);
			$update_purchase_receive = array();
			if (empty($purchase_receive->paid_amount)) {
				$update_purchase_receive['payment_status'] = 1;
			}
			else if ($purchase_receive->pr_net_amount > ($purchase_receive->paid_amount + $purchase_receive->credit_amount)) {
				$update_purchase_receive['payment_status'] = 2;
			}
			else {
				$update_purchase_receive['payment_status'] = 3;
			}
			
			$update_purchase_receive['brn_id'] = $this->user_data['branch_id'];
			$update_purchase_receive['updated_by'] = $this->user_data['user_id'];
			$update_purchase_receive['updated_on'] = date('Y-m-d H:i:s');
			$update_purchase_receive['ip_address'] = $_SERVER['REMOTE_ADDR'];
			$this->purchase_payment_model->update_purchase_receive_payment_status($payment->precv_id, $update_purchase_receive);
			
            $_SESSION['success'] = 'Purchase Payment has been deleted';
        }
        else {
            $_SESSION['error'] = 'Purchase Payment not deleted!';
        }
		
        redirect('purchase_payments', 'refresh');
    }
    
    /*public function get_supplier_details(){
       $pi_no = $_POST['pi_no'];
       $data = array();
       if(!empty($pi_no)){
            $data['pur_order'] = $this->purchase_payment_model->get_pur_order_details($pi_no);
            //error_log($this->db->last_query());
       }      
       echo json_encode($data['pur_order'][0]);  
   }
   
   public function get_paid_amount(){
       $pi_no = $_POST['pi_no'];
       $data = array();
       if(!empty($pi_no)){
           $data['paid_amount'] = $this->purchase_payment_model->get_paid_amount($pi_no);
       }
       echo json_encode($data['paid_amount'],1);  
   }*/
}
?>