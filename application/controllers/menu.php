<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menu extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // To Check session already started or not
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
        
        /* This user_data and ip_address will be accessed in all controllers*/
        $this->user_data = get_user_data();
        $this->ip_address = $_SERVER['REMOTE_ADDR'];
        
        /* Data initialization for render function */
        $this->data = array();
        $this->data['success'] = '';
        $this->data['error'] = '';
        if (!empty($_SESSION['success'])) {
            $this->data['success'] = $_SESSION['success'];
            unset($_SESSION['success']);
        }
        else if (!empty($_SESSION['error'])) {
            $this->data['error'] = $_SESSION['error'];
            unset($_SESSION['error']);
        }
        
        $this->config->set_item('css_url', BASE_URL . 'assets/css/');
        $this->config->set_item('js_url', BASE_URL . 'assets/js/');
        $this->config->set_item('images_url', BASE_URL . 'assets/images/');
        $this->config->set_item('upload_url', BASE_URL . UPLOADS);
        
        $user_data  = get_user_data();
        $this->user_data = $user_data;
        $this->permissions = $user_data['permissions'];
        $this->permission = (!empty($user_data['permission'])) ? $user_data['permission'] : '';
        #check_permission();
    }
    
    public function index() {
        $header     = get_header_data();        
        #$user_data  = get_user_data();
        
        $header['permissions'] = $this->permissions;
        $header['permission'] = $this->permission;
        //$header['page_title'] = $user_data['page_title'];
        $header['user_data'] = $this->user_data;
        $header['left_menu'] = 0;
        
        // To set custom header datas from controllers
        if (!empty($data['header'])) {
            foreach($data['header'] as $key => $value) {
                $header[$key] = $value;
            }
        }
        
        $head = array();
        $data['permission']     = $header['permission'];
        $data['user_data']      = $head['user_data'] = $this->user_data;
        $head['page_title']     = $header['page_title'];
        if (!empty($header['permissions'][$header['current_controller']])) {
            $data['permission'] = $header['permissions'][$header['current_controller']];
        }
        
        $footer = array();
        $this->load->view('templates/head', $head);
        $this->load->view('templates/header', $header);
        $this->load->view('menu', $data);
        $this->load->view('templates/footer', $footer);
    }
}
?>