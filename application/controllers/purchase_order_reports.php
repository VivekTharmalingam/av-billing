<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Purchase_Order_Reports extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('purchase_receive_model', '', TRUE);
        $this->load->model('purchase_order_model', '', TRUE);
    }

    public function index() {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = base_url() . 'purchase_orders/view' . '/';
        $data['form_action'] = base_url() . 'purchase_order_reports';
        $from = '';
        $to = '';
        $data['search_date'] = '';
        if ($this->input->post('search_date') != '') {
            $data['search_date'] = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : '';
            $search_date = explode('-', $this->input->post('search_date'));
            $from = convert_text_date(trim($search_date[0]));
            $to = convert_text_date(trim($search_date[1]));
        }
        $data['from_date'] = $from;
        $data['to_date'] = $to;
        $data['supplier'] = $this->input->post('sup_name');
        $data['inv_sts'] = $this->input->post('inventory_sts');
        $data['pay_sts'] = $this->input->post('payment_status');

        $data['purchase'] = $this->purchase_order_model->list_search_all($from, $to, $this->input->post('sup_name'), $this->input->post('inventory_sts'), $this->input->post('payment_status'));
        if ($this->input->post('reset_btn')) {
            $data['search_date'] = '';
            $data['supplier'] = '';
            $data['inv_sts'] = '';
            $data['pay_sts'] = '';
            $data['purchase'] = $this->purchase_order_model->list_search_all();
        }
        $data['suppliers'] = $this->purchase_order_model->get_all_suppliers();
        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Purchase Order List Reports';
            $head['setting'] = $this->company_model->get_company();
            $filename = 'Purchase_order_Report_' . current_date();
            // Load Views
            $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
            $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
            $header = '<div></div>';
            $content = $this->load->view('purchase_order_report/pdf_report', $data, TRUE);
            $footer = $this->load->view('templates/pdf/pdf_footer', '', TRUE);
            $this->load->helper(array('My_Pdf'));
            create_pdf($header, $content, $footer, $filename);
            exit(0);
        } elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $this->load->view('purchase_order_report/excel_report', $data);
        } else {
            $this->render($data, 'purchase_order_report/list');
        }
    }

    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $data = array();
//        $data['customer'] = $this->customer_model->get_by_id($id);
//        $data['ship_address'] = $this->customer_model->get_address_by_id($id);

        $this->render($data, 'customer/view');
    }

}

?>