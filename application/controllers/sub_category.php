<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sub_category extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('sub_category_model', '', TRUE);
        $this->load->library('upload');
        $this->load->library('Datatables');
        $this->load->library('table');
    }

    public function index() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['add_btn'] = 'Create Item Category';
        $data['view_link'] = $actions['view'] . '/';
        $data['add_link'] = $actions['add'];
        $data['edit_link'] = $actions['edit'] . '/';
        $data['delete_link'] = $actions['delete'] . '/';
        $data['item_category'] = $this->sub_category_model->list_all();
        $this->render($data, 'sub_category/list');
    }

    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('sc.id AS tId,sc.sub_category_name as name,c.item_category_name AS cat_name,DATE_FORMAT(sc.created_on,"%d/%m/%Y") AS created_date,CASE sc.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE)
//                ->unset_column('iterator')
                ->where('sc.status != 10')
                ->join(TBL_ICT . ' as c', 'c.id = sc.category_id', 'left')
                ->add_column('Actions', $this->get_buttons('$1'), 'tId')
                ->from(TBL_SUB_CAT . ' AS sc');
        echo $this->datatables->generate();
    }

    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(3, $this->permission)) {
            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
        }
        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger pop_up_confirm delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Sub Category details!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Sub Category"><span>Delete</span></a>';
        }
        $html.='</span>';
        return $html;
    }

    public function add() {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['category'] = $this->sub_category_model->category_list();
        $data['form_action'] = $actions['insert'];
        $data['list_link'] = $actions['index'];
        $this->render($data, 'sub_category/add');
    }

    public function insert() {
        $user_data = get_user_data();
        $insert = array(
            'category_id' => $this->input->post('item_category'),
            'sub_category_name' => $this->input->post('sub_category'),
            'status' => $this->input->post('status'),
            'branch_id' => $this->user_data['branch_id'],
            'created_by' => $this->user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s')
        );
        $last_id = $this->sub_category_model->insert($insert);
        if ($last_id > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, '', '');
            $_SESSION['success'] = 'Sub Category added successfully';
        } else {
            $_SESSION['error'] = 'Sub Category not added!';
        }
        redirect('sub_category/add', 'refresh');
    }

    public function edit($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['category'] = $this->sub_category_model->category_list();
        $data['sub_cat'] = $this->sub_category_model->get_by_id($id);
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' . $data['sub_cat']->id;
        $this->render($data, 'sub_category/edit');
    }

    public function update($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $update = array(
            'category_id' => $this->input->post('item_category'),
            'sub_category_name' => $this->input->post('sub_category'),
            'status' => $this->input->post('status'),
            'branch_id' => $this->user_data['branch_id'],
            'updated_by' => $this->user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s')
        );
        
        if ($this->sub_category_model->update($id, $update) > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', '');  
            $_SESSION['success'] = 'Sub Category updated successfully';
        } else {
            $_SESSION['error'] = 'Sub Category not updated!';
        }
        redirect('sub_category/edit/' . $id, 'refresh');
    }

    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        $data = array();
        $data['sub_cat'] = $this->sub_category_model->get_by_id($id);
        $this->render($data, 'sub_category/view');
    }

    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }
        $exp = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $this->user_data['user_id']
        );
        if ($this->sub_category_model->update($id, $exp) > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, '', 1);
            $_SESSION['success'] = 'Sub Category deleted successfully';
        } else {
            $_SESSION['error'] = 'Sub Category not deleted!';
        }
        redirect('sub_category', 'refresh');
    }

//    public function check_exp_exists() {
//        $exp = $_REQUEST['exp_name'];
//        echo $this->customer_group_model->check_exp_exists($exp, $_REQUEST['group_id']);
//    }

}

?>