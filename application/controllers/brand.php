<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Brand extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('category_model', '', TRUE);
        $this->load->library('upload');
        $this->load->library('Datatables');
    }
    
    public function index() {
        $data = array();
        $data['success']=$this->data['success'];  
        $data['error']=$this->data['error'];       
        $actions = $this->actions();
        $data['add_btn']    = 'Create Brand';
        $data['view_link']  = $actions['view'] . '/';
        $data['add_link']   = $actions['add'];
        $data['edit_link']  = $actions['edit'] . '/';
        $data['delete_link']= $actions['delete'] . '/';
        $data['category']      = $this->category_model->list_all();   
        $this->render($data, 'brand/list');
    }

    function datatable() {
        $user_data = get_user_data();
        $this->datatables->select('brand.id AS bId,brand.cat_code AS code,brand.cat_name AS name,CASE brand.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str', FALSE)
//                ->unset_column('iterator')
                ->where('brand.status != 10 AND brand.status != 2')
                ->add_column('Actions', $this->get_buttons('$1'), 'bId')
                ->from(TBL_CAT . ' AS brand');
        echo $this->datatables->generate();
    }

    function get_buttons($id) {
        $actions = $this->actions();
        $html = '<span class="actions">';
        if (in_array(1, $this->permission)) {
            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';
        }
        if (in_array(3, $this->permission)) {
            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';
        }
        if (in_array(4, $this->permission)) {
            $html .='<a class="label btn btn-danger pop_up_confirm delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Brand details!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Brand"><span>Delete</span></a>';
        }
        $html.='</span>';
        return $html;
    }
        
    public function add() {
        $data = array();
        $data['success']=$this->data['success'];  
        $data['error']=$this->data['error'];        
        $actions = $this->actions();
        $data['form_action'] = $actions['insert'];
        $data['list_link']   =  $actions['index'];
		
        $this->render($data, 'brand/add');
    }
    
    public function insert() {
        //echo '<pre>';print_r($_REQUEST);print_r($_FILES);
        //exit;
		
        $user_data = get_user_data();  
       
        $short_name = strtoupper(substr($this->input->post('category_name'), 0, 3));
        $prefix_wt_sub=CODE_BND.$short_name;
        $code = $this->auto_generation_code(TBL_CAT, $prefix_wt_sub, '', 3, '');
		
        $insert = array(
            'cat_code' => $code,
            'cat_name' => $this->input->post('category_name'),            
            'status' => $this->input->post('status'),            
            'branch_id' => $this->user_data['branch_id'],
            'created_by' => $this->user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s')
        );
		
        $last_id =$this->category_model->insert($insert);
       
        if ( $last_id > 0) {            
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, $code, '');
            $_SESSION['success'] = 'Category added successfully';			
        } else {
            $_SESSION['error'] = 'Category not added!';        
        }
        redirect('brand/add', 'refresh');
    }
    
    public function edit($id = ''){
        if (empty($id)) {
            show_400_error();
        }
        $data['success']=$this->data['success'];  
        $data['error']=$this->data['error'];
        
        $data['category'] = $this->category_model->get_by_id($id);				
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' . $data['category']->id;        
        $this->render($data, 'brand/edit');
    }
    
    public function update($id = '') {    
			
        if (empty($id)) {
            show_400_error();
        }
        $user_data = get_user_data();
        $uploaded_data = '';
       
        $short_name = strtoupper(substr($this->input->post('category_name'), 0, 3));
        $prefix_wt_sub=CODE_BND.$short_name;
        $code = $this->auto_generation_code(TBL_CAT, $prefix_wt_sub, '', 3, $id);
        
        $update = array(
            'cat_code' => $code,
            'cat_name' => $this->input->post('category_name'),            
            'status' => $this->input->post('status'),            
            'branch_id' => $this->user_data['branch_id'],              
            'updated_by' => $this->user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s')
        );
       
		//echo "<pre>"; print_r($update);die;
        if ($this->category_model->update($id, $update) > 0) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $code, '');    
            $_SESSION['success'] = 'Category updated successfully';
        } else { 
            $_SESSION['error'] = 'Category not updated!';
        }
        redirect('brand/edit/' . $id, 'refresh');
    }
    
    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }        
        $data = array();
        $data['category'] = $this->category_model->get_by_id($id);						
        $this->render($data, 'brand/view');
    }
    
    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }        
        $category = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $this->user_data['user_id']
        );        
        if ($this->category_model->update($id, $category) > 0) { 
            $category_arr = $this->category_model->get_by_id($id);
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $category_arr->cat_code, 1);
            $_SESSION['success'] = 'Category deleted successfully';
        } else { 
            $_SESSION['error'] = 'Category not deleted!'; 
        }
        redirect('brand', 'refresh');
    }
    
    public function check_category_exists(){
		$category_id = (!empty($_REQUEST['category_id'])) ? $_REQUEST['category_id'] : '';
       $category = $_REQUEST['category_name'];
       echo $this->category_model->check_category_exists($category, $category_id);
    }
	
	public function brand_auto_complete() {
        $data = array();
        $data = $this->category_model->list_active();
        foreach ($data as $key => $value) {
            $value->label = $value->cat_name;
            $value->value = $value->label;
        }
		
        $this->ajax_response($data);
    }
}
?>