<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Unit extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('unit_model', '', TRUE);
        $this->load->library('upload');
    }
    
    public function index() {
        $data = array();
        $data['success']=$this->data['success'];  
        $data['error']=$this->data['error'];       
        $actions = $this->actions();
        $data['add_btn']    = 'Create Unit';
        $data['view_link']  = $actions['view'] . '/';
        $data['add_link']   = $actions['add'];
        $data['edit_link']  = $actions['edit'] . '/';
        $data['delete_link']= $actions['delete'] . '/';
        $data['unit']      = $this->unit_model->list_all();
        $this->render($data, 'unit/list');
    }
    
    public function add() {
        $data = array();
        $data['success']=$this->data['success'];  
        $data['error']=$this->data['error'];        
        $actions = $this->actions();
        $data['form_action'] = $actions['insert'];
        $data['list_link']   =  $actions['index'];         
        
        $this->render($data, 'unit/add');
    }
    
    public function insert() {    
    
        $user_data = get_user_data();         
        $insert = array(            
            'unit_name' => $this->input->post('unit'),            
            'status' => $this->input->post('status'),            
            'branch_id' => $this->user_data['branch_id'],   
            'created_by' => $this->user_data['user_id'],
            'created_on' => date('Y-m-d H:i:s')
        );
			
        $last_id =$this->unit_model->insert($insert);
      
        if ( $last_id > 0)            
            $_SESSION['success'] = 'Unit added successfully';			
        else 
            $_SESSION['error'] = 'Unit not added!';        
        redirect('unit/add', 'refresh');
    }
    
    public function edit($id = ''){
        if (empty($id)) {
            show_400_error();
        }
        $data['success']=$this->data['success'];  
        $data['error']=$this->data['error'];
        
        $data['unit'] = $this->unit_model->get_by_id($id);		
        $actions = $this->actions();
        $data['form_action'] = $actions['update'] . '/' . $data['unit']->id;        
        $this->render($data, 'unit/edit');
    }
    
    public function update($id = '') {    
			
        if (empty($id)) {
            show_400_error();
        }
        $user_data = get_user_data();
        
        $update = array(            
            'unit_name' => $this->input->post('unit'),
            'status' => $this->input->post('status'),
			'branch_id' => $this->user_data['branch_id'],   	
            'updated_by' => $user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s')
        ); 
			
        if ($this->unit_model->update($id, $update) > 0)
                
            $_SESSION['success'] = 'Unit updated successfully';
        else 
            $_SESSION['error'] = 'Unit not updated!';
        redirect('unit/edit/' . $id, 'refresh');
    }
    
    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }        
        $data = array();
        $data['unit'] = $this->unit_model->get_by_id($id);
        $this->render($data, 'unit/view');
    }
    
    public function delete($id) {
        if (empty($id)) {
            show_400_error();
        }        
        $unit = array(
            'status' => 10,
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $this->user_data['user_id']
        );        
        if ($this->unit_model->update($id, $unit) > 0) 
            $_SESSION['success'] = 'Unit deleted successfully';
        else 
            $_SESSION['error'] = 'Unit not deleted!';        
        redirect('unit', 'refresh');
    }
    
    public function check_unit_exists(){
       $unit = $_REQUEST['unit_name'];
       echo $this->unit_model->check_unit_exists($unit,$_REQUEST['unit_id']);
    }
        

}
?>