<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('setting_model', '', TRUE);
        $this->load->model('company_model', '', TRUE);
        $this->load->library('upload');
    }

    public function index($id=NULL) {
        $data = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $data['backup_name'] = (!empty($_SESSION['backup_name'])) ? $_SESSION['backup_name'] : '';
        $actions = $this->actions();
        $user_data = get_user_data();
        $actions = $this->actions();
        $data['company'] = $this->company_model->get_company();
        $data['form_action'] = $actions['update'] . '/';
        $data['form_setting_data'] = $actions['update'] . '/' . $data['company']->id;
        
        $data['permissions'] = $this->permission;
        
        $path = BACKUP_FILE_PATH; 
        if(file_exists($path)) {
            $data['latest_ctime'] = 0;
            $data['latest_filename'] = '';    

            $d = dir($path);
            while (false !== ($entry = $d->read())) {
              $filepath = "{$path}/{$entry}";
              // could do also other checks than just checking whether the entry is a file
              if (is_file($filepath) && filectime($filepath) > $data['latest_ctime']) {
                $data['latest_ctime'] = filectime($filepath);
                $data['latest_filename'] = BACKUP_FILE_PATH . $entry;                
              }
            }
        }
        $this->render($data, 'setting/edit');
    }
    
    public function update($id = '') {
        if (empty($id)) {
            show_400_error();
        }
        
        $update = array(
            'daily_from_email' => $this->input->post('daily_from_email'),
            'daily_to_email' => $this->input->post('daily_to_email'),
            'gst' => $this->input->post('gst_percent'),
            'gst_percent' => $this->input->post('gst_percent'),
            'updated_by' => $this->user_data['user_id'],
            'updated_on' => date('Y-m-d H:i:s'),
            'branch_id' => $this->user_data['branch_id'],
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        
        if ($this->company_model->update($id, $update)) {
            $this->insert_log($this->log_controler_name, $this->log_method_name, 'index', $id, '', '');
            $_SESSION['success'] = 'Company updated successfully';
        } else {
            $_SESSION['error'] = 'Company not updated!';
        }

        redirect('settings', 'refresh');
    }
    
    public function db_backup() {
        $this->backup_script(1);
        redirect('settings','refresh');
    }
    
    public function db_import() {
        $file = $_FILES['import_db_file']['tmp_name']; 
        
        //Export DB
        $this->backup_script(2);
        
        // Get Mysql Directory via Query
        $mysql_dir_value = $this->setting_model->get_mysql_directory()->Value;        
        $mysqlDir = $mysql_dir_value . '/bin';
        
        //Drop exists DB
        $drop = 'cd ' . $mysqlDir . ' & mysql -h ' . $this->db->hostname .' -u ' . $this->db->username;
        $drop .= ($this->db->password != '') ? ' -p' . $this->db->password  : ' ';
        $drop .= ' -D ' . $this->db->database . ' -e "DROP DATABASE IF EXISTS ' . $this->db->database .'"';
        exec($drop);
        
        //Create New DB
        $create = 'cd ' . $mysqlDir . ' & mysql -h ' . $this->db->hostname .' -u ' . $this->db->username;
        $create .= ($this->db->password != '') ? ' -p' . $this->db->password  : ' ';
        $create .= ' -e "CREATE DATABASE ' . $this->db->database . '"';
        exec($create,$output=array(),$create_status);
        
        if($create_status == 0) {
            //Import New DB
            $cmd  = 'cd ' . $mysqlDir . ' & mysql -h ' . $this->db->hostname .' -u ' . $this->db->username;
            $cmd .= ($this->db->password != '') ? ' -p' . $this->db->password  : ' ';
            $cmd .=  $this->db->database .' < ' . $file;
            exec($cmd,$output=array(),$status);
            switch($status){
                case 0:
                    $_SESSION['backup_name'] = 1;
                    $_SESSION['success'] = 'Database Imported Successfully!';
                    break;
                    
                case 1:
                    $_SESSION['error'] = 'Database not Imported!';
                    break;
            }
        } else {
            $_SESSION['error'] = 'Database is not Created!';
        }
        
        redirect('settings','refresh');
    }
    
    public function backup_script($type) {
        $user_data = get_user_data();
        // Get Mysql Directory via Query
        $mysql_dir_value = $this->setting_model->get_mysql_directory()->Value;
        
        $mysqlDir = $mysql_dir_value . '/bin';
        
        //path to mysql dump utility
        $save_path = BACKUP_FILE_PATH;
        
        // format dir name
        $filename = $this->db->database . '_' . date('d_m_Y_H_i') . '_' . $user_data['user_name'];
        
        //check if directory exists otherwise create it
        if ( !file_exists ($save_path ) ) {
            mkdir($save_path, 0777, true);
        }
        $cmd = 'cd ' . $mysqlDir . ' & mysqldump -h ' . $this->db->hostname . ' -u ' . $this->db->username;
        $cmd .= ($this->db->password != '') ? ' -p ' . $this->db->password : ' ';
        $cmd .= $this->db->database . '> '. $save_path . $filename . '.sql';
        exec($cmd);

        if($type == 1) {
            if(file_exists ($save_path . $filename . '.sql')) {
                $backup_name = $save_path . $filename . '.sql';
                #download_file($backup_name);
                $_SESSION['backup_name'] = 2;
                $_SESSION['success'] = 'Database Backup has been done!';
            } else {
                $_SESSION['error'] = 'Database Backup has not completed!';
            } 
             
        } 
    }

    public function get_last_backup_file(){
        $path = BACKUP_FILE_PATH; 
        if(file_exists($path)) {
            $latest_ctime = 0;
            $latest_filename = '';    

            $d = dir($path);
            while (false !== ($entry = $d->read())) {
              $filepath = "{$path}/{$entry}";
              // could do also other checks than just checking whether the entry is a file
              if (is_file($filepath) && filectime($filepath) > $latest_ctime) {
                $latest_ctime = filectime($filepath);
                $latest_filename = BACKUP_FILE_PATH . $entry;
              }
            }
        }
        download_file($latest_filename);
    }
}
?>