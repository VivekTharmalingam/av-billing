<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');
class Profit_Loss_Report extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('sales_order_model', '', TRUE);
        $this->load->model('customer_model', '', TRUE);
    }

    public function index() {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = base_url() . 'invoice/view' . '/';
        $data['form_action'] = base_url() . 'profit_loss_report';
        
		$customer = (!empty($this->input->post('cus_name'))) ? $this->input->post('cus_name') : '';
		$issued_by = (!empty($this->input->post('issued_by'))) ? $this->input->post('issued_by') : '';
		$payment_status = (!empty($this->input->post('payment_status'))) ? $this->input->post('payment_status') : '';
		$search_date = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : date('01/m/Y') . ' - ' . date('t/m/Y');
		$branch_id = (!empty($this->input->post('branch_id'))) ? $this->input->post('branch_id') : '';
		if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'Reset')) {
			$customer = '';
            $issued_by = '';
			$branch_id = '';
			$payment_status = '';
			$search_date = date('01/m/Y') . ' - ' . date('t/m/Y');
		}
		
		if ((!empty($this->input->post('submit'))) && (empty($this->input->post('search_date')))) {
			$search_date = '';
			$from = '';
			$to = '';
		}
		else {
			$search_date_arr = explode('-', $search_date);
			$from = convert_text_date(trim($search_date_arr[0]));
			$to = convert_text_date(trim($search_date_arr[1]));
		}
		
        $data['search_date'] = $search_date;
        $data['from_date'] = $from;
        $data['to_date'] = $to;
        $data['custom'] = $customer;
        $data['issued_by'] = $issued_by;
        $data['pay_sts'] = $payment_status;
        $data['branch_id'] = $branch_id;
		
		$data['users'] = $this->user_model->active_user_list();
        $data['purchase'] = $this->sales_order_model->profit_loss_summary($from, $to, $customer, $issued_by, $branch_id, $payment_status);
		$data['branches'] = $this->company_model->active_user_branches();
        $data['customers'] = $this->customer_model->list_active();
		$data['permission'] = $this->permission;
        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Profit Reports';
            $head['setting'] = $this->company_model->get_company();
            $filename = 'Profit_Report_' . current_date();
            // Load Views
            $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
            $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
            $header = '<div></div>';
            $content = $this->load->view('profit_loss_report/pdf_report', $data, TRUE);
            $footer = '<div></div>';
            $this->load->helper(array('My_Pdf'));
			create_pdf($header, $content, $footer, $filename);
            exit(0);
        } elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $this->load->view('profit_loss_report/excel_report', $data);
        } else {
            $this->render($data, 'profit_loss_report/list');
        }
    }


}

?>