<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product_Price_List extends REF_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model('product_model', '', TRUE);
		$this->load->model('inventory_model', '', TRUE);
        $this->load->library('upload');
    }
	
    public function index() {
        $data = array();
		$data1 = '';
		$head = array();
        $data['success']=$this->data['success'];  
        $data['error']=$this->data['error'];       
        $actions = $this->actions();
        $data['view_link']  = base_url() . 'product/view' . '/';
		$data['form_action'] = base_url() . 'product_price_list';
		
		$data['brand_id'] = $this->input->post('brand_id');        
		$data['product']      = $this->product_model->list_search_all($data['brand_id']);
		$data['brand']      = $this->inventory_model->list_brand();
		
		if($this->input->post('reset_btn')){
			$data['search_date']='';
			$data['brand_id'] ='';
			$data['product'] = $this->product_model->list_search_all();
        }
		
		if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Product Price List Reports';
		    $head['setting'] = $this->company_model->get_company();
			$filename = 'Product_price_list_' . current_date();
			
			// Load Views
			$data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
			$data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
			$header = '<div></div>';
			$content = $this->load->view('product_price_list/pdf_report', $data, TRUE);			
			$footer = $this->load->view('templates/pdf/pdf_footer', '', TRUE);
			$this->load->helper(array('My_Pdf'));
									
			create_pdf($header, $content, $footer, $filename);
			exit(0);
			
		} elseif(((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel')) ){
			$this->load->view('product_price_list/excel_report', $data);
			
		}else {
			$this->render($data, 'product_price_list/list');
		}
		
    }
	
    public function view($id = '') {
        if (empty($id)) {
            show_400_error();
        }        
        $data = array();
        $data['customer']   = $this->customer_model->get_by_id($id);
		$data['ship_address']   = $this->customer_model->get_address_by_id($id);
			
        $this->render($data, 'customer/view');
    }
}
?>