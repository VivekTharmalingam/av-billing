<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class notifications extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('sales_order_model', '', TRUE);
        $this->load->model('supplier_model', '', TRUE);
        $this->load->library('upload');
    }

    public function index() {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['form_action'] = base_url() . 'notifications';

        $user_id = $this->user_data['user_id'];$status = array(1,2);
        $notifi_data = $this->sales_order_model->get_notification_by_type('Invoice','',$user_id,$status,'');
        if(count($notifi_data)>0){
            for($i=0;$i<count($notifi_data);$i++){
                $notification_data = array(
                    'status' => 3,
                    'updated_on' => date('Y-m-d H:i:s'),
                    'updated_by' => $this->user_data['user_id']
                );
                $this->sales_order_model->update_notification_item($notification_data,$notifi_data[$i]['not_id']);
            }
        }
        
        $status_all = array(1,2,3);
        $invoice_not = $this->sales_order_model->get_notification_by_type('Invoice','',$user_id,$status_all,'');
        $data['notifications_data'] = $invoice_not;
	
        $this->render($data, 'notifications/list');
    }


}

?>