<?php

if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Item extends REF_Controller {

    public function __construct() {

        parent::__construct();

        $this->load->model('product_model', '', TRUE);

        $this->load->model('category_model', '', TRUE);

        $this->load->model('company_model', '', TRUE);

        $this->load->library('upload');

        $this->load->library('Datatables');

        $this->load->library('table');

    }



    public function index() {

        $data = array();

        $data['success'] = $this->data['success'];

        $data['error'] = $this->data['error'];

        $actions = $this->actions();

        $data['add_btn'] = 'Create Item';

        $data['view_link'] = $actions['view'] . '/';

        $data['add_link'] = $actions['add'];

        $data['edit_link'] = $actions['edit'] . '/';

        $data['delete_link'] = $actions['delete'] . '/';

        $data['product'] = $this->product_model->list_all();

        $data['category'] = $this->product_model->category();

        $this->render($data, 'product/list');

    }



    public function add($id=NULL) {

        $user_data = get_user_data();

        $data = array();

        $data['success'] = $this->data['success'];

        $data['error'] = $this->data['error'];

        $actions = $this->actions();

        $data['form_action'] = $actions['insert'];

        $data['list_link'] = $actions['index'];

        $data['product'] = array();

        if(!empty($id)) {

            $data['product'] = $this->product_model->get_by_id($id);

        }

        $data['item_category'] = $this->product_model->item_category();

        $data['category'] = $this->product_model->category();

        $data['branches'] = $this->company_model->branch_list_active();

		foreach($data['branches'] as $key => $value) {

			$data['branches'][$key]->brn_id = $value->id;

		}

        //$data['so_no'] = $this->auto_generation_code_item(TBL_PDT, CODE_ITEM_CODE . ' ', '', 3, '');

        $data['add_category_link'] = base_url() . 'brand/add/';



        $this->render($data, 'product/add');

    }



    function datatable() {

        $user_data = get_user_data();

        $this->datatables->select('p.id AS pid,p.pdt_code AS code,p.bar_code as bar_code,p.s_no as s_no,p.pdt_name AS pname,CASE p.status WHEN 1 THEN "Active" WHEN 2 THEN "Inactive" ELSE "" END as status_str, c.cat_name as catname', FALSE)

                ->where('p.status != 10 AND p.status != 2')

                ->join(TBL_CAT . ' as c', 'c.id = p.cat_id', 'left')

                ->add_column('Actions', $this->get_buttons('$1'), 'pid')

                ->from(TBL_PDT . ' AS p');



        echo $this->datatables->generate();

    }



    function get_buttons($id) {

        $actions = $this->actions();

        $html = '<span class="actions">';

        if (in_array(7, $this->permission)) {

        $html .='<a class="label btn btn-primary view print-barcode" data-href="' . base_url() . 'item/thermal/' . $id . '" title="Print Item Barcode"><span><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Barcode</span></a>&nbsp';

        }

        $html .='<a class="label btn btn-info pop_up_confirm delete delete-confirm" href="#" data-confirm-content="Do you want copy this Item details!" data-redirect-url="' . base_url() . 'item/add/' . $id . '" data-bindtext="Item"><span>Copy</span></a>&nbsp';

        if (in_array(1, $this->permission)) {

            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';

        }

        if (in_array(3, $this->permission)) {

            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';

        }

        if (in_array(4, $this->permission)) {

            $html .='<a class="label btn btn-danger pop_up_confirm delete delete-confirm" href="#" data-confirm-content="You will not be able to recover Item details!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="Item"><span>Delete</span></a>';

        }

        $html.='</span>';

        return $html;

    }



    public function print_barcode($id) {

        $data = array();

        $actions = $this->actions();

        $data['browser'] = browser_info();

        $data['list_link'] = $actions['index'];

        if (!empty($from_list))

            $data['redirect_link'] = $actions['index'];

        else

            $data['redirect_link'] = $actions['add'];

        $data['product'] = $this->product_model->get_by_id($id);

        $data['success'] = $this->data['success'];

        $data['error'] = $this->data['error'];

        $data['invoice_link'] = base_url() . 'item/barcode/' . $id;

        $this->render($data, 'product/print');

    }



    public function barcode($id, $nos = NULL) {

        $data = array();

        $user_data = get_user_data();

        $data['product'] = $this->product_model->get_by_id($id);

        $data['company'] = $this->company_model->get_company();

        $data['nos'] = $nos;

        $filename = 'Barcode_' . $data['product']->pdt_code;

        $filename = strtoupper($filename);

        // Load Views

        $header = $this->load->view('product/pdf_header', $data, TRUE); //'<div></div>';

        $content = '';

        $footer = '';

        //echo $header;exit;

        $this->load->helper(array('My_Pdf'));

        invoice_pdf($header, $content, $footer, $filename);

        exit(0);

    }



    public function thermal($id, $nos = NULL) {

        $data = array();

        $data['product'] = $this->product_model->get_by_id($id);

        $data['company'] = $this->company_model->get_company();

        $data['nos'] = $id.'--'.$nos;

        $filename = 'Barcode_' . $data['product']->pdt_code;

        echo $this->load->view('product/thermal', $data, TRUE);

    }



    public function insert() {

        if (empty($this->input->post('is_update'))) {

			$qty = ($this->input->post('new_quantity') != '') ? $this->input->post('new_quantity') : 0;

            

			//$this->input->post('pdt_code');

            $pdt_code = $this->auto_generation_code_item(TBL_PDT, CODE_ITEM_CODE . ' ', '', 3, '');

            $category = ($this->input->post('cat_name') != '') ? $this->input->post('cat_name') : null;

			

            $insert = array(

                'pdt_code' => $pdt_code,

                'pdt_name' => $this->input->post('pdt_desc'),
				
				'pdt_spec'	=> $this->input->post('pdt_spec'),

                'cat_id' => $category,

                'item_category' => $this->input->post('item_cat'),

                'sub_category' => $this->input->post('sub_cat'),

                'selling_price' => $this->input->post('sel_price'),

                'buying_price' => $this->input->post('buy_price'),

                'bfr_gst_buying_price' => $this->input->post('buy_price'),

                'status' => $this->input->post('status'),

                'bar_code' => $this->input->post('search_barcode'),

                's_no' => $this->input->post('s_no'),

                'warranty' => $this->input->post('warranty'),

                /* 'model_no' => $this->input->post('model_no'), */

                'branch_id' => $this->user_data['branch_id'],

                'created_by' => $this->user_data['user_id'],

                'created_on' => date('Y-m-d H:i:s')

            );

            if (!empty($_FILES['file']['name'])) :

				$uploaded_data = uploads(array('upload_path' => 'product', 'field' => 'file'));

                $insert['image'] = $uploaded_data['file']['file_path_str'];

            endif;

			

            $last_id = $this->product_model->insert($insert);



            if ($last_id > 0) {

                $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $last_id, $pdt_code, ''); 

                $branches_id = $this->input->post('branch_id');

                $branch_location = $this->input->post('branch_location');

                if (!empty($branches_id)) {

                    foreach ($branches_id as $key => $branch_id) {

                        $brn_item = array(

                            'brn_id' => $branch_id,

                            'branch_location' => $branch_location[$key],

                            'product_id' => $last_id,

                            'branch_id' => $this->user_data['branch_id'],

                            'created_by' => $this->user_data['user_id'],

                            'created_on' => date('Y-m-d H:i:s'),

                            'ip_address' => $_SERVER['REMOTE_ADDR']

                        );

						

                        if($this->user_data['branch_id'] == $branch_id) {

                            $brn_item['available_qty'] = $qty;

                        }

						else {

                            $brn_item['available_qty'] = 0;

                        }

                        $this->product_model->insert_brn_material($brn_item);

                    }

                }

				

				if ($qty > 0) {

					$sc_item = array(

						'branch_id' => $this->user_data['branch_id'],

						'product_id' => $last_id,

						'quantity' => $qty,

						'created_by' => $this->user_data['user_id'],

						'created_on' => date('Y-m-d H:i:s'),

						'ip_address' => $_SERVER['REMOTE_ADDR']

					);

					$this->product_model->insert_scan_product($sc_item);

				}

				

                $insert = array(

                    'product_id' => $last_id,

                    'buying_price' => $this->input->post('buy_price'),

                    'selling_price' => $this->input->post('sel_price'),

                    'updated_from' => 'From Product Master',

                    'brn_id' => $this->user_data['branch_id'],

                    'created_by' => $this->user_data['user_id'],

                    'created_on' => date('Y-m-d H:i:s'),

                    'ip_address' => $_SERVER['REMOTE_ADDR']

                );

                $this->product_model->insert_cost_log($insert);

            }

        }

		else {

			$id = $this->input->post('product_id');

			$prdct = $this->product_model->get_by_id($id);

			

            if ((empty($_FILES['file']['name'])) && (!empty($prdct->image))) :

                // Remove prev. file

                unlink(FCPATH . UPLOADS . $prdct->image);

            endif;

			

			$short_name = strtoupper(substr($this->input->post('pdt_desc'), 0, 3));

			#$prefix_wt_sub = CODE_PDT . $short_name;

			$prefix_wt_sub = $short_name;

			$code = $this->auto_generation_code_item(TBL_PDT, $prefix_wt_sub, '', 3, $id);



			//$pdt_code = ($this->input->post('pdt_code') != '') ? $no : $code;

			$category = ($this->input->post('cat_name') != '') ? $this->input->post('cat_name') : null;

			$update = array(

				//'pdt_code' => $pdt_code,

				'pdt_name' => $this->input->post('pdt_desc'),
				
				'pdt_spec'	=> $this->input->post('pdt_spec'),

				'cat_id' => $category,

				'item_category' => $this->input->post('item_cat'),

				'sub_category' => $this->input->post('sub_cat'),

				'selling_price' => $this->input->post('sel_price'),

				'buying_price' => $this->input->post('buy_price'),

				'bfr_gst_buying_price' => $this->input->post('buy_price'),

				'status' => $this->input->post('status'),

				'bar_code' => $this->input->post('bar_code'),

				's_no' => $this->input->post('s_no'),

				/* 'model_no' => $this->input->post('model_no'), */

				'branch_id' => $this->user_data['branch_id'],

				'updated_by' => $this->user_data['user_id'],

				'updated_on' => date('Y-m-d H:i:s')

			);

			

			if (!empty($_FILES['file']['name'])) :

				$uploaded_data = uploads(array('upload_path' => 'product', 'field' => 'file'));

				$update['image'] = $uploaded_data['file']['file_path_str'];

			endif;

			

			if ($this->product_model->update($id, $update) > 0) {

				$qty = ($this->input->post('new_quantity') != '') ? $this->input->post('new_quantity') : 0;

				

				if ($qty > 0) {

					$sc_item = array(

						'branch_id' => $this->user_data['branch_id'],

						'product_id' => $id,

						'quantity' => $qty,

						'created_by' => $this->user_data['user_id'],

						'created_on' => date('Y-m-d H:i:s'),

						'ip_address' => $_SERVER['REMOTE_ADDR']

					);

					$last_id = $this->product_model->insert_scan_product($sc_item);

					$this->product_model->update_branch_stock($this->user_data['branch_id'], $id, $qty, '+');

				}

				

				if (!empty($id)) {

					$this->update_price_log($id, $this->input->post('buy_price'), $this->input->post('sel_price'), 'From Product master');

				}

				$branches_id = $this->input->post('branch_id');

				$branch_location = $this->input->post('branch_location');

				if (!empty($branches_id)) {

					foreach ($branches_id as $key => $branch_id) {

						$update_brn_item = array(

							'brn_id' => $branch_id,

							'branch_location' => $branch_location[$key],

							'updated_by' => $this->user_data['user_id'],

							'updated_on' => date('Y-m-d H:i:s'),

							'ip_address' => $_SERVER['REMOTE_ADDR']

						);

						

						$insert_brn_item = array(

							'brn_id' => $branch_id,

							'branch_location' => $branch_location[$key],

							'product_id' => $id,

							'available_qty' => 0,

							'branch_id' => $this->user_data['branch_id'],

							'created_by' => $this->user_data['user_id'],

							'created_on' => date('Y-m-d H:i:s'),

							'ip_address' => $_SERVER['REMOTE_ADDR']

						);



						$this->product_model->update_brn_material($branch_id, $id, $update_brn_item, $insert_brn_item);

					}

				}

			}

        }

        if ($last_id > 0) {

            $_SESSION['success'] = 'Item added successfully';

        } else

            $_SESSION['error'] = 'Item not added!';



        redirect('item/add', 'refresh');

    }



    public function edit($id = '') {

        if (empty($id)) {

            show_400_error();

        }

        $user_data = get_user_data();

        $data['success'] = $this->data['success'];

        $data['error'] = $this->data['error'];

        $data['product'] = $this->product_model->get_by_id($id);

        $data['category'] = $this->product_model->category();

        $data['item_category'] = $this->product_model->item_category();

        $data['subcat'] = $this->product_model->subcat_list();

        $data['branches'] = $this->product_model->active_branch_products($id);

        $actions = $this->actions();

        $data['list_link'] = $actions['index'];

        $data['form_action'] = $actions['update'] . '/' . $data['product']->id;

        $data['add_category_link'] = base_url() . 'brand/add/';



        $this->render($data, 'product/edit');

    }



    public function update_price_log($pid, $bprice, $sprice, $tit) {

        $last = $this->product_model->get_product_last_price_log($pid);

        $last_id = '';

        if (($last->buying_price != $bprice) || ($last->selling_price != $sprice)) {

            $insert = array(

                'product_id' => $pid,

                'buying_price' => $bprice,

                'selling_price' => $sprice,

                'updated_from' => $tit,

                'brn_id' => $this->user_data['branch_id'],

                'created_by' => $this->user_data['user_id'],

                'created_on' => date('Y-m-d H:i:s'),

                'ip_address' => $_SERVER['REMOTE_ADDR']

            );

            $last_id = $this->product_model->insert_cost_log($insert);

        }

        return $last_id;

    }



    public function update($id = '') {

        if (empty($id)) {

            show_400_error();

        }

        $user_data = get_user_data();

        $prdct = $this->product_model->get_by_id($id);

        $image = $this->input->post('image');

        if (empty($image)) :

            $image = $this->input->post('hidden-file');

            if (empty($image) && !empty($prdct->image)) :

                // Remove prev. file

                unlink(FCPATH . UPLOADS . $prdct->image);

            endif;

        endif;

        

        $uploaded_data = uploads(array('upload_path' => 'product', 'field' => 'file'));

        $short_name = strtoupper(substr($this->input->post('pdt_desc'), 0, 3));

        #$prefix_wt_sub = CODE_PDT . $short_name;

        $prefix_wt_sub = $short_name;

        $code = $this->auto_generation_code_item(TBL_PDT, $prefix_wt_sub, '', 3, $id);



        $no = $this->input->post('pdt_code');

        if ($no == '') {

            $pdt_code = $code;

        } else {

            $pdt_code = $no;

        }

		

		$category = ($this->input->post('cat_name') != '') ? $this->input->post('cat_name') : null;



        $update = array(

            //'pdt_code' => $pdt_code,

            'pdt_name' => $this->input->post('pdt_desc'),
			
			'pdt_spec'	=> $this->input->post('pdt_spec'),

            'cat_id' => $category,

            'item_category' => $this->input->post('item_cat'),

            'sub_category' => $this->input->post('sub_cat'),

            'selling_price' => $this->input->post('sel_price'),

            'buying_price' => $this->input->post('buy_price'),

            'bfr_gst_buying_price' => $this->input->post('buy_price'),

            'status' => $this->input->post('status'),

            'bar_code' => $this->input->post('bar_code'),

            's_no' => $this->input->post('s_no'),

            'warranty' => $this->input->post('warranty'),

            /* 'model_no' => $this->input->post('model_no'), */

            'branch_id' => $this->user_data['branch_id'],

            'updated_by' => $user_data['user_id'],

            'updated_on' => date('Y-m-d H:i:s')

        );

        if (!empty($uploaded_data['file']['file_path_str'])) :

            $update['image'] = $uploaded_data['file']['file_path_str'];

        endif;

        

        if ($this->product_model->update($id, $update) > 0) {

            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $prdct->pdt_code, ''); 

            if (!empty($id)) {

                $this->update_price_log($id, $this->input->post('buy_price'), $this->input->post('sel_price'), 'From Product master');

            }

            $branches_id = $this->input->post('branch_id');

            $branch_location = $this->input->post('branch_location');

            if (!empty($branches_id)) {

                foreach ($branches_id as $key => $branch_id) {

                    $update_brn_item = array(

                        'brn_id' => $branch_id,

                        'branch_location' => $branch_location[$key],

                        'updated_by' => $user_data['user_id'],

                        'updated_on' => date('Y-m-d H:i:s'),

                        'ip_address' => $_SERVER['REMOTE_ADDR']

                    );



                    $brn_item = array(

                        'brn_id' => $branch_id,

                        'branch_location' => $branch_location[$key],

                        'product_id' => $id,

                        'available_qty' => 0,

                        'branch_id' => $user_data['branch_id'],

                        'created_by' => $user_data['user_id'],

                        'created_on' => date('Y-m-d H:i:s'),

                        'ip_address' => $_SERVER['REMOTE_ADDR']

                    );



                    $this->product_model->update_brn_material($branch_id, $id, $update_brn_item, $brn_item);

                }

            }



            $_SESSION['success'] = 'Item updated successfully';

        }

		else {

            $_SESSION['error'] = 'Item not updated!';

        }

        redirect('item/edit/' . $id, 'refresh');

    }



    public function view($id = '') {

        if (empty($id)) {

            show_400_error();

        }

        $data = array();

        $data['product'] = $this->product_model->get_by_id($id);

        $data['category'] = $this->product_model->category();

        $data['inventory'] = $this->product_model->get_inventory_by_id($id);

        $data['branch'] = $this->product_model->list_all_branch();

        $this->render($data, 'product/view');

    }



    public function delete($id) {

        if (empty($id)) {

            show_400_error();

        }

        $data_product = $this->product_model->get_by_id($id);

        $product = array(

            'status' => 10,

            'updated_on' => date('Y-m-d H:i:s'),

            'updated_by' => $this->user_data['user_id']

        );

        if ($this->product_model->update($id, $product) > 0) {

            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $data_product->pdt_code, 1);

            $_SESSION['success'] = 'Item deleted successfully';

        } else {

            $_SESSION['error'] = 'Item not deleted!';

        }

        redirect('item', 'refresh');

    }



    public function search_item_by_barcode($item = '') {

        $data = array();

        $data = $this->product_model->search_item_by_barcode($item);

        foreach ($data as $key => $value) {

            $value->label = $value->bar_code;

            $value->value = $value->label;

			$value->branches = $this->product_model->active_branch_products($value->id);

        }

		

        $this->ajax_response($data);

    }



    public function search_item_by_seialno($item = '') {

        $data = array();

        $data = $this->product_model->search_item_by_serialno($item);

        foreach ($data as $key => $value) {

            $value->label = $value->s_no;

            $value->value = $value->label;

			$value->branches = $this->product_model->active_branch_products($value->id);

        }

        $this->ajax_response($data);

    }



    public function category_active() {

        $data = array();

        $data = $this->category_model->list_active();

        $this->ajax_response($data);

    }



    public function get_product_by_barcode() {

        $data = array();

        if (!empty($this->input->post('bar_code'))) {

            $data = $this->product_model->get_product_by_barcode(trim($this->input->post('bar_code')));

        }

        $this->ajax_response($data);

    }



    public function check_product_code_exists() {

        echo $this->product_model->check_product_code_exists(trim($_REQUEST['pdt_code']));

        //console.log();

    }



    public function check_product_exists() {

        $product = $_REQUEST['product_name'];

        echo $this->product_model->check_product_exists($product, $_REQUEST['product_id']);

    }



    public function check_product_description_exists() {

        $brand = (!empty($_REQUEST['brand'])) ? $_REQUEST['brand'] : '';

        //$pdt_code = (!empty($_REQUEST['pdt_code'])) ? $_REQUEST['pdt_code'] : '';

        $desc = (!empty($_REQUEST['prodDesc'])) ? $_REQUEST['prodDesc'] : '';

        $id = (!empty($_REQUEST['product_id'])) ? $_REQUEST['product_id'] : '';

        $res = '';

        if (!empty($brand) || !empty($desc)) {

            $res = $this->product_model->check_product_desc_exists($brand, $desc, $id);

        }



        echo $res;

    }

    

	public function check_barcode_exists() {

        $barcode = $_REQUEST['barcode'];

        echo $this->product_model->check_barcode_exists($barcode, $_REQUEST['product_id']);

    }

	

    public function check_sno_exists() {

        $sno = $_REQUEST['sno'];

        echo $this->product_model->check_sno_exists($sno, $_REQUEST['product_id']);

    }

    

    function remove($clm = 'image', $id) {

        $prdct = $this->product_model->get_by_id($id);

        $args = array(

            $clm => ''

        );

        $this->product_model->update($id, $args);

        if ($clm == 'image') :

            // Unlink file

            unlink(FCPATH . UPLOADS . $prdct->image);

        endif;

    }



    public function get_subcat_by_category() {

        $cat = $_REQUEST['category'];

        $data = array();

        $data = $this->product_model->get_subcat_list($cat);

        $this->ajax_response($data);

    }



}



?>