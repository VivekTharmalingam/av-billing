<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Item_Supplier extends REF_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('inventory_model', '', TRUE);
        $this->load->model('product_model', '', TRUE);
        $this->load->library('upload');
    }
    public function index() {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['view_link'] = base_url() . 'supplier/view' . '/';
        $data['form_action'] = base_url() . 'item_supplier';
        $data['supplier'] = array();
        $data['product'] = '';
        if(!empty($this->input->post('item_id'))){
            $data['item_id'] = $this->input->post('item_id');
            $data['supplier'] = $this->inventory_model->get_unique_supplier_lists($data['item_id']);
            $data['product'] = $this->product_model->get_by_id($data['item_id']);
        }
//        echo $this->db->last_query();
        //$data['branch'] = $this->inventory_model->list_branch();
        $data['item'] = $this->inventory_model->list_product();
        if ($this->input->post('reset_btn')) {
            $data['search_date'] = '';
            $data['branch_id'] = '';
            $data['item_id'] = '';
            $data['supplier'] = array();
            $data['product'] = '';
        }

        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Item Wise Supplier Reports';
            $head['setting'] = $this->company_model->get_company();
            $filename = 'Item_Wise_Supplier_' . current_date();
            // Load Views
            $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
            $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
            $header = '<div></div>';
            $content = $this->load->view('item_supplier/pdf_report', $data, TRUE);
            $footer = '<div></div>';
            $this->load->helper(array('My_Pdf'));
            create_pdf($header, $content, $footer, $filename);
            exit(0);
        } elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $this->load->view('item_supplier/excel_report', $data);
        } else {
            $this->render($data, 'item_supplier/list');
        }
    }
}

?>