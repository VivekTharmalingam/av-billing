<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Banking_Report extends REF_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('bank_model', '', TRUE);
    }

    public function index() {
        $data = array();
        $data1 = '';
        $head = array();
        $data['success'] = $this->data['success'];
        $data['error'] = $this->data['error'];
        $actions = $this->actions();
        $data['form_action'] = base_url() . 'banking_report';
        $data['all_bank'] = $bank_arr = $this->bank_model->list_all();
        $curnt_bnk_id = (!empty($this->user_data['bank_account_id'])) ? $this->user_data['bank_account_id'] : $bank_arr[0]->id;
        $bank_acc_id = (!empty($this->input->post('bank_acc_id'))) ? $this->input->post('bank_acc_id') : $curnt_bnk_id;
        $search_date = (!empty($this->input->post('search_date'))) ? $this->input->post('search_date') : date('01/m/Y') . ' - ' . date('t/m/Y');
        $is_cheque = (!empty($this->input->post('is_cheque'))) ? $this->input->post('is_cheque') : '';
        $cheque_text = (!empty($this->input->post('cheque_text'))) ? $this->input->post('cheque_text') : '';
        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'Reset')) {
            $bank_acc_id = $bank_arr[0]->id;
            $search_date = date('01/m/Y') . ' - ' . date('t/m/Y');
            $is_cheque = '';$cheque_text = '';
        }

        if ((!empty($this->input->post('submit'))) && (empty($this->input->post('search_date')))) {
            $search_date = '';
            $from = '';
            $to = '';
        } else {
            $search_date_arr = explode('-', $search_date);
            $from = convert_text_date(trim($search_date_arr[0]));
            $to = convert_text_date(trim($search_date_arr[1]));
        }
        
        if(!empty($this->input->post('home_to_bankrept'))) {
            $home_to_bankrept = $this->input->post('home_to_bankrept');
            $datetext = '';$is_cheque = 'cheque';
            if($home_to_bankrept == 'TODREC') {
                $datetext = date('d/m/Y');
                $cheque_text = '1';
            } 
            else if($home_to_bankrept == 'TMRREC') {
                $datetext = date("d/m/Y", strtotime('tomorrow'));
                $cheque_text = '1';
            } 
            else if($home_to_bankrept == 'TODPAY') {
                $datetext = date('d/m/Y');
                $cheque_text = '2';
            } 
            else if($home_to_bankrept == 'TMRPAY') {
                $datetext = date("d/m/Y", strtotime('tomorrow'));
                $cheque_text = '2';
            }
            
            if($datetext != '') {
                $search_date = $datetext . ' - ' . $datetext;
                $from = convert_text_date($datetext);
                $to = convert_text_date($datetext);
            }
        }

        $data['search_date'] = $search_date;
        $data['from_date'] = $from;
        $data['to_date'] = $to;
        $data['bank_acc_id'] = $bank_acc_id;
        $data['is_cheque'] = $is_cheque;
        $data['cheque_text'] = $cheque_text;
        
        $data['banking_rept'] = $this->bank_model->get_banking_report($from, $to, $bank_acc_id, $is_cheque, $cheque_text);
        
        if ((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'pdf')) {
            $head['title'] = 'Banking Reports';
            $head['setting'] = $this->company_model->get_company();
            $filename = 'Banking_Report_' . current_date();
            // Load Views
            $data['head'] = $this->load->view('templates/pdf/pdf_head', $head, TRUE);
            $data['header'] = $this->load->view('templates/pdf/pdf_header', $head, TRUE);
            $header = '<div></div>';
            $content = $this->load->view('banking_report/pdf_report', $data, TRUE);
            $footer = '<div></div>';
            $this->load->helper(array('My_Pdf'));
            create_pdf($header, $content, $footer, $filename);
            exit(0);
        } elseif (((!empty($this->input->post('submit'))) && ($this->input->post('submit') == 'excel'))) {
            $this->load->view('banking_report/excel_report', $data);
        } else {
            $this->render($data, 'banking_report/list');
        }
    }

}

?>