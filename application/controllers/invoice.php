<?php

//error_reporting(0);

if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Invoice extends REF_Controller {



    public function __construct() {

        parent::__construct();

        $this->load->model('sales_order_model', '', TRUE);

        $this->load->model('sales_return_model', '', TRUE);

        $this->load->model('sales_payment_model', '', TRUE);

        $this->load->model('delivery_order_model', '', TRUE);

        $this->load->model('category_model', '', TRUE);

        $this->load->model('product_model', '', TRUE);

        $this->load->model('payment_type_model', '', TRUE);

        $this->load->model('customer_model', '', TRUE);

        $this->load->model('company_model', '', TRUE);

        $this->load->model('quotation_model', '', TRUE);

        $this->load->model('bank_model', '', TRUE);

        $this->load->model('exchange_order_model', '', TRUE);

        $this->load->model('services_model', '', TRUE);

        $this->load->library('Datatables');

    }



    public function index() {

        $data = array();

        $data['success'] = $this->data['success'];

        $data['error'] = $this->data['error'];

        $actions = $this->actions();

        $data['view_link'] = $actions['view'] . '/';

        $data['add_link'] = $actions['add'];

        $data['edit_link'] = $actions['edit'] . '/';

        $data['delete_link'] = $actions['delete'] . '/';

        $data['print_link'] = base_url() . 'invoice/print_pdf/';

        $data['sales'] = $this->sales_order_model->list_all();



        $user_id = $this->user_data['user_id'];

        $status = array(1, 2);

        $invoice_not = $this->sales_order_model->get_notification_by_type('Invoice', '', $user_id, $status, '');

        if (count($invoice_not) > 0) {

            for ($i = 0; $i < count($invoice_not); $i++) {

                $notification_data = array(

                    'status' => 3,

                    'updated_on' => date('Y-m-d H:i:s'),

                    'updated_by' => $this->user_data['user_id']

                );

                $this->sales_order_model->update_notification_item($notification_data, $invoice_not[$i]['not_id']);

            }

        }



        $this->render($data, 'sales_order/list');

    }



    function datatable() {

        $user_data = get_user_data();

        $this->datatables->select('@i:=@i+1 AS iterator, so.id AS soId, so.so_no AS invoice_no, DATE_FORMAT(so.so_date,"%d/%m/%Y") AS invoice_date, so.total_amt AS invoice_amount , so.status AS invoice_status, so.your_reference AS invoice_reference, CASE so.status WHEN 1 THEN "New" WHEN 2 THEN "Partially Delivered" WHEN 3 THEN "Delivered" ELSE "" END as status_str, CASE so.so_type WHEN 1 THEN c.name ELSE so.customer_name END as name', FALSE)

                ->where('so.status != 10 AND so.branch_id IN ("' . $this->user_data['branch_id'] . '")')

                ->join(TBL_CUS . ' as c', 'c.id = so.customer_id', 'left')

                ->add_column('Actions', $this->get_buttons('$1'), 'soId')

                ->from(TBL_SO . ' AS so, (SELECT @i:=0) AS foo');

        echo $this->datatables->generate();

    }



    function get_buttons($id) {

//        alert($id);

        $actions = $this->actions();

        $html = '<span class="actions">';

        if (in_array(6, $this->permission)) {

            $html .='<a class="label btn btn-warning view" href="' . base_url() . 'invoice/print_pdf/' . $id . '" title="Print Invoice Details"><span><i class="fa fa-file-pdf-o" aria-hidden="true"></i></span></a>&nbsp';

        }

        if (in_array(1, $this->permission)) {

            $html .='<a class="label btn btn-warning view" href="' . $actions['view'] . '/' . $id . '"><span>View</span></a>&nbsp';

        }

        if (in_array(3, $this->permission)) {

            $html .='<a class="label btn btn-primary edit" href="' . $actions['edit'] . '/' . $id . '"><span>Edit</span></a>&nbsp';

        }

        if (in_array(4, $this->permission)) {

            $html .='<a class="label btn btn-danger delete delete-confirm" href="#" data-confirm-content="You will not be able to recover invoice details!" data-redirect-url="' . $actions['delete'] . '/' . $id . '" data-bindtext="invoice"><span>Delete</span></a>&nbsp';

        }

        if (in_array(9, $this->permission)) {

            $html .='<a class="label btn btn-primary view" style="background: #009688; border-color: #009688;display:inline-block;margin-top:8px" data-href="' . base_url() . 'invoice/send_mail/' . $id . '" id="smart-mod-eg2" title="Send Email" data-id="' . $id . '" ><span style="line-height:15px"><i class="fa fa-send" aria-hidden="true"></i> Send Email</span></a>&nbsp';

        }

        $html.='</span>';

        return $html;

    }



    public function add($quotation_id = '') {

        $data = array();

        $actions = $this->actions();

        $user_data = get_user_data();

        $data['list_link'] = $actions['index'] . '/';

        $data['success'] = $this->data['success'];

        $data['error'] = $this->data['error'];

        $data['form_action'] = $actions['insert'];

        //$data['categories'] = $this->category_model->list_active();

        //$data['items'] = $this->product_model->list_active();



        $data['active_qtn_list'] = $this->sales_order_model->get_active_qtn_list();

        $data['exchange_order_list'] = $this->sales_order_model->get_filtered_exc();

        $data['service_list'] = $this->services_model->service_active_list();

        $data['hold_so_list'] = $this->sales_order_model->hold_so_list();

        $data['customers'] = $this->customer_model->list_active();

        $data['address'] = $this->sales_order_model->get_current_address($user_data['branch_id']);

        $data['company'] = $this->company_model->get_company();

        $data['payment_types'] = $this->payment_type_model->list_active();



        $bnk_acc_name = $this->user_model->get_active_acc($this->user_data['bank_account_id']);

        $data['bnk_acc_name'] = (count($bnk_acc_name) > 0) ? $bnk_acc_name[0]['bank_name'] : '';

        $brn_slug = $this->company_model->list_active_by_company_id($this->user_data['branch_id']);

        $inv = strtoupper(get_var_name($brn_slug[0]->branch_name));

        $data['so_no'] = $this->auto_generation_code(TBL_SO, $inv . ' ', '', 5, '', $this->user_data['branch_id']);

        $data['list_item_link'] = base_url() . 'item/';

        $data['add_item_link'] = base_url() . 'item/add/';

        $data['list_customer_link'] = base_url() . 'customer/';

        $data['add_customer_link'] = base_url() . 'customer/add/';

        $data['payment_terms'] = payment_terms();



        $data['to_inv'] = false;

        $quotation = new stdClass();

        $quotation_items = array();

        if (!empty($quotation_id)) {

            $data['to_inv'] = true;

            $quotation = $this->quotation_model->get_by_id($quotation_id);

            $quotation_items = $this->quotation_model->get_products_by_so($quotation_id);

            $data['shipping_address'] = $this->sales_order_model->shipping_address_by_id($quotation->shipping_address);

            $data['addresses'] = $this->customer_model->get_address_by_id($quotation->customer_id);

            $data['customer'] = $this->customer_model->get_by_id($quotation->customer_id);



            if ($this->sales_order_model->get_qtn_by_id($quotation_id)) {

                $_SESSION['error'] = 'Invoice already created for the quotation(' . $quotation->quo_no . ').';

                redirect('quotation', 'refresh');

            }

        }



        $so_hold_id = $this->input->post('so_hold_id');

        $data['hold_invoice_delete_url'] = base_url() . 'invoice/delete_hold_invoice/';

        if (!empty($so_hold_id)) {

            $data['to_hold'] = true;

            $quotation = $this->sales_order_model->get_invholdby_id($so_hold_id);

            $quotation_items = $this->sales_order_model->get_holditems_by_so($so_hold_id);

            $data['shipping_address'] = $this->sales_order_model->shipping_address_by_id($quotation->shipping_address);

            $data['addresses'] = $this->customer_model->get_address_by_id($quotation->customer_id);

            $data['customer'] = $this->customer_model->get_by_id($quotation->customer_id);

        }



        $data['quotation'] = $quotation;

        $data['quotation_items'] = $quotation_items;



        $exchange_order_id = $this->input->post('exchangeorder_id');

        $service_id = $this->input->post('service_id');

        if (!empty($exchange_order_id)) {

            $data['exc_order_item'] = $this->exchange_order_model->get_filtered_item_by_excids($exchange_order_id);

            $data['exchange_order_id'] = $exchange_order_id;

            $this->render($data, 'sales_order/exc_invoice_add');

        } else if (!empty($service_id)) {

            $service_invoice = $this->sales_order_model->get_by_servicebased_invoice($service_id);

            if (count($service_invoice) > 0) {

                $_SESSION['error'] = 'Sorry!.. Invoice Already Generated for this Service..';

                redirect('services', 'refresh');

            } else {

                $data['service_data'] = $this->services_model->get_by_id($service_id);

                $data['service_cost_temp'] = $this->services_model->service_costitems_byid($service_id);

                $data['external_service'] = $this->sales_order_model->get_external_services_item($service_id);

                $data['service_cost'] = array_merge($data['service_cost_temp'], $data['external_service']);

                $data['exc_items'] = $this->services_model->service_excitems_all($service_id);

                $data['inventory_items'] = $this->services_model->service_inventoryitems_byid($service_id);

                $data['service_id'] = $service_id;

                $this->render($data, 'sales_order/service_invoice_add');

            }

        } else {

            $this->render($data, 'sales_order/add');

        }

    }



    public function shipping_address_by_customer() {

        $data = array();

        $data['addresses'] = $this->customer_model->get_address_by_id($this->input->post('customer_id'));

        $data['customer'] = $this->customer_model->get_by_id($this->input->post('customer_id'));

        $this->ajax_response($data);

    }



    public function get_product_by_category() {

        $data = array();

        $data['products'] = $this->product_model->get_product_by_category($this->input->post('category_id'));

        $this->ajax_response($data);

    }



    public function get_products_by_so() {

        $so_id = $_REQUEST['so_id'];

        $data = array();

        if (!empty($so_id)) {

            $data['products'] = $this->purchase_receive_model->get_products_by_so($so_id);

            $data['so_detail'] = $this->purchase_receive_model->get_supplier_by_so($so_id);

        }

        echo json_encode($data);

    }



    public function get_promotions() {

        $data = array();

        $data = $this->product_model->get_item_promotions();

        #echo '<pre>';print_r($data);echo '</div>';

        $this->ajax_response($data);

    }



    public function search_item() {

        $product_code = $this->input->post('product_code');

        $data = array();

        $data = $this->product_model->search_item();

        $promotions = array();

        foreach ($data as $key => $value) {

            //$value->value = $value->id;

            if (!empty($product_code))

                $value->label = $value->pdt_code;

            else

                $value->label = $value->pdt_code . ' / ' . $value->pdt_name;



            $value->value = $value->label;

            $value->branch_products = $this->product_model->branch_products_stock($value->id);

        }



        $this->ajax_response($data);

    }



    public function search_exchange_item() {

        $exchange_id = $this->input->post('exchange_id');

        $edit_page = $this->input->post('edit_page');

        $edit_page_id = $this->input->post('edit_page_id');

        $data = array();

        if (!empty($exchange_id)) {

            $data = $this->exchange_order_model->get_filtered_item_by_excids($exchange_id, $edit_page, $edit_page_id);

            foreach ($data as $key => $value) {

                $value->label = $value->product_text;

                $value->value = $value->label;

            }

        }



        $this->ajax_response($data);

    }



    public function get_this_id_soqty() {

        $so_id = $this->input->post('so_id');

        $item_id = $this->input->post('item_id');

        $data = 0;

        if (!empty($so_id) && !empty($item_id)) {

            $itmdata = $this->sales_order_model->get_soqtybyid($so_id, $item_id);

            $data = (!empty($itmdata)) ? $itmdata->quantity : 0;

        }



        $this->ajax_response($data);

    }



    public function search_service_item() {

        $service_id = $this->input->post('service_id');

        $form_type = $this->input->post('form_type');

        $data = array();

        if (!empty($service_id)) {

            if ($form_type == 1) {

                $data = $this->services_model->service_excitems_all($service_id);

                foreach ($data as $key => $value) {

                    $value->label = $value->product_text;

                    $value->value = $value->label;

                }

            } else if ($form_type == 2) {

                $data = $this->services_model->service_inventoryitems_byid($service_id);

                foreach ($data as $key => $value) {

                    $value->label = $value->pdt_code;

                    $value->value = $value->label;

                    $value->branch_products = $this->product_model->branch_products_stock($value->id);

                }

            } else if ($form_type == 3) {

                $data = $this->services_model->service_inventoryitems_byid($service_id);

                foreach ($data as $key => $value) {

                    // Description gave in service add

                    $value->label = $value->product_description;

                    $value->value = $value->label;

                    $value->branch_products = $this->product_model->branch_products_stock($value->id);

                }

            }

        }



        $this->ajax_response($data);

    }



    public function search_item_description() {

        $data = array();

        $data = $this->product_model->search_item_by_brand();

        foreach ($data as $key => $value) {

            #$value->label = (!empty($value->cat_name)) ? $value->cat_name . ' - ' : '';

            $value->label = (!empty($value->cat_name)) ? $value->cat_name . ' ' : '';

            $value->label .= $value->pdt_name;

            $value->value = $value->label;

            $value->branch_products = $this->product_model->branch_products_stock($value->id);

        }

        $this->ajax_response($data);

    }



    public function active_customers() {

        $data = array();

        $data = $this->customer_model->list_active();

        /* foreach ($data as $key => $value) {

          $value->id = $value->id;

          $value->text = $value->name;

          } */

        $this->ajax_response($data);

    }



    public function insert() {
		
		//echo "<pre>"; print_r($this->input->post()); exit;
		
        $submit_hold = (!empty($this->input->post('submit_hold'))) ? $this->input->post('submit_hold') : '';

        $brn_slug = $this->company_model->branch_by_id($this->user_data['branch_id']);

        $inv = trim(strtoupper(get_var_name($brn_slug->branch_name)));

        //$so_no = ($submit_hold == '') ? $this->auto_generation_code(TBL_SO, $inv . ' ', '', 6, '', $this->user_data['branch_id']) : '';
		
		$so_no = ($this->input->post('so_no')) ? $this->input->post('so_no') : $this->auto_generation_code(TBL_SO, $inv . ' ', '', 6, '', $this->user_data['branch_id']);
		
        if ($this->input->post('gst_type') == '1') {

            $gst_amount = $this->calculate_inclusive_gst($this->input->post('total_amt'));

        } else {

            $gst_amount = $this->input->post('gst_amount');

        }



        $exchange_order_id = (!empty($this->input->post('exchange_order_id'))) ? implode(',', $this->input->post('exchange_order_id')) : '';



        $service_id = (!empty($this->input->post('service_id'))) ? $this->input->post('service_id') : '';



        $invoice_hold_id = (!empty($this->input->post('invoice_hold_id'))) ? $this->input->post('invoice_hold_id') : '';



        $customer_id = $this->input->post('customer_name');

        $customer_name = '';

        if (($customer_id == 'cash') && ($this->input->post('cash_customer_name') != ''))

            $customer_name = $this->input->post('cash_customer_name');

        else if ($customer_id == 'cash')

            $customer_name = 'Cash Customer';



        $insert = array(

            'so_no' => $this->input->post('so_no'), //$so_no,

            'so_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('so_date')))),

            'leave_date' => (!empty($this->input->post('leave_date'))) ? 1 : 0,

            'so_type' => ($this->input->post('customer_name') == 'cash') ? 2 : 1,

            'customer_name' => $customer_name,

            'remarks' => $this->input->post('remarks'),

            'status' => 1,

            'sub_total' => $this->input->post('sub_total'),

            'discount' => $this->input->post('discount'),

            'discount_type' => $this->input->post('discount_type'),

            'discount_amount' => $this->input->post('discount_amt'),

            'gst' => $this->input->post('gst_type'),

            'gst_amount' => $gst_amount,

            'gst_percentage' => $this->input->post('hidden_gst'),

            'total_amt' => $this->input->post('total_amt'),

            #'payment_terms' => $this->input->post('payment_terms'),

            'your_reference' => $this->input->post('your_reference'),

            'branch_id' => $this->user_data['branch_id'],

            'created_by' => $this->user_data['user_id'],

            'created_on' => date('Y-m-d H:i:s')

        );



        if (!empty($this->input->post('quotation_id'))) {

            $insert['quotation_id'] = $this->input->post('quotation_id');

            $insert['invoice_from'] = 2;

        }



        if ($exchange_order_id != '') {

            $insert['exchange_order_id'] = $exchange_order_id;

            $insert['invoice_from'] = 3;

        }



        if ($service_id != '') {

            $insert['service_id'] = $service_id;

            $insert['invoice_from'] = 4;

        }



        if ($customer_id != 'cash') {

            $insert['customer_id'] = $customer_id;

            $ship_to = $this->input->post('ship_to');

            $location_name = $this->input->post('location_name');

            $address = $this->input->post('address');

            if ($ship_to != "") {

                $insert['shipping_address'] = $this->input->post('ship_to');

            } else if (($ship_to == "") && ($location_name != '')) {

                $shipping_address = array(

                    'customer_id' => $customer_id,

                    'location_name' => $location_name,

                    'address' => $address,

                    'is_default' => 1,

                    'status' => 1,

                    'branch_id' => $this->user_data['branch_id'],

                    'created_by' => $this->user_data['user_id'],

                    'created_on' => date('Y-m-d H:i:s')

                );



                $insert['shipping_address'] = $this->customer_model->insert_ship_address($shipping_address);

                if ($insert['shipping_address'] > 0) {

                    $update_ship = array(

                        'is_default' => 0,

                        'branch_id' => $this->user_data['branch_id'],

                        'updated_by' => $this->user_data['user_id'],

                        'updated_on' => date('Y-m-d H:i:s')

                    );



                    $this->customer_model->reset_customer_default_address($customer_id, $insert['shipping_address'], $update_ship);

                }

            }

        }

		

		$is_price_changed = 0;

        $last_id = ($submit_hold == '') ? $this->sales_order_model->insert($insert) : $this->sales_order_model->insert_hold_so($insert);

        if ($last_id > 0) {

            $_SESSION['success'] = ($submit_hold == '') ? 'Invoice added successfully' : 'Invoice has been holded successfully';



            $category_id = $this->input->post('category_id');

            $product_id = $this->input->post('product_id');

            $product_code = $this->input->post('product_code');

            $serial_no = $this->input->post('serial_no');

            $product_description = $this->input->post('product_description');

            $quantity = $this->input->post('quantity');

            $price = $this->input->post('price');

            $unit_cost = $this->input->post('cost');

            $item_discount = $this->input->post('item_discount');

            $item_discount_amt = $this->input->post('item_discount_amt');

            $item_discount_type = $this->input->post('item_discount_type');

            $amount = $this->input->post('amount');

            $show_in_print = $this->input->post('show_in_print');

            $ser_invventory_id = ($service_id != '') ? $this->input->post('ser_invventory_id') : 0;

            foreach ($product_id as $key => $product) {

				if (!empty($product_id[$key])) {

					$profit = $quantity[$key] * ($price[$key] - $unit_cost[$key]);

                    $profit -= (!empty($item_discount_amt[$key])) ? $item_discount_amt[$key] : 0;

                    $product_id_txt = ($service_id == '') ? $product_id[$key] : $ser_invventory_id[$key];



                    if (($service_id != '') && (empty($show_in_print[$key]))) {

                        $show_price_in_print = 0;

                    } else {

                        $show_price_in_print = 1;

                    }

					$product_info = $this->product_model->get_by_id($product_id[$key]);

					if ($product_info->selling_price != $price[$key]) {

						$price_changed = 1;

						$is_price_changed = 1;

					}

					else {

						$price_changed = 0;

					}

                    $item = array(

                        'so_id' => $last_id,

                        'exc_form_type' => ($exchange_order_id != '') ? 2 : 0,

                        'service_form_type' => ($service_id != '') ? 2 : 0,

                        'category_id' => $category_id[$key],

                        'item_id' => $product_id_txt,

                        'so_item_codetxt' => $product_code[$key],

                        'serial_no' => $serial_no[$key],

                        'product_description' => $product_description[$key],

                        'price' => $price[$key],

						'price_changed' => $price_changed,

                        'unit_cost' => $unit_cost[$key],

                        'quantity' => $quantity[$key],

                        'discount' => $item_discount[$key],

                        'discount_type' => (!empty($item_discount_type[$key])) ? $item_discount_type[$key] : 0,

                        'discount_amount' => $item_discount_amt[$key],

                        'total' => $amount[$key],

                        'show_in_print' => $show_price_in_print,

                        'item_profit' => $profit,

                        'status' => 1,

                        'branch_id' => $this->user_data['branch_id'],

                        'created_by' => $this->user_data['user_id'],

                        'created_on' => date('Y-m-d H:i:s')

                    );

					

                    if ($submit_hold == '') {

                        if ($this->sales_order_model->insert_item($item)) {

                            $this->product_model->update_branch_stock($this->user_data['branch_id'], $product_id[$key], $quantity[$key]);

                        }



                        if ($exchange_order_id != "") {

                            $log_txt = "From Exchange Order";

                        } else if ($service_id != "") {

                            $log_txt = "From Service";

                        } else {

                            $log_txt = "From Invoice";

                        }

                        //Insert Item Cost Log

                        $this->update_price_log($product_id[$key], $unit_cost[$key], $price[$key], $log_txt);

                    } else {

                        $this->sales_order_model->insert_item_hold_so($item);

                    }

                }

            }



            /*** Exchange Order || Service Exchange Order Block ***/

            $ser_exc_itemid = ($exchange_order_id != "") ? $this->input->post('exc_product_id') : $this->input->post('ser_exc_itemid');

            $exc_product_code = $this->input->post('exc_product_code');

            $exc_serial_no = $this->input->post('exc_serial_no');

            $exc_product_description = $this->input->post('exc_product_description');

            $exc_quantity = $this->input->post('exc_quantity');

            $exc_price = $this->input->post('exc_price');

            $exc_unit_cost = $this->input->post('exc_cost');

            $exc_item_discount = $this->input->post('exc_item_discount');

            $exc_item_discount_amt = $this->input->post('exc_item_discount_amt');

            $exc_item_discount_type = $this->input->post('exc_item_discount_type');

            $exc_amount = $this->input->post('exc_amount');

            $exc_show_in_print = $this->input->post('exc_show_in_print');

            foreach ($ser_exc_itemid as $key => $product) {

                if (!empty($ser_exc_itemid[$key])) {

                    $profit = $exc_quantity[$key] * ($exc_price[$key] - $exc_unit_cost[$key]);

					$profit -= (!empty($exc_item_discount_amt[$key])) ? $exc_item_discount_amt[$key] : 0;

					

                    if (($service_id != '') && (empty($exc_show_in_print[$key]))) {

                        $show_price_in_print = 0;

                    } else {

                        $show_price_in_print = 1;

                    }

					

					$product_info = $this->exchange_order_model->get_exchange_order_item_by_id($ser_exc_itemid[$key]);

					if ($product_info->exr_price != $exc_price[$key]) {

						$price_changed = 1;

						$is_price_changed = 1;

					}

					else {

						$price_changed = 0;

					}



                    $item = array(

                        'so_id' => $last_id,

                        'exc_form_type' => ($exchange_order_id != '') ? 1 : 0,

                        'service_form_type' => ($service_id != '') ? 1 : 0,

                        'item_id' => $ser_exc_itemid[$key],

                        'so_item_codetxt' => $exc_product_code[$key],

                        'serial_no' => $exc_serial_no[$key],

                        'product_description' => $exc_product_description[$key],

                        'price' => $exc_price[$key],

						'price_changed' => $price_changed,

                        'unit_cost' => $exc_unit_cost[$key],

                        'quantity' => $exc_quantity[$key],

                        'discount' => $exc_item_discount[$key],

                        'discount_type' => (!empty($exc_item_discount_type[$key])) ? $exc_item_discount_type[$key] : 0,

                        'discount_amount' => $exc_item_discount_amt[$key],

                        'total' => $exc_amount[$key],

                        'show_in_print' => $show_price_in_print,

                        'item_profit' => $profit,

                        'status' => 1,

                        'branch_id' => $this->user_data['branch_id'],

                        'created_by' => $this->user_data['user_id'],

                        'created_on' => date('Y-m-d H:i:s')

                    );



                    if ($submit_hold == '') {

                        $this->sales_order_model->insert_item($item);



                        if ($exchange_order_id != "") {

                            $log_txt = "From Exchange Order";

                        } else if ($service_id != "") {

                            $log_txt = "From Service";

                        }

                        //Insert Item Cost Log

                        $this->update_price_log($ser_exc_itemid[$key], $exc_unit_cost[$key], $exc_price[$key], $log_txt);

                    } else {

                        $this->sales_order_model->insert_item_hold_so($item);

                    }

                }

            }

            /*             * * End ** */



            /*             * * Service Cost Block ** */

            $service_cost_id = $this->input->post('service_cost_id');

            $serv_product_code = $this->input->post('serv_product_code');

            $serv_amount = $this->input->post('serv_amount');

            $external_cost = $this->input->post('external_cost');

            $our_cost = $this->input->post('our_cost');

            foreach ($serv_product_code as $key => $product) {

//                if (!empty($serv_product_code[$key])) {

                $profit = $serv_amount[$key] - $external_cost[$key];

				$product_info = $this->services_model->get_service_item_byid($service_cost_id[$key]);

				if ($product_info->total != $serv_amount[$key]) {

					$price_changed = 1;

					$is_price_changed = 1;

				}

				else {

					$price_changed = 0;

				}

				

                $item = array(

                    'so_id' => $last_id,

                    'service_form_type' => 3,

                    'item_id' => $service_cost_id[$key],

                    'product_description' => $serv_product_code[$key],

                    'external_cost' => $external_cost[$key],

                    'our_cost' => $our_cost[$key],

                    'total' => $serv_amount[$key],

					'price_changed' => $price_changed,

                    'item_profit' => $profit,

                    'status' => 1,

                    'branch_id' => $this->user_data['branch_id'],

                    'created_by' => $this->user_data['user_id'],

                    'created_on' => date('Y-m-d H:i:s')

                );



                if ($submit_hold == '') {

                    $this->sales_order_model->insert_item($item);

                } else {

                    $this->sales_order_model->insert_item_hold_so($item);

                }

//                }

            }

            /*             * * End ** */

			

			/* Log report start */

			$method_name_txt = ($submit_hold == '') ? $this->log_method_name : 'Hold';

			$method_name_txt .= ($is_price_changed) ? ' - Price Changed' : '';

            $page_txt = ($submit_hold == '') ? 'view' : 'add';

            $this->insert_log($this->log_controler_name, $method_name_txt, $page_txt, $last_id, $so_no, '');

			/* Log report end */

			

            if ($submit_hold == '') {

                /*                 * * Set Notification ** */

                $notification_user = $this->sales_order_model->get_notification_user_list();

                for ($u = 0; $u < count($notification_user); $u++) {

                    $ntn_item = array(

                        'type_txt' => 'Invoice',

                        'type_id' => $last_id,

                        'user_id' => $notification_user[$u]['id'],

                        'status' => 1,

                        'branch_id' => $this->user_data['branch_id'],

                        'created_by' => $this->user_data['user_id'],

                        'created_on' => date('Y-m-d H:i:s')

                    );

                    $this->sales_order_model->insert_notification_items($ntn_item);

                }

                /*                 * * End ** */



                if ($this->input->post('payment') == 'pay_now') {

                    $upd = array(

                        'payment_group_type' => $this->input->post('data_group'),

                        'updated_by' => $this->user_data['user_id'],

                        'updated_on' => date('Y-m-d H:i:s')

                    );

                    $this->sales_order_model->update($last_id, $upd);

                }

                if ($this->input->post('payment') == 'pay_now' && $this->input->post('data_group') != '3') {

                    $prefix_wt_sub = CODE_SPAY;

                    $sales_payment_code = $this->auto_generation_code(TBL_SPM, $prefix_wt_sub, '', '', '');

                    $sales_payment = array(

                        'pay_no' => $sales_payment_code,

                        'so_id' => $last_id,

                        'pay_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('so_date')))),

                        'pay_amt' => $this->input->post('total_amt'),

                        'pay_pay_type' => $this->input->post('payment_type'),

                        'pay_pay_no' => '',

                        'pay_pay_bank' => '',

                        'pay_pay_date' => NULL,

                        'pay_dd_no' => '',

                        'pay_dd_date' => NULL,

                        'payment_status' => 1,

                        'bank_acc_id' => $this->user_data['bank_account_id'],

                        'status' => 1,

                        'brn_id' => $this->user_data['branch_id'],

                        'created_by' => $this->user_data['user_id'],

                        'created_on' => date('Y-m-d H:i:s'),

                        'ip_address' => $_SERVER['REMOTE_ADDR']

                    );

                    $payment_type = $this->payment_type_model->get_by_id($this->input->post('payment_type'));

                    if ($payment_type->is_cheque == 1) {

                        $sales_payment['pay_pay_no'] = $this->input->post('cheque_acc_no');

                        $sales_payment['pay_pay_bank'] = $this->input->post('cheque_bank_name');

                        $sales_payment['pay_pay_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('cheque_date'))));

                    }

                    /* else if ($this->input->post('payment_type') == '3') {

                      $sales_payment['pay_dd_no'] = $this->input->post('dd_no');

                      $sales_payment['pay_dd_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('dd_date'))));

                      } */

                    if ($payidtxt = $this->sales_payment_model->insert($sales_payment)) {

                        $this->insert_log('invoice_payment', 'insert', 'view', $payidtxt, $so_no, '');

                        // Cash group payment will be added in petty cash

                        if (!in_array($payment_type->group_name, array(1, 3))) {

                            $update_data = array(

                                'updated_by' => $this->user_data['user_id'],

                                'updated_on' => date('Y-m-d H:i:s')

                            );

                            $this->bank_model->update_bank_amount($this->user_data['bank_account_id'], $update_data, $this->input->post('total_amt'), '+');

                        }



                        $update_status = array();

                        $update_status['payment_status'] = 1;

                        $this->sales_payment_model->update_payment_status($last_id, $update_status);

                    }

                }



                /*                 * * Remove Invoice Hold Items ** */

                if ($invoice_hold_id != "") {

                    $this->sales_order_model->delete_so_hold_item($invoice_hold_id);

                    $this->sales_order_model->delete_so_hold($invoice_hold_id);

                }

                /*                 * * End ** */



                redirect('invoice/print_pdf/' . $last_id, 'refresh');

            } else {

                redirect('invoice/add', 'refresh');

            }

        } else {

            $_SESSION['error'] = 'Invoice not added!';

            redirect('invoice/add', 'refresh');

        }

    }



    public function print_pdf($id, $from_list = '') {

        $data = array();

        $actions = $this->actions();

        $data['browser'] = browser_info();

        $data['list_link'] = $actions['index'];

        if (!empty($from_list))

            $data['redirect_link'] = $actions['index'];

        else

            $data['redirect_link'] = $actions['add'];



        $data['success'] = $this->data['success'];

        $data['error'] = $this->data['error'];

        $data['invoice_link'] = base_url() . 'invoice/pdf/' . $id;

        $data['receipt_link'] = base_url() . 'invoice/pdf_receipt/' . $id;

        $data['do_link'] = base_url() . 'invoice/invoice_to_delivery_order/' . $id;

        $data['so_id'] = $id;

        $data['sales_order'] = $this->sales_order_model->get_by_id($id);

        //$data['thermal'] = $this->thermal($id);

        $data['thermal_src'] = base_url() . 'invoice/thermal/' . $id;

        $this->render($data, 'sales_order/print');

    }



    public function invoice_to_delivery_order($invoice_id = '') {

        if (empty($invoice_id)) {

            show_400_error();

        }



        $sales_order = $this->sales_order_model->get_by_id($invoice_id);

        $branch = $this->company_model->branch_by_id($sales_order->branch_id);

        $invoice_id = $sales_order->id;

        if ($sales_order->status != 3) {

            /* $brn_slug = $this->company_model->list_active_by_company_id($this->user_data['branch_id']);

              $inv = strtoupper(get_var_name($brn_slug[0]->branch_name));

              $do_no = $this->auto_generation_code(TBL_SO, $inv . ' ', '', 5, '', $this->user_data['branch_id']); */



            //$do_no = $this->auto_generation_code(TBL_DO, CODE_DO, '', 6, '');

            $do_no = str_replace($branch->branch_name, 'DO', $sales_order->so_no);

//            $items = $this->delivery_order_model->items_by_so_id($invoice_id);

            $items = $this->delivery_order_model->items_by_allso_id($invoice_id);

            $delivery = array(

                'so_id' => $invoice_id,

                'do_no' => $do_no,

                'do_date' => date('Y-m-d H:i:s'),

                'status' => 3,

                'branch_id' => $this->user_data['branch_id'],

                'created_by' => $this->user_data['user_id'],

                'created_on' => date('Y-m-d H:i:s')

            );

            $delivery_id = $this->delivery_order_model->insert($delivery);

            $this->insert_log('delivery_orders', 'invoice to deliveryorder', 'view', $invoice_id, $do_no, '');



            foreach ($items as $key => $value) {

                $quantity = $value->quantity - $value->delivered_qty;

                if ($quantity <= 0)

                    continue;



                $item = array(

                    'do_id' => $delivery_id,

                    'so_item_auto_id' => $value->id,

                    'category_id' => $value->cid,

                    'item_id' => $value->item_id,

                    'quantity' => $quantity,

                    'status' => 1,

                    'branch_id' => $this->user_data['branch_id'],

                    'created_by' => $this->user_data['user_id'],

                    'created_on' => date('Y-m-d H:i:s')

                );



                $this->delivery_order_model->insert_item($item);

            }



            $invoice = array(

                'status' => 3,

                'updated_by' => $this->user_data['user_id'],

                'updated_on' => date('Y-m-d H:i:s')

            );



            $this->sales_order_model->update($invoice_id, $invoice);

        }



        $data = array();

        $data['sales_order'] = $this->sales_order_model->get_by_id($invoice_id);

        $data['delivery_order'] = $this->delivery_order_model->get_by_invoice_id($invoice_id);

        $data['do_items'] = $this->delivery_order_model->delivered_items_by_inv_allid($invoice_id);



        $data['shipping_address'] = $this->sales_order_model->shipping_address_by_id($data['sales_order']->shipping_address);

        $data['user_data'] = get_user_data();

        $data['branch'] = $this->company_model->branch_by_id($data['sales_order']->branch_id);

        $data['branches'] = $this->company_model->branch_list_active();

        $data['branch'] = $this->company_model->branch_by_id($user_data['branch_id']);

        $data['company'] = $this->company_model->get_company();

        $filename = 'DELIVERY_' . $data['sales_order']->so_no;

        $filename = strtoupper($filename);



        // Load Views

        $header = $this->load->view('delivery_order/pdf_header', $data, TRUE); //'<div></div>';

        $content = $this->load->view('delivery_order/pdf', $data, TRUE);

        $footer = $this->load->view('delivery_order/pdf_footer', $data, TRUE);



        #echo $header.$content.$footer;exit;

        $this->load->helper(array('My_Pdf'));

        delivery_order_pdf($header, $content, $footer, $filename);

        exit(0);

    }



    public function edit($id = '') {

        if (empty($id)) {

            show_400_error();

        }



        $data = array();

        $actions = $this->actions();

        $user_data = get_user_data();

        $data['form_action'] = $actions['update'];

        $data['list_link'] = $actions['index'];

        $data['sales_order'] = $this->sales_order_model->get_by_id($id);

        $data['payment'] = $this->sales_payment_model->get_by_so_id($id);

        $data['shipping_address'] = $this->sales_order_model->shipping_address_by_id($data['sales_order']->shipping_address);

        $data['addresses'] = $this->customer_model->get_address_by_id($data['sales_order']->customer_id);

        $data['customer'] = $this->customer_model->get_by_id($data['sales_order']->customer_id);

        $data['address'] = $this->sales_order_model->get_current_address($user_data['branch_id']);

        $data['success'] = $this->data['success'];

        $data['error'] = $this->data['error'];

        $data['categories'] = $this->category_model->list_active();

        $data['items'] = $this->product_model->list_active();

        $data['customers'] = $this->customer_model->list_active();

        $data['company'] = $this->company_model->get_company();

        $data['payment_types'] = $this->payment_type_model->list_active();

        $data['exchange_order_list'] = $this->exchange_order_model->list_all();

//        $data['service_list'] = $this->services_model->service_active_list();

        $data['service_list'] = $this->services_model->all_service_active_list();

        #$data['payment'] = $this->payment_type_model->get_by_id($data['payment']->pay_pay_type);

        $data['payment_terms'] = payment_terms();



        $bnk_acc_name = $this->user_model->get_active_acc($this->user_data['bank_account_id']);

        $data['bnk_acc_name'] = (count($bnk_acc_name) > 0) ? $bnk_acc_name[0]['bank_name'] : '';



        $data['list_item_link'] = base_url() . 'item/';

        $data['add_item_link'] = base_url() . 'item/add/';

        $data['list_customer_link'] = base_url() . 'customer/';

        $data['add_customer_link'] = base_url() . 'customer/add/';

        if ($data['sales_order']->invoice_from == 4) {

            $data['exc_items_service'] = $this->services_model->service_excitems_all($data['sales_order']->service_id);

            $data['inventory_items_service'] = $this->services_model->service_inventoryitems_byid($data['sales_order']->service_id);

            $data['exc_items'] = $this->sales_order_model->get_service_products_by_so($id, 1);

            $data['inventory_items'] = $this->sales_order_model->get_service_products_by_so($id, 2);

            $data['service_cost'] = $this->sales_order_model->get_service_products_by_so($id, 3);

			$this->render($data, 'sales_order/service_invoice_edit');

        } else if ($data['sales_order']->invoice_from == 3) {

            $data['exc_items'] = $this->sales_order_model->get_exc_soitem_by($id, 1);

            $data['inventory_items'] = $this->sales_order_model->get_exc_soitem_by($id, 2);

            $this->render($data, 'sales_order/exc_invoice_edit');

        } else {

            $data['sales_order_items'] = $this->sales_order_model->get_products_by_so($id);

            $this->render($data, 'sales_order/edit');

        }

    }



    public function update() {

		#echo '<pre>';print_r($this->input->post());echo '</pre>';exit;

        $id = $this->input->post('so_id');

        $user_data = get_user_data();

        $gst_type = (!empty($this->input->post('gst_type'))) ? 1 : 0;

        $customer_id = $this->input->post('customer_name');

        $customer_name = '';

        if (($customer_id == 'cash') && ($this->input->post('cash_customer_name') != ''))

            $customer_name = $this->input->post('cash_customer_name');

        else if ($customer_id == 'cash')

            $customer_name = 'Cash Customer';



        if ($this->input->post('gst_type') == '1') {

            $gst_amount = $this->calculate_inclusive_gst($this->input->post('total_amt'));

        } else {

            $gst_amount = $this->input->post('gst_amount');

        }



        $exchange_order_id = (!empty($this->input->post('exchange_order_id'))) ? $this->input->post('exchange_order_id') : '';



        $service_id = (!empty($this->input->post('service_id'))) ? $this->input->post('service_id') : '';



        $update = array(

            'so_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('so_date')))),

            'leave_date' => (!empty($this->input->post('leave_date'))) ? 1 : 0,

            'so_type' => ($this->input->post('customer_name') == 'cash') ? 2 : 1,

            'customer_id' => ($customer_id != 'cash') ? $customer_id : NULL,

            'customer_name' => $customer_name,

            'remarks' => $this->input->post('remarks'),

            'sub_total' => $this->input->post('sub_total'),

            'discount' => $this->input->post('discount'),

            'discount_type' => $this->input->post('discount_type'),

            'discount_amount' => $this->input->post('discount_amt'),

            'gst' => $this->input->post('gst_type'),

            'gst_amount' => $gst_amount,

            'gst_percentage' => $this->input->post('hidden_gst'),

            'total_amt' => $this->input->post('total_amt'),

            #'payment_terms' => $this->input->post('payment_terms'),

            'your_reference' => $this->input->post('your_reference'),

            'branch_id' => $user_data['branch_id'],

            'updated_by' => $user_data['user_id'],

            'updated_on' => date('Y-m-d H:i:s')

        );



        if (!empty($this->input->post('do_date'))) {

            $update['do_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('do_date'))));

        }



        /* To update shipping address start */

        if ($customer_id != 'cash') {

            $ship_to = $this->input->post('ship_to');

            $location_name = $this->input->post('location_name');

            $address = $this->input->post('address');

            if ($ship_to != "") {

                $update['shipping_address'] = $this->input->post('ship_to');

            } else if (($ship_to == "") && ($location_name != '')) {

                $shipping_address = array(

                    'customer_id' => $customer_id,

                    'location_name' => $location_name,

                    'address' => $address,

                    'is_default' => 1,

                    'status' => 1,

                    'branch_id' => $user_data['branch_id'],

                    'created_by' => $user_data['user_id'],

                    'created_on' => date('Y-m-d H:i:s')

                );



                $update['shipping_address'] = $this->customer_model->insert_ship_address($shipping_address);

                if ($update['shipping_address'] > 0) {

                    $update_ship = array(

                        'is_default' => 0,

                        'branch_id' => $user_data['branch_id'],

                        'updated_by' => $user_data['user_id'],

                        'updated_on' => date('Y-m-d H:i:s')

                    );



                    $this->customer_model->reset_customer_default_address($customer_id, $update['shipping_address'], $update_ship);

                }

            } else {

                $update['shipping_address'] = NULL;

            }

        }

        /* To update shipping address end */

		

		$is_price_changed = 0;

        $affected_rows = $this->sales_order_model->update($id, $update);

		$data_sales_order = $this->sales_order_model->get_by_id($id);

        if ($affected_rows > 0) {

            $_SESSION['success'] = 'Invoice updated successfully';



            // Update existing product start

            $hidden_soitem_id = $this->input->post('hidden_soitem_id');

            $category_id = $this->input->post('category_id');

            $product_id = $this->input->post('product_id');

            $product_code = $this->input->post('product_code');

            $product_description = $this->input->post('product_description');

            $serial_no = $this->input->post('serial_no');

            $quantity = $this->input->post('quantity');

            $old_quantity = $this->input->post('old_quantity');

            $price = $this->input->post('price');

            $unit_cost = $this->input->post('cost');

            $item_discount = $this->input->post('item_discount');

            $item_discount_amt = $this->input->post('item_discount_amt');

            $item_discount_type = $this->input->post('item_discount_type');

            $amount = $this->input->post('amount');

            $show_in_print = $this->input->post('show_in_print');

            $ser_invventory_id = ($service_id != '') ? $this->input->post('ser_invventory_id') : 0;



            foreach ($hidden_soitem_id as $key => $so_item_id) {

                // Old sales order item

                $old_so_item = $this->sales_order_model->get_so_item($so_item_id);

				if ($old_so_item->price != $price[$key]) {

					$price_changed = 1;

					$is_price_changed = 1;

				}

				else {

					$price_changed = 0;

				}

				

                $product_id_txt = ($service_id == '') ? $product_id[$key] : $ser_invventory_id[$key];

                $profit = $quantity[$key] * ($price[$key] - $unit_cost[$key]);

				$profit -= (!empty($item_discount_amt[$key])) ? $item_discount_amt[$key] : 0;

				

                if (($service_id != '') && (empty($show_in_print[$key]))) {

                    $show_price_in_print = 0;

                } else {

                    $show_price_in_print = 1;

                }

				

                if ($old_so_item->item_id == $product_id_txt) {

                    $item = array(

                        'category_id' => $category_id[$key],

                        'so_item_codetxt' => $product_code[$key],

                        'product_description' => $product_description[$key],

                        'serial_no' => $serial_no[$key],

                        'price' => $price[$key],

						'price_changed' => $price_changed,

                        'unit_cost' => $unit_cost[$key],

                        'quantity' => $quantity[$key],

						'discount' => $item_discount[$key],

                        'discount_type' => (!empty($item_discount_type[$key])) ? $item_discount_type[$key] : 0,

                        'discount_amount' => $item_discount_amt[$key],

                        'total' => $amount[$key],

                        'show_in_print' => $show_price_in_print,

                        'item_profit' => $profit,

                        'status' => 1,

                        'branch_id' => $user_data['branch_id'],

                        'updated_by' => $user_data['user_id'],

                        'updated_on' => date('Y-m-d H:i:s')

                    );

					

                    if ($this->sales_order_model->update_item($item, $so_item_id)) {

                        $update_quantity = $quantity[$key] - $old_so_item->quantity;

                        $this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $update_quantity, '-');

                    }

                } else {

                    $item = array(

                        'so_id' => $id,

                        'exc_form_type' => ($exchange_order_id != '') ? 2 : 0,

                        'service_form_type' => ($service_id != '') ? 2 : 0,

                        'category_id' => $category_id[$key],

                        'so_item_codetxt' => $product_code[$key],

                        'item_id' => $product_id_txt,

                        'product_description' => $product_description[$key],

                        'serial_no' => $serial_no[$key],

                        'price' => $price[$key],

						'price_changed' => $price_changed,

                        'unit_cost' => $unit_cost[$key],

                        'quantity' => $quantity[$key],

						'discount' => $item_discount[$key],

                        'discount_type' => (!empty($item_discount_type[$key])) ? $item_discount_type[$key] : 0,

                        'discount_amount' => $item_discount_amt[$key],

                        'total' => $amount[$key],

                        'show_in_print' => $show_price_in_print,

                        'item_profit' => $profit,

                        'status' => 1,

                        'branch_id' => $user_data['branch_id'],

                        'updated_by' => $user_data['user_id'],

                        'updated_on' => date('Y-m-d H:i:s')

                    );

					

                    if ($this->sales_order_model->insert_item($item)) {



                        if ($service_id != "") {

                            $get_pro_itemid = $this->services_model->get_service_item_byid($old_so_item->item_id);

                            $old_item_id_txt = $get_pro_itemid->item_id;

                        } else {

                            $old_item_id_txt = $old_so_item->item_id;

                        }



                        /* To update old item stock also delete from sales order items start */

                        $this->product_model->update_branch_stock($user_data['branch_id'], $old_item_id_txt, $old_so_item->quantity, '+');

                        $this->sales_order_model->delete_so_item($so_item_id);

                        /* To update old item stock also delete from sales order items end */



                        // To update new item quantity in stock

                        $this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $quantity[$key], '-');

                    }



                    if ($exchange_order_id != "") {

                        $log_txt = "From Exchange Order";

                    } else if ($service_id != "") {

                        $log_txt = "From Service";

                    } else {

                        $log_txt = "From Invoice";

                    }

                    //Insert Item Cost Log

                    $this->update_price_log($product_id[$key], $unit_cost[$key], $price[$key], $log_txt);

                }

            }

            // Update existing product end

            // Delete existing row start

            if ($exchange_order_id != "") {

                $delete_items = $this->sales_order_model->get_removed_so_excitems($id, $product_id);

            } else if ($service_id != "") {

                $delete_items = $this->sales_order_model->get_removed_so_items_service($id, $ser_invventory_id);

            } else {

                $delete_items = $this->sales_order_model->get_removed_so_items($id, $product_id);

            }



            foreach ($delete_items as $key => $remove_item) {

                if ($service_id != "") {

                    $get_pro_itemid = $this->services_model->get_service_item_byid($remove_item->item_id);

                    $old_item_id_txt = $get_pro_itemid->item_id;

                } else {

                    $old_item_id_txt = $remove_item->item_id;

                }

                if ($this->sales_order_model->delete_so_item($remove_item->id)) {

                    $this->product_model->update_branch_stock($user_data['branch_id'], $old_item_id_txt, $remove_item->quantity, '+');

                }

            }

            // Delete existing row end

			

            // Add newly added product start

            if (!empty($this->input->post('new_quantity'))) {

                $category_id = $this->input->post('new_category_id');

                $product_id = $this->input->post('new_product_id');

                $product_description = $this->input->post('new_product_description');

                $serial_no = $this->input->post('new_serial_no');

                $product_code = $this->input->post('new_product_code');

                $quantity = $this->input->post('new_quantity');

                $price = $this->input->post('new_price');

                $unit_cost = $this->input->post('new_cost');

                $item_discount = $this->input->post('new_item_discount');

				$item_discount_amt = $this->input->post('new_item_discount_amt');

				$item_discount_type = $this->input->post('new_item_discount_type');

                $amount = $this->input->post('new_amount');

                $show_in_print = $this->input->post('new_show_in_print');

                $ser_invventory_id = ($service_id != '') ? $this->input->post('new_ser_invventory_id') : 0;

                foreach ($product_id as $key => $pro_txtid) {

                    if (!empty($product_id[$key])) {

                        $profit = $quantity[$key] * ($price[$key] - $unit_cost[$key]);

						$profit -= (!empty($item_discount_amt[$key])) ? $item_discount_amt[$key] : 0;

						

                        $product_id_txt = ($service_id == '') ? $product_id[$key] : $ser_invventory_id[$key];



                        if (($service_id != '') && (empty($show_in_print[$key]))) {

                            $show_price_in_print = 0;

                        } else {

                            $show_price_in_print = 1;

                        }

						

						$product_info = $this->product_model->get_by_id($product_id[$key]);

						if ($product_info->selling_price != $price[$key]) {

							$price_changed = 1;

							$is_price_changed = 1;

						}

						else {

							$price_changed = 0;

						}

						

                        $item = array(

                            'so_id' => $id,

                            'exc_form_type' => ($exchange_order_id != '') ? 2 : 0,

                            'service_form_type' => ($service_id != '') ? 2 : 0,

                            'category_id' => $category_id[$key],

                            'so_item_codetxt' => $product_code[$key],

                            'item_id' => $product_id_txt,

                            'product_description' => $product_description[$key],

                            'serial_no' => $serial_no[$key],

                            'price' => $price[$key],

							'price_changed' => $price_changed,

                            'unit_cost' => $unit_cost[$key],

                            'quantity' => $quantity[$key],

							'discount' => $item_discount[$key],

							'discount_type' => (!empty($item_discount_type[$key])) ? $item_discount_type[$key] : 0,

							'discount_amount' => $item_discount_amt[$key],

                            'total' => $amount[$key],

                            'show_in_print' => $show_price_in_print,

                            'item_profit' => $profit,

                            'status' => 1,

                            'branch_id' => $user_data['branch_id'],

                            'created_by' => $user_data['user_id'],

                            'created_on' => date('Y-m-d H:i:s')

                        );

						

                        if ($this->sales_order_model->insert_item($item)) {

                            $this->product_model->update_branch_stock($user_data['branch_id'], $product_id[$key], $quantity[$key]);

                        }



                        if ($exchange_order_id != "") {

                            $log_txt = "From Exchange Order";

                        } else if ($service_id != "") {

                            $log_txt = "From Service";

                        } else {

                            $log_txt = "From Invoice";

                        }

                        //Insert Item Cost Log

                        $this->update_price_log($product_id[$key], $unit_cost[$key], $price[$key], $log_txt);

                    }

                }

            }

            // Add newly added product end

			

            if ($exchange_order_id != "" || $service_id != "") {

                /*                 * * Exchange Order || Service Exchange Order Block ** */

                $so_exc_item_id = ($exchange_order_id != "") ? $this->input->post('hidden_excsoitem_id') : $this->input->post('so_exc_item_id');

                $ser_exc_itemid = ($exchange_order_id != "") ? $this->input->post('exc_product_id') : $this->input->post('ser_exc_itemid');

                $exc_product_code = $this->input->post('exc_product_code');

                $exc_product_description = $this->input->post('exc_product_description');

                $exc_serial_no = $this->input->post('exc_serial_no');

                $exc_quantity = $this->input->post('exc_quantity');

                $exc_price = $this->input->post('exc_price');

                $exc_unit_cost = $this->input->post('exc_cost');

				$exc_item_discount = $this->input->post('exc_item_discount');

				$exc_item_discount_amt = $this->input->post('exc_item_discount_amt');

				$exc_item_discount_type = $this->input->post('exc_item_discount_type');

                $exc_amount = $this->input->post('exc_amount');

                $exc_show_in_print = $this->input->post('exc_show_in_print');

                foreach ($so_exc_item_id as $key => $product) {

                    if (!empty($ser_exc_itemid[$key])) {

						$old_so_item = $this->sales_order_model->get_so_item($so_exc_item_id[$key]);

						if ($old_so_item->price != $exc_price[$key]) {

							$price_changed = 1;

							$is_price_changed = 1;

						}

						else {

							$price_changed = 0;

						}

						

                        $profit = $exc_quantity[$key] * ($exc_price[$key] - $exc_unit_cost[$key]);

						$profit -= (!empty($exc_item_discount_amt[$key])) ? $exc_item_discount_amt[$key] : 0;

						

                        if (($service_id != '') && (empty($exc_show_in_print[$key]))) {

                            $show_price_in_print = 0;

                        } else {

                            $show_price_in_print = 1;

                        }



                        $item = array(

                            'item_id' => $ser_exc_itemid[$key],

                            'so_item_codetxt' => $exc_product_code[$key],

                            'product_description' => $exc_product_description[$key],

                            'serial_no' => $exc_serial_no[$key],

                            'price' => $exc_price[$key],

							'price_changed' => $price_changed,

                            'unit_cost' => $exc_unit_cost[$key],

                            'quantity' => $exc_quantity[$key],

							'discount' => $exc_item_discount[$key],

							'discount_type' => (!empty($exc_item_discount_type[$key])) ? $exc_item_discount_type[$key] : 0,

							'discount_amount' => $exc_item_discount_amt[$key],

                            'total' => $exc_amount[$key],

                            'show_in_print' => $show_price_in_print,

                            'item_profit' => $profit,

                            'status' => 1,

                            'branch_id' => $user_data['branch_id'],

                            'created_by' => $user_data['user_id'],

                            'created_on' => date('Y-m-d H:i:s')

                        );



                        $this->sales_order_model->update_item($item, $so_exc_item_id[$key]);



                        $log_txt = "From Service";

                        //Insert Item Cost Log

                        $this->update_price_log($ser_exc_itemid[$key], $exc_unit_cost[$key], $exc_price[$key], $log_txt);

                    }

                }



                if ($exchange_order_id != "") {

                    $delete_items_serv = $this->sales_order_model->get_removed_so_items_excbytype(1, $id, $so_exc_item_id);

                } else {

                    $delete_items_serv = $this->sales_order_model->get_removed_so_items_servicebytype(1, $id, $so_exc_item_id);

                }

                foreach ($delete_items_serv as $key => $remove_item) {

                    $this->sales_order_model->delete_so_item($remove_item->id);

                }

				

				$new_exc_product_id = $this->input->post('new_exc_product_id');

                $new_ser_exc_itemid = ($exchange_order_id != "") ? $new_exc_product_id : $this->input->post('new_ser_exc_itemid');

                $new_exc_product_code = $this->input->post('new_exc_product_code');

                $new_exc_product_description = $this->input->post('new_exc_product_description');

                $new_exc_serial_no = $this->input->post('new_exc_serial_no');

                $new_exc_quantity = $this->input->post('new_exc_quantity');

                $new_exc_price = $this->input->post('new_exc_price');

                $new_exc_unit_cost = $this->input->post('new_exc_cost');

				$new_exc_item_discount = $this->input->post('new_exc_item_discount');

				$new_exc_item_discount_amt = $this->input->post('new_exc_item_discount_amt');

				$new_exc_item_discount_type = $this->input->post('new_exc_item_discount_type');

                $new_exc_amount = $this->input->post('new_exc_amount');

                $new_exc_show_in_print = $this->input->post('new_exc_show_in_print');

				

                foreach ($new_ser_exc_itemid as $key => $product) {

                    if (!empty($new_ser_exc_itemid[$key])) {

                        $profit = $new_exc_quantity[$key] * ($new_exc_price[$key] - $new_exc_unit_cost[$key]);

						$profit -= (!empty($new_exc_item_discount_amt[$key])) ? $new_exc_item_discount_amt[$key] : 0;

						

                        if (($service_id != '') && (empty($new_exc_show_in_print[$key]))) {

                            $show_price_in_print = 0;

                        } else {

                            $show_price_in_print = 1;

                        }

						

						$product_info = $this->exchange_order_model->get_exchange_order_item_by_id($new_exc_product_id[$key]);

						if ($product_info->exr_price != $new_exc_price[$key]) {

							$price_changed = 1;

							$is_price_changed = 1;

						}

						else {

							$price_changed = 0;

						}

						

                        $item = array(

                            'so_id' => $id,

                            'exc_form_type' => ($exchange_order_id != '') ? 1 : 0,

                            'service_form_type' => ($service_id != '') ? 1 : 0,

                            'item_id' => $new_ser_exc_itemid[$key],

                            'so_item_codetxt' => $new_exc_product_code[$key],

                            'product_description' => $new_exc_product_description[$key],

                            'serial_no' => $new_exc_serial_no[$key],

                            'price' => $new_exc_price[$key],

							'price_changed' => $price_changed,

                            'unit_cost' => $new_exc_unit_cost[$key],

                            'quantity' => $new_exc_quantity[$key],

							'discount' => $new_exc_item_discount[$key],

							'discount_type' => (!empty($new_exc_item_discount_type[$key])) ? $new_exc_item_discount_type[$key] : 0,

							'discount_amount' => $new_exc_item_discount_amt[$key],

                            'total' => $new_exc_amount[$key],

                            'show_in_print' => $show_price_in_print,

                            'item_profit' => $profit,

                            'status' => 1,

                            'branch_id' => $user_data['branch_id'],

                            'created_by' => $user_data['user_id'],

                            'created_on' => date('Y-m-d H:i:s')

                        );

                        $this->sales_order_model->insert_item($item);



                        if ($exchange_order_id != "") {

                            $log_txt = "From Exchange Order";

                        } else if ($service_id != "") {

                            $log_txt = "From Service";

                        }

                        //Insert Item Cost Log

                        $this->update_price_log($new_ser_exc_itemid[$key], $new_exc_unit_cost[$key], $new_exc_price[$key], $log_txt);

                    }

                }

                /*                 * * End ** */

            }



            if ($service_id != "") {

                /*                 * * Service Cost Block ** */

                $service_item_id = $this->input->post('service_item_id');

                $serv_product_code = $this->input->post('serv_product_code');

                $external_cost = $this->input->post('external_cost');

                $our_cost = $this->input->post('our_cost');

                $serv_amount = $this->input->post('serv_amount');

                foreach ($service_item_id as $key => $product) {

                    if (!empty($service_item_id[$key])) {

						$old_so_item = $this->sales_order_model->get_so_item($service_item_id[$key]);

						if ($old_so_item->total != $serv_amount[$key]) {

							$price_changed = 1;

							$is_price_changed = 1;

						}

						else {

							$price_changed = 0;

						}

						

                        $profit = $serv_amount[$key] - $external_cost[$key];

                        $item = array(

                            'product_description' => $serv_product_code[$key],

                            'external_cost' => $external_cost[$key],

                            'our_cost' => $our_cost[$key],

							'price_changed' => $price_changed,

                            'total' => $serv_amount[$key],

                            'item_profit' => $profit,

                            'status' => 1,

                            'branch_id' => $user_data['branch_id'],

                            'created_by' => $user_data['user_id'],

                            'created_on' => date('Y-m-d H:i:s')

                        );



                        $this->sales_order_model->update_item($item, $service_item_id[$key]);

                    }

                }



                $delete_items_serv = $this->sales_order_model->get_removed_so_items_servicebytype(3, $id, $service_item_id);

                foreach ($delete_items_serv as $key => $remove_item) {

                    $this->sales_order_model->delete_so_item($remove_item->id);

                }



                $new_serv_product_code = $this->input->post('new_serv_product_code');

                $new_serv_amount = $this->input->post('new_serv_amount');

                $new_external_cost = $this->input->post('new_external_cost');

                $new_our_cost = $this->input->post('new_our_cost');

                foreach ($new_serv_product_code as $key => $product) {

//              if (!empty($new_serv_product_code[$key])) {

					/*$product_info = $this->services_model->get_service_item_byid($service_cost_id[$key]);

					if ($product_info->total != $serv_amount[$key]) {

						$price_changed = 1;

						$is_price_changed = 1;

					}

					else {

						$price_changed = 0;

					}*/

					

                    $profit = $new_serv_amount[$key] - $new_external_cost[$key];

                    $item = array(

                        'so_id' => $id,

                        'service_form_type' => 3,

                        'product_description' => $new_serv_product_code[$key],

                        'external_cost' => $new_external_cost[$key],

						//'price_changed' => $price_changed,

                        'our_cost' => $new_our_cost[$key],

                        'total' => $new_serv_amount[$key],

                        'item_profit' => $profit,

                        'status' => 1,

                        'branch_id' => $user_data['branch_id'],

                        'created_by' => $user_data['user_id'],

                        'created_on' => date('Y-m-d H:i:s')

                    );



                    $this->sales_order_model->insert_item($item);

//              }

                }

                /*                 * * End ** */

            }

			

			/* Log report start */

			$method_name_txt = $this->log_method_name;

			$method_name_txt .= ($is_price_changed) ? ' - Price Changed' : '';

            $this->insert_log($this->log_controler_name, $method_name_txt, 'view', $id, $data_sales_order->so_no, '');

			/* Log report end */

			

            if ($this->input->post('payment') == 'pay_now') {

                $upd = array(

                    'payment_group_type' => $this->input->post('data_group'),

                    'updated_by' => $this->user_data['user_id'],

                    'updated_on' => date('Y-m-d H:i:s')

                );

                $this->sales_order_model->update($id, $upd);

            }



            $amount_to_bank = 0;

            $payment_id = $this->input->post('payment_id');

            if (!empty($payment_id)) {

                $sales_payment_details = $this->sales_payment_model->get_by_id($payment_id);

            }

			

            if ($this->input->post('payment') == 'pay_now' && $this->input->post('data_group') != '3') {

                $sales_payment = array(

                    'pay_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('so_date')))),

                    'pay_amt' => $this->input->post('total_amt'),

                    'pay_pay_type' => $this->input->post('payment_type'),

                    'pay_pay_no' => '',

                    'pay_pay_bank' => '',

                    'pay_pay_date' => NULL,

                    'pay_dd_no' => '',

                    'pay_dd_date' => NULL,

                    'payment_status' => 1,

                    'bank_acc_id' => $this->user_data['bank_account_id'],

                    'status' => 1,

                    'brn_id' => $user_data['branch_id'],

                    'ip_address' => $_SERVER['REMOTE_ADDR']

                );



                $payment_type = $this->payment_type_model->get_by_id($this->input->post('payment_type'));

                if ($payment_type->is_cheque == 1) {

                    $sales_payment['pay_pay_no'] = $this->input->post('cheque_acc_no');

                    $sales_payment['pay_pay_bank'] = $this->input->post('cheque_bank_name');

                    $sales_payment['pay_pay_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('cheque_date'))));

                }



                /* if ($this->input->post('payment_type') == '3') {

                  $sales_payment['pay_dd_no'] = $this->input->post('dd_no');

                  $sales_payment['pay_dd_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('dd_date'))));

                  } */



                if (!empty($payment_id)) {

                    $old_payment_type = $this->payment_type_model->get_by_id($sales_payment_details->pay_pay_type);

                    $sales_payment['updated_by'] = $user_data['user_id'];

                    $sales_payment['updated_on'] = date('Y-m-d H:i:s');

                    $payment_affected_rows = $this->sales_payment_model->update($payment_id, $sales_payment);

                    $this->insert_log('invoice_payment', 'Update', 'view', $payment_id, $data_sales_order->so_no, '');

                    if (!in_array($old_payment_type->group_name, array(1, 3))) {

                        $amount_to_bank -= $sales_payment_details->pay_amt;

                    }



                    if (!in_array($payment_type->group_name, array(1, 3))) {

                        $amount_to_bank += $this->input->post('total_amt');

                    }

                } else {

                    $prefix_wt_sub = CODE_SPAY;

                    $sales_payment_code = $this->auto_generation_code(TBL_SPM, $prefix_wt_sub, '', '', '');

                    $sales_payment['so_id'] = $id;

                    $sales_payment['pay_no'] = $sales_payment_code;

                    $sales_payment['created_by'] = $user_data['user_id'];

                    $sales_payment['created_on'] = date('Y-m-d H:i:s');

                    $payment_affected_rows = $this->sales_payment_model->insert($sales_payment);

                    $this->insert_log('invoice_payment', 'insert', 'view', $payment_affected_rows, $data_sales_order->so_no, '');

                    if (!in_array($payment_type->group_name, array(1, 3))) {

                        $amount_to_bank = $this->input->post('total_amt');

                    }

                }



                if ($payment_affected_rows) {

                    $update_status = array();

                    $update_status['payment_status'] = 1;

                    $this->sales_payment_model->update_payment_status($id, $update_status);

                }

            } else {

                //$payment_id = $this->input->post('payment_id');

                if (!empty($payment_id)) {

                    //$sales_payment_details = $this->sales_payment_model->get_by_id($payment_id);

                    $payment_type = $this->payment_type_model->get_by_id($sales_payment_details->pay_pay_type);

                    $update_status = array();

                    $total_amt = $this->input->post('total_amt');

                    if ($total_amt > $sales_payment_details->pay_amt) {

                        $update_status['payment_status'] = 4;

                        /* $sales_order = array(

                          'status' => 10,

                          'updated_on' => date('Y-m-d H:i:s'),

                          'updated_by' => $user_data['user_id']

                          );

                          $this->sales_payment_model->update($payment_id, $sales_order);



                          if (!in_array($payment_type->group_name, array(1, 3))) {

                          $amount_to_bank -= $sales_payment_details->pay_amt;

                          } */

                    } else {

                        $update_status['payment_status'] = 1;

                    }



                    $this->sales_payment_model->update_payment_status($id, $update_status);

                    $this->sales_payment_model->update($payment_id, $update_status);

                }

            }



            if (!empty($payment_id)) {

                $update_data = array(

                    'updated_by' => $this->user_data['user_id'],

                    'updated_on' => date('Y-m-d H:i:s')

                );

                $this->bank_model->update_bank_amount($sales_payment_details->bank_acc_id, $update_data, $amount_to_bank, '+');

            }



            redirect('invoice/print_pdf/' . $id, 'refresh');

        } else {

            $_SESSION['error'] = 'Invoice not updated!';

            redirect('invoice/edit/' . $id, 'refresh');

        }

    }



    public function view($id) {

        $data = array();



        $user_id = $this->user_data['user_id'];

        $status = array(1, 2);

        $invoice_not = $this->sales_order_model->get_notification_by_type('Invoice', $id, $user_id, $status, '');

        if (count($invoice_not) > 0) {

            for ($i = 0; $i < count($invoice_not); $i++) {

                $notification_data = array(

                    'status' => 3,

                    'updated_on' => date('Y-m-d H:i:s'),

                    'updated_by' => $this->user_data['user_id']

                );

                $this->sales_order_model->update_notification_item($notification_data, $invoice_not[$i]['not_id']);

            }

        }



        $user_data = get_user_data();

        $data['browser'] = browser_info();

        $data['sales_order'] = $this->sales_order_model->get_by_id($id);

        if ($data['sales_order']->invoice_from == 4) {

            $data['exc_items'] = $this->sales_order_model->get_service_products_by_so($id, 1);

            $data['inventory_items'] = $this->sales_order_model->get_service_products_by_so($id, 2);

            $data['service_cost'] = $this->sales_order_model->get_service_products_by_so($id, 3);

            $data['sales_order_items'] = array_merge($data['exc_items'], $data['inventory_items']);

        } else if ($data['sales_order']->invoice_from == 3) {

            $data['exc_items'] = $this->sales_order_model->get_exc_soitem_by($id, 1);

            $data['inventory_items'] = $this->sales_order_model->get_exc_soitem_by($id, 2);

            $data['sales_order_items'] = array_merge($data['exc_items'], $data['inventory_items']);

            $data['service_cost'] = array();

        } else {

            $data['sales_order_items'] = $this->sales_order_model->get_products_by_so($id);

            $data['service_cost'] = array();

        }



        $data['return_items'] = $this->sales_return_model->get_srnitems_allby_soid($id);



        $data['shipping_address'] = $this->sales_order_model->shipping_address_by_id($data['sales_order']->shipping_address);

        $data['address'] = $this->sales_order_model->get_current_address($user_data['branch_id']);

        $data['company'] = $this->company_model->get_company();

        $data['thermal_src'] = base_url() . 'invoice/thermal/' . $id;



        $data['receipt_link'] = base_url() . 'invoice/pdf_receipt/' . $id;

        $data['do_link'] = base_url() . 'invoice/invoice_to_delivery_order/' . $id;

        $this->render($data, 'sales_order/view');

    }



    public function delete_hold_invoice($id) {

        if (empty($id)) {

            show_400_error();

        }



        if ($this->sales_order_model->delete_hold_invoice($id)) {

            $_SESSION['success'] = 'Hold invoice has been deleted successfully';

        } else {

            $_SESSION['success'] = 'Hold invoice not deleted.';

        }



        redirect('invoice/add', 'refresh');

    }



    public function delete($id) {

        if (empty($id)) {

            show_400_error();

        }



        $user_data = get_user_data();

        $sales_orderdata = $this->sales_order_model->get_by_id($id);

        $delete_items = $this->sales_order_model->get_removed_so_items($id);

        foreach ($delete_items as $key => $remove_item) {

            $item = array(

                'status' => 10,

                'updated_on' => date('Y-m-d H:i:s'),

                'updated_by' => $user_data['user_id']

            );

            if ($this->sales_order_model->update_item($item, $remove_item->id)) {

                $old_item_id_txt = '';

                if ($sales_orderdata->invoice_from == 3 && $remove_item->exc_form_type == 2) {

                    $old_item_id_txt = $remove_item->item_id;

                } else if ($sales_orderdata->invoice_from == 4 && $remove_item->service_form_type == 2) {

                    $get_pro_itemid = $this->services_model->get_service_item_byid($remove_item->item_id);

                    $old_item_id_txt = $get_pro_itemid->item_id;

                } else if ($sales_orderdata->invoice_from != 4) {

                    $old_item_id_txt = $remove_item->item_id;

                }



                if ($old_item_id_txt != '') {

                    $this->product_model->update_branch_stock($user_data['branch_id'], $old_item_id_txt, $remove_item->quantity, '+');

                }

            }

        }



        $sales_order = array(

            'status' => 10,

            'updated_on' => date('Y-m-d H:i:s'),

            'updated_by' => $user_data['user_id']

        );



        if ($this->sales_order_model->update($id, $sales_order) > 0) {

            $this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $sales_orderdata->so_no, 1);

            $sales_payment = $this->sales_payment_model->get_by_so_id($id);

            if (count($sales_payment) > 0) {

                $this->sales_payment_model->update($sales_payment->id, $sales_order);

                // Cash group payment will be added in petty cash

                if (!in_array($sales_payment->group_name, array(1, 3))) {

                    $update_data = array(

                        'updated_by' => $this->user_data['user_id'],

                        'updated_on' => date('Y-m-d H:i:s')

                    );

                    $this->bank_model->update_bank_amount($sales_payment->bank_acc_id, $update_data, $sales_payment->pay_amt, '-');

                }

            }



            $notificationlist = $this->sales_order_model->get_notification_by_type('Invoice', $id);

            if (count($notificationlist) > 0) {

                for ($i = 0; $i < count($notificationlist); $i++) {

                    $notification_data = array(

                        'status' => 10,

                        'updated_on' => date('Y-m-d H:i:s'),

                        'updated_by' => $user_data['user_id']

                    );

                    $this->sales_order_model->update_notification_item($notification_data, $notificationlist[$i]['not_id']);

                }

            }



            $_SESSION['success'] = 'Invoice deleted successfully';

        } else {

            $_SESSION['error'] = 'Invoice Order not deleted!';

        }



        redirect('invoice', 'refresh');

    }



    public function pdf($id) {

        $data = array();

        $user_data = get_user_data();

        $data['sales_order'] = $this->sales_order_model->get_by_id($id);

        if ($data['sales_order']->invoice_from == 4) {

            $exc_items = $this->sales_order_model->get_service_products_by_so($id, 1);

            $inventory_items = $this->sales_order_model->get_service_products_by_so($id, 2);

            $service_cost = $this->sales_order_model->get_service_products_by_so($id, 3);

            $data['so_items'] = array_merge($service_cost, $exc_items, $inventory_items);

        } else if ($data['sales_order']->invoice_from == 3) {

            $exc_items = $this->sales_order_model->get_exc_soitem_by($id, 1);

            $inventory_items = $this->sales_order_model->get_exc_soitem_by($id, 2);

            $data['so_items'] = array_merge($exc_items, $inventory_items);

        } else {

            $data['so_items'] = $this->sales_order_model->get_products_by_so($id);

        }



        $data['job_sheet'] = array();

        if ($data['sales_order']->invoice_from == 4) {

            $data['job_sheet'] = $this->services_model->get_by_id($data['sales_order']->service_id);

        }



        $data['address'] = $this->sales_order_model->get_current_address($user_data['branch_id']);

        $data['amt_word'] = convert_amount_to_word_rupees($data['sales_order']->total_amt);

        $data['shipping_address'] = $this->sales_order_model->shipping_address_by_id($data['sales_order']->shipping_address);

        $data['user_data'] = get_user_data();

        $data['branch'] = $this->company_model->branch_by_id($data['sales_order']->branch_id);

        $data['branches'] = $this->company_model->active_branch_first();

        $data['company'] = $this->company_model->get_company();

        $filename = 'Invoice_' . $data['sales_order']->so_no;

        $filename = strtoupper($filename);

        // Load Views

        $header = $this->load->view('sales_order/pdf_header', $data, TRUE); //'<div></div>';

        $content = $this->load->view('sales_order/pdf', $data, TRUE);

        $footer = $this->load->view('sales_order/pdf_footer', $data, TRUE);

        //echo $header.$content.$footer;exit;

        $this->load->helper(array('My_Pdf'));

        invoice_pdf($header, $content, $footer, $filename);

        exit(0);

    }



    public function pdf_receipt($id) {

        $data = array();

        $user_data = get_user_data();

        $data['sales_order'] = $this->sales_order_model->get_by_id($id);

        if ($data['sales_order']->invoice_from == 4) {

            $exc_items = $this->sales_order_model->get_service_products_by_so($id, 1);

            $inventory_items = $this->sales_order_model->get_service_products_by_so($id, 2);

            $service_cost = $this->sales_order_model->get_service_products_by_so($id, 3);

            $data['so_items'] = array_merge($service_cost, $exc_items, $inventory_items);

        } else if ($data['sales_order']->invoice_from == 3) {

            $exc_items = $this->sales_order_model->get_exc_soitem_by($id, 1);

            $inventory_items = $this->sales_order_model->get_exc_soitem_by($id, 2);

            $data['so_items'] = array_merge($exc_items, $inventory_items);

        } else {

            $data['so_items'] = $this->sales_order_model->get_products_by_so($id);

        }



        $data['job_sheet'] = array();

        if ($data['sales_order']->invoice_from == 4) {

            $data['job_sheet'] = $this->services_model->get_by_id($data['sales_order']->service_id);

        }



        $data['address'] = $this->sales_order_model->get_current_address($user_data['branch_id']);

        $data['amt_word'] = convert_amount_to_word_rupees($data['sales_order']->total_amt);

        $data['shipping_address'] = $this->sales_order_model->shipping_address_by_id($data['sales_order']->shipping_address);

        $data['user_data'] = get_user_data();

        $data['branch'] = $this->company_model->branch_by_id($data['sales_order']->branch_id);

        $data['branches'] = $this->company_model->active_branch_first();

        $data['company'] = $this->company_model->get_company();

        $filename = 'Receipt_' . $data['sales_order']->so_no;

        $filename = strtoupper($filename);

        // Load Views

        $header = $this->load->view('sales_order/pdf_header_receipt', $data, TRUE); //'<div></div>';

        $content = $this->load->view('sales_order/pdf_receipt', $data, TRUE);

        $footer = $this->load->view('sales_order/pdf_footer_receipt', $data, TRUE);

        #echo $header.$content.$footer;exit;

        $this->load->helper(array('My_Pdf'));

        invoice_pdf($header, $content, $footer, $filename);

        exit(0);

    }



    public function thermal($id) {

        $data = array();

        //$user_data = get_user_data();

        $data['sales_order'] = $this->sales_order_model->get_by_id($id);

        if ($data['sales_order']->invoice_from == 4) {

            $exc_items = $this->sales_order_model->get_service_products_by_so($id, 1);

            $inventory_items = $this->sales_order_model->get_service_products_by_so($id, 2);

            $service_cost = $this->sales_order_model->get_service_products_by_so($id, 3);

            $data['so_items'] = array_merge($service_cost, $exc_items, $inventory_items);

        } else if ($data['sales_order']->invoice_from == 3) {

            $exc_items = $this->sales_order_model->get_exc_soitem_by($id, 1);

            $inventory_items = $this->sales_order_model->get_exc_soitem_by($id, 2);

            $data['so_items'] = array_merge($exc_items, $inventory_items);

        } else {

            $data['so_items'] = $this->sales_order_model->get_products_by_so($id);

        }



        $data['job_sheet'] = array();

        if ($data['sales_order']->invoice_from == 4) {

            $data['job_sheet'] = $this->services_model->get_by_id($data['sales_order']->service_id);

        }



        $data['user_data'] = $this->user_data;

        $data['branch'] = $this->company_model->branch_by_id($data['sales_order']->branch_id);

        $data['company'] = $this->company_model->get_company();

        //$this->load->helper(array('My_Pdf'));

        //echo thermal_pdf('', $content, '');



        $filename = 'Invoice_' . $data['sales_order']->so_no;



        echo $this->load->view('sales_order/thermal', $data, TRUE);

    }



    public function update_price_log($pid, $bprice, $sprice, $tit) {

        $last = $this->product_model->get_product_last_price_log($pid);

        $last_id = '';

        if (($last->buying_price != $bprice) || ($last->selling_price != $sprice)) {

            $insert = array(

                'product_id' => $pid,

                'buying_price' => $bprice,

                'selling_price' => $sprice,

                'updated_from' => $tit,

                'brn_id' => $this->user_data['branch_id'],

                'created_by' => $this->user_data['user_id'],

                'created_on' => date('Y-m-d H:i:s'),

                'ip_address' => $_SERVER['REMOTE_ADDR']

            );

            $last_id = $this->product_model->insert_cost_log($insert);

        }

        return $last_id;

    }



    public function check_your_reference() {

        $data = array();

        $your_reference = $this->input->post('your_reference');

        $invoice_id = $this->input->post('invoice_id');

        $data['exist'] = count($this->sales_order_model->check_your_reference($your_reference, $invoice_id));



        $this->ajax_response($data);

    }



    public function get_notification_items() {

        $itm_type = $this->input->post('itm_type');

        $data = array();

        if (NOT_VIEW) {

            $data = $this->sales_order_model->get_notification_items($itm_type);

        }

        $this->ajax_response($data);

    }



    public function send_mail($id = '') {

        if (empty($id)) {

            show_400_error();

        }



        $data = array();

        $user_data = get_user_data();

        $this->user_data;



        $data['sales_order'] = $this->sales_order_model->get_by_id($id);

        #$to = $data['sales_order']->email;

        $to = $this->input->post('to_mail');

        if (empty($to)) {

            $_SESSION['error'] = 'Customer email is empty. Please add customer email and try again!';

            redirect('invoice', 'refresh');

        }



        $data['so_items'] = $this->sales_order_model->get_products_by_so($id);

        $data['address'] = $this->sales_order_model->get_current_address($this->user_data['branch_id']);

        $data['amt_word'] = convert_amount_to_word_rupees($data['sales_order']->total_amt);

        $data['shipping_address'] = $this->sales_order_model->shipping_address_by_id($data['sales_order']->shipping_address);

        $data['user_data'] = $this->user_data;

        $data['branch'] = $this->company_model->branch_by_id($data['sales_order']->branch_id);

        $data['branches'] = $this->company_model->active_branch_first();

        $data['company'] = $this->company_model->get_company();

        $filename = 'Invoice_' . $data['sales_order']->quo_no;

        $filename = strtoupper($filename) . '.pdf';



        // Load Views

        $header = $this->load->view('sales_order/pdf_header', $data, TRUE); //'<div></div>';

        $content = $this->load->view('sales_order/pdf', $data, TRUE);

        $footer = ''; //$this->load->view('quotation/pdf_footer', $data, TRUE);

        $mail_html = $header . $content . $footer;

        //echo $mail_html;



        require(APPPATH . '/helpers/mpdf/mpdf.php');

        $mpdf = new mPDF('utf-8', // mode - default ''

                'A4', // format - A4, for example, default ''

                0, // font size - default 0

                '', // default font family

                '15', // 15 margin_left

                '5', // 15 margin right

                '0', // 16 margin top 84

                '0', // margin bottom 75 150 140

                '8', // 9 margin header

                '0', // 9 margin footer

                'P'

        );

        $mpdf->setAutoTopMargin = 'stretch';

        $mpdf->setAutoBottomMargin = 'stretch';

        $mpdf->WriteHTML($header);

        $mpdf->WriteHTML($content);

        $mpdf->SetHTMLFooter($footer);



        $file_path = QUOT_EMAIL_PATH;

        if (!is_dir($file_path)) {

            if (!mkdir($file_path, 0777, true))

                error_log('Failed to create folder for ' . $branch_name);

        }



        $file_path = $file_path . $filename;

        $mpdf->Output($file_path, 'F');



        /* From Email */

        $from = (!empty($data['company']->comp_email)) ? $data['company']->comp_email : 'info@refulgenceinc.com';

        $subject = 'UCS - Quotation(' . $data['sales_order']->quo_no . ')';



        $headers = "MIME-Version: 1.0" . "\r\n";

        $headers .= "From: " . $from . "\r\n";

        $headers .= "Reply-To: " . $from . "\r\n";

        $headers .= "X-Mailer: PHP/" . phpversion();



        // boundary

        $semi_rand = md5(time());

        $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";



        // headers for attachment

        $headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"";



        // multipart boundary

        $mail_html = "This is a multi-part message in MIME format.\n\n" . "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"iso-8859-1\"\n" . "Content-Transfer-Encoding: 7bit\n\n" . $mail_html . "\n\n";

        // preparing attachments



        $file_ptr = fopen($file_path, "rb");

        $data = fread($file_ptr, filesize($file_path));

        fclose($file_ptr);

        $data = chunk_split(base64_encode($data));



        $mail_html .= "--{$mime_boundary}\n";

        $mail_html .= "Content-Type: {\"application/octet-stream\"};\n" . " name=\"$filename\"\n";

        $mail_html .= "Content-Disposition: attachment;\n" . " filename=\"$filename\"\n";

        $mail_html .= "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";

        $mail_html .= "--{$mime_boundary}--\n";

        #echo $mail_html;exit;



        $ok = @mail($to, $subject, $mail_html, $headers);

        if ($ok) {

            $_SESSION['success'] = 'Invoice sent successfully to the customer email <span class="lowercase">' . $to . '</span>.';

			$this->insert_log($this->log_controler_name, $this->log_method_name, 'view', $id, $data['sales_order']->so_no, 1);

        } else {

            $_SESSION['error'] = 'Invoice not sent to the customer email <span class="lowercase">' . $to . '</span>. Try again!';

        }



        $insert = array(

            'invoice_id' => $id,

            'created_on' => date('Y-m-d H:i:s'),

            'created_by' => $this->user_data['user_id']

        );



        $this->sales_order_model->insert_invoice_mail($insert);

        unlink($file_path);

        redirect('invoice', 'refresh');

    }



    public function get_email_by_id() {

        $data = $this->sales_order_model->get_by_id($_POST['id']);

        echo json_encode($data);

    }



    public function update_notifications() {

        $itm_id = $this->input->post('itm_id');

        $data = array();

        if ($itm_id != '') {

            $notification_data = array(

                'status' => 2,

                'updated_on' => date('Y-m-d H:i:s'),

                'updated_by' => $this->user_data['user_id']

            );

            $data = $this->sales_order_model->update_notification_item($notification_data, $itm_id);

        }

        $this->ajax_response($data);

    }



}



?>