<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


if (!function_exists('generate_pdf')) :

    function generate_pdf($args = array()) {
        $upload_path = $args['upload_path'];

        if (empty($args['file_name'])) :
            $args['file_name'] = 'file_' . date("d-m-Y");
        endif;

        if (!empty($upload_path) && !file_exists($upload_path)) :
            mkdir($upload_path, 0777, TRUE);
            chmod($upload_path, 0777);
        endif;

        require 'mpdf/mpdf.php';
        //$mpdf = new mPDF('', 'Letter', 0, '', 12.7, 12.7, 14, 12.7, 8, 8);

        $mpdf = new mPDF(
                '', // 'utf-8',    // mode - default ''   
                'Letter', //'A4',       // format - A4, for example, default ''
                0, // font size - default 0
                'Calibari', // default font family
                '5', // 15 margin_left
                '5', // 15 margin right 
                !empty($args['margin_top']) ? $args['margin_top'] : 60, // 16 margin top 84
                '20', // margin bottom 75 150 140
                '5', // 9 margin header
                '5', // 9 margin footer
                'L'
        );

        $mpdf->SetHTMLHeader($args['header']);
        $mpdf->SetHTMLFooter($args['footer']);
        $mpdf->WriteHTML($args['content']);

        $upload_path = $upload_path . $args['file_name'] . '.pdf';
        $mpdf->Output($upload_path, !empty($args['download_type']) ? $args['download_type'] : 'D' );

        return $upload_path;
    }

endif;

if (!function_exists('grn_pdf')) {

    function grn_pdf($header, $data = '', $footer = '', $file_name = "") {
        #echo $header. $data. $footer;exit;
        if (empty($file_name)) {
            $file_name = 'Report_' . date('d-m-Y');
        }

        require 'mpdf/mpdf.php';
        $mypdf = new mPDF('utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                0, // font size - default 0
                '', // default font family
                '5', // 15 margin_left
                '5', // 15 margin right
                20, // 16 margin top 84
                10, // margin bottom 75 150 140
                '5', // 9 margin header
                '30', // 9 margin footer
                'L');

        $mypdf->debug = true;
        $mypdf->SetHTMLHeader($header);
        $mypdf->WriteHTML($data);
        $mypdf->SetHTMLFooter($footer);

        /* $pdf_data = $header . $data . $footer;
          $mypdf->WriteHTML($pdf_data); */
        $mypdf->Output($file_name . '.pdf', 'D');
    }

}

if (!function_exists('create_pdf')) {
    function create_pdf($header, $data = '', $footer = '', $file_name = "") {
        if (empty($file_name)) {
            $file_name = 'Report_' . date('d-m-Y');
        }

        require 'mpdf/mpdf.php';
        $mypdf = new mPDF('utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                0, // font size - default 0
                '', // default font family
                '5', // 15 margin_left
                '5', // 15 margin right
                10, // 16 margin top 84
                10, // margin bottom 75 150 140
                '5', // 9 margin header
                '30', // 9 margin footer
                'P');
		
		$mypdf->allow_charset_conversion = true;
		$mypdf->charset_in = 'iso-8859-4';
        $mypdf->debug = true;
        $mypdf->SetHTMLHeader($header);
        $mypdf->WriteHTML($data);
        $mypdf->SetHTMLFooter($footer);

        /* $pdf_data = $header . $data . $footer;
          $mypdf->WriteHTML($pdf_data); */
        $mypdf->Output($file_name . '.pdf', 'D');
    }

}

if (!function_exists('delivery_pdf')) {
    /*function delivery_pdf($header, $data = '', $footer = '', $file_name = '') {
        if (empty($file_name)) {
            $file_name = 'Delivery_Report_' . date('d-m-Y');
        }

        require 'mpdf/mpdf.php';
        $mypdf = new mPDF('utf-8', // mode - default ''
			'A4', // format - A4, for example, default ''
			0, // font size - default 0
			'', // default font family
			'15', // 15 margin_left
			'5', // 15 margin right
			'5', // 16 margin top 84
			'25', // margin bottom 75 150 140
			'5', // 9 margin header
			'0', // 9 margin footer
			'P'
		);
		
		$mypdf->setAutoTopMargin = 'stretch';
		$mypdf->setAutoBottomMargin = 'stretch';
        #$mypdf->SetHTMLHeader($header);
        $mypdf->WriteHTML($header);
        $mypdf->WriteHTML($data);
        #$mypdf->WriteHTML($footer);
        $mypdf->SetHTMLFooter($footer);
        $mypdf->Output($file_name . '.pdf', 'D');
    }*/
}

if (!function_exists('invoice_pdf')) {
    function invoice_pdf($header, $data = '', $footer = '', $file_name = '') {
        if (empty($file_name)) {
            $file_name = 'Invoice_' . date('d-m-Y');
        }
		
        require 'mpdf/mpdf.php';
        $mypdf = new mPDF('utf-8', // mode - default ''
			'A4', // format - A4, for example, default ''
			0, // font size - default 0
			'', // default font family
			'15', // 15 margin_left
			'5', // 15 margin right
			'0', // 16 margin top 84
			'0', // margin bottom 75 150 140
			'8', // 9 margin header
			'0', // 9 margin footer
			'P'
		);
		
		$mypdf->setAutoTopMargin = 'stretch';
		$mypdf->setAutoBottomMargin = 'stretch';
        #$mypdf->SetHTMLHeader($header);
        $mypdf->WriteHTML($header);
        $mypdf->WriteHTML($data);
        #$mypdf->WriteHTML($footer);
        $mypdf->SetHTMLFooter($footer);
        $mypdf->Output($file_name . '.pdf', 'D');
    }
}

if (!function_exists('delivery_order_pdf')) {
    function delivery_order_pdf($header, $data = '', $footer = '', $file_name = '') {
        if (empty($file_name)) {
            $file_name = 'DELIVERY_' . date('d-m-Y');
        }

        require 'mpdf/mpdf.php';
        $mypdf = new mPDF('utf-8', // mode - default ''
			'A4', // format - A4, for example, default ''
			0, // font size - default 0
			'', // default font family
			'15', // 15 margin_left
			'5', // 15 margin right
			'5', // 16 margin top 84
			'25', // margin bottom 75 150 140
			'8', // 9 margin header
			'0', // 9 margin footer
			'P'
		);
		
		$mypdf->setAutoTopMargin = 'stretch';
		$mypdf->setAutoBottomMargin = 'stretch';
        #$mypdf->SetHTMLHeader($header);
        $mypdf->WriteHTML($header);
        $mypdf->WriteHTML($data);
        #$mypdf->WriteHTML($footer);
        $mypdf->SetHTMLFooter($footer);
        $mypdf->Output($file_name . '.pdf', 'D');
    }
}

if (!function_exists('voucher_pdf')) {
    function voucher_pdf($header, $data = '', $footer = '', $file_name = "") {
        if (empty($file_name)) {
            $file_name = 'Report_' . date('d-m-Y');
        }

        require 'mpdf/mpdf.php';
        $mypdf = new mPDF('utf-8', // mode - default ''
			'A4', // format - A4, for example, default ''
			0, // font size - default 0
			'', // default font family
			'5', // 15 margin_left
			'5', // 15 margin right
			20, // 16 margin top 84
			10, // margin bottom 75 150 140
			'5', // 9 margin header
			'5', // 9 margin footer
			'P');
		
        $mypdf->debug = true;
        $mypdf->SetHTMLHeader($header);
        $mypdf->WriteHTML($data);
        $mypdf->SetHTMLFooter($footer);
        $mypdf->Output($file_name . '.pdf', 'D');
    }
}