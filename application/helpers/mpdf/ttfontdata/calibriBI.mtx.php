<?php
$name='Calibri-BoldItalic';
$type='TTF';
$desc=array (
  'Ascent' => 750,
  'Descent' => -250,
  'CapHeight' => 632,
  'Flags' => 262212,
  'FontBBox' => '[-691 -306 1330 1039]',
  'ItalicAngle' => -11,
  'StemV' => 165,
  'MissingWidth' => 507,
);
$up=-113;
$ut=65;
$ttffile='/home/refulgenceinc/public_html/demo/lucky_store/application/helpers/mpdf/ttfonts/calibriz.ttf';
$TTCfontID='0';
$originalsize=1074860;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='calibriBI';
$panose=' 8 0 2 f 7 2 3 4 4 a 2 4';
$haskerninfo=false;
$unAGlyphs=false;
?>