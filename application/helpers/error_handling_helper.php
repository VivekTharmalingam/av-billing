<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function show_403_error() {
    $_this = &get_instance();
    $head = array();
    $head['page_title'] = '403 - Access Forbidden';
    
    $header = array();
    $user_data = array();
    $user_data = get_user_data();
    $head['user_data'] = $header['user_data'] = $user_data;
    $header['menu'] = 'home';    $head['company_data'] = $header['company_data'] = get_company_data();
    $header['left_menu'] = 1;
    $header['permissions'] = unserialize($_this->user_model->get_user_permission($user_data['user_id']));
    
    $data = array();
    $data['error'] = $_this->session->userdata('error_message');
    
    $footer = array();
    $footer['branches'] = $_this->company_model->get_branch_name_by_ids($user_data['branch_ids']);
    $footer['user_data'] = $user_data;
    $footer['switch_branch'] = FALSE;
    if (!empty($_SESSION['switch_branch'])) {
        $footer['switch_branch'] = $_SESSION['switch_branch'];
        unset($_SESSION['switch_branch']);
    }
    
    echo $_this->load->view('templates/head', $head, TRUE);
    echo $_this->load->view('templates/header', $header, TRUE);
    echo $_this->load->view('error_pages/403', $data, TRUE);
    echo $_this->load->view('templates/footer', $footer, TRUE);
    exit(0);
}

function show_404_error() {
    $_this = &get_instance();
    $head = array();
    $head['page_title'] = '404 - Page Not Found';
    
    $header = array();
    $user_data = array();
    $user_data = get_user_data();
    $head['user_data'] = $header['user_data'] = $user_data;	$head['company_data'] = $header['company_data'] = get_company_data();
    $header['menu'] = 'home';
    $header['left_menu'] = 1;
    $header['permissions'] = unserialize($_this->user_model->get_user_permission($user_data['user_id']));
    
    $data = array();
    $data['error'] = $_this->session->userdata('error_message');
    
    $footer = array();
    $footer['branches'] = $_this->company_model->get_branch_name_by_ids($user_data['branch_ids']);
    $footer['user_data'] = $user_data;
    $footer['switch_branch'] = FALSE;
    if (!empty($_SESSION['switch_branch'])) {
        $footer['switch_branch'] = $_SESSION['switch_branch'];
        unset($_SESSION['switch_branch']);
    }
    
    echo $_this->load->view('templates/head', $head, TRUE);
    echo $_this->load->view('templates/header', $header, TRUE);
    echo $_this->load->view('error_pages/404', $data, TRUE);
    echo $_this->load->view('templates/footer', $footer, TRUE);
    exit(0);
}

function show_400_error() {
    $_this = &get_instance();
    $head = array();
    $head['page_title'] = '400 - Bad Request';
    
    $header = array();
    $user_data = array();
    $user_data = get_user_data();
    $head['user_data'] = $header['user_data'] = $user_data;	$head['company_data'] = $header['company_data'] = get_company_data();
    $header['menu'] = 'home';
    $header['left_menu'] = 1;
    $header['permissions'] = unserialize($_this->user_model->get_user_permission($user_data['user_id']));
    
    $data = array();
    $data['error'] = $_this->session->userdata('error_message');
    
    $footer = array();
    $footer['branches'] = $_this->company_model->get_branch_name_by_ids($user_data['branch_ids']);
    $footer['user_data'] = $user_data;
    $footer['switch_branch'] = FALSE;
    if (!empty($_SESSION['switch_branch'])) {
        $footer['switch_branch'] = $_SESSION['switch_branch'];
        unset($_SESSION['switch_branch']);
    }
    
    echo $_this->load->view('templates/head', $head, TRUE);
    echo $_this->load->view('templates/header', $header, TRUE);
    echo $_this->load->view('error_pages/400', '', TRUE);
    echo $_this->load->view('templates/footer', $footer, TRUE);
    exit(0);
}
?>