<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('create_excel')) {
    function create_excel($thead, $tbody = '', $file_name = "") {
        //echo '<pre>'; print_r($tbody); exit;
        /** Set default timezone (will throw a notice otherwise) */
        date_default_timezone_set('Asia/Kolkata');
        // include PHPExcel
        require('excel/PHPExcel.php');
        //require 'mpdf/PHPExcel.php';

        // create new PHPExcel object
        $objPHPExcel = new PHPExcel;

        // set default font
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');

        // set default font size
        $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);

        // create the writer
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

        /**

         * Define currency and number format. */
        // currency format, € with < 0 being in red color
        $currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';

        // number format, with thousands separator and two decimal points.
        $numberFormat = '#,#0.##;[Red]-#,#0.##';

        $letters = Array
            (
                '0' => 'A',
                '1' => 'B',
                '2' => 'C',
                '3' => 'D',
                '4' => 'E',
                '5' => 'F',
                '6' => 'G',
                '7' => 'H',
                '8' => 'I',
                '9' => 'J',
                '10' => 'K',
                '11' => 'L',
                '12' => 'M',
                '13' => 'N',
                '14' => 'O',
                '15' => 'P',
                '16' => 'Q',
                '17' => 'R',
                '18' => 'S',
                '19' => 'T',
                '20' => 'U',
                '21' => 'V',
                '22' => 'W',
                '23' => 'X',
                '24' => 'Y',
                '25' => 'Z'
            );

        // writer already created the first sheet for us, let's get it
        $objSheet = $objPHPExcel->getActiveSheet();

        // rename the sheet
        $objSheet->setTitle($file_name);

        // let's bold and size the header font and write the header
        // as you can see, we can specify a range of cells, like here: cells from A1 to A4
        $objSheet->getStyle('A1:G1')->getFont()->setBold(true)->setSize(12);

        // write header
        $i = 0;  
        foreach($thead as $key => $val) { 
            $objSheet->getCell($letters[$i++].'1')->setValue($val['thead']);
        }
        
        // we could get this data from database, but here we are writing for simplicity
        $j = 1;
       
        foreach($tbody as $key => $body) {  
            $i = 0;
            ++$j;
            foreach($thead as $k => $val) {
                if(!empty($val['field_name'])) {
                    $objSheet->getCell($letters[$i++].$j)->setValue($body->$val['field_name']);
                } 
                else {
                    $field_names =  explode(',', $val['field_names']);
                    $fields_arr = array_chunk($field_names, 2);
                    $total = 0;
                    foreach($fields_arr as $index => $field_arr) {
                        switch($val['op']) {
                            case '+':
                                $total += $body->$field_arr[0] + $body->$field_arr[1];
                                break;
                            
                            case '-':
                                $total += $body->$field_arr[0] - $body->$field_arr[1];
                                break;
                            
                            case '*':
                                $total += $body->$field_arr[0] * $body->$field_arr[1];
                                break;
                            
                            case '/':
                                $total += $body->$field_arr[0] / $body->$field_arr[1];
                                break;
                            
                            default:
                                $total = 0;
                        }
                    }
                    
                    $objSheet->getCell($letters[$i++].$j)->setValue($total);    
                }
            }
        }
        
        // echo '<pre>'; print_r($objSheet); exit;

        // autosize the columns
        $i = 0;
        foreach($thead as $key => $val) {  
            $objSheet->getColumnDimension($letters[$i++])->setAutoSize(true);
        } 


        //Setting the header type
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$file_name .'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');

        /* If you want to save the file on the server instead of downloading, replace the last 4 lines by 
                $objWriter->save('test.xlsx');
*/

    }
}

?>
