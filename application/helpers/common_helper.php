<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function get_ci_instance() {
    $ci = &get_instance();
    return $ci;
}

function get_session_userdata() {
    $ci = get_ci_instance();
    return $ci->session->userdata(TBL_PREFIX . 'user_logged_in');
}

function check_permission() {
    $ci = get_ci_instance();

    if (empty($ci->session->userdata(TBL_PREFIX . 'user_logged_in'))) {
        if (!substr_count(current_url(), 'get_notification_items')) {
            $ci->session->set_userdata('request_uri', current_url());
        }

        redirect('login', 'refresh');
    }

    //$ci->user_data = get_session_userdata();
    $permission_str = $ci->user_model->get_user_permission($ci->user_data['user_id']);
    $ci->permissions = (!empty(unserialize($permission_str))) ? unserialize($permission_str) : array();

    $controller = $ci->router->fetch_class();
    $action = $ci->router->fetch_method();
    $permissions = $ci->permissions;

    if (($controller != 'profile') || ($controller != 'menu')) {
        switch ($action) {
            case 'index':
                //if (((!array_key_exists($controller, $permissions)) || (!in_array(1, $permissions[$controller]))) && ($controller != 'home')) {
                if (((!array_key_exists($controller, $permissions)) || (!in_array(1, $permissions[$controller])))) {
                    show_403_error();
                }
                break;

            case 'view':
                if ((!array_key_exists($controller, $permissions)) || (!in_array(1, $permissions[$controller]))) {
                    show_403_error();
                }
                break;

            case 'add':
                if ((!array_key_exists($controller, $permissions)) || (!in_array(2, $permissions[$controller]))) {
                    show_403_error();
                }
                break;

            case 'edit':
                if ((!array_key_exists($controller, $permissions)) || (!in_array(3, $permissions[$controller]))) {
                    show_403_error();
                }
                break;

            case 'delete':
                if ((!array_key_exists($controller, $permissions)) || (!in_array(4, $permissions[$controller]))) {
                    show_403_error();
                }
                break;

            case 'excel':
                if ((!array_key_exists($controller, $permissions)) || (!in_array(5, $permissions[$controller]))) {
                    show_403_error();
                }
                break;

            case 'pdf':
                if ((!array_key_exists($controller, $permissions)) || (!in_array(6, $permissions[$controller]))) {
                    show_403_error();
                }
                break;

            case 'approve':
                if ((!array_key_exists($controller, $permissions)) || (!in_array(10, $permissions[$controller]))) {
                    show_403_error();
                }
                break;

            default:
                break;
        }
    }

    return array(
        'permissions' => $permissions,
        'controller' => $controller,
        'method' => $action
    );
}

function get_page_title() {
    $ci = get_ci_instance();
    $action = $ci->router->fetch_method();
    $page_title = ucfirst($ci->router->fetch_class());
    switch ($action) {
        case 'view':
            $page_title .= ' ' . ucfirst($action);
            break;

        case 'add':
            $page_title .= ' ' . ucfirst($action);
            break;

        case 'edit':
            $page_title .= ' ' . ucfirst($action);
            break;

        default:
            break;
    }

    $value_arr = explode('_', $page_title);
    $value_arr = array_map(function($word) {
        return ucfirst($word);
    }, $value_arr
    );
    $page_title = implode(' ', $value_arr);

    return $page_title;
}

function css_url() {
    $ci = get_ci_instance();
    return $ci->config->item('css_url');
}

function js_url() {
    $ci = get_ci_instance();
    return $ci->config->item('js_url');
}

function upload_url() {
    $ci = get_ci_instance();
    return $ci->config->item('upload_url');
}

function images_url($image_name = '', $folder_path = '', $no_image = '') {
    $ci = get_ci_instance();
    if (!empty($image_name)) {
        $file_path = FCPATH;
        $file_path .= (empty($folder_path)) ? IMG_DIR . $image_name : $folder_path . $image_name;
        if ((!empty($image_name)) && (file_exists($file_path))) {
            if (!empty($folder_path))
                $images_url = base_url() . $folder_path . $image_name;
            else
                $images_url = $ci->config->item('images_url') . $image_name;
        }
        else if (!empty($no_image))
            $images_url = $no_image;
        else
            $images_url = NO_IMG;
    }
    else if (!empty($no_image))
        $images_url = $ci->config->item('images_url');

    return $images_url;
}

function get_header_data() {
    $ci = get_ci_instance();
    $data = array();
    $data['page_title'] = get_page_title();
    $data['current_controller'] = $ci->router->fetch_class();
    $data['current_method'] = $ci->router->fetch_method();
    return $data;
}

function current_date() {
    return date('d-m-Y');
}

function convert_time_12_to_24($time) {
    $time_arr = explode(':', $time);
    $time_period = explode(' ', array_pop($time_arr));
    $hr = array_shift($time_arr);
    $min = array_shift($time_period);
    if (end($time_period) == 'PM') {
        if ($hr != 12)
            $hr = $hr + 12;

        $hr = $hr;
    }
    else if ($hr == 12)
        $hr = 0;
    else
        $hr = $hr;

    return $hr . ':' . $min;
}

function convert_text_date($date = '') {
    $convert_date = '';
    if ($date != '') {
        $pos = strpos($date, '/');
        if ($pos !== false)
            $con_date = str_replace('/', '-', $date);
        else
            $con_date = $date;

        $convert_date = date('Y-m-d', strtotime($con_date));
    }
    return $convert_date;
}

function text($text = '', $empty = 'N/A') {
    return (!empty($text)) ? trim($text) : $empty;
}

function text_date($date = '', $empty = 'N/A', $format = 'd-m-Y') {
    return (!empty($date)) ? date($format, strtotime($date)) : $empty;
}

function text_amount($amt = 0, $empty = '0.00') {
    return (!empty($amt)) ? number_format($amt, 2, '.', '') : $empty;
}

function text_limit($text, $link = '', $limit = 50) {
    if (empty($text))
        $text = 'N/A';
    else if (strlen($text) > $limit) {
        $link = ($link != '') ? $link : '#';
        $text = substr($text, 0, $limit) . "..&nbsp;<a href='$link' title='Read More' style='color: #428bca;'>Read More</a>";
    }

    return trim($text);
}

function get_url_key($url_key) {
    $url_key = strtolower(trim(trim($url_key, '-')));

    $patterns = array();
    $patterns[0] = '/( (\(|\/|\[|\{|&|\*)?)/';
    $patterns[1] = '/[()\'"\[\]{},*&]|[^a-z0-9\-]/';
    $patterns[2] = '/-+/';
    $replacements = array();
    $replacements[2] = '-';
    $replacements[1] = '';
    $replacements[0] = '-';
    $url_key = preg_replace($patterns, $replacements, $url_key);
    return $url_key;
}

function download_file($file_path = '') {
    if (!empty($file_path)) {
        $file_path_parts = explode('/', $file_path);
        $file_name = array_pop($file_path_parts);
        $file_name = urldecode($file_name);

        array_push($file_path_parts, $file_name);
        $file_path = implode('/', $file_path_parts);

        ob_clean();
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $file_name . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file_path));
        readfile($file_path);
    }
}

function image_extensions() {
    return array('jpeg', 'png', 'jpg', 'gif');
}

function recursive_conversion($num, $tri) {
    $ones = array('', ' One', ' Two', ' Three', ' Four', ' Five', ' Six', ' Seven', ' Eight', ' Nine', ' Ten', ' Eleven', ' Twelve', ' Thirteen', ' Fourteen', ' Fifteen', ' Sixteen', ' Seventeen', ' Eighteen', ' Nineteen');
    $tens = array('', '', ' Twenty', ' Thirty', ' Forty', ' Fifty', ' Sixty', ' Seventy', ' Eighty', ' Ninety');
    $triplets = array('', ' Thousand', ' Million', ' Billion', ' Trillion', ' Quadrillion', ' Quintillion', ' Sextillion', ' Septillion', ' Octillion', ' Nonillion');

    // chunk the number, ...rxyy
    $r = (int) ($num / 1000);
    $x = ($num / 100) % 10;
    $y = $num % 100;

    // init the output string
    $str = '';

    // do hundreds
    if ($x > 0)
        $str = $ones[$x] . ' Hundred ';

    // do ones and tens
    if ($y < 20)
        $str .= $ones[$y];
    else
        $str .= $tens[(int) ($y / 10)] . $ones[$y % 10];

    // add triplet modifier only if there // is some output to be modified...
    if ($str != '')
        $str .= $triplets[$tri];

    // continue recursing?
    if ($r > 0)
        return recursive_conversion($r, $tri + 1) . $str;
    else
        return $str;
}

// returns the number as an anglicized string
function convert_amount_to_word($num) {
    $num = (float) $num;
    if ($num == 0)
        return 'zero';

    $amounts = explode('.', $num);
    $amount_in_words = recursive_conversion($amounts[0], 0);
    $amount_in_words .= ' Dollars';
    if (!empty($amounts[1])) {
        $amount_in_words .= ' and';
        $amount_in_words .= recursive_conversion($amounts[1], 0);
        $amount_in_words .= ' Cents';
    }

    return $amount_in_words . ' Only';
}

function convert_amount_to_word_rupees($num) {
    $num = (float) $num;
    if ($num == 0)
        return 'zero';

    $amounts = explode('.', $num);
    $amount_in_words = recursive_conversion($amounts[0], 0);
    $amount_in_words .= ' Dollar';
    if (!empty($amounts[1])) {
        $amount_in_words .= ' and';
        $amount_in_words .= recursive_conversion($amounts[1], 0);
        $amount_in_words .= ' Cents';
    }

    return $amount_in_words . ' Only';
}

function breadcrumb_links() {
    $breadcrumb_items = array();
    $url_items = array();
    $url_items = array_filter(explode('/', str_replace(base_url(), '', current_url())));
    $link = '';
    foreach ($url_items as $key => $value) {
        if ((is_numeric($value)) || ($value == 'index') || ($value == 'home'))
            continue;

        $link .= $value;
        $value_arr = explode('_', $value);

        $value_arr = array_map(function($word) {
            return ucfirst($word);
        }, $value_arr
        );

        $value = implode(' ', $value_arr);
        $menu = array(
            'menu_name' => $value,
            'link' => base_url() . $link
        );

        array_push($breadcrumb_items, $menu);

        $link .= '/';
    }

    $breadcrumb_hmtl = '<ol class="breadcrumb">';
    $breadcrumb_hmtl .= '<li><a href="' . base_url() . '"><i class="fa fa-home"></i> Home</a></li>';
    $total_link = count($breadcrumb_items);
    foreach ($breadcrumb_items as $key => $value) {
        if ($key != ($total_link - 1)) {
            $breadcrumb_hmtl .= '<li><a href="' . $value['link'] . '">' . $value['menu_name'] . '</a></li>';
        } else {
            $breadcrumb_hmtl .= '<li class="active">' . $value['menu_name'] . '</li>';
        }
    }
    $breadcrumb_hmtl .= '</ol>';

    return $breadcrumb_hmtl;
}

/* function get_setting() {
  $ci = &get_instance();
  $ci->load->model('settings_model', '', TRUE);
  return $ci->settings_model->get_by_id();
  } */

function get_company_gst() {
    $ci = get_ci_instance();
    $ci->load->model('company_model', '', TRUE);
    $gst = $ci->company_model->get_company_details()->gst_percent;
    return (!empty($gst)) ? number_format($gst, 2) : '0.00';
}

function get_company_details() {
    $ci = get_ci_instance();
    $ci->load->model('company_model', '', TRUE);
    $result = $ci->company_model->get_company_details();
    return $result;
}

function country_list() {
    $countries = array(
        'AF' => 'Afghanistan',
        'AX' => 'Aland Islands',
        'AL' => 'Albania',
        'DZ' => 'Algeria',
        "AS" => "American Samoa",
        "AD" => "Andorra",
        "AO" => "Angola",
        "AI" => "Anguilla",
        "AQ" => "Antarctica",
        "AG" => "Antigua and Barbuda",
        "AR" => "Argentina",
        "AM" => "Armenia",
        "AW" => "Aruba",
        "AU" => "Australia",
        "AT" => "Austria",
        "AZ" => "Azerbaijan",
        "BS" => "Bahamas",
        "BH" => "Bahrain",
        "BD" => "Bangladesh",
        "BB" => "Barbados",
        "BY" => "Belarus",
        "BE" => "Belgium",
        "BZ" => "Belize",
        "BJ" => "Benin",
        "BM" => "Bermuda",
        "BT" => "Bhutan",
        "BO" => "Bolivia",
        "BA" => "Bosnia and Herzegovina",
        "BW" => "Botswana",
        "BV" => "Bouvet Island",
        "BR" => "Brazil",
        "IO" => "British Indian Ocean Territory",
        "BN" => "Brunei Darussalam",
        "BG" => "Bulgaria",
        "BF" => "Burkina Faso",
        "BI" => "Burundi",
        "KH" => "Cambodia",
        "CM" => "Cameroon",
        "CA" => "Canada",
        "CV" => "Cape Verde",
        "KY" => "Cayman Islands",
        "CF" => "Central African Republic",
        "TD" => "Chad",
        "CL" => "Chile",
        "CN" => "China",
        "CX" => "Christmas Island",
        "CC" => "Cocos (Keeling) Islands",
        "CO" => "Colombia",
        "KM" => "Comoros",
        "CG" => "Congo",
        "CD" => "Congo, The Democratic Republic of The",
        "CK" => "Cook Islands",
        "CR" => "Costa Rica",
        "CI" => "Cote D'ivoire",
        "HR" => "Croatia",
        "CU" => "Cuba",
        "CY" => "Cyprus",
        "CZ" => "Czech Republic",
        "DK" => "Denmark",
        "DJ" => "Djibouti",
        "DM" => "Dominica",
        "DO" => "Dominican Republic",
        "EC" => "Ecuador",
        "EG" => "Egypt",
        "SV" => "El Salvador",
        "GQ" => "Equatorial Guinea",
        "ER" => "Eritrea",
        "EE" => "Estonia",
        "ET" => "Ethiopia",
        "FK" => "Falkland Islands (Malvinas)",
        "FO" => "Faroe Islands",
        "FJ" => "Fiji",
        "FI" => "Finland",
        "FR" => "France",
        "GF" => "French Guiana",
        "PF" => "French Polynesia",
        "TF" => "French Southern Territories",
        "GA" => "Gabon",
        "GM" => "Gambia",
        "GE" => "Georgia",
        "DE" => "Germany",
        "GH" => "Ghana",
        "GI" => "Gibraltar",
        "GR" => "Greece",
        "GL" => "Greenland",
        "GD" => "Grenada",
        "GP" => "Guadeloupe",
        "GU" => "Guam",
        "GT" => "Guatemala",
        "GG" => "Guernsey",
        "GN" => "Guinea",
        "GW" => "Guinea-bissau",
        "GY" => "Guyana",
        "HT" => "Haiti",
        "HM" => "Heard Island and Mcdonald Islands",
        "VA" => "Holy See (Vatican City State)",
        "HN" => "Honduras",
        "HK" => "Hong Kong",
        "HU" => "Hungary",
        "IS" => "Iceland",
        "IN" => "India",
        "ID" => "Indonesia",
        "IR" => "Iran, Islamic Republic of",
        "IQ" => "Iraq",
        "IE" => "Ireland",
        "IM" => "Isle of Man",
        "IL" => "Israel",
        "IT" => "Italy",
        "JM" => "Jamaica",
        "JP" => "Japan",
        "JE" => "Jersey",
        "JO" => "Jordan",
        "KZ" => "Kazakhstan",
        "KE" => "Kenya",
        "KI" => "Kiribati",
        "KP" => "Korea, Democratic People's Republic of",
        "KR" => "Korea, Republic of",
        "KW" => "Kuwait",
        "KG" => "Kyrgyzstan",
        "LA" => "Lao People's Democratic Republic",
        "LV" => "Latvia",
        "LB" => "Lebanon",
        "LS" => "Lesotho",
        "LR" => "Liberia",
        "LY" => "Libyan Arab Jamahiriya",
        "LI" => "Liechtenstein",
        "LT" => "Lithuania",
        "LU" => "Luxembourg",
        "MO" => "Macao",
        "MK" => "Macedonia, The Former Yugoslav Republic of",
        "MG" => "Madagascar",
        "MW" => "Malawi",
        "MY" => "Malaysia",
        "MV" => "Maldives",
        "ML" => "Mali",
        "MT" => "Malta",
        "MH" => "Marshall Islands",
        "MQ" => "Martinique",
        "MR" => "Mauritania",
        "MU" => "Mauritius",
        "YT" => "Mayotte",
        "MX" => "Mexico",
        "FM" => "Micronesia, Federated States of",
        "MD" => "Moldova, Republic of",
        "MC" => "Monaco",
        "MN" => "Mongolia",
        "ME" => "Montenegro",
        "MS" => "Montserrat",
        "MA" => "Morocco",
        "MZ" => "Mozambique",
        "MM" => "Myanmar",
        "NA" => "Namibia",
        "NR" => "Nauru",
        "NP" => "Nepal",
        "NL" => "Netherlands",
        "AN" => "Netherlands Antilles",
        "NC" => "New Caledonia",
        "NZ" => "New Zealand",
        "NI" => "Nicaragua",
        "NE" => "Niger",
        "NG" => "Nigeria",
        "NU" => "Niue",
        "NF" => "Norfolk Island",
        "MP" => "Northern Mariana Islands",
        "NO" => "Norway",
        "OM" => "Oman",
        "PK" => "Pakistan",
        "PW" => "Palau",
        "PS" => "Palestinian Territory, Occupied",
        "PA" => "Panama",
        "PG" => "Papua New Guinea",
        "PY" => "Paraguay",
        "PE" => "Peru",
        "PH" => "Philippines",
        "PN" => "Pitcairn",
        "PL" => "Poland",
        "PT" => "Portugal",
        "PR" => "Puerto Rico",
        "QA" => "Qatar",
        "RE" => "Reunion",
        "RO" => "Romania",
        "RU" => "Russian Federation",
        "RW" => "Rwanda",
        "SH" => "Saint Helena",
        "KN" => "Saint Kitts and Nevis",
        "LC" => "Saint Lucia",
        "PM" => "Saint Pierre and Miquelon",
        "VC" => "Saint Vincent and The Grenadines",
        "WS" => "Samoa",
        "SM" => "San Marino",
        "ST" => "Sao Tome and Principe",
        "SA" => "Saudi Arabia",
        "SN" => "Senegal",
        "RS" => "Serbia",
        "SC" => "Seychelles",
        "SL" => "Sierra Leone",
        "SG" => "Singapore",
        "SK" => "Slovakia",
        "SI" => "Slovenia",
        "SB" => "Solomon Islands",
        "SO" => "Somalia",
        "ZA" => "South Africa",
        "GS" => "South Georgia and The South Sandwich Islands",
        "ES" => "Spain",
        "LK" => "Sri Lanka",
        "SD" => "Sudan",
        "SR" => "Suriname",
        "SJ" => "Svalbard and Jan Mayen",
        "SZ" => "Swaziland",
        "SE" => "Sweden",
        "CH" => "Switzerland",
        "SY" => "Syrian Arab Republic",
        "TW" => "Taiwan, Province of China",
        "TJ" => "Tajikistan",
        "TZ" => "Tanzania, United Republic of",
        "TH" => "Thailand",
        "TL" => "Timor-leste",
        "TG" => "Togo",
        "TK" => "Tokelau",
        "TO" => "Tonga",
        "TT" => "Trinidad and Tobago",
        "TN" => "Tunisia",
        "TR" => "Turkey",
        "TM" => "Turkmenistan",
        "TC" => "Turks and Caicos Islands",
        "TV" => "Tuvalu",
        "UG" => "Uganda",
        "UA" => "Ukraine",
        "AE" => "United Arab Emirates",
        "GB" => "United Kingdom",
        "US" => "United States",
        "UM" => "United States Minor Outlying Islands",
        "UY" => "Uruguay",
        "UZ" => "Uzbekistan",
        "VU" => "Vanuatu",
        "VE" => "Venezuela",
        "VN" => "Viet Nam",
        "VG" => "Virgin Islands, British",
        "VI" => "Virgin Islands, U.S.",
        "WF" => "Wallis and Futuna",
        "EH" => "Western Sahara",
        "YE" => "Yemen",
        "ZM" => "Zambia",
        "ZW" => "Zimbabwe");
    return $countries;
}

function form_auto_generation_code($id, $prefix = '') {
    $code = $prefix;
    $code .= ($id > 999) ? $id : sprintf('%03d', $id);
    return $code;
}

function file_extensions() {
    return array('pdf', 'odt', 'xls', 'xlsx', 'doc',
        'docx', 'jpeg', 'png', 'jpg', 'gif');
}

/* function filter_special_characters($file_name) {
  $filter_file_name = trim($file_name, '-'); // -name  -
  $filter_file_name = strtolower(trim($filter_file_name));
  $patterns = array();
  $patterns[0] = '/( (\(|\/|\[|\{|&|\*)?)/';
  $patterns[1] = '/[()\'"\[\]{},*&]|[^a-z0-9\-\.]/';
  $patterns[2] = '/-+/';
  $replacements = array();
  $replacements[2] = '-';
  $replacements[1] = '';
  $replacements[0] = '-';
  $filter_file_name = preg_replace($patterns, $replacements, $filter_file_name);
  return $filter_file_name;
  } */

function file_uploads($file_path = '', $temp_path = '') {
    if (!empty($file_path)) {
        $file_path_parts = explode('/', $file_path);
        $file_name = array_pop($file_path_parts);
        $file_name = urldecode($file_name);

        $image_name = filter_special_characters($file_name);
        array_push($file_path_parts, $image_name);
        $target_path = implode('/', $file_path_parts);
        move_uploaded_file($temp_path, $target_path);
        return $image_name;
    }
}

/*
 * Make a directory
 */

function mk_dir($dir = './', $hex = '0777') {

    if (!file_exists($dir)) :
        mkdir($dir, $hex, TRUE);
        chmod($dir, $hex);
    endif;

    return TRUE;
}

/*
 * Fill Padding Digits
 */

function fill_digits($value, $fill_count = '4', $fill_by = '0') {

    if (empty($value)) :
        return $value;
    endif;

    $fill_digits = pow(10, $fill_count);
    if ($value < $fill_digits) :
        $digits = '';
        for ($i = 0; $i < ( $fill_count - strlen($value) ); $i++) :
            $digits .= $fill_by;
        endfor;

        $digits .= $value;
    else :
        return $value;
    endif;

    return $digits;
}

/*
 * Covert Number to Words
 */

function number_in_words($number = 0) {

    $hyphen = '-';
    $conjunction = ' and ';
    $separator = ', ';
    $negative = 'negative ';
    $decimal = ' point ';
    $dictionary = array(
        0 => 'zero',
        1 => 'one',
        2 => 'two',
        3 => 'three',
        4 => 'four',
        5 => 'five',
        6 => 'six',
        7 => 'seven',
        8 => 'eight',
        9 => 'nine',
        10 => 'ten',
        11 => 'eleven',
        12 => 'twelve',
        13 => 'thirteen',
        14 => 'fourteen',
        15 => 'fifteen',
        16 => 'sixteen',
        17 => 'seventeen',
        18 => 'eighteen',
        19 => 'nineteen',
        20 => 'twenty',
        30 => 'thirty',
        40 => 'fourty',
        50 => 'fifty',
        60 => 'sixty',
        70 => 'seventy',
        80 => 'eighty',
        90 => 'ninety',
        100 => 'hundred',
        1000 => 'thousand',
        1000000 => 'million',
        1000000000 => 'billion',
        1000000000000 => 'trillion',
        1000000000000000 => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
                'Only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . number_in_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {

        /*
         * Check fraction is '00'
         */
        $explode = explode('.', $number);
        if ((int) $explode[1] == 0) :
            $number = $explode[0];
        else :
            list($number, $fraction) = $explode;
        endif;
    }

    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens = ((int) ($number / 10)) * 10;
            $units = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . number_in_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = number_in_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= number_in_words($remainder);
            }
            break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return ucwords($string);
}

/*
 * Get a Variable Name
 * Usage: get_var_name( '?-- -- n??a(m)e{} %6 @@-\/', '-' );
 */

function get_var_name($str, $replace_with = '-', $allows = '') {
    if (empty($str)) :
        return '';
    endif;

    $ignores = array(' ', ',', '\\', '`', '.', '*', '#', '@', '[', ']', '-', '!', '$', '^', '%', '/', '{', '}', '"', '\'', '?', '(', ')');

    if (!empty($allows)) :
        $ignores = array_diff($ignores, $allows);
    endif;

    $pattern = '/' . implode('|\\', $ignores) . '/';

    //$pattern = '/ |,|\\\|`|\.|\*|\#|@|\[|\]|\-|\!|\$|\^|\%|\/|\{|\}|\"|\'|\?|\(|\)/i';
    return strtolower(trim(preg_replace('/' . $replace_with . $replace_with . '+/', $replace_with, preg_replace($pattern, $replace_with, $str)), $replace_with));
}

function get_user_data($user_id = '', $set_session = FALSE) {
    $logo = '';
    $ci = get_ci_instance();
    if (empty($user_id)) :
        $user_data = $ci->session->userdata(TBL_PREFIX . 'user_logged_in');
        if (empty($user_data)) :
            // NOt logged In.
            $ci->session->set_userdata('request_uri', current_url());
            redirect('login', 'refresh');
        endif;
        //return $user_data;
        $user_id = $user_data['user_id'];
        $logo = $user_data['logo'];
    endif;

    /*
     * Get User
     */
    //echo '<pre>';print_r($user_data);exit;
    $user = $ci->user_model->get_by_id($user_id);
    $sess_data = $ci->session->userdata(TBL_PREFIX . 'user_logged_in');
    if (!empty($user_data['branch_id'])) {
        $cur_brn = $user_data['branch_id'];
    } else {
        if ($user_id != '' && $user_id == $sess_data['user_id']) {
            $cur_brn = $sess_data['branch_id'];
        }
    }

    $branch_data = $ci->user_model->branch_by_id($cur_brn);

    $user_data = array(
        'name' => $user->name,
        'user_email' => $user->email,
        'user_id' => $user->id,
        'user_name' => $user->user_name,
        #'is_admin'      => ( $user->user_name == 'admin' ) ? TRUE : FALSE,
        'is_admin' => $user->is_admin,
        'bank_account_id' => $branch_data[0]['bank_acc_id'],
        'branch_id' => $cur_brn,
        'employee_id' => $user->emp_id,
        'branch_ids' => !empty($user->branch_id) ? explode(',', $user->branch_id) : array(),
        'controller' => $ci->router->fetch_class(),
        'method' => $ci->router->fetch_method(),
        'logo' => $logo
    );

    if (!$set_session) :
        // Set all permissions
        $user_data['permissions'] = !empty($user->permissions) ? unserialize($user->permissions) : array();

        // Set current Controller Permission
        if (!empty($user_data['permissions'][$user_data['controller']])) :
            $user_data['permission'] = $user_data['permissions'][$user_data['controller']];
        endif;

        $user_data['user'] = (array) $user;
    endif;

    return $user_data;
}

function send_sms($nos, $msg) {
    $return = array();
    $status = 4;
    $message = '';
    if (empty($nos)) :
        $status = 3;
        $msg = 'None of the contact no\'s are given to sent!';
    elseif (empty($msg)) :
        $status = 3;
        $msg = 'Message can not be empty!';
    else :
        // Send SMS
        $sms_to = '';
        $nos = explode(',', $nos);
        foreach ($nos as $k => $no) :
            $sms_to .= MOB_PREFIX . $no . ',';
        endforeach;
        $sms_to = substr($sms_to, 0, -1);
        $sms_msg = $msg; //"For SSBEC IB Login : Use ".$mem_otp." (IB OTP) to login into your SSBEC Online account. Sent on ".date('d M H:i');

        $query_string = "api.aspx?apiusername=" . SMS_USER . "&apipassword=" . SMS_PWD;
        $query_string .= "&senderid=" . rawurlencode(SMS_FROM) . "&mobileno=" . rawurlencode($sms_to);
        $query_string .= "&message=" . rawurlencode(stripslashes($sms_msg)) . "&languagetype=1";

        // $url = "http://gateway.onewaysms.com.au:10001/".$query_string;
        $url = "http://gateway.onewaysms.sg:10002/" . $query_string;
        //error_log($url); die;
        if (TO_SEND) :
            $fd = @implode('', file($url));
            //$fd = 1;
            if ($fd) :
                if ($fd > 0) :
                    // print("MT ID : " . $fd);
                    $status = 1;
                    $msg = 'SMS is sent successfully';
                else :
                    //print("Please refer to API on Error : " . $fd);
                    $status = 2;
                    $msg = 'Oops: Something went wrong...Please refer to API (Error Code: ' . $fd . ')';
                endif;
            else :
                // no contact with gateway
                $status = 3;
                $msg = 'Oops: Couldn\'t connect with API';
            endif; // fd
        endif; // fd
        $return['status'] = $status;
        $return['msg'] = $msg;
    endif;
    return $return;
}

function get_company_data() {
    $ci = get_ci_instance();
    $company_data = $ci->user_model->get_by_company_details();
    return $company_data;
}

function set_user_data($user_id) {
    $user_data = get_user_data($user_id, TRUE);
    $ci = get_ci_instance();
    $ci->session->set_userdata(TBL_PREFIX . 'user_logged_in', $user_data);
}

function get_permissions_struct() {
    return array(
        /*
         * Settings
         * -----------------------------------------------------------------
         */
        'settings' => array(
            'label' => 'Settings`fa-gears',
            'cls_menu' => 'setting_clr',
            'settings' => array(
                'label' => 'Settings`fa-gears',
                'controller' => 'settings',
                'extra_per' => array(
                    '2' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '3' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '4' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '5' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '6' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '7' => array(
                        'label' => 'Export',
                        'show_in_menu' => FALSE
                    ),
                    '8' => array(
                        'label' => 'Import',
                        'show_in_menu' => FALSE
                    ),
                ),
            ),
        ),
        /*
         * Dashboard
         * -----------------------------------------------------------------
         */
        'dashboard' => array(
            'label' => 'Dashboard`fa-dashboard',
            'cls_menu' => 'dashboard_clr',
//            'show_in_menu'  => FALSE,
            'dashboard' => array(
                'label' => 'Dashboard`fa-dashboard',
                'controller' => 'home',
                'extra_per' => array(
                    '2' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '3' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '4' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '5' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '6' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                ),
            ),
        ),
        /*
         * Masters
         * -----------------------------------------------------------------
         */
        'masters' => array(
            'label' => 'Masters`fa-windows',
            'cls_menu' => 'master_clr',
            'user' => array(
                'label' => 'User`fa-user'
            ),
            'comp' => array(
                'label' => 'Company`fa-building',
                'controller' => 'company',
                'extra_per' => array(
                    '2' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                ),
            ),
            'custgroup' => array(
                'label' => 'Customer Group`fa-group'
            ),
            'cust' => array(
                'label' => 'Customer`fa-male'
            ),
            'sup' => array(
                'label' => 'Supplier`fa-shopping-cart'
            ),
            'serptn' => array(
                'label' => 'Service Partner`fa-user-secret'
            ),
            'cat' => array(
                'label' => 'Brand`fa-sitemap'
            ),
            'sersts' => array(
                'label' => 'Service Status`fa-flag'
            ),
            'category' => array(
                'label' => 'Item Category`fa-cube'
            ),
            'sub category' => array(
                'label' => 'Sub Category`fa-cubes'
            ),
            'prod' => array(
                'label' => 'Item`fa-archive',
                'extra_per' => array(
                    '7' => array(
                        'label' => 'Barcode PDF',
                        'show_in_menu' => FALSE
                    ),
                )
            ),
//            'sprod' => array(
//                'label' => 'Scan Product`fa-barcode',
//                'extra_per' => array(
//                    '2' => array(
//                        'label' => '',
//                        'show_in_menu' => FALSE
//                    ),
//                ),
//            ),
//            'sprod' => array(
//                'label' => 'Scan Product`fa-barcode',
//                'extra_per' => array(
//                    '1' => array(
//                        'label' => '',
//                        'show_in_menu' => FALSE
//                    ),
//                    '3' => array(
//                        'label' => '',
//                        'show_in_menu' => FALSE
//                    ),
//                    '4' => array(
//                        'label' => '',
//                        'show_in_menu' => FALSE
//                    ),
//
//
//                ),
//            ),
            'prodprice' => array(
                'label' => 'Update Item`fa-edit',
                'controller' => 'update_item',
                'extra_per' => array(
                    '2' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                ),
            ),
            'emp' => array(
                'label' => 'Employee`fa-black-tie'
            ),
            'exp_type' => array(
                'label' => 'Expense Type`fa-list-alt',
            ),
            'payment_type' => array(
                'label' => 'Payment Type`fa-dollar',
            )
//				'loc'  => array(
//                    'label' => 'Location `fa-map-marker',
//                ),
        ),
        /*
         * Purchase Operations
         * -----------------------------------------------------------------
         */
        'promotions' => array(
            'label' => 'Promotions`fa-shopping-cart',
            'cls_menu' => 'promotions_clr',
            'pwp' => array(
                'label' => 'Purchase With Purchase`fa-shopping-basket',
                'show_in_menu' => TRUE,
            ),
            'ds' => array(
                'label' => 'Discount Sales`fa-dollar',
                'show_in_menu' => TRUE
            ),
            'b1g1' => array(
                'label' => 'Buy One Get One`fa-dollar',
                'show_in_menu' => TRUE
            ),
            'gby' => array(
                'label' => 'Group Buy`fa-dollar',
                'show_in_menu' => TRUE
            ),
        ),
        /*
         * Masters
         * -----------------------------------------------------------------
         */
        'banking' => array(
            'label' => 'Banking`fa-bank',
            'cls_menu' => 'banking_clr',
            'bank' => array(
                'label' => 'Bank`fa-university',
                'extra_per' => array(
                    '6' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                )
            ),
            'deposit' => array(
                'label' => 'Deposit`fa-database',
                'extra_per' => array(
                    '6' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                )
            ),
            'deduction' => array(
                'label' => 'Deduction`fa-upload',
                'extra_per' => array(
                    '6' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                )
            ),
            'payment' => array(
                'label' => 'Payment`fa-money',
                'extra_per' => array(
                    '6' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                )
            ),
            'transfer' => array(
                'label' => 'Bank Transfer`fa-reply-all',
                'extra_per' => array(
                    '6' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                )
            )
        ),
        /*
         * Exchange Order Operations
         * -----------------------------------------------------------------
         */
        'exchange_order' => array(
            'label' => 'Exchange Order`fa-handshake-o',
            'cls_menu' => 'exchange_order_clr',
            'exr' => array(
                'label' => 'Exchange Order`fa-handshake-o',
                'show_in_menu' => TRUE,
                'extra_per' => array(
                    '7' => array(
                        'label' => 'Convert to Invoice',
                        'show_in_menu' => FALSE
                    ),
                )
            ),
            'exrpy' => array(
                'label' => 'Exc-Order Payments`fa-money',
                'controller' => 'exchange_order_payments',
                'show_in_menu' => TRUE
            )
        ),
        /*
         * Services
         * -----------------------------------------------------------------
         */
        'services' => array(
            'label' => 'Services`fa-cogs',
            'cls_menu' => 'services_clr',
            'exterserv' => array(
                'label' => 'External Services`fa-link',
                'show_in_menu' => TRUE
            ),
            'extspy' => array(
                'label' => 'Ext-Service Payments`fa-money',
                'controller' => 'external_service_payments',
                'show_in_menu' => TRUE
            ),
            'serv' => array(
                'label' => 'Services`fa-wrench',
                'show_in_menu' => TRUE
            ),
            'scan' => array(
                'label' => 'Scan Service`fa-shirtsinbulk',
                'controller' => 'scan_service',
                'extra_per' => array(
                    '2' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '3' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '4' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    )
                )
            )
        ),
        /*
         * Purchase Operations
         * -----------------------------------------------------------------
         */
        'purchases' => array(
            'label' => 'Purchases`fa-shopping-cart',
            'cls_menu' => 'purchase_clr',
            'po' => array(
                'label' => 'Purchase Orders`fa-fax',
                'show_in_menu' => TRUE,
                'extra_per' => array(
                    '7' => array(
                        'label' => 'Product unit cost',
                        'show_in_menu' => FALSE
                    ),
                    '8' => array(
                        'label' => 'Convert to Good Receive',
                        'show_in_menu' => FALSE
                    ),
                    '9' => array(
                        'label' => 'Send Email',
                        'show_in_menu' => FALSE
                    )
                )
            ),
            'gr' => array(
                'label' => 'Goods Receive`fa-shopping-basket',
                'show_in_menu' => TRUE,
                'extra_per' => array(
                    '7' => array(
                        'label' => 'Selling Price',
                        'show_in_menu' => FALSE
                    ),
                )
            ),
            'pp' => array(
                'label' => 'Purchase Payments`fa-dollar',
                'show_in_menu' => TRUE
            ),
            'por' => array(
                'label' => 'Purchase Returns`fa-undo',
                'show_in_menu' => TRUE
            )
        ),
        /*
         * Sales Operations
         * -----------------------------------------------------------------
         */
        'invoice' => array(
            'label' => 'Invoice`fa fa-line-chart',
            'cls_menu' => 'invoice_clr',
            'quo' => array(
                'label' => 'Quotation`fa-file-text-o',
                'show_in_menu' => TRUE,
                'extra_per' => array(
                    '7' => array(
                        'label' => 'Show Item cost',
                        'show_in_menu' => FALSE
                    ),
                    '8' => array(
                        'label' => 'Convert to Invoice',
                        'show_in_menu' => FALSE
                    ),
                    '9' => array(
                        'label' => 'Send Email',
                        'show_in_menu' => FALSE
                    )
                )
            ),
            'inv' => array(
                'label' => 'Invoice`fa-fax',
                'show_in_menu' => TRUE,
                'extra_per' => array(
                    '7' => array(
                        'label' => 'Show Item cost',
                        'show_in_menu' => FALSE
                    ),
                    '9' => array(
                        'label' => 'Send Email',
                        'show_in_menu' => FALSE
                    ),
					'10' => array(
                        'label' => 'Pay Now',
                        'show_in_menu' => FALSE
                    )
                )
            ),
            'invp' => array(
                'label' => 'Invoice Payment`fa-dollar',
                'show_in_menu' => TRUE,
            ),
            'invr' => array(
                'label' => 'Invoice Return`fa-exchange',
                'show_in_menu' => TRUE,
                'extra_per' => array(
                    '3' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    )
                )
            ),
            'warrantyitem' => array(
                'label' => 'Warranty Item`fa-cube',
                'controller' => 'invoice_return_warrantyitem',
                'extra_per' => array(
                    '2' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '3' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    )
                ),
            ),
            'do' => array(
                'label' => 'Delivery Orders`fa-shopping-bag',
                'show_in_menu' => TRUE
            )
        ),
        /*
         * Inventory
         * -----------------------------------------------------------------
         */
        'inventory' => array(
            'label' => 'Inventory`fa-cubes',
            'cls_menu' => 'inventory_clr',
            'curnt_stock' => array(
                'label' => 'Current Stock`fa-file-text-o',
                'controller' => 'inventory',
                'extra_per' => array(
                    '2' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '3' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '4' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '6' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    )
                ),
            ),
            'adjustment' => array(
                'label' => 'Adjust Stock`fa-dropbox',
                'controller' => 'adjust_stock',
                'extra_per' => array(
                    '6' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                )
            ),
            'movement_history' => array(
                'label' => 'Movement History`fa-cube',
                'controller' => 'movement_history',
                'extra_per' => array(
                    '2' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '3' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '4' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    ),
                    '6' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    )
                ),
            ),
            'transfer_stock' => array(
                'label' => 'Transfer Stock`fa-truck',
                'controller' => 'transfer_stock',
                'show_in_menu' => TRUE
            ),
            'count_sheet' => array(
                'label' => 'Count Sheet`fa-thumbs-o-up',
                'controller' => 'count_sheet',
                'show_in_menu' => TRUE,
                'extra_per' => array(
                    '2' => array(
                        'label' => '',
                        'show_in_menu' => TRUE
                    ),
                    '3' => array(
                        'label' => '',
                        'show_in_menu' => TRUE
                    ),
                    '4' => array(
                        'label' => '',
                        'show_in_menu' => TRUE
                    ),
                    '6' => array(
                        'label' => '',
                        'show_in_menu' => FALSE
                    )
                ),
            ),
        ),
        /*
         * Operations
         * -----------------------------------------------------------------
         */
        'operations' => array(
            'label' => 'Operations`fa-bar-chart-o',
            'cls_menu' => 'operation_clr',
            'payslip' => array(
                'label' => 'Payslip`fa-money',
                'show_in_menu' => TRUE
            ),
            'exp' => array(
                'label' => 'Expense`fa-pie-chart',
                'controller' => 'expense',
                'show_in_menu' => TRUE
            ),
            'petty_cash' => array(
                'label' => 'Petty Cash`fa-suitcase',
                'controller' => 'petty_cash',
                'show_in_menu' => TRUE
            ),
            'enquiry_log_form' => array(
                'label' => 'Enquiry Log Form`fa-envelope-o',
                'controller' => 'enquiry_log_form',
                'extra_per' => array(
                    '7' => array(
                        'label' => 'Add Log',
                        'show_in_menu' => FALSE
                    )
                )
            //'show_in_menu' => TRUE
            )
        ),
        /*
         * Reports
         * -----------------------------------------------------------------
         */
        'reports' => array(
            'label' => 'Reports`fa-flag-checkered',
            'cls_menu' => 'reports_clr',
            //'show_in_menu'  => TRUE,
            'daily_invoice' => array(
                'label' => 'Daily Report`fa-wpforms',
                'controller' => 'daily_report',
                'show_in_menu' => true,
                'extra_per' => array(
                    '7' => array(
                        'label' => 'Show Profit',
                        'show_in_menu' => FALSE
                    ),
                )
            ),
            'profit_loss' => array(
                'label' => 'Profit Loss Report`fa-money',
                'controller' => 'profit_loss_report',
                'extra_per' => array(
                    '7' => array(
                        'label' => 'Show Profit',
                        'show_in_menu' => FALSE
                    ),
                )
            ),
            'banking_report' => array(
                'label' => 'Banking Report`fa-bank',
                'controller' => 'banking_report'
            ),
            'exchange_order_report' => array(
                'label' => 'Exchange Order`fa-handshake-o',
                'controller' => 'exchange_order_report'
            ),
            'service' => array(
                'label' => 'Service Report`fa-wrench',
                'controller' => 'service_reports'
            ),
            /* 'purchase_order' => array(
              'label' => 'Purchase Order`fa-shopping-bag',
              'controller' => 'purchase_order_reports',
              'show_in_menu' => true
              ), */
            'invoice' => array(
                'label' => 'Invoice Summary`fa-line-chart',
                'controller' => 'invoice_reports',
                'show_in_menu' => true,
                'extra_per' => array(
                    '7' => array(
                        'label' => 'Show Profit',
                        'show_in_menu' => FALSE
                    ),
                )
            ),
            'warnty_rep' => array(
                'label' => 'Warranty Report`fa-ticket',
                'controller' => 'warranty_report'
            ),
            'account_statement' => array(
                'label' => 'Statement of Account`fa-reorder',
                'controller' => 'account_statement'
            ),
            'customer_payment_summary' => array(
                'label' => 'Customer Payment`fa-dollar',
                'controller' => 'customer_payment_summary'
            ),
            'customer_list' => array(
                'label' => 'Customer List`fa-male',
                'controller' => 'customer_list_reports'
            ),
            'good_receive_report' => array(
                'label' => 'Goods Receive`fa-shopping-basket',
                'controller' => 'goods_receive_report'
            ),
            'supplier_payment_summary' => array(
                'label' => 'Supplier Payment`fa-dollar',
                'controller' => 'supplier_payment_summary'
            ),
            'supplier_list' => array(
                'label' => 'Supplier List`fa-shopping-cart',
                'controller' => 'supplier_list_reports'
            ),
            'item_supplier' => array(
                'label' => 'Item Supplier`fa-cube',
                'controller' => 'item_supplier'
            ),
            'supplier_item' => array(
                'label' => 'Supplier Item`fa-industry',
                'controller' => 'supplier_item'
            ),
            'inventory_by_location' => array(
                'label' => 'Inventory By Location`fa-file-text-o',
                'controller' => 'inventory_by_location',
                'show_in_menu' => false
            ),
            'item_price_list' => array(
                'label' => 'Price List`fa-archive',
                'controller' => 'price_list'
            ),
            'inventory_details' => array(
                'label' => 'Inventory Details`fa-dropbox',
                'controller' => 'inventory_details'
            ),
            'expense_detail' => array(
                'label' => 'Expense Details`fa-pie-chart',
                'controller' => 'expense_detail_reports'
            ),
            'payslip_generation' => array(
                'label' => 'Payslip Generation`fa-file-powerpoint-o',
                'controller' => 'payslip_generation_reports'
            ),
            'enquiry_log' => array(
                'label' => 'Enquiry Log Form`fa-envelope-o',
                'controller' => 'enquiry_log_form_reports'
            ),
            'notif' => array(
                'label' => 'Notifications `fa-bell',
                'controller' => 'notifications'
            ),
            'log' => array(
                'label' => 'Log Report `fa-cog',
                'controller' => 'log_reports'
            )
        ),
    );
}

$uploaded_fields = array();

function uploads($config = array()) {
    $ci = get_ci_instance();
    global $uploaded_fields;
    $errors = 0;

    if (!empty($config['field'])) :
        // For particular file field.
        $files = array($config['field'] => $_FILES[$config['field']]);

        //if( strpos( $config['field'], '[' ) < 0 ) :
        // Single
        unset($_FILES[$config['field']]);
    //endif;

    else :
        $files = $_FILES;
    endif;


    if (!empty($files)) :

        $return = array();
        $user_data = get_user_data();

        // Load upload library
        if (empty($ci->upload)) :
            $ci->load->library('upload');
        endif;

        $file_name = (!empty($config['file_name'])) ? $config['file_name'] : '';
        $upload_path = trim(str_replace('\\', '/', $config['upload_path']), '/') . '/';
        if (empty($upload_path)) :
            $config['upload_path'] = UPLOADS;
        else :
            $config['upload_path'] = UPLOADS . $upload_path;
        endif;

        mk_dir($config['upload_path']);

        if (empty($config['allowed_types'])) :
            $config['allowed_types'] = '*';
        endif;

        foreach ($files as $fk => $file) : // $fk = file1

            $upload_clm = $fk;
            if (!is_array($file['name'])) :

                $file = $files[$fk] = array(
                    'name' => array($file['name']),
                    'type' => array($file['type']),
                    'tmp_name' => array($file['tmp_name']),
                    'error' => array($file['error']),
                    'size' => array($file['size']),
                );

            elseif (empty($config['field'])) :
                $upload_clm .= '[]';
            endif;

            $file_str = $file_path_str = $full_path_str = '';
            foreach ($file['name'] as $k => $name) : // $k = 0 // $name = Chrysanthemum.jpg

                if (!empty($name)) :
                    if (array_key_exists($fk . '_' . $k, $uploaded_fields)) :
                        $return[$fk][$k] = $uploaded_fields[$fk . '_' . $k];

                    else :

                        // Do upload
                        if (empty($file_name)) :
                            $config['file_name'] = $user_data['user_id'] . '_' . get_var_name($name, '_', array('.'));
                        endif;

                        $_FILES[$upload_clm]['name'] = $file['name'][$k];
                        $_FILES[$upload_clm]['type'] = $file['type'][$k];
                        $_FILES[$upload_clm]['tmp_name'] = $file['tmp_name'][$k];
                        $_FILES[$upload_clm]['error'] = $file['error'][$k];
                        $_FILES[$upload_clm]['size'] = $file['size'][$k];

                        $ci->upload->initialize($config);
                        $ci->upload->do_upload($upload_clm);


                        $return[$fk][$k] = $ci->upload->data();

                        if (empty($return[$fk][$k]['file_size']) || !file_exists($return[$fk][$k]['full_path'])) :
                            // Error while uploading
                            $return[$fk][$k]['error'] = '1';
                            ++$errors;
                        else :
                            $return[$fk][$k]['error'] = '0';

                        endif;

                        $uploaded_fields[$fk . '_' . $k] = $return[$fk][$k];

                    endif;

                    if ($return[$fk][$k]['error'] === '0') :
                        $file_str .= $return[$fk][$k]['file_name'] . ',';
                        $file_path_str .= $upload_path . $return[$fk][$k]['file_name'] . ',';
                        $full_path_str .= $return[$fk][$k]['full_path'] . ',';
                    endif;

                endif;

            endforeach;

            $return[$fk]['file_str'] = substr($file_str, 0, -1);
            $return[$fk]['file_path_str'] = substr($file_path_str, 0, -1);
            $return[$fk]['full_path_str'] = substr($full_path_str, 0, -1);

        endforeach;

    endif;

    //echo "<pre>fles: "; print_r( $_FILES );
    $return['error'] = $errors;
    return $return;
}

function extract_file_name($url = '') {

    if (empty($url)) :
        return array(
            'ext_img' => NO_IMG
        );
    endif;

    $return = array();
    $url = trim(str_replace('\\', '/', $url), ' ');
    $e = explode('/', $url);
    $return['file_name'] = end($e);

    $ext = end(explode('.', $return['file_name']));
    $return['ext'] = $ext;
    $return['exact_file_name'] = substr($return['file_name'], 0, -( strlen($return['ext']) + 1 ));

    if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif') :
        $return['ext_url'] = $url;
    else :

        $icon_url = 'assets/images/file-icons/';
        if (file_exists(FCPATH . $icon_url . $ext . '.png')) :
            $return['ext_url'] = BASE_URL . $icon_url . $ext . '.png';
        else :
            $return['ext_url'] = BASE_URL . $icon_url . 'file.png';
        endif;

    endif;

    return $return;
}

function get_report_fields() {
    // return current session values
    return json_decode($_SESSION['data_fields'], TRUE);
}

function set_report_fields($fields = array()) {

    foreach ($fields as $lbl => $f) :

        $fields[$lbl]['is_must'] = ( $f['is_must'] ) ? TRUE : FALSE;
        $fields[$lbl]['is_digits'] = ( $f['type'] == 'number' || $f['type'] == 'amount' ) ? TRUE : FALSE;
        $fields[$lbl]['align'] = !empty($f['align']) ? $f['align'] : ( ( $f['type'] == 'amount' ) ? 'right' : 'left' );
        $field = $f['field'];

        if (empty($f['alias'])) :
            $fields[$lbl]['alias'] = 'self';
        endif;

        // Fraction
        $fields[$lbl]['fractions'] = !empty($fields[$lbl]['fraction']) ? $fields[$lbl]['fraction'] : 2;

        if (empty($field)) :
            $field = get_var_name($lbl, '_');
            $fields[$lbl]['field'] = $field;
        endif;

        if ($f['type'] == 'date' && empty($f['format'])) :
            $fields[$lbl]['format'] = 'd-m-Y';
        endif;

        if (empty($f['ico']) && $f['ico'] !== '') :
            if ($f['type'] == 'date') :
                $fields[$lbl]['ico'] = 'fa fa-calendar'; //'glyphicon glyphicon-calendar';
            elseif ($f['type'] == 'amount') :
                $fields[$lbl]['ico'] = 'fa fa-rupee fa-amt';
            endif;
        endif;
    endforeach;

    $_SESSION['data_fields'] = json_encode($fields);

    return $fields;
}

function get_post_query() {
    $ci = get_ci_instance();
    $post = $ci->input->post();
    $post_data = $post['data'];

    $fields = get_report_fields();
    $post_fields = $post_data['fields'];
    $post_arr = array();
    $query = $alt_query_slct = $alt_query_hvng = '';


    //echo "<pre>Data: "; print_r( $fields ); die;
    if (!empty($fields)) :// && !empty( $post_fields ) ) :


        foreach ($fields as $lbl => $f) :

            if ($f['case'] == 'function') :
                // Skip setting auto generate of where condition.
                continue;
            endif;

            if (!empty($f['alt_field'])) :
                $alt_query_slct .= $f['alt_field'] . ' AS ' . $f['field'] . ', ';
            endif;

            if (!empty($post_fields[$f['field']])) :
                $op = $post_fields[$f['field'] . '_op'];
                $symbol = !empty($op) ? $op : ':';
                $qry_val = '';

                if (empty($f['alt_field'])) :
                    $query .= ( $f['type'] == 'date' ? 'DATE(`' . $f['alias'] . '`.`' . $f['field'] . '`)' : '`' . $f['alias'] . '`.`' . $f['field'] . '`' );
                endif;

                if ($f['type'] == 'date' && !empty($f['opts']['range'])) :
                    // Date With Range

                    $e = explode('-', $post_fields[$f['field']]);
                    if (!empty($e[1])) :
                        //$e = explode('-', str_replace( 'undefined- ' , '', $post_fields[$f['field']] ) );
                        //$e[1] = date( "Y-m-d", strtotime( trim( $e[1] ) ) );
                        //$e[2] = date( "Y-m-d", strtotime( trim( $e[2] ) ) );

                        $from_date = explode('/', trim($e[2]));
                        $to_date = explode('/', trim($e[1]));

                        $qry_val = ' BETWEEN DATE("' . $from_date[2] . '-' . $from_date[1] . '-' . $from_date[0] . '") AND DATE("' . $to_date[2] . '-' . $to_date[1] . '-' . $to_date[0] . '") AND ';
                    else :
                        $qry_val = ' LIKE "' . $post_fields[$f['field']] . '%" AND ';
                    endif;


                elseif ($f['type'] == 'number' || $f['type'] == 'amount' || $f['type'] == 'date' || !empty($f['val'])) :
                    // Operator || Status || Date ( Single || With Relation )

                    $op = ( $f['type'] == 'date' && $op == '=' ) ? ' LIKE ' : (!empty($op) ? $op : '=' );

                    if ($f['type'] == 'date') :
                        $format = 'Y-m-d';
                        $prcnt_symbl = ( $op == '=' ) ? '%' : '';

                        $post_fields[$f['field']] = $prcnt_symbl . date(( strpos($post_fields[$f['field']], ':') ? $format . ' H:i:s' : $format), strtotime($post_fields[$f['field']])) . $prcnt_symbl;
                    //$post_fields[$f['relation']]= !empty( $post_fields[$f['relation']] ) ? $prcnt_symbl. date( ( strpos( $post_fields[$f['relation']], ':' ) ? $format. ' H:i:s' : $format ), strtotime( $post_fields[$f['relation']] ) ) . $prcnt_symbl : '';

                    endif;


                    /* if( !empty( $f['relation'] ) && !empty( $post_fields[$f['relation']] ) ) :
                      // Relation

                      $qry_val = ' '. $op .' "' . $post_fields[$f['field']] . '" AND DATE('. ( strpos( $f['relation'], '.' ) ?  $f['relation'] : $f['alias'] .'.'. $f['relation'] ) .') ' . ( $post_fields[$f['relation']. '_op'] == '=' ? ( $op == '>=' ) : $post_fields[$f['relation']. '_op'] ) .' "' . $post_fields[$f['relation']] . '" AND ';
                      // from_date >= 2016-10-23 AND to_date <= 2016-10-25

                      unset( $post_fields[$f['relation']] );

                      else :

                      // Without Relation
                      $qry_val = ' '.$op .' "' . $post_fields[$f['field']] . '" AND ';
                      endif; */

                    $qry_val = ' ' . $op . ' "' . $post_fields[$f['field']] . '" AND ';


                else :
                    $qry_val = ' LIKE "%' . $post_fields[$f['field']] . '%" AND ';
                endif;


                if (!empty($f['alt_field'])) :

                    $alt_query_hvng .= $f['alt_field'] . $qry_val;
                else :
                    $query .= $qry_val;
                endif;

                $post_arr[$field] = array(
                    'lbl' => $lbl,
                    'symbol' => $symbol,
                    'value' => $post_fields[$f['field']],
                );

            endif;


        endforeach;

        $query = substr($query, 0, -4);
        $alt_query_slct = substr($alt_query_slct, 0, -2);
        $alt_query_hvng = substr($alt_query_hvng, 0, -4);

    endif;


    /* if( !empty( $fields ) && !empty( $post_fields ) ) :
      $args['where']['query'] = '';
      if( !empty( $fields['boq_code'] ) ) :
      $args['where']['query'] .= '`self`.`boq_code` LIKE "%'. $fields['boq_code'] .'%" AND ';
      endif;

      if( !empty( $fields['proj_name'] ) ) :
      $args['where']['query'] .= '`proj`.`proj_name` LIKE "%'. $fields['proj_name'] .'%" AND ';
      endif;

      if( !empty( $fields['comp_name'] ) ) :
      $args['where']['query'] .= '`comp`.`comp_name` LIKE "%'. $fields['comp_name'] .'%" AND ';
      endif;

      if( !empty( $fields['boq_sub_total_amt'] ) ) :
      if( empty( $fields['boq_sub_total_amt_op'] ) ) :
      $fields['boq_sub_total_amt_op'] = '=';
      endif;
      $args['where']['query'] .= '`self`.`boq_sub_total_amt` '. $fields['boq_sub_total_amt_op'] . $fields['boq_sub_total_amt'] .' AND ';
      endif;

      if( !empty( $fields['boq_net_amt'] ) ) :
      if( empty( $fields['boq_net_amt_op'] ) ) :
      $fields['boq_net_amt_op'] = '=';
      endif;
      $args['where']['query'] .= '`self`.`boq_net_amt` '. $fields['boq_net_amt_op'] . $fields['boq_net_amt'] .' AND ';
      endif;

      if( !empty( $fields['created_on'] ) ) :
      $args['where']['query'] .= '`self`.`created_on` LIKE "'. $fields['created_on'] .'%" AND ';
      endif;

      if( !empty( $fields['status'] ) ) :
      $args['where']['query'] .= '`self`.`status` = '. $fields['status'] .' AND ';
      endif;

      $args['where']['query'] = substr( $args['where']['query'], 0, -4 );

      endif; */

    return array(
        'query' => $query,
        'alt_query' => array(
            'select' => $alt_query_slct,
            'having' => $alt_query_hvng
        ),
        'posted' => $post_arr
    );

    /* return array(
      'filtered'  => !empty( $query ) ? TRUE : FALSE,
      'query'     => $query
      ); */
}

function inr_format($str, $digits = 2, $sepr = ',') {
    $return = '';
    $str = number_format($str, $digits, '.', '');
    if (empty($str)) :
        return $str;
    endif;

    $e = explode('.', $str);
    $len = strlen($e[0]);
    $lst = substr($e[0], -3);
    $str = substr($str, 0, $len - 3);
    for ($i = ($len - 4); $i >= 0; $i -= 2) :
        $return = ( (isset($str[$i - 1]) && ($str[$i - 1] !== NULL)) ? $str[$i - 1] : '' ) . ( $str[$i] != '-' ? $str[$i] . $sepr : $str[$i] ) . $return;
    endfor;

    return $return . $lst . '.' . $e[1];
}

function browser_info() {
    $userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
    if ((substr($_SERVER['HTTP_USER_AGENT'], 0, 6) == "Opera/") || (strpos($userAgent, 'opera')) != false) {
        $name = 'opera';
    } elseif ((strpos($userAgent, 'chrome')) != false) {
        $name = 'chrome';
    } elseif ((strpos($userAgent, 'safari')) != false && (strpos($userAgent, 'chrome')) == false && (strpos($userAgent, 'chrome')) == false) {
        $name = 'safari';
    } elseif (preg_match('/msie/', $userAgent)) {
        $name = 'msie';
    } elseif ((strpos($userAgent, 'firefox')) != false) {
        $name = 'firefox';
    } else {
        $name = 'unrecognized';
    }
//    if (preg_match('/.+(?:me|ox|it|ra|ie)[\/: ]([\d.]+)/', $userAgent, $matches) && $browser['name'] == 'safari') {
//        $version = $matches[1];
//    }
//    if (preg_match('/.+(?:me|ox|it|on|ra|ie)[\/: ]([\d.]+)/', $userAgent, $matches) && $browser['name'] != 'safari') {
//        $version = $matches[1];
//    }
//    else {
//        $version = 'unknown';
//    }

    return array(
        'name' => $name,
    );
}

function payment_terms() {
    return array(
        'COD' => 'COD',
        'E-Invoice' => 'E-Invoice',
        '14-Days' => '14-Days',
        '30-Days' => '30-Days',
        '45-Days' => '45-Days'
    );
}

?>