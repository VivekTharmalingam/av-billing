/*if(!document.attachEvent){
  Object.prototype.attachEvent=function(event,func){
    this.addEventListener(event.split('on')[1], func);
  }
}*/

/* 
 * Purpose      : Global scripts
 * Created On   : 2016-04-19 09:55
 */

var globalTimeout = '';
function setGlobalMsg( msg, type, delay ) {
    window.clearTimeout( globalTimeout );
    
    /*$('header:first').slideUp('slow');
    $('html, body').css({'margin-top': -$('header:first').height()+'px'});*/
    /*$('html, body').animate({
        scrollTop: $('header:first').height()
    }, 1000);*/
    
    if( typeof type == 'undefined' || type == '' ) {
        type = 'error';
    }
    
    if( typeof delay == 'undefined' || delay == '' || delay < -1 ) {
        delay = 5000;
    }else{
        delay *= 1000;
    }
    
    //console.log( 'Global Msg is activated' + ' Type: '+type + ' Delay: '+delay );
    
    var ico = 'thumb-tack';
    if( type == 'warning' ) {
        ico = 'warning';
    }else if( type == 'success' ) {
        ico = 'check-circle';
    }else if( type == 'info' ) {
        ico = 'info-circle';
    }else if( type == 'error' ) {
        ico = 'meh-o';
    }        
    
    $('#global-msg').html( '<i class="fa fa-'+ ico +' ico"></i>' + msg );
    $('#global-msg-bar').removeClass('warning success info error').addClass(type+' active').slideDown('slow');
    if( $('body').hasClass('container') && $('body').hasClass('mobile-detected') ) {
        $('#global-msg-bar').css({width: '95%'});
    }else if( $('body').hasClass('container') ) {    
        $('#global-msg-bar').css({width: '74%'});
    }else{
        $('#global-msg-bar').css({width: '100%'});
    }
    
    if( delay >= 0 || ( type == 'error' && delay >= 0 ) ) {
        globalTimeout = window.setTimeout(function(){
            closeGlobalMsg();
        }, delay);
    }
}

function closeGlobalMsg() {
    $('#global-msg-bar').slideUp('slow', function(){
        $('#global-msg').html( '' );
        $(this).removeClass('active');
        /*$('header:first').slideDown('slow', function(){
            $('html, body').css({'margin-top': '0px'});
        });*/
        
    });
}

function setOpts() {
    //$('#ribbon').append( $('#settings-panel').html() );
    
    //$('input[type="checkbox"]#smart-fixed-navigation').trigger('click');
    if( userOpts != '' && typeof userOpts != 'undefined' ) {
        $('body').attr('data-flag', '1');
        
        $('#'+userOpts.themeId).trigger('click');
        if( userOpts.fixContainer ) {
            //$('#smart-fixed-container').prop( 'checked', true );
            if( !$('#smart-fixed-container').is( ':checked' ) ) {
                $('#smart-fixed-container').trigger('click');
            }
        }
        
        if( userOpts.fixRibbon ) {
            $('#smart-fixed-ribbon').trigger('click');
        }else if( userOpts.fixNav ) {
            $('#smart-fixed-navigation').trigger('click');
        }else if( userOpts.fixHead ) {
            $('#smart-fixed-header').trigger('click');
        }
        
        if( userOpts.fixFooter ) {
            $('#smart-fixed-footer').trigger('click');
        }
        
        
        
        if( userOpts.fixTopMenu && !$('#smart-topmenu').is(':checked') ) {
            $('#smart-topmenu').trigger('click');
        }
        
        
        $('body').attr('data-flag', '');
    }
    
    
    $(document).on( 'click', '.demo.activate input[type="checkbox"], .demo.activate .btn, #smart-bgimages img', function(){
        if( $('body').attr('data-flag') != '' ) {
            return false;
        };
        
        
        var clas = ''
        ,   toReload = false
        ,   bgImgUrl = $('#smart-bgimages img.active').attr('data-htmlbg-url')
        ,   data = {};
        
        if( $('#smart-fixed-ribbon').is(':checked') ) {
            clas = ' fixed-header fixed-navigation fixed-ribbon';
            data.fixRibbon = true;
        }else if( $('#smart-fixed-navigation').is(':checked') ){
            clas = ' fixed-header fixed-navigation';
            data.fixNav = true;
        }else if( $('#smart-fixed-header').is(':checked') ){
            clas = ' fixed-header';
            data.fixHead = true;
        }
        
        if( $('#smart-fixed-footer').is(':checked') ) {
            clas += ' fixed-footer';
            data.fixFooter = true;
        }
        
        if( $('#smart-fixed-container').is(':checked') ) {
            clas += ' container';
            data.fixContainer = true;
        }
        
        if( $('#smart-topmenu').is(':checked') ) {
            clas += ' menu-on-top';
            data.fixTopMenu = true;
        }
        
        if( typeof bgImgUrl != 'undefined' && bgImgUrl != '' ) {
            var s = bgImgUrl.split('/');
            
            data.bgImg = s[s.length-1];
        }else if( userOpts.bgImg != '' && typeof userOpts.bgImg != 'undefined' ) {
            data.bgImg = userOpts.bgImg;
        }
        
        data.themeId = $('#skin-checked').parent('a').attr('id');
        data.class   = clas;   
        
        if( $(this).attr('id') == 'smart-topmenu' ) {
            toReload = true;
        }
        //alert( data.class );
        
        $.post( baseUrl + 'user/update_db/user_opts/' + curUserId, { data: data }, function(){
            
            if( toReload ) {
                location.reload();
            }
            
        });
        
        
    });
    
}
    

$(document).on('ready', function() {
    
    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    pageSetUp();
   
	$(document).on('drop', '.cost, .price', function() {
		event.preventDefault();
	});
	
    $('.dialog').dialog({
        autoOpen : false
    });
    
    setOpts();
    
    $(document).on('keyup', '.percent', function() {
        
        if( $(this).val() > 100 ) {
            $(this).val('100');
        }else if( $(this).val() < 0 ) {
            $(this).val('0');
        }
    })
    
    $('.amt').on('focus', function(){
        $(this).attr( 'data-placeholder', $(this).attr( 'placeholder') ); 
        $(this).attr( 'placeholder', '');
        $(this).addClass('text-right');
    });
    
    $('.amt').on('blur', function(){
        
        if( $(this).val() == '' ) {
            
            var placeHldr = $(this).attr( 'data-placeholder') || "";
            $(this).removeClass('text-right');
            $(this).attr( 'placeholder', placeHldr ); 
        }
    });
    
    
    
    $(document).on('keyup keypress', '.number, .float', function(){
        var value = $(this).val()
        ,   pattern = /^(\d+)$/;

        if( $(this).hasClass('amt') || $(this).hasClass('float') ) {
            pattern = /^([0-9.,]|[0-9])+$/;
        }

        if( value != '' && !pattern.test( value ) ) {
            if( $(this).hasClass('amt') || $(this).hasClass('float') ) {
                // If this is the Amount field then, ignore ‚dot‘ and ‚comma‘ chars
                value = value.replace( /[^0-9.,]/g, '' );
            }else{
                value = value.replace( /\D+/g, '' );
            }    
            $(this).focus().val( value );
            //alert( 'Only numbers are allowed.' );
            return false;
        }
    });

    
    
    
    if( $('nav li.open').length > 0 && !userOpts.fixTopMenu ) {
            
        var $open   = $('nav li.open')
        ,   $master = $open.parent('ul').parent();
        
        if( $open.hasClass('dashboard') ) {
            return false;
        }
        
        /*if( $master.parent().attr('id') != 'menu' ) {
           $master = $master.parent();
        }*/

        //$open.find('a').trigger('click');
        
        //window.setTimeout( function(){
            //$master.find('a').trigger('click');
            $master.addClass('open');
            $master.find('ul:first').show('slow',function(){
                $open.find('ul:first').slideDown();
            });
        //}, 50);
        
        //console.log( 'ID: '+ $master.attr( 'id' ) );
        //$open.removeAtrr('style');
        
        var _class = ($open.closest('.mstr-menu').data('clr') != undefined) ? $open.closest('.mstr-menu').data('clr') : '';
        _class = _class.replace("_clr", "");
        $open.closest('.mstr-menu').addClass(_class);
    }
    
    $('.preview').each( function(i, ikey) {
        var $file = $(this).parent().find( 'input[type="file"]' )
        ,   actual = $file.attr( 'data-actual' );
        
        
        if( typeof actual != 'undefined' && actual != '' && actual != '0'  ) {
            $(this).append( '<div class="remove-img-btn confirm" data-key="'+ i +'" data-url="'+ $file.attr( 'data-url') +'" title="Remove" ><i class="fa fa-close" ></i></div><input type="hidden" name="hidden-file" id="hidden-file-'+ i +'" value="'+ actual +'">' );
        }
    });
    
    $(document).on('click', '.dialog-btn', function(){
        var target  = $(this).attr('data-target');
        
        if( typeof target == 'undefined' || target == '' ) {
            target = '.dialog';
        }
        
        $(target).dialog( 'open' );
    });
    
    $('.remove-img-btn').on( 'click', function(e){
        //alert( $( e.target ).attr( 'class' ) );
        
        var $this   = $(this)
        ,   $parent = $this.parent().closest('section')
        ,   id      = $this.attr( 'data-id' )
        ,   key     = $this.attr( 'data-key' )
        ,   dataUrl = $this.attr('data-url')
        ,   data = {
            'action' : 'remove',
        }; 
        
        if( typeof actual != 'undefined' && actual != ''  ) {
            data.id = id;
        }
        
        //$.post( dataUrl, { data : data }, function( results ) {
            $this.remove();
            $( '#hidden-file-'+ key ).val('');
            $parent.find('.preview img').attr( 'src', $parent.find('input[type="file"]').attr( 'data-default' ) );
        //});
        
    });
    
    $('input[type="file"]').on('change', function(e){
//        var url = ''
//        ,   $parent = $(this).parent().closest('section');
//        if( $(this).val() == ''  ) {
//            url = $(this).attr( 'data-default' );
//        }else {
//            url = URL.createObjectURL( e.target.files[0] );
//        }
//        
//        $parent.find('.preview img').attr( 'src', url );
//        $parent.find('.remove-img-btn').remove();
        
        
    });
    
    $('.img_file').on('change', function(){
        $('.browse_logo').val($(this).val());
    });
    
    $('#global-msg-bar .clse-btn').on('click', function(){
        closeGlobalMsg();
    });
    
    $(document).on('.confirm', 'click', function(e) {
        var bindUrl = $(this).data('url')
        ,   bindText = $(this).data('text');
        
        $.SmartMessageBox({
                title : "Are you sure?",
                content : "You will not be able to recover this!",
                buttons : '[No][Yes]'
        }, function( ButtonPressed ) {
                if( ButtonPressed === "Yes" ) { 
                    if( typeof bindUrl == 'undefined' || bindUrl == '' ) {
                        return true;
                    }else{    
                        window.location.replace( bindUrl );
                    }    
                }else if( ButtonPressed === "No" ) {
                        $.smallBox({
                                title : "Sorry ! ...",
                                content : "<i class='fa fa-info-circle'></i> <i>You just selected NO...<br>So the action could not be completed! </i>",
                                color : "#C46A69",
                                iconSmall : "fa fa-times fa-2x fadeInRight animated",
                                timeout : 4000
                        });
                }

        });
        e.preventDefault();
    })
    
    
    $('.smart-mod-delete').on('click', function(e) {
        var bindUrl = $(this).data('url')
        ,   bindText = $(this).data('text');
        
        if( typeof bindUrl == 'undefined' || bindUrl == '' ) {
            return false;
        }
        
        $.SmartMessageBox({
                title : "Are you sure?",
                content : "You will not be able to recover this "+ bindText +" details!",
                buttons : '[No][Yes]'
        }, function( ButtonPressed ) {
            if( ButtonPressed === "Yes" ) { 
                window.location.replace( bindUrl );
            }
            else if( ButtonPressed === "No" ) {
                $.smallBox({
                    title : "Sorry ! ...",
                    content : "<i class='fa fa-info-circle'></i> <i>You just selected NO...<br>So the action of DELETE is disallowed for removing the "+ bindText +" details.. </i>",
                    color : "#C46A69",
                    iconSmall : "fa fa-times fa-2x fadeInRight animated",
                    timeout : 4000
                });
            }
        });
        e.preventDefault();
    })
    
})


// Your GOOGLE ANALYTICS CODE Below
    
/*var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
_gaq.push(['_trackPageview']);


(function() {
var ga = document.createElement('script');
ga.type = 'text/javascript';
ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(ga, s);
})();*/


/****** Other Defaults *******/

function hide_available_quantity() {
	$('.stock-window').hide();
}

function show_available_quantity(currentTarget, branch_products) {
	$('.stock-window-content .branch-row').remove();
	if (branch_products.length) {
		for(var i in branch_products) {
			var item = $('.stock-window-content .no-item').clone();
			item.removeClass('no-item').addClass('branch-row');
			var product = branch_products[i];
			item.find('.branch-name').html(product.branch_name);
			var available_qty = '';
			if (product.available_qty <= 0) {
				available_qty += '<i class="fa fa-exclamation-triangle text-warning"></i> &nbsp;';
			}
			else {
				available_qty += '<i class="fa fa-check-circle text-success"></i> &nbsp;';
			}
			available_qty += product.available_qty;
			item.find('.available-qty').html(available_qty);
			$('.stock-window-content').append(item);
		}
	}
	else {
		var item = $('.stock-window-content .no-item').clone();
		item.removeClass('no-item').addClass('branch-row');
		item.find('.branch-name').html('Not available!');
		item.find('.available-qty').html('');
		$('.stock-window-content').append(item);
	}
	if($(currentTarget).is("input")) {
            var _top = $(currentTarget).offset().top + $(currentTarget).outerHeight() + 'px';
            var _left = $(currentTarget).offset().left + 'px';
        } else {
            var _top = $(currentTarget).offset().top + 'px';
            var _left = $(currentTarget).offset().left + $(currentTarget).width() + 'px';  
        }
	$('.stock-window').css('top', _top);
	$('.stock-window').css('left', _left);
	$('.stock-window').show();
}

var focus_element;
function set_timepicker() {
    $(".timepicker").timepicker({
        showInputs: false
    });
}

function reset_timepicker() {
    $('.timepicker:not(.value_setted)').val('');
    $(".timepicker").addClass('value_setted');
}

function set_datepicker() {
    $('.datepicker').each(function(index, element) {
        if ($(element).attr('datepicker_set') === undefined) {
            $(element).datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
                todayHighlight: true,
                startView: 0,
            });
            
            $(element).prop('autocomplete', 'off');
            $(element).attr('datepicker_set', true);
        }
    });
}

function set_datetimepicker() {
    $('.datepicker').each(function(index, element) {
        if ($(element).attr('datetimepicker_set') === undefined) {
            $(element).datetimepicker();
            
            $(element).prop('autocomplete', 'off');
            $(element).attr('datetimepicker_set', true);
        }
    });
}

function convert_str_to_date(date_str, time_str) {
    var date, month, year;
    if (date_str != '') {
        date = parseInt(date_str.substr(0, 2));
        month = parseInt(date_str.substr(3, 2));
        year = parseInt(date_str.substr(6, 4));
    }
    else {
        var cur_date = new Date();
        date = cur_date.getDate();
        month = cur_date.getMonth();
        month = month + 1;
        year = cur_date.getFullYear();
    }
    
    var hr = 0;
    var mins = 0;
    if (time_str != '') {
        hr = parseInt(time_str.substr(0, 2));
        mins = parseInt(time_str.substr(3, 2));
        var periode = time_str.substr(6, 2);
        
        if (periode == 'PM') {
            if (hr != 12) {
                hr = hr + 12;
            }
        }
        else if (hr == 12) {
            hr = 0;
        }
    }

    return new Date(year, month - 1, date, hr, mins)
}

function cal_date_diff_in_ms(date1, date2) {
    return date2.getTime() - date1.getTime();
}

function cal_date_diff(date1, date2) {
    var date_in_ms = date2.getTime() - date1.getTime();
    return new Date(date_in_ms);
}

function open_new_window(msg) {
    $('#new_window .modal-body').html(msg);
    $("#new_window").modal({
        backdrop: "static",
        keyboard: true,
        show: true
    });
}

function show_alert(msg) {
    $('#alert_message .modal-body .message_container').html(msg);
    $("#alert_message").modal({
        backdrop: "static",
        keyboard: true,
        show: true
    });
}

function show_confirm(msg, redirect_url) {
    $('#confirm .modal-body .message_container').html(msg);
    $('#confirm .modal-footer input[name="redirect_url"]').val(redirect_url);
    $("#confirm").modal({
        backdrop: "static",
        keyboard: true,
        show: true
    });
}

function show_loader(msg) {
    $('#loader_popup .modal-body .loading_content').html(msg);
    $("#loader_popup").modal({
        backdrop: "static",
        show: true
    });
}

function hide_loader() {
    $("#loader_popup").modal('hide');
}

function show_log_in_console(log_obj, error_thrown) {
    console.log(log_obj.responseText);
    //alert(log_obj.responseText);
}

function file_preview(file) {
    var file_data = $(file).prop('files')[0];
    var _URL = window.URL || window.webkitURL;
    var img = new Image();
    var file_size = file_data.size / 1024;
    file_size = file_size / 1024;
    img.onload = function() {
        var return_val = true;
        if (($(file).attr('data-allowed_width') !== undefined) && ($(file).attr('data-allowed_width') != '')
            || ($(file).attr('data-allowed_height') !== undefined) && ($(file).attr('data-allowed_height') != '')) {
            var allowed_width = parseFloat($(file).attr('data-allowed_width'));
            var allowed_height = parseFloat($(file).attr('data-allowed_height'));
            if ((allowed_width != img.width) || (allowed_height != img.height)) {
                alert('Image size should be Width: ' + allowed_width + 'px X Height: ' + allowed_height + 'px');
                $(file).val('');
                return_val = false;
            }
        }
        else if ($(file).attr('data-allowed_size') != 'undefined') {
            var allowed_size = parseInt($(file).attr('data-allowed_size'));
            if (file_size > allowed_size) {
                alert('The file size exceeds the limit allowed and cannot be upload!');
                $(file).val('');
                return_val = false;
            }
        }
        
        if (return_val) {
            $(file).parents('.section-group').find('.img_preview').html(img);
        }
        
        return return_val;
    }
    img.src = _URL.createObjectURL(file_data);  
}

function show_chosen_tooltip(_this) {
    $('#tool_tip').css('top', $(_this).offset().top - 10);
    $('#tool_tip').css('left', $(_this).offset().left + 230);
    $('#tool_tip .tool_tip_content').html($(_this).attr('tool_tip_text'));
    $('#tool_tip').show();
}

function hide_tooltip() {
    $('#tool_tip').hide();
}

function set_chosen() {
    $('.chosen-select').each(function(index, element) {
        if (!$(element).hasClass('has_chosen')) {
            $(element).chosen({
                allow_single_deselect: true,
                placeholder_text_single: 'Select an Option',
                no_results_text: 'No results match!'
            });
            $(element).addClass('has_chosen');
            
            $(document).on('mouseover', '.active-result', function() {
                if ($(this).attr('tool_tip_text') !== undefined) {
                    show_chosen_tooltip(this);
                }
            });
            
            $(document).on('mouseout', '.active-result', function() {
                if ($(this).attr('tool_tip_text') !== undefined) {
                    hide_tooltip();
                }
            });
        }
        /*else {
            $(element).trigger("chosen:updated");
        }*/
    });
}

function update_choosen(_this){
    $(_this).trigger("chosen:updated");
}

function showSmallBox(title, content) {
    $.smallBox({
        title : title,
        content : content,
        color : "#C46A69",
        iconSmall : "fa fa-times fa-2x fadeInRight animated",
        timeout : 4000
    });
}

function asyncAjaxRequest(url, data, _option) {
	if ((typeof(_option) == 'object') && (_option.loader_show !== undefined)) {
		_option.loader_show();
	}
	
    $.ajax({
        async: true,
        url: url,
        type: 'POST',
        data: data,
        dataType: 'JSON'
    })
    .done(function(data) {
		if ((typeof(_option) == 'object') && (_option.loader_hide !== undefined)) {
			_option.loader_hide();
		}
		
        if ((typeof(_option) == 'object') && (_option.call_back_done !== undefined)) {
			_option.call_back_done(data);
		}
    })
    .fail(function(jqXHR, status_code, error_thrown) {
		if ((typeof(_option) == 'object') && (_option.loader_hide !== undefined)) {
			_option.loader_hide();
		}
		
        console.log(error_thrown);
		if ((typeof(_option) == 'object') && (_option.call_back_fail !== undefined)) {
			_option.call_back_fail(data);
		}
    });
}

function ajaxRequest(url, data, loaderOnField) {
    if (loaderOnField != '') {
        $(loaderOnField).after('<span class="ajax-loading">&nbsp;</span>');
        $(loaderOnField).prop('disabled', true);
    }
    
    var respData;
    $.ajax({
        async: false,
        url: url,
        type: 'POST',
        data: data,
        dataType: 'JSON'
    })
    .done(function(data) {
        if (loaderOnField != '') {
            $(loaderOnField).prop('disabled', false);
            $('.ajax-loading').remove();
        }
        //console.log(data);
        respData = data;
    })
    .fail(function(jqXHR, status_code, error_thrown) {
        show_log_in_console(jqXHR, error_thrown);
    });
    
    return respData;
}

function update_left_character_count(_this, text_count) {
	$(_this).next().find('b').html(parseInt(text_count) - $(_this).val().length);
}

function show_textarea_count() {
    $('textarea.show_txt_count:not(.count-left-added)').each(function() {
        var text_count = (typeof($(this).attr('maxlength')) != 'undefined') ? $(this).prop('maxlength') : 500;
        $(this).after('<div class="text-muted txt_left">Left: <b>' + text_count + '</b> charecters only.</div>');
        $(this).addClass('count-left-added');
		update_left_character_count(this, text_count);
        $(this).on('keyup', function() {
			update_left_character_count(this, text_count);
        });
    });
}

/*$(window).load(function() {
    $(".page_loader").fadeOut("slow");
});*/

function bootstrap_datepk_addrow() {
    $('.bootstrap-datepicker-comncls').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
}

function show_switch_branch_message() {
    if ($('.branch_switched').length > 0) {
        if ($('#shortcut').data('branch_update') == 'S') {
            $.smallBox({
                title : "Success",
                content : "<i class='fa fa-clock-o'></i> <i>Branch has been changed successfully</i>",
                color : "#5b835b",
                iconSmall : "fa fa-thumbs-up bounce animated",
                timeout : 4000
            });
        }
        else {
            $.smallBox({
                title : "Fail",
                content : "<i class='fa fa-clock-o'></i> <i>Branch not changed!. Please try again</i>",
                color : "#77021d",
                iconSmall : "fa fa-thumbs-down bounce animated",
                timeout : 4000
            });
        }
    }
}

$(document).ready(function() {
    /*
     * setTimeout() function will be fired after page is loaded
     * it will wait for 5 sec. and then will fire
     */ 
    $(document).on('change', 'input:not(.lowercase), textarea:not(.lowercase)', function(e) {
		$(this).val($(this).val().toUpperCase());
	});
	
    /* bootstrap datepicker */
    $('.bootstrap-datepicker-comncls').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    /* end */
    
    /* bootstrap datepicker with startDate */
    $('.bootstrap-datepicker-strdatecls').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true,
        startDate: new Date()
    });
    /* end */
    
	// Function to open new window while clicking .new-window-open element
	$('.new-window-popup').on('click', function() {
		$('#new_window iframe').prop('src', $(this).data('iframe-src'));
		$('#new_window').data('cmd', $(this).data('cmd'));
		$('#new_window .modal-title').html($(this).data('title'));
		
		var iframe_height = $('#new_window iframe').height();
		$('#new_window .modal-content').height((iframe_height + 130) + 'px');
		$('#new_window .modal-dialog').height((iframe_height + 130) + 'px');
		
		$("#new_window").modal({
			backdrop: "static",
			keyboard: true,
			show: true
		});
		
		$('#new_window iframe').contents().find('#content form input[type="text"]:first').focus();
	});
	
	// Callback function of window close
	$('#new_window').on('hidden.bs.modal', function (e) {
                //alert($('#new_window').data('cmd'));
		switch($('#new_window').data('cmd')) {
			case 'item':
			    // Function exists in sales_order.js
			    reset_item();
				break;
			
			case 'customer':
				// Function exists in sales_order.js
				reset_customer();
				break;
			
			case 'brand':
				// Function exists in product.js
				reset_brand();
				break;
					
			case 'supplier':
				// Function exists in sales_order.js
				reset_supplier();
				break;
			
			default:
				break;
		}
	});
	
    setTimeout(function() {
        $(".flash_msg").hide();
        //$(".alert-dismissable").slideUp();
        $(".alert:not(.alert-info, .not-dismissable)").slideUp();
    }, 5000);
    
    setTimeout(function() {
        show_switch_branch_message();
    }, 1000);
    

    $(document).on('change','.img_file', function(e){
        file_preview($(this));
    });
	
	if ($('.quantity, .price').length) {
		$('.quantity, .price').each(function() {
			$(this).attr('autocomplete', 'off');
		});
	}
	
	$(document).on('change','.img_file', function(e){
        file_preview($(this));
    });
	
    $(document).on('click','.im_link', function(e){
        window.location.href = $(this).attr('data-href');
    });
    
    $(document).on('click','.cancel', function(e){
        if (typeof($(this).data('url')) !== 'undefined') {
            location.href = baseUrl + $(this).data('url');
        }
        else {
            window.history.back();
        }
    });
    
    /* To set js cache storage reset hover text */
    if (($('#ribbon').length > 0) && ($('#ribbon .ribbon-button-alignment').length == 0)) {
        var cache_reset = '<span class="ribbon-button-alignment">';
        cache_reset += '<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class=\'text-warning fa fa-warning\'></i> Warning! This will reset all your widget settings." data-html="true">';
        cache_reset += '<i class="fa fa-refresh"></i>';
        cache_reset += '</span>';
        cache_reset += '</span>';
        $('#ribbon').prepend(cache_reset);
    }
    
    show_textarea_count();
    
    /* To set Water mark in view page if span[id="watermark"] is empty*/
    if ($('#watermark').length > 0) {
        if ($('#watermark').html().trim().length == 0) {
            $('#watermark').html('Malar ERP.')
        }
    }
    
    $(document).on('click','.delete-confirm', function(e) {
        var _this = $(this);
        $.SmartMessageBox({
            title : "Are you sure?",
            content : $(_this).data('confirm-content'),
            buttons : '[No][Yes]'
        }, function(ButtonPressed) {
            if (ButtonPressed === "Yes") {
                window.location.replace($(_this).data('redirect-url'));
            }
            if (ButtonPressed === "No") {
                var action = ($(_this).data('action') === undefined) ? 'DELETE' : $(_this).data('action');
                var smallBoxTitle = "Sorry ! ...";
                var smallBoxMsg = "<i class='fa fa-info-circle'></i> <i>You just selected NO...<br>So the action of " + action + " is disallowed for the " + $(_this).data('bindtext') + " details.. </i>";
                showSmallBox(smallBoxTitle, smallBoxMsg);
            }
        });
        e.preventDefault();
    });
    
    $(document).on('click','.formpost-confirm', function(e) {
        var _this = $(this);
        $.SmartMessageBox({
            title : "Are you sure?",
            content : $(_this).data('confirm-content'),
            buttons : '[No][Yes]'
        }, function(ButtonPressed) {
            if (ButtonPressed === "Yes") {
                var form = document.createElement("form");
                $(form).attr("action", $(_this).data('redirect-url'))
                       .attr("method", "post");
                $(form).html('<input type="hidden" name="' + $(_this).data('bindinputfield') + '" value="' + $(_this).data('bindpostid') + '" />');
                document.body.appendChild(form);
                $(form).submit();
                document.body.removeChild(form); 
            }
            if (ButtonPressed === "No") {
                var action = ($(_this).data('action') === undefined) ? 'DELETE' : $(_this).data('action');
                var smallBoxTitle = "Sorry ! ...";
                var smallBoxMsg = "<i class='fa fa-info-circle'></i> <i>You just selected NO...<br>So the action of " + action + " is disallowed for the " + $(_this).data('bindtext') + " details.. </i>";
                showSmallBox(smallBoxTitle, smallBoxMsg);
            }
        });
        e.preventDefault();
    });

    $(document).on('click','.delete-prevention', function(e) {    
        var _this = $(this);
        $.SmartMessageBox({
            title : "Oops !...",
            content : $(_this).data('prevention-content'),
            buttons : '[OK]'
        }, function(ButtonPressed) {
            if (ButtonPressed === "OK") {
                var smallBoxTitle = "Sorry ! ...";
                var smallBoxMsg = "<i class='fa fa-info-circle'></i> <i>So the action of DELETE is disallowed for the " + $(_this).data('bindtext') + " details.. </i>";
                showSmallBox(smallBoxTitle, smallBoxMsg);
            }
        });
        e.preventDefault();
    });

    $(document).on('click','.action-prevention', function(e){
        
        var _this = $(this);
        $.SmartMessageBox({
            title : "Oops !...",
            content : $(_this).data('prevention-content'),
            buttons : '[OK]'
        }, function(ButtonPressed) {
            if (ButtonPressed === "OK") {
                var smallBoxTitle = "Sorry ! ...";
                var smallBoxMsg = "<i class='fa fa-info-circle'></i> <i>You just selected NO...<br>So the Action is disallowed for the " + $(_this).data('bindtext') + " details.. </i>";
                showSmallBox(smallBoxTitle, smallBoxMsg);
            }
        });
        e.preventDefault();
    });
  
    $(document).on('click', '.delete-confirm-ajax', function(e) {
        
        var _this = $(this);
        $.SmartMessageBox({
            title : "Are you sure?",
            content : $(_this).data('confirm-content'),
            buttons : '[No][Yes]'
        }, function(ButtonPressed) {
            if (ButtonPressed === "Yes") {
                window.location.replace($(_this).data('redirect-url'));
            }
            if (ButtonPressed === "No") {
                var action = ($(_this).data('action') === undefined) ? 'DELETE' : $(_this).data('action');
                var smallBoxTitle = "Sorry ! ...";
                var smallBoxMsg = "<i class='fa fa-info-circle'></i> <i>You just selected NO...<br>So the action of " + action + " is disallowed for the " + $(_this).data('bindtext') + " details.. </i>";
                showSmallBox(smallBoxTitle, smallBoxMsg);
            }
        });
        e.preventDefault();
    });
    
    $(document).on('keydown', '.float_value', function(e) {
        e = (e) ? e : window.event;
        var charCode = (e.which) ? e.which : e.keyCode;
        if (((charCode >= 96 && charCode <= 105) || charCode == 110) // Number pad
            || ((charCode >= 48 && charCode <= 57) || charCode == 190)
            || ((charCode >= 37 && charCode <= 40) || charCode == 8 || charCode == 9 || charCode == 13 || charCode == 46 ) //  Left, right, top, bottom, Backspace, tap, enter, delete
            ) { // Conditions to check float value
            
            return true;
        }
        
        return false;
    });
    
    $(document).on('keyup', '.float_value', function(e) {
        var value = $(this).val().trim();
        if (value == '') 
            return true;
		else if (parseFloat(value) == 0) {
			//$(this).val('0');
			$(this).focus();
			return true;
		}
        
        var pattrn = /^\d+(\.\d{1,2})?$/;
        if (!pattrn.test(value)) {
            var amount_arr = value.split('.');
            var temp_amt_arr = new Array();
            pattrn = /^[0-9]$/;
            //var dot_index = value.indexOf('.');
            var amount = '';
            /*if (pattrn.test(amount_arr[0])) 
                amount = amount_arr[0];
            else {*/
                temp_amt_arr = amount_arr[0].split('');
                for(var i = 0; i < temp_amt_arr.length; i ++) {
                    amount += (pattrn.test(temp_amt_arr[i])) ? temp_amt_arr[i] : '';
                }
            //}
            
            var decimal = (amount_arr[1] !== undefined) ? amount_arr[1] : '';
            if (decimal.length > 0) {
                decimal = decimal.substr(0, 2);
                amount += '.';
                
                    temp_amt_arr = decimal.split('');
                    for(var i = 0; i < temp_amt_arr.length; i ++) {
                        amount += (pattrn.test(temp_amt_arr[i])) ? temp_amt_arr[i] : '';
                    }
            }
            else if (value.indexOf('.') == (value.length - 1)) {
                amount += '.';
            }
            
            /*if (decimal.length > 0) {
                $(this).val(amount);
            }*/
            $(this).val(amount);
        }
    });
    
    $(document).on('keydown', '.integer_value', function(e) {
        e = (e) ? e : window.event;
        var charCode = (e.which) ? e.which : e.keyCode;
        if ((charCode >= 96 && charCode <= 105) || (charCode >= 48 && charCode <= 57) || (charCode >= 37 && charCode <= 40)
            || charCode == 8 || charCode == 9 || charCode == 13 || charCode == 46) { // Conditions to check integer
            return true;
        }
        
        return false;
    });
    
    String.prototype.ucfirst = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }    
    
    /* Array.prototype.diff = function(a) {
        return this.filter(function(i) {return a.indexOf(i) < 0;});
    }; */
});