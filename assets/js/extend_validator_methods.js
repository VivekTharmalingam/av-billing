// JavaScript Document
$(document).ready(function(e) {
    $.validator.addMethod("valid_amount", function(value, element) {
        if (value == '') 
            return true;
        
        var pattrn = /^\d+(\.\d{1,2})?$/;
        return (pattrn.test(value));
    });
    
    $.validator.addMethod("alphabets", function(value, element) {
        if (value == '') 
            return true;
        
        var pattrn = /^[a-zA-Z ]+$/;
        return (pattrn.test(value));
    });
    
    $.validator.addMethod("valid_phone", function(value, element) {
        if (value == '') 
            return true;
        
        var pattrn = /^[0-9 +]+$/;
        return (pattrn.test(value));
    });
    
    $.validator.addMethod('phone_minlength', function(value, element) {
        return (value.trim().length < 8) ? false : true;
    });
    
    $.validator.addMethod('phone_maxlength', function(value, element) {
        return (value.trim().length > 12) ? false : true;
    });
    
    $.validator.addMethod("pattrn_valid_amount", function(value, element) {
        var filter = /^\d+(\.\d{1,2})?$/;
        return (filter.test(value));
    });
    
    $.validator.addMethod('date_pattern', function(value, element) {
        return (/^[0-9]{2}-[0-9]{2}-[0-9]{4}$/).test(value);
    });
    
    $.validator.addMethod('email_pattern', function(value, element) {
        return (/^[0-9a-zA-Z_.-]+\@[0-9a-zA-Z_.-]+\.[0-9a-zA-Z_.-]+$/).test(value);
    });
});