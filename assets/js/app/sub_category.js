$(document).ready(function () {

    var companyForm = $("#sub_cat_form").validate({
        // Rules for form validation
        rules: {
            item_category: {
                required: true
            },
            sub_category: {
                required: true
            }
        },
        // Messages for form validation
        messages: {
            item_category: {
                required: 'Please select Category'
            },
            sub_category: {
                required: 'Please enter Sub Category'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

});
