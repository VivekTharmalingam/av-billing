function calculate_ledger_balance() {
	var closing_amt = 0;
	$('input[name="opening_balance"]').each(function() {
		closing_amt += ($(this).val() != '') ? parseFloat($(this).val()) : 0;
	});
	
	$('input.group_name[value="1"]').closest('.sales-amt').find('input.amount').each(function() {
		closing_amt += ($(this).val() != '') ? parseFloat($(this).val()) : 0;
	});
	
	$('input.prev_group_name[value="1"]').closest('.sales-amt').find('input.amount').each(function() {
		closing_amt += ($(this).val() != '') ? parseFloat($(this).val()) : 0;
	});
	
	$('input[name="purchase_amount"], input[name="expense_amount"], input[name="bill_payment"], input[name="refund_amount"]').each(function() {
		closing_amt -= ($(this).val() != '') ? parseFloat($(this).val()) : 0;
	});
	
	$('input[name="ledger_amount"]').val(closing_amt.toFixed(2));
	calculate_closing_balance();
}

function calculate_closing_balance() {
	var ledger_amount = ($('input[name="ledger_amount"]').val() != '') ? parseFloat($('input[name="ledger_amount"]').val()) : 0;
	ledger_amount -= ($('input[name="banking_amount"]').val() != '') ? parseFloat($('input[name="banking_amount"]').val()) : 0;
	$('input[name="closing_balance"]').val(ledger_amount.toFixed(2));
}

function get_amounts_done(data) {
	var opening_balance = (data.opening_balance != null) ? parseFloat(data.opening_balance) : 0;
	var total_sales_amount = (data.total_sales_amount != null) ? parseFloat(data.total_sales_amount) : 0;
	var unpaid_invoice_amount = (data.unpaid_invoice_amount != null) ? parseFloat(data.unpaid_invoice_amount) : 0;
	for(var i in data.sales_payment) {
		var sale = data.sales_payment[i];
		var sale_amount = (sale.total_amt != null) ? parseFloat(sale.total_amt) : 0;
		$('.type_name[value="' + sale.type_name + '"]').closest('.sales-amt').find('.sale_amount').val(sale_amount);
	}
	
	for(var i in data.prev_day_sales_payment) {
		var sale = data.prev_day_sales_payment[i];
		var sale_amount = (sale.total_amt != null) ? parseFloat(sale.total_amt) : 0;
		$('.prev_type_name[value="' + sale.type_name + '"]').closest('.sales-amt').find('.prev_sale_amount').val(sale_amount);
	}
	var prev_sales_amount = (data.prev_sales_amount != null) ? parseFloat(data.prev_sales_amount) : 0;
	var expense_amount = (data.expense_amount != null) ? parseFloat(data.expense_amount) : 0;
	var bill_amount = (data.bill_amount != null) ? parseFloat(data.bill_amount) : 0;
	var refund_amount = (data.refund_amount != null) ? parseFloat(data.refund_amount) : 0;
	var purchase_amount = (data.purchase_amount != null) ? parseFloat(data.purchase_amount) : 0;
	var banking_amount = (data.banking_amount != null) ? parseFloat(data.banking_amount) : 0;
	$('input[name="opening_balance"]').val(opening_balance.toFixed(2));
	$('input[name="total_sales"]').val(total_sales_amount.toFixed(2));
	$('input[name="unpaid_amount"]').val(unpaid_invoice_amount.toFixed(2));
	$('input[name="prev_day_inv_amount"]').val(prev_sales_amount.toFixed(2));
	$('input[name="expense_amount"]').val(expense_amount.toFixed(2));
	$('input[name="bill_payment"]').val(bill_amount.toFixed(2));
	$('input[name="refund_amount"]').val(refund_amount.toFixed(2));
	$('input[name="purchase_amount"]').val(purchase_amount.toFixed(2));
	$('input[name="banking_amount"]').val(banking_amount.toFixed(2));
	calculate_ledger_balance();
	calculate_closing_balance();
}

function get_amounts_fail(data) {
	
}

function get_amounts() {
	var url = baseUrl + 'petty_cash/get_petty_cash_amount';
	var data = {
		petty_cash_date : $('input[name="petty_cash_date"]').val()
	};
	var _option = {
		call_back_done : get_amounts_done,
		call_back_fail : get_amounts_fail
    };
	
    asyncAjaxRequest(url, data, _option);
}

$(document).ready(function () {
	calculate_ledger_balance();
	calculate_closing_balance();
	
	$('input[name="opening_balance"], input[name="total_sales"], input[name="nets_amount"], input[name="unpaid_amount"], input[name="cheque_amount"], input[name="prev_day_inv_amount"], input[name="expense_amount"], input[name="bill_payment"], input[name="refund_amount"], input[name="purchase_amount"]').on('keyup', function() {
		calculate_ledger_balance();
	});
	
	$('input[name="banking_amount"]').on('keyup', function() {
		calculate_closing_balance();
	});
	
	$('#total-sales').on('click', function() {
		$('#total-sales i').toggleClass('fa-plus-square-o fa-minus-square-o');
		if ($('.total-sales-content').css('display') == 'block') {
			$('.total-sales-content').slideUp();
		}
		else {
			$('.total-sales-content').slideDown();
		}
	});
	
	$('#prev-sales-amt').on('click', function() {
		$('#prev-sales-amt i').toggleClass('fa-plus-square-o fa-minus-square-o');
		if ($('.prev-day-sales-content').css('display') == 'block') {
			$('.prev-day-sales-content').slideUp();
		}
		else {
			$('.prev-day-sales-content').slideDown();
		}
	});
	
	$('input[name="petty_cash_date"]').on('changeDate', function() {
		if (this.value.trim() != '') {
			get_amounts();
			$('#petty_cash_form').validate().element(this);
		}
	});
	
	$.validator.addMethod('already_exists', function (value, element) {
        var return_val = true;
		var ajaxURL = baseUrl + 'petty_cash/date_already_exists';
		var ajaxData = {
			petty_cash_date: value
		}
		
		var data = ajaxRequest(ajaxURL, ajaxData, '');
		if (data.exists >= 1) {
			return_val = false;
		}
        
        return return_val;
    });
	
	$('#petty_cash_form').validate({
        // Rules for form validation
        rules: {
            petty_cash_date: {
                required: true,
				already_exists: true
            }
        },
        // Messages for form validation
        messages: {
            petty_cash_date: {
                required: 'Please select/enter Date!',
				already_exists: 'Petty Cash already closed for this date!'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
});
