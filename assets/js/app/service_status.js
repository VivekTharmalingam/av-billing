$(document).ready(function () {
    
    $.validator.addMethod("service_statusExist", function (value, element) {
        var return_val = true;
        var service_statusName = $('#status_name').val();
        var service_statusId = $('.service_status_id').val();
        if (service_statusName != '') {
            $.ajax({
                async: false,
                url: baseUrl + 'service_status/check_service_status_exists',
                type: 'POST',
                data: {service_status_name: service_statusName, service_status_id: service_statusId},
            })
                    .done(function (data) {
                        if (data == '1') {
                            return_val = false
                        }
                    })
                    .fail(function (jqXHR, status_code, error_thrown) {
                        alert(error_thrown);
                    })
        }
        return return_val;
    });
    var companyForm = $("#service_status_form").validate({
        // Rules for form validation
        rules: {
            status_name: {
                required: true,
                service_statusExist: true
            }
        },
        // Messages for form validation
        messages: {
            status_name: {
                required: 'Please enter Service Status Name',
                service_statusExist: 'Service Status Name Already Exist'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

});