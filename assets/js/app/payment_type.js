$(document).ready(function () {
    $.validator.addMethod("already_exists", function (value, element) {
        var return_val = true;
        var type_id = $('#type_id').val();
        var type_name = $('#type_name').val();
        if (type_name != '') {
            $.ajax({
                async: false,
                url: baseUrl + 'payment_type/already_exists',
                type: 'POST',
                data: {type_name: type_name, type_id: type_id},
            })
            .done(function (data) { 
                if (data == '1') {
                    return_val = false
                }
            })
            .fail(function (jqXHR, status_code, error_thrown) {
                alert(error_thrown);
            });
        }
        return return_val;
    });
    
    $("#payment_type").validate({
        // Rules for form validation
        rules: {
            type_name: {
                required: true,
                already_exists: true
            }
        },
        // Messages for form validation
        messages: {
            type_name: {
                required: 'Please enter Payment Type',
                already_exists: 'Payment Type already exists',
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
            if (($('#type_name').val().toLowerCase() == 'cheque') && ($('input[name="is_cheque"]:checked').val() != '1')) {
                if (confirm('Your payment type like cheque. Do you want to update the "Is Check as Yes"?'))
                    $('input[name="is_cheque"][value="1"]').prop('checked', true);
            }
            
            form.submit();
        }
    });
});
