
$(document).ready(function () {

    $('.item_others_txt').hide();
    $('.send_sms_cls').hide();

    $('input[type="file"]').on('change', function(e){
        var url = ''
        ,   $parent = $(this).parent().closest('section');
        if( $(this).val() == ''  ) {
            url = $(this).attr( 'data-default' );
        }else {
            url = URL.createObjectURL( e.target.files[0] );
        }
        
        $parent.find('.preview img').attr( 'src', url );
        $parent.find('.remove-img-btn').remove();
    });
    
    $('#estimated_delivery_date').datepicker().on('changeDate', function (ev) {
        var date_txt = $(this).val();
        var temp_txt = $('#estimated_delivery_date_temp').val();
        $('.send_sms_cls').hide();
        $('.sms_type').prop('checked', false);
        $('#estimated_date_change').val('no');
        if(date_txt != '' && temp_txt != '' && date_txt != temp_txt) {
            $('.send_sms_cls').show();
            $('#estimated_date_change').val('yes');    
        }
    });
   
    var cust_id = $('select.customer_id').val();
    if (cust_id == '0') {
        $('.customer_name_div').show();
    } else {
        $('.customer_name_div').hide();
    }

    delivery_date_div();
    $(document).on('change', '#service_status', function () {
        delivery_date_div();
    });
    
    $(document).on('change', 'select.customer_id', function () {
        cust_name_change();
    });
    
     $('.others_chk_bok').on('click', function(){
         $(this).closest('.others_top').find('.item_others_txt').toggle();
     });
     
     pro_name_change();
     
     $(document).on('change', '#material_name', function () {
        pro_name_change();
     });
     
     $(document).on('change', '.pro_text', function () {
        $('#material_name').val($(this).val());
     });
     
     $(document).on('click', '.remove_pro_txt', function () {
        $('#material_name').val('');
        $('.others_cls').prop('checked', true);
     });
     
     $('.others_chk_bok').each(function(){
         if($(this).is(':checked')) {
            $(this).closest('.others_top').find('.item_others_txt').show();  
         }
     });

    var $orderForm = $("#services_form").validate({
        // Rules for form validation
        invalidHandler: function (event, validator) {
            $('body').animate({
                scrollTop: $(validator.currentForm).offset().top
            }, '500', 'swing');
        },
        rules: {
            customer_id: {
                required: true
            },
            customer_name: {
                required: true
            },
            engineer_id: {
                required: true
            },
            estimated_price_quoted: {
                required: true
            },
            estimated_delivery_date: {
                required: true
            },
            sms_type: {
                required: true
            },
            received_date: {
                required: true
            }
        },
        // Messages for form validation
        messages: {
            customer_id: {
                required: 'Please select company name'
            },
            customer_name: {
                required: 'Please enter company name'
            },
            engineer_id: {
                required: 'Please select engineer name'
            },
            estimated_price_quoted: {
                required: 'Please enter estimated price quoted'
            },
            estimated_delivery_date: {
                required: 'Please select estimated delivery date'
            },
            sms_type: {
                required: 'Please Choose any one'
            },
            received_date: {
                required: 'Please select received date'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});

function cust_name_change(){
    var temp = '';
    $('.customer_name').val(temp);
    $('.customer_name_txt').val(temp);
    $('.contact_person').val(temp);
    $('.contact_number').val(temp);
    $('.email_address').val(temp);
    var cust_id = $('select.customer_id').val();
    if (cust_id == '0') {
        $('.customer_name_div').show();
    } else {
        $('.customer_name_div').hide();
        if(cust_id != ''){
           get_customer_data();
        }
    }
}

function get_customer_data(){
    var cust_id = $('select.customer_id').val();
    var ajaxURL = baseUrl + 'services/getcustomer_data';
    var ajaxData = {
        cust_id: cust_id
    }
    var data = ajaxRequest(ajaxURL, ajaxData);
    var customer = data.cust;
    if (customer.name != undefined) {
        $('.customer_name_txt').val(customer.name);
        $('.contact_person').val(customer.contact_name);
        $('.contact_number').val(customer.phone_no);
        $('.email_address').val(customer.email);
    }
}

function delivery_date_div(){
    var service_status = $('select.service_status').val();
    if(service_status=='3') {
        $('.delivered_date_div_cls').show();
    } else {
        $('.delivered_date_div_cls').hide();
    }
}

function reset_brand() {
    var ajaxURL = baseUrl + 'item/category_active';
    var ajaxData = {}

    var data = ajaxRequest(ajaxURL, ajaxData, '');
    var _option = '<option value="">Select Brand</option>';
    for (var i in data) {
        _option += '<option value="' + data[i].id + '">' + data[i].cat_name + '</option>';
    }

    $('#cat_name').html(_option);
    $('#cat_name').select2({width: '100%'});
}

function pro_name_change() {
    var mat_txt = $('#material_name').val() , temp_chk=''; 
     $('.pro_text').each(function(){
         var protxt = ($(this).val()!="") ? $(this).val().toLowerCase() : '';
         mat_txt = (mat_txt!="") ? mat_txt.toLowerCase() : '';
         if(protxt == mat_txt && protxt != "") {
             $(this).prop('checked', true);
             temp_chk = '1';
         }
     });     
     if(temp_chk == '') {
         $('.others_cls').prop('checked', true);
     }
}