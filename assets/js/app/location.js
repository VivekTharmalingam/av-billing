$(document).ready(function () {
  
    $.validator.addMethod("locationExist", function (value, element) {
        var return_val = true;
        var locationName = $('#location_name').val();
        var locationId = $('.location_id').val();	
        if (locationName != '') {
            $.ajax({
                async: false,
                url: baseUrl + 'location/check_location_exists',
                type: 'POST',
                data: {location_name: locationName, location_id: locationId},
            })  
                    .done(function (data) { 
                        if (data == '1') {
                            return_val = false
                        }
                    })
                    .fail(function (jqXHR, status_code, error_thrown) {
                        alert(error_thrown);
                    })
        }
        return return_val;
    });
	
	 var locationForm = $("#location_form").validate({
        // Rules for form validation
        rules: {
            location_name: {
                required: true,
				locationExist : true
            },
            city: {
                required: true
            },
            address: {
                required: true,
            },
            phone_no: {
                required: true,
                number: true
            },            
            pincode: {
                required: true
            },
            state: {
                required: true
            },
			country: {
                required: true
            }

        },
        // Messages for form validation
        messages: {
            location_name: {
                required: 'Please enter Location Name',
				locationExist : 'Location Name Already Exist'
            },
            city: {
                required: 'Please enter City',
            },
            address: {
                required: 'Please enter address',
            },
            phone_no: {
                required: 'Please enter phone number',
                number: 'Please enter valid phone',
            },
            pincode: {
                required: 'Please enter Pincode'
            },
            state: {
                required: 'Please enter State'
            },
			country: {
                required: 'Please select Country'
            }

        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

});