var addRowIndex = 0;
$(document).ready(function () {
    $('#so_no').on('change', function () {
        $('.customer_name').html('');
        $('.customer_address').html('');
        $('.ship_to_location').html('');
        $('.ship_to_address').html('');
        $('.invoice_date').html('');
        $('.customer_reference').html('');
		$('.items').find('.so_quantity').val('');
        $('.items').find('.category_id').val('');
        $('.items').find('.quantity').val('');
        $('.items').find('.product_description').val('');
		$('.items').find('.delivered_quantity').html('');
        if ($(this).val() == '') {
            $('#so_details').slideUp();
            $('select.category_id').find('option:not(:first)').remove();
            $('select.category_id').select2('val', '');
            $('select.pdt_id').find('option:not(:first)').remove();
            $('select.pdt_id').select2('val', '');
            $('select.quantity').val('');

            return false;
        }
        var ajaxURL = baseUrl + 'delivery_orders/so_details';
        var ajaxData = {
            so_id: $(this).val()
        }
        var data = ajaxRequest(ajaxURL, ajaxData, '');
        if (data.sales_order.so_type == '1') {
            $('.customer_name').html(data.customer.name);
            $('.customer_address').html(data.customer.address);
            $('.ship_to_location').html(data.shipping_address.location_name);
            $('.ship_to_address').html(data.shipping_address.address);
            $('.invoice_date').html(data.sales_order.so_date);
            var your_reference = (data.sales_order.your_reference != '') ? data.sales_order.your_reference : 'N/A';
            $('.customer_reference').html(your_reference);
            $('#so_details').slideDown();
        } else {
            $('.customer_name').html(data.sales_order.name);
            $('.customer_address').html(data.address.address);
            $('.ship_to_location').html(data.address.branch_name);
            $('.ship_to_address').html(data.address.address);
            $('.invoice_date').html(data.sales_order.so_date);
            var your_reference = (data.sales_order.your_reference != '') ? data.sales_order.your_reference : 'N/A';
            $('.customer_reference').html(your_reference);
            $('#so_details').slideDown();
        }

        var _option = '<option value="">Select Item Code</option>';
        $.each(data.so_items, function (key, value) {
            _option += '<option value="' + value.item_id + '" data-desc="'+value.product_description + '"  data-cat_id="' + value.cid + '" data-so_qty="' + value.quantity + '" data-delivered_qty="' + value.delivered_qty + '">' + value.pdt_code + '</option>';
        });

        $('select.pdt_id').html(_option);
		$('.items:not(:first)').remove();
		$('.items').find('select.pdt_id').select2({width: '100%'});
		$('.items').find('.remove-row').removeClass('remove-row fa-minus-circle').addClass('add-row fa-plus-circle');
		if ($('.items').find('.add-row').length > 1) {
			$('.items').find('.add-row:not(:first)').remove();
		}
    });

    /*$('select.category_id').on('change', function () {
        if ($('#so_no').val() == '') {
            $("#delivery_order").validate();
            return '';
        }

        var ajaxURL = baseUrl + 'delivery_orders/so_item_details';
        var ajaxData = {
            so_id: $('#so_no').val(),
            category_id: $(this).val()
        }

        var data = ajaxRequest(ajaxURL, ajaxData, '');
        var _option = '<option value="">Select Item Description</option>';
        $.each(data.so_items, function (key, value) {
            _option += '<option value="' + value.item_id + '" data-so_qty="' + value.quantity + '" data-delivered_qty="' + value.delivered_qty + '">' + value.pdt_code + ' / ' + value.pdt_name + '</option>';
        });

        $(this).closest('.items').find('select.product_id').html(_option);
    });*/

    $(document).on('change', 'select.pdt_id', function () {
        if ($(this).val() == '') {
			$(this).closest('.items').find('.delivered_quantity').html('');
            $("#delivery_order").validate();
            return '';
        }
		
        var _option = $(this).find('option:selected');
        var so_qty = _option.data('so_qty');
        //var so_qty = eval(_option.data('so_qty') - _option.data('delivered_qty'));
        var desc = _option.data('desc');
		if (_option.data('delivered_qty') > 0) {
			$(this).closest('.items').find('.delivered_quantity').html('Delivered quantity <span class="do_qty">' + _option.data('delivered_qty') + '</span>');
		}
		else {
			$(this).closest('.items').find('.delivered_quantity').html('');
		}
		
        //var textAreas = $('textarea');
//        Array.prototype.forEach.call(desc, function(elem) {
//            desc = elem.replace(/\\n/g, '\n');
//        });
        $(this).closest('.items').find('.so_quantity').val(so_qty);
        $(this).closest('.items').find('.category_id').val(_option.data('cat_id'));
        $(this).closest('.items').find('.quantity').val(so_qty);
        $(this).closest('.items').find('.product_description').val(desc);
		//$(this).closest('.items').find('.quantity').validate();
    });

    /* Add row functionality for material */
    $(document).on('click', '.items .add-row', function () {
        addRowIndex++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.items').clone();
		itemObj.find('.delivered_quantity').html('');
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');
        itemObj.find('.vat_percentage').attr('checked', false);

        itemObj.find('input, select, textarea, label .discount_type').each(function (index, element) {
            if ($(element).hasClass('item_hidden_ids')) {
                $(element).remove();
            }
            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndex + ']');
            }
            $(element).val('');
            if ($(element).prop('tagName').toLowerCase() == 'select') {
                if ($(element).prev().hasClass('select2')) {
                    $(element).prev().remove();
                    $(element).select2({width: '100%'});
                }
            }

            if ($(element).hasClass('discount_type')) {
                var id = $(element).prop('id').split('-')[0];
                var num_row = $(element).prop('id').split('-')[1];
                ++num_row;
                $(element).prop('id', id + '-' + (num_row));
                $(element).prop('class', 'discount_type').next().attr('for', id + '-' + (num_row));
                if ($(element).prop('name') != '') {
                    $(element).addClass('onoffswitch-checkbox');
                    $(element).addClass('vat_percentage');
                }
            }
        });

        var materialContainer = $(this).parents('.material-container');
        var optionsLength = $(materialContainer).find('select.product_id:first option:not([value=""]):not(:disabled)').length;
        if ($(this).parent().find('.remove-row').length)
            $(this).remove();
        else
            $(this).removeClass('add-row fa-plus-circle').addClass('remove-row fa-minus-circle');

        if ($(itemObj).find('.remove-row').length == 0)
            $(itemObj).find('.add-row').after('<i class="fa fa-2x fa-minus-circle remove-row"></i>');

        $(materialContainer).find('.items:last').after(itemObj);
    });

    /* Remove row functionality for material */
    $(document).on('click', '.items .remove-row', function () {
        var materialContainer = $(this).parents('.material-container');
        $(this).parents('.items').remove();
        if (materialContainer.find('.items:last').find('.add-row').length == 0) {
            materialContainer.find('.items:last').find('.remove-row').before('<i class="fa fa-2x fa-plus-circle add-row"></i>');
        }

        if ((materialContainer.find('.items').length == 1) && (materialContainer.find('.items:last').find('.add-row').length)) {
            materialContainer.find('.items:last').find('.remove-row').remove();
        }
    });

    $.validator.addMethod('mat_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please select Item Name!');

    $.validator.addMethod('mat_already_exist', function (value, element) {
        var prevItems = $(element).parents('.items').prevAll('.items');
        return (prevItems.find('select.pdt_id option[value="' + value + '"]:selected').length) ? false : true;
    }, 'Item Name is already Exists!');

    $.validator.addMethod('cat_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please select Brand Name!');

    $.validator.addMethod('qty_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please enter Quantity!');

    $.validator.addMethod('valid_quantity', function (value, element) {
        var so_quantity = $(element).closest('.items').find('.so_quantity').val();
		var do_quantity = $(element).closest('.items').find('.do_qty').text();
        so_quantity = (so_quantity != '') ? parseFloat(so_quantity) : 0;
		so_quantity -= (do_quantity != '') ? parseFloat(do_quantity) : 0;
        var quantity = (value != '') ? parseFloat(value) : 0;

        return (so_quantity - quantity >= 0) ? true : false;
    }, 'Quantity should not be greater than SO Qty. Please check delivery quantity!');

    $.validator.addClassRules({
        category_id: {
            cat_required: true
        },
        pdt_id: {
            mat_required: true,
            mat_already_exist: true
        },
        quantity: {
            qty_required: true,
            valid_quantity: true
        }
    });

    $("#delivery_order").validate({
        // Rules for form validation
        invalidHandler: function (event, validator) {
            $('body').animate({
                scrollTop: $(validator.currentForm).offset().top
            },
            '500',
                    'swing'
                    );
        },
        rules: {
            so_no: {
                required: true
            },
            do_date: {
                required: true
            }
        },
        // Messages for form validation
        messages: {
            so_no: {
                required: 'Please select Sales Order number!'
            },
            do_date: {
                required: 'Please select Date!'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
});