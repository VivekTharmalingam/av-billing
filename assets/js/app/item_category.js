$(document).ready(function () {
//    $.validator.addMethod("groupExist", function (value, element) {
//        var return_val = true;
//        var expName = $('#customer_group').val();
//        var expId = $('.group_id').val();
//
//        if (expName != '') {
//            $.ajax({
//                async: false,
//                url: baseUrl + 'customer_group/check_exp_exists',
//                type: 'POST',
//                data: {exp_name: expName, group_id: expId},
//            })
//                    .done(function (data) {
//                        if (data == '1') {
//                            return_val = false
//                        }
//                    })
//                    .fail(function (jqXHR, status_code, error_thrown) {
//                        alert(error_thrown);
//                    })
//        }
//        return return_val;
//    });

    var companyForm = $("#item_cat_form").validate({
        // Rules for form validation
        rules: {
            item_category: {
                required: true
               // groupExist: true
            }
        },
        // Messages for form validation
        messages: {
            item_category: {
                required: 'Please enter Item Category'
                ///groupExist: 'Customer Group already exists',
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

});
