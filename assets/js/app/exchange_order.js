
function calculate_total() {

    var sub_total = 0;
    $('.amount').each(function (index, element) {
        sub_total += ($(element).val() != '') ? parseFloat($(element).val()) : 0;
    });
    $('.sub_total').val(sub_total.toFixed(2));

    var discount = ($('.discount').val() != '') ? parseFloat($('.discount').val()) : 0;
    var totGrn = 0.00;
    var dis = 0.00;
    if ($('.discount_type:checked').val() == '1') { // Discount percentage
        discount = parseFloat((discount / 100) * sub_total);
    }
    $('.discount_amount').html(discount.toFixed(2));
    $('.discount_amt').val(discount.toFixed(2));
    sub_total -= discount;

    var total_amount = sub_total;
    var gst_percentage = 0;
    var gst_amount = 0;
    if($('.supplier_gst').val() != '2') {
        gst_percentage = ($('#hidden_gst').val() != '') ? parseFloat($('#hidden_gst').val()) : 0;
        var gst_amount = parseFloat((gst_percentage / 100) * sub_total);
    }
        
    if ($('.gst_type:checked').val() == '2') {
        total_amount = sub_total + gst_amount;
    }

    $('.gst_amount').val(gst_amount.toFixed(2));
    $('.gst_percentage').html(gst_percentage.toFixed(2));
    $('.total_amt').val(total_amount.toFixed(2));

    $('form[name="exchange_order"]').validate().element('.total_amt');
}

function get_all_supplier(product_code) {
    var ajaxURL = baseUrl + 'exchange_order/search_supplier';
    var ajaxData = {
        supplier_name: ''
    }
    return ajaxRequest(ajaxURL, ajaxData, '');
}


function hide_product_code_msg() {
    $('.product_code_label').slideUp();
}

function hide_error_description_msg() {
    $('.error_description').slideUp();
}


var supplier_names = get_all_supplier(1);
set_supplier_autocomplete();

function set_supplier_autocomplete() {
    $(".supplier_name").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }

        $(this).on('change', function () {
            valid_sub_name($(this));
        });

        $(this).autocomplete({
            source: supplier_names,
            focus: function (event, ui) {
                //$(this).val(ui.item.value);
                //return false;
            },
            select: function (event, ui) {
                $(this).val(ui.item.name.trim());
                $('.supplier_id').val(ui.item.id.trim());
                get_supplier_data();
                reset_supplier_itemcode();
                calculate_total();
                
                return false;
            }
        });
        $(this).addClass('hasAutocomplete');
    });
}

function valid_sub_name(sub_name) {
    var sub_name_exists = false;
    sub_name.css('color', '#d812cf');
    for (var i in supplier_names) {
        if (supplier_names[i].name.trim().toLowerCase() == sub_name.val().trim().toLowerCase()) {
            sub_name_exists = true;
            sub_name.css('color', '');
            $('.supplier_id').val(supplier_names[i].id);
            break;
        }
    }

    if (!sub_name_exists) {
        $('.supplier_id').val('');
        sub_name.css('color', '#d812cf');
//		sub_name.val('');
//		sub_name.focus();
        return false;
    }
}

function calculate_item_total(_this) {
    var itemsCol = $(_this).closest('.items');
    var quantity = (itemsCol.find('.quantity').val() != '') ? parseFloat(itemsCol.find('.quantity').val()) : 0;
    var price = (itemsCol.find('.cost').val() != '') ? parseFloat(itemsCol.find('.cost').val()) : 0;
    var item_discount = (itemsCol.find('.item_discount').val() != '') ? parseFloat(itemsCol.find('.item_discount').val()) : 0;
    var item_total = parseFloat((quantity * price).toFixed(2));
    item_discount = (itemsCol.find('.item_discount_type:checked').length) ? item_total * parseFloat((item_discount / 100).toFixed(2)) : item_discount;
    itemsCol.find('.item_discount_amt').html(item_discount.toFixed(2));
    itemsCol.find('.item_discount_amt').val(item_discount.toFixed(2));
    var amount = (quantity * price) - parseFloat(item_discount.toFixed(2));
    itemsCol.find('.amount').val(amount.toFixed(2));
    
    $('form[name="exchange_order"]').validate().element(itemsCol.find('.amount'));
    
    calculate_total();
}

var addRowIndex = 0;
var active = $('.supplier_name').parents('form').hasClass('add_form') ? 'add' : 'edit';

var scanItem = 0;
var items;
var supplier_names;
var this_item_row = '';

$(document).ready(function () {

    var supplier_names = get_all_supplier(1);
    set_supplier_autocomplete();

    if (active == 'edit') {
        //calculate_total();
    }

    var pr_items = '';

    $(document).on('focusin', ".quantity, .price", function () {
        $(this).select();
    });

    $('.so_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });

    $(document).on('keyup', '.supplier_name', function () {
        if ($(this).val() == '') {
            $('.supplier_id').val('');
        }
    });

    $(document).on('change', '.item_discount_type', function () {
        calculate_item_total($(this));
    });

    $(document).on('keyup', '.quantity, .cost, .item_discount, .quantity1, .cost1', function () {
        calculate_item_total($(this));
    });

    $(document).on('keyup', '.discount, .discount1', function () {
        calculate_total();
    });

    $(document).on('change', '.discount_type, .discount,.discount_type1, .discount1', function () {
        calculate_total();
    });

    $(document).on('change', '.gst_type,.gst_type1', function () {
        calculate_total();
    });

    $(document).on('click', '.item-serialnum-popup', function () {
        open_serial_no_popup($(this));
    });
	
    $(document).on('click', '.serial_no_close, .close', function () {
        set_serial_no_input();
    });

    if($('.supplier_gst').val() != "") {
        get_supplier_data();
        calculate_total();
    }
    
    $(document).on('blur', 'input.quantity', function (e) {
        var item_sl_no = ($(this).closest('.items').find('.item_sl_no').val() != '') ? $(this).closest('.items').find('.item_sl_no').val().split(',') : [];     
        var qnty = ($(this).val() != '') ? parseFloat($(this).val()) : 0;   
        var item_sl_no_txt = '';
        if(item_sl_no.length>qnty) {
            item_sl_no_txt = item_sl_no.slice(0, qnty);
        }
        if((qnty>0 && item_sl_no_txt!='') || (qnty==0 && item_sl_no_txt=='')) {
            $(this).closest('.items').find('.item_sl_no').val(item_sl_no_txt);
        }
    });
    
    $(document).on('blur', '.product_code', function (e) {
        var items = $(this).closest('.items');
        var itm_id = $(this).val();
        get_supplier_itemcode(items,itm_id);
    });

    /* Add row functionality for material */
    $(document).on('click', '.items .add-row', function () {
        addRowIndex++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.items').clone();
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');
        itemObj.find('.brand_name').html('');
        itemObj.find('.brand_label').hide();

        itemObj.find('input, select, textarea').each(function (index, element) {
            if ($(element).hasClass('item_hidden_ids')) {
                $(element).remove();
            }

            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndex + ']');
            }
            $(element).val('');

            if ($(element).hasClass('product_description'))
                $(element).data('value', '');

            if ($(element).hasClass('hasAutocomplete'))
                $(element).removeClass('hasAutocomplete')

        });

        itemObj.find('.item_discount_type').prop('checked', true);
        itemObj.find('.item_discount_type').val(1);
        itemObj.find('.item_discount_amt').html(0);
        itemObj.find('.item_discount_amt').val(0);

        if (formType == 'edit') {
            itemObj.find('.item_discount_type').prop('id', 'new_item_discount_type-' + addRowIndex);
            itemObj.find('.onoffswitch-label').prop('for', 'new_item_discount_type-' + addRowIndex);
        }
        else {
            itemObj.find('.item_discount_type').prop('id', 'item_discount_type-' + addRowIndex);
            itemObj.find('.onoffswitch-label').prop('for', 'item_discount_type-' + addRowIndex);
        }
        var materialContainer = $(this).parents('.material-container');

        if ($(this).parent().find('.remove-row').length)
            $(this).remove();
        else
            $(this).removeClass('add-row fa-plus-circle').addClass('remove-row fa-minus-circle');

        if ($(itemObj).find('.remove-row').length == 0)
            $(itemObj).find('.add-row').after('<i class="fa fa-2x fa-minus-circle remove-row"></i>');

        $(materialContainer).find('.items:last').after(itemObj);

        $('.items:last .product_code').focus();
    });

    /* Remove row functionality for material */
    $(document).on('click', '.items .remove-row', function () {
        var materialContainer = $(this).parents('.material-container');
        $(this).parents('.items').remove();
        if (materialContainer.find('.items:last').find('.add-row').length == 0) {
            materialContainer.find('.items:last').find('.remove-row').before('<i class="fa fa-2x fa-plus-circle add-row"></i>');
        }

        if ((materialContainer.find('.items').length == 1) && (materialContainer.find('.items:last').find('.add-row').length)) {
            materialContainer.find('.items:last').find('.remove-row').remove();
        }
        calculate_total();
        $('.items:last .product_code').focus();
    });


    $.validator.addMethod('product_code_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please enter Item Code!');

    $.validator.addMethod('qty_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please enter Quantity!');
    
    $.validator.addMethod('valid_amount', function (value, element) {
        return (parseFloat(value) >= 0) ? true : false;
    }, 'Invalid&nbsp;amount!');

    $.validator.addClassRules({
        product_code: {
            product_code_required: true
        },
        quantity: {
            qty_required: true
        },
        amount: {
            valid_amount: true
        }
    });

    var $orderForm = $("#exchange_order_form").validate({
        // Rules for form validation
        invalidHandler: function (event, validator) {
            $('body').animate({
                scrollTop: $(validator.currentForm).offset().top
            },
            '500',
                    'swing'
                    );
        },
        rules: {
            supplier_name: {
                required: true
            },
            exr_date: {
                required: true
            },
            total_amt: {
                valid_amount: true
            }
        },
        // Messages for form validation
        messages: {
            supplier_name: {
                required: 'Please enter supplier name'
            },
            exr_date: {
                required: 'Please select Date'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
            if (scanItem) {
                scanItem = 0;
                return false;
            }

            form.submit();
        }
    });
});



function open_serial_no_popup($_this) {
    
    var open_popup = false;
    /* Set Serial No. */
    var qty_count = ($_this.closest('.items').find('.quantity').val() != '') ? parseFloat($_this.closest('.items').find('.quantity').val()) : 0;    
    var product_code = $_this.closest('.items').find('.product_code').val();     
    var item_sl_no = ($_this.closest('.items').find('.item_sl_no').val() != '') ? $_this.closest('.items').find('.item_sl_no').val().split(',') : [];     
    var html = '', temp='';
    this_item_row = '';
    if((qty_count > 0) && product_code!='') {
        open_popup = true;
        this_item_row = $_this;
        $('.serial_no_title').html('Add Serial No. for '+product_code);
        for (var i=0; i<qty_count; i++) {
                    var ths_item_sl_no = (item_sl_no[i] != undefined) ? item_sl_no[i] : "";
                    var inc_i = i+1;
                    html += '<div class="col-md-12 itemsernos">';
                    
                    html += '<div class="col-md-1">';
                    html += '<label class="item_id">' + inc_i + '</label>';
                    html += '</div>';

                    html += '<div class="col-md-11">';
                    html += '<label class="input" style="width:100%;"><input type="text" name="ser_no_txt[]" class="ser_no_txt form-control" onkeypress="return isRmvComma(event)" value="'+ths_item_sl_no+'"/></label>';
                    html += '</div>';
                    
                    html += '</div>';
                
        }
        $('.item_ser_nos').html(html);
    }
    /* End */

    if (open_popup) {
        $("#grn_serial_no").modal({
            backdrop: "static",
            keyboard: true,
            show: true
        });
    }
}

function set_serial_no_input() {
    var ser_no_txtall = '';
    if(this_item_row != '') {
        $('.ser_no_txt').each(function(){
            if($(this).val().trim() != "") {
                ser_no_txtall += $(this).val().trim()+',';
            }
        });
        ser_no_txtall = (ser_no_txtall != '') ? ser_no_txtall.replace(/^,|,$/g,'') : '';
        this_item_row.closest('.items').find('.item_sl_no').val(ser_no_txtall);    
    }
}

function isRmvComma(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 44) {
        return false;
    }
    return true;
}

function get_supplier_itemcode(ItmRow,ItmId) {
        var supp_id = $('.supplier_id').val(), temp='';
        if(supp_id != "" && ItmId != '') {
            var ajaxURL = baseUrl + 'purchase_orders/get_supplier_exc_itemcode';
            var ajaxData = {
                item_id: ItmId,
                supp_id: supp_id
            }
            var data = ajaxRequest(ajaxURL, ajaxData, '');

            if (data.itemcode.sl_itm_code != '')
                ItmRow.find('.sl_itm_code').val(data.itemcode.sl_itm_code);
            else
                ItmRow.find('.sl_itm_code').val(temp);
        } else {
            ItmRow.find('.sl_itm_code').val(temp);  
        }
}

function reset_supplier_itemcode() {
        $('.sl_itm_code').each(function(){
            var items = $(this).closest('.items');
            var itm_id = items.find('.product_code').val();
            get_supplier_itemcode(items,itm_id);
        });
}

function get_supplier_data() {
    var supp_id = $('.supplier_id').val();
    if(supp_id != "") {
        var ajaxURL = baseUrl + 'goods_receive/get_supplier_data';
        var ajaxData = {
            supp_id: supp_id
        }
        var data = ajaxRequest(ajaxURL, ajaxData, '');

        if (data.supplier.gst_type == '2') {
            $('.supplier_gst').val('2');
            $('.without_gst').prop('checked', true);
            $('.gst_details').hide();
        } else {
            $('.supplier_gst').val('1');
            $('.with_gst').prop('checked', true);
            $('.gst_details').show();
            
        }
    }
    
}