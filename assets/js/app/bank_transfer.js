$(document).ready(function () {
    
    get_frm_crnt_bln('no');
    get_to_crnt_bln();
    
    $('#payment_type').on('change', function() {
        $('.cheque_details').slideUp();
        if ($(this).find('option:selected').data('is_cheque') == '1') {
            $('.cheque_details').slideDown();
            $('input[name="cheque_bank_name"]').focus();
        }
    });
    
    $('.ded_percentage').keyup(function(){
        var perct = ($(this).val() != "") ? parseFloat($(this).val()) : 0;
        var temp = '';
        if(perct > 100){
            $(this).val(temp);
        }
    });
    
    var IsChk = $('#payment_type').find('option:selected').data('is_cheque');
    if(IsChk=='1') {
        $('.cheque_details').slideDown();
        $('input[name="cheque_bank_name"]').focus();
    }
    
    $('#frm_account_id').on('change', function() {
        get_frm_crnt_bln('yes');
    });
    
    $('#to_account_id').on('change', function() {
        get_to_crnt_bln();
    });
    
    $.validator.addMethod('amountValidate', function (value, element) {
        var amt = parseFloat(value) || 0;
        var frm_amt = parseFloat($('b.frm_crnt_bln').text()) || 0;
        return (amt <= frm_amt) ? true : false;
    }, 'Insufficient Amount in From Account!');
    
    $.validator.addMethod('tobankValidate', function (value, element) {
        var tobank = value;
        var frmbank = $('#frm_account_id option:selected').val();
        return (frmbank != '' && tobank != '' && tobank != frmbank) ? true : false;
    }, 'Credit Account No. should not be same as Debit Account No.!');

    $.validator.addClassRules({
        trans_amount: {
            amountValidate: true
        },
        to_account_id: {
           tobankValidate: true 
        }
    });
    
    $(document).on('change', 'select.select2, #ded_date', function() {
        $('form[name="transfer_form"]').validate().element(this);
    });
    
    var companyForm = $("#transfer_form").validate({
        // Rules for form validation
        rules: {
            frm_account_id: {
                required: true
            },
            to_account_id: {
                required: true
            },
            trans_amount: {
                required: true
            },
            payment_type: {
                required: true
            },
            cheque_bank_name : {
                    required : true
            },
            cheque_acc_no : {
                    required : true
            },
            cheque_date : {
                    required : true
            }
        },
        // Messages for form validation
        messages: {
            frm_account_id: {
                required: 'Please select Debit Account No.'
            },
            to_account_id: {
                required: 'Please select Credit Account No.'
            },
            trans_amount: {
                required: 'Please enter Amount'
            },
            payment_type: {
                required: 'Please select Payment Type'
            },
            cheque_bank_name : {
                    required : 'Please enter Cheque Bamk Name'
            },
            cheque_acc_no : {
                    required : 'Please enter Cheque Account No'
            },
            cheque_date : {
                    required : 'Please enter Cheque Date'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    }); 

});

function get_frm_crnt_bln(typ){
    var amount = $('#frm_account_id').find('option:selected').data('frmcrntbln');
    var amt_txt = ($('#frm_account_id').val() != "") ? 'Current Balance : <b style="color: #3276B1;" class="frm_crnt_bln">'+amount+'</b>' : '';
    $('.frm_crnt_bln').html(amt_txt);
    if(typ=='yes') {
        var bnktext = $('#frm_account_id').find('option:selected').data('bnktext');
        $('.cheque_bank_name').val(bnktext);
    }
}

function get_to_crnt_bln(){
    var amount = $('#to_account_id').find('option:selected').data('tocrntbln');
    var amt_txt = ($('#to_account_id').val() != "") ? 'Current Balance : <b style="color: #3276B1;" class="to_crnt_bln">'+amount+'</b>' : '';
    $('.to_crnt_bln').html(amt_txt);
}