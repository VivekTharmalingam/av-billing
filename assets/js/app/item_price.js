function item_show_loader() {
	$('.loader').show();
	$('.actions').hide();
	$('.category').slideUp();
}

function item_hide_loader() {
	$('.loader').hide();
	$('.actions').show();
	$('.category').slideDown();
}

function get_item_done(data) {
	if (data.length != '') {
		htmlRow = make_item_rows(data);
	}
	
	if (htmlRow == '') {
		htmlRow = '<p style="text-align: center;margin: 10px 0px;font-weight: bold;">NO CATEGORY ITEMS FOUND!!</p>'
	}
	
	$('.items').html(htmlRow);
	$('.items select.brand, .items select.category, .items select.sub_category').each(function() {
		$(this).select2({width: '100%'});
	});
	$('.itemsFrame').slideDown();
}

function get_item_fail(data) {
	
}
 $('.category').on('change', function () {
     var cat = $(this).val();
        var sub_cat = '';
        $('.sub_category').select2('destroy');
        category_wise_subcategory(cat, sub_cat);
 });
        
    
    
function category_wise_subcategory(catId, sub_cat) {
    var ajaxURL = baseUrl + 'item/get_subcat_by_category';
    var ajaxData = {category: catId}

    var data = ajaxRequest(ajaxURL, ajaxData, '');
    var _option = '<option value="">Select Sub Category</option>';
    for (var i in data) {
        var select_txt = (sub_cat == data[i].id) ? "selected" : "";
        _option += '<option value="' + data[i].id + '" '+select_txt+'>' + data[i].sub_category_name + '</option>';
    }
    
    $('.sub_category').html(_option);
    $('.sub_category').select2({width: '100%'});
}
$(document).ready(function () {
    $('#get_items').on('click', function () {
		var brand = $('#brand option:selected').val();
		var page_no = $('#page_no option:selected').val();
		var cat = $('#item_cat option:selected').val();
		var all_items = $('input[name="check_all"]:checked').length;
                
		if ((all_items == 0) && (cat == '') && (brand == '')) {
			$('.category').slideUp();
			$('.itemsFrame').slideUp();
			alert('Please select Brand or Item category');
			return false;
		}
                if(page_no==''){
                    alert("Select page number");
                    return false;
                }
		
        var title = $('#item_cat option:selected').attr('data-title');
        $('#category_name').html(title);
        $('#cat_sign').val('+');
        $('#cat_val').val('0.00');
        $('.items').html('');
		
        var url = baseUrl + 'update_item/get_category_items';
        var data = {
            brand: brand,
            page_no: page_no,
            cat_id: cat,
			all_items: all_items
        }
		
		var _option = {
			loader_show : item_show_loader,
			loader_hide : item_hide_loader,
			call_back_done : get_item_done,
			call_back_fail : get_item_fail
		};
		asyncAjaxRequest(url, data, _option);        
    });
	$('input[name="check_all"]').click(function () {
        if ($(this).prop("checked") == true) {
            var pagecount_values = '<option value="">Select</option>';
            $('#page_no').html(pagecount_values);
            $('#item_cat').attr('disabled', true);
            $('#brand').attr('disabled', true);
            $.ajax({
                url: baseUrl+ 'update_item/get_record_page_count' ,
                data: {cmd: "all_pages"},
                dataType: 'JSON',
                type: 'post',
                success: function (output) {
                    var pagenumbers = [];
                    for (var i = 1; i <= output.page_count; i++) {
                        var pagcount_values = "<option value='" + i + "'>" + i + "</option>";
                        $('#page_no').append(pagcount_values);
                        pagenumbers.push(i);
                    }
                    var pagevalues = pagenumbers.join();
                    $('#pagecountnunbers').val(pagevalues);
                }
            });
        }
        else if ($(this).prop("checked") == false) {
            $('#item_cat').removeAttr('disabled');
            $('#brand').removeAttr('disabled');
            var pagecount_values = '<option value="">All</option>';
            $('#page_no').html(pagecount_values);
        }
    });

    $('#brand').on('change', function () {
		var pagecount_values = '<option value="">Select</option>';
		$('#page_no').html(pagecount_values);
        var brand = ($(this).val());
        var item_cat = $('#item_cat').val();
        $.ajax({
            url: baseUrl+'update_item/get_record_page_count' ,
            data: {cmd: "category_pages", brand: brand, item_cat: item_cat},
            dataType: 'JSON',
            type: 'post',
            success: function (output) {
                if (output.counts == "yes") {
                    var pagenumbers = [];
                    $('#pagecountnunbers').val(output.page_count);
                    for (var i = 1; i <= output.page_count; i++) {
                        var pagcount_values = "<option value='" + i + "'>" + i + "</option>";
                        $('#page_no').append(pagcount_values);
                        pagenumbers.push(i);
                    }
                    var pagevalues = pagenumbers.join();
                    $('#pagecountnunbers').val(pagevalues);
                } else {
                    if (output.page_count != 0) {
                        $('#pagecountnunbers').val(1);
                        var pagcount_values = "<option value='1'>1</option>";
                        $('#page_no').append(pagcount_values);
                    }
                }
            }
        });
    });
    $('#item_cat').on('change', function () {
        var item_cat = $(this).val();
        var brand = $('#brand').val();
        var pagecount_values = '<option value="">Select</option>';
            $('#page_no').html(pagecount_values);
        $.ajax({
            url: baseUrl+'update_item/get_record_page_count' ,
            data: {cmd: "category_pages", item_cat: item_cat, brand: brand},
            dataType: 'JSON',
            type: 'post',
            success: function (output) {
                if (output.counts == "yes") {
                    var pagenumbers = [];
                    $('#pagecountnunbers').val(output.page_count);
                    for (var i = 1; i <= output.page_count; i++) {
                        var pagcount_values = "<option value='" + i + "'>" + i + "</option>";
                        $('#page_no').append(pagcount_values);
                        pagenumbers.push(i);
                    }
                    var pagevalues = pagenumbers.join();
                    $('#pagecountnunbers').val(pagevalues);
                } else {
                    if (output.page_count != 0) {
                        $('#pagecountnunbers').val(1);
                        var pagcount_values = "<option value='1'>1</option>";
                        $('#page_no').append(pagcount_values);
                    }
                }
            }
        });
    });
    $(document).on('click', '.remove-row', function () {
        $(this).parents('.item_details').remove();
//        $(this).parents('.item_details').slideUp("slow", function () {
//            $(this).parents('.item_details').remove();
//        });
        $('.item_details:last').focus();
    });

    $(document).on('change', '.cat_val, .cat_sign', function (e) {
        var cat_val = $('.cat_val').val();
        var cat_sign = $('.cat_sign').val();
        var cost, selling_price, sp;
        $('.item_details').each(function () {
            cost = ($(this).find('.item_cost').val() != '') ? $(this).find('.item_cost').val() : '0.00';
            $('.item_value').val(parseFloat(cat_val).toFixed(2));
            selling_price = calc(cat_sign,cost,cat_val);
            sp = (parseFloat(selling_price).toFixed(2) > 0) ? parseFloat(selling_price).toFixed(2) : '0.00';
            $(this).find('.item_sign').val(cat_sign);
            $(this).find('.item_selling_price').val(sp);
        });        
    });
    
    $(document).on('change', '.item_cost, .item_sign', function (e) {
        var selling_price;
        var itemCost  =  $(this).parents('.item_details').find('.item_cost').val();
        var itemSign  =  $(this).parents('.item_details').find('.item_sign').val();
        var itemValue =  $(this).parents('.item_details').find('.item_value').val();
        selling_price = calc(itemSign,itemCost,itemValue);
        sp = (parseFloat(selling_price).toFixed(2) > 0) ? parseFloat(selling_price).toFixed(2) : '0.00';
        $(this).parents('.item_details').find('.item_selling_price').val(sp);  
        
    });
	
	$(document).on('keyup', '.item_value', function (e) {
        var selling_price;
        var itemCost  =  $(this).parents('.item_details').find('.item_cost').val();
        var itemSign  =  $(this).parents('.item_details').find('.item_sign').val();
        var itemValue =  $(this).parents('.item_details').find('.item_value').val();
        selling_price = calc(itemSign,itemCost,itemValue);
        sp = (parseFloat(selling_price).toFixed(2) > 0) ? parseFloat(selling_price).toFixed(2) : '0.00';
        $(this).parents('.item_details').find('.item_selling_price').val(sp);        
    });
    $(document).on('change', '.itemcategory', function (e) {
       var category_value=$(this).val();
        var sub_cat = '';
       
        category_wise_subcategory(category_value, sub_cat, $(this).closest('.item_details'));
    });
});
function category_wise_subcategory(catId, sub_cat,selectargument) {
    var ajaxURL = baseUrl + 'item/get_subcat_by_category';
    var ajaxData = {category: catId}

    var data = ajaxRequest(ajaxURL, ajaxData, '');
    var _option = '<option value="">Select Sub Category</option>';
    for (var i in data) {
        var select_txt = (sub_cat == data[i].id) ? "selected" : "";
        _option += '<option value="' + data[i].id + '" '+select_txt+'>' + data[i].sub_category_name + '</option>';
    }
        selectargument.find('select.sub_category').html(_option);
        selectargument.find('select.sub_category').select2({width:'100%'});
//    $('#sub_cat').html(_option);
//    $('#sub_cat').select2({width: '100%'});
}
function calc(sign,cost,val) {
	var selling_price = 0;
    switch (sign) {
        case '+':
            selling_price = parseFloat(cost) + parseFloat(val);
            break;
        case '-':
            selling_price = parseFloat(cost) - parseFloat(val);
            break;
        case '*':
            selling_price = parseFloat(cost) * parseFloat(val);
            break;
        case '/':
            selling_price = parseFloat(cost) / parseFloat(val);
            break;
        default:
            selling_price = parseFloat(cost);
    }
    return selling_price;
}
function make_item_rows(data) {
    html = '';
    var buy, selling_price;
    $.each(data.items, function (key, item) {
        
        buy = '0.00';
        if (item.buying_price != '') {
            buy = item.buying_price;
        }
        selling_price = '0.00';
        if (item.selling_price != '') {
            selling_price = item.selling_price;
        }
        html += '<div class="row item_details">';
		html += '<section class="col col-2">';
        html += '<label class="input">';
		html += '<select name="brand[]" class="form-control select2 brand">';
		html += '<option value="">Select Brand</option>';
		for(var i in brands){
			var row = brands[i];
			html += '<option value="' + row.id + '"';
			html += (item.cat_id == row.id) ? ' selected' : '';
			html += '>' + row.cat_name + '</option>';
		}
		html += '</select>';
        html += '</label>';
        html += '</section>';
		html += '<section class="col col-2">';
        html += '<label class="input">';
		html += '<select name="category[]" class="form-control select2 category itemcategory">';
		html += '<option value="">Select Category</option>';
		for(var i in item_categories){
			var row = item_categories[i];
			html += '<option value="' + row.id + '"';
			html += (item.item_category == row.id) ? ' selected' : '';
			html += '>' + row.item_category_name + '</option>';
		}
		html += '</select>';
        html += '</label>';
        html += '</section>';
		html += '<section class="col col-2">';
        html += '<label class="input">';
		html += '<select name="sub_category[]" class="form-control select2 sub_category" id="sub_category">';
		html += '<option value="">Select Sub Category</option>';
		for(var i in sub_categories){
			var row = sub_categories[i];
			html += '<option value="' + row.id + '" data-item_category="' + row.category_id + '"';
			html += (item.sub_category == row.id) ? ' selected' : '';
			html += '>' + row.sub_category_name + '</option>';
		}
		html += '</select>';
        html += '</label>';
        html += '</section>';
		
        html += '<section class="col col-2">';
        html += '<label class="input">';
		//html += '<i class="icon-append fa fa-archive"></i>';
        //html += '<input type="text" name="item_name[]" id ="item_name" class="lowercase item_name" value="';
		html += item.pdt_name;
		//html += '" maxlength="150" placeholder="Item Name"/>';
        html += '<input type ="hidden" name="item_id[]" id="item_id" class="item_id" value="' + item.pid + '" >';
        html += '</label>';
        html += '</section>';
		
        html += '<section class="col col-1">';
        html += '<label class="input" >';
		//html += '<i class="icon-append fa fa-dollar"></i>';
        html += '<input type="text" name="item_cost[]" id="item_cost" class="lowercase item_cost float_value" placeholder="0.00" value="' + buy + '" style="padding: 5px;" />';
        html += '</label>';
        html += '</section>';
        
		html += '<section class="col col-1" >';
        html += '<label class="select">';
		html += '<i class="icon-append"></i>';
		html += '<select class="form-control item_sign" name="item_sign[]">';
		html += '<option value="+">+</option>';
		html += '<option value="-">-</option>';
		html += '<option value="*">*</option>';
		html += '<option value="/">/</option>';
		html += '</select>';
        html += '</label>';
        html += '</section>';
		
        html += '<section class="col col-1">';
        html += '<label class="input">';
		//html += '<i class="icon-append fa fa-dollar" style="padding: 0px; width: 10px;"></i>';
        html += '<input type="text" name="item_value[]" id="item_value" class="lowercase item_value float_value" placeholder="0.00" value="0.00" style="padding: 10px;" />';
        html += '</label>';
        html += '</section>';
        
		html += '<section class="col col-1">';
        html += '<label class="input" >';
		//html += '<i class="icon-append fa fa-dollar"></i>';
        html += '<input type="text" name="item_selling_price[]" id="item_selling_price" class="lowercase item_selling_price float_value" value="' + selling_price + '" placeholder="0.00" style="padding: 5px;" />';
        html += '</label>';
		html += '<i class="fa fa-2x fa-minus-circle remove-row"></i>';
        html += '</section>';
        
        html += '</div>';
    });
    return html;
}

 