function total_paid_amount() {
    var paid_amount = 0;
    $('.exr_noId:checked').each(function () {
        var item = $(this).parents('.items');
        paid_amount += (item.find('.paid_amount').val() != '') ? parseFloat(item.find('.paid_amount').val()) : 0;
    });
    $('.total_amount').html(paid_amount.toFixed(2));
}

var addRowIndex = 1;
$(document).ready(function () {
    $('#payment_type').on('change', function () {
        if ($(this).find('option:selected').data('is_cheque') == '1') {
            $('.cheque_details').slideDown();
            $('input[name="cheque_bank_name"]').focus();
        }
        else
            $('.cheque_details').slideUp();

        $('select[name="payment_type"]').valid();
    });

    $('#supplier').on('change', function () {
        $('#check_all').prop('checked', false);
        $('.total_amount').html('0.00');
        $('.items').remove();
        if ($(this).val() == '') {
            $('.loader').hide();
            $('.total_amt_row').hide();
            $('.no-items').show();
            return false;
        }

        $('.no-items').hide();
        $('.loader').show();
        var ajaxURL = baseUrl + 'exchange_order_payments/unpaid_purchases';
        $('.loader').hide();
        var ajaxData = {
            supplier_id: $(this).val()
        }

        var data = ajaxRequest(ajaxURL, ajaxData, '');
        var html = '';
        if (data.exc_order.length) {
            for (var i in data.exc_order) {
                var exor = data.exc_order[i];
                html += '<div class="items col-xs-12 clr">';
                html += '<section class="col col-1 no-padding no-margin" style="padding-left: 3px !important;">';
                html += '<label class="checkbox">';
                html += '<input type="checkbox" name="exr_noId[' + addRowIndex + ']" class="exr_noId" value="' + exor.id + '" />';
                html += '<i></i>';
                html += '</label>';
                html += '</section>';

                html += '<section class="col col-3 no-margin">';
                html += '<label class="label exor_no" style="font-weight: normal !important;">' + exor.exr_code + '</label>';
                html += '</section>';

                html += '<section class="col col-2 no-margin">';
                html += '<label class="label exor_date" style="font-weight: normal !important;">' + exor.exr_date_str + '</label>';
                html += '</section>';

                html += '<section class="col col-3 no-margin">';
                html += '<label class="input">';
                html += '<input type="text" name="amount[' + addRowIndex + ']" class="float_value amount exor_amount" value="' + exor.exr_net_amount + '" readonly />';
                html += '</label>';
                html += '<label class="label" style="color: #2e3192; font-size: 11px;';
                if (exor.paid_amount > 0)
                    html += 'display: block;';
                else
                    html += 'display: none;';

                html += '">Paid Amount: <span class="already_paid_text">' + exor.paid_amount + '</span></label>';
                html += '<input type="hidden" name="already_paid[' + addRowIndex + ']" class="float_value amount already_paid" value="' + exor.paid_amount + '" />';
                html += '</section>';

                html += '<section class="col col-3 no-margin">';
                html += '<label class="input">';
                html += '<input type="text" name="paid_amount[' + addRowIndex + ']" class="float_value amount paid_amount" value="0.00" readonly />';
                html += '</label>';
                html += '</section>';
                html += '</div>';
                addRowIndex++;
            }
            $('.total_amt_row').show();
        }
        else {
            html += '<div class="items col-xs-12 clr">';
            html += '<p class="text-center">No Invoices Found!</p>';
            html += '</div>';
            $('.total_amt_row').hide();
        }
        $('#exc_order').append(html);

    });

    $('#check_all').on('change', function () {
        if ($(this).prop('checked') === true)
            $('.exr_noId').prop('checked', true);
        else
            $('.exr_noId').prop('checked', false);

        $('.exr_noId').trigger('change');
        $('input[name="check_all"]').valid();
    });

    $(document).on('change', '.exr_noId', function () {
        var item = $(this).parents('.items');
        if (($(this).prop('checked') === true) && (!$(item).hasClass('credit_notes'))) {
            var pay_amount = parseFloat($(item).find('.exor_amount').val()) - parseFloat($(item).find('.already_paid').val());
            $(item).find('.paid_amount').val(pay_amount.toFixed(2));
        }
        else
            $(item).find('.paid_amount').val('0.00');

        total_paid_amount();
        $('input[name="check_all"]').valid();
    });

    $(document).on('keyup', '.paid_amount', function () {
        total_paid_amount();
    });

    $.validator.addMethod('exr_noId_required', function (value, element) {
        var return_val = true;
        if ($(element).prop('checked') !== true) {
            var paid_amount = $(element).parents('.items').find('.paid_amount').val();
            paid_amount = (paid_amount != '') ? parseFloat(paid_amount) : 0;
            return_val = (paid_amount > 0) ? false : true;
        }
        return return_val;
    }, 'Please enter Pay Amount!');

    $.validator.addClassRules({
        exr_noId: {
            exr_noId_required: true
        }
    });

    $.validator.addMethod('min_invoice', function (value, element) {
        return ($('.exr_noId:checked').length) ? true : false;
    });

    $("#exchange_order_payment_form").validate({
        // Rules for form validation
        rules: {
            /*amnt_to_pay: {
             required: true
             },*/
            pay_date: {
                required: true
            },
            payment_type: {
                required: true
            },
            cheque_bank_name: {
                required: true
            },
            cheque_acc_no: {
                required: true
            },
            cheque_date: {
                required: true
            },
            check_all: {
                min_invoice: true
            }
        },
        // Messages for form validation
        messages: {
            /*amnt_to_pay: {
             required: 'Please enter Pay Amount'
             },*/
            pay_date: {
                required: 'Please select Pay Date!'
            },
            payment_type: {
                required: 'Please select Payment Type!'
            },
            cheque_bank_name: {
                required: 'Please enter Cheque Bamk Name!'
            },
            cheque_acc_no: {
                required: 'Please enter Cheque Account No!'
            },
            cheque_date: {
                required: 'Please enter Cheque Date!'
            },
            check_all: {
                min_invoice: 'Please select atlease one Exchange Order to pay!'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
});

function cheque_fields() {
    if ($('#payment_type').val() == '2') {
        $('.cheque_details').show();
    }
    else {
        $('.cheque_details').hide();
    }
}

function bal_amt() {
    $('.bal_amount').css('display', 'block');
}

function dd_fields() {
    if ($('#payment_type').val() == '3') {
        $('.dd_details').show();
    }
    else {
        $('.dd_details').hide();
    }
}
