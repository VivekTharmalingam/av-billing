$(document).ready(function () {	
	
    var companyForm = $("#expense_form").validate({
        // Rules for form validation
        rules: {
            
            expense_date: {
                required: true,
            },
            exp_type: {
                required: true
            },
            pay_to: {
                required: true
            },
			amt: {
				required: true
			}

        },
        // Messages for form validation
        messages: {
            expense_date: {
                required: 'Please choose Expense Date',
            },
            exp_type: {
                required: 'Please select Expense Type',
            },
            pay_to: {
                required: 'Please enter Pay To'
            },
			amt: {
				required: 'Please enter Amount'
			}
			
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

});