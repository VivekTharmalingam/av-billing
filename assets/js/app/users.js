
$(document).ready(function() { 
    
      $('input[type="file"]').on('change', function(e){
        var url = ''
        ,   $parent = $(this).parent().closest('section');
        if( $(this).val() == ''  ) {
            url = $(this).attr( 'data-default' );
        }else {
            url = URL.createObjectURL( e.target.files[0] );
        }
        
        $parent.find('.preview img').attr( 'src', url );
        $parent.find('.remove-img-btn').remove();
        
        
    });
    
    $('[name="user_form"]').on('submit', function(){
        
        var $sbmtBtn= $('form [type="submit"]'); 
        if( $('#permissions input[type="checkbox"]:checked').length == 0 ) {
            $sbmtBtn.attr('disabled', 'disabled');
            setGlobalMsg( 'Atleast one permission is need to set!' ); // success | warning | info | error (default)
            return false;
        }else{
            $sbmtBtn.removeAttr('disabled');
        }
        return true;    
        
    })
    
    $('input[type="checkbox"]').on('click', function(){
        
        var $this   = $(this)
        ,   isChkd  = $this.is(':checked')
        ,   lvl     = parseInt( $this.attr('data-lvl') )
        ,   $parent = $this.parent().closest('.col')
        ,   isParent= ( $parent.find('input[type="checkbox"]').length > 1 ) ? true : false    
        ,   $sbmtBtn= $('form [type="submit"]');
        
        
        if( $this.hasClass('check-all') ) {
            isParent = true;
            $parent = $('#permissions');
        }
        
        if( isParent )  {
            
            $parent.find( 'input[type="checkbox"]' ).each(function(){
                $(this).prop( 'checked', isChkd );
            });
            
        }
        
        if( $('#permissions input[type="checkbox"]:checked').length > 0 ) {
            $sbmtBtn.removeAttr('disabled');
            closeGlobalMsg();
        }
        
        
        
        
        
        /*if( !isParent ) {
        
        $parent = $parent.parent().closest('section');
        var allChkd         = false
        ,   parentChkLen    = $parent.find('.acts').length//$parent.find('input[type="checkbox"]').length
        ,   parentChkdLen   = $parent.find('.acts:checked').length;//$parent.find('input[type="checkbox"]:checked').length;

        if( parentChkdLen >= parentChkLen ) {
            allChkd = true;
        }

        console.log( lvl +' | '+parentChkLen+ ' | ' + parentChkdLen );
        $parent.find('.master').prop('checked', allChkd);
    }*/
        
     /*   var totalLvl = 3;
        
        for( i = lvl; i > 0; i-- ) {
            
            
            
            
            
        }*/
        
        /*$this.siblings('input[type="checkbox"]').each(function(){
            console.log( $(this).attr('class') );
        })*/
        
        /*for( i = 1; i <= totalLvl; i++ ) {
            
            $parent = $('.lvl-'+i).parent().closest('section');
            var allChkd         = false
            ,   parentChkLen    = $parent.find('.acts').length//$parent.find('input[type="checkbox"]').length
            ,   parentChkdLen   = $parent.find('.acts:checked').length;//$parent.find('input[type="checkbox"]:checked').length;

            if( parentChkdLen >= parentChkLen ) {
                allChkd = true;
            }

            console.log( lvl +' | '+parentChkLen+ ' | ' + parentChkdLen );
            $parent.find('.master').prop('checked', allChkd);
            
            $this = $parent;
            if( i >= lvl  ) {
                break;
            }
        }*/
        
        /*$('.master').each(function(){
        
        //if( isParent ) {
        
           $parent = $(this).parent().closest('.col');
       /*}else{
           $parent = $this.parent().closest('section');
       }*/
        
        /*var allChkd         = false
        ,   parentChkLen    = $parent.find('input[type="checkbox"]').length
        ,   parentChkdLen   = $parent.find('input[type="checkbox"]:checked').length;
        
        console.log( parentChkLen+ ' | ' + parentChkdLen );
        if( parentChkdLen >= --parentChkLen ) {
            allChkd = true;
        }
        
        
           $parent.find('.master:first').prop('checked', allChkd);
        
        
        //}
    //});
        
        
        
       /* if( isParent ) {
        
            $parent = $parent.parent().closest('section');
            
            var allChkd         = false
            ,   parentChkLen    = $parent.find('input[type="checkbox"]').not('.master').length
            ,   parentChkdLen   = $parent.find('input[type="checkbox"]:checked').not('.master').length;

            
            //if( !isParent ){
              //  --parentChkLen;
                //--parentChkdLen;
            //}    
                if( isChkd ) {
                //    --parentChkLen;
                }



            console.log( parentChkLen+ ' | ' + parentChkdLen );
            if( parentChkdLen >= parentChkLen ) {
                allChkd = true;
            }

            //console.log( 'Click Parent: '+ $parent.find('.master').parent('label').find('span').html() );
            $parent.prop('checked', allChkd);
            /*$parent.find('.lvl-'+lvl).not($this).each(function(){
                console.log( 'Parent: '+ $(this).parent('label').find('span').html() );
                $(this).prop('checked', allChkd);
            })*/
        
        
        //}
        
        
        
        
        
    });
    
    $.validator.addMethod("user_name_exists", function(value, element) {
		var return_val = true;
		var usr_name = $('.user_name').val();
		var usr_id = $('.user_id').val();
		if (usr_name != '' ){
			    $.ajax({
					async: false,
					url: baseUrl + 'user/check_user_name_exists',
					type: 'POST',
					data: {user_name: usr_name,user_id: usr_id}, 
				})
				.done(function(data) {
					if(data == '1') {
						return_val = false
		            }
	     		})
				.fail(function(jqXHR, status_code, error_thrown) {
					alert(error_thrown);
				})
        }
		
		return return_val;
	});
    $.validator.addMethod("user_email_exists", function(value, element) {
		var return_val = true;
		var email_id = $('.email').val();
		var usr_id = $('.user_id').val();
		if (email_id != '' ){
			    $.ajax({
					async: false,
					url: baseUrl + 'user/check_email_id_exists',
					type: 'POST',
					data: {email_id: email_id,user_id: usr_id}, 
				})
				.done(function(data) {
					if(data == '1') {
						return_val = false
		            }
	     		})
				.fail(function(jqXHR, status_code, error_thrown) {
					alert(error_thrown);
				})
        }
		
		return return_val;
	});
    
    
var $orderForm = $("#user_form").validate({
    // Rules for form validation
    rules : {
            'company_ids[]' : {
                    required : true
            },
            name : {
                    required : true
            },
            user_name : {
                    required : true,
                    user_name_exists: true
            },
            email : {
                    required : true,
                    email : true,
                    user_email_exists: true
            },
            password : {
                    required : true
            },
            status : {
                    required : true
            },
            
    },

    // Messages for form validation
    messages : {
            'company_ids[]' : {
                    required : 'Please select the company name'
            },
            name : {
                    required : 'Please enter your name'
            },
            user_name : {
                    required : 'Please enter login user name',
                    user_name_exists : 'User Name already Exists'
            },
            email : {
                    required : 'Please enter your email address',
                    email : 'Please enter a VALID email address',
                    user_email_exists : 'Email already Exists'
            },
            password : {
                    required : 'Please enter your password'
            },
            status : {
                    required : 'Please select the Status'
            },
    },
    /*
    submitHandler : function(form) {
      //alert('submmm');
        /*var $sbmtBtn= $(form).find('[type="submit"]'); 
        if( $('#permissions input[type="checkbox"]:checked').length == 0 ) {
            $sbmtBtn.attr('disabled', 'disabled');
            setGlobalMsg( 'Atleast one permission is need to set!' ); // success | warning | info | error (default)
            return false;
        }/*else{
            $sbmtBtn.removeAttr('disabled');
        }*/
        //alert( 'Submitted' );
        /*return true;     
        
        
    },*/

    // Do not change code below
    errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
    }
});
$('.company_ids').change(function(){
//   $('#user_form').validate('revalidateField', 'company_ids[]'); 
//   var company = $(".company_ids");
//   company.data("previousValue", null).valid();
});

   
});