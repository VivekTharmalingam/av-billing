$(document).ready(function () {

    $.validator.addMethod("supplierExist", function (value, element) {
        var return_val = true;
        var supplierName = $('#supplier_name').val();
        var supplierId = $('.supplier_id').val();
        if (supplierName != '') {
            $.ajax({
                async: false,
                url: baseUrl + 'supplier/check_supplier_exists',
                type: 'POST',
                data: {supplier_name: supplierName, supplier_id: supplierId},
            })
                    .done(function (data) {
                        if (data == '1') {
                            return_val = false
                        }
                    })
                    .fail(function (jqXHR, status_code, error_thrown) {
                        alert(error_thrown);
                    })
        }
        return return_val;
    });

    $.validator.addMethod("is_valid_url", function (value, element) {
        if (value != '' && value != undefined) {
            return /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);
        }
//        return /^(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)/.test(value); 
//        return /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/.test(value); 
        return true;
    });

    var companyForm = $("#supplier_form").validate({
        // Rules for form validation
        rules: {
            supplier_name: {
                required: true,
                supplierExist: true
            },
            website: {
                is_valid_url: true
            },
            gst_type: {
                required: true
            }
//            name: {
//                required: true
//            },
//            address: {
//                required: true,
//            },
//            phone_no: {
//                required: true,
//                number: true
//            },
//            email: {
//                required: true,
//                email: true
//            },
//            payment_term: {
//                required: true
//            }

        },
        // Messages for form validation
        messages: {
            supplier_name: {
                required: 'Please enter Supplier Name',
                supplierExist: 'Supplier Name Already Exist'
            },
            website: {
                is_valid_url: 'Please enter valid website'
            },
            gst_type: {
                required: 'Please choose GST'
            }
//            name: {
//                required: 'Please enter Contact Name',
//            },
//            address: {
//                required: 'Please enter address',
//            },
//            phone_no: {
//                required: 'Please enter phone number',
//                number: 'Please enter valid phone',
//            },
//            email: {
//                required: 'Please enter email',
//                email: 'Please enter valid email'
//            },
//            payment_term: {
//                required: 'Please select Payment Term'
//            }

        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

});