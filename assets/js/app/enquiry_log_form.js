$(document).ready(function () {
    $(document).on('change', 'select.company_id', function () {
        var compa_id = $(this).val()
        if(compa_id == 'others') {
            document.getElementById('div').style.display = "block";
        } else {
            document.getElementById('div').style.display = "none";
        }
    });
    
    $(document).on('change', 'select.enquiry_status', function () {
        var enqry_sts = $(this).val()
        if(enqry_sts == '3') {
            document.getElementById('reason').style.display = "block";
        } else {
            document.getElementById('reason').style.display = "none";
        }
    });
    
    $(document).on('change', 'select[name="enquiry_status"]', function () {
        $('#log_form').validate().element(this);
    });

//    $('input[name="por_date"]').on('changeDate', function () {
//        $('form[name="por"]').validate().element('input[name="por_date"]');
//    });

var $orderForm = $("#log_form").validate({
    // Rules for form validation
    rules : {
            
            log_description : {
                    required : true
            },
            enquiry_status: {
                    required : true
            },
            reason: {
                    required : true
            },
            status : {
                    required : true
            },
            
    },

    // Messages for form validation
    messages : {
            
            log_description : {
                    required : 'Please enter Log Description'
            },
            enquiry_status: {
                    required : 'Please select Enquiry Status'
            },
            reason: {
                    required : 'Please enter Reason'
            },
            status : {
                    required : 'Please select the Status'
            },
    },
    // Do not change code below
    errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
    }
});

    $(document).on('change', 'select[name="user_ids[]"], select[name="company_id"], select[name="type"]', function () {
        $('#enquiry_log_form').validate().element(this);
    });

var $orderForm = $("#enquiry_log_form").validate({
    // Rules for form validation
    rules : {
            company_id: {
                    required : true
            },
            other_name: {
                    required : true
            },
            contact_person : {
                    required : true
            },
            contact_no : {
                    required : true
            },
            email : {
                    required : true,
                    email : true
            },
            type: {
                    required : true
            },
            description : {
                    required : true
            },
            'user_ids[]' : {
                    required : true
            },
            status : {
                    required : true
            },
            
    },

    // Messages for form validation
    messages : {
            company_id: {
                    required : 'Please select the Company Name'
            },
            other_name: {
                    required : 'Please enter Company Name'
            },
            contact_person : {
                    required : 'Please enter contact name'
            },
            contact_no : {
                    required : 'Please enter contact no'
            },
            email : {
                    required : 'Please enter your email address',
                    email : 'Please enter a VALID email address'
            },
            type : {
                    required : 'Please select the type'
            },
            description : {
                    required : 'Please enter Description'
            },
            'user_ids[]' : {
                    required : 'Please select the Assigned To'
            },
            status : {
                    required : 'Please select the Status'
            },
    },
    // Do not change code below
    errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
    }
});

});