$(document).ready(function () {
    
    $('.open_balance').keyup(function(){
        var open_balance = $(this).val();
        var opb_update_status = $('.opb_update_status').val();
        if(opb_update_status == 'yes') {
            $('.crnt_bln').text(open_balance);
        }
    });
    
    $.validator.addMethod("accExist", function (value, element) {
        var return_val = true;
        var accNo = $('#account_no').val();
        var bnkId = $('.bank_id').val();
        if (accNo != '') {
            $.ajax({
                async: false,
                url: baseUrl + 'bank/check_exp_exists',
                type: 'POST',
                data: {account_no: accNo, bank_id: bnkId},
            })
                    .done(function (data) { 
                        if (data == '1') {
                            return_val = false
                        }
                    })
                    .fail(function (jqXHR, status_code, error_thrown) {
                        alert(error_thrown);
                    })
        }
        return return_val;
    });
    
    var companyForm = $("#bank_form").validate({
        // Rules for form validation
        rules: {
            bank_name: {
                required: true
            },
            account_no: {
                required: true,
                accExist: true
            }
        },
        // Messages for form validation
        messages: {
            bank_name: {
                required: 'Please enter Bank Name'
            },
            account_no: {
                required: 'Please enter Account No.',
                accExist: 'Account No. already exists',
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

});
