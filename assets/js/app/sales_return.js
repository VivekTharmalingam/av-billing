var addRowIndex = 0;
var so_items;
var formType = $('.so_no').parents('form').hasClass('add_form') ? 'add' : 'edit';
function return_type_change(_this) {
	if ($(_this).val() == '2') {
		$(_this).closest('.items').find('.restock_quantity').prop('readonly', false);
	}
	else {
		$(_this).closest('.items').find('.restock_quantity').val('');
		$(_this).closest('.items').find('.restock_quantity').prop('readonly', true);
	}
}

$(document).ready(function () {
    $('.so_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    
	var so = $('select.so_no option:selected').val();
	if(so!=''){
		get_customer_details(so); 
	}
	
    $('#so_no').on('change', function (e) {
        if ($(this).val() == '') {
            $('#invoice_details').slideUp();
			$('#customer_name, #contact_name, #contact_no, #contact_address').html('N/A');
            return '';
        }
		else {
			$('#invoice_details').slideDown();
		}
		
        var soId = $(this).val();
        get_customer_details(soId);
    });
	
	$('select.return_item_type').each(function() {
		return_type_change(this);
	});
	
	$(document).on('change', 'select.return_item_type', function () {
        return_type_change(this);
    });
	
    $(document).on('change', 'select.pdt_id', function () {
        var option = $(this).children(':selected');
		if ($('.items').find('.pdt_id option:selected[value="' + option.val() + '"]').length > 1) {
			alert('Selected item already exists!');
			$(this).val('').trigger('change');
			return false;
		}
		else {
			var item = $(this).parents('.items');
			item.find('.received_quantity').val(option.data('grn_quantity'));
			item.find('.grni_quantity').val(option.data('hidden_grn_quantity'));
			item.find('.category_id').select2('val', option.data('category_id'));
			item.find('.product_description').val(option.data('desc'));
			item.find('.product_id').val(option.data('item_id'));
			if (parseFloat(option.data('returned_qty')) > 0) {
				item.find('.already_return_qty').val(already_return_qty);
				item.find('.already_return').show();
			}
		}
    });


    /* Add row functionality for material */
    $(document).on('click', '.items .add-row', function () {
        addRowIndex++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.items').clone();
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');
        //itemObj.find('.vat_percentage').attr('checked', false);

        itemObj.find('input, select, textarea').each(function (index, element) {
            if ($(element).hasClass('item_hidden_ids')) {
                $(element).remove();
            }
            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndex + ']');
            }
            
            if (($(element).prop('tagName').toLowerCase() == 'select') && ($(element).prev().hasClass('select2'))) {
				$(element).prev().remove();
				$(element).select2({width: '100%'});
				$(element).val('2');
            }
			else {
				$(element).val('');
			}
        });

        var materialContainer = $(this).parents('.material-container');
        var optionsLength = $(materialContainer).find('select.product_id:first option:not([value=""]):not(:disabled)').length;
        /*if (optionsLength > $(materialContainer).find('.items').length) {*/
        if ($(this).parent().find('.remove-row').length)
            $(this).remove();
        else
            $(this).removeClass('add-row fa-plus-circle').addClass('remove-row fa-minus-circle');

        if ($(itemObj).find('.remove-row').length == 0)
            $(itemObj).find('.add-row').after('<i class="fa fa-2x fa-minus-circle remove-row"></i>');

        $(materialContainer).find('.items:last').after(itemObj);
		set_product_autocomplete();
    });

    /* Remove row functionality for material */
    $(document).on('click', '.items .remove-row', function () {
        var materialContainer = $(this).parents('.material-container');
        $(this).parents('.items').remove();
        if (materialContainer.find('.items:last').find('.add-row').length == 0) {
            materialContainer.find('.items:last').find('.remove-row').before('<i class="fa fa-2x fa-plus-circle add-row"></i>');
        }

        if ((materialContainer.find('.items').length == 1) && (materialContainer.find('.items:last').find('.add-row').length)) {
            materialContainer.find('.items:last').find('.remove-row').remove();
        }
        calculate_refund_amount();
    });

    $.validator.addMethod('mat_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please enter Item Code!');

    $.validator.addMethod('mat_already_exist', function (value, element) {
        var prevItems = $(element).parents('.items').prevAll('.items');
        return (prevItems.find('input.product_id[value="' + value + '"]').length) ? false : true;
    }, 'Item Name is already Exists!');
	
    $.validator.addMethod('qty_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please enter Quantity!');

    $.validator.addMethod('item_type_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please select Type!');

    $.validator.addMethod('return_quantity_required', function (value, element) {
        var return_val = true;
        if ($(element).parents('.items').find('input.product_id').val() == '') {
            return_val = true;
        }
        else {
            if (value == '') {
                return_val = false;
            }
        }
        return return_val;
    }, 'Please enter Return Quantity!');
    
    $.validator.addMethod('return_quantity_grt', function (value, element) {
        var return_val = true;
        var ths_return_qty = (value != '') ? parseFloat(value) : 0;
        var ths_inv_qty = ($(element).parents('.items').find('input.inv_quantity').val() != '') ? parseFloat($(element).parents('.items').find('input.inv_quantity').val()) : 0;
        if(ths_inv_qty < ths_return_qty) {
            return_val = false;
        }
        
        return return_val;
    }, 'Return Quantity should be less than Sold Quantity!..');

    /*$.validator.addMethod('valid_qty', function (value, element) {        
        var receivedQty = parseFloat($(element).parents('.items').find('.received_quantity').val());
        
        var return_val = true;
        if ($(element).parents('.items').find('select.product_id').val() == '') {
            return_val = true;
        }
        else {
            if ((receivedQty != '') && (receivedQty >= parseFloat(value))) {
                return_val = true;
            }
            else {
                return_val = false;
            }
        }
        return return_val;
    }, 'Returned Quantity should not be greater than Sold Quantity!');*/

    $.validator.addClassRules({
        product_id: {
            mat_required: true,
            mat_already_exist: true
        },
        return_quantity: {
            return_quantity_required: true,
            return_quantity_grt: true
        },
        return_item_type: {
            item_type_required: true
        }
    });

    $('select[name="customer_name"], select[name="ship_to"], select.category_id, select.product_id, select.quantity').on('change', function () {
        $('#sales_order_ret_add').validate().element(this);
    });

    $('input[name="so_date"], input[name="cheque_date"], input[name="dd_date"]').on('changeDate', function () {
        $('#sales_order_ret_add').validate().element(this);
    });

    $("#sales_order_ret_add").validate({
        // Rules for form validation
        invalidHandler: function (event, validator) {
            $('body').animate({
                scrollTop: $(validator.currentForm).offset().top
            },
            '500',
			'swing'
			);
        },
        rules: {
            customer_name: {
                required: true
            },
            ship_to: {
                required: true
            },
            so_date: {
                required: true
            }
        },
        // Messages for form validation
        messages: {
            customer_name: {
                required: 'Please select Bill To!'
            },
            ship_to: {
                required: 'Please select Ship To!'
            },
            so_date: {
                required: 'Please select Date!'
            }            
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
	
	$(document).on('keyup', '.return_quantity', function() {
		if ($(this).closest('.items').find('select.return_item_type').val() == '2') {
			$(this).closest('.items').find('input.restock_quantity').val($(this).val());
		}
		
		calculate_refund_amount();
	});
	
	$(document).on('change', '.return_item_type', function() {
		var item = $(this).closest('.items');
		var return_quantity = 0;
		if ($(this).val() == '2') {
			return_quantity = item.find('input.return_quantity').val();
		}
		item.find('input.restock_quantity').val(return_quantity);
		calculate_refund_amount();
	});
});

function hide_product_code_msg() {
    $('.product_code_label').slideUp();
}

function calculate_refund_amount() {
	var refund_amount = 0;
	$(".items").each(function (key, element) {
		var items = $(element);
		if(items.find('select.return_item_type').val() != '3') {
			var quantity = (items.find('.return_quantity').val() != '') ? parseFloat(items.find('.return_quantity').val()) : 0;
			var sold_price = (items.find('.sold_price').val() != '') ? parseFloat(items.find('.sold_price').val()) : 0;
			refund_amount += quantity * sold_price;
		}
	});
	
	$('#refund_amount').val(refund_amount.toFixed(2));
}

function set_product_autocomplete() {
	$(".product_code").each(function () {
		/*if ($(this).hasClass('hasAutocomplete')) {
			return true;
		}*/
		
		$(this).autocomplete({
            source: so_items,
            focus: function (event, ui) {
                //$(this).val(ui.item.value);
                //return false;
            },
            select: function (event, ui) {
                var items = $(this).closest('.items');
                var product_id_name = items.find('.product_id').prop('name');
                if ($('.product_details').find('.product_id[value="' + ui.item.item_id + '"]:not(.product_id[name="' + product_id_name + '"])').length) {
                    // Item already exists
                    if (items.find('.product_code').data('value').trim() != ui.item.value.trim()) {
                        items = $('.product_details').find('.product_id[value="' + ui.item.item_id + '"]').closest('.items');
						
						$(this).val('');
                        $(this).closest('.items').find('.product_code_label').slideDown();
                        setTimeout(hide_product_code_msg, 3000)
                        return false;
                    }
                    else {
                        items.find('.product_code').val(ui.item.value.trim());
                        des = ui.item.pdt_name;
                        items.find('.product_description').val(des.trim());
                    }
                }
                else {
                    items.find('.product_code').data('value', ui.item.pdt_code.trim());
                    items.find('.product_code').val(ui.item.pdt_code.trim());
                    //items.find('.category_id').val(ui.item.cat_id);
                    items.find('.product_id').val(ui.item.item_id);
                    items.find('.so_item_autoid').val(ui.item.so_item_autoid);
//                    items.find('.brand_name').html(ui.item.cat_name);
//                    items.find('.brand_label').slideDown();
//                    items.find('.product_description').val(ui.item.pdt_name);
                    
                    items.find('.product_code').data('value', ui.item.pdt_code.trim());
                    items.find('.product_description').val(ui.item.pdt_name.trim());
					items.find('span.sold_price').val(ui.item.price);
					items.find('input.sold_price').val(ui.item.price);
					items.find('span.inv_quantity').html(ui.item.so_quantity);
					items.find('input.inv_quantity').val(ui.item.so_quantity);
                                        
                                        items.find('select.serial_nos_txt').find('option').remove();
                                        var serial_nums = (ui.item.serial_no != '') ? ui.item.serial_no.split(',') : new Array();
                                        var opt_txt  = '';
                                        for (var i in serial_nums) {
                                            opt_txt += '<option value="'+serial_nums[i]+'">'+serial_nums[i]+'</option>';
                                        }
                                        items.find('select.serial_nos_txt').append(opt_txt);
                                        
					items.find('input.sold_item').show();
					var returned_qty = (ui.item.already_returned_qty != '') ? ui.item.already_returned_qty : 0;
                                        items.find('span.already_return_qty').html(returned_qty);
					if (returned_qty > 0) {
						items.find('.already_return').show();
					}
                }
				
                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || event.keyCode;
                if (charCode == 13)
                    items.find('.quantity').focus();
                else if (event.type == 'autocompleteselect')
                    items.find('.product_description').focus();
				
                return false;
            }
        });

        //$(this).addClass('hasAutocomplete');
	});
	calculate_refund_amount();
}

function get_customer_details(soId) {
    if (soId != '') {
        var ajaxURL = baseUrl + 'invoice_return/customer_details';
        var ajaxData = {
            so_id: soId
        }
        var data = ajaxRequest(ajaxURL, ajaxData, '');
		$('#customer_name').html(data.so.name);
		$('#contact_name').html(data.so.contact_name);
		$('#contact_no').html(data.so.phone_no);
		$('#contact_address').html(data.so.address);
		
        $('#remarks').val(data.so.your_reference);
		
		var itemObj = $('.items:first').clone();
		var addRowObj = $('.add-row:first').clone();
		$('.items').remove()
		so_items = data.so_items;
        
        $.each(so_items, function (index, value) {
			addRowIndex ++;
			if ($('.items').length) {
				itemObj = $('.items:first').clone();
			}
			
			itemObj.find('input, select, textarea').each(function (index, element) {
				if ($(element).hasClass('item_hidden_ids')) {
					$(element).remove();
				}
				if ($(element).prop('name') != '') {
					var name = $(element).prop('name').split('[')[0];
					name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                                        if(name == 'serial_nos_txt') {
                                            $(element).prop('name', name + '[' + addRowIndex + '][]');
                                        } else {
                                            $(element).prop('name', name + '[' + addRowIndex + ']');
                                        }
				}
				
				if (($(element).prop('tagName').toLowerCase() == 'select') && ($(element).prev().hasClass('select2'))) {
					$(element).prev().remove();
					$(element).select2({width: '100%'});
					$(element).val('2');
				}
				else {
					$(element).val('');
				}
			});
			
			if (itemObj.find('select.return_item_type').prev().hasClass('select2')) {
				itemObj.find('select.return_item_type').prev().remove();
				itemObj.find('select.return_item_type').select2({width: '100%'});
            }
			
			itemObj.find('input.product_id').val(value.item_id);
			itemObj.find('input.so_item_autoid').val(value.so_item_autoid);
			itemObj.find('input.sold_price').val(value.price);
			itemObj.find('span.sold_price').html(value.price);
			itemObj.find('input.product_code').val(value.pdt_code);
			itemObj.find('input.product_code').data('value', value.pdt_code);
			itemObj.find('textarea.product_description').val(value.product_description);
			
			itemObj.find('.sold_item').show();
			itemObj.find('span.inv_quantity').html(value.so_quantity);
			itemObj.find('input.inv_quantity').val(value.so_quantity);
                        
                        itemObj.find('select.serial_nos_txt').find('option').remove();
                        var serial_nums = (value.serial_no != '') ? value.serial_no.split(',') : new Array();
                        var opt_txt  = '';
                        for (var i in serial_nums) {
                            opt_txt += '<option value="'+serial_nums[i]+'">'+serial_nums[i]+'</option>';
                        }
                        itemObj.find('select.serial_nos_txt').append(opt_txt);
			
			var returned_qty = (value.already_returned_qty != '') ? value.already_returned_qty : 0;
			if (returned_qty > 0) {
				itemObj.find('span.already_return_qty').html(returned_qty);
				itemObj.find('label.already_return').show();
			}
			
			//var return_qty = value.so_quantity + returned_qty;
			//return_qty -= value.so_quantity;
			var return_qty = value.so_quantity;
			itemObj.find('input.return_quantity').val(return_qty);
			itemObj.find('input.restock_quantity').val(0);
			if (itemObj.find('select.return_item_type').val() == '2') {
				itemObj.find('input.restock_quantity').val(return_qty);
			}
			
			if (itemObj.find('.add-row').length && itemObj.find('.remove-row').length) {
				itemObj.find('.remove-row').remove();
			}
			
            itemObj.find('.add-row').removeClass('add-row fa-plus-circle').addClass('remove-row fa-minus-circle');
			$('.product_details').append(itemObj);
        });
		
		$('.product_details').find('.remove-row:last').before(addRowObj);
		set_product_autocomplete();
    }
}

/* Barcode search functions */
function get_product_details() {
    if ($('input[name="search_product"]').val() == '') {
        var smallBoxTitle = 'Sorry ! ...';
        var smallBoxMsg = '<i class="fa fa-info-circle"></i> <i>Please enter Item code or name.</i>';
        if ($('#divSmallBoxes').html().length == 0)
            showSmallBox(smallBoxTitle, smallBoxMsg);
    }

    return false;
}