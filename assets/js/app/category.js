$(document).ready(function () {
	var ajaxURL = baseUrl + 'brand/brand_auto_complete';
    var ajaxData = {};
    var brands = ajaxRequest(ajaxURL, ajaxData, '');
	
	$('#category_name').autocomplete({
		source: brands,
		select: function (event, ui) {
			$('#category_form').validate().element($('#category_name'));
		}
	});
	
    $.validator.addMethod("categoryExist", function (value, element) {
        var return_val = true;
        var categoryName = $('#category_name').val();
        var categoryId = $('.category_id').val();
        if (categoryName != '') {
            $.ajax({
                async: false,
                url: baseUrl + 'brand/check_category_exists',
                type: 'POST',
                data: {category_name: categoryName, category_id: categoryId},
            })
                    .done(function (data) {
                        if (data == '1') {
                            return_val = false
                        }
                    })
                    .fail(function (jqXHR, status_code, error_thrown) {
                        alert(error_thrown);
                    })
        }
        return return_val;
    });
    var companyForm = $("#category_form").validate({
        // Rules for form validation
        rules: {
            category_name: {
                required: true,
                categoryExist: true
            }
        },
        // Messages for form validation
        messages: {
            category_name: {
                required: 'Please enter Category Name',
                categoryExist: 'Category Name Already Exist'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

});