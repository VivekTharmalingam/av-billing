var addRowIndex = 0;
var active = $('.po_id').parents('form').hasClass('add_form') ? 'add' : 'edit';
$(document).ready(function () {
    /* Add row functionality for material */
    $(document).on('click', '.items .add-row', function () {
        poId = $('.po_id').select2('val')
//        if ($('.po_type:checked').val() == '1' && poId != '') {
//            items = get_all_items1(poId);
//            product_codes = get_all_items1(poId, 1);
//            descriptions = get_all_items_descriptions1(poId);
//            $(".product_code").autocomplete('option', 'source', product_codes)
//            $(".product_description").autocomplete('option', 'source', descriptions);
//        } else {
//            items = get_all_items('');
//            product_codes = get_all_items(1);
//            descriptions = get_all_items_descriptions('');
//            $(".product_code").autocomplete('option', 'source', product_codes)
//            $(".product_description").autocomplete('option', 'source', descriptions);
//        }
        addRowIndex++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.items').clone();
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');
        itemObj.find('.brand_name').html('');
        itemObj.find('.brand_label').hide();
        //itemObj.find('.vat_percentage').attr('checked', false);

        itemObj.find('input, select, textarea').each(function (index, element) {
            if ($(element).hasClass('item_hidden_ids')) {
                $(element).remove();
            }

            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndex + ']');
            }
            $(element).val('');

            if ($(element).hasClass('product_description'))
                $(element).data('value', '');

            if ($(element).hasClass('hasAutocomplete'))
                $(element).removeClass('hasAutocomplete')

            /*if ($(element).prop('tagName').toLowerCase() == 'select') {
             if ($(element).prev().hasClass('select2')) {
             $(element).prev().remove();
             $(element).select2({width: '100%'});
             }
             }*/
        });

        var materialContainer = $(this).parents('.material-container');
        var optionsLength = items.length;
        if (optionsLength > $(materialContainer).find('.items').length) {
            if ($(this).parent().find('.remove-row').length)
                $(this).remove();
            else
                $(this).removeClass('add-row fa-plus-circle').addClass('remove-row fa-minus-circle');

            if ($(itemObj).find('.remove-row').length == 0)
                $(itemObj).find('.add-row').after('<i class="fa fa-2x fa-minus-circle remove-row"></i>');

            $(materialContainer).find('.items:last').after(itemObj);
        }
        else if ((optionsLength == $(materialContainer).find('.items').length) || (optionsLength == 0)) {
            var smallBoxTitle = 'Sorry ! ...';
            var smallBoxMsg = '<i class="fa fa-info-circle"></i> <i>You can\'t add more...<br> Because you have';
            smallBoxMsg += (optionsLength > 0) ? ' only ' + optionsLength : ' no';
            smallBoxMsg += ($(materialContainer).find('.items').length > 1) ? ' items' : ' item';
            smallBoxMsg += '.</i>';

            if ($('#divSmallBoxes').html().length == 0)
                showSmallBox(smallBoxTitle, smallBoxMsg);
        }

        set_product_autocomplete();
        $('.items:last .product_code').focus();
    });

    /* Remove row functionality for material */
    $(document).on('click', '.items .remove-row', function () {
        var materialContainer = $(this).parents('.material-container');
        $(this).parents('.items').remove();
        if (materialContainer.find('.items:last').find('.add-row').length == 0) {
            materialContainer.find('.items:last').find('.remove-row').before('<i class="fa fa-2x fa-plus-circle add-row"></i>');
        }

        if ((materialContainer.find('.items').length == 1) && (materialContainer.find('.items:last').find('.add-row').length)) {
            materialContainer.find('.items:last').find('.remove-row').remove();
        }
        $('.items:last .product_code').focus();
    });

    $(document).on('change', 'select.select2', function () {
        $('form[name="por"]').validate().element(this);
    });

    $('input[name="por_date"]').on('changeDate', function () {
        $('form[name="por"]').validate().element('input[name="por_date"]');
    });

    $.validator.addMethod('mat_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please select Material Name!');

    $.validator.addMethod('mat_already_exist', function (value, element) {
        var prevItems = $(element).parents('.items').prevAll('.items');
        return (prevItems.find('select.mat_id option[value="' + value + '"]:selected').length) ? false : true;
    }, 'Material Name is already Exists!');

    $.validator.addMethod('qty_received', function (value, element) {
        var return_val = true;
        if ($(element).parents('.items').find('.product_id').val() == '') {
            return_val = true;
        }
        else {
            if (value == '') {
                return_val = false;
            }
        }
        return return_val;
    }, 'Please enter Return Quantity!');

    $.validator.addMethod('valid_qty', function (value, element) {
        var receivedQty = parseFloat($(element).parents('.items').find('.rec_quantity').val());
        var return_val = true;
        if ($(element).parents('.items').find('.product_id').val() == '') {
            return_val = true;
        }
        else {
            if ((receivedQty != '') && (receivedQty >= parseFloat(value))) {
                return_val = true;
            }
            else {
                return_val = false;
            }
        }
        return return_val;
    }, 'Return Quantity should not be greater than Received Quantity!');

    $.validator.addMethod('min_ware_qty', function (value, element) {
        if ($('select.po_id option:selected').val() != '' && $('select.po_id option:selected').val() != undefined) {
            var return_val = false;
            var ware_id = $('.hidden_brn_id').val();
            var mat_id = $(element).parents('.items').find('select.mat_id').val();
            $.ajax({
                async: false,
                url: baseUrl + 'purchase_returns/get_item_qty_by_mat_id',
                type: 'POST',
                data: {mat_id: mat_id, ware_id: ware_id},
                dataType: "json",
            })
                    .done(function (data) {
                        if (parseFloat(data.quantity.available_qty) < parseFloat(value)) {
                            if (!$('.SmallBox').length) {
                                $.smallBox({
                                    title: "Error",
                                    content: "<i class='fa fa-warning'></i> <i> Insufficient Quantity in Warehouse (" + $('.ware_id').val() + ") </i>",
                                    color: "#A90329",
                                    iconSmall: "fa fa-thumbs-o-down bounce animated",
                                    timeout: 5000
                                });
                            }
                            return_val = false;
                        }
                        else {
                            return_val = true;
                        }
                    });
            return return_val;

        }
    }, '');


    $.validator.addClassRules({
//        mat_id: {
//            mat_required: true,
//            mat_already_exist: true
//        },
//        returned_quantity: {
//            qty_received: true,
//            valid_qty: true,
//            min_ware_qty: true
//        }
        retn_quantity: {
            qty_received: true,
            valid_qty: true,
        }
    });

    var $orderForm = $("#por_form").validate({
        // Rules for form validation
        invalidHandler: function (event, validator) {
            $('body').animate({
                scrollTop: $(validator.currentForm).offset().top
            },
            '500',
                    'swing'
                    );
        },
        rules: {
            po_id: {
                required: true
            },
            por_date: {
                required: true
            }
        },
        // Messages for form validation
        messages: {
            po_id: {
                required: 'Please select PO Number'
            },
            por_date: {
                required: 'Please select Date'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

    if (active == 'edit') {
        var po = $('select.po_id option:selected').val();
        $('.hidden_po_id').val(po);
        var ware = '';
        var mat_id = '';
        if(po !=''){
            po_wise_warehouse(po, ware);
            items = get_all_items(po);
            product_codes = get_all_items(po, 1);
            descriptions = get_all_items_descriptions(po);
        }        
        set_product_autocomplete();
    }
	
    $(document).on('change', '.po_id', function (e) {        
        var po = $(this).val();
        if(po==''){
            location.reload();
        }
        $('.hidden_po_id').val(po);
        var branch = '';
        var products = '';
        if(po !=''){
            po_wise_warehouse(po, branch);
            items = get_all_items(po);
            product_codes = get_all_items(po, 1);
            descriptions = get_all_items_descriptions(po);
        }        
        set_product_autocomplete();
        $(".product_code").autocomplete('option', 'source', product_codes);
        $(".product_description").autocomplete('option', 'source', descriptions);
        $('.product_details .items .remove-row').trigger('click');           
        fillData(items);
    });
    $(document).on('change', 'select.mat_id', function () {
        var option = $(this).children(':selected');
        var item = $(this).parents('.items');
        item.find('.received_quantity').val(option.data('grn_quantity'));
        item.find('.grni_quantity').val(option.data('hidden_grn_quantity'));
        item.find('.unit_name').val(option.data('unit_name'));
        item.find('.unit_id').val(option.data('unit_id'));
    });
    
    $(document).on('keyup', 'input.search_product', function () {
        var ser = $(this).val();
        var _this = $(this);
        if (ser) {
            $.each(items, function (key, value) {
                if (ser === value.bar_code) {
                    if ($('.product_details').find('.product_id[value="' + value.id + '"]').length) {
                        // Item already exists
                        var itemsCol = $('.product_details').find('.product_id[value="' + value.id + '"]').closest('.items');
                        var quantity = (itemsCol.find('.retn_quantity').val() != '') ? parseFloat(itemsCol.find('.retn_quantity').val()) : 0;
                        quantity += 1;
                        itemsCol.find('.retn_quantity').val(quantity);
                    }
                    else {
                        if ($('.product_details .items:last .product_id').val() != '') {// Last row occupied by another product.
                            $('.product_details .items:last .add-row').trigger('click');
                        }
                        var itemsCol = $('.product_details .items:last');
                        var returned_qty = (value.already_returned_qty != '') ? value.already_returned_qty : 0;
                        var available_qty = parseFloat(value.pr_quantity) -  parseFloat(returned_qty);
                        itemsCol.find('.category_id').val(value.cat_id);
                        itemsCol.find('.product_id').val(value.id);
                        itemsCol.find('.product_code').val(value.pdt_code);
                        des = value.pdt_name;
                        if (value.cat_name != '' && value.cat_name != null) {
                            des = value.cat_name + ' ' + value.pdt_name;
                        }
                        itemsCol.find('.product_description').val(des);
                        itemsCol.find('.rec_quantity').val(available_qty);
                        itemsCol.find('.por_qty').val(value.pr_quantity);
                        itemsCol.find('.retn_quantity').val(1);
                        itemsCol.find('.retn_quantity').focus().select();
                    }                
                    itemsCol.find('.retn_quantity').focus().select();
                    $(_this).val('');
                    return false;
                }
            });
        }
    });
});

function fillData(items) {
        var filled = $('.product_details .items').length
            , $row = $('.product_details .items:eq(' + (filled - 1) + ')');
    var returned_qty = (items[filled - 1].already_returned_qty != '') ? items[filled - 1].already_returned_qty : 0;
    //var available_qty = parseFloat(items[filled - 1].pr_quantity) -  parseFloat(returned_qty);
	if (returned_qty > 0) {
		$row.find('.recd_qty').html(returned_qty);
		$row.find('.received_quantity').show();
	}
	
    var available_qty = parseFloat(items[filled - 1].pr_quantity);       
        $row.find('.category_id').val(items[filled - 1].cat_id);
        $row.find('.product_id').val(items[filled - 1].id);
        $row.find('.product_code').val(items[filled - 1].pdt_code);
        des = items[filled - 1].pdt_name;
        if (items[filled - 1].cat_name != '' && items[filled - 1].cat_name != null) {
            des = items[filled - 1].cat_name + ' ' + items[filled - 1].pdt_name;
        }
        $row.find('.product_description').val(des);   
        $row.find('.rec_quantity').val(available_qty);
        $row.find('.por_qty').val(items[filled - 1].pr_quantity);
        $row.find('.retn_quantity').val('');
        $row.find('.retn_quantity:first').focus().select();
        if (filled < items.length) {
            $('.product_details .items:last .add-row').trigger('click');
            //window.setTimeout(function () {
                fillData(items);
            //}, 100);
        }

}

function set_product_autocomplete() {
    $(".product_code").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }
        $(this).autocomplete({
            source: product_codes,
            focus: function (event, ui) {
                //$(this).val(ui.item.value);
                //return false;
            },
            select: function (event, ui) {
                var itemsCol = $(this).closest('.items');
                var returned_qty = (ui.item.already_returned_qty != '') ? ui.item.already_returned_qty : 0;
                var available_qty = parseFloat(ui.item.pr_quantity) -  parseFloat(returned_qty);
                if ($('.product_details').find('.product_id[value="' + ui.item.id + '"]').length) {
                    // Item already exists
                    if ($(this).data('value') != ui.item.value) {
                        itemsCol = $('.product_details').find('.product_id[value="' + ui.item.id + '"]').closest('.items');
                        $(this).val('');
                        $(this).closest('.items').find('.product_code_label').slideDown();
                        setTimeout(hide_product_code_msg, 3000)
                        //return false;
                    }
                    else {
                        $(this).val(ui.item.value);
                    }
                }else {
                    $(this).val(ui.item.pdt_code);
                    itemsCol.find('.category_id').val(ui.item.cat_id);
                    itemsCol.find('.product_id').val(ui.item.id);
                    des = ui.item.pdt_name;
                    if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                        des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                    }
                    itemsCol.find('.product_code').data('value', ui.item.pdt_code);
                    itemsCol.find('.product_description').val(des);
                    itemsCol.find('.rec_quantity').val(available_qty);
                    itemsCol.find('.por_qty').val(ui.item.pr_quantity);
                    itemsCol.find('.retn_quantity').val(1);
                }
                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || e.keyCode;
                if (charCode == 13)
                    itemsCol.find('.retn_quantity').focus();
                else if (event.type == 'autocompleteselect')
                    itemsCol.find('.product_description').focus();
                itemsCol.find('.retn_quantity').focus();
                /* To focus quantity of particular item end */
                return false;
            }
        });
        $(this).addClass('hasAutocomplete');
    });

    $(".product_description").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }
        $(this).autocomplete({
            source: descriptions,
            focus: function (event, ui) {
                //$(this).val(ui.item.value);
                //return false;
            },
            select: function (event, ui) {
                //alert('test');
                var itemsCol = $(this).closest('.items');
                var returned_qty = (ui.item.already_returned_qty != '') ? ui.item.already_returned_qty : 0;
                var available_qty = parseFloat(ui.item.pr_quantity) -  parseFloat(returned_qty);
                if ($('.product_details').find('.product_id[value="' + ui.item.id + '"]').length) {
                    // Item already exists
                    if ($(this).data('value') != ui.item.value) {
                        itemsCol = $('.product_details').find('.product_id[value="' + ui.item.id + '"]').closest('.items');
                        $(this).val($(this).data('value'));
                        $(this).closest('.items').find('.error_description').slideDown();
                        setTimeout(hide_error_description_msg, 3000);
                        itemsCol.find('.retn_quantity').focus();
                        //return false;
                    }
                    else {
                        $(this).val(ui.item.value);
                    }
                }
                else {
                    itemsCol.find('.product_code').val(ui.item.pdt_code);
                    //$(this).val(ui.item.pdt_code);
                    itemsCol.find('.category_id').val(ui.item.cat_id);
                    itemsCol.find('.product_id').val(ui.item.id);
                    if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                        des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                    }
                    else {
                        des = ui.item.pdt_name;
                    }
                    itemsCol.find('.product_description').data('value', des);
                    itemsCol.find('.product_description').val(des);
                    itemsCol.find('.rec_quantity').val(available_qty);
                    itemsCol.find('.por_qty').val(ui.item.pr_quantity);
                    itemsCol.find('.retn_quantity').val(1);
                }
                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || e.keyCode;
                if (charCode == 13)
                    itemsCol.find('.quantity').focus();
                else if (event.type == 'autocompleteselect')
                    itemsCol.find('.product_description1').focus();
                itemsCol.find('.retn_quantity').focus();
                /* To focus quantity of particular item end */
                //return false;
            }
        });
        $(this).addClass('hasAutocomplete');
    });
}

function get_all_items_descriptions(po) {
    var ajaxURL = baseUrl + 'purchase_returns/search_item_description';
    var ajaxData = {
        po: po,
    }
    return ajaxRequest(ajaxURL, ajaxData, '');
}

function get_all_items(po, product_code) {
    var ajaxURL = baseUrl + 'purchase_returns/search_item';
    var ajaxData = {
        po: po,
        product_code: product_code,
    }
    return ajaxRequest(ajaxURL, ajaxData, '');
}

function po_wise_warehouse(poId, brnId) {
    if (poId != '') {
        var ajaxURL = baseUrl + 'goods_receive/get_branch_by_po';
        var ajaxData = {
            po_id: poId
        }
        var loaderSelector = 'input[name="branch_name"]';
        var data = ajaxRequest(ajaxURL, ajaxData, loaderSelector);
        //$(".supplier_name").val(data.supplier.name);
        //$(".branch_name").val(data.brn.branch_name);
        $(".hidden_brn_id").val(data.brn.brnid);
        //$(".contact_person").val(data.supplier.contact_name);
        //$(".contact_no").val(data.supplier.phone_no);
        //$(".supplier_address").val(data.supplier.address);
        $(".supplier_name").html(data.supplier.name);
        $(".supplier_address").html((data.supplier.address) ? data.supplier.address : 'N/A');
        $(".supplier_cname").html((data.supplier.contact_name) ? data.supplier.contact_name : 'N/A');
        $(".supplier_cno").html((data.supplier.phone_no) ? data.supplier.phone_no : 'N/A');
        $(".sup_details").slideDown();
        /*var per_disc = 0;
        if (data.brn.po_discount_type != '1') {
            per_disc = parseFloat(data.brn.po_total_discount / data.brn.po_sub_total);
        }
        $(".per_item_discount").val(per_disc);*/
        //$('.gst_type1').val(data.brn.po_discount_type);
    }
	else {
        $(".supplier_name").val('');
        $(".branch_name").val('');
        $(".contact_person").val('');
        $(".contact_no").val('');
        $(".supplier_address").val('');
        $(".sup_details").slideUp();
        //$('.gst_type1').val();
    }
}

/*function calculate_price(_this) {
    var received_qty = ($(_this).val() != '') ? $(_this).val() : 0;
    var rate = ($(_this).parents('.items').closest('div').find('.grni_price').val() != '') ? $(_this).parents('.items').closest('div').find('.grni_price').val() : 0;

    var price = parseFloat(received_qty) * parseFloat(rate);
    $(_this).parents('.items').closest('div').find('.grni_total_amt').val(price.toFixed(2));
    if ($(_this).val() != '') {
        getsubtotalval();
        getnettotalval();
    }
}

function getsubtotalval() {
    var subtotalpriceval = 0;
    $('.grni_total_amt').each(function () {
        if ($(this).val() != '') {
            subtotalpriceval += parseFloat($(this).val());
        }
    });
    $('.grn_sub_total_amt').val(subtotalpriceval.toFixed(2));

    var dis = $('.discount_type:checked').val();
    var sub_total = $('.grn_sub_total_amt').val();
    var discount = ($('.discount').val() != '') ? parseFloat($('.discount').val()) : 0;
    var item_total = ($('.grn_sub_total_amt').val() != '') ? parseFloat($('.grn_sub_total_amt').val()) : 0;
    if (dis == '1') {
        var discount_amount = (parseFloat(item_total) * parseFloat(discount / 100));
        var Disamount = discount_amount.toFixed(2);
        $('.dis_per_to_amt').html('<b style="color: #3276B1;">Dis. Amount: ' + Disamount + '</b>');
        $('.dis_to_amount').val(discount_amount.toFixed(2));
        sub_total = parseFloat(item_total) - parseFloat(discount_amount);
    }
    else if (dis == '2') {
        sub_total = parseFloat(item_total) - parseFloat(discount_amount);
    }

    $('.discount_amt').val(sub_total.toFixed(2));


    $('.hidden_grnt_percentage').each(function () {
        var tax_id = $(this).parents('.separate_row').find('.hidden_tax_id').val();
        var hidden_percentage_amount = ($(this).val() != '') ? parseFloat($(this).val()) : 0;
        var percentage_amount = ($(this).parents('.separate_row').find('.grnt_percentage').val() != '') ? parseFloat($(this).parents('.separate_row').find('.grnt_percentage').val()) : 0;
        var po_sub_amount = ($(this).parents('.separate_row').find('.hidden_po_amount').val() != '') ? parseFloat($(this).parents('.separate_row').find('.hidden_po_amount').val()) : 0;
        var checkbox_val = $(this).parents('.separate_row').find('.grnt_type:checked').val();
        var sub_total = ($('.discount_amt').val() != '') ? parseFloat($('.discount_amt').val()) : 0;
        if (percentage_amount != '') {
            if (checkbox_val == '1') {
                var tax_amount = (parseFloat(sub_total) * parseFloat(percentage_amount / 100));
            }
            else if (checkbox_val == '2') {
                var tax_amount = (parseFloat(hidden_percentage_amount) / parseFloat(po_sub_amount)) * parseFloat(sub_total);
                $(this).parents('.separate_row').find('.grnt_percentage').val(tax_amount.toFixed(2));
            }
            $(this).parents('.separate_row').find('.grnt_amt').val(tax_amount.toFixed(2));
            getnettotalval();
        }

    });

}*/