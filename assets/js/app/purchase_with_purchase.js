$(document).ready(function () {   
    
    $(document).on('change', '.item_name', function () {
        var selling_price = $(this).find(':selected').data("item-price");
        $(this).parents('.items').find('.oprice').val(selling_price);
    });
    
    $.validator.addMethod('item_required', function (value, element) { 
        return (value != '') ? true : false;
    }, 'Please select Item Name!');
    
    
    $.validator.addMethod('offer_price', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please enter Offered Price!');
    
    
    $.validator.addClassRules({
        item_name: {
            item_required: true
        },
        price: {
            offer_price : true
        }
        
    });
 

    var companyForm = $("#pwp_form").validate({
        // Rules for form validation
        rules: {
            purchase_item: {
                required: true,
            },
        },
        // Messages for form validation
        messages: {
            purchase_item: {
                required: 'Please select item',
            },
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

    var addRowIndex = 0;
    /* Add row functionality for Branch */
    $(document).on('click', '.items .add-row', function () {
        addRowIndex++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.items').clone();
        //itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');
        itemObj.find('.img_preview').remove();


        itemObj.find('input, select, textarea').each(function (index, element) {

            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndex + ']');
                $(element).parent().find('.is_default').val(1);
            }

            if ($(this).attr('type') == 'radio') {
                $(this).prop('checked', false);
            }

            $(element).val('');
            if ($(element).prop('tagName').toLowerCase() == 'select') {
                if ($(element).prev().hasClass('select2')) {
                    $(element).prev().remove();
                    $(element).select2({width: '100%'});
                }
            }

            $(element).parent().find('.is_default').val(1);

        });

        $(itemObj).find('.section-group').append('<span class="img_preview" style="float: right;"></span>');

        var materialContainer = $(this).parents('.material-container');
        if ($(this).parent().find('.remove-row').length)
            $(this).remove();
        else
            $(this).removeClass('add-row fa-plus-circle').addClass('remove-row fa-minus-circle');

        if ($(itemObj).find('.remove-row').length == 0)
            $(itemObj).find('.add-row').after('<i class="fa fa-2x fa-minus-circle remove-row"></i>');

        $(materialContainer).find('.items:last').after(itemObj);
    });


    /* Remove row functionality for Branches */
    $(document).on('click', '.items .remove-row', function () {
        var materialContainer = $(this).parents('.material-container');
        $(this).parents('.items').remove();
        if (materialContainer.find('.items:last').find('.add-row').length == 0) {
            materialContainer.find('.items:last').find('.remove-row').before('<i class="fa fa-2x fa-plus-circle add-row"></i>');
        }

        if ((materialContainer.find('.items').length == 1) && (materialContainer.find('.items:last').find('.add-row').length)) {
            materialContainer.find('.items:last').find('.remove-row').remove();
        }
    });

    $('#customer_name').focus();
});
