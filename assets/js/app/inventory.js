var items;
$(document).ready(function () {
    if ($("#inv_form").length) {
	var ajaxURL = baseUrl + 'adjust_stock/get_items_by_branch_id';
	var ajaxData = {
		branch_id:$('select[name="branch_name"]').val()
	}
	items = ajaxRequest(ajaxURL, ajaxData, '');
    }
    
    var companyForm = $("#inv_form").validate({
        // Rules for form validation
        rules: {
            branch_name: {
                required: true
            },
            product_id: {
                required: true,
            },
            quantity: {
                required: true,
            },
        },
        // Messages for form validation
        messages: {
            branch_name: {
                required: 'Please select Branch Name'
            },
            product_id: {
                required: 'Please select item',
            },
            quantity: {
                required: 'Please select enter quantity',
            },
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
	
	$('#product_id').on('change', function() {
		if ($(this).val() != '') 
			$('.current-stock').html($(this).find('option:selected').data('available_qty'));
		else 
			$('.current-stock').html(0);
	});
	
	$('select[name="branch_name"]').on('change', function() {
		var _option = '<option value="">Select Item</option>';
		for(var i in items) {
			var item = items[i];
			var available_qty = 0;
			for(var j in item.branches) {
				var branch_product = item.branches[j];
				if (branch_product.brn_id == $(this).val()) {
					available_qty = branch_product.available_qty;
					break;
				}
			}
			
			_option += '<option value="' + item.product_id + '" data-available_qty="' + available_qty + '">' + item.pdt_code + ' / ' + item.pdt_name + '</option>';
		}
		
		$('#product_id').html(_option);
		$('#product_id').trigger('change');
	});
	
	$('select[name="branch_name"]').trigger('change');
	$('input[name="search_item"]').autocomplete({
		source: items,
		focus: function (event, ui) {
			//$(this).val(ui.item.value);
			//return false;
		},
		select: function (event, ui) {
			$('#product_id').select2('val', ui.item.product_id);
			$('#product_id').trigger('change');
			return true;
		}
	});
        
        $(document).on('blur, keyup', 'input.search_item', function (e) {
            var item = {};
            var tmpval = '';
            var product_code = $(this).val().trim();
            if (product_code != '') {
                for (var i in items) {
                        var s_no_array = new Array();
                            if (items[i].s_no != '')
                                    s_no_array = items[i].s_no.split(',');
                                
                    if ((product_code != '') && (items[i].label.trim() == product_code || items[i].bar_code.trim() == product_code || ($.inArray(product_code, s_no_array) !== -1))) {
                        item = items[i];
                        $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: item});
                        $('input.search_item').val(tmpval);
                        break;
                    }
                }
            }
        });
});