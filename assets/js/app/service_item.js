function selected_item_ids(){
    var item_arr = new Array();
    var inc = 0;
    $('.exchange_order_id').each(function(index, element){
        if($(this).val() != '') {
            item_arr[inc]=$(this).val();
            inc++;
        }
    });
    return item_arr;
}
function reset_select_option() {
    var arr_itm = selected_item_ids();
    $('select.exchange_order_id').each(function(e){

        $(this).find('option').each(function(index, element){
            $(element).prop('disabled', false);
        });

        var thisval = $(this).val();  
        $(this).find('option').each(function(index, element){
            if(($.inArray( $(element).val(), arr_itm ) != -1) && ($(element).val() != thisval)) {
                  $(element).prop('disabled', true);
              }
        });
      });
}

function selected_exterser_item_ids(){
    var item_arr = new Array();
    var inc = 0;
    $('.external_service_id').each(function(index, element){
        if($(this).val() != '') {
            item_arr[inc]=$(this).val();
            inc++;
        }
    });
    return item_arr;
}
function reset_exterser_select_option() {
    var arr_itm = selected_exterser_item_ids();
    $('select.external_service_id').each(function(e){

        $(this).find('option').each(function(index, element){
            $(element).prop('disabled', false);
        });

        var thisval = $(this).val();  
        $(this).find('option').each(function(index, element){
            if(($.inArray( $(element).val(), arr_itm ) != -1) && ($(element).val() != thisval)) {
                  $(element).prop('disabled', true);
              }
        });
      });
}

function calculate_total() {

    var sub_total = 0;
    $('.amount').each(function (index, element) {
        sub_total += ($(element).val() != '') ? parseFloat($(element).val()) : 0;
    });
    $('.sub_total').val(sub_total.toFixed(2));
    var total_amount = sub_total;
    $('.total_amt').val(total_amount.toFixed(2));
    $('form[name="service_item"]').validate().element('.total_amt');
}

function calculate_item_total(_this) {
    var itemsCol = $(_this).closest('.calculate_items');
    var quantity = (itemsCol.find('.quantity').val() != '') ? parseFloat(itemsCol.find('.quantity').val()) : 0;
    var price = (itemsCol.find('.price').val() != '') ? parseFloat(itemsCol.find('.price').val()) : 0;
    var item_total = parseFloat((quantity * price).toFixed(2));
    itemsCol.find('.amount').val(item_total.toFixed(2));

    calculate_total();
}

function calculate_external_total() {

    var sub_total = 0;
    $('.external_sub_amount').each(function (index, element) {
        sub_total += ($(element).val() != '') ? parseFloat($(element).val()) : 0;
    });
    $('.total_exter_amt').val(sub_total.toFixed(2));
    $('form[name="exchangeorder_form"]').validate().element('.total_exter_amt');
}

function calculate_external_item_total(_this) {
    var itemsCol = $(_this).closest('.exter_items');
    var external_cost = (itemsCol.find('.external_cost').val() != '') ? parseFloat(itemsCol.find('.external_cost').val()) : 0;
    var our_cost = (itemsCol.find('.our_cost').val() != '') ? parseFloat(itemsCol.find('.our_cost').val()) : 0;
    var item_total = parseFloat((external_cost + our_cost).toFixed(2));
    itemsCol.find('.external_sub_amount').val(item_total.toFixed(2));

    calculate_external_total();
}

function hide_product_code_msg() {
    $('.product_code_label').slideUp();
}

function hide_error_description_msg() {
    $('.error_description').slideUp();
}

function get_all_items_descriptions() {
    var ajaxURL = baseUrl + 'invoice/search_item_description';
    var ajaxData = {}

    return ajaxRequest(ajaxURL, ajaxData, '');
}

function get_all_items(product_code) {
    var ajaxURL = baseUrl + 'invoice/search_item';
    var ajaxData = {
        product_code: product_code
    }

    return ajaxRequest(ajaxURL, ajaxData, '');
}

function get_all_exc_items(_this) {
    var ajaxURL = baseUrl + 'invoice/search_exchange_item';
    var exchange_id = $(_this).closest('.exchange_order_items').find("select.exchange_order_id").val();
    var exchange_id_arr = [];
    if(exchange_id != '') {
        exchange_id_arr.push(exchange_id);
    }
    var frm_name = $('#service_item_form').hasClass('edit_form') ? 'edit' : 'add';
    var ser_edit_id = (frm_name == 'edit') ? $('.ser_edit_id').val() : '';
    var edit_page = (frm_name == 'edit') ? 'service' : '';
    var ajaxData = {
        exchange_id: exchange_id_arr,
        edit_page_id: ser_edit_id,
        edit_page: edit_page
    }

    return ajaxRequest(ajaxURL, ajaxData, '');
}

function valid_exc_item_code(item_code) {
	var item_code_exists = false;
        exc_product_codes = get_all_exc_items(item_code);
	for(var i in exc_product_codes) {
		if (exc_product_codes[i].product_text.trim().toLowerCase() == item_code.val().trim().toLowerCase()) {
			item_code_exists = true;
			break;
		}
	}
	
	if (!item_code_exists) {
		item_code.val('');
		item_code.focus();
		return false;
	}
}

function set_exc_product_autocomplete() {
    
    $('.exchange_order_id').each(function(index1, element1){
        exc_product_codes = get_all_exc_items($(this));
        
        $(this).closest('.exchange_order_items').find('.exc_product_code').each(function () {
            
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }
		
        $(this).on('change', function() {
			valid_exc_item_code($(this));
        });
        
        $(this).autocomplete({
            source: exc_product_codes,
            focus: function (event, ui) {
            },
            select: function (event, ui) {
                var items = $(this).closest('.exc-items');
                if ($('.exc_product_details').find('.exc_product_id[value="' + ui.item.id + '"]').length) {
                    // Item already exists
                    if ($(this).data('value').trim() != ui.item.value.trim()) {
                        items = $('.exc_product_details').find('.exc_product_id[value="' + ui.item.id + '"]').closest('.exc-items');
                        $(this).val('');
                        $(this).closest('.exc-items').find('.product_code_label').slideDown();
                        setTimeout(hide_product_code_msg, 3000)
                        return false;
                    } else {
                        $(this).val(ui.item.value.trim());
                    }
                } else {
                    $(this).data('value', ui.item.product_text.trim());
                    $(this).val(ui.item.product_text.trim());
                    items.find('.exc_product_id').val(ui.item.id);
                    items.find('.exc_product_code').data('value', ui.item.product_text.trim());
                    items.find('.exc_product_description').val(ui.item.product_description.trim());
                    var selling_price = (ui.item.exr_price != '') ? parseFloat(ui.item.exr_price) : 0;
                    items.find('.price').val(selling_price.toFixed(2));
                    var exr_qty = (ui.item.exr_quantity != '') ? parseFloat(ui.item.exr_quantity) : 0;
                    var invoice_qty = (ui.item.invoice_qty != '' && ui.item.invoice_qty != null) ? parseFloat(ui.item.invoice_qty) : 0;
                    var service_qty = (ui.item.service_qty != '' && ui.item.service_qty != null) ? parseFloat(ui.item.service_qty) : 0;
                    var used_qty = parseFloat(invoice_qty) + parseFloat(service_qty);
                    var exr_quantity = parseFloat(exr_qty) - parseFloat(used_qty);
                    var sub_totaltxt = (ui.item.total != '') ? parseFloat(ui.item.total) : 0;
                    var cost_txt = (parseFloat(sub_totaltxt) / parseFloat(exr_quantity));
                    var amount_txt = (parseFloat(selling_price) * parseFloat(exr_quantity));
                    items.find('.cost').val(cost_txt.toFixed(2));
                    items.find('.quantity').val(exr_quantity);
                    items.find('.amount').val(amount_txt.toFixed(2));
                }
                calculate_total();
                
                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || e.keyCode;
                if (charCode == 13) 
                    items.find('.quantity').focus();
                else if (event.type == 'autocompleteselect') 
                    items.find('.product_description').focus();
                
                /* To focus quantity of particular item end */
                
                return false;
            }
        });

        $(this).addClass('hasAutocomplete');
    });
    });
    
}

function reset_exc_ajaxrow(_this){
         $(_this).closest('.exchange_order_items').find('.exc-items').each(function (index, element) {
               if(index == 0) {
                    var temp = '';
                    $(this).find('.exc_product_id').val(temp);
                    $(this).find('.exc_product_code').val(temp);
                    $(this).find('.exc_product_code').data('value','');
                    $(this).find('.exc_product_description').val(temp);
                    $(this).find('.exc_quantity').val(temp);
                    $(this).find('.exc_price').val(temp);
                    $(this).find('.exc_cost').val(temp);
                    $(this).find('.exc_amount').val(temp);
                    
                    if ($(this).find('.exc_product_code').hasClass('hasAutocomplete'))
                        $(this).find('.exc_product_code').removeClass('hasAutocomplete');
                    
                    if (active == 'edit') {     
                        $(this).find('.ser_exc_itemid').remove();
                        $(this).find('input, select, textarea').each(function (index1, element1) {
                            var tmp_name = ($(this).prop('name').indexOf('new') == -1) ? 'new_' + $(this).prop('name') : $(this).prop('name');
                            $(this).prop('name', tmp_name);
                        });
                    }
                    
               } else {
                    var materialContainer = $(this).parents('.material-container');
                    $(this).remove();
                    if (materialContainer.find('.exc-items:last').find('.add-row').length == 0) {
                        materialContainer.find('.exc-items:last').find('.remove-row').before('<i class="fa fa-2x fa-plus-circle add-row"></i>');
                    }

                    if ((materialContainer.find('.exc-items').length == 1) && (materialContainer.find('.exc-items:last').find('.add-row').length)) {
                        materialContainer.find('.exc-items:last').find('.remove-row').remove();
                    }
                    calculate_total();
               }
          });
          exc_ajaxrow_update(_this);
}

function exc_ajaxrow_update(_this) {
        exc_product_codes = get_all_exc_items(_this);
        var rwcount = exc_product_codes.length;
        for(rw=1;rw<rwcount;rw++){
            $(_this).closest('.exchange_order_items').find( ".add-row" ).trigger( "click" );
        }
        exc_product_codes = get_all_exc_items(_this);
        $(_this).closest('.exchange_order_items').find('.exc-items').each(function (index, element) {
            var temp = '';
            $(this).find('.exc_product_id').val(exc_product_codes[index].id.trim());
            $(this).find('.exc_product_code').val(exc_product_codes[index].product_text.trim());
            $(this).find('.exc_product_code').data('value',exc_product_codes[index].product_text.trim());
            $(this).find('.exc_product_description').val(exc_product_codes[index].product_description.trim());
            var selling_price = (exc_product_codes[index].exr_price != '') ? parseFloat(exc_product_codes[index].exr_price) : 0;
            var exr_qty = (exc_product_codes[index].exr_quantity != '') ? parseFloat(exc_product_codes[index].exr_quantity) : 0;
            var invoice_qty = (exc_product_codes[index].invoice_qty != '' && exc_product_codes[index].invoice_qty != null) ? parseFloat(exc_product_codes[index].invoice_qty) : 0;
            var service_qty = (exc_product_codes[index].service_qty != '' && exc_product_codes[index].service_qty != null) ? parseFloat(exc_product_codes[index].service_qty) : 0;
            var used_qty = parseFloat(invoice_qty) + parseFloat(service_qty);
            var exr_quantity = parseFloat(exr_qty) - parseFloat(used_qty);
            var sub_totaltxt = (exc_product_codes[index].total != '') ? parseFloat(exc_product_codes[index].total) : 0;
            var cost_txt = (parseFloat(sub_totaltxt) / parseFloat(exr_quantity));
            var amount_txt = (parseFloat(selling_price) * parseFloat(exr_quantity));
            $(this).find('.exc_quantity').val(exr_quantity);
            $(this).find('.exc_price').val(selling_price.toFixed(2));
            $(this).find('.exc_cost').val(cost_txt.toFixed(2));
            $(this).find('.exc_amount').val(amount_txt.toFixed(2));
        });
        
}

var items = get_all_items('');
var product_codes = get_all_items(1);
var descriptions = get_all_items_descriptions('');

var exc_product_codes;

var addRowIndextop = 0;
var addRowIndex = 0;
var addRowIndexinvn = 0;
var active = $('.exchange_order_id').parents('form').hasClass('add_form') ? 'add' : 'edit';

var scanItem = 0;

function valid_item_code(item_code) {
    var item_code_exists = false;
    for (var i in product_codes) {
        if (product_codes[i].pdt_code.trim().toLowerCase() == item_code.val().trim().toLowerCase()) {
            item_code_exists = true;
            break;
        }
    }

    if (!item_code_exists) {
        item_code.val('');
        item_code.focus();
        return false;
    }
}

function reset_item() {
    product_codes = get_all_items(1);
    descriptions = get_all_items_descriptions('');
    $(".product_code, .product_description").removeClass('hasAutocomplete');
    set_product_autocomplete();
}

function set_product_autocomplete() {
    $(".product_code").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }

        $(this).on('change', function () {
            valid_item_code($(this));
        });

        $(this).autocomplete({
			source: function(request, response) {
				var results = $.ui.autocomplete.filter(product_codes, request.term);
				response(results.slice(0, 15));
			},
            focus: function (event, ui) {
				show_available_quantity(event.currentTarget, ui.item.branch_products);
            },
			close: function() {
				hide_available_quantity();
				if ($(this).val().trim().length) 
					$(this).closest('.items').find('.product_description').trigger('focus');
			},
            select: function (event, ui) {
                var items = $(this).closest('.items');
                var product_id_name = items.find('.product_id').prop('name');
                if ($('.product_details').find('.product_id[value="' + ui.item.id + '"]:not(.product_id[name="' + product_id_name + '"])').length) {
                    // Item already exists
                    if ($(this).data('value').trim() != ui.item.value.trim()) {
                        items = $('.product_details').find('.product_id[value="' + ui.item.id + '"]').closest('.items');

                        $(this).val('');
                        $(this).closest('.items').find('.product_code_label').slideDown();
                        setTimeout(hide_product_code_msg, 3000)
                        return false;
                    }
                    else {
                        $(this).val(ui.item.value.trim());
                        des = ui.item.pdt_name;
                        if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                            des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                        }
                        items.find('.product_description').val(des.trim());
                    }
                }
                else {
                    $(this).data('value', ui.item.pdt_code.trim());
                    $(this).val(ui.item.pdt_code.trim());
//                    alert(ui.item.cat_id);
                    items.find('.category_id').val(ui.item.cat_id);
                    items.find('.product_id').val(ui.item.id);
//                    items.find('.brand_name').html(ui.item.cat_name);
//                    items.find('.brand_label').slideDown();
//                    items.find('.product_description').val(ui.item.pdt_name);
                    des = ui.item.pdt_name;
                    if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                        des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                    }
                    items.find('.product_code').data('value', ui.item.pdt_code.trim());
                    items.find('.product_description').val(des.trim());
                    var selling_price = (ui.item.selling_price != '') ? parseFloat(ui.item.selling_price) : 0;
                    items.find('.price').val(selling_price.toFixed(2));
                    if (items.find('.cost').length) {
                        var buying_price = (ui.item.buying_price != '') ? parseFloat(ui.item.buying_price) : 0;
                        items.find('.cost').val(buying_price.toFixed(2));
                    }
                    items.find('.quantity').val(1);
                    items.find('.amount').val(selling_price.toFixed(2));
                }
                calculate_total();

                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || event.keyCode;
                if (charCode == 13)
                    items.find('.quantity').focus();
                else if (event.type == 'autocompleteselect')
                    items.find('.product_description').focus();

                /* To focus quantity of particular item end */
                return false;
            }
        });

        $(this).addClass('hasAutocomplete');
    });

    $(".product_description").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }

        $(this).autocomplete({
            source: function(request, response) {
				var results = $.ui.autocomplete.filter(descriptions, request.term);
				response(results.slice(0, 15));
			},
            focus: function (event, ui) {
				show_available_quantity(event.currentTarget, ui.item.branch_products);
            },
			close: function() {
				hide_available_quantity();
			},
            select: function (event, ui) {
                //alert('test');
                var items = $(this).closest('.items');
                var product_id_name = items.find('.product_id').prop('name');
                if ($('.product_details').find('.product_id[value="' + ui.item.id + '"]:not(.product_id[name="' + product_id_name + '"])').length) {
                    // Item already exists
                    if ($(this).data('value').trim() != ui.item.value.trim()) {
                        items = $('.product_details').find('.product_id[value="' + ui.item.id + '"]').closest('.items');
                        $(this).val($(this).data('value').trim());
                        $(this).closest('.items').find('.error_description').slideDown();
                        setTimeout(hide_error_description_msg, 3000);
                        items.find('.quantity').focus();
                        //return false;
                    }
                    else {
                        $(this).val(ui.item.value.trim());
                    }
                }
                else {
                    items.find('.product_code').val(ui.item.pdt_code.trim());
                    //$(this).val(ui.item.pdt_code);
                    items.find('.category_id').val(ui.item.cat_id);
                    items.find('.product_id').val(ui.item.id);
//                    items.find('.brand_name').html(ui.item.cat_name);
//                    items.find('.brand_label').slideDown();
//                    items.find('.product_description').val(ui.item.pdt_name);

                    if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                        des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                    }
                    else {
                        des = ui.item.pdt_name;
                    }
                    des = des.trim();
                    items.find('.product_description').data('value', des);
                    items.find('.product_description').val(des);
                    var selling_price = (ui.item.selling_price != '') ? parseFloat(ui.item.selling_price) : 0;
                    items.find('.price').val(selling_price.toFixed(2));
                    if (items.find('.cost').length) {
                        var buying_price = (ui.item.buying_price != '') ? parseFloat(ui.item.buying_price) : 0;
                        items.find('.cost').val(buying_price.toFixed(2));
                    }
                    items.find('.quantity').val(1);
                    items.find('.amount').val(selling_price.toFixed(2));
                }
                calculate_total();

                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || event.keyCode;
                if (charCode == 13)
                    items.find('.quantity').focus();
                else if (event.type == 'autocompleteselect')
                    items.find('.quantity').focus();

                /* To focus quantity of particular item end */
                return true;
            }
        });

        $(this).addClass('hasAutocomplete');
    });
}

$(document).ready(function () {
    
    selected_item_ids();
    reset_exterser_select_option();
    
    $(document).on('change', 'select.exchange_order_id', function () {   
        reset_exc_ajaxrow($(this));
        reset_select_option();
        calculate_total();
        set_exc_product_autocomplete();
    });
    
    $(document).on('change', 'select.external_service_id', function () {   
        reset_exterser_select_option();
        get_exter_service_data($(this));
        calculate_external_total();
        $('form[name="exchangeorder_form"]').validate().element($(this));
        $('form[name="exchangeorder_form"]').validate().element($(this).closest('.exter_items').find('.external_cost'));
    });

    if (active == 'edit') {     
        reset_select_option();
    }

    set_exc_product_autocomplete();
    set_product_autocomplete();
    var pr_items = '';
    
	$(document).on('focusout', 'input.product_description', function (e) {
        hide_available_quantity();
    });
	
    $(document).on('focus', 'input.product_code, input.product_description', function (e) {
        var item = {};
        var product_code = $(this).closest('.items').find('.product_code').val().trim();
        if (product_code != '') {
            for (var i in product_codes) {
                if ((product_code != '') && (product_codes[i].label.trim() == product_code)) {
                    item = product_codes[i];
                    show_available_quantity($(this), product_codes[i].branch_products);
                    break;
                }
            }
        } else {
            hide_available_quantity();
        }
    });
	
    $(document).on('blur', 'input.product_code', function (e) {
        var item = {};
        var product_code = $(this).val().trim();
        if (product_code != '') {
            for (var i in product_codes) {
                if ((product_code != '') && (product_codes[i].label.trim() == product_code)) {
                    item = product_codes[i];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: item});
                    break;
                }
            }
        }
		else {
			hide_available_quantity();
		}
    });
    
	/*$(document).on('blur', 'input.product_description', function (e) {
        var item = {};
        var product_description = $(this).val().trim();
        if (product_description != '') {
            for (var i in descriptions) {
                if ((product_description != '') && (descriptions[i].label.trim() == product_description)) {
                    item = descriptions[i];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: item});
                    break;
                }
            }
        }
    });*/

    $(document).on('blur', 'input.exc_product_code', function (e) {
        var item = {};
        var product_code = $(this).val().trim();
        exc_product_codes = get_all_exc_items($(this));
        if (product_code != '') {
            for (var i in exc_product_codes) {
                if ((product_code != '') && (exc_product_codes[i].label.trim() == product_code)) {
                    item = exc_product_codes[i];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: item});
                    break;
                }
            }
        }
    });

    $(document).on('keyup', 'input.search_product', function (e) {
        var ser = $(this).val();
        var _this = $(this);
        if (ser) {
            $.each(items, function (key, value) {
				var s_no_array = new Array();
				if (value.s_no != '')
					s_no_array = value.s_no.split(',');
				
                if ((ser.toUpperCase() === value.bar_code.toUpperCase()) || ($.inArray(ser, s_no_array) !== -1) || (ser.toUpperCase() == value.pdt_code.toUpperCase())) {
                    if ($('.product_details').find('.product_id[value="' + value.id + '"]').length) {
                        // Item already exists
                        var items = $('.product_details').find('.product_id[value="' + value.id + '"]').closest('.items');
                        var quantity = (items.find('.quantity').val() != '') ? parseFloat(items.find('.quantity').val()) : 0;
                        quantity += 1;
                        items.find('.quantity').val(quantity);
                        var price = (items.find('.price').val() != '') ? parseFloat(items.find('.price').val()) : 0;
                        items.find('.amount').val((price * quantity).toFixed(2));
                    }
                    else {
                        if ($('.product_details .items:last .product_id').val() != '') {
                            // Last row occupied by another product.
                            $('.product_details .items:last .inv-add-row').trigger('click');
                            $('input.search_product').focus();
                        }
                        var items = $('.product_details .items:last');
                        items.find('.category_id').val(value.cat_id);
                        items.find('.product_id').val(value.id);
                        items.find('.product_code').val(value.pdt_code);
                        //items.find('.brand_name').html(value.cat_name);
                        //items.find('.brand_label').slideDown();
                        des = value.pdt_name;
                        if (value.cat_name != '' && value.cat_name != null) {
                            des = value.cat_name + ' ' + value.pdt_name;
                        }
                        items.find('.product_description').val(des);
						
                        var selling_price = (value.selling_price != '') ? parseFloat(value.selling_price) : 0;
                        items.find('.price').val(selling_price.toFixed(2));
                        if (items.find('.cost').length) {
                            var buying_price = (value.buying_price != '') ? parseFloat(value.buying_price) : 0;
                            items.find('.cost').val(buying_price.toFixed(2));
                        }

                        items.find('.quantity').val(1);
                        items.find('.amount').val(selling_price.toFixed(2));
                    }
                    
                    calculate_total();
                    $(_this).val('');
                    $(_this).focus();
                    scanItem = 1;
                    return false;
                }
            });
        }
    });

    $(document).on('focusin', ".quantity, .price, .cost", function () {
        $(this).select();
    });

    $(document).on('keyup', '.quantity, .price', function () {
        calculate_item_total($(this));
    });
    
    $(document).on('keyup', '.external_cost, .our_cost', function () {
        calculate_external_item_total($(this));
    });
    
    $(document).on('keyup', '.serv_amount', function () {
        calculate_total();
    });
    
    
    addRowIndexServ = $('.service-items').length - 1;
    /* Add row functionality for Service Cost */
    $(document).on('click', '.service-items .serv-add-row', function () {
        addRowIndexServ++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.service-items').clone();
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');

        itemObj.find('input, select, textarea').each(function (index, element) {
            
            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndexServ + ']');
            }
            $(element).val('');

        });

        var materialContainer = $(this).parents('.service-material-container');
       
        if ($(this).parent().find('.serv-remove-row').length)
            $(this).remove();
        else
            $(this).removeClass('serv-add-row fa-plus-circle').addClass('serv-remove-row fa-minus-circle');

        if ($(itemObj).find('.serv-remove-row').length == 0)
            $(itemObj).find('.serv-add-row').after('<i class="fa fa-2x fa-minus-circle serv-remove-row"></i>');

        $(materialContainer).find('.service-items:last').after(itemObj);
        
        $('.service-items:last .serv_product_code').focus();
    });

    /* Remove row functionality for Service Cost */
    $(document).on('click', '.service-items .serv-remove-row', function () {
        var materialContainer = $(this).parents('.service-material-container');
        $(this).parents('.service-items').remove();
        if (materialContainer.find('.service-items:last').find('.serv-add-row').length == 0) {
            materialContainer.find('.service-items:last').find('.serv-remove-row').before('<i class="fa fa-2x fa-plus-circle serv-add-row"></i>');
        }

        if ((materialContainer.find('.service-items').length == 1) && (materialContainer.find('.service-items:last').find('.serv-add-row').length)) {
            materialContainer.find('.service-items:last').find('.serv-remove-row').remove();
        }
        calculate_total();
        $('.service-items:last .serv_product_code').focus();
    });
    
    
    addRowIndextop = $('.exchange_order_items').length - 1;
    /* Add row functionality for material */
    $(document).on('click', '.exchange_order_items .exc-add-row-top', function () {
//        addRowIndextop++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.exchange_order_items').clone();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');

        itemObj.find('input, select, textarea').each(function (index, element) {
            if ($(element).hasClass('item_hidden_ids')) {
                $(element).remove();
            }

            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                var splElem = $(element).prop('name').split('[');
                var splElemTwo = splElem[1].split(']');
                 addRowIndextop = parseInt(splElemTwo[0]) + 1;
                if(splElem.length==2) {
                var element_name = name+'['+ addRowIndextop +']';
                } else if(splElem.length==3) {
                var element_name = name+'['+ addRowIndextop +'][0]';    
                } 
                $(element).prop('name', element_name);
            }
            $(element).val('');

            if ($(element).prop('tagName').toLowerCase() == 'select') {
            if ($(element).prev().hasClass('select2')) {
             $(element).prev().remove();
             $(element).select2({width: '100%'});
            }
            }

            if ($(element).hasClass('product_description'))
                $(element).data('value', '');

            if ($(element).hasClass('hasAutocomplete'))
                $(element).removeClass('hasAutocomplete');

        });

        
        var optionsLength = $('#exchange_order_id option[value!=""]').length;
        if (optionsLength > $('.exchange_order_items').length) {
        itemObj.find('.exc-items').each(function (index, element) {
            if(index > 0) {
                $(element).remove();
            } else {
                $(element).find('.remove-row').removeClass('remove-row fa-minus-circle').addClass('add-row fa-plus-circle');
            }
        });

        if ($(this).parent().find('.exc-remove-row-top').length)
            $(this).remove();
        else
            $(this).removeClass('exc-add-row-top fa-plus-circle').addClass('exc-remove-row-top fa-minus-circle');

        if ($(itemObj).find('.exc-remove-row-top').length == 0)
            $(itemObj).find('.exc-add-row-top').after('<i class="fa fa-2x fa-minus-circle exc-remove-row-top"></i>');

        $('.exchange_order_items:last').after(itemObj);
        
        } else if ((optionsLength == $('.exchange_order_items').length) || (optionsLength == 0)) {
            
            var smallBoxTitle = 'Sorry ! ...';
            var smallBoxMsg = '<i class="fa fa-info-circle"></i> <i>You can\'t add more...<br> Because you have';
            smallBoxMsg += (optionsLength > 0) ? ' only ' + optionsLength : ' no';
            smallBoxMsg += ($('.exchange_order_items').length > 1) ? ' items' : ' item';
            smallBoxMsg += '.</i>';

            if ($('#divSmallBoxes').html().length == 0)
                showSmallBox(smallBoxTitle, smallBoxMsg);
        }
        
        reset_select_option();
        set_exc_product_autocomplete();
        $('.exchange_order_items:last .product_code').focus();
    });

    /* Remove row functionality for material */
    $(document).on('click', '.exchange_order_items .exc-remove-row-top', function () {
        $(this).parents('.exchange_order_items').remove();
        if ($('.exchange_order_items:last').find('.exc-add-row-top').length == 0) {
            $('.exchange_order_items:last').find('.exc-remove-row-top').before('<i class="fa fa-2x fa-plus-circle exc-add-row-top"></i>');
        }

        if (($('.exchange_order_items').length == 1) && ($('.exchange_order_items:last').find('.exc-add-row-top').length)) {
            $('.exchange_order_items:last').find('.exc-remove-row-top').remove();
        }
        reset_select_option();
        calculate_total();
        $('.exchange_order_items:last .product_code').focus();
    });

    addRowIndex = $('.exc-items').length - 1;
    /* Add row functionality for exchange order */
    $(document).on('click', '.exc-items .add-row', function () {
//        addRowIndex++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.exc-items').clone();
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');

        itemObj.find('input, select, textarea').each(function (index, element) {
            if ($(element).hasClass('item_hidden_ids')) {
                $(element).remove();
            }

            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                var splElem = $(element).prop('name').split('[');
                var splElemTwo = splElem[1].split(']');    
                var splElemThree = splElem[2].split(']');
                    addRowIndex = parseInt(splElemThree[0]) + 1;
                if(splElem.length==3) {
                var element_name = name+'['+ splElemTwo[0] +']['+ addRowIndex +']';    
                }
                $(element).prop('name', element_name);
            }
            $(element).val('');

            if ($(element).hasClass('product_description'))
                $(element).data('value', '');

            if ($(element).hasClass('hasAutocomplete'))
                $(element).removeClass('hasAutocomplete')

        });

        
        var materialContainer = $(this).parents('.material-container');
        exc_product_codes = get_all_exc_items($(this));
        var optionsLength = exc_product_codes.length;
        if (optionsLength > $(materialContainer).find('.exc-items').length) {
        if ($(this).parent().find('.remove-row').length)
            $(this).remove();
        else
            $(this).removeClass('add-row fa-plus-circle').addClass('remove-row fa-minus-circle');

        if ($(itemObj).find('.remove-row').length == 0)
            $(itemObj).find('.add-row').after('<i class="fa fa-2x fa-minus-circle remove-row"></i>');

        $(materialContainer).find('.exc-items:last').after(itemObj);
        }
        else if ((optionsLength == $(materialContainer).find('.exc-items').length) || (optionsLength == 0)) {
            var smallBoxTitle = 'Sorry ! ...';
            var smallBoxMsg = '<i class="fa fa-info-circle"></i> <i>You can\'t add more...<br> Because you have';
            smallBoxMsg += (optionsLength > 0) ? ' only ' + optionsLength : ' no';
            smallBoxMsg += ($(materialContainer).find('.exc-items').length > 1) ? ' items' : ' item';
            smallBoxMsg += '.</i>';

            if ($('#divSmallBoxes').html().length == 0)
                showSmallBox(smallBoxTitle, smallBoxMsg);
        }
        
        set_exc_product_autocomplete();
        $('.exc-items:last .product_code').focus();
    });

    /* Remove row functionality for exchange order */
    $(document).on('click', '.exc-items .remove-row', function () {
        var materialContainer = $(this).parents('.material-container');
        $(this).parents('.exc-items').remove();
        if (materialContainer.find('.exc-items:last').find('.add-row').length == 0) {
            materialContainer.find('.exc-items:last').find('.remove-row').before('<i class="fa fa-2x fa-plus-circle add-row"></i>');
        }

        if ((materialContainer.find('.exc-items').length == 1) && (materialContainer.find('.exc-items:last').find('.add-row').length)) {
            materialContainer.find('.exc-items:last').find('.remove-row').remove();
        }
        calculate_total();
        $('.exc-items:last .product_code').focus();
    });


    addRowIndexInven = $('.inv-items').length - 1;
    /* Add row functionality for Inventory */
    $(document).on('click', '.inv-items .inv-add-row', function () {
        addRowIndexInven++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.inv-items').clone();
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');
        itemObj.find('.brand_name').html('');
        itemObj.find('.brand_label').hide();

        itemObj.find('input, select, textarea').each(function (index, element) {
            if ($(element).hasClass('item_hidden_ids')) {
                $(element).remove();
            }

            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndexInven + ']');
            }
            $(element).val('');

            if ($(element).hasClass('product_description'))
                $(element).data('value', '');

            if ($(element).hasClass('hasAutocomplete'))
                $(element).removeClass('hasAutocomplete')

        });

        
        var materialContainer = $(this).parents('.inv-material-container');
        var optionsLength = items.length;
        if (optionsLength > $(materialContainer).find('.items').length) {
        if ($(this).parent().find('.inv-remove-row').length)
            $(this).remove();
        else
            $(this).removeClass('inv-add-row fa-plus-circle').addClass('inv-remove-row fa-minus-circle');

        if ($(itemObj).find('.inv-remove-row').length == 0)
            $(itemObj).find('.inv-add-row').after('<i class="fa fa-2x fa-minus-circle inv-remove-row"></i>');

        $(materialContainer).find('.inv-items:last').after(itemObj);
        }
        else if ((optionsLength == $(materialContainer).find('.items').length) || (optionsLength == 0)) {
            var smallBoxTitle = 'Sorry ! ...';
            var smallBoxMsg = '<i class="fa fa-info-circle"></i> <i>You can\'t add more...<br> Because you have';
            smallBoxMsg += (optionsLength > 0) ? ' only ' + optionsLength : ' no';
            smallBoxMsg += ($(materialContainer).find('.items').length > 1) ? ' items' : ' item';
            smallBoxMsg += '.</i>';

            if ($('#divSmallBoxes').html().length == 0)
                showSmallBox(smallBoxTitle, smallBoxMsg);
        }
        
        set_product_autocomplete();
        $('.inv-items:last .product_code').focus();
    });

    /* Remove row functionality for Inventory */
    $(document).on('click', '.inv-items .inv-remove-row', function () {
        var materialContainer = $(this).parents('.inv-material-container');
        $(this).parents('.inv-items').remove();
        if (materialContainer.find('.inv-items:last').find('.inv-add-row').length == 0) {
            materialContainer.find('.inv-items:last').find('.inv-remove-row').before('<i class="fa fa-2x fa-plus-circle inv-add-row"></i>');
        }

        if ((materialContainer.find('.inv-items').length == 1) && (materialContainer.find('.inv-items:last').find('.inv-add-row').length)) {
            materialContainer.find('.inv-items:last').find('.inv-remove-row').remove();
        }
        calculate_total();
        $('.inv-items:last .product_code').focus();
    });
    
    
    
    addRowIndexExter = $('.exter_items').length - 1;
    /* Add row functionality for Service Cost */
    $(document).on('click', '.exter_items .external-add-row', function () {
        addRowIndexExter++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.exter_items').clone();
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');

        itemObj.find('input, select, textarea').each(function (index, element) {
            
            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndexExter + ']');
            }
            $(element).val('');
            
            if ($(element).prop('tagName').toLowerCase() == 'select') {
                if ($(element).prev().hasClass('select2')) {
                $(element).prev().remove();
                $(element).select2({width: '100%'});
                }
            }
        });

        var materialContainer = $(this).parents('.exter-material-container');
        var optionsLength = $('#external_service_id option[value!=""]').length;
        if (optionsLength > $('.exter_items').length) {
        if ($(this).parent().find('.external-remove-row').length)
            $(this).remove();
        else
            $(this).removeClass('external-add-row fa-plus-circle').addClass('external-remove-row fa-minus-circle');

        if ($(itemObj).find('.external-remove-row').length == 0)
            $(itemObj).find('.external-add-row').after('<i class="fa fa-2x fa-minus-circle external-remove-row"></i>');

        $(materialContainer).find('.exter_items:last').after(itemObj);
        } else if ((optionsLength == $('.exter_items').length) || (optionsLength == 0)) {
            
            var smallBoxTitle = 'Sorry ! ...';
            var smallBoxMsg = '<i class="fa fa-info-circle"></i> <i>You can\'t add more...<br> Because you have';
            smallBoxMsg += (optionsLength > 0) ? ' only ' + optionsLength : ' no';
            smallBoxMsg += ($('.exter_items').length > 1) ? ' items' : ' item';
            smallBoxMsg += '.</i>';

            if ($('#divSmallBoxes').html().length == 0)
                showSmallBox(smallBoxTitle, smallBoxMsg);
        }
        reset_exterser_select_option();
        $('.exter_items:last .exter_service_desc').focus();
    });

    /* Remove row functionality for Service Cost */
    $(document).on('click', '.exter_items .external-remove-row', function () {
        var materialContainer = $(this).parents('.exter-material-container');
        $(this).parents('.exter_items').remove();
        if (materialContainer.find('.exter_items:last').find('.external-add-row').length == 0) {
            materialContainer.find('.exter_items:last').find('.external-remove-row').before('<i class="fa fa-2x fa-plus-circle external-add-row"></i>');
        }

        if ((materialContainer.find('.exter_items').length == 1) && (materialContainer.find('.exter_items:last').find('.external-add-row').length)) {
            materialContainer.find('.exter_items:last').find('.external-remove-row').remove();
        }
        reset_exterser_select_option();
        calculate_external_total();
        $('.exter_items:last .exter_service_desc').focus();
    });
    

    $.validator.addMethod('valid_amount', function (value, element) {
        return (parseFloat(value) >= 0) ? true : false;
    }, 'Invalid&nbsp;amount!');
    
    $.validator.addMethod('one_item', function (value, element) {
        var tem_val=0;
        $('.product_code_valcls').each(function(){
            if($(this).val()!=""){
                tem_val=1;
            }
        });
        return (tem_val == 1) ? true : false;
    }, 'Fill&nbsp;at&nbsp;least One&nbsp;Item !');

    var $orderForm = $("#service_item_form").validate({
        // Rules for form validation
        invalidHandler: function (event, validator) {
            $('body').animate({
                scrollTop: $(validator.currentForm).offset().top
            }, '500', 'swing');
        },
        rules: {
            total_amt: {
                valid_amount: true,
                one_item: true
            }
        },
        // Messages for form validation
        messages: {
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
            if (scanItem) {
                scanItem = 0;
                return false;
            }

            form.submit();
        }
    });
    
    
    
    
    $.validator.addMethod('external_service_id_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please select External Service!');

    $.validator.addMethod('external_cost_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please enter Service Cost!');

    $.validator.addClassRules({
        external_service_id: {
            external_service_id_required: true
        },
        external_cost: {
            external_cost_required: true
        }
    });

    $("#exchangeorder_form").validate({
        // Rules for form validation
        invalidHandler: function (event, validator) {
            $('body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top - 100
            }, '500', 'swing');
        },
        rules: {
            total_exter_amt: {
                required: true
            }
        },
        // Messages for form validation
        messages: {
            total_exter_amt: {
                required: 'Net Total should Not Empty!'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
    
    
});

function get_exter_service_data($_extrthis) {
    var ext_serid = $_extrthis.val(), temp = '';
    var ajaxURL = baseUrl + 'services/get_exter_service_data';
    var ajaxData = {
        ext_serid: ext_serid
    }

    var data = ajaxRequest(ajaxURL, ajaxData, '');
    if(data.length > 0) {
        $_extrthis.closest('.exter_items').find('.exter_service_desc').val(data[0].exter_desc);
        $_extrthis.closest('.exter_items').find('.external_cost').val(data[0].exter_amount);
        $_extrthis.closest('.exter_items').find('.our_cost').val(temp);
        $_extrthis.closest('.exter_items').find('.external_sub_amount').val(data[0].exter_amount);
    }
}
