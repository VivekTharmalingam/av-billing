/* Barcode search functions */
function get_product_details() {
    if ($('input[name="search_product"]').val() == '') {
        var smallBoxTitle = 'Sorry ! ...';
        var smallBoxMsg = '<i class="fa fa-info-circle"></i> <i>Please enter product code or name.</i>';
        if ($('#divSmallBoxes').html().length == 0)
            showSmallBox(smallBoxTitle, smallBoxMsg);
    }

    return false;
}

function disable_payment() {
    if (!$('.payment_type').hasClass('payment_type_req'))
        $('.payment_type').removeClass('payment_type_req');

    $('#sales_order').data('form_submit', '1');
    //$("#sales_order").submit();
}
function calculate_discount() {
    var sub_total = ($('.sub_total').val() != '') ? parseFloat($('.sub_total').val()) : 0;
    var discount = ($('.discount').val() != '') ? parseFloat($('.discount').val()) : 0;
    if ($('.discount_type:checked').val() == '1') { // Discount percentage
        discount = parseFloat((discount / 100) * sub_total);
    }

    items.find('.discount_amount').html(discount.toFixed(2));
    items.find('.discount_amt').val(discount.toFixed(2));
    sub_total -= discount;
    items.find('.sub_total').val(sub_total.toFixed(2));
    calculate_total();
}

function calculate_total() {
    var sub_total = 0;
    $('.amount').each(function (index, element) {
        sub_total += ($(element).val() != '') ? parseFloat($(element).val()) : 0;
    });
    $('.sub_total').val(sub_total.toFixed(2));

    var discount = ($('.discount').val() != '') ? parseFloat($('.discount').val()) : 0;
    if ($('.discount_type:checked').val() == '1') { // Discount percentage
        discount = parseFloat((discount / 100) * sub_total);
    }

    $('.discount_amount').html(discount.toFixed(2));
    $('.discount_amt').val(discount.toFixed(2));
    sub_total -= discount;

    var total_amount = sub_total;
    var gst_percentage = 0;
    var gst_amount = 0;
    if ($('.gst_type:checked').val() == '2') {
        gst_percentage = ($('#hidden_gst').val() != '') ? parseFloat($('#hidden_gst').val()) : 0;
        var gst_amount = parseFloat((gst_percentage / 100) * sub_total);
        total_amount = sub_total + gst_amount;
    }

    $('.gst_amount').val(gst_amount.toFixed(2));
    $('.gst_percentage').html(gst_percentage.toFixed(2));
    $('.total_amt').val(total_amount.toFixed(2));
    $('.payment_amount').html(total_amount.toFixed(2));
}

function get_all_items_descriptions() {
    var ajaxURL = baseUrl + 'invoice/search_item_description';
    var ajaxData = {}

    return ajaxRequest(ajaxURL, ajaxData, '');
}

function get_all_items(product_code) {
    var ajaxURL = baseUrl + 'invoice/search_item';
    var ajaxData = {
        product_code: product_code
    }

    return ajaxRequest(ajaxURL, ajaxData, '');
}

function get_promotions() {
    var ajaxURL = baseUrl + 'invoice/get_promotions';
    var ajaxData = {}

    return ajaxRequest(ajaxURL, ajaxData, '');
}

function hide_product_code_msg() {
    $('.product_code_label').slideUp();
}

function hide_error_description_msg() {
    $('.error_description').slideUp();
}

var promotions = get_promotions();
var items = get_all_items('');
//var product_codes = get_all_items(1);
var product_codes = get_all_items(1);
var descriptions = get_all_items_descriptions('');
function valid_item_code(item_code) {
    var item_code_exists = false;
    for (var i in product_codes) {
        if (product_codes[i].pdt_code.trim().toLowerCase() == item_code.val().trim().toLowerCase()) {
            item_code_exists = true;
            break;
        }
    }

    if (!item_code_exists) {
        item_code.val('');
        item_code.focus();
        return false;
    }
}

function reset_item() {
    items = get_all_items('');
    product_codes = get_all_items(1);
    descriptions = get_all_items_descriptions('');
    $(".product_code, .product_description").removeClass('hasAutocomplete');
    set_product_autocomplete();
}

function reset_customer() {
    var ajaxURL = baseUrl + 'invoice/active_customers';
    var ajaxData = {}

    var data = ajaxRequest(ajaxURL, ajaxData, '');
    var _option = '';
    _option += '<option value="cash">Cash</option>';
    for (var i in data) {
        _option += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
    }

    $('#customer_name').html(_option);
    $('#customer_name').select2({width: '100%'});
}

function apply_group_buy_price(_this) {
    if ($('.apply-group-buy').hasClass('group_buy_unlocked')) {
        $(_this).closest('.group-buy-items').find('.promo_items .promo_item').each(function (key, element) {
            //var promo_item = $(element).closest('.promo_item');
            var item_id = $(element).find('.item_id').val();
            var itemObj = $('.product_details').find('.product_id[value="' + item_id + '"]').closest('.items');
            var quantity = (itemObj.find('.quantity').val() != '') ? parseFloat(itemObj.find('.quantity').val()) : 0;
            var price = ($(element).find('input.offer_price').val() != '') ? parseFloat($(element).find('input.offer_price').val()) : 0;
            //itemObj.find('.price').val(price.toFixed(2));
            
            var amount = parseFloat((quantity * price).toFixed(2));
            var item_discount = (itemObj.find('.item_discount').val() != '') ? parseFloat(itemObj.find('.item_discount').val()) : 0;
            item_discount = (itemObj.find('.item_discount_type:checked').length) ? amount * parseFloat((item_discount / 100).toFixed(2)) : item_discount;
            amount -= item_discount;
            itemObj.find('.item_discount_amt').html(item_discount.toFixed(2));
            itemObj.find('.item_discount_amt').val(item_discount.toFixed(2));
            
            itemObj.find('.amount').val(amount.toFixed(2));
            close_promotion_popup();
        });

        calculate_total();
    }
}

function add_promotion_item_to_invoice(_this, promotion_type) {
    _this = $(_this).parents('.promo_item');
    var item_id = _this.find('input.item_id').val();
    switch (promotion_type) {
        case 'discount':
            var itemObj = $('.product_details').find('.product_id[value="' + item_id + '"]').closest('.items');
            var quantity = (itemObj.find('.quantity').val() != '') ? parseFloat(itemObj.find('.quantity').val()) : 0;
            var price = (_this.find('input.offer_price').val() != '') ? parseFloat(_this.find('input.offer_price').val()) : 0;
            itemObj.find('.price').val(price.toFixed(2));
            
            var amount = parseFloat((quantity * price).toFixed(2));
            var item_discount = (itemObj.find('.item_discount').val() != '') ? parseFloat(itemObj.find('.item_discount').val()) : 0;
            item_discount = (itemObj.find('.item_discount_type:checked').length) ? amount * parseFloat((item_discount / 100).toFixed(2)) : item_discount;
            amount -= item_discount;
            itemObj.find('.item_discount_amt').html(item_discount.toFixed(2));
            itemObj.find('.item_discount_amt').val(item_discount.toFixed(2));
            itemObj.find('.amount').val(amount.toFixed(2));
            break;

        case 'purchase':
        case 'group_buy':
            var selling_price = 0;
            if (promotion_type == 'group_buy') {
                selling_price = (_this.find('input.selling_price').val() != '') ? parseFloat(_this.find('input.selling_price').val()) : 0;
            }
            else {
                selling_price = (_this.find('input.offer_price').val() != '') ? parseFloat(_this.find('input.offer_price').val()) : 0;
            }
            if ($('.product_details').find('.product_id[value="' + item_id + '"]').length) {
                // Item already exists
                var itemObj = $('.product_details').find('.product_id[value="' + item_id + '"]').closest('.items');
                var quantity = (itemObj.find('.quantity').val() != '') ? parseFloat(itemObj.find('.quantity').val()) : 0;
                quantity += 1;
                itemObj.find('.quantity').val(quantity);

                itemObj.find('.price').val(selling_price.toFixed(2));
                
                var amount = parseFloat((quantity * selling_price).toFixed(2));
                var item_discount = (itemObj.find('.item_discount').val() != '') ? parseFloat(itemObj.find('.item_discount').val()) : 0;
                item_discount = (itemObj.find('.item_discount_type:checked').length) ? amount * parseFloat((item_discount / 100).toFixed(2)) : item_discount;
                amount -= item_discount;
                itemObj.find('.item_discount_amt').html(item_discount.toFixed(2));
                itemObj.find('.item_discount_amt').val(item_discount.toFixed(2));
                itemObj.find('.amount').val(amount.toFixed(2));
            }
            else {
                if ($('.product_details .items:last .product_id').val() != '') {
                    // Last row occupied by another product.
                    $('.product_details .items:last .add-row').trigger('click');
                }
                var itemObj = $('.product_details .items:last');
                itemObj.find('.promotion').hide();
                itemObj.find('.product_code').val(_this.find('input.item_code').val());
                //items.find('.category_id').val(ui.item.cat_id);
                itemObj.find('.product_id').val(item_id);
                var cat_name = _this.find('input.cat_name').val();
                var item_name = _this.find('input.item_name').val();
                if (cat_name != '' && cat_name != null) {
                    item_name = cat_name + ' ' + item_name;
                }
                item_name = item_name.trim();
                itemObj.find('.product_description').data('value', item_name);
                itemObj.find('.product_description').val(item_name);
                //var selling_price = (_this.find('input.offer_price').val() != '') ? parseFloat(_this.find('input.offer_price').val()) : 0;
                itemObj.find('.price').val(selling_price.toFixed(2));
                if (itemObj.find('.cost').length) {
                    var buying_price = (_this.find('input.buying_price').val() != '') ? parseFloat(_this.find('input.buying_price').val()) : 0;
                    itemObj.find('.cost').val(buying_price.toFixed(2));
                }
                itemObj.find('.quantity').val(1);
                
                var amount = parseFloat(selling_price.toFixed(2));
                var item_discount = (itemObj.find('.item_discount').val() != '') ? parseFloat(itemObj.find('.item_discount').val()) : 0;
                item_discount = (itemObj.find('.item_discount_type:checked').length) ? amount * parseFloat((item_discount / 100).toFixed(2)) : item_discount;
                amount -= item_discount;
                itemObj.find('.item_discount_amt').html(item_discount.toFixed(2));
                itemObj.find('.item_discount_amt').val(item_discount.toFixed(2));
                itemObj.find('.amount').val(amount.toFixed(2));
                
                //itemObj.find('.amount').val(selling_price.toFixed(2));
            }
            itemObj.find('.quantity').focus();
            break;

        case 'buy_one_get_one':
            if ($('.product_details').find('.product_id[value="' + item_id + '"]').length) {
                // Item already exists
                var itemObj = $('.product_details').find('.product_id[value="' + item_id + '"]').closest('.items');
                var selling_price = 0;
                itemObj.find('.price').val(selling_price.toFixed(2));
                itemObj.find('.item_discount_amt').html('0.00');
                itemObj.find('.item_discount_amt').val('0.00');
                itemObj.find('.amount').val(selling_price.toFixed(2));
            }
            else {
                if ($('.product_details .items:last .product_id').val() != '') {
                    // Last row occupied by another product.
                    $('.product_details .items:last .add-row').trigger('click');
                }
                var itemObj = $('.product_details .items:last');
                itemObj.find('.promotion').hide();
                itemObj.find('.product_code').val(_this.find('input.item_code').val());
                //items.find('.category_id').val(ui.item.cat_id);
                itemObj.find('.product_id').val(item_id);
                var cat_name = _this.find('input.cat_name').val();
                var item_name = _this.find('input.item_name').val();
                if (cat_name != '' && cat_name != null) {
                    item_name = cat_name + ' ' + item_name;
                }
                item_name = item_name.trim();
                itemObj.find('.product_description').data('value', item_name);
                itemObj.find('.product_description').val(item_name);
                var selling_price = 0;
                itemObj.find('.price').val(selling_price.toFixed(2));
                if (itemObj.find('.cost').length) {
                    var buying_price = (_this.find('input.buying_price').val() != '') ? parseFloat(_this.find('input.buying_price').val()) : 0;
                    itemObj.find('.cost').val(buying_price.toFixed(2));
                }
                itemObj.find('.quantity').val(1);
                
                var amount = parseFloat(selling_price.toFixed(2));
                var item_discount = (itemObj.find('.item_discount').val() != '') ? parseFloat(itemObj.find('.item_discount').val()) : 0;
                item_discount = (itemObj.find('.item_discount_type:checked').length) ? amount * parseFloat((item_discount / 100).toFixed(2)) : item_discount;
                amount -= item_discount;
                itemObj.find('.item_discount_amt').html(item_discount.toFixed(2));
                itemObj.find('.item_discount_amt').val(item_discount.toFixed(2));
                itemObj.find('.amount').val(amount.toFixed(2));
            }
            itemObj.find('.quantity').focus();
            break;

        default:
            break;
    }

    calculate_total();
    close_promotion_popup();
    enable_promotion_icon(item_id, promotion_type)
}

function close_promotion_popup() {
    $('#promotion').modal('hide');
}

function show_promotion(item_id) {
    var items = $('.product_details').find('.product_id[value="' + item_id + '"]').closest('.items');
    items.find('.promotion').show();
}

function enable_promotion_icon(item_id, promotion_type) {
    if (promotion_type == 'buy_one_get_one') {
        return false;
    }

    /* Purchase with purchase start */
    var purchase = promotions.purchase_with_purchase;
    for (var i in purchase) {
        var item = purchase[i];
        if (item.item_id == item_id) {
            show_promotion(item_id);
            return true;
        }
    }
    /* Purchase with purchase end */

    /* Discount Sale start */
    var discount_sale = promotions.discount_sale;
    for (var i in discount_sale) {
        var item = discount_sale[i];
        if (item.item_id == item_id) {
            show_promotion(item_id);
            return true;
        }
    }
    /* Discount Sale end */

    /* Buy one Get one start */
    var bogo = promotions.bogo;
    for (var i in purchase) {
        var item = bogo[i];
        if (item.item_id == item_id) {
            show_promotion(item_id);
            return true;
        }
    }
    /* Buy one Get one end */

    return false;
}

function open_promotion_popup(item_id) {
    $('.purchase_with_purchase, .discount_sale, .buy_one_get_one, .group_buy').hide();
    var open_popup = false;
    /* Purchase with purchase start */
    var purchase = promotions.purchase_with_purchase;
    for (var i in purchase) {
        var item = purchase[i];
        if (item.item_id == item_id) {
            $('.purchase_with_purchase').show();
            open_popup = true;
            var items_ids = item.offer_item_ids.split(',');
            var offer_prices = item.offer_prices.split(',');
            var selling_prices = item.selling_prices.split(',');
            var pdt_codes = item.pdt_codes.split(',');
            var pdt_names = item.pdt_names.split(',');
            var cat_names = item.cat_names.split(',');
            var buying_prices = item.buying_prices.split(',');

            var html = '';
            for (var j = 0; j < items_ids.length; j++) {
                var pdt_name = pdt_names[j].trim().replace(/"/g, '&quot;');
                var cat_name = (cat_names[j] !== undefined) ? cat_names[j].trim() + ' ' : ''
                cat_name = cat_name.replace(/"/g, '&quot;');
                var already_exists = '';
                if ($('.product_details').find('.product_id[value="' + items_ids[j].trim() + '"]').length)
                    already_exists = 'added';

                html += '<div class="col-md-12 promo_item ' + already_exists + '">';
                html += '<div class="col-md-3">';
                html += '<label class="item_code">' + pdt_codes[j].trim() + '</label>';
                html += '<input type="hidden" name="item_id" class="item_id" value="' + items_ids[j].trim() + '" />';
                html += '<input type="hidden" name="item_code" class="item_code" value="' + pdt_codes[j].trim() + '" />';
                html += '<input type="hidden" name="item_name" class="item_name" value="' + cat_name + pdt_name + '" />';
                html += '<input type="hidden" name="selling_price" class="selling_price" value="' + selling_prices[j].trim() + '" />';
                html += '<input type="hidden" name="buying_price" class="buying_price" value="' + buying_prices[j].trim() + '" />';
                html += '<input type="hidden" name="offer_price" class="offer_price" value="' + offer_prices[j].trim() + '" />';
                html += '</div>';

                html += '<div class="col-md-3">';
                html += '<label class="brand_des">' + cat_name + pdt_names[j].trim() + '</label>';
                html += '</div>';

                html += '<div class="col-md-2">';
                html += '<label class="selling_price">' + parseFloat(selling_prices[j].trim()).toFixed(2) + '</label>';
                html += '</div>';

                html += '<div class="col-md-2">';
                html += '<label class="offered_price">' + parseFloat(offer_prices[j].trim()).toFixed(2) + '</label>';
                html += '</div>';

                html += '<div class="col-md-2">';
                html += '<label class="btn btn-primary add_to_invoice" onclick="add_promotion_item_to_invoice(this, \'purchase\');">Add to Invoice</label>';

                html += '</div>';
                html += '</div>';
            }
            $('.purchase_with_purchase .promo_items').html(html);
        }
    }
    /* Purchase with purchase end */

    /* Discount sale start */
    var discount_sale = promotions.discount_sale;
    for (var i in discount_sale) {
        var item = discount_sale[i];
        if (item.offer_item_id == item_id) {
            open_popup = true;
            $('.discount_sale').show();

            var pdt_name = item.pdt_name.replace(/"/g, '&quot;');
            var cat_name = (item.cat_name !== undefined) ? item.cat_name.trim() + ' ' : ''
            cat_name = cat_name.replace(/"/g, '&quot;');

            var html = '';
            html += '<div class="col-md-12 promo_item">';
            html += '<div class="col-md-3">';
            html += '<label class="item_code">' + item.pdt_code + '</label>';
            html += '<input type="hidden" name="item_id" class="item_id" value="' + item.offer_item_id + '" />';
            html += '<input type="hidden" name="item_code" class="item_code" value="' + item.pdt_code + '" />';
            html += '<input type="hidden" name="item_name" class="item_name" value="' + cat_name + pdt_name + '" />';
            html += '<input type="hidden" name="selling_price" class="selling_price" value="' + item.selling_price + '" />';
            html += '<input type="hidden" name="buying_price" class="buying_price" value="' + item.buying_price + '" />';
            html += '<input type="hidden" name="offer_price" class="offer_price" value="' + item.offer_price + '" />';
            html += '</div>';

            html += '<div class="col-md-3">';
            html += '<label class="brand_des">' + cat_name + item.pdt_name + '</label>';
            html += '</div>';

            html += '<div class="col-md-2">';
            html += '<label class="selling_price">' + parseFloat(item.selling_price).toFixed(2) + '</label>';
            html += '</div>';

            html += '<div class="col-md-2">';
            html += '<label class="offered_price">' + parseFloat(item.offer_price).toFixed(2) + '</label>';
            html += '</div>';

            html += '<div class="col-md-2">';
            html += '<label class="btn btn-primary add_to_invoice" onclick="add_promotion_item_to_invoice(this, \'discount\');" >Apply Discount</label>';

            html += '</div>';
            html += '</div>';
            $('.discount_sale .promo_items').html(html);
        }
    }
    /* Discount sale end */

    /* Buy one Get one start */
    var bogo = promotions.bogo;
    for (var i in bogo) {
        var item = bogo[i];
        if (item.item_id == item_id) {
            $('.buy_one_get_one').show();
            open_popup = true;
            var items_ids = item.offer_item_ids.split(',');
            var offer_prices = item.offer_prices.split(',');
            var selling_prices = item.selling_prices.split(',');
            var pdt_codes = item.pdt_codes.split(',');
            var pdt_names = item.pdt_names.split(',');
            var cat_names = item.cat_names.split(',');
            var buying_prices = item.buying_prices.split(',');

            var html = '';
            for (var j = 0; j < items_ids.length; j++) {
                var pdt_name = pdt_names[j].trim().replace(/"/g, '&quot;');
                var cat_name = (cat_names[j] !== undefined) ? cat_names[j].trim() + ' ' : ''
                cat_name = cat_name.replace(/"/g, '&quot;');

                html += '<div class="col-md-12 promo_item">';
                html += '<div class="col-md-3">';
                html += '<label class="item_code">' + pdt_codes[j].trim() + '</label>';
                html += '<input type="hidden" name="item_id" class="item_id" value="' + items_ids[j].trim() + '" />';
                html += '<input type="hidden" name="item_code" class="item_code" value="' + pdt_codes[j].trim() + '" />';
                html += '<input type="hidden" name="item_name" class="item_name" value="' + cat_name + pdt_name + '" />';
                html += '<input type="hidden" name="selling_price" class="selling_price" value="' + selling_prices[j].trim() + '" />';
                html += '<input type="hidden" name="buying_price" class="buying_price" value="' + buying_prices[j].trim() + '" />';
                html += '<input type="hidden" name="offer_price" class="offer_price" value="0.00" />';
                html += '</div>';

                html += '<div class="col-md-3">';
                html += '<label class="brand_des">' + cat_name + pdt_names[j].trim() + '</label>';
                html += '</div>';

                html += '<div class="col-md-2">';
                html += '<label class="selling_price">' + parseFloat(selling_prices[j].trim()).toFixed(2) + '</label>';
                html += '</div>';

                html += '<div class="col-md-2">';
                html += '<label class="offered_price">' + parseFloat(offer_prices[j].trim()).toFixed(2) + '</label>';
                html += '</div>';

                html += '<div class="col-md-2">';
                html += '<label class="btn btn-primary add_to_invoice" onclick="add_promotion_item_to_invoice(this, \'buy_one_get_one\');">Add to Invoice</label>';

                html += '</div>';
                html += '</div>';
            }
            $('.buy_one_get_one .promo_items').html(html);
        }
    }
    /* Buy one Get one end */

    /* Group Buy start */
    var group_buy = promotions.group_buy;
    var content = '';
    for (var i in group_buy) {
        var item = group_buy[i];
        var items_ids = item.offer_item_ids.split(',');
        $.each(items_ids, function (key, value) {
            items_ids[key] = value.trim();
        });

        if (items_ids.indexOf(item_id) != -1) {
            $('.group_buy').show();
            open_popup = true;
            var group_names = item.group_names.split(',');
            var offer_prices = item.offer_prices.split(',');
            var selling_prices = item.selling_prices.split(',');
            var pdt_codes = item.pdt_codes.split(',');
            var pdt_names = item.pdt_names.split(',');
            var cat_names = item.cat_names.split(',');
            var buying_prices = item.buying_prices.split(',');

            var html = '';
            var existing_count = 0;
            var save_amt = 0;
            for (var j = 0; j < items_ids.length; j++) {
                var pdt_name = pdt_names[j].trim().replace(/"/g, '\"');
                var cat_name = (cat_names[j] !== undefined) ? cat_names[j].trim() + ' ' : ''
                cat_name = cat_name.replace(/"/g, '&quot;');

                var already_exists = '';
                if ($('.product_details').find('.product_id[value="' + items_ids[j].trim() + '"]').length) {
                    already_exists = 'added';
                    existing_count++;
                }

                var offer_price = (offer_prices[j] != '') ? parseFloat(offer_prices[j].trim()) : 0;
                var selling_price = (selling_prices[j] != '') ? parseFloat(selling_prices[j].trim()) : 0;
                save_amt += selling_price - offer_price;

                /* .promo_item start */
                html += '<div class="col-md-12 promo_item ' + already_exists + '">';
                html += '<div class="col-md-3">';
                html += '<label class="item_code">' + pdt_codes[j].trim() + '</label>';
                html += '<input type="hidden" name="item_id" class="item_id" value="' + items_ids[j].trim() + '" />';
                html += '<input type="hidden" name="item_code" class="item_code" value="' + pdt_codes[j].trim() + '" />';
                html += '<input type="hidden" name="item_name" class="item_name" value="' + cat_name + pdt_name + '" />';
                html += '<input type="hidden" name="selling_price" class="selling_price" value="' + selling_price.toFixed(2) + '" />';
                html += '<input type="hidden" name="buying_price" class="buying_price" value="' + buying_prices[j].trim() + '" />';
                html += '<input type="hidden" name="offer_price" class="offer_price" value="' + offer_price.toFixed(2) + '" />';
                html += '</div>';

                html += '<div class="col-md-3">';
                html += '<label class="brand_des">' + cat_name + pdt_names[j].trim() + '</label>';
                html += '</div>';

                html += '<div class="col-md-2">';
                html += '<label class="selling_price">' + parseFloat(selling_prices[j].trim()).toFixed(2) + '</label>';
                html += '</div>';

                html += '<div class="col-md-2">';
                html += '<label class="offered_price">' + parseFloat(offer_prices[j].trim()).toFixed(2) + '</label>';
                html += '</div>';

                html += '<div class="col-md-2">';
                html += '<label class="btn btn-primary add_to_invoice" onclick="add_promotion_item_to_invoice(this, \'group_buy\');">Add to Invoice</label>';

                html += '</div>';
                html += '</div>';
                /* .promo_item end */
            }

            /* .group-buy-items start */
            content += '<div class="group-buy-items">';
            /* .group-buy-heading start */
            content += '<div class="group-buy-heading">';
            content += '<h3 class="promotion_title">';
            content += '<span class="group_name">' + group_names[i].trim() + '</span>';
            content += '<div class="pull-right">';
            content += '<label class="save-amt" style="margin-right: 10px;">Save : &dollar; <span class="group_price">' + save_amt.toFixed(2) + '</span></label>';

            content += '<span class="btn ';
            if (existing_count == items_ids.length) {
                content += 'btn-success group_buy_unlocked';
            }
            else {
                content += ' btn-default group_buy_locked';
            }
            content += ' apply-group-buy" onclick="apply_group_buy_price(this);">Apply Offered Price</span>';
            content += '</div>';
            content += '</h3>';
            content += '</div>';
            /* .group-buy-heading end */

            /* .group-buy-body start */
            content += '<div class="group-buy-body promo_items_container">';
            content += '<div class="col-md-12 promo_item_head">';
            content += '<div class="col-md-3"><label>Item Code</label></div>';
            content += '<div class="col-md-3"><label>Brand - Description</label></div>';
            content += '<div class="col-md-2"><label>Selling Price</label></div>';
            content += '<div class="col-md-2"><label>Offered Price</label></div>';
            content += '<div class="col-md-2"><label>Actions</label></div>';
            content += '</div>';
            content += '<div class="promo_items">' + html + '</div>';
            content += '</div>';
            content += '</div>';
            /* .group-buy-body end */

            content += '</div>';
            /* .group-buy-items end */
        }

        $('.group-buy-content').html(content);
    }
    /* Group Buy end */

    if (open_popup) {
        if ($('.promo_items .added').length) {
            $('.added-item-legend').show();
        }
        else {
            $('.added-item-legend').hide();
        }
        $("#promotion").modal({
            backdrop: "static",
            keyboard: true,
            show: true
        });

        var items = $('.product_details').find('.product_id[value="' + item_id + '"]').closest('.items');
        items.find('.promotion').slideDown('slow');
    }
}

function set_product_autocomplete() {
    $(".product_code").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }

        $(this).on('change', function () {
            valid_item_code($(this));
        });

        $(this).autocomplete({
			source: function(request, response) {
				var results = $.ui.autocomplete.filter(product_codes, request.term);
				response(results.slice(0, 15));
			},
            focus: function (event, ui) {
				show_available_quantity(event.currentTarget, ui.item.branch_products);
            },
			open: function() {
				hide_available_quantity();
            },
			close: function() {
				hide_available_quantity();
				if ($(this).val().trim().length) 
					$(this).closest('.items').find('.product_description').trigger('focus');
			},
            select: function (event, ui) {
                var items = $(this).closest('.items');
                var product_id_name = items.find('.product_id').prop('name');
                if ($('.product_details').find('.product_id[value="' + ui.item.id + '"]:not(.product_id[name="' + product_id_name + '"])').length) {
                    // Item already exists
                    if ($(this).data('value').trim() != ui.item.value.trim()) {
                        items = $('.product_details').find('.product_id[value="' + ui.item.id + '"]').closest('.items');

                        $(this).val('');
                        $(this).closest('.items').find('.product_code_label').slideDown();
                        setTimeout(hide_product_code_msg, 3000)
                        return false;
                    }
                    else {
                        $(this).val(ui.item.value.trim());
                        des = ui.item.pdt_name;
                        if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                            des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                        }
                        items.find('.product_description').val(des.trim());
                    }
                }
                else {
                    $(this).data('value', ui.item.pdt_code.trim());
                    $(this).val(ui.item.pdt_code.trim());
                    items.find('.category_id').val(ui.item.cat_id);
                    items.find('.product_id').val(ui.item.id);
//                    items.find('.brand_name').html(ui.item.cat_name);
//                    items.find('.brand_label').slideDown();
//                    items.find('.product_description').val(ui.item.pdt_name);
                    des = ui.item.pdt_name;
                    if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                        des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                    }
                    items.find('.product_code').data('value', ui.item.pdt_code.trim());
                    items.find('.product_description').val(des.trim());
                    var selling_price = (ui.item.selling_price != '') ? parseFloat(ui.item.selling_price) : 0;
                    items.find('.price').val(selling_price.toFixed(2));
                    if (items.find('.cost').length) {
                        var buying_price = (ui.item.buying_price != '') ? parseFloat(ui.item.buying_price) : 0;
                        items.find('.cost').val(buying_price.toFixed(2));
                    }
                    items.find('.quantity').val(1);
                    
                    var amount = parseFloat(selling_price.toFixed(2));
                    var item_discount = (items.find('.item_discount').val() != '') ? parseFloat(items.find('.item_discount').val()) : 0;
                    item_discount = (items.find('.item_discount_type:checked').length) ? amount * parseFloat((item_discount / 100).toFixed(2)) : item_discount;
                    amount -= item_discount;
                    items.find('.item_discount_amt').html(item_discount.toFixed(2));
                    items.find('.item_discount_amt').val(item_discount.toFixed(2));
                    items.find('.amount').val(amount.toFixed(2));
                }
                calculate_total();

                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || event.keyCode;
                if (charCode == 13)
                    items.find('.quantity').focus();
                else if (event.type == 'autocompleteselect')
                    items.find('.product_description').focus();

                /* To focus quantity of particular item end */
                open_promotion_popup(ui.item.id);
                return false;
            }
        });

        $(this).addClass('hasAutocomplete');
    });

    $(".product_description").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }

        $(this).autocomplete({
			source: function(request, response) {
				var results = $.ui.autocomplete.filter(descriptions, request.term);
				response(results.slice(0, 15));
			},
            focus: function (event, ui) {
				show_available_quantity(event.currentTarget, ui.item.branch_products);
            },
			open: function() {
				hide_available_quantity();
            },
			close: function() {
				hide_available_quantity();
			},
            select: function (event, ui) {
                //alert('test');
                var items = $(this).closest('.items');
                var product_id_name = items.find('.product_id').prop('name');
                if ($('.product_details').find('.product_id[value="' + ui.item.id + '"]:not(.product_id[name="' + product_id_name + '"])').length) {
                    // Item already exists
                    if ($(this).data('value').trim() != ui.item.value.trim()) {
                        items = $('.product_details').find('.product_id[value="' + ui.item.id + '"]').closest('.items');
                        $(this).val($(this).data('value').trim());
                        $(this).closest('.items').find('.error_description').slideDown();
                        setTimeout(hide_error_description_msg, 3000);
                        items.find('.quantity').focus();
                        //return false;
                    }
                    else {
                        $(this).val(ui.item.value.trim());
                    }
                }
                else {
                    items.find('.product_code').val(ui.item.pdt_code.trim());
                    //$(this).val(ui.item.pdt_code);
                    items.find('.category_id').val(ui.item.cat_id);
                    items.find('.product_id').val(ui.item.id);
//                    items.find('.brand_name').html(ui.item.cat_name);
//                    items.find('.brand_label').slideDown();
//                    items.find('.product_description').val(ui.item.pdt_name);

                    if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                        des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                    }
                    else {
                        des = ui.item.pdt_name;
                    }
                    des = des.trim();
                    items.find('.product_description').data('value', des);
                    items.find('.product_description').val(des);
                    var selling_price = (ui.item.selling_price != '') ? parseFloat(ui.item.selling_price) : 0;
                    items.find('.price').val(selling_price.toFixed(2));
                    if (items.find('.cost').length) {
                        var buying_price = (ui.item.buying_price != '') ? parseFloat(ui.item.buying_price) : 0;
                        items.find('.cost').val(buying_price.toFixed(2));
                    }
                    items.find('.quantity').val(1);
                    
                    var amount = parseFloat(selling_price.toFixed(2));
                    var item_discount = (items.find('.item_discount').val() != '') ? parseFloat(items.find('.item_discount').val()) : 0;
                    item_discount = (items.find('.item_discount_type:checked').length) ? amount * parseFloat((item_discount / 100).toFixed(2)) : item_discount;
                    amount -= item_discount;
                    items.find('.item_discount_amt').html(item_discount.toFixed(2));
                    items.find('.item_discount_amt').val(item_discount.toFixed(2));
                    items.find('.amount').val(amount.toFixed(2));
                }
                calculate_total();

                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || event.keyCode;
                if (charCode == 13)
                    items.find('.quantity').focus();
                else if (event.type == 'autocompleteselect')
                    items.find('.quantity').focus();

                open_promotion_popup(ui.item.id);
                /* To focus quantity of particular item end */
                return true;
            }
        });

        $(this).addClass('hasAutocomplete');
    });
}

function delivery_to_change_focus() {
    if ($('#ship_to').val() == '')
        $('input[name="location_name"]').focus();
    else
        $('#your_reference').focus();
}

function focus_quantity(qty) {
    $(qty).focus();
}

function open_serial_no_popup($_this) {
    var open_popup = false;
    /* Set Serial No. */
    var qty_count = ($_this.closest('.items').find('.quantity').val() != '') ? parseFloat($_this.closest('.items').find('.quantity').val()) : 0;    
    var product_code = $_this.closest('.items').find('.product_code').val();     
    var serial_no = ($_this.closest('.items').find('.serial_no').val() != '') ? $_this.closest('.items').find('.serial_no').val().split(',') : [];     
    var html = '', temp='';
    this_item_row = '';
    if((qty_count > 0) && product_code != '') {
        open_popup = true;
        this_item_row = $_this;
        $('.serial_no_title').html('Add Serial No. for '+product_code);
        for (var i=0; i < qty_count; i++) {
			var ths_item_sl_no = (serial_no[i] != undefined) ? serial_no[i] : "";
			var inc_i = i+1;
			html += '<div class="col-md-12 itemsernos">';
			
			html += '<div class="col-md-1">';
			html += '<label class="item_id" style="padding-top: 5px;">' + inc_i + '</label>';
			html += '</div>';

			html += '<div class="col-md-11">';
			html += '<label class="input" style="width:100%;">';
			html += '<input type="text" name="ser_no_txt[]" class="ser_no_txt form-control" onkeypress="return isRmvComma(event)" value="'+ths_item_sl_no+'" />';
			html += '</label>';
			html += '</div>';
			
			html += '</div>';
        }
        $('.item_ser_nos').html(html);
    }
    /* End */

    if (open_popup) {
        $("#grn_serial_no").modal({
            backdrop: "static",
            keyboard: true,
            show: true
        });
    }
}

function set_serial_no_input() {
    var ser_no_txtall = '';
    if(this_item_row != '') {
        $('.ser_no_txt').each(function(){
            if($(this).val().trim() != "") {
                ser_no_txtall += $(this).val().trim()+',';
            }
        });
        ser_no_txtall = (ser_no_txtall != '') ? ser_no_txtall.replace(/^,|,$/g,'') : '';
        this_item_row.closest('.items').find('.serial_no').val(ser_no_txtall);    
    }
}

function isRmvComma(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 44) {
        return false;
    }
    return true;
}

function calculate_item_total(_this) {
	var items = $(_this).closest('.items');
	var quantity = (items.find('.quantity').val() != '') ? parseFloat(items.find('.quantity').val()) : 0;
	var price = (items.find('.price').val() != '') ? parseFloat(items.find('.price').val()) : 0;
	var amount = parseFloat((quantity * price).toFixed(2));
	var item_discount = (items.find('.item_discount').val() != '') ? parseFloat(items.find('.item_discount').val()) : 0;
    item_discount = (items.find('.item_discount_type:checked').length) ? amount * parseFloat((item_discount / 100).toFixed(2)) : item_discount;
	amount -= item_discount;
	items.find('.item_discount_amt').html(item_discount.toFixed(2));
    items.find('.item_discount_amt').val(item_discount.toFixed(2));
	items.find('.amount').val(amount.toFixed(2));
	calculate_total();
}

var addRowIndex = 0;
var scanItem = 0;
var tabPressed = 0;
$(document).ready(function () {
    /*var ship_toSelect = $('#customer_name');
     $('#ship_to').on('keydown', function (e) {
     alert("select2:close");
     });*/

    $('#service_id').on('change', function () {
        if ($(this).val() != '') {
            $("#service_form").submit();
        }
    });
    
    $('#so_hold_id').on('change', function () {
        if ($(this).val() != '') {
            $("#so_hold_form").submit();
        }
    });
    
    $('#pst_quotation_id').on('change', function () {
        if ($(this).val() != '') {
            var pst_urltxt = baseUrl + 'invoice/add/'+$(this).val();
            $("#quotation_form").prop('action', pst_urltxt)
            $("#quotation_form").submit();
        }
    });

    /* Function to focus customer name on first tab */
    if ($('.edit_form').length == 0) {
        $(document).keydown(function (e) {
            if ((e.which == 9) && (tabPressed == 0)) {
                $('#customer_name').select2('open');
                $('#customer_name').select2('val', '');
                $('#customer_name').focus();
                tabPressed++;
            }
        });
    }

    $('#search_product').focus();

    $("#your_reference").focus(function () {
        $('#your_reference').data('focused', true);
    });

    $("#remarks").focus(function () {
        if (!$('#your_reference').data('focused')) {
            $('#your_reference').focus();
        }
    });
	
	$(document).on('focusout', 'input.product_description', function (e) {
        hide_available_quantity();
    });
	
    $(document).on('focus', 'input.product_code, input.product_description', function (e) {
        var item = {};
        var product_code = $(this).closest('.items').find('.product_code').val().trim();
        if (product_code != '') {
            for (var i in product_codes) {
                if ((product_code != '') && (product_codes[i].label.trim() == product_code)) {
                    item = product_codes[i];
                    show_available_quantity($(this), product_codes[i].branch_products);
                    break;
                }
            }
        } else {
            hide_available_quantity();
        }
    });
	
    $(document).on('blur', 'input.product_code', function (e) {
        var item = {};
        var product_code = $(this).val().trim();
        if (product_code != '') {
            for (var i in product_codes) {
                if ((product_code != '') && (product_codes[i].label.trim() == product_code)) {
                    item = product_codes[i];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: item});
                    break;
                }
            }
        }
		else {
			hide_available_quantity();
		}
    });

    /*$(document).on('blur', 'input.product_description', function (e) {
        var item = {};
        var product_description = $(this).val().trim();
        if (product_description != '') {
            for (var i in descriptions) {
                if ((product_description != '') && (descriptions[i].label.trim() == product_description)) {
                    item = descriptions[i];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: item});
                    break;
                }
            }
        }
    });*/

    $(document).on('focus', ".quantity, .price, .cost", function () {
        /*alert('test');
         if (!$(this).data('focused')) {
         $(this).focus();
         }
         
         return false;*/
        $(this).select();
    });

    //$('#customer_name').select2({width: '100%'}).trigger('click');
    /* Textarea new line placeholder start */
    /*var textAreas = $('textarea');
     Array.prototype.forEach.call(textAreas, function(elem) {
     elem.placeholder = elem.placeholder.replace(/\\n/g, '\n');
     });*/
    /* Textarea new line placeholder end */

    /*$(document).on('change', '.cus_type', function () {
     if ($(this).val() == '2') {
     $('.reg_customer_add').hide();
     $('.current_address').show();
     $('.cash_customer_name').show();
     $('#cash_customer_name').focus();
     }
     else {
     $('.reg_customer_add').show();
     $('.current_address').hide();
     $('.cash_customer_name').hide();
     $('select[name="customer_name"]').focus();
     }
     });*/

    $('.so_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });

    $("#search_product").keydown(function (e) {
        e = e || window.event;
        var charCode = e.which || e.keyCode;
        if (charCode == 13) {
            return false;
        }
    });

    /*
     // To focus bar code scan field
     $(document).on('keydown', 'input.quantity', function (e) {
     e = e || window.event;
     var charCode = e.which || e.keyCode;
     if (charCode == 9) {
     $("#search_product").focus();
     return false;
     }
     });*/

    $(document).on('click', '.promotion', function () {
        var product_id = $(this).parents('.items').find('.product_id').val();
        open_promotion_popup(product_id);
    });
    
    $(document).on('change', '.payment_type', function () {
        var group = $('.payment_type option:selected').attr('data-group');
        $('#data_group').val(group)
    });

    $(document).on('keyup', 'input.search_product', function (e) {
        var ser = $(this).val();
        var _this = $(this);
        if (ser) {
            $.each(items, function (key, value) {
				var s_no_array = new Array();
				if (value.s_no != '')
					s_no_array = value.s_no.split(',');
				
                if ((ser.toUpperCase() === value.bar_code.toUpperCase()) || ($.inArray(ser, s_no_array) !== -1) || (ser.toUpperCase() == value.pdt_code.toUpperCase())) {
                    if ($('.product_details').find('.product_id[value="' + value.id + '"]').length) {
                        // Item already exists
                        var items = $('.product_details').find('.product_id[value="' + value.id + '"]').closest('.items');
                        var quantity = (items.find('.quantity').val() != '') ? parseFloat(items.find('.quantity').val()) : 0;
                        quantity += 1;
                        items.find('.quantity').val(quantity);
                        var price = (items.find('.price').val() != '') ? parseFloat(items.find('.price').val()) : 0;
                        
                        var amount = parseFloat((price * quantity).toFixed(2));
                        var item_discount = (items.find('.item_discount').val() != '') ? parseFloat(items.find('.item_discount').val()) : 0;
                        item_discount = (items.find('.item_discount_type:checked').length) ? amount * parseFloat((item_discount / 100).toFixed(2)) : item_discount;
                        amount -= item_discount;
                        items.find('.item_discount_amt').html(item_discount.toFixed(2));
                        items.find('.item_discount_amt').val(item_discount.toFixed(2));
                        items.find('.amount').val(amount.toFixed(2));
                    }
                    else {
                        if ($('.product_details .items:last .product_id').val() != '') {
                            // Last row occupied by another product.
                            $('.product_details .items:last .add-row').trigger('click');
                            $('input.search_product').focus();
                        }
                        var items = $('.product_details .items:last');
                        items.find('.category_id').val(value.cat_id);
                        items.find('.product_id').val(value.id);
                        items.find('.product_code').val(value.pdt_code);
                        //items.find('.brand_name').html(value.cat_name);
                        //items.find('.brand_label').slideDown();
                        des = value.pdt_name;
                        if (value.cat_name != '' && value.cat_name != null) {
                            des = value.cat_name + ' ' + value.pdt_name;
                        }
                        items.find('.product_description').val(des);
						
                        var selling_price = (value.selling_price != '') ? parseFloat(value.selling_price) : 0;
                        items.find('.price').val(selling_price.toFixed(2));
                        if (items.find('.cost').length) {
                            var buying_price = (value.buying_price != '') ? parseFloat(value.buying_price) : 0;
                            items.find('.cost').val(buying_price.toFixed(2));
                        }

                        items.find('.quantity').val(1);
                        
                        var amount = parseFloat(selling_price.toFixed(2));
                        var item_discount = (items.find('.item_discount').val() != '') ? parseFloat(items.find('.item_discount').val()) : 0;
                        item_discount = (items.find('.item_discount_type:checked').length) ? amount * parseFloat((item_discount / 100).toFixed(2)) : item_discount;
                        amount -= item_discount;
                        items.find('.item_discount_amt').html(item_discount.toFixed(2));
                        items.find('.item_discount_amt').val(item_discount.toFixed(2));
                        items.find('.amount').val(amount.toFixed(2));
                        
                        //items.find('.amount').val(selling_price.toFixed(2));
                    }
                    
                    calculate_total();
                    $(_this).val('');
                    $(_this).focus();
                    scanItem = 1;
                    open_promotion_popup(value.id);
                    return false;
                }
            });
        }
    });
	
    // To set autocomplete in product add row
    set_product_autocomplete();

    if ($('#customer_name option:selected').val() == 'cash') {
        get_cashCustomer($('#customer_name'));
    }

    $('#customer_name').on('change', function (e) {
        get_cashCustomer($(this));
    });

    $('#ship_to').on('change', function () {
        if ($(this).val() == '') {
            $('.new_shipping_address').slideDown();
            $('.ship_to_address').slideUp();
        }
        else {
            $('.new_shipping_address').slideUp();
            var ship_to_address = $(this).find('option:selected').data('address');
            html = '';
            if (ship_to_address != '')
                html = '<p class="alert alert-info not-dismissable"> Delivery Address: ' + ship_to_address + '</p>';

            $('.ship_to_address').html(html);
            $('.ship_to_address').slideDown();
        }

        setTimeout(delivery_to_change_focus, 100);
        //return false;
    });
	
	$(document).on('keyup', '.quantity, .price, .item_discount', function () {
        calculate_item_total(this);
    });
	
	$(document).on('change', '.item_discount_type', function () {
        calculate_item_total($(this));
    });
	
    $(document).on('keyup', '.discount', function () {
        calculate_total();
    });

    $(document).on('change', '.discount_type, .discount', function () {
        calculate_total();
    });

    $(document).on('change', '.gst_type', function () {
        calculate_total();
    });

    addRowIndex = $('.items').length - 1;
    /* Add row functionality for material */
    $(document).on('click', '.items .add-row', function () {
        addRowIndex++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.items').clone();
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');
        itemObj.find('.brand_name').html('');
        itemObj.find('.brand_label').hide();
        //itemObj.find('.vat_percentage').attr('checked', false);

        itemObj.find('input, select, textarea').each(function (index, element) {
            if ($(element).hasClass('item_hidden_ids')) {
                $(element).remove();
            }

            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndex + ']');
            }
            $(element).val('');

            if ($(element).hasClass('product_description'))
                $(element).data('value', '');

            if ($(element).hasClass('hasAutocomplete'))
                $(element).removeClass('hasAutocomplete');

            if ($(element).hasClass('product_code')) {
                $(element).data('value', '');
            }

            /*if ($(element).prop('tagName').toLowerCase() == 'select') {
             if ($(element).prev().hasClass('select2')) {
             $(element).prev().remove();
             $(element).select2({width: '100%'});
             }
             }*/
        });
		
		itemObj.find('.item_discount_type').prop('checked', true);
        itemObj.find('.item_discount_type').val(1);
        itemObj.find('.item_discount_amt').html('0.00');
        itemObj.find('.item_discount_amt').val('0.00');

        if (formType == 'edit') {
            itemObj.find('.item_discount_type').prop('id', 'new_item_discount_type-' + addRowIndex);
            itemObj.find('.onoffswitch-label').prop('for', 'new_item_discount_type-' + addRowIndex);
        }
        else {
            itemObj.find('.item_discount_type').prop('id', 'item_discount_type-' + addRowIndex);
            itemObj.find('.onoffswitch-label').prop('for', 'item_discount_type-' + addRowIndex);
        }
		
        var materialContainer = $(this).parents('.material-container');
        var optionsLength = items.length;
        if (optionsLength > $(materialContainer).find('.items').length) {
            if ($(this).parent().find('.remove-row').length)
                $(this).remove();
            else
                $(this).removeClass('add-row fa-plus-circle').addClass('remove-row fa-minus-circle');

            if ($(itemObj).find('.remove-row').length == 0)
                $(itemObj).find('.add-row').after('<i class="fa fa-2x fa-minus-circle remove-row"></i>');

            $(materialContainer).find('.items:last').after(itemObj);
        }
        else if ((optionsLength == $(materialContainer).find('.items').length) || (optionsLength == 0)) {
            var smallBoxTitle = 'Sorry ! ...';
            var smallBoxMsg = '<i class="fa fa-info-circle"></i> <i>You can\'t add more...<br> Because you have';
            smallBoxMsg += (optionsLength > 0) ? ' only ' + optionsLength : ' no';
            smallBoxMsg += ($(materialContainer).find('.items').length > 1) ? ' items' : ' item';
            smallBoxMsg += '.</i>';

            if ($('#divSmallBoxes').html().length == 0)
                showSmallBox(smallBoxTitle, smallBoxMsg);
        }

        set_product_autocomplete();
        $('.items:last .product_code').focus();
    });

    /* Remove row functionality for material */
    $(document).on('click', '.items .remove-row', function () {
        var materialContainer = $(this).parents('.material-container');
        $(this).parents('.items').remove();
        if (materialContainer.find('.items:last').find('.add-row').length == 0) {
            materialContainer.find('.items:last').find('.remove-row').before('<i class="fa fa-2x fa-plus-circle add-row"></i>');
        }

        if ((materialContainer.find('.items').length == 1) && (materialContainer.find('.items:last').find('.add-row').length)) {
            materialContainer.find('.items:last').find('.remove-row').remove();
        }
        calculate_total();
        $('.items:last .product_code').focus();
    });

    $.validator.addMethod('mat_already_exist', function (value, element) {
        var prevItems = $(element).parents('.items').prevAll('.items');
        return (prevItems.find('.product_id[value="' + value + '"]').length) ? false : true;
    }, 'Item Code is already exist!');

    $.validator.addMethod('product_code_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please select Product Code!');

    $.validator.addMethod('qty_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please enter Quantity!');

    $.validator.addMethod('payment_type_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please select Payment Type!');

    $.validator.addMethod('your_reference_exist', function (value, element) {
        var formType = $(element).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var return_val = true;
        var your_reference = $(element).val();
        var invoice_id = (formType == 'edit') ? $('input[name="so_id"]').val() : '';
        if (your_reference != '') {
            var ajaxURL = baseUrl + 'invoice/check_your_reference';
            var ajaxData = {
                your_reference: your_reference,
                invoice_id: invoice_id
            }

            var data = ajaxRequest(ajaxURL, ajaxData, '');
            if (data.exist >= 1) {
                return_val = false;
            }
        }
        return return_val;
    });

    $.validator.addClassRules({
        product_code: {
            product_code_required: true,
            mat_already_exist: true
        },
        quantity: {
            qty_required: true
        },
        payment_type_req: {
            payment_type_required: true
        }
    });

    $('select[name="customer_name"], select[name="ship_to"]').on('change', function () {
        $('#sales_order').validate().element(this);
    });

    $('input[name="so_date"], input[name="cheque_date"], input[name="dd_date"]').on('changeDate', function () {
        $('#sales_order').validate().element(this);
    });

    $("#sales_order").validate({
        // Rules for form validation
        invalidHandler: function (event, validator) {
            $('body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top - 100
            },
            '500',
                    'swing'
                    );
        },
        rules: {
            customer_name: {
                required: true
            },
            your_reference: {
                your_reference_exist: true
            },
            so_date: {
                required: true
            },
            cheque_bank_name: {
                required: true
            },
            cheque_acc_no: {
                required: true
            },
            cheque_date: {
                required: true
            },
            dd_no: {
                required: true
            },
            dd_date: {
                required: true
            }
        },
        // Messages for form validation
        messages: {
            customer_name: {
                required: 'Please select Sold To!'
            },
            your_reference: {
                your_reference_exist: 'Customer reference already exists!'
            },
            so_date: {
                required: 'Please select Date!'
            },
            cheque_bank_name: {
                required: 'Please enter Bank Name!'
            },
            cheque_acc_no: {
                required: 'Please enter Cheque No!'
            },
            cheque_date: {
                required: 'Please enter Cheque Date!'
            },
            dd_no: {
                required: 'Please enter DD No!'
            },
            dd_date: {
                required: 'Please enter DD Date!'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
            if (scanItem) {
                scanItem = 0;
                return false;
            }
            
            var subbtn_name = $("#sales_order").context.activeElement.name;
            if(subbtn_name == 'submit_hold') {
                form.submit();
                return true;
            }
            // var payment_terms defined at the bottom of add/edit form
            var go_to_print = false;
            for (var i in payment_terms) {
                if ($('#payment_terms').val() == i) {
                    go_to_print = true;
                }
            }

            if (go_to_print) {
                form.submit();
                return true;
            }

            tabPressed++;
            $('#sales_form').hide();
            $('.payment_type').removeClass('payment_type_req');
            $('#payment_form').show();
            $('body').animate({
                scrollTop: 0
            },
            '500',
                    'swing'
                    );

            $('#pay_now').focus();
            // do other things for a valid form
            if ($('#sales_order').data('form_submit') == '0') {
                return false;
            }

            form.submit();
        }
    });

    /* Salse Payment operation start */
    $('#pay_now').on('click', function () {
        if (!$('.payment_type').hasClass('payment_type_req'))
            $('.payment_type').addClass('payment_type_req');

        $('#sales_order').data('form_submit', '1');
    });

    $('#skip_payment').on('click', function () {
        $('#sales_order').data('form_submit', '1');
    });

    //$('.cheque_details').hide();
    //$('.dd_details').hide();

    $('#payment_type').on('change', function () {
        $('.cheque_details').slideUp();
        //$('.dd_details').hide();

        if ($(this).find('option:selected').data('is_cheque') == '1')
            $('.cheque_details').slideDown();

        $('#pay_now').focus();
    });
    /* Salse Payment operation end */
    
    
    $("#exchangeorder_form").validate({
        // Rules for form validation
        invalidHandler: function (event, validator) {
            $('body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top - 100
            },
            '500',
                    'swing'
                    );
        },
        rules: {
            'exchangeorder_id[]' : {
                required: true
            }
        },
        // Messages for form validation
        messages: {
            'exchangeorder_id[]' : {
                required: 'Please select the exchange order!'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
	
	$(document).on('click', '.item-serialnum-popup', function () {
        open_serial_no_popup($(this));
		$('input.ser_no_txt:first').focus();
    });
	
	$(document).on('click', '.serial_no_close, .close', function () {
        set_serial_no_input();
    });
    
    $(document).on('blur', 'input.quantity', function (e) {
        var serial_no = ($(this).closest('.items').find('.serial_no').val() != '') ? $(this).closest('.items').find('.serial_no').val().split(',') : [];     
        var qnty = ($(this).val() != '') ? parseFloat($(this).val()) : 0;   
        var item_sl_no_txt = '';
        if(serial_no.length>qnty) {
            item_sl_no_txt = serial_no.slice(0, qnty);
        }
        if((qnty>0 && item_sl_no_txt!='') || (qnty==0 && item_sl_no_txt=='')) {
            $(this).closest('.items').find('.serial_no').val(item_sl_no_txt);
        }
    });
});

function show_sales_form() {
    $('#sales_order').data('form_submit', '0');
    $('#payment_form').hide();
    $('#sales_form').show();
    $('#search_product').focus();
}

function get_cashCustomer(_this) {
    /*if ($(this).val() == '') {
     $('#ship_to').find('option:not(:first)').remove();
     $('#ship_to').select2('val', '');
     $('.cust_address').slideUp();
     $('.new_shipping_address').slideDown();
     $('.ship_to_address').slideUp();
     return '';
     }*/
    //alert('test');
    if ($(_this).val() == 'cash') {
        $('#ship_to').find('option:not(:first)').remove();
        $('#ship_to').select2('val', '');
        $('#payment_type').select2('val', '');
        $('.reg_customer_add').slideUp();
        $('.cash_customer_name').slideDown();
        $('input[name="cash_customer_name"]').focus();
        /* $('.cheque_details').slideUp();
         $('[name="cheque_bank_name"]').val('');
         $('[name="cheque_acc_no"]').val('');
         $('[name="cheque_date"]').val(''); */
        //$('#cash_customer_name').prop('tabindex', '2');
        return '';
    }

    $('.cash_customer_name').slideUp();
    $('.new_shipping_address').slideUp();

    var ajaxURL = baseUrl + 'invoice/shipping_address_by_customer';
    var ajaxData = {
        customer_id: $(_this).val()
    }

    var data = ajaxRequest(ajaxURL, ajaxData, '');
    var html = '';
    if (data.customer.address != '')
        html = '<p class="alert alert-info not-dismissable"> Address: ' + data.customer.address + '</p>';
    else
        html = '<p class="alert alert-warning not-dismissable">No Address for this Customer</p>';

    $('.cust_address').html(html);
    $('.cust_address').slideDown();

    var _option = '<option value="">Use different Location</option>';
    var default_address = '';
    var shipping_address = '';
    $.each(data.addresses, function (key, value) {
        _option += '<option value="' + value.id + '"';
        if (value.is_default == '1') {
            default_address = value.id;
            shipping_address = '<p class="alert alert-info not-dismissable"> Delivery Address: ' + value.address + '</p>';
        }

        _option += ' data-address="' + value.address + '">' + value.location_name + '</option>';
    });

    $('#ship_to').html(_option);
    $('#ship_to').select2('val', default_address);

    $('.ship_to_address').html(shipping_address);
    $('.ship_to_address').slideDown();
    $('.reg_customer_add').slideDown();
    $('#payment_form select[name="payment_type"]').select2('val', data.customer.payment_term);
    if ($('#payment_form select[name="payment_type"] option:selected').data('is_cheque') == '1') {
        $('.cheque_details').slideDown();
    } else {
        $('.cheque_details').slideUp();
    }

    $('select[name="ship_to"]').select2('open');
    $('select[name="ship_to"]').focus();
    //$('select[name="ship_to"]').select2('val', '');
    //$('#cash_customer_name').prop('tabindex', '-1');
    //$('#ship_to').prop('tabindex', '2');
    if (default_address == '') {
        $('.new_shipping_address').slideDown();
    }

    //tabPressed ++;
}