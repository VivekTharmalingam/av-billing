$(document).ready(function () {
    
//    $('.clockpicker').clockpicker({
//            placement: 'top',
//        donetext: 'Done'
//    }); 
    $('#employee_name').change(function () {
        //$('#payslip_form').validate();
        //$('#payslip_form').submit();
    });

    change_payment_div($("#commission"));
    $("#commission").keyup(function () {
        change_payment_div($(this));
    });

    change_payment_type($("#commission_payment_mode"));
    $("#commission_payment_mode").change(function () {
        change_payment_type($(this));
    });

    change_payment_div($("#allow_amt"));
    $("#allow_amt").keyup(function () {
        change_payment_div($(this));
    });

    change_payment_type($("#allowance_payment_mode"));
    $("#allowance_payment_mode").change(function () {
        change_payment_type($(this));
    });

    change_payment_div($("#mbf"));
    $("#mbf").keyup(function () {
        change_payment_div($(this));
    });

    change_payment_type($("#mbf_payment_mode"));
    $("#mbf_payment_mode").change(function () {
        change_payment_type($(this));
    });

    change_payment_div($("#loan"));
    $("#loan").keyup(function () {
        change_payment_div($(this));
    });

    change_payment_type($("#loan_payment_mode"));
    $("#loan_payment_mode").change(function () {
        change_payment_type($(this));
    });

    change_payment_div($("#income_tax"));
    $("#income_tax").keyup(function () {
        change_payment_div($(this));
    });

    change_payment_type($("#income_payment_mode"));
    $("#income_payment_mode").change(function () {
        change_payment_type($(this));
    });

    change_payment_div($("#reimbursement"));
    $("#reimbursement").keyup(function () {
        change_payment_div($(this));
    });

    change_payment_type($("#reimbursement_payment_mode"));
    $("#reimbursement_payment_mode").change(function () {
        change_payment_type($(this));
    });

    $('.payment_mode').change(function () {
        if ($(this).val() == 'Cash') {
            $('.emp_acc_no').hide();
            $('.emp_acc_bank').hide();
            $('.comp_acc_bank').hide();
            $('.cheque_no').hide();
            $('.cheque_date').hide();
            $('#emp_acc_no').val(' ');
            $('#emp_acc_bank').val(' ');
            $('#comp_acc_bank').val(' ');
            $('#cheque_no').val(' ');
            $('#cheque_date').val(' ');
        }
        else {
            $('.emp_acc_no').show();
            $('.emp_acc_bank').show();
            $('.comp_acc_bank').show();
            $('.cheque_no').show();
            $('.cheque_date').show();
        }
    });

    var payment_mode = $('.payment_mode').val();
    if (payment_mode != '') {
        modechg(payment_mode);
    }

    $('.payment_mode').change(function () {
        var payment_mode = $(this).val();
        modechg(payment_mode);
    });

    function modechg(value) {
        if (value == 'Cash') {
            $('.emp_acc_no').hide();
            $('.emp_acc_bank').hide();
            $('.comp_acc_bank').hide();
            $('.cheque_no').hide();
            $('.cheque_date').hide();
            $('#emp_acc_no').val(' ');
            $('#emp_acc_bank').val(' ');
            $('#comp_acc_bank').val(' ');
            $('#cheque_no').val(' ');
            $('#cheque_date').val(' ');
        }
        else {
            $('.emp_acc_no').show();
            $('.emp_acc_bank').show();
            $('.comp_acc_bank').show();
            $('.cheque_no').show();
            $('.cheque_date').show();
        }

    }


    $('form').on('submit', function () {
        var $sbmtBtn = $('form [type="submit"]');
        if ($('.browse_logo').val() == '') {
            setGlobalMsg('Please choose logo!'); // success | warning | info | error (default)
            return false;
        }
        return true;
    });

    $.validator.addMethod('check_no_of_working_days', function (value, element) {
        var availableQty = parseFloat($(element).parents('.items').find('.act_quantity').val());
        if (availableQty >= parseFloat(value)) {
            return true;
        }
        else {
            if (!$('.SmallBox').length) {
                $.smallBox({
                    title: "Error",
                    content: "<i class='fa fa-warning'></i> <i> Received Quantity should not be greater than Actual Quantity!</i>",
                    color: "#A90329",
                    iconSmall: "fa fa-thumbs-o-down bounce animated",
                    timeout: 5000
                });
            }
            return false;
        }
    }, '');


    $.validator.addMethod("empExist", function (value, element) {

        var return_val = true;
        var empName = $('#employee_name').val();
        var mnth = $('#month').val();
        var year = $('#year').val();
        var id = $('.payslip_id').val();

        if (empName != '') {
            $.ajax({
                async: false,
                url: baseUrl + 'payslip/check_emp_exists',
                type: 'POST',
                data: {emp_name: empName, mnth: mnth, year: year, id: id},
            })
                    .done(function (data) {
                        if (data == '1') {
                            return_val = false
                        }
                    })
                    .fail(function (jqXHR, status_code, error_thrown) {
                        alert(error_thrown);
                    })
        }
        return return_val;
    });

    $.validator.addMethod("working_days", function (value, element) {

        var return_val = true;
        var days = $('#no_of_workingdays').val();

        if (days > 31) {
            return_val = false
        }
        return return_val;
    });

    $.validator.addMethod("hours_format", function (value, element) {

        var return_val = true;
        var hour = $('#ot_hour').val();

        if (!/^\d{2}:\d{2}:\d{2}$/.test(hour)) {
            return_val = false
        }
        return return_val;
    });
    
    $.validator.addMethod("employee_required", function (value, element) {

        var return_val = true;
        var emp_salary_type = $('#emp_salary_type').val();

        if (value == '' && emp_salary_type.trim().toLowerCase() == 'foreign') {
            return_val = false
        }
        return return_val;
    });

    var empForm = $("#payslip_form").validate({
        // Rules for form validation
        rules: {
            employee_name: {
                required: true
            },
            month: {
                required: true,
                empExist: true
            },
            year: {
                required: true,
                empExist: true
            },
            ot_hour: {
                required: true
            },
            no_of_workingdays: {
                required: true,
                working_days: true
            },
            emp_worked_days: {
                required: true,
            },
            cheque_no: {
                required: true
            },
            cheque_date: {
                required: true
            },
            pay_date: {
                required: true
            },
            basic_pay: {
                required: true
            },
            cpf_employer: {
                required: true
            },
            cpf_employee: {
                employee_required: true
            }
        },
        // Messages for form validation
        messages: {
            employee_name: {
                required: 'Please select Employee Name'
            },
            month: {
                required: 'Please select Month',
                empExist: 'Payslip Already Generated'
            },
            year: {
                required: 'Please select Year',
                empExist: 'Payslip Already Generated'
            },
            ot_hour: {
                required: 'Please enter OT Hour'
            },
            no_of_workingdays: {
                required: 'Please enter Number of Working Days',
                working_days: 'Working days should not exceed 31'
            },
            emp_worked_days: {
                required: 'Please enter Number of Worked Days',
            },
            cheque_no: {
                required: 'Please enter Cheque Number',
            },
            cheque_date: {
                required: 'Please select Cheque Date',
            },
            pay_date: {
                required: 'Please select Pay Date',
            },
            basic_pay: {
                required: 'Please enter Basic Pay',
            },
            cpf_employer: {
                required: 'Please enter CPF Amount From Employer',
            },
            cpf_employee: {
                employee_required: 'Please enter CPF Amount From Employee',
            }

        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

});


var addRowIndex = 0;
/* Add row functionality for Branch */
$(document).on('click', '.upload .add-row', function () {
    addRowIndex++;
    var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
    var itemObj = $(this).parents('.upload').clone();

    itemObj.find('.label').remove();
    itemObj.find('em.invalid').remove();
    itemObj.find('.state-success').removeClass('state-success');
    itemObj.find('.state-error').removeClass('state-error');
    itemObj.find('.invalid').removeClass('invalid');

    itemObj.find('input, select, textarea').each(function (index, element) {
        if ($(element).hasClass('item_hidden_ids')) {
            $(element).remove();
        }

        if ($(element).prop('name') != '') {
            var name = $(element).prop('name').split('[')[0];
            name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
            $(element).prop('name', name + '[' + addRowIndex + ']');
        }

        $(element).val('');
        if ($(element).prop('tagName').toLowerCase() == 'select') {
            if ($(element).prev().hasClass('select2')) {
                $(element).prev().remove();
                $(element).select2({width: '100%'});
            }
        }
    });

    var materialContainer = $(this).parents('.material-container');
    if ($(this).parent().find('.remove-row').length)
        $(this).remove();
    else
        $(this).removeClass('add-row fa-plus-circle').addClass('remove-row fa-minus-circle');

    if ($(itemObj).find('.remove-row').length == 0)
        $(itemObj).find('.add-row').after('<i class="fa fa-2x fa-minus-circle remove-row"></i>');

    $(materialContainer).find('.upload:last').after(itemObj);
});


/* Remove row functionality for Branches */
$(document).on('click', '.upload .remove-row', function () {
    var materialContainer = $(this).parents('.material-container');
    $(this).parents('.upload').remove();
    if (materialContainer.find('.upload:last').find('.add-row').length == 0) {
        materialContainer.find('.upload:last').find('.remove-row').before('<i class="fa fa-2x fa-plus-circle add-row"></i>');
    }

    if ((materialContainer.find('.upload').length == 1) && (materialContainer.find('.upload:last').find('.add-row').length)) {
        materialContainer.find('.upload:last').find('.remove-row').remove();
    }
});



function getEmployeeDetails() {
    var employee = $('#employee_name').val();
    if (employee != "") {
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: baseUrl + "payslip/get_employee_det/" + employee,
            //data: { company: company },
            success: function (responseData) {
                //console.log(responseData);
                $("#basic_pay").val(responseData.salary_amount);
                $("#ot_per_hour").val(responseData.overtime_amt_per_hour);
                $("#cpf_employer").val(responseData.cpf_from_employer);
                $("#cpf_employee").val(responseData.cpf_from_employee);
                $("#emp_acc_no").val(responseData.salary_account_no);
                $("#emp_acc_bank").val(responseData.salary_account_bank);
                $("#emp_salary_type").val(responseData.salary_type);
                var total = parseFloat(responseData.cpf_from_employer) + parseFloat(responseData.cpf_from_employee);
                $("#total_cpf").val(total.toFixed(2));
                var net = parseFloat(responseData.salary_amount) - parseFloat(responseData.cpf_from_employee);
                $("#net_amount").val(net.toFixed(2));
                getOTamount();
                netAmount();
            }
        });
    } else {
        $("#NewPayslipForm").reset();
    }
}

function getOTamount() {
    var ot_per_hour =0;
    if($("#ot_per_hour").val()!=''){
     ot_per_hour = $("#ot_per_hour").val();}
 
    var ot_hour=0;
    if($("#ot_hour").val()!=''){
        ot_hour = $("#ot_hour").val();
    }
    var per_mins=0;
    var total=0;
    if (ot_per_hour != "" && ot_hour != "") {
        ot_time = ot_hour.split(':');
        hr=0;min=0;
        if(ot_time[0]!='' &&  ot_time[0]!=undefined){
           hr =ot_time[0];
        }
        if(ot_time[1]!='' &&  ot_time[1]!=undefined){
           min = ot_time[1];
        }
        tot_mins = (parseInt(hr) * 60) + parseInt(min);
        per_mins = parseFloat(ot_per_hour / 60);
        total = parseFloat(tot_mins * per_mins);
        $("#over_time_amt").val(total.toFixed(2));
    }
}

function netAmount() {
    var basic = $("#basic_pay").val();

    var ot = 0;
    if ($("#over_time_amt").val() != "") {
        ot = $("#over_time_amt").val();
    }

    var allowance = 0;
    if ($("#allowance").val() != "") {
        allowance = $("#allowance").val();
    }

    var commission = 0;
    if ($("#commission").val() != "") {
        commission = $("#commission").val();
    }

    var cpf = 0;
    if ($("#cpf_employee").val() != "") {
        cpf = $("#cpf_employee").val();
    }

    var mbf = 0;
    if ($("#mbf").val() != "") {
        mbf = $("#mbf").val();
    }

    var loan = 0;
    if ($("#loan").val() != "") {
        loan = $("#loan").val();
    }

    var it = 0;
    if ($("#income_tax").val() != "") {
        it = $("#income_tax").val();
    }

    /*var addt=0;
     if(document.getElementById("addition").value!=""){
     addt=document.getElementById("addition").value;
     }*/

    var reimb = 0;
    if ($("#reimbursement").val() != "") {
        reimb = $("#reimbursement").val();
    }

    var net = 0;
    var addi = 0;
    var subt = 0;
    var par_day = 0;
    var total_pay = 0;

    var tot_working = $('#no_of_workingdays').val();
    var tot_worked = $('#emp_worked_days').val();

    if (tot_working != '' && tot_worked != '') {
        per_day = parseFloat(basic) / parseFloat(tot_working);
        total_pay = parseFloat(per_day) * parseFloat(tot_worked);
    }

    if (total_pay != '') {
        addi = parseFloat(total_pay) + parseFloat(ot) + parseFloat(allowance) + parseFloat(commission) + parseFloat(reimb);
    } else {
        addi = parseFloat(basic) + parseFloat(ot) + parseFloat(allowance) + parseFloat(commission) + parseFloat(reimb);
    }
    subt = parseFloat(cpf) + parseFloat(mbf) + parseFloat(loan) + parseFloat(it);
    net = addi - subt;
    $("#net_amount").val(net.toFixed(2));
}

function allowanceamt() {
    var tot = 0;
    $(".allow_amt").each(function () {
        if ($(this).val().trim() != '') {
            tot = tot + parseFloat($(this).val().trim());
        }
        $("#allowance").val(tot.toFixed(2));
    });
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    //if (charCode > 31 && (charCode < 48 || charCode > 57))
    if ((charCode > 45 && charCode < 58) || charCode == 8) {
        return true;
    }
    return false;
}

function check_no_of_working_days(value) {
    var return_val = 1;
    if (value > 31) {
        return_val = 0;
    }
    return return_val;
}

function compare_total_and_present_days() {
    var return_val = 1;
    var total_days = $('#no_of_workingdays').val();
    var present_days = $('#emp_worked_days').val();
    if (present_days > total_days) {
        return_val = 0;
    }
    return return_val;
}

function change_payment_div(_this) {
    var amt = parseInt($(_this).val());
    if (amt > 0) {
        $(_this).parents('.form').next('.div_payment').show("slow");
    } else {
        $(_this).parents('.form').next('.div_payment').hide("slow");
    }
}

function change_payment_type(_this) {
    var type = $(_this).val();
    if (type != 'Cheque') {
        $(_this).parents('.form').next('.payment_type').hide("slow");
    } else {
        $(_this).parents('.form').next('.payment_type').show("slow");
    }
}		