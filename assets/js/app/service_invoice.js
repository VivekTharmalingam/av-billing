function disable_payment() {
    if (!$('.payment_type').hasClass('payment_type_req'))
        $('.payment_type').removeClass('payment_type_req');

    $('#sales_order').data('form_submit', '1');
}

function calculate_discount() {
    var sub_total = ($('.sub_total').val() != '') ? parseFloat($('.sub_total').val()) : 0;
    var discount = ($('.discount').val() != '') ? parseFloat($('.discount').val()) : 0;
    if ($('.discount_type:checked').val() == '1') { // Discount percentage
        discount = parseFloat((discount / 100) * sub_total);
    }

    items.find('.discount_amount').html(discount.toFixed(2));
    items.find('.discount_amt').val(discount.toFixed(2));
    sub_total -= discount;
    items.find('.sub_total').val(sub_total.toFixed(2));
    calculate_total();
}

function calculate_total() {
    var sub_total = 0;
    $('.amount:not(.ignore_amount)').each(function (index, element) {
        sub_total += ($(element).val() != '') ? parseFloat($(element).val()) : 0;
    });
    $('.sub_total').val(sub_total.toFixed(2));

    var discount = ($('.discount').val() != '') ? parseFloat($('.discount').val()) : 0;
    if ($('.discount_type:checked').val() == '1') { // Discount percentage
        discount = parseFloat((discount / 100) * sub_total);
    }

    $('.discount_amount').html(discount.toFixed(2));
    $('.discount_amt').val(discount.toFixed(2));
    sub_total -= discount;

    var total_amount = sub_total;
    var gst_percentage = 0;
    var gst_amount = 0;
    if ($('.gst_type:checked').val() == '2') {
        gst_percentage = ($('#hidden_gst').val() != '') ? parseFloat($('#hidden_gst').val()) : 0;
        var gst_amount = parseFloat((gst_percentage / 100) * sub_total);
        total_amount = sub_total + gst_amount;
    }

    $('.gst_amount').val(gst_amount.toFixed(2));
    $('.gst_percentage').html(gst_percentage.toFixed(2));
    $('.total_amt').val(total_amount.toFixed(2));
    $('.payment_amount').html(total_amount.toFixed(2));
}

function reset_customer() {
	var ajaxURL = baseUrl + 'invoice/active_customers';
        var ajaxData = {}
        var data = ajaxRequest(ajaxURL, ajaxData, '');
	var _option = '';
	_option += '<option value="cash">Cash</option>';
	for(var i in data) {
		_option += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
	}
	
	$('#customer_name').html(_option);
	$('#customer_name').select2({width: '100%'});
}

function hide_product_code_msg() {
    $('.product_code_label').slideUp();
}

function hide_error_description_msg() {
    $('.error_description').slideUp();
}

var exc_product_codes = get_service_items(1);
var product_codes = get_service_items(2);
var product_description = get_service_items(3);
var items;

function get_service_items(form_type) {
    var ajaxURL = baseUrl + 'invoice/search_service_item';
    var service_id = $("select.service_id").val();
    var ajaxData = {
        service_id: service_id,
        form_type: form_type
    }

    return ajaxRequest(ajaxURL, ajaxData, '');
}

function valid_exc_item_code(item_code) {
	var item_code_exists = false;
	for(var i in exc_product_codes) {
		if (exc_product_codes[i].product_text.trim().toLowerCase() == item_code.val().trim().toLowerCase()) {
			item_code_exists = true;
			break;
		}
	}
	
	if (!item_code_exists) {
		item_code.val('');
		item_code.focus();
		return false;
	}
}

function valid_item_code(item_code) {
	var item_code_exists = false;
        for (var i in product_codes) {
            if (product_codes[i].pdt_code.trim().toLowerCase() == item_code.val().trim().toLowerCase()) {
                item_code_exists = true;
                break;
            }
        }

        if (!item_code_exists) {
            item_code.val('');
            item_code.focus();
            return false;
        }
}

function set_exc_item_autocomplete() {
        $(".exc_product_code").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }
		
        $(this).on('change', function() {
                valid_exc_item_code($(this));
        });

        $(this).autocomplete({
            source: exc_product_codes,
            focus: function (event, ui) {
            },
            select: function (event, ui) {
                var items = $(this).closest('.exc-items');
                if ($('.exc-product_details').find('.exc_product_id[value="' + ui.item.item_id + '"]').length) {
                    // Item already exists
                    if ($(this).data('value').trim() != ui.item.value.trim()) {
                        items = $('.exc-product_details').find('.exc_product_id[value="' + ui.item.item_id + '"]').closest('.exc-items');
                        $(this).val('');
                        $(this).closest('.exc-items').find('.product_code_label').slideDown();
                        setTimeout(hide_product_code_msg, 3000)
                        return false;
                    } else {
                        $(this).val(ui.item.value.trim());
                    }
                } else {
                    $(this).data('value', ui.item.product_text.trim());
                    $(this).val(ui.item.product_text.trim());
                    items.find('.ser_exc_itemid').val(ui.item.id);
                    items.find('.exc_product_id').val(ui.item.item_id);
                    items.find('.exc_product_code').data('value', ui.item.product_text.trim());
                    items.find('.exr_code_cls').html(ui.item.exr_code.trim());
                    items.find('.exc_product_description').val(ui.item.product_description.trim());
                    items.find('.exc_price').val(ui.item.price);
                    items.find('.exc_cost').val(ui.item.unit_cost);
                    items.find('.exc_quantity').val(ui.item.quantity);
					var amount = ui.item.total;
                    var item_discount = (items.find('.item_discount').val() != '') ? parseFloat(items.find('.item_discount').val()) : 0;
                    item_discount = (items.find('.item_discount_type:checked').length) ? amount * parseFloat((item_discount / 100).toFixed(2)) : item_discount;
                    amount -= item_discount;
                    items.find('.item_discount_amt').html(item_discount.toFixed(2));
                    items.find('.item_discount_amt').val(item_discount.toFixed(2));
                    
                    items.find('.exc_amount').val(amount.toFixed(2));
					
                    //items.find('.exc_amount').val(ui.item.total);
                }
                calculate_total();
                
                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || e.keyCode;
                if (charCode == 13) 
                    items.find('.exc_quantity').focus();
                else if (event.type == 'autocompleteselect') 
                    items.find('.exc_product_description').focus();
                
                /* To focus quantity of particular item end */
                
                return false;
            }
        });

        $(this).addClass('hasAutocomplete');
    });
    
}

function set_inventory_item_autocomplete() {
    $(".product_code").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }

        $(this).on('change', function () {
            valid_item_code($(this));
        });

        $(this).autocomplete({
			source: function(request, response) {
				var results = $.ui.autocomplete.filter(product_codes, request.term);
				response(results.slice(0, 15));
			},
            focus: function (event, ui) {
				show_available_quantity(event.currentTarget, ui.item.branch_products);
            },
			open: function() {
				hide_available_quantity();
            },
			close: function() {
				hide_available_quantity();
				if ($(this).val().trim().length) 
					$(this).closest('.inv-items').find('.product_description').trigger('focus');
			},
            select: function (event, ui) {
                var items = $(this).closest('.inv-items');
                var product_id_name = items.find('.product_id').prop('name');
                if ($('.product_details').find('.product_id[value="' + ui.item.item_id + '"]:not(.product_id[name="' + product_id_name + '"])').length) {
                    // Item already exists
                    if ($(this).data('value').trim() != ui.item.value.trim()) {
                        items = $('.product_details').find('.product_id[value="' + ui.item.item_id + '"]').closest('.inv-items');

                        $(this).val('');
                        $(this).closest('.inv-items').find('.product_code_label').slideDown();
                        setTimeout(hide_product_code_msg, 3000)
                        return false;
                    }
                    else {
                        $(this).val(ui.item.value.trim());
                        des = ui.item.pdt_name;
                        if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                            des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                        }
                        items.find('.product_description').val(des.trim());
                    }
                }
                else {
                    $(this).data('value', ui.item.pdt_code.trim());
                    $(this).val(ui.item.pdt_code.trim());
                    items.find('.ser_invventory_id').val(ui.item.id);
                    items.find('.category_id').val(ui.item.category_id);
                    items.find('.product_id').val(ui.item.item_id);
                    items.find('.product_code').data('value', ui.item.pdt_code.trim());
                    items.find('.product_description').val(ui.item.product_description.trim());
                    items.find('.price').val(ui.item.price);
                    items.find('.cost').val(ui.item.unit_cost);
                    items.find('.quantity').val(ui.item.quantity);
					var amount = ui.item.total;
                    var item_discount = (items.find('.item_discount').val() != '') ? parseFloat(items.find('.item_discount').val()) : 0;
                    item_discount = (items.find('.item_discount_type:checked').length) ? amount * parseFloat((item_discount / 100).toFixed(2)) : item_discount;
                    amount -= item_discount;
                    items.find('.item_discount_amt').html(item_discount.toFixed(2));
                    items.find('.item_discount_amt').val(item_discount.toFixed(2));
                    
                    items.find('.amount').val(amount.toFixed(2));
					
                    //items.find('.amount').val(ui.item.total);
                }
                calculate_total();

                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || event.keyCode;
                if (charCode == 13)
                    items.find('.quantity').focus();
                else if (event.type == 'autocompleteselect')
                    items.find('.product_description').focus();

                /* To focus quantity of particular item end */
                return false;
            }
        });

        $(this).addClass('hasAutocomplete');
    });
	
	$(".product_description").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }

        $(this).on('change', function () {
            //valid_item_description($(this));
        });

        $(this).autocomplete({
			source: function(request, response) {
				var results = $.ui.autocomplete.filter(product_description, request.term);
				response(results.slice(0, 15));
			},
            focus: function (event, ui) {
				show_available_quantity(event.currentTarget, ui.item.branch_products);
            },
			open: function() {
				hide_available_quantity();
            },
			close: function() {
				hide_available_quantity();
			},
            select: function (event, ui) {
                var items = $(this).closest('.inv-items');
                var product_id_name = items.find('.product_id').prop('name');
                if ($('.product_details').find('.product_id[value="' + ui.item.item_id + '"]:not(.product_id[name="' + product_id_name + '"])').length) {
                    // Item already exists
                    if ($(this).data('value').trim() != ui.item.value.trim()) {
                        items = $('.product_details').find('.product_id[value="' + ui.item.item_id + '"]').closest('.inv-items');
                        $(this).val($(this).data('value').trim());
                        $(this).closest('.inv-items').find('.product_code_label').slideDown();
                        setTimeout(hide_error_description_msg, 3000);
						items.find('.quantity').focus();
                        return false;
                    }
                    else {
                        $(this).val(ui.item.value.trim());
                    }
                }
                else {
                    $(this).data('value', ui.item.product_description.trim());
                    $(this).val(ui.item.product_description.trim());
                    items.find('.ser_invventory_id').val(ui.item.id);
                    items.find('.category_id').val(ui.item.category_id);
                    items.find('.product_id').val(ui.item.item_id);
                    items.find('.product_code').data('value', ui.item.pdt_code.trim());
                    items.find('.product_description').val(ui.item.product_description.trim());
                    items.find('.price').val(ui.item.price);
                    items.find('.cost').val(ui.item.unit_cost);
                    items.find('.quantity').val(ui.item.quantity);
					var amount = ui.item.total;
                    var item_discount = (items.find('.item_discount').val() != '') ? parseFloat(items.find('.item_discount').val()) : 0;
                    item_discount = (items.find('.item_discount_type:checked').length) ? amount * parseFloat((item_discount / 100).toFixed(2)) : item_discount;
                    amount -= item_discount;
                    items.find('.item_discount_amt').html(item_discount.toFixed(2));
                    items.find('.item_discount_amt').val(item_discount.toFixed(2));
                    
                    items.find('.amount').val(amount.toFixed(2));
					
                    //items.find('.amount').val(ui.item.total);
                }
                calculate_total();

                /* To focus quantity of particular item start */
                items.find('.quantity').focus();
                /* To focus quantity of particular item end */
                return false;
            }
        });

        $(this).addClass('hasAutocomplete');
    });
}

function reset_item() {
	exc_product_codes = get_service_items(1);
        product_codes = get_service_items(2);
        product_description = get_service_items(3);
	
	$(".product_code, .exc_product_code, .product_description").removeClass('hasAutocomplete');
	set_exc_item_autocomplete();
	set_inventory_item_autocomplete();
}

function delivery_to_change_focus() {
    if ($('#ship_to').val() == '') 
        $('input[name="location_name"]').focus();
    else 
        $('#your_reference').focus();
}

function focus_quantity(qty) { 
    $(qty).focus();
}

function open_serial_no_popup($_this) {
    var open_popup = false;
    /* Set Serial No. */
    var qty_count = ($_this.closest('.calculate_items').find('.quantity').val() != '') ? parseFloat($_this.closest('.calculate_items').find('.quantity').val()) : 0;    
    var product_code = $_this.closest('.calculate_items').find('.product_code').val();     
    var serial_no = ($_this.closest('.calculate_items').find('.serial_no').val() != '') ? $_this.closest('.calculate_items').find('.serial_no').val().split(',') : [];     
    var html = '', temp='';
    this_item_row = '';
    if((qty_count > 0) && product_code != '') {
        open_popup = true;
        this_item_row = $_this;
		if (product_code !== undefined)
			$('.serial_no_title').html('Add Serial No. for '+product_code);
		else 
			$('.serial_no_title').html('Add Serial No.');
        
        for (var i=0; i < qty_count; i++) {
			var ths_item_sl_no = (serial_no[i] != undefined) ? serial_no[i] : "";
			var inc_i = i+1;
			html += '<div class="col-md-12 itemsernos">';
			
			html += '<div class="col-md-1">';
			html += '<label class="item_id" style="padding-top: 5px;">' + inc_i + '</label>';
			html += '</div>';

			html += '<div class="col-md-11">';
			html += '<label class="input" style="width:100%;">';
			html += '<input type="text" name="ser_no_txt[]" class="ser_no_txt form-control" onkeypress="return isRmvComma(event)" value="'+ths_item_sl_no+'" />';
			html += '</label>';
			html += '</div>';
			
			html += '</div>';
        }
        $('.item_ser_nos').html(html);
    }
    /* End */

    if (open_popup) {
        $("#grn_serial_no").modal({
            backdrop: "static",
            keyboard: true,
            show: true
        });
    }
}

function set_serial_no_input() {
    var ser_no_txtall = '';
    if(this_item_row != '') {
        $('.ser_no_txt').each(function(){
            if($(this).val().trim() != "") {
                ser_no_txtall += $(this).val().trim()+',';
            }
        });
        ser_no_txtall = (ser_no_txtall != '') ? ser_no_txtall.replace(/^,|,$/g,'') : '';
        this_item_row.closest('.calculate_items').find('.serial_no').val(ser_no_txtall);    
    }
}

function isRmvComma(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 44) {
        return false;
    }
    return true;
}

function calculate_item_total(_this) {
    var items = $(_this).closest('.calculate_items');
    var quantity = (items.find('.quantity').val() != '') ? parseFloat(items.find('.quantity').val()) : 0;
    var price = (items.find('.price').val() != '') ? parseFloat(items.find('.price').val()) : 0;
    
    var amount = parseFloat((quantity * price).toFixed(2));
	var item_discount = (items.find('.item_discount').val() != '') ? parseFloat(items.find('.item_discount').val()) : 0;
    item_discount = (items.find('.item_discount_type:checked').length) ? amount * parseFloat((item_discount / 100).toFixed(2)) : item_discount;
	amount -= item_discount;
	items.find('.item_discount_amt').html(item_discount.toFixed(2));
    items.find('.item_discount_amt').val(item_discount.toFixed(2));
	items.find('.amount').val(amount.toFixed(2));
    calculate_total();
}

var addRowIndex = 0;
var scanItem = 0;
var tabPressed = 0;
$(document).ready(function () {
    localStorage.clear();
    
    $('#service_id').on('change', function () {
        var serv_val = $("select.service_id").val();
        var url = baseUrl + 'invoice/add/';
        var form = document.createElement("form");
        $(form).attr("action", url)
               .attr("name", "sales_order_new")
               .attr("id", "sales_order_new")
               .attr("method", "post");
        $(form).html('<input type="hidden" name="service_id" value="' + serv_val + '" />');
        document.body.appendChild(form);
        $(form).submit();
        document.body.removeChild(form); 
    });    
    
    /* Function to focus customer name on first tab */
    if ($('.edit_form').length == 0) {
        $(document).keydown(function (e) {
           if ((e.which == 9) && (tabPressed == 0)) {
               $('#customer_name').select2('open');
               $('#customer_name').select2('val', '');
               $('#customer_name').focus();
               tabPressed ++;
           }
        });
    }
	
    $( "#your_reference" ).focus(function() {
        $('#your_reference').data('focused', true);
    });
    
    $( "#remarks" ).focus(function() {
        if (!$('#your_reference').data('focused')) {
            $('#your_reference').focus();
        }
    });
    
    $(document).on('focus', ".quantity, .price, .cost", function() {
        $(this).select();
    });
    
    $(document).on('change', ".show_in_print", function() {
        if ($(this).prop('checked') == false) {
			$(this).closest('.calculate_items').find('.amount').addClass('ignore_amount');
		}
		else {
			$(this).closest('.calculate_items').find('.amount').removeClass('ignore_amount');
		}
		calculate_total();
    });
    
    $('.so_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    
    
    set_exc_item_autocomplete();
    set_inventory_item_autocomplete();
    get_cashCustomer($('#customer_name'));
    
    $(document).on('blur', 'input.exc_product_code', function (e) {
        var item = {};
        var product_code = $(this).val().trim();
        if (product_code != '') {
            for (var i in exc_product_codes) {
                if ((product_code != '') && (exc_product_codes[i].label.trim() == product_code)) {
                    item = exc_product_codes[i];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: item});
                    break;
                }
            }
        }
    });
    
	$(document).on('focusout', 'input.product_description', function (e) {
        hide_available_quantity();
    });
	
    $(document).on('focus', 'input.product_code, input.product_description', function (e) {
        var item = {};
        var product_code = $(this).closest('.inv-items').find('.product_code').val().trim();
        if (product_code != '') {
            for (var i in product_codes) {
                if ((product_code != '') && (product_codes[i].label.trim() == product_code)) {
                    item = product_codes[i];
                    show_available_quantity($(this), product_codes[i].branch_products);
                    break;
                }
            }
        }
		else {
            hide_available_quantity();
        }
    });
	
    $(document).on('blur', 'input.product_code', function (e) {
        var item = {};
        var product_code = $(this).val().trim();
        if (product_code != '') {
            for (var i in product_codes) {
                if ((product_code != '') && (product_codes[i].label.trim() == product_code)) {
                    item = product_codes[i];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: item});
                    break;
                }
            }
        }
		else {
            hide_available_quantity();
        }
    });

    if($('#customer_name option:selected').val() == 'cash') {
        get_cashCustomer($('#customer_name'));
    }

    $('#customer_name').on('change', function (e) {
        get_cashCustomer($(this));
    });
    
    $('#ship_to').on('change', function () {
        if ($(this).val() == '') {
            $('.new_shipping_address').slideDown();
            $('.ship_to_address').slideUp();
        }
        else {
            $('.new_shipping_address').slideUp();
            var ship_to_address = $(this).find('option:selected').data('address');
            html = '';
            if (ship_to_address != '')
                html = '<p class="alert alert-info not-dismissable"> Delivery Address: ' + ship_to_address + '</p>';
            
            $('.ship_to_address').html(html);
            $('.ship_to_address').slideDown();
        }
        
        setTimeout(delivery_to_change_focus, 100);
        //return false;
    });
    
    $(document).on('keyup', '.quantity, .price, .item_discount', function () {
		calculate_item_total(this);
    });
	
	$(document).on('change', '.item_discount_type', function () {
        calculate_item_total(this);
    });
	
    $(document).on('keyup', '.discount, .amount', function () {
        calculate_total();
    });

    $(document).on('change', '.discount_type, .discount', function () {
        calculate_total();
    });

    $(document).on('change', '.gst_type', function () {
        calculate_total();
    });
    
    calculate_total();

    addRowIndex = $('.service-items').length - 1;
    /* Add row functionality for Service Cost */
    $(document).on('click', '.service-items .serv-add-row', function () {
        addRowIndex ++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.service-items').clone();
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');
        itemObj.find('.make-blank').hide();
        
        itemObj.find('input, select, textarea').each(function (index, element) {
            if ($(element).hasClass('item_hidden_ids')) {
                $(element).remove();
            }
			
            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndex + ']');
            }
            $(element).val('');
        });

        var materialContainer = $(this).parents('.service-material-container');
        
        if ($(this).parent().find('.serv-remove-row').length)
            $(this).remove();
        else
            $(this).removeClass('serv-add-row fa-plus-circle').addClass('serv-remove-row fa-minus-circle');

        if ($(itemObj).find('.serv-remove-row').length == 0)
            $(itemObj).find('.serv-add-row').after('<i class="fa fa-2x fa-minus-circle serv-remove-row"></i>');

        $(materialContainer).find('.service-items:last').after(itemObj);
        
        $('.service-items:last .serv_product_code').focus();
    });
	
    /* Remove row functionality for Service Cost */
    $(document).on('click', '.service-items .serv-remove-row', function () {
        var materialContainer = $(this).parents('.service-material-container');
        $(this).parents('.service-items').remove();
        if (materialContainer.find('.service-items:last').find('.serv-add-row').length == 0) {
            materialContainer.find('.service-items:last').find('.serv-remove-row').before('<i class="fa fa-2x fa-plus-circle serv-add-row"></i>');
        }

        if ((materialContainer.find('.service-items').length == 1) && (materialContainer.find('.service-items:last').find('.serv-add-row').length)) {
            materialContainer.find('.service-items:last').find('.serv-remove-row').remove();
        }
        calculate_total();
        $('.service-items:last .serv_product_code').focus();
    });
    
    
    addRowIndexExc = $('.exc-items').length - 1;
    /* Add row functionality for Exchange Order */
    $(document).on('click', '.exc-items .exc-add-row', function () {
        addRowIndexExc++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.exc-items').clone();
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');
        itemObj.find('.exr_code_cls').html('&nbsp;');
		
		itemObj.find('input, select, textarea').each(function (index, element) {
            if ($(element).hasClass('item_hidden_ids')) {
                $(element).remove();
            }

            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndexExc + ']');
            }
            $(element).val('');

            if ($(element).hasClass('product_description'))
                $(element).data('value', '');

            if ($(element).hasClass('hasAutocomplete'))
                $(element).removeClass('hasAutocomplete')
            
            if ($(element).hasClass('exc_product_code')) {
                $(element).data('value', '');
            }
        });
		
		var id = itemObj.find('input.show_in_print').prop('id').split('-')[0];
        if (formType == 'edit') {
			id = (id.indexOf('new') == -1) ? 'new_' + id : id;
			id += '-' + addRowIndexExc;
        }
        else {
            id += '-' + addRowIndexExc;
        }
		
		itemObj.find('.item_discount_type').prop('checked', true);
        itemObj.find('.item_discount_type').val(1);
        itemObj.find('.item_discount_amt').html('0.00');
        itemObj.find('.item_discount_amt').val('0.00');

        if (formType == 'edit') {
            itemObj.find('.item_discount_type').prop('id', 'new_item_discount_type-' + addRowIndexExc);
            itemObj.find('.onoffswitch-label').prop('for', 'new_item_discount_type-' + addRowIndexExc);
        }
        else {
            itemObj.find('.item_discount_type').prop('id', 'item_discount_type-' + addRowIndexExc);
            itemObj.find('.onoffswitch-label').prop('for', 'item_discount_type-' + addRowIndexExc);
        }
		
		itemObj.find('input.show_in_print').prop('id', id);
		itemObj.find('label.show_in_print').prop('for', id);
		itemObj.find('input.show_in_print').prop('checked', false);
        itemObj.find('input.show_in_print').val(0);
		
        var materialContainer = $(this).parents('.material-container');
        var optionsLength = exc_product_codes.length;
        if (optionsLength > $(materialContainer).find('.exc-items').length) {
        if ($(this).parent().find('.exc-remove-row').length)
            $(this).remove();
        else
            $(this).removeClass('exc-add-row fa-plus-circle').addClass('exc-remove-row fa-minus-circle');

        if ($(itemObj).find('.exc-remove-row').length == 0)
            $(itemObj).find('.exc-add-row').after('<i class="fa fa-2x fa-minus-circle exc-remove-row"></i>');

        $(materialContainer).find('.exc-items:last').after(itemObj);
        }
        else if ((optionsLength == $(materialContainer).find('.exc-items').length) || (optionsLength == 0)) {
            var smallBoxTitle = 'Sorry ! ...';
            var smallBoxMsg = '<i class="fa fa-info-circle"></i> <i>You can\'t add more...<br> Because you have';
            smallBoxMsg += (optionsLength > 0) ? ' only ' + optionsLength : ' no';
            smallBoxMsg += ($(materialContainer).find('.exc-items').length > 1) ? ' items' : ' item';
            smallBoxMsg += '.</i>';

            if ($('#divSmallBoxes').html().length == 0)
                showSmallBox(smallBoxTitle, smallBoxMsg);
        }
        
        set_exc_item_autocomplete();
        $('.exc-items:last .exc_product_code').focus();
    });

    /* Remove row functionality for Exchange Order */
    $(document).on('click', '.exc-items .exc-remove-row', function () {
        var materialContainer = $(this).parents('.material-container');
        $(this).parents('.exc-items').remove();
        if (materialContainer.find('.exc-items:last').find('.exc-add-row').length == 0) {
            materialContainer.find('.exc-items:last').find('.exc-remove-row').before('<i class="fa fa-2x fa-plus-circle exc-add-row"></i>');
        }

        if ((materialContainer.find('.exc-items').length == 1) && (materialContainer.find('.exc-items:last').find('.exc-add-row').length)) {
            materialContainer.find('.exc-items:last').find('.exc-remove-row').remove();
        }
        calculate_total();
        $('.exc-items:last .exc_product_code').focus();
    });
    
    addRowIndexInven = $('.inv-items').length - 1;
    /* Add row functionality for Inventory */
    $(document).on('click', '.inv-items .inv-add-row', function () {
        addRowIndexInven++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.inv-items').clone();
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');
		
		if (formType == 'edit') {
			var id = itemObj.find('.show_in_print').prop('id').split('-')[0];
			id = (id.indexOf('new') == -1) ? 'new_' + id : id;
			id += '-' + addRowIndexInven;
			itemObj.find('.show_in_print').prop('id', id);
			itemObj.find('label.onoffswitch-label').attr('for', id);
		}
		
        itemObj.find('input, select, textarea').each(function (index, element) {
            if ($(element).hasClass('item_hidden_ids')) {
                $(element).remove();
            }

            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndexInven + ']');
            }
            $(element).val('');

            if ($(element).hasClass('product_description'))
                $(element).data('value', '');

            if ($(element).hasClass('hasAutocomplete'))
                $(element).removeClass('hasAutocomplete')

            if ($(element).hasClass('product_code')) {
                $(element).data('value', '');
            }   
        });
		
		var id = itemObj.find('input.show_in_print').prop('id').split('-')[0];
        if (formType == 'edit') {
			id = (id.indexOf('new') == -1) ? 'new_' + id : id;
			id += '-' + addRowIndexExc;
        }
        else {
            id += '-' + addRowIndexExc;
        }
		
		itemObj.find('.item_discount_type').prop('checked', true);
        itemObj.find('.item_discount_type').val(1);
        itemObj.find('.item_discount_amt').html('0.00');
        itemObj.find('.item_discount_amt').val('0.00');

        if (formType == 'edit') {
            itemObj.find('.item_discount_type').prop('id', 'new_item_discount_type-' + addRowIndexExc);
            itemObj.find('.onoffswitch-label').prop('for', 'new_item_discount_type-' + addRowIndexExc);
        }
        else {
            itemObj.find('.item_discount_type').prop('id', 'item_discount_type-' + addRowIndexExc);
            itemObj.find('.onoffswitch-label').prop('for', 'item_discount_type-' + addRowIndexExc);
        }
		
		itemObj.find('input.show_in_print').prop('id', id);
		itemObj.find('label.show_in_print').prop('for', id);
		itemObj.find('input.show_in_print').prop('checked', false);
        itemObj.find('input.show_in_print').val(0);
        
        var materialContainer = $(this).parents('.inv-material-container');
        var optionsLength = product_codes.length;
        if (optionsLength > $(materialContainer).find('.inv-items').length) {
        if ($(this).parent().find('.inv-remove-row').length)
            $(this).remove();
        else
            $(this).removeClass('inv-add-row fa-plus-circle').addClass('inv-remove-row fa-minus-circle');

        if ($(itemObj).find('.inv-remove-row').length == 0)
            $(itemObj).find('.inv-add-row').after('<i class="fa fa-2x fa-minus-circle inv-remove-row"></i>');

        $(materialContainer).find('.inv-items:last').after(itemObj);
        }
        else if ((optionsLength == $(materialContainer).find('.inv-items').length) || (optionsLength == 0)) {
            var smallBoxTitle = 'Sorry ! ...';
            var smallBoxMsg = '<i class="fa fa-info-circle"></i> <i>You can\'t add more...<br> Because you have';
            smallBoxMsg += (optionsLength > 0) ? ' only ' + optionsLength : ' no';
            smallBoxMsg += ($(materialContainer).find('.inv-items').length > 1) ? ' items' : ' item';
            smallBoxMsg += '.</i>';

            if ($('#divSmallBoxes').html().length == 0)
                showSmallBox(smallBoxTitle, smallBoxMsg);
        }
        
        set_inventory_item_autocomplete();
        $('.inv-items:last .product_code').focus();
    });

    /* Remove row functionality for Inventory */
    $(document).on('click', '.inv-items .inv-remove-row', function () {
        var materialContainer = $(this).parents('.inv-material-container');
        $(this).parents('.inv-items').remove();
        if (materialContainer.find('.inv-items:last').find('.inv-add-row').length == 0) {
            materialContainer.find('.inv-items:last').find('.inv-remove-row').before('<i class="fa fa-2x fa-plus-circle inv-add-row"></i>');
        }

        if ((materialContainer.find('.inv-items').length == 1) && (materialContainer.find('.inv-items:last').find('.inv-add-row').length)) {
            materialContainer.find('.inv-items:last').find('.inv-remove-row').remove();
        }
        calculate_total();
        $('.inv-items:last .product_code').focus();
    });
    

    $.validator.addMethod('product_code_required', function (value, element) {
        var tem_val=0;
        $('.product_code_valcls').each(function(){
            if($(this).val()!=""){
                tem_val=1;
            }
        });
        return (tem_val == 1) ? true : false;
    }, 'Please select at&nbsp;least One Item!');
    
    $.validator.addMethod('payment_type_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please select Payment Type!');
	
    $.validator.addMethod('your_reference_exist', function (value, element) {
        var formType = $(element).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var return_val = true;
        var your_reference = $(element).val();
        var invoice_id = (formType == 'edit') ? $('input[name="so_id"]').val() : '';
        if (your_reference != '') {
			var ajaxURL = baseUrl + 'invoice/check_your_reference';
			var ajaxData = {
				your_reference: your_reference,
				invoice_id: invoice_id
			}

			var data = ajaxRequest(ajaxURL, ajaxData, '');
			if (data.exist >= 1) {
				return_val = false;
			}
        }
        return return_val;
    });

    $.validator.addClassRules({
        product_code: {
            product_code_required: true
        },
        payment_type_req: {
            payment_type_required: true
        }
    });

    $('select[name="customer_name"], select[name="ship_to"], select.category_id, select.product_id, select.quantity').on('change', function () {
        $('#sales_order').validate().element(this);
    });

    $('input[name="so_date"], input[name="cheque_date"], input[name="dd_date"]').on('changeDate', function () {
        $('#sales_order').validate().element(this);
    });

    $("#sales_order").validate({
        // Rules for form validation
        invalidHandler: function (event, validator) {
            $('body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top - 100
            },
            '500',
                    'swing'
                    );
        },
        rules: {
            customer_name: {
                required: true
            },
            your_reference: {
                your_reference_exist: true
            },
            so_date: {
                required: true
            },
            cheque_bank_name: {
                required: true
            },
            cheque_acc_no: {
                required: true
            },
            cheque_date: {
                required: true
            },
            dd_no: {
                required: true
            },
            dd_date: {
                required: true
            }
        },
        // Messages for form validation
        messages: {
            customer_name: {
                required: 'Please select Sold To!'
            },
            your_reference: {
                your_reference_exist: 'Customer reference already exists!'
            },
            so_date: {
                required: 'Please select Date!'
            },
            cheque_bank_name: {
                required: 'Please enter Bank Name!'
            },
            cheque_acc_no: {
                required: 'Please enter Cheque No!'
            },
            cheque_date: {
                required: 'Please enter Cheque Date!'
            },
            dd_no: {
                required: 'Please enter DD No!'
            },
            dd_date: {
                required: 'Please enter DD Date!'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
			if (scanItem) {
				scanItem = 0;
				return false;
			}
			
			// var payment_terms defined at the bottom of add/edit form
			var go_to_print = false;
			for (var i in payment_terms) {
				if ($('#payment_terms').val() == i) {
					go_to_print = true;
				}
			}
			
			if (go_to_print) {
				form.submit();
				return true;
			}
			
			tabPressed ++;
            $('#sales_form').hide();
            $('.payment_type').removeClass('payment_type_req');
            $('#payment_form').show();
            $('body').animate({
                    scrollTop: 0
                },
                '500',
                'swing'
            );

            $('#pay_now').focus();
            // do other things for a valid form
			if ($('#sales_order').data('form_submit') == '0') {
                return false;
            }

            form.submit();
        }
    });

    /* Salse Payment operation start */
    $('#pay_now').on('click', function () {
        if (!$('.payment_type').hasClass('payment_type_req'))
            $('.payment_type').addClass('payment_type_req');

        $('#sales_order').data('form_submit', '1');
    });

    $('#skip_payment').on('click', function () {
        $('#sales_order').data('form_submit', '1');
    });

    $('#payment_type').on('change', function () {
        $('.cheque_details').slideUp();        
        if ($(this).find('option:selected').data('is_cheque') == '1')
            $('.cheque_details').slideDown();
        $('#pay_now').focus();        
        var group = $('.payment_type option:selected').attr('data-group');
        $('#data_group').val(group)
    });
    /* Salse Payment operation end */
	
	$(document).on('click', '.item-serialnum-popup', function () {
        open_serial_no_popup($(this));
    });
	
	$(document).on('click', '.serial_no_close, .close', function () {
        set_serial_no_input();
    });
    
    $(document).on('blur', 'input.quantity', function (e) {
        var serial_no = ($(this).closest('.calculate_items').find('.serial_no').val() != '') ? $(this).closest('.calculate_items').find('.serial_no').val().split(',') : [];     
        var qnty = ($(this).val() != '') ? parseFloat($(this).val()) : 0;   
        var item_sl_no_txt = '';
        if(serial_no.length>qnty) {
            item_sl_no_txt = serial_no.slice(0, qnty);
        }
        if((qnty>0 && item_sl_no_txt!='') || (qnty==0 && item_sl_no_txt=='')) {
            $(this).closest('.calculate_items').find('.serial_no').val(item_sl_no_txt);
        }
    });
});

function show_sales_form() {
    $('#sales_order').data('form_submit', '0');
    $('#payment_form').hide();
    $('#sales_form').show();
}


function get_cashCustomer(_this) {
    
    if ($(_this).val() == 'cash') {
        $('#ship_to').find('option:not(:first)').remove();
        $('#ship_to').select2('val', '');
        $('#payment_type').select2('val', '');
        $('.reg_customer_add').slideUp();
        $('.cash_customer_name').slideDown();
        $('input[name="cash_customer_name"]').focus();
        $('.cheque_details').slideUp();
//        $('[name="cheque_bank_name"]').val('');
        $('[name="cheque_acc_no"]').val('');
        $('[name="cheque_date"]').val('');
        return '';
    }
    
    $('.cash_customer_name').slideUp();
    $('.new_shipping_address').slideUp();
    
    var ajaxURL = baseUrl + 'invoice/shipping_address_by_customer';
    var ajaxData = {
        customer_id: $(_this).val()
    }

    var data = ajaxRequest(ajaxURL, ajaxData, '');
    var html = '';
    if (data.customer.address != '')
        html = '<p class="alert alert-info not-dismissable"> Address: ' + data.customer.address + '</p>';
    else
        html = '<p class="alert alert-warning not-dismissable">No Address for this Customer</p>';
    
    $('.cust_address').html(html);
    $('.cust_address').slideDown();
    
    var _option = '<option value="">Use different Location</option>';
    var default_address = '';
    var shipping_address = '';
    $.each(data.addresses, function (key, value) {
        _option += '<option value="' + value.id + '"';
        if (value.is_default == '1') {
            default_address = value.id;
            shipping_address = '<p class="alert alert-info not-dismissable"> Delivery Address: ' + value.address + '</p>';
        }

        _option += ' data-address="' + value.address + '">' + value.location_name + '</option>';
    });

    $('#ship_to').html(_option);
    $('#ship_to').select2('val', default_address);
    
    $('.ship_to_address').html(shipping_address);
    $('.ship_to_address').slideDown();
    $('.reg_customer_add').slideDown();
    $('#payment_form select[name="payment_type"]').select2('val', data.customer.payment_term);
    if ($('#payment_form select[name="payment_type"] option:selected').data('is_cheque') == '1') {
        $('.cheque_details').slideDown();
    } else {
        $('.cheque_details').slideUp();
//        $('[name="cheque_bank_name"]').val('');
        $('[name="cheque_acc_no"]').val('');
        $('[name="cheque_date"]').val('');
    }
    
    $('select[name="ship_to"]').select2('open');
    $('select[name="ship_to"]').focus();
    if (default_address == '') {
        $('.new_shipping_address').slideDown();
    }
    
    tabPressed ++;
}