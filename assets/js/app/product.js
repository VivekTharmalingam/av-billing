var scanItem = 0;
$(document).ready(function () {
    $('input[type="file"]').on('change', function (e) {
        var url = '', $parent = $(this).parent().closest('section');
        if ($(this).val() == '') {
            url = $(this).attr('data-default');
        } else {
            url = URL.createObjectURL(e.target.files[0]);
        }
        $parent.find('.preview img').attr('src', url);
        $parent.find('.remove-img-btn').remove();
    });

    $('#cat_name').on('change', function () {
        $('#pdt_desc').valid();
    });

    $('#pdt_code').on('change', function () {
        $('#pdt_desc').valid();
    });
    
    $(document).on('click', '.item-serialnum-popup', function () {
        open_serial_no_popup();
    });
	
    $(document).on('click', '.serial_no_close, .close', function () {
        set_serial_no_input();
    });
    
    $(document).on('blur', 'input.new_quantity', function (e) {
        var item_sl_no = ($('.item_sl_no').val() != '') ? $('.item_sl_no').val().split(',') : [];     
        var qty_count_txt = ($(this).val() != '') ? parseFloat($(this).val()) : 0;    
        var old_itm_quantitytxt = ($('.old_itm_quantitytxt').val() != '') ? parseFloat($('.old_itm_quantitytxt').val()) : 0;    
        var qnty = parseFloat(qty_count_txt) + parseFloat(old_itm_quantitytxt);
        var item_sl_no_txt = '';
        if(item_sl_no.length>qnty) {
            item_sl_no_txt = item_sl_no.slice(0, qnty);
        }
        if((qnty>0 && item_sl_no_txt!='') || (qnty==0 && item_sl_no_txt=='')) {
            $('.item_sl_no').val(item_sl_no_txt);
        }
    });

    $('.item_cat').on('change', function () {
        
        var cat = $(this).val();
        var sub_cat = '';
        category_wise_subcategory(cat, sub_cat);
    });
    var copy_product_id = $('.copy_product_id').val();
    
    if(copy_product_id != '') {
        var editCat = $('select.item_cat option:selected').val();
        var sub_cat = $('#copy_subcat_id').val();
        category_wise_subcategory(editCat, sub_cat);
    }
//        var editCat = $('select.item_cat option:selected').val();
//        var editSub = $('select.sub_cat option:selected').val();
//        if(editCat!=''){
//            category_wise_subcategory(editCat,editSub);
//        }

    $.validator.addMethod("barCodeExist", function (value, element) {
        var return_val = true;
        var productId = $('#product_id').val();
        //var mId = $('#method').val();

        if (value.trim() == '') {
            return true;
        }

        $.ajax({
            async: false,
            url: baseUrl + 'item/check_barcode_exists',
            type: 'POST',
            data: {barcode: value.trim(), product_id: productId},
        })
		.done(function (data) {
			if (data == '1') {
				return_val = false;
			}
		})
		.fail(function (jqXHR, status_code, error_thrown) {
			console.log(error_thrown);
			show_alert('Temporarily service not available. Please try again later!');
		});
		
        return return_val;
    });
	
	$.validator.addMethod("snoCodeExist", function (value, element) {
        var return_val = true;
        var sno = $('#s_no').val().trim();
        var productId = $('#product_id').val();
        //var mId = $('#method').val();

        if (sno == '') {
            return true;
        }

        $.ajax({
            async: false,
            url: baseUrl + 'item/check_sno_exists',
            type: 'POST',
            data: {sno: sno, product_id: productId},
        })
		.done(function (data) {
			if (data == '1') {
				return_val = false;
			}
		})
		.fail(function (jqXHR, status_code, error_thrown) {
			console.log(error_thrown);
			//show_alert('Temporarily service not available. Please try again later!');
		});
		
        return return_val;
    });

    /*$.validator.addMethod("productCodeExist", function (value, element) {
        var return_val = true;
        var productName = $('#pdt_code').val();
        var productId = $('.product_id').val();
        var mId = $('#method').val();

        //if (productName != '' && mId=='2') {
        $.ajax({
            async: false,
            url: baseUrl + 'item/check_product_exists',
            type: 'POST',
            data: {product_name: productName, product_id: productId},
        })
		.done(function (data) {
			if (data == '1') {
				return_val = false;
			}
		})
		.fail(function (jqXHR, status_code, error_thrown) {
			alert(error_thrown);
		});
        //}
        return return_val;
    });*/

    $.validator.addMethod("productExist", function (value, element) {
        var return_val = true;
        var brand = $('#cat_name').val();
        var pdt_code = '';
        var prodDesc = $('#pdt_desc').val();
        var productId = $('#product_id').val();
        //var mId = $('#method').val();
        if (brand != '' || prodDesc != '') {
            $.ajax({
                async: false,
                url: baseUrl + 'item/check_product_description_exists',
                type: 'POST',
                data: {brand: brand, prodDesc: prodDesc, product_id: productId},
            })
			.done(function (data) {
				if (data == '1') {
					return_val = false;
				}
				//$('#pdt_desc').valid();
			})
			.fail(function (jqXHR, status_code, error_thrown) {
				alert(error_thrown);
			})
        }
        return return_val;
    });

    $.validator.addMethod("productRequired", function (value, element) {
        var brand = $('#cat_name').val();
        var ProductDesc = $('#pdt_desc').val();

        if (brand == '' && ProductDesc == '') {
            return false
        }
		
        return true;
    });
	
	$(document).on('keyup', '#bar_code', function (e) {
		// For item edit
		scanItem = 1;
	});
	//$(document).on('keyup', '#bar_code, #s_no', function (e) {
		/*var initial_length = $(this).val().length;
		var _this = $(this);
		setTimeout(function() {
			if (_this.val().length - initial_length > 5) {*/
				//scanItem = 1;
			/*}
		}, 100);*/
		
		//return false;
	//});
	
    $("#item_form").validate({
		// Rules for form validation
        invalidHandler: function (event, validator) {
            $('body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top - 100
            },
            '500',
            'swing'
            );
        },
        // Rules for form validation
        rules: {
            /*pdt_code: {
                productCode: true,
                productCodeExist: true
            },*/
            pdt_desc: {
				productRequired: true,
                productExist: true
            }/*,
            s_no: {
                snoCodeExist: true
            }*/
        },
        // Messages for form validation
        messages: {
            /*pdt_code: {
                productCode: 'Please enter Item Code Or Item Description',
                productCodeExist: 'Item Code already exist'
            },*/
            pdt_desc: {
				productRequired: 'Please select/enter Brand Name or Item Description',
                productExist: 'Product already exist'
            }/*,
            s_no: {
                snoCodeExist: 'Serial No. already exist'
            }*/
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
            if (scanItem) {
                scanItem = 0;
                return false;
            }
			
            form.submit();
        }
    });

    var items = get_all_items('');
    $(document).on('keyup', 'input.search_barcode', function () {
		//alert('test');
        var ser = $(this).val().trim();
        var _this = $(this);
		//var _return = true;
        if (ser != '') {
			scanItem = 1;
			var item_available = false;
			var pdt_code = '';
            $.each(items, function (key, data) {
				var s_no_array = new Array();
				if (data.s_no != '')
					s_no_array = data.s_no.split(',');
				
                if ((ser.toUpperCase() === data.bar_code.toUpperCase()) || (ser.toUpperCase() === data.pdt_code.toUpperCase()) || ($.inArray(ser, s_no_array) !== -1)) {
                    $('#is_update').val('1');
                    $('.new_quantity').val(1);
                    $('.new_quantity').focus();
                    qty = 0;
                    if (data.available_qty != '') {
                        qty = data.available_qty;
                    }
                    $('.avail_qty').html(qty);
                    $('.avail_qty_cls').show();
					$('#cat_name').select2('val', data.cat_id);
					$('#item_cat').select2('val', data.item_category);
					$('#item_cat').trigger('change');
					$('#sub_cat').select2('val', data.sub_category);
                    $('#bar_code').val(data.bar_code);
                    
					if ((ser === data.pdt_code) || ($.inArray(ser, s_no_array) !== -1)) {
						$('input.search_barcode').val(data.bar_code);
					}
					
					$('#s_no').val(data.s_no);
					pdt_code = data.pdt_code;
                    var itm_qtytxt = (data.s_no != '') ? data.s_no.split(',').length : 0;
                    $('#old_itm_quantitytxt').val(itm_qtytxt);
                    $('#pdt_desc').val(data.pdt_name);
                    $('#sel_price').val(data.selling_price);
                    $('#buy_price').val(data.buying_price);
                    $('#product_id').val(data.id);
					for(var i in data.branches) {
						var branch = data.branches[i];
						var objBranch = $('input[name="branch_id[]"][value="'+branch.brn_id + '"]').closest('.branch_location');
						objBranch.find('.branch_location').val(branch.branch_location);
					}
					
					item_available = true;
					return false;
                }
            });
			
			if (!item_available) {
				$('.item-not-exists').slideDown();
				$('#old_itm_quantitytxt').val('');
				$('#product_id').val('');
				$('#is_update').val('');
				$('.item-code').html('').parent().hide();
			}
			else {
				$('.item-not-exists').slideUp();
				$('.item-code').html(pdt_code).parent().show();
			}
        }
    });
	
    var serial = get_all_serialno('');
    $(document).on('keyup', 'input.serial_no', function () {
        var ser = $(this).val().trim();
        var _this = $(this);
        if (ser != '') {
			scanItem = 1;
			var item_available = false;
			var pdt_code = '';
            $.each(serial, function (key, data) {
				var s_no_array = new Array();
				if (data.s_no != '')
					s_no_array = data.s_no.split(',');
				
                //if (ser === data.s_no) {
                if ($.inArray(ser, s_no_array) !== -1) {
					$('#is_update').val('1');
                    $('.new_quantity').val(1);
                    $('.new_quantity').focus();
                    qty = 0;
					pdt_code = data.pdt_code;
                    if (data.available_qty != '') {
                        qty = data.available_qty;
                    }
                    $('.avail_qty').html(qty);
                    $('.avail_qty_cls').show();
					$('#cat_name').select2('val', data.cat_id);
					$('#item_cat').select2('val', data.item_category);
					$('#item_cat').trigger('change');
					$('#sub_cat').select2('val', data.sub_category);
                    $('#s_no').val(data.s_no);
                    $('#pdt_desc').val(data.pdt_name);
                    $('#sel_price').val(data.selling_price);
                    $('#buy_price').val(data.buying_price);
                    $('#product_id').val(data.id);
					for(var i in data.branches) {
						var branch = data.branches[i];
						var objBranch = $('input[name="branch_id[]"][value="'+branch.brn_id + '"]').closest('.branch_location');
						objBranch.find('.branch_location').val(branch.branch_location);
					}
					
					return false;
                }
            });
			
			if (!item_available) {
				$('.item-not-exists').slideDown();
				$('#old_itm_quantitytxt').val('');
				$('#product_id').val('');
				$('#is_update').val('');
				$('.item-code').html('').parent().hide();
			}
			else {
				$('.item-not-exists').slideUp();
				$('.item-code').html(pdt_code).parent().show();
			}
        }
    });
	
	$('input.search_barcode').focus();
	$('#pdt_desc, #new_quantity').on('change', function() {
		scanItem = 0;
	});
});

function reset_brand() {
    var ajaxURL = baseUrl + 'item/category_active';
    var ajaxData = {}

    var data = ajaxRequest(ajaxURL, ajaxData, '');
    var _option = '<option value="">Select Brand</option>';
    for (var i in data) {
        _option += '<option value="' + data[i].id + '">' + data[i].cat_name + '</option>';
    }

    $('#cat_name').html(_option);
    $('#cat_name').select2({width: '100%'});
}

function get_all_items(item) {
    var ajaxURL = baseUrl + 'item/search_item_by_barcode';
    var ajaxData = {
        item: item
    }
    return ajaxRequest(ajaxURL, ajaxData, '');
}

function get_all_serialno(item) {
    var ajaxURL = baseUrl + 'item/search_item_by_seialno';
    var ajaxData = {
        item: item
    }
    return ajaxRequest(ajaxURL, ajaxData, '');
}

function category_wise_subcategory(catId, sub_cat) {
    var ajaxURL = baseUrl + 'item/get_subcat_by_category';
    var ajaxData = {category: catId}

    var data = ajaxRequest(ajaxURL, ajaxData, '');
    var _option = '<option value="">Select Sub Category</option>';
    for (var i in data) {
        var select_txt = (sub_cat == data[i].id) ? "selected" : "";
        _option += '<option value="' + data[i].id + '" '+select_txt+'>' + data[i].sub_category_name + '</option>';
    }

    $('#sub_cat').html(_option);
    $('#sub_cat').select2({width: '100%'});
}


function open_serial_no_popup() {
    var open_popup = false;
    /* Set Serial No. */
    var qty_count_txt = ($('.new_quantity').val() != '') ? parseFloat($('.new_quantity').val()) : 0;    
    var old_itm_quantitytxt = ($('.old_itm_quantitytxt').val() != '') ? parseFloat($('.old_itm_quantitytxt').val()) : 0;    
    var qty_count = parseFloat(qty_count_txt) + parseFloat(old_itm_quantitytxt);    
    var product_code = 'this Item';     
    var item_sl_no = ($('.item_sl_no').val() != '') ? $('.item_sl_no').val().split(',') : [];     
    var html = '', temp='';
    if((qty_count > 0) && product_code!='') {
        open_popup = true;
        $('.serial_no_title').html('Add Serial No. for '+product_code);
        for (var i=0; i<qty_count; i++) {
			var ths_item_sl_no = (item_sl_no[i] != undefined) ? item_sl_no[i] : "";
			var inc_i = i+1;
			html += '<div class="col-md-12 itemsernos">';
			
			html += '<div class="col-md-1">';
			html += '<label class="item_id">' + inc_i + '</label>';
			html += '</div>';

			html += '<div class="col-md-11">';
			html += '<label class="input" style="width:100%;"><input type="text" name="ser_no_txt[]" class="ser_no_txt form-control" onkeypress="return isRmvComma(event)" value="'+ths_item_sl_no+'"/></label>';
			html += '</div>';
			
			html += '</div>';
        }
        $('.item_ser_nos').html(html);
    }
    /* End */

    if (open_popup) {
        $("#grn_serial_no").modal({
            backdrop: "static",
            keyboard: true,
            show: true
        });
    }
}

function set_serial_no_input() {
    var ser_no_txtall = '';
	$('.ser_no_txt').each(function(){
		if($(this).val().trim() != "") {
			ser_no_txtall += $(this).val().trim()+',';
		}
	});
	ser_no_txtall = (ser_no_txtall != '') ? ser_no_txtall.replace(/^,|,$/g,'') : '';
	$('.item_sl_no').val(ser_no_txtall);
}

function isRmvComma(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 44) {
        return false;
    }
    return true;
}