function gst_results(_this) {
    if ($('.hidden_charge_type').val() == '1') {
        amt = $('.amount_with_vat').val();
        d = parseFloat(($('#hidden_gst').val() / 100) * amt);
        $('.charges').val(parseFloat(d).toFixed(2));
        $('.gst_val').html('GST @ ' + $('#hidden_gst').val() + ' %');
    } else if ($('.hidden_charge_type').val() == '2') {
        $('.gst_val').html('GST @ 0.00 %');
        $('.charges').val('0.00');
    }
    var vamt = parseFloat($('.amount_with_vat').val());
    var char = parseFloat($('.charges ').val());
    aa = vamt + char;
    $('.tot_amt').val(parseFloat(aa).toFixed(2));
}
/* Common -End */

//Methode 2 - Start
function calculate_discount() {
    var sub_total = ($('.sub_total').val() != '') ? parseFloat($('.sub_total').val()) : 0;
    var discount = ($('.discount').val() != '') ? parseFloat($('.discount').val()) : 0;
    if ($('.discount_type:checked').val() == '1') { // Discount percentage
        discount = parseFloat((discount / 100) * sub_total);
    }
    items.find('.discount_amount').html(discount.toFixed(2));
    items.find('.discount_amt').val(discount.toFixed(2));
    sub_total -= discount;
    items.find('.sub_total').val(sub_total.toFixed(2));
    calculate_total();
}

function calculate_total() {
    var sub_total = 0;
    $('.amount').each(function (index, element) {
        sub_total += ($(element).val() != '') ? parseFloat($(element).val()) : 0;
    });
    $('.sub_total').val(sub_total.toFixed(2));

    var discount = ($('.discount').val() != '') ? parseFloat($('.discount').val()) : 0;
    var totGrn = 0.00;
    var dis = 0.00;
    if ($('.discount_type:checked').val() == '1') { // Discount percentage
        discount = parseFloat((discount / 100) * sub_total);
    }
    $('.discount_amount').html(discount.toFixed(2));
    $('.discount_amt').val(discount.toFixed(2));
    sub_total -= discount;

    var total_amount = sub_total;
    var gst_percentage = 0;
    var gst_amount = 0;
    if($('.supplier_gst').val() != '2') {
        gst_percentage = ($('#hidden_gst').val() != '') ? parseFloat($('#hidden_gst').val()) : 0;
        var gst_amount = parseFloat((gst_percentage / 100) * sub_total);
    }
    
    if ($('.gst_type:checked').val() == '2') {
        total_amount = sub_total + gst_amount;
    }

	$('.gst_amount').val(gst_amount.toFixed(2));
	$('.gst_percentage').html(gst_percentage.toFixed(2));
	$('.total_amt').val(total_amount.toFixed(2));
	$('.payment_amount').html(total_amount.toFixed(2));
	
	if($('#currency').length) {
		var exchange_rate = ($('#exchange_rate').val() != '') ? parseFloat($('#exchange_rate').val()) : 0;
		var sgd_total_amt = exchange_rate * parseFloat(total_amount.toFixed(2));
		$('#sgd_total_amt').val(sgd_total_amt.toFixed(2));
	}
}

function get_all_items_descriptions() {
    var ajaxURL = baseUrl + 'invoice/search_item_description';
    var ajaxData = {}

    return ajaxRequest(ajaxURL, ajaxData, '');
}

function get_all_items(product_code) {
    var ajaxURL = baseUrl + 'invoice/search_item';
    var ajaxData = {
        product_code: product_code
    }
    return ajaxRequest(ajaxURL, ajaxData, '');
}

function get_all_items_descriptions1(po, grn) {
    var ajaxURL = baseUrl + 'goods_receive/search_item_description1';
    var ajaxData = {
        po: po,
        grn: grn
    }
    return ajaxRequest(ajaxURL, ajaxData, '');
}

function get_all_items1(po, product_code, grn) {
    var ajaxURL = baseUrl + 'goods_receive/search_item1';
    var ajaxData = {
        po: po,
        product_code: product_code,
        grn: grn
    }
    return ajaxRequest(ajaxURL, ajaxData, '');
}

function edit_purchase_receive_desc(po, grn) {
    var ajaxURL = baseUrl + 'goods_receive/edit_purchase_receive_desc';
    var ajaxData = {
        po: po,
        grn: grn
    }
    return ajaxRequest(ajaxURL, ajaxData, '');
}

function edit_purchase_receive_items(po, product_code, grn) {
    var ajaxURL = baseUrl + 'goods_receive/edit_purchase_receive_items';
    var ajaxData = {
        po: po,
        product_code: product_code,
        grn: grn
    }
    return ajaxRequest(ajaxURL, ajaxData, '');
}


function hide_product_code_msg() {
    $('.product_code_label').slideUp();
}

function hide_error_description_msg() {
    $('.error_description').slideUp();
}

//alert($('.po_type:checked').val());
//if ($('.po_type:checked').val() == '2') {
/*var items = get_all_items('');
var product_codes = get_all_items(1);
var descriptions = get_all_items_descriptions('');// To set autocomplete in product add row
set_product_autocomplete();*/
//}

function set_product_autocomplete() {
    $(".product_code").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }

        $(this).on('change', function () {
            valid_item_code($(this));
        });

        //console.log(product_codes);
        $(this).autocomplete({
            source: product_codes,
            focus: function (event, ui) {
                //$(this).val(ui.item.value);
                //return false;
            },
            select: function (event, ui) {
                var itemsCol = $(this).closest('.items');
                if ($('.product_details').find('.product_id[value="' + ui.item.id + '"]').length) {
                 // Item already exists
                 if ($(this).data('value').trim() != ui.item.value.trim()) {
                 itemsCol = $('.product_details').find('.product_id[value="' + ui.item.id + '"]').closest('.items');
                 $(this).val('');
                 $(this).closest('.items').find('.product_code_label').slideDown();
                 setTimeout(hide_product_code_msg, 3000)
                 //return false;
                 }
                 else {
                 $(this).val(ui.item.value.trim());
                 des = ui.item.pdt_name;
                 if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                 des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                 }
                 items.find('.product_description').val(des.trim());
                 }
                 }
                 else {
                $(this).val(ui.item.pdt_code.trim());
                itemsCol.find('.category_id').val(ui.item.cat_id);
                itemsCol.find('.product_id').val(ui.item.id);
//                    itemsCol.find('.brand_name').html(ui.item.cat_name);
//                    itemsCol.find('.brand_label').slideDown();
//                    itemsCol.find('.product_description').val(ui.item.pdt_name);
                des = ui.item.pdt_name;
                if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                    des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                }
                itemsCol.find('.product_code').data('value', ui.item.pdt_code.trim());
                itemsCol.find('.product_description').val(des.trim());
                var selling_price = (ui.item.selling_price != '') ? parseFloat(ui.item.selling_price) : 0;
                itemsCol.find('.price').val(selling_price.toFixed(2));
                var buying_price = (ui.item.bfr_gst_buying_price != '') ? parseFloat(ui.item.bfr_gst_buying_price) : 0;
                if (itemsCol.find('.cost').length) {
                    itemsCol.find('.cost').val(buying_price.toFixed(2));
                }

                itemsCol.find('.quantity').val(1);
                itemsCol.find('.item_discount').val(0);
                itemsCol.find('.item_discount_amt').html(0);
                itemsCol.find('.item_discount_amt').val(0);
                itemsCol.find('.amount').val(buying_price.toFixed(2));
                }
                get_supplier_itemcode(itemsCol,ui.item.id);
				calculate_item_total(itemsCol.find('.product_code'));
                calculate_total();

                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || e.keyCode;
                if (charCode == 13)
                    itemsCol.find('.quantity').focus();
                else if (event.type == 'autocompleteselect')
                    itemsCol.find('.product_description').focus();

                /* To focus quantity of particular item end */

                return false;
            }
        });
        $(this).addClass('hasAutocomplete');
    });

    $(".product_description").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }

        $(this).autocomplete({
            source: descriptions,
            focus: function (event, ui) {
                //$(this).val(ui.item.value);
                //return false;
            },
            select: function (event, ui) {
                //alert('test');
                var itemsCol = $(this).closest('.items');
                if ($('.product_details').find('.product_id[value="' + ui.item.id + '"]').length) {
                 // Item already exists
                 if ($(this).data('value').trim() != ui.item.value.trim()) {
                 itemsCol = $('.product_details').find('.product_id[value="' + ui.item.id + '"]').closest('.items');
                 $(this).val($(this).data('value').trim());
                 $(this).closest('.items').find('.error_description').slideDown();
                 setTimeout(hide_error_description_msg, 3000);
                 itemsCol.find('.quantity').focus();
                 //return false;
                 }
                 else {
                 $(this).val(ui.item.value.trim());
                 }
                 }
                 else {
                itemsCol.find('.product_code').val(ui.item.pdt_code.trim());
                //$(this).val(ui.item.pdt_code);
                itemsCol.find('.category_id').val(ui.item.cat_id);
                itemsCol.find('.product_id').val(ui.item.id);
//                    itemsCol.find('.brand_name').html(ui.item.cat_name);
//                    itemsCol.find('.brand_label').slideDown();
//                    itemsCol.find('.product_description').val(ui.item.pdt_name);

                if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                    des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                }
                else {
                    des = ui.item.pdt_name;
                }
                des = des.trim();
                itemsCol.find('.product_description').data('value', des);
                itemsCol.find('.product_description').val(des);
                var selling_price = (ui.item.selling_price != '') ? parseFloat(ui.item.selling_price) : 0;
                itemsCol.find('.price').val(selling_price.toFixed(2));
                var buying_price = (ui.item.bfr_gst_buying_price != '') ? parseFloat(ui.item.bfr_gst_buying_price) : 0;
                if (itemsCol.find('.price').length)
                    itemsCol.find('.price').val(selling_price.toFixed(2));

                if (itemsCol.find('.cost').length)
                    itemsCol.find('.cost').val(buying_price.toFixed(2));

                itemsCol.find('.quantity').val(1);
                itemsCol.find('.item_discount').val(0);
                itemsCol.find('.item_discount_amt').val(0);
                itemsCol.find('.amount').val(buying_price.toFixed(2));
                }
		get_supplier_itemcode(itemsCol,ui.item.id);		
				calculate_item_total(itemsCol.find('.product_code'));
                calculate_total();

                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || e.keyCode;
                if (charCode == 13)
                    itemsCol.find('.quantity').focus();
                else if (event.type == 'autocompleteselect')
                    itemsCol.find('.product_description').focus();

                /* To focus quantity of particular item end */
                //return false;
            }
        });
		
        $(this).addClass('hasAutocomplete');
    });
}

function valid_item_code(item_code) {
    var item_code_exists = false;
    for (var i in product_codes) {
        if (product_codes[i].pdt_code.trim().toLowerCase() == item_code.val().trim().toLowerCase()) {
            item_code_exists = true;
            break;
        }
    }

    if (!item_code_exists) {
        item_code.val('');
        item_code.focus();
        return false;
    }
}

//Methode2 -End
function reset_item() {
	items = get_all_items('');
    product_codes = get_all_items(1);
    descriptions = get_all_items_descriptions('');
    $(".product_code, .product_description").removeClass('hasAutocomplete');
    set_product_autocomplete();
}

function reset_supplier() {
    var ajaxURL = baseUrl + 'purchase_orders/active_suppliers';
    var ajaxData = {}

    var data = ajaxRequest(ajaxURL, ajaxData, '');
    var _option = '';
    _option = '<option value="">Select Supplier name</option>';
    for (var i in data) {
        _option += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
    }
    $('#sup_name').html(_option);
    $('#sup_name').select2({width: '100%'});
}

function calculate_cost_total(){
    $('.product_code').each(function(){
        calculate_item_total($(this));
    });
}

function calculate_item_total(_this) {
    var itemsCol = $(_this).closest('.items');
    var quantity = (itemsCol.find('.quantity').val() != '') ? parseFloat(itemsCol.find('.quantity').val()) : 0;
    var price = (itemsCol.find('.cost').val() != '') ? parseFloat(itemsCol.find('.cost').val()) : 0;
    var item_discount = (itemsCol.find('.item_discount').val() != '') ? parseFloat(itemsCol.find('.item_discount').val()) : 0;
    var item_total = parseFloat((quantity * price).toFixed(2));
    item_discount = (itemsCol.find('.item_discount_type:checked').length) ? item_total * parseFloat((item_discount / 100).toFixed(2)) : item_discount;

    var price_after_gst = price;
    var gst = 0;
    var gst_amount = 0;
    if ($('.gst_type:checked').val() == '2') {
        gst = ($('#hidden_gst').val() != '') ? parseFloat($('#hidden_gst').val()) : 0;
        var gst_amount = parseFloat((gst / 100) * price);
        price_after_gst = price + gst_amount;
    }   
    itemsCol.find('.price_after_gst').val(price_after_gst.toFixed(2));
    
    itemsCol.find('.item_discount_amt').html(item_discount.toFixed(2));
    itemsCol.find('.item_discount_amt').val(item_discount.toFixed(2));
    var amount = (quantity * price) - parseFloat(item_discount.toFixed(2));
    itemsCol.find('.amount').val(amount.toFixed(2));
    calculate_total();
}

function get_unreceived_purchase_orders() {
	var ajaxURL = baseUrl + 'goods_receive/unreceived_purchase_orders';
    var ajaxData = {}
    return ajaxRequest(ajaxURL, ajaxData, '');
}

$(document).ready(function () {
    if ($('#po_form').length) {
		var data = get_unreceived_purchase_orders();
		$('#purchase_id option:not([value=""])').remove();
		var _option = '';
		for(var i in data.purchase_orders ) {
			var purchase_order = data.purchase_orders[i];
			_option += '<option value="' + purchase_order.id + '">' + purchase_order.po_no + ' / ' + purchase_order.supplier_name + ' / ' + purchase_order.po_date;
		}
		$('#purchase_id').append(_option);
		
		$('#purchase_id').on('change', function () {
			if ($(this).val() != '') {
				var pst_urltxt = add_link + $(this).val();
				$("#po_form").prop('action', pst_urltxt)
				$("#po_form").submit();
			}
		});
	}

    /*if (active == 'edit') {
        calculate_total();
    }*/
    
    $(document).on('focusin', ".quantity, .price", function () {
        $(this).select();
    });

    $('.so_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });

    $("#search_product").keydown(function (e) {
        e = e || window.event;
        var charCode = e.which || e.keyCode;
        if (charCode == 13) {
            return false;
        }
    });
    
    $(document).on('click', '.item-serialnum-popup', function () {
        open_serial_no_popup($(this));
    });
	
    $(document).on('click', '.serial_no_close, .close', function () {
        set_serial_no_input();
    });
    
    $(document).on('blur', 'input.quantity', function (e) {
        var item_sl_no = ($(this).closest('.items').find('.item_sl_no').val() != '') ? $(this).closest('.items').find('.item_sl_no').val().split(',') : [];     
        var qnty = ($(this).val() != '') ? parseFloat($(this).val()) : 0;   
        var item_sl_no_txt = '';
        if(item_sl_no.length>qnty) {
            item_sl_no_txt = item_sl_no.slice(0, qnty);
        }
        if((qnty>0 && item_sl_no_txt!='') || (qnty==0 && item_sl_no_txt=='')) {
            $(this).closest('.items').find('.item_sl_no').val(item_sl_no_txt);
        }
    });
	
    $(document).on('keyup', 'input.search_product', function () {
        var ser = $(this).val();
        var _this = $(this);
        if (ser) {
            $.each(items, function (key, value) {
                var s_no_array = new Array();
                if (value.s_no != '')
                        s_no_array = value.s_no.split(',');
                    
                //if (ser === value.bar_code) {
                if ((ser.toUpperCase() === value.bar_code.toUpperCase()) || ($.inArray(ser, s_no_array) !== -1) || (ser.toUpperCase() == value.pdt_code.toUpperCase())) {
                    if ($('.product_details').find('.product_id[value="' + value.id + '"]').length) {
                        // Item already exists
                        var itemsCol = $('.product_details').find('.product_id[value="' + value.id + '"]').closest('.items');
                        var quantity = (itemsCol.find('.quantity').val() != '') ? parseFloat(itemsCol.find('.quantity').val()) : 0;
                        quantity += 1;
                        itemsCol.find('.quantity').val(quantity);
                        var price = (itemsCol.find('.cost').val() != '') ? parseFloat(itemsCol.find('.cost').val()) : 0;
                        itemsCol.find('.amount').val((price * quantity).toFixed(2));
                    }
                    else {
                        if ($('.product_details .items:last .product_id').val() != '') {// Last row occupied by another product.
                            $('.product_details .items:last .add-row').trigger('click');
                        }
                        var itemsCol = $('.product_details .items:last');
                        itemsCol.find('.category_id').val(value.cat_id);
                        itemsCol.find('.product_id').val(value.id);
                        itemsCol.find('.product_code').val(value.pdt_code);
                        //itemsCol.find('.brand_name').html(value.cat_name);
                        //itemsCol.find('.brand_label').slideDown();
                        des = value.pdt_name;
                        if (value.cat_name != '' && value.cat_name != null) {
                            des = value.cat_name + ' ' + value.pdt_name;
                        }
                        itemsCol.find('.product_description').val(des);
                        if (itemsCol.find('.price').length)
                            itemsCol.find('.price').val(value.selling_price);

                        if (itemsCol.find('.cost').length)
                            itemsCol.find('.cost').val(value.buying_price);

                        itemsCol.find('.quantity').val(1);
                        itemsCol.find('.amount').val(value.buying_price);
                    }
                    itemsCol.find('.quantity').focus().select();
					
					calculate_item_total(itemsCol.find('.product_code'));
                    calculate_total();
                    $(_this).val('');
                    scanItem = 1;
                    return false;
                }
            });
        }
    });
	
    $(document).on('change', '.item_discount_type', function () {
        calculate_item_total($(this));
    });
	
    $(document).on('keyup', '.quantity, .cost, .item_discount', function () {
        calculate_item_total($(this));
    });
	
    $(document).on('keyup', '.discount, .discount1, #exchange_rate', function () {
        calculate_total();
    });
	
    $(document).on('change', '.discount_type, .discount,.discount_type1, .discount1', function () {
        calculate_total();
    });
	
    $(document).on('change', '.gst_type,.gst_type1', function () {
        calculate_cost_total();
        //calculate_total();
    });
    
    if($('.supplier_gst').val() != "") {
        get_supplier_data();
        calculate_total();
    }
	
    $(document).on('change', '#sup_name', function () {
        get_supplier_data();
        reset_supplier_itemcode();
        calculate_cost_total();
    });

    if($('.po_idtxt').val() != '') {
        get_supplier_data();
        calculate_cost_total();
    }
    /*if (active == 'edit') {
        $(document).on('keyup', 'input.product_code1', function () {
            var po = $('select.po_id option:selected').val();
            var grn = $('.grn_id').val();
            if ($('.po_type:checked').val() == '1') {
                product_codes = edit_purchase_receive_items(po, 1, grn);
                descriptions = edit_purchase_receive_desc(po, grn);
                set_product_autocomplete();
            }
        });
        $(".product_code1").autocomplete('option', 'source', product_codes);
        $(".product_description1").autocomplete('option', 'source', descriptions);
    }*/

    /* Add row functionality for material */
    $(document).on('click', '.items .add-row', function () {
        //poId = $('.po_id').select2('val');
        addRowIndex ++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.items').clone();
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');
        itemObj.find('.brand_name').html('');
        itemObj.find('.brand_label').hide();
        //itemObj.find('.vat_percentage').attr('checked', false);

        itemObj.find('input, select, textarea').each(function (index, element) {
            if ($(element).hasClass('item_hidden_ids')) {
                $(element).remove();
            }

            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndex + ']');
            }
            $(element).val('');

            if ($(element).hasClass('product_description'))
                $(element).data('value', '');

            if ($(element).hasClass('hasAutocomplete'))
                $(element).removeClass('hasAutocomplete')

            /*if ($(element).prop('tagName').toLowerCase() == 'select') {
             if ($(element).prev().hasClass('select2')) {
             $(element).prev().remove();
             $(element).select2({width: '100%'});
             }
             }*/
        });

        itemObj.find('.item_discount_type').prop('checked', true);
        itemObj.find('.item_discount_type').val(1);
        itemObj.find('.item_discount_amt').html(0);
        itemObj.find('.item_discount_amt').val(0);

        if (formType == 'edit') {
            itemObj.find('.item_discount_type').prop('id', 'new_item_discount_type-' + addRowIndex);
            itemObj.find('.onoffswitch-label').prop('for', 'new_item_discount_type-' + addRowIndex);
        }
        else {
            itemObj.find('.item_discount_type').prop('id', 'item_discount_type-' + addRowIndex);
            itemObj.find('.onoffswitch-label').prop('for', 'item_discount_type-' + addRowIndex);
        }
        var materialContainer = $(this).parents('.material-container');
        var optionsLength = items.length;
        if (optionsLength > $(materialContainer).find('.items').length) {
            if ($(this).parent().find('.remove-row').length)
                $(this).remove();
            else
                $(this).removeClass('add-row fa-plus-circle').addClass('remove-row fa-minus-circle');

            if ($(itemObj).find('.remove-row').length == 0)
                $(itemObj).find('.add-row').after('<i class="fa fa-2x fa-minus-circle remove-row"></i>');

            $(materialContainer).find('.items:last').after(itemObj);
        }
        else if ((optionsLength == $(materialContainer).find('.items').length) || (optionsLength == 0)) {
            var smallBoxTitle = 'Sorry ! ...';
            var smallBoxMsg = '<i class="fa fa-info-circle"></i> <i>You can\'t add more...<br> Because you have';
            smallBoxMsg += (optionsLength > 0) ? ' only ' + optionsLength : ' no';
            smallBoxMsg += ($(materialContainer).find('.items').length > 1) ? ' items' : ' item';
            smallBoxMsg += '.</i>';

            if ($('#divSmallBoxes').html().length == 0)
                showSmallBox(smallBoxTitle, smallBoxMsg);
        }

        set_product_autocomplete();
        $('.items:last .product_code').focus();
    });

    /* Remove row functionality for material */
    $(document).on('click', '.items .remove-row', function () {
        var materialContainer = $(this).parents('.material-container');
        $(this).parents('.items').remove();
        if (materialContainer.find('.items:last').find('.add-row').length == 0) {
            materialContainer.find('.items:last').find('.remove-row').before('<i class="fa fa-2x fa-plus-circle add-row"></i>');
        }

        if ((materialContainer.find('.items').length == 1) && (materialContainer.find('.items:last').find('.add-row').length)) {
            materialContainer.find('.items:last').find('.remove-row').remove();
        }
        calculate_total();
        $('.items:last .product_code').focus();
    });
	
    $.validator.addMethod('mat_already_exist', function (value, element) {
        var prevItems = $(element).parents('.items').prevAll('.items');
        return (prevItems.find('.product_id[value="' + value + '"]').length) ? false : true;
    }, 'Item Code is already exist!');

    $.validator.addMethod('product_code_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please select Product Code!');

    /*$.validator.addMethod('mat_already_exist1', function (value, element) {
        var prevItems = $(element).parents('.items1').prevAll('.items1');
        return (prevItems.find('.product_id[value="' + value + '"]').length) ? false : true;
    }, 'Item Code is already exist!');*/

    $.validator.addMethod('qty_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please enter Quantity!');

    $.validator.addMethod('payment_type_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please select Payment Type!');

    $.validator.addMethod('valid_qty', function (value, element) {
        if ($('.po_type:checked').val() == 2) {
            return true;
        }
        else {
            var availableQty = parseFloat($(element).parents('.items1').find('.po_quantity1').val());
            if (availableQty >= parseFloat(value)) {
                return true;
            }
            else {
                if (!$('.SmallBox').length) {
                    $.smallBox({
                        title: "Error",
                        content: "<i class='fa fa-warning'></i> <i> Received Quantity should not be greater than Actual Quantity!</i>",
                        color: "#A90329",
                        iconSmall: "fa fa-thumbs-o-down bounce animated",
                        timeout: 5000
                    });
                }
                return false;
            }
        }
    }, '');

    $.validator.addClassRules({
        product_code: {
            product_code_required: true/*,
             mat_already_exist: true*/
        },
        quantity: {
            qty_required: true
//            valid_qty: true
        }
    });

    var $orderForm = $("#purchase_receive_form").validate({
        // Rules for form validation
        invalidHandler: function (event, validator) {
            $('body').animate({
                scrollTop: $(validator.currentForm).offset().top
            },
            '500',
                    'swing'
                    );
        },
        rules: {
            /*po_id: {
                required: true
            },*/
            sup_name: {
                required: true
            },
			exchange_rate: {
                required: true
            },
            pr_date: {
                required: true
            },
            inv_date: {
                required: true
            },
            branch_name: {
                required: true
            },
        },
        // Messages for form validation
        messages: {
            /*po_id: {
                required: 'Please select PO Number'
            },*/
            sup_name: {
                required: 'Please select supplier name'
            },
			exchange_rate: {
                required: 'Please enter Exchange Rate'
            },
            pr_date: {
                required: 'Please select Goods Receive Date'
            },
            inv_date: {
                required: 'Please select Invoice Date'
            },
            branch_name: {
                required: 'Please select Branch Name'
            },
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
            if (scanItem) {
                scanItem = 0;
                return false;
            }

            form.submit();
        }
    });
});

var addRowIndex = 0;
//var active = $('.po_id').parents('form').hasClass('add_form') ? 'add' : 'edit';
var this_item_row = '';
var scanItem = 0;
var items = get_all_items('');
var product_codes = get_all_items(1);
var descriptions = get_all_items_descriptions('');// To set autocomplete in product add row
set_product_autocomplete();


function get_supplier_data() {
    var supp_id = $('#sup_name').val();
    if(supp_id != "") {
        var ajaxURL = baseUrl + 'goods_receive/get_supplier_data';
        var ajaxData = {
            supp_id: supp_id
        }
        var data = ajaxRequest(ajaxURL, ajaxData, '');
        
		if ($('#currency').length) {
			$('#currency').val(data.supplier.currency);
			if (data.supplier.currency == '2') { // USD Currency
				$('#exchange_rate_container').slideDown();
				$('#sgd_total_amt_container').slideDown();
			}
			else { // SGD Currency
				$('#exchange_rate_container').slideUp();
				$('#sgd_total_amt_container').slideUp();
			}
			
		}
		
        if (data.supplier.gst_type == '2') {
            $('.supplier_gst').val('2');
            $('.without_gst').prop('checked', true);
            $('.gst_details').hide();
        }
		else {
            $('.supplier_gst').val('1');
            $('.with_gst').prop('checked', true);
            $('.gst_details').show();
        }
    }
    
}

function open_serial_no_popup($_this) {
    
    var open_popup = false;
    /* Set Serial No. */
    var qty_count = ($_this.closest('.items').find('.quantity').val() != '') ? parseFloat($_this.closest('.items').find('.quantity').val()) : 0;    
    var product_code = $_this.closest('.items').find('.product_code').val();     
    var item_sl_no = ($_this.closest('.items').find('.item_sl_no').val() != '') ? $_this.closest('.items').find('.item_sl_no').val().split(',') : [];     
    var html = '', temp='';
    this_item_row = '';
    if((qty_count > 0) && product_code!='') {
        open_popup = true;
        this_item_row = $_this;
        $('.serial_no_title').html('Add Serial No. for '+product_code);
        for (var i=0; i<qty_count; i++) {
                    var ths_item_sl_no = (item_sl_no[i] != undefined) ? item_sl_no[i] : "";
                    var inc_i = i+1;
                    html += '<div class="col-md-12 itemsernos">';
                    
                    html += '<div class="col-md-1">';
                    html += '<label class="item_id">' + inc_i + '</label>';
                    html += '</div>';

                    html += '<div class="col-md-11">';
                    html += '<label class="input" style="width:100%;"><input type="text" name="ser_no_txt[]" class="ser_no_txt form-control" onkeypress="return isRmvComma(event)" value="'+ths_item_sl_no+'"/></label>';
                    html += '</div>';
                    
                    html += '</div>';
                
        }
        $('.item_ser_nos').html(html);
    }
    /* End */

    if (open_popup) {
        $("#grn_serial_no").modal({
            backdrop: "static",
            keyboard: true,
            show: true
        });
    }
}

function set_serial_no_input() {
    var ser_no_txtall = '';
    if(this_item_row != '') {
        $('.ser_no_txt').each(function(){
            if($(this).val().trim() != "") {
                ser_no_txtall += $(this).val().trim()+',';
            }
        });
        ser_no_txtall = (ser_no_txtall != '') ? ser_no_txtall.replace(/^,|,$/g,'') : '';
        this_item_row.closest('.items').find('.item_sl_no').val(ser_no_txtall);    
    }
}

function isRmvComma(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 44) {
        return false;
    }
    return true;
}

function get_supplier_itemcode(ItmRow,ItmId) {
        var supp_id = $('#sup_name').val(), temp='';
        if(supp_id != "" && ItmId != '') {
            var ajaxURL = baseUrl + 'purchase_orders/get_supplier_itemcode';
            var ajaxData = {
                item_id: ItmId,
                supp_id: supp_id
            }
            var data = ajaxRequest(ajaxURL, ajaxData, '');

            if (data.itemcode.sl_itm_code != '')
                ItmRow.find('.sl_itm_code').val(data.itemcode.sl_itm_code);
            else
                ItmRow.find('.sl_itm_code').val(temp);
        } else {
            ItmRow.find('.sl_itm_code').val(temp);  
        }
}

function reset_supplier_itemcode() {
        $('.sl_itm_code').each(function(){
            var items = $(this).closest('.items');
            var itm_id = items.find('.product_id').val();
            get_supplier_itemcode(items,itm_id);
        });
}