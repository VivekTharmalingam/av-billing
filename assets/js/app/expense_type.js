$(document).ready(function () {
	
    $.validator.addMethod("expExist", function (value, element) {
        var return_val = true;
        var expName = $('#exp_type').val();
        var expId = $('.exp_id').val();
			
        if (expName != '') {
            $.ajax({
                async: false,
                url: baseUrl + 'expense_type/check_exp_exists',
                type: 'POST',
                data: {exp_name: expName, exp_id: expId},
            })
                    .done(function (data) { 
                        if (data == '1') {
                            return_val = false
                        }
                    })
                    .fail(function (jqXHR, status_code, error_thrown) {
                        alert(error_thrown);
                    })
        }
        return return_val;
    });
    
    var companyForm = $("#exp_form").validate({
        // Rules for form validation
        rules: {
            exp_type: {
                required: true,
                expExist: true
            }
        },
        // Messages for form validation
        messages: {
            exp_type: {
                required: 'Please enter Expense Type',
                expExist: 'Expense Type already exists',
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

});
