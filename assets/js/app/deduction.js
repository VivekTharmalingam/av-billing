$(document).ready(function () {
    
    get_crnt_bln('no');
    
    $('#payment_type').on('change', function() {
        $('.cheque_details').slideUp();
        if ($(this).find('option:selected').data('is_cheque') == '1') {
            $('.cheque_details').slideDown();
            $('input[name="cheque_bank_name"]').focus();
            get_crnt_bln('yes');
        }
    });
    
    $('.ded_percentage').keyup(function(){
        var perct = ($(this).val() != "") ? parseFloat($(this).val()) : 0;
        var temp = '';
        if(perct > 100){
            $(this).val(temp);
        }
    });
    
    var IsChk = $('#payment_type').find('option:selected').data('is_cheque');
    if(IsChk=='1') {
        $('.cheque_details').slideDown();
        $('input[name="cheque_bank_name"]').focus();
    }
    
    $('#bank_acc_id').on('change', function() {
        get_crnt_bln('yes');
    });
    
    $(document).on('change', 'select.select2, #ded_date', function() {
    $('form[name="deduction_form"]').validate().element(this);
    });
    
    var companyForm = $("#deduction_form").validate({
        // Rules for form validation
        rules: {
            bank_acc_id: {
                required: true
            },
            ded_date: {
                required: true
            },
            ded_percentage: {
                required: true
            },
            ded_amount: {
                required: true
            },
            payment_type : {
                    required : true
            },
            cheque_bank_name : {
                    required : true
            },
            cheque_acc_no : {
                    required : true
            },
            cheque_date : {
                    required : true
            }
        },
        // Messages for form validation
        messages: {
            bank_acc_id: {
                required: 'Please select Account No.'
            },
            ded_date: {
                required: 'Please select Date'
            },
            ded_percentage: {
                required: 'Please enter Percentage'
            },
            ded_amount: {
                required: 'Please enter Amount'
            },
            payment_type : {
                    required : 'Please select Payment Type'
            },
            cheque_bank_name : {
                    required : 'Please enter Cheque Bamk Name'
            },
            cheque_acc_no : {
                    required : 'Please enter Cheque Account No'
            },
            cheque_date : {
                    required : 'Please enter Cheque Date'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

});

function get_crnt_bln(typ){
    var amount = $('#bank_acc_id').find('option:selected').data('crntbln');
    var amt_txt = ($('#bank_acc_id').val() != "") ? 'Current Balance : <b style="color: #3276B1;" class="crnt_bln">'+amount+'</b>' : '';
    $('.crnt_bln').html(amt_txt);
    if(typ=='yes') {
        var bnktext = $('#bank_acc_id').find('option:selected').data('bnktext');
        $('.cheque_bank_name').val(bnktext);
    }
}