$(document).ready(function () {
    $('input[type="file"]').on('change', function (e) {
        var url = '', $parent = $(this).parent().closest('section');
        if ($(this).val() == '') {
            url = $(this).attr('data-default');
        } else {
            url = URL.createObjectURL(e.target.files[0]);
        }
        $parent.find('.preview img').attr('src', url);
        $parent.find('.remove-img-btn').remove();
    });
    
    $('#email').on('keyup', function () {
        if ($('form.edit_form').length == 0) {
            $('#daily_from_email').val($(this).val().trim());
            $('#daily_to_email').val($(this).val().trim());
        }
    });

    $(document).on('change', 'select.select2', function () {
        $('form[name="company_form"]').validate().element(this);
    });

    $.validator.addMethod('branchNameRequired', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please enter Branch Name');

    $.validator.addMethod('addressRequired', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please enter Address');

    $.validator.addMethod('statusRequired', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please select Status!');

    $.validator.addClassRules({
        brn_status: {
            statusRequired: true
        },
        branch_name: {
            branchNameRequired: true
        },
        brn_address: {
            addressRequired: true
        }
    });

    $('form').on('submit', function () {
        var $sbmtBtn = $('form [type="submit"]');
        if ($('.browse_logo').val() == '') {
            setGlobalMsg('Please choose logo!'); // success | warning | info | error (default)
            return false;
        }
        return true;
    });

    $.validator.addMethod("is_valid_url", function (value, element) {
        if (value != '' && value != undefined) {
            return /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);
        }
//        return /^(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)/.test(value); 
//        return /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/.test(value); 
        return true;
    });

    var companyForm = $("#company_form").validate({
        // Rules for form validation
        rules: {
            company_name: {
                required: true,
            },
            reg_no: {
                required: true,
            },
            address: {
                required: true,
            },
            phone_no: {
                required: true,
                number: true
            },
            pin_code: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            website: {
                is_valid_url: true
            },
            bank_acc_id: {
                required: true
            }

        },
        // Messages for form validation
        messages: {
            company_name: {
                required: 'Please enter company name'
            },
            reg_no: {
                required: 'Please enter registration number',
            },
            address: {
                required: 'Please enter address',
            },
            phone_no: {
                required: 'Please enter phone number',
                number: 'Please enter valid phone',
            },
            pin_code: {
                required: 'Please enter pincode',
            },
            email: {
                required: 'Please enter email',
                email: 'Please enter valid email'
            },
            website: {
                is_valid_url: 'Please enter valid website'
            },
            bank_acc_id: {
                required: 'Please select Account No.'
            }

        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

});


var addRowIndex = 0;
/* Add row functionality for Branch */
$(document).on('click', '.branch .add-row', function () {
    addRowIndex++;
    var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
    var itemObj = $(this).parents('.branch').clone();

    itemObj.find('.label').remove();
    itemObj.find('em.invalid').remove();
    itemObj.find('.state-success').removeClass('state-success');
    itemObj.find('.state-error').removeClass('state-error');
    itemObj.find('.invalid').removeClass('invalid');

    itemObj.find('input, select, textarea').each(function (index, element) {
        if ($(element).hasClass('item_hidden_ids')) {
            $(element).remove();
        }

        if ($(element).prop('name') != '') {
            var name = $(element).prop('name').split('[')[0];
            name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
            $(element).prop('name', name + '[' + addRowIndex + ']');
        }

        $(element).val('');
        if ($(element).prop('tagName').toLowerCase() == 'select') {
            if ($(element).prev().hasClass('select2')) {
                $(element).prev().remove();
                $(element).select2({width: '100%'});
                $(element).select2('val', '1');
            }
        }
    });

    var materialContainer = $(this).parents('.material-container');
    if ($(this).parent().find('.remove-row').length)
        $(this).remove();
    else
        $(this).removeClass('add-row fa-plus-circle').addClass('remove-row fa-minus-circle');

    if ($(itemObj).find('.remove-row').length == 0)
        $(itemObj).find('.add-row').after('<i class="fa fa-2x fa-minus-circle remove-row"></i>');

    $(materialContainer).find('.branch:last').after(itemObj);
});


/* Remove row functionality for Branches */
$(document).on('click', '.branch .remove-row', function () {
    var materialContainer = $(this).parents('.material-container');
    $(this).parents('.branch').remove();
    if (materialContainer.find('.branch:last').find('.add-row').length == 0) {
        materialContainer.find('.branch:last').find('.remove-row').before('<i class="fa fa-2x fa-plus-circle add-row"></i>');
    }

    if ((materialContainer.find('.branch').length == 1) && (materialContainer.find('.branch:last').find('.add-row').length)) {
        materialContainer.find('.branch:last').find('.remove-row').remove();
    }
});

function get_value(_this) {
    var val = (_this).value;
    if (val == '1') {
        $('.gst_div').slideDown('slow');
    } else if (val == '0') {
        $('.gst_div').slideUp('slow');
    }
}		