$(document).ready(function () {
	
		$.validator.addMethod("empExist", function (value, element) {
        var return_val = true;
        var empName = $('#emp_name').val();
        var empId = $('.emp_id').val();		
        if (empName != '') {
            $.ajax({
                async: false,
                url: baseUrl + 'employee/check_employee_exists',
                type: 'POST',
                data: {emp_name: empName, emp_id: empId},
            })  
                    .done(function (data) { 
                        if (data == '1') {
                            return_val = false
                        }
                    })
                    .fail(function (jqXHR, status_code, error_thrown) {
                        alert(error_thrown);
                    })
        }
        return return_val;
    });

	
    $('form').on('submit', function () {
        var $sbmtBtn = $('form [type="submit"]');
        if ($('.browse_logo').val() == '') {
            setGlobalMsg('Please choose logo!'); // success | warning | info | error (default)
            return false;
        }
        return true;
    });
    
   
    var companyForm = $("#employee_form").validate({
        // Rules for form validation
        rules: {
            emp_name: {
                required: true,
				empExist:true
            },
            designation: {
                required: true,
            },
            phone_no: {
                required: true,
				number: true
            },
            pass_type: {
                required: true
            },
            salary_type: {
                required: true,
            },
            nric_no: {
                required: true
            },
            /*nric_date: {
                required: true
            },*/
            passport_no: {
                required: true
            },
			passport_expiry: {
                required: true
            },
			salary: {
                required: true
            },
			/*cpf_val: {
                required: true
            },
			cpf_employee: {
                required: true
            },*/
			overtime: {
                required: true
            }
//            salary_acc_no: {
//                required: true
//            },
//			salary_acc_bank: {
//                required: true
//            },

        },
        // Messages for form validation
        messages: {
            emp_name: {
                required: 'Please enter Employee name',
				empExist: 'Employee Name Already Exist!'
            },
            designation: {
                required: 'Please enter Employee Designation',
            },
            pass_type: {
                required: 'Please select Pass Type',
            },
            phone_no: {
                required: 'Please enter phone number',
                number: 'Please enter valid phone',
            },
            salary_type: {
                required: 'Please select Salary Type',
            },
            nric_no: {
                required: 'Please enter NRIC No',
            },
            /*nric_date: {
                required: 'Please choose NRIC Exp Date',
            },*/
            passport_no: {
                required: 'Please enter Passport No',
            },
			passport_expiry: {
                required: 'Please choose Passport Exp Date',
            },
            salary: {
                required: 'Please enter Salary Amount',
            },
			cpf_val: {
                required: 'Please enter CPF from employer',
            },
            cpf_employee: {
                required: 'Please enter CPF from employee',
            },
			overtime: {
                required: 'Please enter Overtime Amount per Hour',
            }
//            salary_acc_no: {
//                required: 'Please enter Salary Account Number',
//            },
//			salary_acc_bank: {
//                required: 'Please enter Salary Account Bank',
//            }

        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

});


var addRowIndex = 0 ;
/* Add row functionality for Branch */
$(document).on('click', '.upload .add-row', function() {
        addRowIndex ++ ;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.upload').clone();
		
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');
        itemObj.find('.img_preview').remove();
         itemObj.find('input, select, textarea').each(function(index, element) {
            
            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndex + ']');
            }
            
            $(element).val('');
            if ($(element).prop('tagName').toLowerCase() == 'select') {
                if ($(element).prev().hasClass('select2')) {
                    $(element).prev().remove();
                    $(element).select2({width: '100%'});                    
                }
            }
        });
		
		 $(itemObj).find('.section-group').append('<span class="img_preview" style="float: right;"></span>');
        
        var materialContainer = $(this).parents('.material-container');        
            if ($(this).parent().find('.remove-row').length) 
                $(this).remove();
            else 
                $(this).removeClass('add-row fa-plus-circle').addClass('remove-row fa-minus-circle');
            
            if ($(itemObj).find('.remove-row').length == 0) 
                $(itemObj).find('.add-row').after('<i class="fa fa-2x fa-minus-circle remove-row"></i>');
           
            $(materialContainer).find('.upload:last').after(itemObj);      
    });
	
	
	/* Remove row functionality for Branches */
	$(document).on('click', '.upload .remove-row', function() {
        var materialContainer = $(this).parents('.material-container');
        $(this).parents('.upload').remove();
        if (materialContainer.find('.upload:last').find('.add-row').length == 0) {
            materialContainer.find('.upload:last').find('.remove-row').before('<i class="fa fa-2x fa-plus-circle add-row"></i>');
        }
        
        if ((materialContainer.find('.upload').length == 1) && (materialContainer.find('.upload:last').find('.add-row').length)) {
            materialContainer.find('.upload:last').find('.remove-row').remove();
        }
    });
    
    $(document).on('change', '.salary_type', function() {
        var salary_type = $(this).val();
        if(salary_type == 'Foreign') {
            var html = '<label class="input"><i class="icon-append fa fa-money"></i>';
                html += '<input style="background-color: rgb(238, 238, 238);" type="text" name="cpf_val" readonly=""  id="cpf_val" class="float_value">';
                html += '</label>'; 
                $('.cpf_val').html(html)
            var html1 = '<label class="input"><i class="icon-append fa fa-money"></i>';
                html1 += '<input type="text" style="background-color: rgb(238, 238, 238);" name="cpf_employee" readonly="" id="cpf_employee" class="float_value">';
                html1 += '</label>'; 
                $('.cpf_employee').html(html)
        } else {
                var html = '<label class="input"><i class="icon-append fa fa-money"></i>';
                html += '<input type="text" name="cpf_val" id="cpf_val" class="float_value">';
                html += '</label>';  
                $('.cpf_val').html(html)
                var html1 = '<label class="input"><i class="icon-append fa fa-money"></i>';
                html1 += '<input type="text" name="cpf_employee" id="cpf_employee" class="float_value">';
                html1 += '</label>'; 
                $('.cpf_employee').html(html)
        }
        
    });
		