$(document).ready(function () {

    $.validator.addMethod("service_partnerExist", function (value, element) {
        var return_val = true;
        var service_partnerName = $('#service_partner_name').val();
        var service_partnerId = $('.service_partner_id').val();
        if (service_partnerName != '') {
            $.ajax({
                async: false,
                url: baseUrl + 'service_partner/check_service_partner_exists',
                type: 'POST',
                data: {service_partner_name: service_partnerName, service_partner_id: service_partnerId},
            })
                    .done(function (data) {
                        if (data == '1') {
                            return_val = false
                        }
                    })
                    .fail(function (jqXHR, status_code, error_thrown) {
                        alert(error_thrown);
                    })
        }
        return return_val;
    });

    $.validator.addMethod("is_valid_url", function (value, element) {
        if (value != '' && value != undefined) {
            return /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);
        }
//        return /^(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)/.test(value); 
//        return /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/.test(value); 
        return true;
    });

    var companyForm = $("#service_partner_form").validate({
        // Rules for form validation
        rules: {
            service_partner_name: {
                required: true,
                service_partnerExist: true
            },
//            name: {
//                required: true
//            },
//            address: {
//                required: true,
//            },
//            phone_no: {
//                required: true,
//                number: true
//            },
//            email: {
//                required: true,
//                email: true
//            },
//            website: {
//                is_valid_url: true
//            },
//            payment_term: {
//                required: true
//            }

        },
        // Messages for form validation
        messages: {
            service_partner_name: {
                required: 'Please enter Supplier Name',
                service_partnerExist: 'Supplier Name Already Exist'
            },
//            name: {
//                required: 'Please enter Contact Name',
//            },
//            address: {
//                required: 'Please enter address',
//            },
//            phone_no: {
//                required: 'Please enter phone number',
//                number: 'Please enter valid phone',
//            },
//            email: {
//                required: 'Please enter email',
//                email: 'Please enter valid email'
//            },
//            website: {
//                is_valid_url: 'Please enter valid website'
//            },
//            payment_term: {
//                required: 'Please select Payment Term'
//            }

        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

});