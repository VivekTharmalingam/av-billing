$(document).ready(function () {   
    $(document).on('change', '.item', function () {
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var selling_price = $(this).find(':selected').data("item-price");
		selling_price = (selling_price !== undefined) ? selling_price : 0;
        
        var offer_per = $('.offer_percentage').val();
		offer_per = (offer_per != '') ? offer_per : 0;
		var off_percentage = parseFloat((offer_per / 100) * selling_price);
		var off_amount = selling_price - off_percentage;
		$('.o_price').html(off_amount.toFixed(2));
        
        $('.s_price').html(selling_price);
        $('.selling_price').val(selling_price);
    });
    
    $(document).on('keyup', '.offer_percentage', function () {
        var offer_per = $(this).val();
        var sell_price = $('.selling_price').val();
        var off_percentage = parseFloat((offer_per / 100) * sell_price);
        var off_amount = sell_price - off_percentage;
        $('.offer_price').val(off_amount);
        $('.o_price').html(off_amount.toFixed(2));
    });

    var companyForm = $("#ds_form").validate({
        // Rules for form validation
        rules: {
            item: {
                required: true,
            },
            offer_percentage: {
                required: true,
            },
        },
        // Messages for form validation
        messages: {
            purchase_item: {
                required: 'Please select item',
            },
            offer_percentage: {
                required: 'Please enter Offer Percentage',
            },
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
});