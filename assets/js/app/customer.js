$(document).ready(function () {

    $.validator.addMethod('locationNameRequired', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please enter Location Name');

    $.validator.addMethod('addressRequired', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please enter address!');


    $.validator.addClassRules({
        location_name: {
            locationNameRequired: true
        },
        ship_address: {
            addressRequired: true
        },
    });

    $.validator.addMethod("customerExist", function (value, element) {
        var return_val = true;
        var customerName = $('#customer_name').val();
        var customerId = $('.customer_id').val();
        if (customerName != '') {
            $.ajax({
                async: false,
                url: baseUrl + 'customer/check_customer_exists',
                type: 'POST',
                data: {customer_name: customerName, customer_id: customerId},
            })
                    .done(function (data) {
                        if (data == '1') {
                            return_val = false
                        }
                    })
                    .fail(function (jqXHR, status_code, error_thrown) {
                        alert(error_thrown);
                    })
        }
        return return_val;
    });


    $.validator.addMethod("is_valid_url", function (value, element) {
        if (value != '' && value != undefined) {
            return /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);
        }
//        return /^(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)/.test(value); 
//        return /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/.test(value); 
        return true;
    });

    var companyForm = $("#customer_form").validate({
        // Rules for form validation
        rules: {
            customer_name: {
                required: true,
                customerExist: true
            },
//            cust_group: {
//                required: true,
//            },
            address: {
                required: true,
            },
        },
        // Messages for form validation
        messages: {
            customer_name: {
                required: 'Please enter Customer Name',
                customerExist: 'Customer Name Already Exist'
            },
//            cust_group: {
//                required: 'Please select Customer Group',
//            },
            address: {
                required: 'Please enter address',
            },
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

    var addRowIndex = 0;
    /* Add row functionality for Branch */
    $(document).on('click', '.shipping_address .add-row', function () {
        addRowIndex++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.shipping_address').clone();

        //itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');
        itemObj.find('.img_preview').remove();


        itemObj.find('input, select, textarea').each(function (index, element) {

            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndex + ']');
                $(element).parent().find('.is_default').val(1);
            }

            if ($(this).attr('type') == 'radio') {
                $(this).prop('checked', false);
            }

            $(element).val('');
            if ($(element).prop('tagName').toLowerCase() == 'select') {
                if ($(element).prev().hasClass('select2')) {
                    $(element).prev().remove();
                    $(element).select2({width: '100%'});
                }
            }

            $(element).parent().find('.is_default').val(1);

        });

        $(itemObj).find('.section-group').append('<span class="img_preview" style="float: right;"></span>');

        var materialContainer = $(this).parents('.material-container');
        if ($(this).parent().find('.remove-row').length)
            $(this).remove();
        else
            $(this).removeClass('add-row fa-plus-circle').addClass('remove-row fa-minus-circle');

        if ($(itemObj).find('.remove-row').length == 0)
            $(itemObj).find('.add-row').after('<i class="fa fa-2x fa-minus-circle remove-row"></i>');

        $(materialContainer).find('.shipping_address:last').after(itemObj);
    });


    /* Remove row functionality for Branches */
    $(document).on('click', '.shipping_address .remove-row', function () {
        var materialContainer = $(this).parents('.material-container');
        $(this).parents('.shipping_address').remove();
        if (materialContainer.find('.shipping_address:last').find('.add-row').length == 0) {
            materialContainer.find('.shipping_address:last').find('.remove-row').before('<i class="fa fa-2x fa-plus-circle add-row"></i>');
        }

        if ((materialContainer.find('.shipping_address').length == 1) && (materialContainer.find('.shipping_address:last').find('.add-row').length)) {
            materialContainer.find('.shipping_address:last').find('.remove-row').remove();
        }
    });

    $('#customer_name').focus();
});

function unchecked_fields(_this) {
    $(_this).parents().closest('div').find('.shipping_address .is_default').not(_this).prop('checked', false);
}

function checked(_this) {
    $(this).attr('checked', true);
}