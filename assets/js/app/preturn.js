var addRowIndex = 0 ;
var active = $('.po_id').parents('form').hasClass('add_form') ? 'add' : 'edit';
$(document).ready(function() {
    /* Add row functionality for material */
    $(document).on('click', '.items .add-row', function() {
        addRowIndex ++ ;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.items').clone();
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');        
        itemObj.find('input, select, textarea').each(function(index, element) {
            if ($(element).hasClass('item_hidden_ids')) {
                $(element).remove();
            }            
            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndex + ']');
            }            
            $(element).val('');
            if ($(element).prop('tagName').toLowerCase() == 'select') {
                if ($(element).prev().hasClass('select2')) {
                    $(element).prev().remove();
                    $(element).select2({width: '100%'});
                }
            }
        });
        
        var materialContainer = $(this).parents('.material-container');
        var optionsLength = $(materialContainer).find('select.mat_id:first option:not([value=""]):not(:disabled)').length;
        if (optionsLength > $(materialContainer).find('.items').length) {
            if ($(this).parent().find('.remove-row').length) 
                $(this).remove();
            else 
                $(this).removeClass('add-row fa-plus-circle').addClass('remove-row fa-minus-circle');
            
            if ($(itemObj).find('.remove-row').length == 0) 
                $(itemObj).find('.add-row').after('<i class="fa fa-2x fa-minus-circle remove-row"></i>');
            
            $(materialContainer).find('.items:last').after(itemObj);
        }
        else if ((optionsLength == $(materialContainer).find('.items').length) || (optionsLength == 0)) {
            var smallBoxTitle = 'Sorry ! ...';
            var smallBoxMsg = '<i class="fa fa-info-circle"></i> <i>You can\'t add more...<br> Because you have';
            smallBoxMsg += (optionsLength > 0) ? ' only ' + optionsLength : ' no';
            smallBoxMsg += ($(materialContainer).find('.items').length > 1) ? ' materials' : ' material';
            smallBoxMsg += '.</i>';
            
            if($('#divSmallBoxes').html().length == 0)
                showSmallBox(smallBoxTitle, smallBoxMsg);
        }
    });    
    
    /* Remove row functionality for material */
    $(document).on('click', '.items .remove-row', function() {
        var materialContainer = $(this).parents('.material-container');
        $(this).parents('.items').remove();
        if (materialContainer.find('.items:last').find('.add-row').length == 0) {
            materialContainer.find('.items:last').find('.remove-row').before('<i class="fa fa-2x fa-plus-circle add-row"></i>');
        }
        
        if ((materialContainer.find('.items').length == 1) && (materialContainer.find('.items:last').find('.add-row').length)) {
            materialContainer.find('.items:last').find('.remove-row').remove();
        }
     });   
     
     $(document).on('change', 'select.select2', function() {
        $('form[name="por"]').validate().element(this);
     });
     
     $('input[name="por_date"]').on('changeDate', function() {
        $('form[name="por"]').validate().element('input[name="por_date"]');
     });
     
     $.validator.addMethod('mat_required', function(value, element) {
        return (value != '') ? true : false;
     }, 'Please select Material Name!');
     
     $.validator.addMethod('mat_already_exist', function(value, element) {
        var prevItems = $(element).parents('.items').prevAll('.items');
        return (prevItems.find('select.mat_id option[value="' + value + '"]:selected').length) ? false : true;
     }, 'Material Name is already Exists!');
     
     $.validator.addMethod('qty_received', function(value, element) {
        var return_val = true;
        if($(element).parents('.items').find('select.mat_id').val() == '' ){
            return_val = true;
        }
        else{
          if(value == '') {
             return_val = false; 
          }          
        }
        return return_val;
     }, 'Please enter Returned Quantity!');
    
     $.validator.addMethod('valid_qty', function(value, element) {
        var receivedQty = parseFloat($(element).parents('.items').find('.received_quantity').val());
        var return_val = true;
        if($(element).parents('.items').find('select.mat_id').val() == '' ){
            return_val = true;
        }
        else{
            if((receivedQty != '') && (receivedQty >= parseFloat(value))){
               return_val = true; 
            }
            else{
                return_val = false;
            }
        }
        return return_val;
     }, 'Returned Quantity should not be greater than Received Quantity!');
     
     $.validator.addMethod('min_ware_qty', function(value, element) {
         if($('select.po_id option:selected').val()!='' && $('select.po_id option:selected').val()!=undefined ){
        var return_val = false;
        var ware_id = $('.hidden_brn_id').val();
        var mat_id = $(element).parents('.items').find('select.mat_id').val();
        $.ajax({
            async: false,
            url: baseUrl+'purchase_returns/get_item_qty_by_mat_id',
            type: 'POST',
            data: {mat_id: mat_id, ware_id: ware_id},
            dataType: "json",
        })
        .done(function (data) {
            if(parseFloat(data.quantity.available_qty) < parseFloat(value)) {
                if (!$('.SmallBox').length) {
                    $.smallBox({
                        title : "Error",
                        content : "<i class='fa fa-warning'></i> <i> Insufficient Quantity in Warehouse (" + $('.ware_id').val() + ") </i>",
                        color : "#A90329",
                        iconSmall : "fa fa-thumbs-o-down bounce animated",
                        timeout : 5000
                    });
                }
                return_val = false;
            }
            else {
                return_val = true;
            } 
        });         
       return return_val;
       
         }
}, '');
     
     
     $.validator.addClassRules({
        mat_id: {
            mat_required: true,
            mat_already_exist: true
        },
        returned_quantity: {
            qty_received: true,
            valid_qty: true,
            min_ware_qty: true
        }
    });
    
     var $orderForm = $("#por_form").validate({
        // Rules for form validation
        invalidHandler: function(event, validator) {
            $('body').animate({
                    scrollTop: $(validator.currentForm).offset().top
                },
                '500',
                'swing'
            );
        },
        rules : {
            po_id : {
                    required : true
            },
            por_date : {
                    required : true
            }
        },

        // Messages for form validation
        messages : {
            po_id : {
                    required : 'Please select PO Number'
            },
            por_date : {
                    required : 'Please select Date'
            }
        },

        // Do not change code below
        errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
        }
    });
    
    if(active=='edit'){
        var po = $('select.po_id option:selected').val();
        $('.hidden_po_id').val(po);
        var ware = '';
        var mat_id = '';
        po_wise_warehouse(po,ware);
    }
    
    $('.po_id').on('change', function () {
        var po = $(this).val();
        $('.hidden_po_id').val(po);
        var ware = '';
        var mat_id = '';
        po_wise_warehouse(po,ware);
        po_wise_material(po,mat_id);
    }); 
    
    $(document).on('change', 'select.mat_id', function() {
        var option = $(this).children(':selected');
        var item = $(this).parents('.items');
        item.find('.received_quantity').val(option.data('grn_quantity'));
        item.find('.grni_quantity').val(option.data('hidden_grn_quantity'));
        item.find('.unit_name').val(option.data('unit_name'));
        item.find('.unit_id').val(option.data('unit_id'));
    });
    
});


function po_wise_warehouse(poId,wareId){
    if (poId != '') {
    var ajaxURL = baseUrl + 'purchase_returns/get_branch_by_po';
    var ajaxData = {
        po_id: poId
     }
     var loaderSelector = 'input[name="ware_id"]';
        var data = ajaxRequest(ajaxURL, ajaxData, loaderSelector);
        $(".supplier_name").val(data.supplier.name);
        $(".branch_name").val(data.brn.branch_name);
        $(".hidden_brn_id").val(data.brn.brnid);
        $(".contact_person").val(data.supplier.contact_name);
        $(".contact_no").val(data.supplier.phone_no);
        $(".supplier_address").val(data.supplier.address);
    } else {
        $(".supplier_name").val('');
        $(".branch_name").val('');
        $(".contact_person").val('');
        $(".contact_no").val('');
        $(".supplier_address").val('');
    }     
}

function po_wise_material(poId,matId){
    if (poId != '') {
        $.ajax({
            async: false,
            url: baseUrl+'purchase_returns/get_material_by_po',
            type: 'POST',
            data: {po_id: poId},
            dataType: "json",
        })
        .done(function (data) {
            //console.log(data);
            if (data.mats.length > 0) {
                var mat_list = '';
                mat_list = '<option value="">Select</option>';                    
                $.each(data.mats, function (index, value) {
                    var returned_qty = (value.already_returned_qty != '') ? value.already_returned_qty : 0;
                    var available_qty = parseFloat(value.pri_quantity) -  parseFloat(returned_qty);
                    mat_list += '<option value="' + value.product_id + '" data-hidden_grn_quantity="' + value.pri_quantity + '" data-grn_quantity="' + available_qty + '" data-unit_name="' + value.unit_name + '" >' + value.pdt_code + ' / '+ value.cat_name + ' - ' + value.pdt_name + '</option>';                                  
                })
                $("select.mat_id").html(mat_list);
                if(matId!=''){
                    $("select.mat_id").val(matId).trigger("change");
                }
            }
        });
    }else{
       $("select.mat_id").val('').trigger("change");
    }
}

function calculate_price(_this){
    var received_qty = ($(_this).val() !='') ? $(_this).val() : 0;
    var rate = ($(_this).parents('.items').closest('div').find('.grni_price').val() != '') ? $(_this).parents('.items').closest('div').find('.grni_price').val() : 0;
    
    var price = parseFloat(received_qty) * parseFloat(rate);
    $(_this).parents('.items').closest('div').find('.grni_total_amt').val(price.toFixed(2));
    if($(_this).val() != '') {
        getsubtotalval();
        getnettotalval();
    }
}

function getsubtotalval() {
    var subtotalpriceval = 0;
    $('.grni_total_amt').each(function() {
		if($(this).val()!='') {
			subtotalpriceval += parseFloat($(this).val());
		}
    });
    $('.grn_sub_total_amt').val(subtotalpriceval.toFixed(2));
    
    var dis = $('.discount_type:checked').val();
    var sub_total = $('.grn_sub_total_amt').val();
    var discount = ($('.discount').val() != '') ? parseFloat($('.discount').val()) : 0;
    var item_total = ($('.grn_sub_total_amt').val() != '') ? parseFloat($('.grn_sub_total_amt').val()) : 0;
    if(dis == '1') {
        var discount_amount =  (parseFloat(item_total) * parseFloat(discount/100));
        var Disamount = discount_amount.toFixed(2);
        $('.dis_per_to_amt').html('<b style="color: #3276B1;">Dis. Amount: ' + Disamount +'</b>');
        $('.dis_to_amount').val(discount_amount.toFixed(2));
        sub_total = parseFloat(item_total) - parseFloat(discount_amount);
    }
    else if(dis == '2') {
        sub_total = parseFloat(item_total) - parseFloat(discount_amount);  
    }
    
    $('.discount_amt').val(sub_total.toFixed(2));
    
    
    $('.hidden_grnt_percentage').each(function() {
        var tax_id = $(this).parents('.separate_row').find('.hidden_tax_id').val();
        var hidden_percentage_amount = ($(this).val() != '') ? parseFloat($(this).val()) : 0;
        var percentage_amount = ($(this).parents('.separate_row').find('.grnt_percentage').val() != '') ? parseFloat($(this).parents('.separate_row').find('.grnt_percentage').val()) : 0;
        var po_sub_amount = ($(this).parents('.separate_row').find('.hidden_po_amount').val() != '') ? parseFloat($(this).parents('.separate_row').find('.hidden_po_amount').val()) : 0;
        var checkbox_val = $(this).parents('.separate_row').find('.grnt_type:checked').val();
        var sub_total = ($('.discount_amt').val() != '') ? parseFloat($('.discount_amt').val()) : 0;
        if(percentage_amount != '') {
            if(checkbox_val == '1') {
                var tax_amount =  (parseFloat(sub_total) * parseFloat(percentage_amount/100)); 
            } 
            else if(checkbox_val == '2') {
                var tax_amount = ( parseFloat(hidden_percentage_amount) / parseFloat(po_sub_amount)) * parseFloat(sub_total) ;
                $(this).parents('.separate_row').find('.grnt_percentage').val(tax_amount.toFixed(2));
            }
            $(this).parents('.separate_row').find('.grnt_amt').val(tax_amount.toFixed(2));
            getnettotalval();
        }
        
    });
   
}