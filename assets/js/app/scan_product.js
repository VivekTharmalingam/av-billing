$(document).ready(function () {
    $.validator.addMethod("codeExists", function (value, element) {
        var return_val = true;
        var ajaxURL = baseUrl + 'item/check_product_code_exists';
        var ajaxData = {
            pdt_code: $('#pdt_code').val()
        }
        var data = ajaxRequest(ajaxURL, ajaxData, '');      
        if (data == '1') {
            return_val = false
        }
        return return_val;
    });
    var companyForm = $("#product_form").validate({
        // Rules for form validation
        rules: {
            search_barcode: {
                required: true,
            },
            new_quantity: {
                required: true
            },
            pdt_code: {
                codeExists: true
            },
        },
        // Messages for form validation
        messages: {
            search_barcode: {
                required: 'Bar code should not be empty',
            },
            new_quantity: {
                required: 'Please enter quantity',
            },
            pdt_code: {
                codeExists: 'This Item code already exists'
            },
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

    var items = get_all_items('');
    $("#search_barcode").autocomplete({
        source: items,
        select: function (event, ui) {
            $('.product_code').html(ui.item.pdt_code);
            $('.product_desc').html(ui.item.pdt_name);
            $('.brand_name').html(ui.item.cat_name);
            $('#hidden_pdt_id').val(ui.item.id);
            qty = 0;
            if (ui.item.available_qty != '') {
                qty = ui.item.available_qty;
            }
            $('.avail_qty').html(qty);
            $('.new_quantity').val(1);
            $('.product_details').show('slow');
            $(this).val(ui.item.bar_code);
            return false;
        }
    });
    $(document).on('change', '.search_barcode', function () {
        var ajaxURL = baseUrl + 'item/get_product_by_barcode';
        var ajaxData = {
            bar_code: $(this).val()
        }
        var data = ajaxRequest(ajaxURL, ajaxData, '');
        if (data.length != '0') {
            $('#method').val('1');
            $('.old_product').show();
            $('.new_product').hide();
            $('.product_code').html(data.pdt_code);
            $('.product_desc').html(data.pdt_name);
            $('.brand_name').html(data.cat_name);
            $('#hidden_pdt_id').val(data.id);
            qty = 0;
            if (data.available_qty != '') {
                qty = data.available_qty;
            }
            $('.avail_qty').html(qty);
            $('.new_quantity').val(1);
            $('.product_details').show('slow');
        } else {
            $('#bar_code').val($('.search_barcode ').val());
            $('#method').val('2');
            $('.old_product').hide();
            $('.new_product').show();
        }
    });
});
function get_all_items(item) {
    var ajaxURL = baseUrl + 'item/search_item_by_barcode';
    var ajaxData = {
        item: item
    }
    return ajaxRequest(ajaxURL, ajaxData, '');
}