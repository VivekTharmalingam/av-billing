/* Barcode search functions */
function get_product_details() {
    if ($('input[name="search_product"]').val() == '') {
        var smallBoxTitle = 'Sorry ! ...';
        var smallBoxMsg = '<i class="fa fa-info-circle"></i> <i>Please enter product code or name.</i>';
        if ($('#divSmallBoxes').html().length == 0)
            showSmallBox(smallBoxTitle, smallBoxMsg);
    }

    return false;
}

function disable_payment() {
    if (!$('.payment_type').hasClass('payment_type_req'))
        $('.payment_type').removeClass('payment_type_req');

    $('#sales_order').data('form_submit', '1');
    //$("#sales_order").submit();
}
function calculate_discount() {
    var sub_total = ($('.sub_total').val() != '') ? parseFloat($('.sub_total').val()) : 0;
    var discount = ($('.discount').val() != '') ? parseFloat($('.discount').val()) : 0;
    if ($('.discount_type:checked').val() == '1') { // Discount percentage
        discount = parseFloat((discount / 100) * sub_total);
    }

    items.find('.discount_amount').html(discount.toFixed(2));
    items.find('.discount_amt').val(discount.toFixed(2));
    sub_total -= discount;
    items.find('.sub_total').val(sub_total.toFixed(2));
    calculate_total();
}

function calculate_total() {
    var sub_total = 0;
    $('.amount').each(function (index, element) {
        sub_total += ($(element).val() != '') ? parseFloat($(element).val()) : 0;
    });
    $('.sub_total').val(sub_total.toFixed(2));

    var discount = ($('.discount').val() != '') ? parseFloat($('.discount').val()) : 0;
    if ($('.discount_type:checked').val() == '1') { // Discount percentage
        discount = parseFloat((discount / 100) * sub_total);
    }

    $('.discount_amount').html(discount.toFixed(2));
    $('.discount_amt').val(discount.toFixed(2));
    sub_total -= discount;

    var total_amount = sub_total;
    var gst_percentage = 0;
    var gst_amount = 0;
    if ($('.gst_type:checked').val() == '2') {
        gst_percentage = ($('#hidden_gst').val() != '') ? parseFloat($('#hidden_gst').val()) : 0;
        var gst_amount = parseFloat((gst_percentage / 100) * sub_total);
        total_amount = sub_total + gst_amount;
    }

    $('.gst_amount').val(gst_amount.toFixed(2));
    $('.gst_percentage').html(gst_percentage.toFixed(2));
    $('.total_amt').val(total_amount.toFixed(2));
    $('.payment_amount').html(total_amount.toFixed(2));
}

function get_all_items_descriptions() {
    var ajaxURL = baseUrl + 'invoice/search_item_description';
    var ajaxData = {}
    
    return ajaxRequest(ajaxURL, ajaxData, '');
}

function get_all_items(product_code) {
    var ajaxURL = baseUrl + 'invoice/search_item';
    var ajaxData = {
        product_code: product_code
    }

    return ajaxRequest(ajaxURL, ajaxData, '');
}

function hide_product_code_msg() {
    $('.product_code_label').slideUp();
}

function hide_error_description_msg() {
    $('.error_description').slideUp();
}

var items = get_all_items('');
//var product_codes = get_all_items(1);
var product_codes = get_all_items(1);
var descriptions = get_all_items_descriptions('');
function valid_item_code(item_code) {
	var item_code_exists = false;
	for(var i in product_codes) {
		if (product_codes[i].pdt_code.trim().toLowerCase() == item_code.val().trim().toLowerCase()) {
			item_code_exists = true;
			break;
		}
	}
	
	if (!item_code_exists) {
		item_code.val('');
		item_code.focus();
		return false;
	}
}

function reset_item() {
        items = get_all_items('');
	product_codes = get_all_items(1);
	descriptions = get_all_items_descriptions('');
	$(".product_code, .product_description").removeClass('hasAutocomplete');
	set_product_autocomplete();
}

function reset_customer() {
	var ajaxURL = baseUrl + 'invoice/active_customers';
    var ajaxData = {}
    
    var data = ajaxRequest(ajaxURL, ajaxData, '');
	var _option = '';
	_option += '<option value="cash">Cash</option>';
	for(var i in data) {
		_option += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
	}
	
	$('#customer_name').html(_option);
	$('#customer_name').select2({width: '100%'});
}

function set_product_autocomplete() {
    $(".product_code").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }
		
		$(this).on('change', function() {
			valid_item_code($(this));
		});

        $(this).autocomplete({
            source: function(request, response) {
				var results = $.ui.autocomplete.filter(product_codes, request.term);
				response(results.slice(0, 15));
			},
            focus: function (event, ui) {
				show_available_quantity(event.currentTarget, ui.item.branch_products);
            },
			open: function() {
				hide_available_quantity();
            },
			close: function() {
				hide_available_quantity();
				if ($(this).val().trim().length) 
					$(this).closest('.items').find('.product_description').trigger('focus');
			},
            select: function (event, ui) {
                var items = $(this).closest('.items');
                //if ($('.product_details').find('.product_id[value="' + ui.item.id + '"]').length) {
				var product_id_name = items.find('.product_id').prop('name');
				if ($('.product_details').find('.product_id[value="' + ui.item.id + '"]:not(.product_id[name="' + product_id_name + '"])').length) {
                    // Item already exists
                    if ($(this).data('value').trim() != ui.item.value.trim()) {
                        items = $('.product_details').find('.product_id[value="' + ui.item.id + '"]').closest('.items');
                        
						$(this).val('');
                        $(this).closest('.items').find('.product_code_label').slideDown();
                        setTimeout(hide_product_code_msg, 3000)
                        return false;
                    }
                    else {
                        $(this).val(ui.item.value.trim());
						des = ui.item.pdt_name;
						if (ui.item.cat_name != '' && ui.item.cat_name != null) {
							des = ui.item.cat_name + ' ' + ui.item.pdt_name;
						}
						if (ui.item.pdt_spec != '' && ui.item.pdt_spec != null) {
							des = des + ' ' + ui.item.pdt_spec;
						}
						console.log(des);
						items.find('.product_description').val(des.trim());
                    }
                }
                else {
					$(this).data('value', ui.item.pdt_code.trim());
                    $(this).val(ui.item.pdt_code.trim());
                    items.find('.category_id').val(ui.item.cat_id);
                    items.find('.product_id').val(ui.item.id);
//                    items.find('.brand_name').html(ui.item.cat_name);
//                    items.find('.brand_label').slideDown();
//                    items.find('.product_description').val(ui.item.pdt_name);
                    des = ui.item.pdt_name;
                    if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                        des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                    }
					if (ui.item.pdt_spec != '' && ui.item.pdt_spec != null) {
						des = des + ' ' + ui.item.pdt_spec;
					}
					console.log(des);
                    items.find('.product_code').data('value', ui.item.pdt_code.trim());
                    items.find('.product_description').val(des.trim());
					var selling_price = (ui.item.selling_price != '') ? parseFloat(ui.item.selling_price) : 0;
                    items.find('.price').val(selling_price.toFixed(2));
                    if (items.find('.cost').length) {
						var buying_price = (ui.item.buying_price != '') ? parseFloat(ui.item.buying_price) : 0;
                        items.find('.cost').val(buying_price.toFixed(2));
					}
                    items.find('.quantity').val(1);
                    items.find('.amount').val(selling_price.toFixed(2));
                }
                calculate_total();
                
                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || event.keyCode;
                if (charCode == 13) 
                    items.find('.quantity').focus();
                else if (event.type == 'autocompleteselect') 
                    items.find('.product_description').focus();
                
                /* To focus quantity of particular item end */
                
                return false;
            }
        });

        $(this).addClass('hasAutocomplete');
    });
    
    $(".product_description").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }

        $(this).autocomplete({
            source: descriptions,
            source: function(request, response) {
				var results = $.ui.autocomplete.filter(product_codes, request.term);
				response(results.slice(0, 15));
			},
            focus: function (event, ui) {
				show_available_quantity(event.currentTarget, ui.item.branch_products);
            },
			open: function() {
				hide_available_quantity();
            },
			close: function() {
				hide_available_quantity();
				if ($(this).val().trim().length) 
					$(this).closest('.items').find('.product_description').trigger('focus');
			},
            select: function (event, ui) {
                //alert('test');
                var items = $(this).closest('.items');
                //if ($('.product_details').find('.product_id[value="' + ui.item.id + '"]').length) {
				var product_id_name = items.find('.product_id').prop('name');
				if ($('.product_details').find('.product_id[value="' + ui.item.id + '"]:not(.product_id[name="' + product_id_name + '"])').length) {
                    // Item already exists
                    if ($(this).data('value').trim() != ui.item.value.trim()) {
                        items = $('.product_details').find('.product_id[value="' + ui.item.id + '"]').closest('.items');
                        $(this).val($(this).data('value').trim());
                        $(this).closest('.items').find('.error_description').slideDown();
                        setTimeout(hide_error_description_msg, 3000);
                        items.find('.quantity').focus();
                        //return false;
                    }
                    else {
                        $(this).val(ui.item.value.trim());
                    }
                }
                else {
                    items.find('.product_code').val(ui.item.pdt_code.trim());
                    //$(this).val(ui.item.pdt_code);
                    items.find('.category_id').val(ui.item.cat_id);
                    items.find('.product_id').val(ui.item.id);
//                    items.find('.brand_name').html(ui.item.cat_name);
//                    items.find('.brand_label').slideDown();
//                    items.find('.product_description').val(ui.item.pdt_name);
                    des = ui.item.pdt_name;
                    if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                        des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                    }
					if (ui.item.pdt_spec != '' && ui.item.pdt_spec != null) {
						des = des + ' ' + ui.item.pdt_spec;
					}
					des = des.trim();
                    items.find('.product_description').data('value', des);
                    items.find('.product_description').val(des);
					var selling_price = (ui.item.selling_price != '') ? parseFloat(ui.item.selling_price) : 0;
                    items.find('.price').val(selling_price.toFixed(2));
                    if (items.find('.cost').length) {
						var buying_price = (ui.item.buying_price != '') ? parseFloat(ui.item.buying_price) : 0;
                        items.find('.cost').val(buying_price.toFixed(2));
					}
                    items.find('.quantity').val(1);
                    items.find('.amount').val(selling_price.toFixed(2));
                }
                calculate_total();
                
                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || event.keyCode;
                if (charCode == 13) 
                    items.find('.quantity').focus();
                else if (event.type == 'autocompleteselect') 
                    items.find('.product_description').focus();
                
                /* To focus quantity of particular item end */
                //return false;
            }
        });

        $(this).addClass('hasAutocomplete');
    });
}

function delivery_to_change_focus() {
    if ($('#ship_to').val() == '') 
        $('input[name="location_name"]').focus();
    else 
        $('#your_reference').focus();
}

function focus_quantity(qty) { 
    $(qty).focus();
}

var addRowIndex = 0;
var scanItem = 0;
var tabPressed = 0;
$(document).ready(function () {
    /*var ship_toSelect = $('#customer_name');
    $('#ship_to').on('keydown', function (e) {
        alert("select2:close");
    });*/
	
    /* Function to focus customer name on first tab */
    if ($('.edit_form').length == 0) {
        $(document).keydown(function (e) {
           if ((e.which == 9) && (tabPressed == 0)) {
               $('#customer_name').select2('open');
               $('#customer_name').select2('val', '');
               $('#customer_name').focus();
               tabPressed ++;
           }
        });
    }
	
    $('#search_product').focus();
	
    $( "#your_reference" ).focusin(function() {
        $('#your_reference').data('focused', true);
    });
    
    $( "#remarks" ).focusin(function() {
        if (!$('#your_reference').data('focused')) {
            $('#your_reference').focus();
        }
    });
    
    $(document).on('focus', ".quantity, .price, .cost", function() {
        /*alert('test');
        if (!$(this).data('focused')) {
            $(this).focus();
        }
        
        return false;*/
        $(this).select();
    });
    
    //$('#customer_name').select2({width: '100%'}).trigger('click');
    /* Textarea new line placeholder start */
    /*var textAreas = $('textarea');
     Array.prototype.forEach.call(textAreas, function(elem) {
     elem.placeholder = elem.placeholder.replace(/\\n/g, '\n');
     });*/
    /* Textarea new line placeholder end */

    /*$(document).on('change', '.cus_type', function () {
        if ($(this).val() == '2') {
            $('.reg_customer_add').hide();
            $('.current_address').show();
            $('.cash_customer_name').show();
            $('#cash_customer_name').focus();
        }
        else {
            $('.reg_customer_add').show();
            $('.current_address').hide();
            $('.cash_customer_name').hide();
            $('select[name="customer_name"]').focus();
        }
    });*/
    
    $('.so_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    
    $("#search_product").keydown(function (e) {
        e = e || window.event;
        var charCode = e.which || e.keyCode;
        if (charCode == 13) {
            return false;
        }
    });
    
	/*
	// To focus bar code scan field
    $(document).on('keydown', 'input.quantity', function (e) {
        e = e || window.event;
        var charCode = e.which || e.keyCode;
        if (charCode == 9) {
            $("#search_product").focus();
            return false;
        }
    });*/
    
    $(document).on('keyup', 'input.search_product', function (e) {
        var ser = $(this).val();
        var _this = $(this);
        if (ser) {
            $.each(items, function (key, value) {
                var s_no_array = new Array();
                if (value.s_no != '')
                        s_no_array = value.s_no.split(',');
                    
		if ((ser.toUpperCase() === value.bar_code.toUpperCase()) || ($.inArray(ser, s_no_array) !== -1) || (ser.toUpperCase() == value.pdt_code.toUpperCase())) {
                    if ($('.product_details').find('.product_id[value="' + value.id + '"]').length) {
                        // Item already exists
                        var items = $('.product_details').find('.product_id[value="' + value.id + '"]').closest('.items');
                        var quantity = (items.find('.quantity').val() != '') ? parseFloat(items.find('.quantity').val()) : 0;
                        quantity += 1;
                        items.find('.quantity').val(quantity);
                        var price = (items.find('.price').val() != '') ? parseFloat(items.find('.price').val()) : 0;
                        items.find('.amount').val((price * quantity).toFixed(2));
                    }
                    else {
                        if ($('.product_details .items:last .product_id').val() != '') {
                            // Last row occupied by another product.
                            $('.product_details .items:last .add-row').trigger('click');
                        }
                        var items = $('.product_details .items:last');
                        items.find('.category_id').val(value.cat_id);
                        items.find('.product_id').val(value.id);
                        items.find('.product_code').val(value.pdt_code);
                        //items.find('.brand_name').html(value.cat_name);
                        //items.find('.brand_label').slideDown();
                        des = value.pdt_name;
                        if (value.cat_name != '' && value.cat_name != null) {
                            des = value.cat_name + ' ' + value.pdt_name + ' ' + value.pdt_spec;
                        }
                        items.find('.product_description').val(des);
						var selling_price = (value.selling_price != '') ? parseFloat(value.selling_price) : 0;
                        items.find('.price').val(selling_price.toFixed(2));
                        if (items.find('.cost').length) {
							var buying_price = (value.buying_price != '') ? parseFloat(value.buying_price) : 0;
							items.find('.cost').val(buying_price.toFixed(2));
						}

                        items.find('.quantity').val(1);
                        items.find('.amount').val(selling_price.toFixed(2));
                    }
                    //items.find('.quantity').focus().select();
                    
                    calculate_total();
                    $(_this).val('');
                    scanItem = 1;
                    return false;
                }
            });
        }
    });
	
    // To set autocomplete in product add row
    set_product_autocomplete();
	
	$(document).on('focusout', 'input.product_description', function (e) {
        hide_available_quantity();
    });
	
    $(document).on('focus', 'input.product_code, input.product_description', function (e) {
        var item = {};
        var product_code = $(this).closest('.items').find('.product_code').val().trim();
        if (product_code != '') {
            for (var i in product_codes) {
                if ((product_code != '') && (product_codes[i].label.trim() == product_code)) {
                    item = product_codes[i];
                    show_available_quantity($(this), product_codes[i].branch_products);
                    break;
                }
            }
        }
		else {
            hide_available_quantity();
        }
    });
	
    $(document).on('blur', 'input.product_code', function (e) {
        var item = {};
        var product_code = $(this).val().trim();
        if (product_code != '') {
            for (var i in product_codes) {
                if ((product_code != '') && (product_codes[i].label.trim() == product_code)) {
                    item = product_codes[i];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: item});
                    break;
                }
            }
        }
		else {
            hide_available_quantity();
        }
    });
	
    $('#customer_name').on('change', function (e) {
        /*if ($(this).val() == '') {
         $('#ship_to').find('option:not(:first)').remove();
         $('#ship_to').select2('val', '');
         $('.cust_address').slideUp();
         $('.new_shipping_address').slideDown();
         $('.ship_to_address').slideUp();
         return '';
         }*/
         //alert('test');
        if ($(this).val() == 'cash') {
            $('#ship_to').find('option:not(:first)').remove();
            $('#ship_to').select2('val', '');
			$('#payment_terms').select2('val', '');
            $('.reg_customer_add').slideUp();
            $('.cash_customer_name').slideDown();
            $('input[name="cash_customer_name"]').focus();
            //$('#cash_customer_name').prop('tabindex', '2');
            return '';
        }
        
        $('.cash_customer_name').slideUp();
        $('.new_shipping_address').slideUp();
        
        var ajaxURL = baseUrl + 'invoice/shipping_address_by_customer';
        var ajaxData = {
            customer_id: $(this).val()
        }

        var data = ajaxRequest(ajaxURL, ajaxData, '');
        var html = '';
        if (data.customer.address != '')
            html = '<p class="alert alert-info not-dismissable"> Address: ' + data.customer.address + '</p>';
        else
            html = '<p class="alert alert-warning not-dismissable">No Address for this Customer</p>';
		
        $('.cust_address').html(html);
        $('.cust_address').slideDown();
        
        var _option = '<option value="">Use different Location</option>';
        var default_address = '';
        var shipping_address = '';
        $.each(data.addresses, function (key, value) {
            _option += '<option value="' + value.id + '"';
            if (value.is_default == '1') {
                default_address = value.id;
                shipping_address = '<p class="alert alert-info not-dismissable"> Delivery Address: ' + value.address + '</p>';
            }

            _option += ' data-address="' + value.address + '">' + value.location_name + '</option>';
        });

        $('#ship_to').html(_option);
        $('#ship_to').select2('val', default_address);
        $('#payment_terms').select2('val', data.customer.payment_term);
        $('.ship_to_address').html(shipping_address);
        $('.ship_to_address').slideDown();
        $('.reg_customer_add').slideDown();
        
        $('select[name="ship_to"]').select2('open');
        $('select[name="ship_to"]').focus();
        //$('select[name="ship_to"]').select2('val', '');
        //$('#cash_customer_name').prop('tabindex', '-1');
        //$('#ship_to').prop('tabindex', '2');
        if (default_address == '') {
            $('.new_shipping_address').slideDown();
        }
        
        tabPressed ++;
        /*$('#ship_to').select2('close', function() {
            alert('sf');
        });*/
    });
    
    $('#ship_to').on('change', function () {
        if ($(this).val() == '') {
            $('.new_shipping_address').slideDown();
            $('.ship_to_address').slideUp();
        }
        else {
            $('.new_shipping_address').slideUp();
            var ship_to_address = $(this).find('option:selected').data('address');
            html = '';
            if (ship_to_address != '')
                html = '<p class="alert alert-info not-dismissable"> Delivery Address: ' + ship_to_address + '</p>';
            
            $('.ship_to_address').html(html);
            $('.ship_to_address').slideDown();
        }
        
        setTimeout(delivery_to_change_focus, 100);
        //return false;
    });
    
    $(document).on('keyup', '.quantity, .price', function () {
        var items = $(this).closest('.items');
        var quantity = (items.find('.quantity').val() != '') ? parseFloat(items.find('.quantity').val()) : 0;
        var price = (items.find('.price').val() != '') ? parseFloat(items.find('.price').val()) : 0;
        items.find('.amount').val((quantity * price).toFixed(2));
        calculate_total();
    });

    $(document).on('keyup', '.discount', function () {
        calculate_total();
    });

    $(document).on('change', '.discount_type, .discount', function () {
        calculate_total();
    });

    $(document).on('change', '.gst_type', function () {
        calculate_total();
    });

    /* Add row functionality for material */
    $(document).on('click', '.items .add-row', function () {
        addRowIndex++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.items').clone();
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');
        itemObj.find('.brand_name').html('');
        itemObj.find('.brand_label').hide();
        //itemObj.find('.vat_percentage').attr('checked', false);

        itemObj.find('input, select, textarea').each(function (index, element) {
            if ($(element).hasClass('item_hidden_ids')) {
                $(element).remove();
            }
            
            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndex + ']');
            }
            $(element).val('');
            
            if ($(element).hasClass('product_description'))
                $(element).data('value', '');
            
            if ($(element).hasClass('hasAutocomplete'))
                $(element).removeClass('hasAutocomplete');
			
			if ($(element).hasClass('product_code')) {
				$(element).data('value', '');
			}

            /*if ($(element).prop('tagName').toLowerCase() == 'select') {
             if ($(element).prev().hasClass('select2')) {
             $(element).prev().remove();
             $(element).select2({width: '100%'});
             }
             }*/
        });

        var materialContainer = $(this).parents('.material-container');
        var optionsLength = items.length;
        if (optionsLength > $(materialContainer).find('.items').length) {
            if ($(this).parent().find('.remove-row').length)
                $(this).remove();
            else
                $(this).removeClass('add-row fa-plus-circle').addClass('remove-row fa-minus-circle');

            if ($(itemObj).find('.remove-row').length == 0)
                $(itemObj).find('.add-row').after('<i class="fa fa-2x fa-minus-circle remove-row"></i>');

            $(materialContainer).find('.items:last').after(itemObj);
        }
        else if ((optionsLength == $(materialContainer).find('.items').length) || (optionsLength == 0)) {
            var smallBoxTitle = 'Sorry ! ...';
            var smallBoxMsg = '<i class="fa fa-info-circle"></i> <i>You can\'t add more...<br> Because you have';
            smallBoxMsg += (optionsLength > 0) ? ' only ' + optionsLength : ' no';
            smallBoxMsg += ($(materialContainer).find('.items').length > 1) ? ' items' : ' item';
            smallBoxMsg += '.</i>';

            if ($('#divSmallBoxes').html().length == 0)
                showSmallBox(smallBoxTitle, smallBoxMsg);
        }

        set_product_autocomplete();
        $('.items:last .product_code').focus();
    });
	
    /* Remove row functionality for material */
    $(document).on('click', '.items .remove-row', function () {
        var materialContainer = $(this).parents('.material-container');
        $(this).parents('.items').remove();
        if (materialContainer.find('.items:last').find('.add-row').length == 0) {
            materialContainer.find('.items:last').find('.remove-row').before('<i class="fa fa-2x fa-plus-circle add-row"></i>');
        }

        if ((materialContainer.find('.items').length == 1) && (materialContainer.find('.items:last').find('.add-row').length)) {
            materialContainer.find('.items:last').find('.remove-row').remove();
        }
        calculate_total();
        $('.items:last .product_code').focus();
    });

    $.validator.addMethod('mat_already_exist', function (value, element) {
        var prevItems = $(element).parents('.items').prevAll('.items');
        return (prevItems.find('.product_id[value="' + value + '"]').length) ? false : true;
    }, 'Item Code is already exist!');

    $.validator.addMethod('product_code_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please select Product Code!');

    $.validator.addMethod('qty_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please enter Quantity!');

    $.validator.addMethod('payment_type_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please select Payment Type!');
	
	$.validator.addMethod('your_reference_exist', function (value, element) {
		var formType = $(element).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var return_val = true;
        var your_reference = $(element).val();
        var invoice_id = (formType == 'edit') ? $('input[name="so_id"]').val() : '';
        if (your_reference != '') {
			var ajaxURL = baseUrl + 'invoice/check_your_reference';
			var ajaxData = {
				your_reference: your_reference,
				invoice_id: invoice_id
			}

			var data = ajaxRequest(ajaxURL, ajaxData, '');
			if (data.exist >= 1) {
				return_val = false;
			}
        }
        return return_val;
    });

    $.validator.addClassRules({
        product_code: {
            product_code_required: true,
            mat_already_exist: true
        },
        quantity: {
            qty_required: true
        },
        payment_type_req: {
            payment_type_required: true
        }
    });

    $('select[name="customer_name"], select[name="ship_to"]').on('change', function () {
        $('#sales_order').validate().element(this);
    });

    $('input[name="so_date"], input[name="cheque_date"], input[name="dd_date"]').on('changeDate', function () {
        $('#sales_order').validate().element(this);
    });

    $("#sales_order").validate({
        // Rules for form validation
        invalidHandler: function (event, validator) {
            $('body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top - 100
            },
            '500',
                    'swing'
                    );
        },
        rules: {
            customer_name: {
                required: true
            },
			your_reference: {
                your_reference_exist: true
            },
            so_date: {
                required: true
            },
            cheque_bank_name: {
                required: true
            },
            cheque_acc_no: {
                required: true
            },
            cheque_date: {
                required: true
            },
            dd_no: {
                required: true
            },
            dd_date: {
                required: true
            }
        },
        // Messages for form validation
        messages: {
            customer_name: {
                required: 'Please select Sold To!'
            },
			your_reference: {
                your_reference_exist: 'Customer reference already exists!'
            },
            so_date: {
                required: 'Please select Date!'
            },
            cheque_bank_name: {
                required: 'Please enter Bank Name!'
            },
            cheque_acc_no: {
                required: 'Please enter Cheque No!'
            },
            cheque_date: {
                required: 'Please enter Cheque Date!'
            },
            dd_no: {
                required: 'Please enter DD No!'
            },
            dd_date: {
                required: 'Please enter DD Date!'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
			if (scanItem) {
				scanItem = 0;
				return false;
			}
            form.submit();
        }
    });

    /* Salse Payment operation start */
    $('#pay_now').on('click', function () {
        if (!$('.payment_type').hasClass('payment_type_req'))
            $('.payment_type').addClass('payment_type_req');

        $('#sales_order').data('form_submit', '1');
    });

    $('#skip_payment').on('click', function () {
        $('#sales_order').data('form_submit', '1');
    });

    //$('.cheque_details').hide();
    //$('.dd_details').hide();

    $('#payment_type').on('change', function () {
        $('.cheque_details').slideUp();
        //$('.dd_details').hide();

        if ($(this).find('option:selected').data('is_cheque') == '1')
            $('.cheque_details').slideDown();

        $('#pay_now').focus();
    });
    /* Salse Payment operation end */
});

function show_sales_form() {
    $('#sales_order').data('form_submit', '0');
    $('#payment_form').hide();
    $('#sales_form').show();
    $('#search_product').focus();
}