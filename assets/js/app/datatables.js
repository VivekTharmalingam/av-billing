/* 
 * Purpose : Server Side Datatables
 * Author  : Sankar Vellaisamy
 * Created On : 10/2/2017 10:53:00
 */
$(document).ready(function () {
    
    switch(current_controller) {
        case 'user':
            controller = baseUrl + 'index.php/user/datatable';
            columnDef = [
                {"data": "iterator", "name": "iterator"},
                {"data": "uname", "name": "uname"},
                {"data": "username", "name": "username"},
                {"data": "useremail", "name": "useremail"},
                {"data": "status_str", "name": "status_str"},
                {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'customer_group':
            controller = baseUrl + 'index.php/customer_group/datatable';
            columnDef = [
                {"data": "tId", "name": "tId"},
                {"data": "name", "name": "name"},
                {"data": "crated_date", "name": "crated_date"},
                {"data": "status_str", "name": "status_str"},
                {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'customer':
            controller = baseUrl + 'index.php/customer/datatable';
            columnDef = [
                {"data": "cId", "name": "cId"},
                {"data": "cust_grp", "name": "cust_grp"},
                {"data": "customer", "name": "customer"},
                {"data": "name", "name": "name"},
                {"data": "phone", "name": "phone"},
                {"data": "cusemail", "name": "cusemail"},
                {"data": "status_str", "name": "status_str"},
                {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'supplier':
            controller = baseUrl + 'index.php/supplier/datatable';
            columnDef = [
                {"data": "supplier_id", "name": "supplier_id"},
                {"data": "supplier_name", "name": "supplier_name"},
                {"data": "scontact_contact_name", "name": "scontact_contact_name"},
                {"data": "supplier_phone", "name": "supplier_phone"},
                {"data": "supplier_email", "name": "supplier_email"},
                {"data": "status_str", "name": "status_str"},
                {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'service_partner':
            controller = baseUrl + 'index.php/service_partner/datatable';
            columnDef = [
                {"data": "service_partner_id", "name": "service_partner_id"},
                {"data": "service_partner_name", "name": "service_partner_name"},
                {"data": "scontact_contact_name", "name": "scontact_contact_name"},
                {"data": "service_partner_phone", "name": "service_partner_phone"},
                {"data": "service_partner_email", "name": "service_partner_email"},
                {"data": "status_str", "name": "status_str"},
                {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'brand':
            controller = baseUrl + 'index.php/brand/datatable';
            columnDef = [
                {"data": "bId", "name": "bId"},
                {"data": "code", "name": "code"},
                {"data": "name", "name": "name"},
                {"data": "status_str", "name": "status_str"},
                {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'service_status':
            controller = baseUrl + 'index.php/service_status/datatable';
            columnDef = [
                {"data": "bId", "name": "bId"},
                {"data": "name", "name": "name"},
                {"data": "Actions", "name": "", "sClass": "text-center"},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'item_category':
            controller = baseUrl + 'index.php/item_category/datatable';
            columnDef = [
                {"data": "tId", "name": "tId"},
                {"data": "name", "name": "name"},
                {"data": "created_date", "name": "created_date"},
                {"data": "status_str", "name": "status_str"},
                {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'sub_category':
            controller = baseUrl + 'index.php/sub_category/datatable';
            columnDef = [
                {"data": "tId", "name": "tId"},
                {"data": "cat_name", "name": "cat_name"},
                {"data": "name", "name": "name"},
                {"data": "created_date", "name": "created_date"},
                {"data": "status_str", "name": "status_str"},
                {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'item':
            controller = baseUrl + 'index.php/item/datatable';
            columnDef = [
                {"data": "pid", "name": "pid"},
                {"data": "code", "name": "code"},
                {"data": "bar_code", "name": "bar_code", "sClass": "datatable-column-hidden"},
                {"data": "s_no", "name": "s_no", "sClass": "datatable-column-hidden"},
                {"data": "catname", "name": "catname"},
                {"data": "pname", "name": "pname"},
                {"data": "status_str", "name": "status_str"},
                {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'employee':
            controller = baseUrl + 'index.php/employee/datatable';
            columnDef = [
                {"data": "eId", "name": "eId"},
                {"data": "name", "name": "name"},
                {"data": "desgn", "name": "desgn"},
                {"data": "nric", "name": "nric"},
                {"data": "expiry_date", "name": "expiry_date"},
                {"data": "status_str", "name": "status_str"},
                {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'expense_type':
            controller = baseUrl + 'index.php/expense_type/datatable';
            columnDef = [
                {"data": "tId", "name": "tId"},
                    {"data": "name", "name": "name"},
                    {"data": "crated_date", "name": "crated_date"},
                    {"data": "status_str", "name": "status_str"},
                    {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'payment_type':
            controller = baseUrl + 'index.php/payment_type/datatable';
            columnDef = [
               {"data": "pId", "name": "pId"},
                    {"data": "name", "name": "name"},
                    {"data": "created", "name": "created"},
                    {"data": "status_str", "name": "status_str"},
                    {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'purchase_with_purchase':
            controller = baseUrl + 'index.php/purchase_with_purchase/datatable';
            columnDef = [
                    {"data": "bId", "name": "bId"},
                    {"data": "item", "name": "item"},
                    {"data": "f_date", "name": "f_date"},
                    {"data": "t_date", "name": "t_date"},
                    {"data": "status_str", "name": "status_str"},
                    {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'buy_one_get_one':
            controller = baseUrl + 'index.php/buy_one_get_one/datatable';
            columnDef = [
                    {"data": "bId", "name": "bId"},
                    {"data": "item", "name": "item"},
                    {"data": "f_date", "name": "f_date"},
                    {"data": "t_date", "name": "t_date"},
                    {"data": "status_str", "name": "status_str"},
                    {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'group_buy':
            controller = baseUrl + 'index.php/group_buy/datatable';
            columnDef = [
                    {"data": "bId", "name": "bId"},
                    {"data": "name", "name": "name"},
                    {"data": "f_date", "name": "f_date"},
                    {"data": "t_date", "name": "t_date"},
                    {"data": "amount", "name": "amount"},
                    {"data": "status_str", "name": "status_str"},
                    {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'discount_sales':
            controller = baseUrl + 'index.php/discount_sales/datatable';
            columnDef = [
                    {"data": "bId", "name": "bId"},
                    {"data": "name", "name": "name"},
                    {"data": "f_date", "name": "f_date"},
                    {"data": "t_date", "name": "t_date"},
                    {"data": "offer_price", "name": "offer_price"},
                    {"data": "status_str", "name": "status_str"},
                    {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'bank':
            controller = baseUrl + 'index.php/bank/datatable';
            columnDef = [
                    {"data": "bId", "name": "bId"},
                    {"data": "name", "name": "name"},
                    {"data": "accountNo", "name": "accountNo"},
                    {"data": "currentBlnc", "name": "currentBlnc"},
                    {"data": "status_str", "name": "status_str"},
                    {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'deposit':
            controller = baseUrl + 'index.php/deposit/datatable';
            columnDef = [
                    {"data": "dpId", "name": "dpId"},
                    {"data": "bankname", "name": "bankname"},
                    {"data": "accountNo", "name": "accountNo"},
                    {"data": "dep_date_str", "name": "dep_date_str"},
                    {"data": "type_name", "name": "type_name"},
                    {"data": "depAmount", "name": "depAmount"},
                    /*{"data": "status_str", "name": "status_str"},*/
                    {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'deduction':
            controller = baseUrl + 'index.php/deduction/datatable';
            columnDef = [
                    {"data": "dedId", "name": "dedId"},
                    {"data": "bankname", "name": "bankname"},
                    {"data": "accountNo", "name": "accountNo"},
                    {"data": "ded_date_str", "name": "ded_date_str"},
                    {"data": "type_name", "name": "type_name"},
                    {"data": "dedAmount", "name": "dedAmount"},
                    /*{"data": "status_str", "name": "status_str"},*/
                    {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'payment':
            controller = baseUrl + 'index.php/payment/datatable';
            columnDef = [
                    {"data": "pymId", "name": "pymId"},
                    {"data": "bankname", "name": "bankname"},
                    {"data": "accountNo", "name": "accountNo"},
                    {"data": "pay_date_str", "name": "pay_date_str"},
                    {"data": "type_name", "name": "type_name"},
                    {"data": "pymtAmount", "name": "pymtAmount"},
                    /*{"data": "status_str", "name": "status_str"},*/
                    {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'bank_transfer':
            controller = baseUrl + 'index.php/bank_transfer/datatable';
            columnDef = [
                    {"data": "btId", "name": "btId"},
                    {"data": "debitbank", "name": "debitbank"},
                    {"data": "creditbank", "name": "creditbank"},
                    {"data": "type_name", "name": "type_name"},
                    {"data": "btAmount", "name": "btAmount"},
                    {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'exchange_order':
            controller = baseUrl + 'index.php/exchange_order/datatable';
            columnDef = [
                    {"data": "exId", "name": "exId"},
                    {"data": "exr_code", "name": "exr_code"},
                    {"data": "exr_date_str", "name": "exr_date_str"},
                    {"data": "supplier", "name": "supplier"},
                    {"data": "net", "name": "net", "sClass": "text-right"},
                    {"data": "status_str", "name": "status_str"},
                    {"data": "Actions", "name": "", "sClass": "text-center"},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'exchange_order_payments':
            controller = baseUrl + 'index.php/exchange_order_payments/datatable';
            columnDef = [
                    {"data": "exrId", "name": "exrId"},
                    {"data": "exr_code", "name": "exr_code"},
                    {"data": "exr_date_str", "name": "exr_date_str"},
                    {"data": "supplier", "name": "supplier"},
                    {"data": "net", "name": "net","sClass": "text-right"},
                    {"data": "paid_amount", "name": "paid_amount","sClass": "text-right"},
                    {"data": "type_name", "name": "type_name"},
                    {"data": "status_str", "name": "status_str"},
                    {"data": "Actions", "name": "","sClass": "text-center"},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'services':
            controller = baseUrl + 'index.php/services/datatable';
            columnDef = [
                    {"data": "srId", "name": "srId"},
                    {"data": "jobsheet_number", "name": "jobsheet_number"},
                    {"data": "cust_name", "name": "cust_name"},
                    {"data": "received_date_str", "name": "received_date_str"},
                    {"data": "eng_name", "name": "eng_name"},
                    {"data": "service_status", "name": "service_status"},
                    {"data": "Updates", "name": "", "sClass": "text-center"},
                    {"data": "Actions", "name": "", "sClass": "text-center"},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'external_services':
            controller = baseUrl + 'index.php/external_services/datatable';
            columnDef = [
                    {"data": "srId", "name": "srId"},
                    {"data": "exter_service_no", "name": "exter_service_no"},
                    {"data": "issued_date_str", "name": "issued_date_str"},
                    {"data": "jobsheet_number", "name": "jobsheet_number"},
                    {"data": "service_partner_name", "name": "service_partner_name"},
                    {"data": "exter_amount", "name": "exter_amount"},
                    {"data": "exter_pay_status_str", "name": "exter_pay_status_str"},
                    {"data": "status_str", "name": "status_str"},
                    {"data": "Actions", "name": "", "sClass": "text-center"},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'external_service_payments':
            controller = baseUrl + 'index.php/external_service_payments/datatable';
            columnDef = [
                    {"data": "exrId", "name": "exrId"},
                    {"data": "exter_service_no", "name": "exter_service_no"},
                    {"data": "issued_date_str", "name": "issued_date_str"},
                    {"data": "jobsheet_number", "name": "jobsheet_number"},
                    {"data": "supplier", "name": "supplier"},
                    {"data": "net", "name": "net","sClass": "text-right"},
                    {"data": "paid_amount", "name": "paid_amount","sClass": "text-right"},
                    {"data": "type_name", "name": "type_name"},
                    {"data": "status_str", "name": "status_str"},
                    {"data": "Actions", "name": "","sClass": "text-center"},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'purchase_orders':
            controller = baseUrl + 'index.php/purchase_orders/datatable';
            columnDef = [
               {"data": "poId", "name": "poId"},
                    {"data": "poDate", "name": "poDate"},
                    {"data": "po_no", "name": "po_no"},
                    {"data": "supplier", "name": "supplier"},
                    {"data": "po_net_amount", "name": "po_net_amount", "sClass": "text-right"},
                    {"data": "Actions", "name": "", "sClass": "text-center"},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'goods_receive':
            controller = baseUrl + 'index.php/goods_receive/datatable';
            columnDef = [
               {"data": "pId", "name": "pId"},
                    {"data": "inv_no", "name": "inv_no"},
                    {"data": "pr_invoice_date", "name": "pr_invoice_date"},
                    {"data": "supplier", "name": "supplier"},
                    {"data": "net", "name": "net", "sClass": "text-right"},
                    {"data": "status_str", "name": "status_str"},
                    {"data": "Actions", "name": "", "sClass": "text-center"},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'purchase_payments':
            controller = baseUrl + 'index.php/purchase_payments/datatable';
            columnDef = [
               {"data": "prId", "name": "prId"},
                    {"data": "pr_invoice_date", "name": "pr_invoice_date"},
                    {"data": "supplier_invoice_no", "name": "supplier_invoice_no"},
                    {"data": "supplier", "name": "supplier"},
                    {"data": "net", "name": "net","sClass": "text-right"},
                    {"data": "paid_amount", "name": "paid_amount","sClass": "text-right"},
                    {"data": "type_name", "name": "type_name"},
                    {"data": "status_str", "name": "status_str"},
                    {"data": "Actions", "name": "","sClass": "text-center"},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'purchase_returns':
            controller = baseUrl + 'index.php/purchase_returns/datatable';
            columnDef = [
               {"data": "prId", "name": "prId"},
                    {"data": "inv", "name": "inv"},
                    {"data": "inv_date", "name": "inv_date"},
                    {"data": "pr_return_date", "name": "pr_return_date"},
                    {"data": "return_qty", "name": "return_qty"},
                    {"data": "Actions", "name": "","sClass": "text-center"},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'quotation':
            controller = baseUrl + 'index.php/quotation/datatable';
            columnDef = [
                {"data": "quoId", "name": "quoId"},
                    {"data": "quo_no", "name": "quo_no"},
                    {"data": "soDate", "name": "soDate"},
                    {"data": "name", "name": "name"},
                    {"data": "your_reference", "name": "your_reference"},
                    {"data": "total_amt", "name": "total_amt", "sClass": "text-right"},
                    {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'invoice':
            controller = baseUrl + 'index.php/invoice/datatable';
            columnDef = [
                {"data": "iterator", "name": "iterator"},
                    {"data": "invoice_no", "name": "invoice_no"},
                    {"data": "invoice_date", "name": "invoice_date"},
                    {"data": "name", "name": "name"},
                    {"data": "invoice_reference", "name": "invoice_reference"},
                    {"data": "invoice_amount", "name": "invoice_amount", "sClass": "text-right"},
                    {"data": "Actions", "name": ""},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'invoice_payment':
            controller = baseUrl + 'index.php/invoice_payment/datatable';
            columnDef = [
                {"data": "pId", "name": "pId"},
                    {"data": "inv", "name": "inv"},
                    {"data": "pay", "name": "pay"},
                    {"data": "date", "name": "date"},
                    {"data": "name", "name": "name"},
                    {"data": "ref", "name": "ref"},
                    {"data": "paid_amount", "name": "paid_amount","sClass": "text-right"},
                    {"data": "type_name", "name": "type_name"},
                    {"data": "status_str", "name": "status_str"},
                    {"data": "Actions", "name": "","sClass": "text-center"},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'invoice_return':
            controller = baseUrl + 'index.php/invoice_return/datatable';
            columnDef = [
                {"data": "srId", "name": "srId"},
                    {"data": "ret_date", "name": "ret_date"},
                    {"data": "inv", "name": "inv"},
                    {"data": "inv_date", "name": "inv_date"},
                    {"data": "return_qty", "name": "return_qty"},
                    {"data": "Actions", "name": "", "sClass": "text-center"},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'invoice_return_warrantyitem':
            controller = baseUrl + 'index.php/invoice_return_warrantyitem/datatable';
            columnDef = [
                {"data": "srId", "name": "srId"},
                    {"data": "ret_date", "name": "ret_date"},
                    {"data": "inv", "name": "inv"},
                    {"data": "pdt_code", "name": "pdt_code"},
                    {"data": "pdt_desc", "name": "pdt_desc"},
                    {"data": "return_qty", "name": "return_qty"},
                    {"data": "warranty_cleared_str", "name": "warranty_cleared_str"},
                    {"data": "Actions", "name": "", "sClass": "text-center"},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'delivery_orders':
            controller = baseUrl + 'index.php/delivery_orders/datatable';
            columnDef = [
                {"data": "dId", "name": "dId"},
                    {"data": "inv", "name": "inv"},
                    {"data": "date", "name": "date"},
                    {"data": "name", "name": "name"},
                    {"data": "do_quantity", "name": "do_quantity"},
                    {"data": "so_quantity", "name": "so_quantity"},
                    {"data": "Actions", "name": "", "sClass": "text-center"},   
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'inventory':
            controller = baseUrl + 'index.php/inventory/datatable';
            columnDef = [
                {"data": "bId", "name": "bId"},
				{"data": "code", "name": "code"},
                                {"data": "bar_code", "name": "bar_code", "sClass": "datatable-column-hidden"},
                                {"data": "s_no", "name": "s_no", "sClass": "datatable-column-hidden"},
				{"data": "cat", "name": "cat"},
				{"data": "product", "name": "product"},
				{"data": "quantity", "name": "quantity"},
				{"data": "Actions", "name": "", "sClass": "text-center"},  
            ];
            colSort = [[1, "asc"]];
        break;
        
        case 'adjust_stock':
            controller = baseUrl + 'index.php/adjust_stock/datatable';
            columnDef = [
                {"data": "iId", "name": "iId"},
				{"data": "created_on", "name": "created_on"},
				{"data": "code", "name": "code"},
                                {"data": "bar_code", "name": "bar_code", "sClass": "datatable-column-hidden"},
                                {"data": "s_no", "name": "s_no", "sClass": "datatable-column-hidden"},
				{"data": "cat", "name": "cat"},
				{"data": "product", "name": "product"},
				{"data": "qty", "name": "qty"},
				{"data": "adjust_type_str", "name": "adjust_type_str"},
				{"data": "rem", "name": "rem"},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'transfer_stock':
            controller = baseUrl + 'index.php/transfer_stock/datatable';
            columnDef = [
                 {"data": "tId", "name": "tId"},
                    {"data": "tracking_no", "name": "tracking_no"},
                    {"data": "bfrom", "name": "bfrom"},
                    {"data": "bto", "name": "bto"},
                    {"data": "transfer_date", "name": "transfer_date"},
                    {"data": "tot_product", "name": "tot_product"},
                    {"data": "status_str", "name": "status_str"},
                    {"data": "Actions", "name": "", "sClass": "text-center"},
            ];
            colSort = [[0, "desc"]];
    
        break;
        
        case 'payslip':
            controller = baseUrl + 'index.php/payslip/datatable';
            columnDef = [
                {"data": "pId", "name": "pId"},
                    {"data": "date", "name": "date"},
                    {"data": "emp", "name": "emp"},
                    {"data": "net", "name": "net", "sClass": "text-right"},
                    {"data": "status_str", "name": "status_str"},
                    {"data": "Actions", "name": "", "sClass": "text-center"},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'expense':
            controller = baseUrl + 'index.php/expense/datatable';
            columnDef = [
                {"data": "eId", "name": "eId"},
                    {"data": "date", "name": "date"},
                    {"data": "emp", "name": "emp"},
                    {"data": "net", "name": "net", "sClass": "text-right"},
                    {"data": "type", "name": "type"},
                    {"data": "status_str", "name": "status_str"},
                    {"data": "Actions", "name": "", "sClass": "text-center"},
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'petty_cash':
            controller = baseUrl + 'index.php/petty_cash/datatable';
            columnDef = [
                {"data": "id", "name": "id"},
                {"data": "date", "name": "date"},
                {"data": "opening_balance", "name": "opening_balance", "sClass": "text-right"},
                {"data": "closing_balance", "name": "closing_balance", "sClass": "text-right"},
                {"data": "Actions", "name": "", "sClass": "text-center"}
            ];
            colSort = [[0, "desc"]];
        break;
        
        case 'enquiry_log_form':
            controller = baseUrl + 'index.php/enquiry_log_form/datatable';
            columnDef = [
                {"data": "enqid", "name": "enqid"},
                {"data": "company_name", "name": "company_name"},
                {"data": "contact_person", "name": "contact_person"},
                {"data": "type_str", "name": "type_str"},
                {"data": "assigned_to", "name": "assigned_to"},
                {"data": "name", "name": "name"},
                {"data": "status_str", "name": "status_str"},
                {"data": "Actions", "name": "", "sClass": "text-center"}
            ];
            colSort = [[0, "desc"]];
        break;
        
        default:
        break;
    }
    
    var responsiveHelper_dt_basic = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var oTable = $('#dt_basic').dataTable({
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth": true,
        'iDisplayLength': 25,
        "preDrawCallback": function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_dt_basic) {
                responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
            }
        },
        "rowCallback": function (nRow) {
            responsiveHelper_dt_basic.createExpandIcon(nRow);
        },
        "drawCallback": function (oSettings) {
            responsiveHelper_dt_basic.respond();
        },
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": controller,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "iDisplayStart ": 20,
        "order": colSort,
        "lengthMenu": [[25, 50, 75, 100], [25, 50, 75, 100]],
        "columns": columnDef,
        "oLanguage": {
            "sProcessing": "<img src='" + baseUrl + "assets/images/st-13.gif'>"
        },
        "fnInitComplete": function () {
            //oTable.fnAdjustColumnSizing();
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            var oSettings = oTable.fnSettings();
            $("td:first", nRow).html(oSettings._iDisplayStart + iDisplayIndex + 1);
            $($(nRow).find('td')).each(function () {
                if ($(this).html() == '')
                    $(this).html("N/A");
            })
            return nRow;
        },
        'fnServerData': function (sSource, aoData, fnCallback)
        {
            $.ajax
                    ({
                        'dataType': 'json',
                        'type': 'POST',
                        'url': sSource,
                        'data': aoData,
                        'success': fnCallback
                    });
        },
    });
    
    
    if(current_controller == 'transfer_stock') {
        var controller_new = baseUrl + 'index.php/transfer_stock/datatable_new';
        var columnDef_new = [
             {"data": "tId", "name": "tId"},
                {"data": "tracking_no", "name": "tracking_no"},
                {"data": "bfrom", "name": "bfrom"},
                {"data": "bto", "name": "bto"},
                {"data": "transfer_date", "name": "transfer_date"},
                {"data": "tot_product", "name": "tot_product"},
                {"data": "status_str", "name": "status_str"},
                {"data": "Actions", "name": "", "sClass": "text-center"},
        ];
        var colSort_new = [[0, "desc"]];     

        var responsiveHelper_dt_basic_new = undefined;
        var breakpointDefinition_new = {
            tablet: 1024,
            phone: 480
        };
        var oTable = $('#dt_basic_new').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            'iDisplayLength': 25,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic_new) {
                    responsiveHelper_dt_basic_new = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition_new);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic_new.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic_new.respond();
            },
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": controller_new,
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "order": colSort_new,
            "lengthMenu": [[25, 50, 75, 100], [25, 50, 75, 100]],
            "columns": columnDef_new,
            "oLanguage": {
                "sProcessing": "<img src='" + baseUrl + "assets/images/st-13.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                $("td:first", nRow).html(oSettings._iDisplayStart + iDisplayIndex + 1);
                $($(nRow).find('td')).each(function () {
                    if ($(this).html() == '')
                        $(this).html("N/A");
                })
                return nRow;
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            },
        });
    }
    
    $('select[name="dt_basic_length"]').addClass('form-control');
    $('input[type="search"]').addClass('form-control');
    $('.fg-button').removeClass('ui-button');
});