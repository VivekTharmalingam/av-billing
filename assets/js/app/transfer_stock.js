var items = get_all_items('');
var product_codes = get_all_items(1);
var descriptions = get_all_items_descriptions('');
function valid_item_code(item_code) {
    var item_code_exists = false;
    for (var i in product_codes) {
        if (product_codes[i].pdt_code.trim().toLowerCase() == item_code.val().trim().toLowerCase()) {
            item_code_exists = true;
            break;
        }
    }

    if (!item_code_exists) {
        item_code.val('');
        item_code.focus();
        return false;
    }
}

function reset_item() {
    items = get_all_items('');
    product_codes = get_all_items(1);
    descriptions = get_all_items_descriptions('');
    $(".product_code, .product_description").removeClass('hasAutocomplete');
    set_product_autocomplete();
}


function get_all_items_descriptions() {
    var ajaxURL = baseUrl + 'invoice/search_item_description';
    var ajaxData = {}

    return ajaxRequest(ajaxURL, ajaxData, '');
}

function get_all_items(product_code) {
    var ajaxURL = baseUrl + 'invoice/search_item';
    var ajaxData = {
        product_code: product_code
    }

    return ajaxRequest(ajaxURL, ajaxData, '');
}

function hide_product_code_msg() {
    $('.product_code_label').slideUp();
}

function hide_error_description_msg() {
    $('.error_description').slideUp();
}

function set_product_autocomplete() {
    $(".product_code").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }

        $(this).on('change', function () {
            valid_item_code($(this));
        });

        $(this).autocomplete({
            source: function(request, response) {
                    var results = $.ui.autocomplete.filter(product_codes, request.term);
                    response(results.slice(0, 15));
            },
            focus: function (event, ui) {
                   show_available_quantity(event.currentTarget, ui.item.branch_products);
            },
            open: function() {
                    hide_available_quantity();
            },
            close: function() {
                    hide_available_quantity();
            },
            select: function (event, ui) {
                var items = $(this).closest('.items');
                var product_id_name = items.find('.product_id').prop('name');
                if ($('.product_details').find('.product_id[value="' + ui.item.id + '"]:not(.product_id[name="' + product_id_name + '"])').length) {
                    // Item already exists
                    if ($(this).data('value').trim() != ui.item.value.trim()) {
                        items = $('.product_details').find('.product_id[value="' + ui.item.id + '"]').closest('.items');

                        $(this).val('');
                        $(this).closest('.items').find('.product_code_label').slideDown();
                        setTimeout(hide_product_code_msg, 3000)
                        return false;
                    }
                    else {
                        $(this).val(ui.item.value.trim());
                        des = ui.item.pdt_name;
                        if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                            des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                        }
                        items.find('.product_description').val(des.trim());
                    }
                }
                else {
                    $(this).data('value', ui.item.pdt_code.trim());
                    $(this).val(ui.item.pdt_code.trim());
                    items.find('.product_id').val(ui.item.id);

                    des = ui.item.pdt_name;
                    if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                        des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                    }
                    items.find('.product_code').data('value', ui.item.pdt_code.trim());
                    items.find('.product_description').val(des.trim());
                    items.find('.quantity').val(1);
                }
                

                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || event.keyCode;
                if (charCode == 13)
                    items.find('.quantity').focus();
                else if (event.type == 'autocompleteselect') {
                    items.find('.product_description').focus();
                    items.find('.product_description').trigger('focus');
                }

                return false;
            }
        });

        $(this).addClass('hasAutocomplete');
    });

    $(".product_description").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }

        $(this).autocomplete({
            source: function(request, response) {
                    var results = $.ui.autocomplete.filter(descriptions, request.term);
                    response(results.slice(0, 15));
            },
            focus: function (event, ui) {
				show_available_quantity(event.currentTarget, ui.item.branch_products);
            },
            open: function() {
                    hide_available_quantity();
            },
            close: function() {
                    hide_available_quantity();
            },
            select: function (event, ui) {
                //alert('test');
                var items = $(this).closest('.items');
                var product_id_name = items.find('.product_id').prop('name');
                if ($('.product_details').find('.product_id[value="' + ui.item.id + '"]:not(.product_id[name="' + product_id_name + '"])').length) {
                    // Item already exists
                    if ($(this).data('value').trim() != ui.item.value.trim()) {
                        items = $('.product_details').find('.product_id[value="' + ui.item.id + '"]').closest('.items');
                        $(this).val($(this).data('value').trim());
                        $(this).closest('.items').find('.error_description').slideDown();
                        setTimeout(hide_error_description_msg, 3000);
                        items.find('.quantity').focus();
                        //return false;
                    }
                    else {
                        $(this).val(ui.item.value.trim());
                    }
                }
                else {
                    items.find('.product_code').val(ui.item.pdt_code.trim());
                    items.find('.product_id').val(ui.item.id);

                    if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                        des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                    }
                    else {
                        des = ui.item.pdt_name;
                    }
                    des = des.trim();
                    items.find('.product_description').data('value', des);
                    items.find('.product_description').val(des);
                    items.find('.quantity').val(1);
                }

                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || event.keyCode;
                if (charCode == 13)
                    items.find('.quantity').focus();
                else if (event.type == 'autocompleteselect')
                    items.find('.quantity').focus();

                return true;
            }
        });

        $(this).addClass('hasAutocomplete');
    });
    
}

function reset_select_option() {
   
    $('.frm_branch_id').find('option').each(function(index, element){
        if($(element).val() == $('select.to_branch_id').val()) {
              $(element).prop('disabled', true);
        } else {
              $(element).prop('disabled', false);
        }
    });
    
    $('.to_branch_id').find('option').each(function(index, element){
        if($(element).val() == $('select.frm_branch_id').val()) {
              $(element).prop('disabled', true);
        } else {
              $(element).prop('disabled', false);
        }
    });
      
}

var addRowIndex = 0;
var scanItem = 0;
var tabPressed = 0;
$(document).ready(function () {
    
    reset_select_option();
    
    $(document).on('change', 'select.frm_branch_id, select.to_branch_id', function () {          
        reset_select_option();
    });
    
    $(document).on('blur', 'input.product_code', function (e) {
        var item = {};
        var product_code = $(this).val().trim();
        if (product_code != '') {
            for (var i in product_codes) {
                if ((product_code != '') && (product_codes[i].label.trim() == product_code)) {
                    item = product_codes[i];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: item});
                    break;
                }
            }
        }
    });
    
    $(document).on('focusout', 'input.product_code, input.product_description', function (e) {
        hide_available_quantity();
    });
    
    $(document).on('focus', 'input.product_code, input.product_description', function (e) {
        var item = {};
        var product_code = $(this).closest('.items').find('.product_code').val().trim();
        if (product_code != '') {
            for (var i in product_codes) {
                if ((product_code != '') && (product_codes[i].label.trim() == product_code)) {
                    item = product_codes[i];
                    show_available_quantity($(this), product_codes[i].branch_products);
                    break;
                }
            }
        } else {
            hide_available_quantity();
        }
    });
    
    $(document).on('keyup', 'input.search_product', function (e) {
        var ser = $(this).val();
        var _this = $(this);
        if (ser) {
            $.each(items, function (key, value) {
                var s_no_array = new Array();
                if (value.s_no != '')
                    s_no_array = value.s_no.split(',');
				
                if ((ser.toUpperCase() === value.bar_code.toUpperCase()) || ($.inArray(ser, s_no_array) !== -1) || (ser.toUpperCase() == value.pdt_code.toUpperCase())) {
                    if ($('.product_details').find('.product_id[value="' + value.id + '"]').length) {
                        // Item already exists
                        var items = $('.product_details').find('.product_id[value="' + value.id + '"]').closest('.items');
                        var quantity = (items.find('.quantity').val() != '') ? parseFloat(items.find('.quantity').val()) : 0;
                        quantity += 1;
                        items.find('.quantity').val(quantity);
                    }
                    else {
                        if ($('.product_details .items:last .product_id').val() != '') {
                            // Last row occupied by another product.
                            $('.product_details .items:last .add-row').trigger('click');
                            $('input.search_product').focus();
                        }
                        var items = $('.product_details .items:last');
                        items.find('.product_id').val(value.id);
                        items.find('.product_code').val(value.pdt_code);
                        des = value.pdt_name;
                        if (value.cat_name != '' && value.cat_name != null) {
                            des = value.cat_name + ' ' + value.pdt_name;
                        }
                        items.find('.product_description').val(des);
                        items.find('.quantity').val(1);
                    }
                    
                    $(_this).val('');
                    $(_this).focus();
                    scanItem = 1;
                    return false;
                }
            });
        }
    });
	
    // To set autocomplete in product add row
    set_product_autocomplete();
    
    addRowIndex = $('.items').length - 1;
    /* Add row functionality for material */
    $(document).on('click', '.items .add-row', function () {
        addRowIndex++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.items').clone();
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');
        itemObj.find('.brand_name').html('');
        itemObj.find('.brand_label').hide();
        //itemObj.find('.vat_percentage').attr('checked', false);

        itemObj.find('input, select, textarea').each(function (index, element) {
            if ($(element).hasClass('item_hidden_ids')) {
                $(element).remove();
            }

            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndex + ']');
            }
            $(element).val('');

            if ($(element).hasClass('product_description'))
                $(element).data('value', '');

            if ($(element).hasClass('hasAutocomplete'))
                $(element).removeClass('hasAutocomplete');

            if ($(element).hasClass('product_code')) {
                $(element).data('value', '');
            }

        });

        var materialContainer = $(this).parents('.material-container');
        var optionsLength = items.length;
        if (optionsLength > $(materialContainer).find('.items').length) {
            if ($(this).parent().find('.remove-row').length)
                $(this).remove();
            else
                $(this).removeClass('add-row fa-plus-circle').addClass('remove-row fa-minus-circle');

            if ($(itemObj).find('.remove-row').length == 0)
                $(itemObj).find('.add-row').after('<i class="fa fa-2x fa-minus-circle remove-row"></i>');

            $(materialContainer).find('.items:last').after(itemObj);
        }
        else if ((optionsLength == $(materialContainer).find('.items').length) || (optionsLength == 0)) {
            var smallBoxTitle = 'Sorry ! ...';
            var smallBoxMsg = '<i class="fa fa-info-circle"></i> <i>You can\'t add more...<br> Because you have';
            smallBoxMsg += (optionsLength > 0) ? ' only ' + optionsLength : ' no';
            smallBoxMsg += ($(materialContainer).find('.items').length > 1) ? ' items' : ' item';
            smallBoxMsg += '.</i>';

            if ($('#divSmallBoxes').html().length == 0)
                showSmallBox(smallBoxTitle, smallBoxMsg);
        }

        set_product_autocomplete();
        $('.items:last .product_code').focus();
    });

    /* Remove row functionality for material */
    $(document).on('click', '.items .remove-row', function () {
        var materialContainer = $(this).parents('.material-container');
        $(this).parents('.items').remove();
        if (materialContainer.find('.items:last').find('.add-row').length == 0) {
            materialContainer.find('.items:last').find('.remove-row').before('<i class="fa fa-2x fa-plus-circle add-row"></i>');
        }

        if ((materialContainer.find('.items').length == 1) && (materialContainer.find('.items:last').find('.add-row').length)) {
            materialContainer.find('.items:last').find('.remove-row').remove();
        }
        
        $('.items:last .product_code').focus();
    });
    
    $.validator.addMethod('mat_already_exist', function (value, element) {
        var prevItems = $(element).parents('.items').prevAll('.items');
        return (prevItems.find('.product_id[value="' + value + '"]').length) ? false : true;
    }, 'Item Code is already exist!');

    $.validator.addMethod('product_code_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please select Product Code!');

    $.validator.addMethod('qty_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please enter Quantity!');

    $.validator.addClassRules({
        product_code: {
            product_code_required: true,
            mat_already_exist: true
        },
        quantity: {
            qty_required: true
        }
    });

    $('select[name="frm_branch_id"], select[name="to_branch_id"]').on('change', function () {
        $('#transfer_stock_form').validate().element(this);
    });

    $('input[name="so_date"]').on('changeDate', function () {
        $('#transfer_stock_form').validate().element(this);
    });

    $("#transfer_stock_form").validate({
        // Rules for form validation
        invalidHandler: function (event, validator) {
            $('body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top - 100
            },
            '500',
                    'swing'
                    );
        },
        rules: {
            frm_branch_id: {
                required: true
            },
            to_branch_id: {
                required: true
            },
            transfer_date: {
                required: true
            }
        },
        // Messages for form validation
        messages: {
            frm_branch_id: {
                required: 'Please select From Branch!'
            },
            to_branch_id: {
                required: 'Please select To Branch!'
            },
            transfer_date: {
                required: 'Please select Date!'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
            if (scanItem) {
                scanItem = 0;
                return false;
            }
            
            form.submit();
        }
    });

    
});