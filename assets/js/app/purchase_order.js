/* Barcode search functions */
function get_product_details() {
    if ($('input[name="search_product"]').val() == '') {
        var smallBoxTitle = 'Sorry ! ...';
        var smallBoxMsg = '<i class="fa fa-info-circle"></i> <i>Please enter product code or name.</i>';
        if ($('#divSmallBoxes').html().length == 0)
            showSmallBox(smallBoxTitle, smallBoxMsg);
    }
    return false;
}

function calculate_discount() {
    var sub_total = ($('.sub_total').val() != '') ? parseFloat($('.sub_total').val()) : 0;
    var discount = ($('.discount').val() != '') ? parseFloat($('.discount').val()) : 0;
    if ($('.discount_type:checked').val() == '1') { // Discount percentage
        discount = parseFloat((discount / 100) * sub_total);
    }
    items.find('.discount_amount').html(discount.toFixed(2));
    items.find('.discount_amt').val(discount.toFixed(2));
    sub_total -= discount;
    items.find('.sub_total').val(sub_total.toFixed(2));
    calculate_total();
}

function calculate_total() {
    var sub_total = 0;
    $('.amount').each(function (index, element) {
        sub_total += ($(element).val() != '') ? parseFloat($(element).val()) : 0;
    });
    $('.sub_total').val(sub_total.toFixed(2));

    var discount = ($('.discount').val() != '') ? parseFloat($('.discount').val()) : 0;
    if ($('.discount_type:checked').val() == '1') { // Discount percentage
        discount = parseFloat((discount / 100) * sub_total);
    }

    $('.discount_amount').html(discount.toFixed(2));
    $('.discount_amt').val(discount.toFixed(2));
    sub_total -= discount;

    var total_amount = sub_total;
    var gst_percentage = 0;
    var gst_amount = 0;
    if($('.supplier_gst').val() != '2') {
        gst_percentage = ($('#hidden_gst').val() != '') ? parseFloat($('#hidden_gst').val()) : 0;
        var gst_amount = parseFloat((gst_percentage / 100) * sub_total);
    }
    
    if ($('.gst_type:checked').val() == '2') {
        total_amount = sub_total + gst_amount;
    }

    $('.gst_amount').val(gst_amount.toFixed(2));
    $('.gst_percentage').html(gst_percentage.toFixed(2));
    $('.total_amt').val(total_amount.toFixed(2));
    $('.payment_amount').html(total_amount.toFixed(2));
}

function get_all_items_descriptions() {
    var ajaxURL = baseUrl + 'invoice/search_item_description';
    var ajaxData = {}

    return ajaxRequest(ajaxURL, ajaxData, '');
}

function get_all_items(product_code) {
    var ajaxURL = baseUrl + 'invoice/search_item';
    var ajaxData = {
        product_code: product_code
    }

    return ajaxRequest(ajaxURL, ajaxData, '');
}

function hide_product_code_msg() {
    $('.product_code_label').slideUp();
}

function hide_error_description_msg() {
    $('.error_description').slideUp();
}

var items = get_all_items('');
//var product_codes = get_all_items(1);
var product_codes = get_all_items(1);
var descriptions = get_all_items_descriptions('');
function set_product_autocomplete() {
    $(".product_code").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }
        
        $(this).on('change', function() {
			valid_item_code($(this));
		});
        
        $(this).autocomplete({
            source: product_codes,
            focus: function (event, ui) {
                //$(this).val(ui.item.value);
                //return false;
            },
            select: function (event, ui) {
                var items = $(this).closest('.items');
				var product_id_name = items.find('.product_id').prop('name');
                if ($('.product_details').find('.product_id[value="' + ui.item.id + '"]:not(.product_id[name="' + product_id_name + '"])').length) {
                    // Item already exists
                    if ($(this).data('value') != ui.item.value) {
                        items = $('.product_details').find('.product_id[value="' + ui.item.id + '"]').closest('.items');
                        $(this).val('');
                        $(this).closest('.items').find('.product_code_label').slideDown();
                        setTimeout(hide_product_code_msg, 3000)
                        //return false;
                    }
                    else {
                        $(this).val(ui.item.value);
                    }
                }
                else {
                    $(this).val(ui.item.pdt_code);
                    items.find('.category_id').val(ui.item.cat_id);
                    items.find('.product_id').val(ui.item.id);
//                    items.find('.brand_name').html(ui.item.cat_name);
//                    items.find('.brand_label').slideDown();
//                    items.find('.product_description').val(ui.item.pdt_name);
                    des = ui.item.pdt_name;
                    if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                        des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                    }
                    items.find('.product_code').data('value', ui.item.pdt_code);
                    items.find('.product_description').val(des);
                    items.find('.price').val(ui.item.selling_price);
                    if (items.find('.cost').length)
                        items.find('.cost').val(ui.item.buying_price);
                    items.find('.quantity').val(1);
                    items.find('.amount').val(ui.item.buying_price);
                    get_supplier_itemcode(items,ui.item.id);
                }
                calculate_total();

                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || e.keyCode;
                if (charCode == 13)
                    items.find('.quantity').focus();
                else if (event.type == 'autocompleteselect')
                    items.find('.product_description').focus();

                /* To focus quantity of particular item end */

                return false;
            }
        });

        $(this).addClass('hasAutocomplete');
    });

    $(".product_description").each(function () {
        if ($(this).hasClass('hasAutocomplete')) {
            return true;
        }

        $(this).autocomplete({
            source: descriptions,
            focus: function (event, ui) {
                //$(this).val(ui.item.value);
                //return false;
            },
            select: function (event, ui) {
                var items = $(this).closest('.items');
				var product_id_name = items.find('.product_id').prop('name');
                if ($('.product_details').find('.product_id[value="' + ui.item.id + '"]:not(.product_id[name="' + product_id_name + '"])').length) {
                    // Item already exists
                    if ($(this).data('value') != ui.item.value) {
                        items = $('.product_details').find('.product_id[value="' + ui.item.id + '"]').closest('.items');
                        $(this).val($(this).data('value'));
                        $(this).closest('.items').find('.error_description').slideDown();
                        setTimeout(hide_error_description_msg, 3000);
                        items.find('.quantity').focus();
                        //return false;
                    }
                    else {
                        $(this).val(ui.item.value);
                    }
                }
                else {
                    items.find('.product_code').val(ui.item.pdt_code);
                    //$(this).val(ui.item.pdt_code);
                    items.find('.category_id').val(ui.item.cat_id);
                    items.find('.product_id').val(ui.item.id);
//                    items.find('.brand_name').html(ui.item.cat_name);
//                    items.find('.brand_label').slideDown();
//                    items.find('.product_description').val(ui.item.pdt_name);

                    if (ui.item.cat_name != '' && ui.item.cat_name != null) {
                        des = ui.item.cat_name + ' ' + ui.item.pdt_name;
                    }
                    else {
                        des = ui.item.pdt_name;
                    }
                    items.find('.product_description').data('value', des);
                    items.find('.product_description').val(des);
                    items.find('.price').val(ui.item.selling_price);
                    if (items.find('.cost').length)
                        items.find('.cost').val(ui.item.buying_price);
                    items.find('.quantity').val(1);
                    items.find('.amount').val(ui.item.buying_price);
                    get_supplier_itemcode(items,ui.item.id);
                }
                calculate_total();

                /* To focus quantity of particular item start */
                event = event || window.event;
                var charCode = event.which || event.keyCode;
                if (charCode == 13)
                    items.find('.quantity').focus();
                else if (event.type == 'autocompleteselect')
                    items.find('.product_description').focus();

                /* To focus quantity of particular item end */
                //return false;
            }
        });

        $(this).addClass('hasAutocomplete');
    });
}

function delivery_to_change_focus() {
    if ($('#ship_to').val() == '')
        $('input[name="location_name"]').focus();
    else
        $('#your_reference').focus();
}

function focus_quantity(qty) {
    $(qty).focus();
}

function new_pop_up_window(_this) {
    var html = '';
    html += '<iframe src="' + $(_this).data('iframe-src') + '" style="height: 800px; width: 100%;max-width: 980px;"></iframe>';
    open_new_window(html);
    var iframe_height = $('#new_window .modal-content .modal-body').find('iframe').height();
    $('#new_window .modal-content').height((iframe_height + 130) + 'px');
    $('#new_window .modal-dialog').height((iframe_height + 130) + 'px');
    //$("#new_window iframe").contents().find("header").remove();
    //alert($('#new_window .modal-content .modal-body').find('iframe').height());
}

function new_pop_up_window_close() {
    product_codes = get_all_items(1);
    descriptions = get_all_items_descriptions('');
    $(".product_code, .product_description").removeClass('hasAutocomplete');
    set_product_autocomplete();
}

function get_supplier_details(sup) {
    if (sup != '') {
        var ajaxURL = baseUrl + 'purchase_orders/get_supplier_details';
        var ajaxData = {
            sup_id: sup,
        }
        var loaderSelector = 'select[name="sup_name"]';
        var data = ajaxRequest(ajaxURL, ajaxData, loaderSelector);
        $(".hidden_sup").val(data.sup.id);
        $(".contact_person").val(data.sup.contact_name);
        $(".contact_no").val(data.sup.phone_no);
        $(".supplier_address").val(data.sup.address);
        
        if (data.sup.gst_type == '2') {
            $('.supplier_gst').val('2');
            $('.without_gst').prop('checked', true);
            $('.gst_details').hide();
        } else {
            $('.supplier_gst').val('1');
            $('.with_gst').prop('checked', true);
            $('.gst_details').show();
        }
        
    } else {
        $(".hidden_sup").val('');
        $(".contact_person").val('');
        $(".contact_no").val('');
        $(".supplier_address").val('');
        $('.supplier_gst').val('2');
        $('.without_gst').prop('checked', true);
    }
}
var items = get_all_items('');
//var product_codes = get_all_items(1);
var product_codes = get_all_items(1);
var descriptions = get_all_items_descriptions('');

function valid_item_code(item_code) {
	var item_code_exists = false;
	for(var i in product_codes) {
		if (product_codes[i].pdt_code.trim().toLowerCase() == item_code.val().trim().toLowerCase()) {
			item_code_exists = true;
			break;
		}
	}
	
	if (!item_code_exists) {
		item_code.val('');
		item_code.focus();
		return false;
	}
}


function reset_item() {
    items = get_all_items('');
    product_codes = get_all_items(1);
    descriptions = get_all_items_descriptions('');
    $(".product_code, .product_description").removeClass('hasAutocomplete');
    set_product_autocomplete();
}

function reset_supplier() {
        var ajaxURL = baseUrl + 'purchase_orders/active_suppliers';
        var ajaxData = {}

        var data = ajaxRequest(ajaxURL, ajaxData, '');
        var _option = '';
        _option = '<option value="">Select Supplier name</option>';
        for(var i in data) {
                _option += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
        }	
        $('#sup_name').html(_option);
        $('#sup_name').select2({width: '100%'});
}

function reset_supplier_itemcode() {
        $('.sl_itm_code').each(function(){
            var items = $(this).closest('.items');
            var itm_id = items.find('.product_id').val();
            get_supplier_itemcode(items,itm_id);
        });
}

function get_supplier_itemcode(ItmRow,ItmId) {
        var supp_id = $('#sup_name').val(), temp='';
        if(supp_id != "" && ItmId != '') {
            var ajaxURL = baseUrl + 'purchase_orders/get_supplier_itemcode';
            var ajaxData = {
                item_id: ItmId,
                supp_id: supp_id
            }
            var data = ajaxRequest(ajaxURL, ajaxData, '');

            if (data.itemcode.sl_itm_code != '')
                ItmRow.find('.sl_itm_code').val(data.itemcode.sl_itm_code);
            else
                ItmRow.find('.sl_itm_code').val(temp);
        } else {
            ItmRow.find('.sl_itm_code').val(temp);  
        }
}

var addRowIndex = 0;
var tabPressed = 0;
var scanItem = 0;
var active = $('.sup_name').parents('form').hasClass('add_form') ? 'add' : 'edit';
$(document).ready(function () {
    /*var ship_toSelect = $('#customer_name');
     $('#ship_to').on('keydown', function (e) {
     alert("select2:close");
     });*/

    $('.new-window-open').on('click', function () {
        new_pop_up_window($(this));
    });

    // Callback function of window close
    $('#new_window').on('hidden.bs.modal', function (e) {
        new_pop_up_window_close();
    });

    /* Function to focus customer name on first tab */
    if ($('.edit_form').length == 0) {
        $(document).keydown(function (e) {
            if ((e.which == 9) && (tabPressed == 0)) {
                $('#customer_name').select2('open');
                $('#customer_name').select2('val', '');
                $('#customer_name').focus();
                tabPressed++;
            }
        });
    }

    $("#your_reference").focusin(function () {
        $('#your_reference').data('focused', true);
    });

    $("#remarks").focusin(function () {
        if (!$('#your_reference').data('focused')) {
            $('#your_reference').focus();
        }
    });

    $(document).on('focusin', ".quantity, .price", function () {
        /*alert('test');
         if (!$(this).data('focused')) {
         $(this).focus();
         }
         
         return false;*/
        $(this).select();
    });

    $('.so_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });

    $("#search_product").keydown(function (e) {
        e = e || window.event;
        var charCode = e.which || e.keyCode;
        if (charCode == 13) {
            return false;
        }
    });

    /* $(document).on('keydown', 'input.quantity', function (e) {
        e = e || window.event;
        var charCode = e.which || e.keyCode;
        if (charCode == 9) {
            $("#search_product").focus();
            return false;
        }
    }); */

    $(document).on('keyup', 'input.search_product', function () {
        var ser = $(this).val();
        var _this = $(this);
        if (ser) {
            $.each(items, function (key, value) {
                var s_no_array = new Array();
                if (value.s_no != '')
                        s_no_array = value.s_no.split(',');
                    
                    
//                if (ser === value.bar_code) {
                if ((ser.toUpperCase() === value.bar_code.toUpperCase()) || ($.inArray(ser, s_no_array) !== -1) || (ser.toUpperCase() == value.pdt_code.toUpperCase())) {
                    if ($('.product_details').find('.product_id[value="' + value.id + '"]').length) {
                        // Item already exists
                        var items = $('.product_details').find('.product_id[value="' + value.id + '"]').closest('.items');
                        var quantity = (items.find('.quantity').val() != '') ? parseFloat(items.find('.quantity').val()) : 0;
                        quantity += 1;
                        items.find('.quantity').val(quantity);
                        var price = (items.find('.cost').val() != '') ? parseFloat(items.find('.cost').val()) : 0;
                        items.find('.amount').val((price * quantity).toFixed(2));
                    }
                    else {
                        if ($('.product_details .items:last .product_id').val() != '') {
                            // Last row occupied by another product.
                            $('.product_details .items:last .add-row').trigger('click');
                        }
                        var items = $('.product_details .items:last');
                        items.find('.category_id').val(value.cat_id);
                        items.find('.product_id').val(value.id);
                        items.find('.product_code').val(value.pdt_code);
                        //items.find('.brand_name').html(value.cat_name);
                        //items.find('.brand_label').slideDown();
                        des = value.pdt_name;
                        if (value.cat_name != '' && value.cat_name != null) {
                            des = value.cat_name + ' ' + value.pdt_name;
                        }
                        items.find('.product_description').val(des);
                        items.find('.price').val(value.selling_price);
                        if (items.find('.cost').length)
                            items.find('.cost').val(value.buying_price);

                        items.find('.quantity').val(1);
                        items.find('.amount').val(value.buying_price);
                        get_supplier_itemcode(items,value.id);
                    }
                    items.find('.quantity').focus().select();

                    calculate_total();
                    $(_this).val('');
                    scanItem = 1;
                    return false;
                }
            });
        }
    });

    // To set autocomplete in product add row
    set_product_autocomplete();
    
    $(document).on('blur', 'input.product_code', function (e) {
		var item = {};
		var product_code = $(this).val().trim();
		if (product_code != '') {
			for(var i in product_codes) {
				if ((product_code != '') && (product_codes[i].label.trim() == product_code)) {
					item = product_codes[i];
					$(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: item});
					break;
				}
			}
		}
	});
	
	$(document).on('blur', 'input.product_description', function (e) {
		/*var item = {};
		var product_description = $(this).val().trim();
		if (product_description != '') {
			for(var i in descriptions) {
				if ((product_description != '') && (descriptions[i].label.trim() == product_description)) {
					item = descriptions[i];
					$(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: item});
					break;
				}
			}
		}*/
	});

    $(document).on('keyup', '.quantity, .cost', function () {
        var items = $(this).closest('.items');
        var quantity = (items.find('.quantity').val() != '') ? parseFloat(items.find('.quantity').val()) : 0;
        var price = (items.find('.cost').val() != '') ? parseFloat(items.find('.cost').val()) : 0;
        items.find('.amount').val((quantity * price).toFixed(2));
        calculate_total();
    });

    $(document).on('keyup', '.discount', function () {
        calculate_total();
    });

    $(document).on('change', '.discount_type, .discount', function () {
        calculate_total();
    });

    $(document).on('change', '.gst_type', function () {
        calculate_total();
    });
    
    if($('.supplier_gst').val() != "") {
        var subval = $('select.sup_name').val();
        get_supplier_details(subval);
        calculate_total();
    }
    
    /* Add row functionality for material */
    $(document).on('click', '.items .add-row', function () {
        addRowIndex++;
        var formType = $(this).parents('form').hasClass('edit_form') ? 'edit' : 'add';
        var itemObj = $(this).parents('.items').clone();
        itemObj.find('.label').remove();
        itemObj.find('em.invalid').remove();
        itemObj.find('.state-success').removeClass('state-success');
        itemObj.find('.state-error').removeClass('state-error');
        itemObj.find('.invalid').removeClass('invalid');
        itemObj.find('.brand_name').html('');
        itemObj.find('.brand_label').hide();
        //itemObj.find('.vat_percentage').attr('checked', false);

        itemObj.find('input, select, textarea').each(function (index, element) {
            if ($(element).hasClass('item_hidden_ids')) {
                $(element).remove();
            }

            if ($(element).prop('name') != '') {
                var name = $(element).prop('name').split('[')[0];
                name = ((formType == 'edit') && (name.indexOf('new') == -1)) ? 'new_' + name : name;
                $(element).prop('name', name + '[' + addRowIndex + ']');
            }
            $(element).val('');

            if ($(element).hasClass('product_description'))
                $(element).data('value', '');

            if ($(element).hasClass('hasAutocomplete'))
                $(element).removeClass('hasAutocomplete')

            /*if ($(element).prop('tagName').toLowerCase() == 'select') {
             if ($(element).prev().hasClass('select2')) {
             $(element).prev().remove();
             $(element).select2({width: '100%'});
             }
             }*/
        });

        var materialContainer = $(this).parents('.material-container');
        var optionsLength = items.length;
        if (optionsLength > $(materialContainer).find('.items').length) {
            if ($(this).parent().find('.remove-row').length)
                $(this).remove();
            else
                $(this).removeClass('add-row fa-plus-circle').addClass('remove-row fa-minus-circle');

            if ($(itemObj).find('.remove-row').length == 0)
                $(itemObj).find('.add-row').after('<i class="fa fa-2x fa-minus-circle remove-row"></i>');

            $(materialContainer).find('.items:last').after(itemObj);
        }
        else if ((optionsLength == $(materialContainer).find('.items').length) || (optionsLength == 0)) {
            var smallBoxTitle = 'Sorry ! ...';
            var smallBoxMsg = '<i class="fa fa-info-circle"></i> <i>You can\'t add more...<br> Because you have';
            smallBoxMsg += (optionsLength > 0) ? ' only ' + optionsLength : ' no';
            smallBoxMsg += ($(materialContainer).find('.items').length > 1) ? ' items' : ' item';
            smallBoxMsg += '.</i>';

            if ($('#divSmallBoxes').html().length == 0)
                showSmallBox(smallBoxTitle, smallBoxMsg);
        }

        set_product_autocomplete();
        $('.items:last .product_code').focus();
    });

    /* Remove row functionality for material */
    $(document).on('click', '.items .remove-row', function () {
        var materialContainer = $(this).parents('.material-container');
        $(this).parents('.items').remove();
        if (materialContainer.find('.items:last').find('.add-row').length == 0) {
            materialContainer.find('.items:last').find('.remove-row').before('<i class="fa fa-2x fa-plus-circle add-row"></i>');
        }

        if ((materialContainer.find('.items').length == 1) && (materialContainer.find('.items:last').find('.add-row').length)) {
            materialContainer.find('.items:last').find('.remove-row').remove();
        }
        calculate_total();
        $('.items:last .product_code').focus();
    });

    $.validator.addMethod('mat_already_exist', function (value, element) {
        var prevItems = $(element).parents('.items').prevAll('.items');
        return (prevItems.find('.product_id[value="' + value + '"]').length) ? false : true;
    }, 'Item Code is already exist!');

    $.validator.addMethod('product_code_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please select Product Code!');

    $.validator.addMethod('qty_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please enter Quantity!');

    $.validator.addMethod('payment_type_required', function (value, element) {
        return (value != '') ? true : false;
    }, 'Please select Payment Type!');

    $.validator.addClassRules({
        product_code: {
            product_code_required: true,
            mat_already_exist: true
        },
        quantity: {
            qty_required: true
        },
        payment_type_req: {
            payment_type_required: true
        }
    });

    $('select[name="customer_name"], select[name="ship_to"], select.category_id, select.product_id, select.quantity').on('change', function () {
        $('#po_form').validate().element(this);
    });

    $('input[name="so_date"], input[name="cheque_date"], input[name="dd_date"]').on('changeDate', function () {
        $('#po_form').validate().element(this);
    });

    $("#po_form").validate({
        // Rules for form validation
        invalidHandler: function (event, validator) {
            $('body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top - 100
            },
            '500',
                    'swing'
                    );
        },
        rules: {
            sup_name: {
                required: true
            },
            branch_name: {
                required: true
            },
            contact_person: {
                required: true
            },
            contact_no: {
                required: true
            },
            po_date: {
                required: true
            },
            status: {
                required: true
            }
        },
        // Messages for form validation
        messages: {
            sup_name: {
                required: 'Please select supplier name'
            },
            branch_name: {
                required: 'Please select branch name'
            },
            contact_person: {
                required: 'Please enter contact person name'
            },
            contact_no: {
                required: 'Please enter contact number'
            },
            po_date: {
                required: 'Please select date'
            },
            status: {
                required: 'Please select status'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
            if (scanItem) {
                scanItem = 0;
                return false;
            }

            form.submit();
        }
    });
    
    $('.sup_name').on('change', function () {
        get_supplier_details($(this).val());
        reset_supplier_itemcode();
        calculate_total();
    });
    

    

});

