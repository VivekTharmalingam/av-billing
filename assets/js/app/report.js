function category_wise_subcategory() {
	var _option = '<option value="">Select Sub Category</option>';
	if ($('#category_id').val() != '') {
		var ajaxURL = baseUrl + 'item/get_subcat_by_category';
		var ajaxData = {
			category: $('#category_id').val()
		}

		var data = ajaxRequest(ajaxURL, ajaxData, '');
		var sub_category_id = $('#category_id').data('sub_category_id');
		for (var i in data) {
			var select_txt = (sub_category_id == data[i].id) ? "selected" : "";
			_option += '<option value="' + data[i].id + '" ' + select_txt + '>' + data[i].sub_category_name + '</option>';
		}
	}
    
    $('#sub_category_id').html(_option);
    $('#sub_category_id').select2({width: '100%'});
}

$(document).ready(function () {
	category_wise_subcategory();
	
	$('#category_id').on('change', function() {
		category_wise_subcategory();
	});
});