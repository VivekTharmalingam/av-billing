
$(document).ready(function () {

    hide_show_service_partner('');
    
    $(document).on('click', '.add-user, .user-list', function () {
        $('.sup_main_div').find('em.invalid').remove();
        $('.sup_main_div').find('.state-success').removeClass('state-success');
        $('.sup_main_div').find('.state-error').removeClass('state-error');
        $('.sup_main_div').find('.invalid').removeClass('invalid');
        hide_show_service_partner($(this));
    });
    
    $('select[name="service_partner_id"]').on('change', function () {
        $('#external_services_form').validate().element(this);
        var sup_txt = ($(this).val() != '') ? $("#service_partner_id option:selected").text() : '';
        $('.service_partner_name').val(sup_txt);
    });
    
    $.validator.addMethod('valid_amount', function (value, element) {
        return (parseFloat(value) >= 0) ? true : false;
    }, 'Invalid&nbsp;amount!');
    
    var $orderForm = $("#external_services_form").validate({
        // Rules for form validation
        invalidHandler: function (event, validator) {
            $('body').animate({
                scrollTop: $(validator.currentForm).offset().top
            }, '500', 'swing');
        },
        rules: {
            service_partner_id: {
                required: true
            },
            service_partner_name_txt: {
                required: true
            },
            issued_date: {
                required: true
            },
            exter_amount: {
                required: true,
                valid_amount: true
            },
            exter_desc: {
                required: true
            }
        },
        // Messages for form validation
        messages: {
            service_partner_id: {
                required: 'Please select Service Partner Name!'
            },
            service_partner_name_txt: {
                required: 'Please enter Service Partner Name!'
            },
            issued_date: {
                required: 'Please select Issued Date!'
            },
            exter_amount: {
                required: 'Please enter Amount!'
            },
			exter_desc: {
				required: 'Please enter Description!'
			}
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
});

function hide_show_service_partner(clk_txt) {
    var act_class = '', temp='';
    if(clk_txt != '') {
        act_class = ($(clk_txt).hasClass('add-user') == true) ? 'list' : 'add';
//        if(act_class == 'add') { ('#external_services_form').validate().element($('select[name="service_partner_id"]')); } else {
//            ('#external_services_form').validate().element($('.sup_name_new'));
//        }
    } else {
        if ($('.add-user').css('display') == 'none'){ act_class = 'list'; 
        } else if ($('.user-list').css('display') == 'none'){ act_class = 'add'; }
    }
    
    if(act_class == 'add') {
        $('.sup_name_list_div').show();
        $('.add-user').show();
        $('.sup_name_new_div').hide();
        $('.user-list').hide();
        $('.sup_name_new').val(temp);
    } else {
        $('.sup_name_list_div').hide();
        $('.add-user').hide();
        $('.sup_name_new_div').show();
        $('.user-list').show();
        $('.service_partner_id').select2('val', '');
        $('.service_partner_name').val(temp);
    }
}


